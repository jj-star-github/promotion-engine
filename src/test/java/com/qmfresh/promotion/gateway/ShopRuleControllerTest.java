package com.qmfresh.promotion.gateway;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.gis.Shop;
import com.qmfresh.promotion.bean.sku.QueryProductSsuPrice;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.TimeUtil;
import com.qmfresh.promotion.dto.ModifyPriceParam;
import com.qmfresh.promotion.dto.SendVipPriceParam;
import com.qmfresh.promotion.dto.qmpp.QmppSkuDto;
import com.qmfresh.promotion.entity.AreaConditionQuery;
import com.qmfresh.promotion.entity.BaseArea;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.platform.application.ShopAppFundsService;
import com.qmfresh.promotion.platform.domain.model.settle.apportion.ApportionManager;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopPromotion;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopPromotionRule;
import com.qmfresh.promotion.platform.interfaces.platform.PlatformAccountController;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.AccountQueryDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.ShopRuleController;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto.*;
import com.qmfresh.promotion.platform.interfaces.shop.ShopAppCpController;
import com.qmfresh.promotion.platform.interfaces.shop.facade.cp.ShopPriceQueryDTO;
import com.qmfresh.promotion.platform.interfaces.shop.facade.cp.ShopSkuQueryDTO;
import com.qmfresh.promotion.platform.interfaces.shop.facade.funds.ShopWeekClearDTO;
import io.swagger.models.auth.In;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(properties = {"spring.profiles.active=test"})
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ShopRuleControllerTest {

    @Resource
    private MockMvc mockMvc;
    @Resource
    private ShopRuleController shopRuleController;
    @Resource
    private IQmExternalApiService iQmExternalApiService;
    @Resource
    private PlatformAccountController accountController;
    @Resource
    private ShopAppFundsService shopAppFundsService;

    private void postRequest(String uri, String content) throws Exception {
        mockMvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void test_shop_promotion_rule() throws Exception {
        BigDecimal loss =new BigDecimal("10");
        BigDecimal promotion =new BigDecimal("5");
        BigDecimal base =new BigDecimal("50");
        BigDecimal shop =new BigDecimal("50");
        BigDecimal hundred =new BigDecimal("100");

        BigDecimal itemGMV = loss.multiply(promotion);
        BigDecimal itemPromotion = itemGMV.multiply(base.divide(hundred));
        BigDecimal shopPromotion = itemPromotion.multiply(shop.divide(hundred));
//        System.out.println(itemGMV);
//        System.out.println(itemPromotion);
//        System.out.println(shopPromotion);


        ShopRuleReqDTO shopRuleReqDTO = new ShopRuleReqDTO();
//        shopRuleReqDTO.setShopGrade(1);
//        shopRuleReqDTO.setShopType(1);
//        shopRuleReqDTO.setLossPercent(loss);
//        shopRuleReqDTO.setPromotionPercent(promotion);
//        shopRuleReqDTO.setBasePercent(base);
//        shopRuleReqDTO.setShopPercent(shop);
        shopRuleReqDTO.setOperatorId(120);
        shopRuleReqDTO.setOperatorName("测试");

        shopRuleController.createShopRule(shopRuleReqDTO);
    }

    @Test
    public void queryRuleInfoList() throws Exception{
        QueryRuleInfoReqDTO queryRuleInfoReqDTO = new QueryRuleInfoReqDTO();
//        queryRuleInfoReqDTO.setShopType(1);
        ServiceResult<List<ShopPromotion>> list = shopRuleController.queryRuleInfoList(queryRuleInfoReqDTO);
        System.out.println("查询queryRuleInfoList========="+JSON.toJSONString(list));
    }

    @Test
    public void queryRuleDetailInfo() throws Exception{
        IdReqDto id = new IdReqDto();
        id.setId(0L);
        ServiceResult<ShopPromotionRule> shopPromotionRule= shopRuleController.queryRuleDetailInfo(id);
        System.out.println("查询queryRuleDetailInfo========="+JSON.toJSONString(shopPromotionRule));
    }

    @Test
    public void modifyShopRule() throws Exception {
        BigDecimal loss =new BigDecimal("7");
        BigDecimal promotion =new BigDecimal("5");
        BigDecimal base =new BigDecimal("60");
        BigDecimal shop =new BigDecimal("40");

        UpdateShopRuleReqDTO shopRuleReqDTO = new UpdateShopRuleReqDTO();
        shopRuleReqDTO.setId(Long.valueOf(1));
        shopRuleReqDTO.setShopGrade(3);
        shopRuleReqDTO.setShopType(3);
        shopRuleReqDTO.setLossPercent(loss);
        shopRuleReqDTO.setPromotionPercent(promotion);
        shopRuleReqDTO.setBasePercent(base);
        shopRuleReqDTO.setShopPercent(shop);

        shopRuleController.modifyShopRule(shopRuleReqDTO);
    }

    @Test
    public void getShopByCondition() throws Exception {
        Shop shop = new Shop();
        List<Shop> shopList = iQmExternalApiService.getShopByCondition(shop);
        System.out.println("查询门店信息========="+JSON.toJSONString(shopList));
    }

    @Test
    public void test() {
        QueryProductSsuPrice param = new QueryProductSsuPrice();
        param.setShopId(24);
        param.setSkuId(10024);
        List<QmppSkuDto> skuDtos = iQmExternalApiService.getProductSsuPriceListByCondition(param);
        System.out.println("----------------"+ JSON.toJSONString(skuDtos));
    }

    @Test
    public void time() throws Exception {

//        ShopIdDTO shopIdDTO = new ShopIdDTO();
//        shopIdDTO.setShopId(24);
//        System.out.println("查询门店信息========="+JSON.toJSONString(shopRuleController.queryRuleByShopId(shopIdDTO)));

        AccountQueryDTO accountQueryDTO = new AccountQueryDTO();
        accountQueryDTO.setShopType(4);
        System.out.println("查询门店信息========="+JSON.toJSONString(accountController.queryAccountList(accountQueryDTO)));

//        ShopWeekClearDTO shopWeekClearDTO = new ShopWeekClearDTO();
//        shopWeekClearDTO.setShopId(10);
//        shopWeekClearDTO.setTimeDay("2020-10-08");
//        System.out.println("查询门店信息========="+JSON.toJSONString(shopAppFundsService.queryWeekClear(shopWeekClearDTO)));
//
//        String rule = "去化奖励=(出货量-目标量)*";
//        String remark = " 最低扣至营销经费池不小于";
//        String jj = rule + "80%"+remark;
//
//        String format = "yyyy-MM-dd";
//        System.out.println("查询门店信息========="+DateUtil.getDateStringByTimeStamp(1602320348,format));
//        apportionManager.dayWmsReceive();

//        apportionManager.weekClear();

//        Date date = new Date();
//        Long time = 1599222324L;
//        System.out.println(time * 1000);
//        Date d = new Date(time * 1000);
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        System.out.println(sdf.format(d));
//        System.out.println(TimeUtil.getSecsOfMorning(time.intValue()));
//        System.out.println(sdf.format((long)TimeUtil.getSecsOfMorning(time.intValue())*1000));
//
//        Long t1=1598716800000L;
//        Long t2=1599580799000L;
//        long nd = 1000 * 24 * 60 * 60;
//        System.out.println(TimeUtil.isBetween(new Date(t1),new Date(t2),d));

//
//        String dd = new String("2017-06-09 10:22:22");
//
//        System.out.println(DateUtil.parseDate(dd));
//        {"shopId":24,"skuId":10034}

//        ShopPriceQueryDTO shopPriceQueryDTO = new ShopPriceQueryDTO();
//        shopPriceQueryDTO.setShopId(88);
//        shopPriceQueryDTO.setSkuId(10012);
//        shopPriceQueryDTO.setPromotionPrice(new BigDecimal(8));
//        shopPriceQueryDTO.setStartTime(1600963200L);
//        shopPriceQueryDTO.setEndTime(1600963200L);
//
//        System.out.println(JSON.toJSONString(shopAppCpController.shopDiffPrice(shopPriceQueryDTO)));
//
//        ShopSkuQueryDTO shopSkuQueryDTO = new ShopSkuQueryDTO();
//        shopSkuQueryDTO.setShopId(88);
//        shopSkuQueryDTO.setSkuId(19332);
//        System.out.println(JSON.toJSONString(shopAppCpController.shopSkuPrice(shopSkuQueryDTO)));

//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date sst = sdf.parse("2020-09-25 00:00:00");
//        Date sse = sdf.parse("2020-09-25 23:59:59");
//
//        Date bst = sdf.parse("2020-09-25 12:00:00");
//        Date bse = sdf.parse("2020-09-25 23:59:59");
//
//        System.out.println(DateUtil.isInTheTime(sst,bst,bse));
//        System.out.println(DateUtil.isInTheTime(sse,bst,bse));

//        List shopList = new ArrayList();
//        shopList.add(10);
//        AreaConditionQuery param = new AreaConditionQuery();
//        param.setShopIds(shopList);
//        List<BaseArea> areaDetail = iQmExternalApiService.selectAreaByShopIds(param);
//        System.out.println(JSON.toJSONString(areaDetail));

//        String ids = "90010200904203148204,90010200904202524605";
//        List<String> list = new ArrayList<>();
//        String str[] = ids.split(",");
//        list = Arrays.asList(str);
//        System.out.println(JSON.toJSONString(list));
//        apportionManager.updateOrder(list);

//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//        String start = sdf.format(DateUtil.getDateBefore(7));
//        String end = sdf.format(DateUtil.getDateBefore(1));
//        System.out.println(start);
//        System.out.println(end);

//        Calendar calendar = Calendar.getInstance();
//        calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)-1,0,0,0);
//        long tt = calendar.getTime().getTime()/1000;
//        System.out.println(tt);
//
//        Calendar calendar1 = Calendar.getInstance();
//        calendar1.set(calendar1.get(Calendar.YEAR),calendar1.get(Calendar.MONTH),calendar1.get(Calendar.DAY_OF_MONTH)-1,23,59,59);
//        long ttt = calendar1.getTime().getTime()/1000;
//        System.out.println(ttt);
    }
}
