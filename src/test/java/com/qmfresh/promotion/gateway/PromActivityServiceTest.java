package com.qmfresh.promotion.gateway;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.coupon.SendCouponReturnBean;
import com.qmfresh.promotion.bean.promotion.MzCampOn;
import com.qmfresh.promotion.bean.promotion.PromotionSsu;
import com.qmfresh.promotion.dto.*;
import com.qmfresh.promotion.platform.application.PromActivityService;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.impl.PromActivityManagerImpl;
import com.qmfresh.promotion.platform.domain.service.CouponReduceTypeEnum;
import com.qmfresh.promotion.platform.domain.service.SkuPriceService;
import com.qmfresh.promotion.platform.domain.service.vo.CouponPreferentialBean;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityPO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivitySendDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromCouponInfoDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromExecuteStrategyDTO;
import org.junit.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by wyh on 2019/6/11.
 *
 * @author wyh
 */
public class PromActivityServiceTest extends BaseServiceTest {

    @Resource
    private PromActivityService service;
    @Resource
    private PromActivityManagerImpl promActivityManagerIml;
    @Resource
    private PromActivityManager promActivityManager;
    @Resource
    private SkuPriceService skuPriceService;

    @Test
    public void price() {
        PromotionContext context = new PromotionContext();
        List<PromotionSsuContext> ssuContexts = new ArrayList<>();
        PromotionSsuContext context1 = new PromotionSsuContext();
        context1.setPreferentialPrice(new BigDecimal("2"));
        context1.setAmount(new BigDecimal("5"));
        context1.setOriginPrice(new BigDecimal("5"));
        context1.setSkuId(10060);
        ssuContexts.add(context1);
        context1 = new PromotionSsuContext();
        context1.setPreferentialPrice(new BigDecimal("1"));
        context1.setAmount(new BigDecimal("1"));
        context1.setOriginPrice(new BigDecimal("1"));
        context1.setSkuId(10070);
        ssuContexts.add(context1);
        context1 = new PromotionSsuContext();
        context1.setPreferentialPrice(new BigDecimal("1"));
        context1.setAmount(new BigDecimal("1"));
        context1.setOriginPrice(new BigDecimal("1"));
        context1.setSkuId(10080);
        ssuContexts.add(context1);
        context.setSsuContexts(ssuContexts);
        PromExecuteStrategyDTO protocol = new PromExecuteStrategyDTO();
        protocol.setCoinToMoney(new BigDecimal("10"));
        CouponPreferentialBean couponPreferential = new CouponPreferentialBean();
        couponPreferential.setPreferentialType(CouponReduceTypeEnum.DISCOUNT.getCode());
        couponPreferential.setUseCouponSkuId(Arrays.asList(10060));
        couponPreferential.setCouponPreferentialMoney(new BigDecimal("8"));
        couponPreferential.setCouponDiscount(8);
        skuPriceService.realPrice(context, protocol, couponPreferential, "12343454456");
        System.out.println(JSON.toJSONString(context));
    }

    @Test
    public void executeStrategy() {
        PromExecuteStrategyDTO dto = new PromExecuteStrategyDTO();
        dto.setCoinToMoney(new BigDecimal("10"));
        dto.setFreeMoney(new BigDecimal("0.09"));
        Area area = new Area();
        area.setShopId(10);
        area.setCityId(1);
        dto.setArea(area);
        dto.setChannel(3);
        User user = new User();
        user.setUserId(20);
        dto.setUser(user);
        List<PromotionSsu> promotionSsuList = new ArrayList<>();
        /**
         * "amount": 20,
         *     "class1Id": 57,
         *     "class2Id": 280,
         *     "originPrice": 4,
         *     "priceAfterDiscount": 4,
         *     "priceType": 0,
         *     "skuId": 10007,
         *     "vipPrice": 4
         */
        PromotionSsu ssu = new PromotionSsu();
        ssu.setSkuId(10571);
        ssu.setAmount(new BigDecimal("3.257"));
        ssu.setClass1Id(57);
        ssu.setClass2Id(280);
        ssu.setOriginPrice(new BigDecimal("4"));
        ssu.setPriceAfterDiscount(new BigDecimal("4"));
        ssu.setVipPrice(new BigDecimal("4"));
        ssu.setPriceType(0);
        promotionSsuList.add(ssu);
        ssu = new PromotionSsu();
        ssu.setSkuId(10006);
        ssu.setAmount(new BigDecimal(10));
        ssu.setClass1Id(57);
        ssu.setClass2Id(280);
        ssu.setOriginPrice(new BigDecimal("4"));
        ssu.setPriceAfterDiscount(new BigDecimal("3.5"));
        ssu.setVipPrice(new BigDecimal("3.5"));
        ssu.setPriceType(0);
        promotionSsuList.add(ssu);
        dto.setPromotionSsuList(promotionSsuList);
        PromCouponInfoDTO couponInfo = new PromCouponInfoDTO();
        couponInfo.setBizChannel(3);
        couponInfo.setCouponCode("01044650198600777730");
        dto.setCouponInfoDTO(couponInfo);
        PromotionContext context = service.executeStrategy(dto, "123234234532454654");
        System.out.println("executeStrategy  resp = " + JSON.toJSONString(context));
    }

    @Test
    public void campOn() {
        PromotionMzCampOnReq campOnDTO = new PromotionMzCampOnReq();
        List<MzCampOn> mzCampOnList = new ArrayList<>();
        MzCampOn mzCampOn = new MzCampOn();
        mzCampOn.setActivityId(40l);
        mzCampOn.setCouponId(405l);
        mzCampOn.setGivingNum(3);
        mzCampOnList.add(mzCampOn);
        campOnDTO.setCampOnList(mzCampOnList);
        campOnDTO.setOrderCode("1234567890098");
        campOnDTO.setUserId(20);
        campOnDTO.setShopId(10);
        List<PromotionMzCampOnRsp> context = service.campOn(campOnDTO, "12323213432534");
        System.out.println("campOn  resp = " + JSON.toJSONString(context));
    }

    @Test
    public void send() {
        PromActivitySendDTO sendDTO = new PromActivitySendDTO();
        sendDTO.setSourceOrderNo("1234567890098");
        sendDTO.setSourceShopId(10);
        sendDTO.setUserId(20);
        List<SendCouponReturnBean> context = service.send(sendDTO, "12323213432534");
        System.out.println("send  resp = " + JSON.toJSONString(context));
    }


    @Test
    public void cleanActivityCache() {
        PromActivityPO activityPO = promActivityManagerIml.getById(16);
        promActivityManagerIml.cleanActivityCache(activityPO);

    }

    @Test
    public void endJob() {
        promActivityManager.endJob();
    }

    @Test
    public void openJob() {
        promActivityManager.openJob();
    }

}
