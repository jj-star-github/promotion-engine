package com.qmfresh.promotion.gateway;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.application.ShopAppFundsService;
import com.qmfresh.promotion.platform.domain.valueobj.funds.*;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsAccount;
import com.qmfresh.promotion.platform.domain.model.funds.FundsAccountManager;
import com.qmfresh.promotion.platform.domain.model.funds.enums.FundsBusinessTypeEnum;
import com.qmfresh.promotion.platform.interfaces.shop.facade.funds.ShopFundsChangeDTO;
import com.qmfresh.promotion.platform.interfaces.shop.facade.funds.ShopFundsChangeQueryDTO;
import org.junit.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyh on 2019/6/11.
 *
 * @author wyh
 */
public class FundsAccountServiceTest extends BaseServiceTest {

    @Resource
    private FundsAccountManager service;
    @Resource
    ShopAppFundsService shopAppFundsService;

    @Test
    public void byShopIdAndType() {
        FundsAccount account = service.byShopIdAndType(20, 2);
        System.out.println("账号查询======"+JSON.toJSONString(account));
    }

    @Test
    public void businessChange() {
        ShopFundsChangeQueryDTO queryDTO = new ShopFundsChangeQueryDTO();
        queryDTO.setShopId(24);
        queryDTO.setTimeDay("2020-08-29");
        List<String> userType = new ArrayList<>();
//        userType.add("-1");
        userType.add("1");
        userType.add("2");
        queryDTO.setUseType(userType);
        queryDTO.setBusinessType(3);
        List<ShopFundsChangeDTO> fundsChangeList = shopAppFundsService.businessChange(queryDTO);
        System.out.println(JSON.toJSONString(fundsChangeList));
    }

  @Test
    public void getSum (){
        System.out.println("根据门店类型统计======"+JSON.toJSONString(service.queryAccountSum(2)));
    }

    @Test
    public void lockAmount(){
        FundsLockVO lockDto = new FundsLockVO();
        lockDto.setBusinessId("1");
        lockDto.setShopId(20);
        lockDto.setAccountType(4);
        lockDto.setChangeAmount(new BigDecimal("8000"));
        System.out.println(service.lockAmount(lockDto));
    }

    @Test
    public void updateAccount2(){
        FundsTransactionVO dto = new FundsTransactionVO();
        dto.setBusinessId("1");
        dto.setShopId(20);
        dto.setShopType(4);
        dto.setShopName("测试店001");
        dto.setBusinessType(FundsBusinessTypeEnum.FUNDS_APPLY.getCode());
        dto.setBusinessCode("1");
        dto.setChangeAmount(new BigDecimal("-200"));
        dto.setRemark("申请测试");
        System.out.println(service.updateAccount(dto));
    }

    @Test
    public void updateAccountBySettle(){
        FundsSettleVO dto = new FundsSettleVO();
        dto.setShopId(28);
        dto.setShopType(4);
        dto.setShopName("测试店001");
        dto.setOrderCode("202008220002456");
        //门店 交易费用
        dto.setChangeAmount(new BigDecimal("10"));
        //订单金额
        dto.setRealPay(new BigDecimal("58.60"));
        //订单营销销售额
        dto.setMarketingPay(new BigDecimal("26.90"));
        dto.setFundsAmount(new BigDecimal("10.5"));
        dto.setShopFundsAmount(new BigDecimal("6.8"));
        dto.setShopUseAmount(new BigDecimal("3.8"));
        List<FundsOrderTransactionVO>  voList = new ArrayList<>();
        //单品收入
        FundsOrderTransactionVO vo = new FundsOrderTransactionVO();
        vo.setTargetShop(28);
        vo.setBusinessId("1"); //SKUID
        vo.setBusinessCode("202008220002456");
        vo.setBusinessType(FundsBusinessTypeEnum.SKU_INCOME.getCode());
        vo.setChangeAmount(new BigDecimal("1.56"));//单品门店分摊值
        vo.setBusinessGmv(new BigDecimal("2.9"));
        vo.setRemark("测试单品01");
        vo.setBusinessRealAmount(new BigDecimal("2.8")); //单品订单金额
        vo.setBusinessMarketingAmount(new BigDecimal("4")); //总营销费用
        voList.add(vo);
        //单品支出
        FundsOrderTransactionVO zcVO = new FundsOrderTransactionVO();
        zcVO.setTargetShop(28);
        zcVO.setActivityId(1l);
        zcVO.setBusinessId("1"); //SKUID
        zcVO.setBusinessCode("202008220002456");
        zcVO.setBusinessType(FundsBusinessTypeEnum.SKU_MARKETING.getCode());
        zcVO.setChangeAmount(new BigDecimal("-2"));
        FundsSkuVO skuVO = new FundsSkuVO();
        skuVO.setAmount(new BigDecimal("0.98")); //
        skuVO.setOriginPrice(new BigDecimal("1.20"));
        skuVO.setPriceAfterDiscount(new BigDecimal("0.9"));
        skuVO.setSkuId(1l);
        skuVO.setSkuName("测试单品01");
        skuVO.setRealAmount(new BigDecimal("1.5"));
        skuVO.setSkuFormat("斤");
        zcVO.setRemark(JSON.toJSONString(skuVO));
        zcVO.setBusinessRealAmount(new BigDecimal("2.8")); //单品订单金额
        voList.add(zcVO);

        //
        //单品支出
        FundsOrderTransactionVO dcVO = new FundsOrderTransactionVO();
        dcVO.setTargetShop(28);
        dcVO.setActivityId(1l);
        dcVO.setBusinessId("1"); //SKUID
        dcVO.setBusinessCode("202008220002456");
        dcVO.setBusinessType(FundsBusinessTypeEnum.ITEM_DISCOUNT.getCode());
        dcVO.setChangeAmount(new BigDecimal("-2"));
        FundsSkuVO dcSku = new FundsSkuVO();
        dcSku.setAmount(new BigDecimal("0.98")); //
        dcSku.setOriginPrice(new BigDecimal("1.20"));
        dcSku.setPriceAfterDiscount(new BigDecimal("0.9"));
        dcSku.setSkuId(1l);
        dcSku.setSkuName("测试单品01");
        dcSku.setRealAmount(new BigDecimal("1.5"));
        dcSku.setSkuFormat("斤");
        dcVO.setRemark(JSON.toJSONString(dcSku));
        dcVO.setBusinessRealAmount(new BigDecimal("2.8")); //单品订单金额
        voList.add(dcVO);
        //订单抹零--总部有
        FundsOrderTransactionVO mlVO = new FundsOrderTransactionVO();
        mlVO.setTargetShop(28);
        mlVO.setBusinessId("202008220002456"); //订单号
        mlVO.setBusinessCode("202008220002456");
        mlVO.setBusinessType(FundsBusinessTypeEnum.ORDER_ML.getCode());
        mlVO.setChangeAmount(new BigDecimal("-1.5"));
        mlVO.setRemark("抹零");
        mlVO.setBusinessRealAmount(new BigDecimal("2.8")); //订单金额
        voList.add(mlVO);
        //订单改价--门店有
        FundsOrderTransactionVO cpVO = new FundsOrderTransactionVO();
        cpVO.setTargetShop(28);
        cpVO.setBusinessId("202008220002456"); //订单号
        cpVO.setBusinessCode("202008220002456");
        cpVO.setBusinessType(FundsBusinessTypeEnum.ORDER_CP.getCode());
        cpVO.setChangeAmount(new BigDecimal("-1.8"));
        FundsOrderCPVO orderCPVO = new FundsOrderCPVO();
        orderCPVO.setOriginPrice(new BigDecimal("2.8"));
        orderCPVO.setSkuNum(4);
        cpVO.setRemark(JSON.toJSONString(skuVO));
        cpVO.setBusinessRealAmount(new BigDecimal("2.8")); //订单金额
        voList.add(cpVO);
        dto.setOrderTransaction(voList);
        System.out.println(service.updateAccountBySettle(dto));
    }
}
