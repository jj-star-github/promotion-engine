package com.qmfresh.promotion.gateway;

import com.alibaba.fastjson.JSONObject;
import com.qmfresh.promotion.common.business.ServiceResult;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(properties = {"spring.prof iles.active=test"})
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class BaseServiceTest {
    public void commonAssert(ServiceResult<?> result) {
        System.err.println(JSONObject.toJSONString(result));
        Assert.assertTrue(result.getSuccess());
        Assert.assertNotNull(result.getBody());
    }

}


