package com.qmfresh.promotion.gateway;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.promotion.PageQueryActivityParam;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = {"spring.profiles.active=test"})
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class PromotionActivityControllerTest {

    @Resource
    private MockMvc mockMvc;

    @Test
    public void test_promotion_activity_page_query() throws Exception {
        PageQueryActivityParam pageQueryActivityParam = new PageQueryActivityParam();
        pageQueryActivityParam.setPageIndex(1);
        pageQueryActivityParam.setPageSize(10);

        String uri = "/promotion/activity/pageQuery";
        String content = JSON.toJSONString(pageQueryActivityParam);
        postRequest(uri, content);
    }

    private void postRequest(String uri, String content) throws Exception {
        mockMvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
