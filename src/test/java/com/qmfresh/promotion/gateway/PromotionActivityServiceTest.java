package com.qmfresh.promotion.gateway;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.promotion.PageQueryActivityParam;
import com.qmfresh.promotion.bean.promotion.PromotionActivityBean;
import com.qmfresh.promotion.bean.promotion.PromotionActivityDto;
import com.qmfresh.promotion.bean.promotion.QueryActivityParam;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.dto.SearchVipActParam;
import com.qmfresh.promotion.dto.SearchVipSkuParam;
import com.qmfresh.promotion.dto.VipSkuVo;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.service.IPromotionActivityService;

import javax.annotation.Resource;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyh on 2019/6/11.
 *
 * @author wyh
 */
public class PromotionActivityServiceTest extends BaseServiceTest {

    @Resource
    private IPromotionActivityService promotionActivityService;

    @Test
    public void queryCurrentPromotionActivity() {
        QueryActivityParam param = new QueryActivityParam();
        param.setShopId(5);
        param.setIsOnline(1);
        System.out.println(JSON.toJSONString(promotionActivityService.queryCurrentPromotionActivity(param)));
    }

    @Test
    public void queryPromotionActivityByShop() {
        QueryActivityParam param = new QueryActivityParam();
        param.setShopId(5);
        param.setIsOnline(1);
        System.out.println(JSON.toJSONString(promotionActivityService.queryPromotionActivityByShop(param)));
    }

    @Test
    public void queryByCondition() {
        QueryActivityParam param = new QueryActivityParam();
        param.setShopId(10);
        param.setEffectBeginTime(DateUtil.getStartTimeStamp());
        param.setEffectEndTime(DateUtil.getEndTimeStamp());
        System.out.println(promotionActivityService.queryByCondition(param));
    }

    @Test
    public void pageQuery() {
        PageQueryActivityParam param = new PageQueryActivityParam();
        param.setModuleId(6);
        param.setPageIndex(1);
        param.setPageSize(30);
        ListWithPage<PromotionActivityBean> page = promotionActivityService.pageQuery(param);
        System.out.println(JSON.toJSONString(page));
    }

    @Test
    public void online() {
        PromotionActivity promotionActivity = new PromotionActivity();
        promotionActivity.setId(4L);
        promotionActivity.setUpdateUserId(12L);
        promotionActivity.setUpdateUserName("aaabbb");
        promotionActivityService.online(promotionActivity);
    }

    @Test
    public void searchVipSku() {
        SearchVipActParam param = new SearchVipActParam();
        param.setShopId(24);
        List<SearchVipSkuParam> skus = new ArrayList<>();
        SearchVipSkuParam sku = new SearchVipSkuParam();
        sku.setSkuId(15476);
        skus.add(sku);
        param.setSkus(skus);
        List<VipSkuVo> searchVipSku = promotionActivityService.searchVipSku(param);
    }



    @Test
    public void stop() {
        PromotionActivity promotionActivity = new PromotionActivity();
        promotionActivity.setId(2L);
        promotionActivity.setUpdateUserId(12L);
        promotionActivity.setUpdateUserName("bbb");
        promotionActivityService.stop(promotionActivity);
    }
}
