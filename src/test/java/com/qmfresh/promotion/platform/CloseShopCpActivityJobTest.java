package com.qmfresh.promotion.platform;

import com.qmfresh.promotion.platform.infrastructure.job.activity.shop.CloseShopCpActivityJob;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = {"spring.profiles.active=site"})
@RunWith(SpringRunner.class)
public class CloseShopCpActivityJobTest {

    @Resource
    private CloseShopCpActivityJob closeShopCpActivityJob;

    @Test
    public void test_close_shop_cp_activity() throws Exception {
        closeShopCpActivityJob.execute("");
    }

}
