package com.qmfresh.promotion.platform.infrastructure.job.activity.area;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = {"spring.profiles.active=site"})
@RunWith(SpringRunner.class)
public class StartAreaCpActivityJobTest {

    @Resource
    private StartAreaCpActivityJob startAreaCpActivityJob;

    @Test
    public void test_start_area_cp_activity_job() {
        try {
            startAreaCpActivityJob.execute("");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
