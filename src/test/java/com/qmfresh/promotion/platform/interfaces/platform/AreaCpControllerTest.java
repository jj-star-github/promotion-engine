package com.qmfresh.promotion.platform.interfaces.platform;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.Mocks;
import com.qmfresh.promotion.platform.interfaces.platform.facade.area.AreaCpCloseDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.area.AreaCpCreateDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.area.AreaCpQueryDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = {"spring.profiles.active=site"})
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class AreaCpControllerTest {

    @Resource
    private MockMvc mockMvc;

    @Test
    public void test_page_area_cp_activity() {
        String uri = "/platform/cp/area/cp/page";
        AreaCpQueryDTO areaCpQueryDTO = new AreaCpQueryDTO();
        areaCpQueryDTO.setPageNum(1);
        areaCpQueryDTO.setPageSize(10);
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(areaCpQueryDTO), uri);
    }

    @Test
    public void test_close_area_cp_activity() {

        String uri = "/platform/cp/area/cp/close";
        AreaCpCloseDTO areaCpCloseDTO = new AreaCpCloseDTO();
        areaCpCloseDTO.setActivityId(330L);
        areaCpCloseDTO.setRemark("手动关闭");
        areaCpCloseDTO.setOperatorId(1);
        areaCpCloseDTO.setOperatorName("测试");
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(areaCpCloseDTO), uri);

    }

    @Test
    public void test_create_area_cp_activity() {
        String uri = "/platform/cp/area/cp/create";
        String param = "{\"activityName\":\"zb-1\",\"beginPlanCpActivityTime\":1600704000000,\"endPlanCpActivityTime\":1600876799000,\"shopIds\":[88],\"skuCps\":[{\"skuId\":10011,\"rulePrice\":\"1\"},{\"skuId\":10013,\"rulePrice\":\"1\"},{\"skuId\":13732,\"rulePrice\":\"1\"}]}";
        AreaCpCreateDTO cpCreateDTO = JSON.parseObject(param, new TypeReference<AreaCpCreateDTO>() {
        });
        cpCreateDTO.setMarketingAccountId(1L);
        cpCreateDTO.setOperatorId(1);
        cpCreateDTO.setOperatorName("test");
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(cpCreateDTO), uri);
    }

    @Test
    public void test_page_shop_of_area_cp_activity() {
        String uri = "/platform/cp/area/cp/shop/page";
        AreaCpQueryDTO areaCpQueryDTO = new AreaCpQueryDTO();
        areaCpQueryDTO.setPageNum(1);
        areaCpQueryDTO.setPageSize(10);
        areaCpQueryDTO.setActivityId(1578L);
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(areaCpQueryDTO), uri);
    }
}
