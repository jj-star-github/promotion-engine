package com.qmfresh.promotion.platform.interfaces.shop;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.Mocks;
import com.qmfresh.promotion.platform.interfaces.shop.facade.cp.ShopCpCloseDTO;
import com.qmfresh.promotion.platform.interfaces.shop.facade.cp.ShopCpCreateDTO;
import com.qmfresh.promotion.platform.interfaces.shop.facade.cp.ShopCpQueryDTO;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = "spring.profiles.active=site")
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ShopAppCpControllerTest {

    @Resource
    private MockMvc mockMvc;

    @Test
    public void test_time() {
        Integer millisOfDay = DateTime.now().getMillisOfDay();
        System.out.println(millisOfDay);
        DateTime.now().withTimeAtStartOfDay().plusDays(1);
    }

    /**
     * 创建门店改价
     * <p>
     * 1. 营销费用超额
     */
    @Test
    public void test_shop_create_cp() {

        Map<String, Object> header = new HashMap<>();
        header.put("shopId", 24);
        header.put("userId", 1023);


        ShopCpCreateDTO shopCpCreateDTO = new ShopCpCreateDTO();
        shopCpCreateDTO.setSkuId(19143);
        shopCpCreateDTO.setPlanStartActivityTime(DateTime.now().withTimeAtStartOfDay().getMillis());
        shopCpCreateDTO.setPlanEndActivityTime(DateTime.now().withTimeAtStartOfDay().getMillis());
        shopCpCreateDTO.setRulePrice(new BigDecimal("1.021"));
        shopCpCreateDTO.setPlanMarketingFee(new BigDecimal("100000.00"));
        shopCpCreateDTO.setOperatorId(1);
        shopCpCreateDTO.setOperatorName("系统");
        shopCpCreateDTO.setShopId(24);
        String content = JSON.toJSONString(shopCpCreateDTO);
        String uri = "/shop/cp/shop/createCommand";
        Mocks.postRequestBody(mockMvc, header, content, uri);
    }

    @Test
    public void test_shop_create_cp_original_price_exception() {

        Map<String, Object> header = new HashMap<>();
        header.put("shopId", 5);
        header.put("userId", 8);

        String param = "{\"operatorId\":8,\"operatorName\":\"钟思云\",\"planEndActivityTime\":1599408000000,\"planMarketingFee\":0.00,\"planStartActivityTime\":1599408000000,\"rulePrice\":2,\"shopId\":5,\"skuId\":10031}";
        String uri = "/shop/cp/shop/createCommand";
        Mocks.postRequestBody(mockMvc, header, param, uri);

    }

    /**
     * 1.营销费用超额
     * 2.当前时间正在运行，门店
     * 3.待开始重复的门店改价
     */
    @Test
    public void test_shop_create_cp_create() {

        Map<String, Object> header = new HashMap<>();
        header.put("shopId", 24);
        header.put("userId", 1023);

        ShopCpCreateDTO shopCpCreateDTO = new ShopCpCreateDTO();
        shopCpCreateDTO.setSkuId(19143);
        shopCpCreateDTO.setPlanStartActivityTime(DateTime.now().withTimeAtStartOfDay().plusDays(1).getMillis());
        shopCpCreateDTO.setPlanEndActivityTime(DateTime.now().withTimeAtStartOfDay().plusDays(2).getMillis());

        shopCpCreateDTO.setRulePrice(new BigDecimal("1.021"));
        shopCpCreateDTO.setPlanMarketingFee(new BigDecimal("100.00"));
        shopCpCreateDTO.setOperatorId(1);
        shopCpCreateDTO.setOperatorName("系统");
        shopCpCreateDTO.setShopId(24);
        String content = JSON.toJSONString(shopCpCreateDTO);
        String uri = "/shop/cp/shop/createCommand";
        Mocks.postRequestBody(mockMvc, header, content, uri);
    }

    /**
     * 分页查询门店改价
     */
    @Test
    public void test_page_shop_cp_activity() {
        Map<String, Object> header = new HashMap<>();
        header.put("shopId", 24);
        header.put("userId", 3);

        ShopCpQueryDTO shopCpQueryDTO = new ShopCpQueryDTO();
        shopCpQueryDTO.setPageNum(1);
        shopCpQueryDTO.setPageSize(20);
        shopCpQueryDTO.setShopId(24);
        shopCpQueryDTO.setOperatorId(1);
        shopCpQueryDTO.setOperatorName("测试");
        shopCpQueryDTO.setSkuKey("牛心白");
        String uri = "/shop/cp/shop/page";
        Mocks.postRequestBody(mockMvc, header, JSON.toJSONString(shopCpQueryDTO), uri);

    }

    /**
     * 门店改价关闭
     */
    @Test
    public void test_close_shop_cp_activity() {
        Map<String, Object> header = new HashMap<>();
        header.put("shopId", 24);
        header.put("userId", 1023);
        ShopCpCloseDTO closeDTO = new ShopCpCloseDTO();
        closeDTO.setActivityCpId(1300053816183439361L);
        String uri = "/shop/cp/shop/close";
        Mocks.postRequestBody(mockMvc, header, JSON.toJSONString(closeDTO), uri);
    }

    /**
     * 当前门店的总部改价
     */
    @Test
    public void test_list_shop_general_cp_activities() {
        Map<String, Object> header = new HashMap<>();
        header.put("shopId", 24);
        header.put("userId", 3);

        ShopCpQueryDTO shopCpQueryDTO = new ShopCpQueryDTO();
        shopCpQueryDTO.setPageNum(1);
        shopCpQueryDTO.setPageSize(20);
        shopCpQueryDTO.setStatus(3);
        String uri = "/shop/cp/general/list";
        Mocks.postRequestBody(mockMvc, header, JSON.toJSONString(shopCpQueryDTO), uri);
    }

    /**
     * 查询门店已改价的商品
     */
    @Test
    public void test_page_shop_cp_activity_changed() {
        Map<String, Object> header = new HashMap<>();
        header.put("shopId", 24);
        header.put("userId", 3);

        ShopCpQueryDTO shopCpQueryDTO = new ShopCpQueryDTO();
        shopCpQueryDTO.setPageNum(1);
        shopCpQueryDTO.setPageSize(20);
//        shopCpQueryDTO.setSkuKey("牛心白");

        String uri = "/shop/cp/shop/changed/page";
        Mocks.postRequestBody(mockMvc, header, JSON.toJSONString(shopCpQueryDTO), uri);
    }

}
