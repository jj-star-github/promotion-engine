package com.qmfresh.promotion.platform.interfaces.platform;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.Mocks;
import com.qmfresh.promotion.platform.interfaces.platform.facade.CpItemDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.SkuCpDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.general.*;
import com.qmfresh.promotion.platform.interfaces.platform.facade.shop.ShopCpQueryDTO;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = {"spring.profiles.active=site"})
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class GeneralCpControllerTest {

    @Resource
    private MockMvc mockMvc;

    @Test
    public void test_page_general_cp_activity() {

        String uri = "/platform/cp/general/page";

        GeneralCpQueryDTO generalCpQueryDTO = new GeneralCpQueryDTO();
        generalCpQueryDTO.setPageNum(1);
        generalCpQueryDTO.setPageSize(20);
//        generalCpQueryDTO.setActivityId(778L);
        String content = JSON.toJSONString(generalCpQueryDTO);

        Mocks.postRequestBody(mockMvc, content, uri);
    }

    @Test
    public void test_page_general_cp_activity_with_param() {

        String uri = "/platform/cp/general/page";

        GeneralCpQueryDTO generalCpQueryDTO = new GeneralCpQueryDTO();
        generalCpQueryDTO.setPageNum(1);
        generalCpQueryDTO.setPageSize(20);
        generalCpQueryDTO.setShopType(1);
        generalCpQueryDTO.setQueryShopId(10);
        generalCpQueryDTO.setActivityNameKey("总部16");
        generalCpQueryDTO.setFromPlanBeginCpActivityTime(DateTime.now().minusDays(0).getMillis());
        generalCpQueryDTO.setToPlanBeginCpActivityTime(DateTime.now().plusDays(1).getMillis());
        String content = JSON.toJSONString(generalCpQueryDTO);

        Mocks.postRequestBody(mockMvc, content, uri);
    }

    @Test
    public void test_page_shop_cp_activity() {
        ShopCpQueryDTO shopCpQueryDTO = new ShopCpQueryDTO();
        shopCpQueryDTO.setPageNum(1);
        shopCpQueryDTO.setPageSize(20);
        shopCpQueryDTO.setStatus(1);
        shopCpQueryDTO.setCurrentShopId(88);
        shopCpQueryDTO.setActivityNameKey("蒿子杆");

        String uri = "/platform/cp/shop/page";
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(shopCpQueryDTO), uri);
    }

    @Test
    public void test_create_general_cp() {

        GeneralCpCreateDTO generalCpCreateDTO = new GeneralCpCreateDTO();
        generalCpCreateDTO.setBeginPlanCpActivityTime(DateTime.now().plusDays(1).getMillis());
        generalCpCreateDTO.setEndPlanCpActivityTime(DateTime.now().plusDays(2).getMillis());
        generalCpCreateDTO.setShopIds(Arrays.asList(128, 147, 263));
        generalCpCreateDTO.setSkuCps(Arrays.asList(SkuCpDTO.create(19060, new BigDecimal("10.0"), BigDecimal.ZERO)));
        generalCpCreateDTO.setActivityName("测试活动");
        generalCpCreateDTO.setOperatorId(1);
        generalCpCreateDTO.setOperatorName("测试");

        String uri = "/platform/cp/general/create";
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(generalCpCreateDTO), uri);
    }

    @Test
    public void test_create_general_cp_create() {
        String json = "{\"activityName\":\"测试总部改价\",\"beginPlanCpActivityTime\":1599062400000,\"endPlanCpActivityTime\":1599148800000,\"shopIds\":[10,99,128,147,263,332,390,419,447,466,500,514,519,528,529,531,141,142,143,151,247,248,249,341,350,391,459,515,520,524,530,51,59,111,112,126,127,377,387,516,53,54,55,101,104,146,357,373,532,48,61,74,208,358,96,84,89,91,92,131,235,378,385,418,452,465,508,517,523,533,94,49,60,82,116,121,136,223,392,463,501,503,506,511,521,67,103,117,129,210,282,348,90,118,119,168,230,386,502,518,102,110,122,153,154,250,319,375,458,464,507,522,13,14,57,93,205,206,242,244,298,388,393,8],\"skuCps\":[{\"skuId\":17997,\"rulePrice\":\"1\"},{\"skuId\":18137,\"rulePrice\":\"1\"},{\"skuId\":18590,\"rulePrice\":\"1\"},{\"skuId\":18700,\"rulePrice\":\"1\"},{\"skuId\":18701,\"rulePrice\":\"1\"},{\"skuId\":18702,\"rulePrice\":\"1\"},{\"skuId\":18737,\"rulePrice\":\"1\"},{\"skuId\":18896,\"rulePrice\":\"1\"},{\"skuId\":18916,\"rulePrice\":\"1\"},{\"skuId\":17451,\"rulePrice\":\"1\"},{\"skuId\":17467,\"rulePrice\":\"1\"},{\"skuId\":18133,\"rulePrice\":\"1\"},{\"skuId\":18135,\"rulePrice\":\"1\"},{\"skuId\":18188,\"rulePrice\":\"1\"},{\"skuId\":18213,\"rulePrice\":\"1\"},{\"skuId\":18214,\"rulePrice\":\"1\"},{\"skuId\":18384,\"rulePrice\":\"1\"},{\"skuId\":10006,\"rulePrice\":\"1\"},{\"skuId\":10012,\"rulePrice\":\"1\"},{\"skuId\":10024,\"rulePrice\":\"1\"},{\"skuId\":10026},{\"skuId\":10027},{\"skuId\":10033},{\"skuId\":10034},{\"skuId\":10038},{\"skuId\":10041},{\"skuId\":10055},{\"skuId\":10068},{\"skuId\":10076},{\"skuId\":10087},{\"skuId\":10090},{\"skuId\":10101},{\"skuId\":10102},{\"skuId\":10103},{\"skuId\":10106},{\"skuId\":10107},{\"skuId\":10108},{\"skuId\":10109},{\"skuId\":10385},{\"skuId\":11026},{\"skuId\":11133},{\"skuId\":11136},{\"skuId\":11159},{\"skuId\":11165},{\"skuId\":12136},{\"skuId\":12142},{\"skuId\":12766},{\"skuId\":12847},{\"skuId\":13563},{\"skuId\":13720},{\"skuId\":13792},{\"skuId\":14889},{\"skuId\":14957},{\"skuId\":14961},{\"skuId\":14962},{\"skuId\":14986},{\"skuId\":15015},{\"skuId\":15073},{\"skuId\":15127},{\"skuId\":15143},{\"skuId\":15171},{\"skuId\":15702},{\"skuId\":16104},{\"skuId\":16211},{\"skuId\":16789},{\"skuId\":17402},{\"skuId\":17448},{\"skuId\":17724},{\"skuId\":17962},{\"skuId\":18503},{\"skuId\":18504},{\"skuId\":18612},{\"skuId\":18613},{\"skuId\":18614},{\"skuId\":18645},{\"skuId\":18646},{\"skuId\":18973},{\"skuId\":19254},{\"skuId\":19255},{\"skuId\":10007},{\"skuId\":10021},{\"skuId\":10025},{\"skuId\":10032},{\"skuId\":10040},{\"skuId\":10057},{\"skuId\":10074},{\"skuId\":10086},{\"skuId\":10096},{\"skuId\":10099},{\"skuId\":10100},{\"skuId\":10216},{\"skuId\":10219},{\"skuId\":10234},{\"skuId\":10386},{\"skuId\":10402},{\"skuId\":11682},{\"skuId\":12484},{\"skuId\":12799},{\"skuId\":12844},{\"skuId\":13296},{\"skuId\":13544},{\"skuId\":13569},{\"skuId\":13588},{\"skuId\":14765},{\"skuId\":14926},{\"skuId\":15323},{\"skuId\":15337},{\"skuId\":15630},{\"skuId\":15693},{\"skuId\":15996},{\"skuId\":16184},{\"skuId\":16219},{\"skuId\":16885},{\"skuId\":17098},{\"skuId\":17680},{\"skuId\":17721},{\"skuId\":17991},{\"skuId\":17995},{\"skuId\":17996},{\"skuId\":18136},{\"skuId\":18495},{\"skuId\":18505},{\"skuId\":18539},{\"skuId\":18541},{\"skuId\":10010},{\"skuId\":10020},{\"skuId\":10031},{\"skuId\":10039},{\"skuId\":10051},{\"skuId\":10061},{\"skuId\":10062},{\"skuId\":10065},{\"skuId\":10067},{\"skuId\":10072},{\"skuId\":10075},{\"skuId\":10082},{\"skuId\":10083},{\"skuId\":10085},{\"skuId\":10088},{\"skuId\":10094},{\"skuId\":10098},{\"skuId\":10110},{\"skuId\":10214},{\"skuId\":10244},{\"skuId\":10347},{\"skuId\":10373},{\"skuId\":10397},{\"skuId\":11073},{\"skuId\":11166},{\"skuId\":11647},{\"skuId\":11648},{\"skuId\":11689},{\"skuId\":11713},{\"skuId\":12113},{\"skuId\":12143},{\"skuId\":12470},{\"skuId\":12649},{\"skuId\":13806},{\"skuId\":14888},{\"skuId\":14987},{\"skuId\":14997},{\"skuId\":15419},{\"skuId\":15512},{\"skuId\":15513},{\"skuId\":15691},{\"skuId\":15717},{\"skuId\":15757},{\"skuId\":16100},{\"skuId\":16159},{\"skuId\":16235},{\"skuId\":16249},{\"skuId\":16325},{\"skuId\":16417},{\"skuId\":16529},{\"skuId\":18189},{\"skuId\":18491},{\"skuId\":18492},{\"skuId\":18493},{\"skuId\":18494},{\"skuId\":18501},{\"skuId\":18506},{\"skuId\":18507},{\"skuId\":18508},{\"skuId\":18535},{\"skuId\":18548},{\"skuId\":18733},{\"skuId\":18905},{\"skuId\":18546},{\"skuId\":18661},{\"skuId\":18662},{\"skuId\":18845},{\"skuId\":18849},{\"skuId\":19553},{\"skuId\":14861},{\"skuId\":17980},{\"skuId\":17981},{\"skuId\":18075},{\"skuId\":18294},{\"skuId\":18798},{\"skuId\":18799},{\"skuId\":19005},{\"skuId\":10153},{\"skuId\":10388},{\"skuId\":10389},{\"skuId\":10557},{\"skuId\":10563},{\"skuId\":10572},{\"skuId\":10576},{\"skuId\":10585},{\"skuId\":10587},{\"skuId\":10588},{\"skuId\":10603},{\"skuId\":10610},{\"skuId\":10617},{\"skuId\":11074},{\"skuId\":11657},{\"skuId\":11731},{\"skuId\":11766},{\"skuId\":12563},{\"skuId\":12564},{\"skuId\":12565},{\"skuId\":12716},{\"skuId\":12778},{\"skuId\":12811},{\"skuId\":12882},{\"skuId\":13186},{\"skuId\":13187},{\"skuId\":13582},{\"skuId\":13583},{\"skuId\":13686},{\"skuId\":13822},{\"skuId\":14693},{\"skuId\":14749},{\"skuId\":14750},{\"skuId\":14862},{\"skuId\":14863},{\"skuId\":15099},{\"skuId\":15104},{\"skuId\":15328},{\"skuId\":15474},{\"skuId\":15941},{\"skuId\":16010},{\"skuId\":16012},{\"skuId\":16013},{\"skuId\":16180},{\"skuId\":16181},{\"skuId\":16182},{\"skuId\":16187},{\"skuId\":16188},{\"skuId\":16230},{\"skuId\":16231},{\"skuId\":16329},{\"skuId\":16330},{\"skuId\":16482},{\"skuId\":16483},{\"skuId\":16897},{\"skuId\":16898},{\"skuId\":16899},{\"skuId\":16900},{\"skuId\":16901},{\"skuId\":16902},{\"skuId\":16903},{\"skuId\":16904},{\"skuId\":16905},{\"skuId\":16906},{\"skuId\":17327},{\"skuId\":17330},{\"skuId\":17331},{\"skuId\":17332},{\"skuId\":17560},{\"skuId\":17561},{\"skuId\":19847},{\"skuId\":10011},{\"skuId\":10013},{\"skuId\":10050},{\"skuId\":10093},{\"skuId\":10104},{\"skuId\":10105},{\"skuId\":10217},{\"skuId\":10218},{\"skuId\":11044},{\"skuId\":11162},{\"skuId\":11186},{\"skuId\":11305},{\"skuId\":12816,\"rulePrice\":\"1\"},{\"skuId\":13628,\"rulePrice\":\"1\"},{\"skuId\":13732,\"rulePrice\":\"1\"},{\"skuId\":15441,\"rulePrice\":\"1\"}]}";

        String json2 = "{\"activityName\":\"互动-测试\",\"beginPlanCpActivityTime\":1599148800000,\"endPlanCpActivityTime\":1599235200000,\"shopIds\":[10,99,128,147,263,332,390,419,447,466,500,514,519,528,529,531,141,142,143,151,247,248,249,341,350,391,459,515,520,524,530,51,145,148,167,200,220,262,111,112,126,127,377,387,516,53,54,55,101,104,146,357,373,532,294,345,355,359,360,382,61,74,208,358,96,84,347,363,372,405,56,169,187,340,351,366,397,426,177,194,212,238,255,267,315,131,235,378,385,418,452,465,508,517,523,533,94,279,280,284,300,60,82,116,121,136,223,392,463,501,503,506,511,521,67,103,117,129,210,282,348,90,118,119,168,230,386,502,518,102,110,122,153,154,250,319,375,458,464,507,522,13,5,11,272,273,277,295,322,323,324,353,361,368,371,261,268,269,293,79,85,114,228,437,439,440,456,467,469,473,474,504,422,429,430,205,206,242,244,298,388,393,8,183,256,259,260,352,402,421,513,160,181,185,191,197,218,219,49,165,186,236,257,258,330,344,281,287,297,335,336,354,356,370,379,423,455,48,93,159,178,196,214,314,367,399,427,158,202,216,234,308,400,195,213,232,254,310,311,317,342,155,175,176,201,233,237,316,157,179,215,231,309,312,343,92,137,189,471,166,180,221,222,227,333,457,468,475,505,525,526,527,57,6,105,106,115,124,199,334,436,470,59,274,290,299,301,303,321,428,241,243,245,275,62,276,305,401,407,410,415,265,283,285,78,149,162,318,337,339,394,403,412,138,150,184,304,364,411,414,24,47,50,120,123,152,211,395,404,406,472,86,97,113,264,413,88,100,144,374,376,383,389,89,91,15,203,229,349,396,425,95,107,172,252,253,320,80,135,170,188,207,365,384,58,81,108,109,125,132,239,240,424,325,327,328,329,362,380,381,398,408,409,416,417,31,63,64,133,134,139,156,190,192,226,246,251,266,288,289,291,292,296,306,313,326,338,420,431,432,433,434,435,438,441,442,443,444,445,446,448,449,450,451,453,454,460,461,462,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491],\"skuCps\":[{\"skuId\":17997,\"rulePrice\":\"1\"},{\"skuId\":18137,\"rulePrice\":\"1\"},{\"skuId\":18590,\"rulePrice\":\"1\"},{\"skuId\":18700,\"rulePrice\":\"1\"},{\"skuId\":18701,\"rulePrice\":\"1\"},{\"skuId\":18702,\"rulePrice\":\"1\"},{\"skuId\":18737,\"rulePrice\":\"1\"},{\"skuId\":18896,\"rulePrice\":\"1\"},{\"skuId\":18916,\"rulePrice\":\"1\"},{\"skuId\":17451,\"rulePrice\":\"1\"},{\"skuId\":17467,\"rulePrice\":\"1\"},{\"skuId\":18133,\"rulePrice\":\"1\"},{\"skuId\":18135,\"rulePrice\":\"1\"},{\"skuId\":18188},{\"skuId\":18213},{\"skuId\":18214},{\"skuId\":18384},{\"skuId\":10006},{\"skuId\":10012},{\"skuId\":10024},{\"skuId\":10026},{\"skuId\":10027},{\"skuId\":10033},{\"skuId\":10034},{\"skuId\":10038},{\"skuId\":10041},{\"skuId\":10055},{\"skuId\":10068},{\"skuId\":10076},{\"skuId\":10087},{\"skuId\":10090},{\"skuId\":10101},{\"skuId\":10102},{\"skuId\":10103},{\"skuId\":10106},{\"skuId\":10107},{\"skuId\":10108},{\"skuId\":10109},{\"skuId\":10385},{\"skuId\":11026},{\"skuId\":11133},{\"skuId\":11136},{\"skuId\":11159},{\"skuId\":11165},{\"skuId\":12136},{\"skuId\":12142},{\"skuId\":12766},{\"skuId\":12847},{\"skuId\":13563},{\"skuId\":13720},{\"skuId\":13792},{\"skuId\":14889},{\"skuId\":14957},{\"skuId\":14961},{\"skuId\":14962},{\"skuId\":14986},{\"skuId\":15015},{\"skuId\":15073},{\"skuId\":15127},{\"skuId\":15143},{\"skuId\":15171},{\"skuId\":15702},{\"skuId\":16104},{\"skuId\":16211},{\"skuId\":16789},{\"skuId\":17402},{\"skuId\":17448},{\"skuId\":17724},{\"skuId\":17962},{\"skuId\":18503},{\"skuId\":18504},{\"skuId\":18612},{\"skuId\":18613},{\"skuId\":18614},{\"skuId\":18645},{\"skuId\":18646},{\"skuId\":18973},{\"skuId\":19254},{\"skuId\":19255},{\"skuId\":10007},{\"skuId\":10021},{\"skuId\":10025},{\"skuId\":10032},{\"skuId\":10040},{\"skuId\":10057},{\"skuId\":10074},{\"skuId\":10086},{\"skuId\":10096},{\"skuId\":10099},{\"skuId\":10100},{\"skuId\":10216},{\"skuId\":10219},{\"skuId\":10234},{\"skuId\":10386},{\"skuId\":10402},{\"skuId\":11682},{\"skuId\":12484},{\"skuId\":12799},{\"skuId\":12844},{\"skuId\":13296},{\"skuId\":13544},{\"skuId\":13569},{\"skuId\":13588},{\"skuId\":14765},{\"skuId\":14926},{\"skuId\":15323},{\"skuId\":15337},{\"skuId\":15630},{\"skuId\":15693},{\"skuId\":15996},{\"skuId\":16184},{\"skuId\":16219},{\"skuId\":16885},{\"skuId\":17098},{\"skuId\":17680},{\"skuId\":17721},{\"skuId\":17991},{\"skuId\":17995},{\"skuId\":17996},{\"skuId\":18136},{\"skuId\":18495},{\"skuId\":18505},{\"skuId\":18539},{\"skuId\":18541},{\"skuId\":10010},{\"skuId\":10020},{\"skuId\":10031},{\"skuId\":10039},{\"skuId\":10051},{\"skuId\":10061},{\"skuId\":10062},{\"skuId\":10065},{\"skuId\":10067},{\"skuId\":10072},{\"skuId\":10075},{\"skuId\":10082},{\"skuId\":10083},{\"skuId\":10085},{\"skuId\":10088},{\"skuId\":10094},{\"skuId\":10098},{\"skuId\":10110},{\"skuId\":10214},{\"skuId\":10244},{\"skuId\":10347},{\"skuId\":10373},{\"skuId\":10397},{\"skuId\":11073},{\"skuId\":11166},{\"skuId\":11647},{\"skuId\":11648},{\"skuId\":11689},{\"skuId\":11713},{\"skuId\":12113},{\"skuId\":12143},{\"skuId\":12470},{\"skuId\":12649},{\"skuId\":13806},{\"skuId\":14888},{\"skuId\":14987},{\"skuId\":14997},{\"skuId\":15419},{\"skuId\":15512},{\"skuId\":15513},{\"skuId\":15691},{\"skuId\":15717},{\"skuId\":15757},{\"skuId\":16100},{\"skuId\":16159},{\"skuId\":16235},{\"skuId\":16249},{\"skuId\":16325},{\"skuId\":16417},{\"skuId\":16529},{\"skuId\":18189},{\"skuId\":18491},{\"skuId\":18492},{\"skuId\":18493},{\"skuId\":18494},{\"skuId\":18501},{\"skuId\":18506},{\"skuId\":18507},{\"skuId\":18508},{\"skuId\":18535},{\"skuId\":18548},{\"skuId\":18733},{\"skuId\":18905},{\"skuId\":18546},{\"skuId\":18661},{\"skuId\":18662},{\"skuId\":18845},{\"skuId\":18849},{\"skuId\":19553},{\"skuId\":14861},{\"skuId\":17980},{\"skuId\":17981},{\"skuId\":18075},{\"skuId\":18294},{\"skuId\":18798},{\"skuId\":18799},{\"skuId\":19005},{\"skuId\":10153},{\"skuId\":10388},{\"skuId\":10389},{\"skuId\":10557},{\"skuId\":10563},{\"skuId\":10572},{\"skuId\":10576},{\"skuId\":10585},{\"skuId\":10587},{\"skuId\":10588},{\"skuId\":10603},{\"skuId\":10610},{\"skuId\":10617},{\"skuId\":11074},{\"skuId\":11657},{\"skuId\":11731},{\"skuId\":11766},{\"skuId\":12563},{\"skuId\":12564},{\"skuId\":12565},{\"skuId\":12716},{\"skuId\":12778},{\"skuId\":12811},{\"skuId\":12882},{\"skuId\":13186},{\"skuId\":13187},{\"skuId\":13582},{\"skuId\":13583},{\"skuId\":13686},{\"skuId\":13822},{\"skuId\":14693},{\"skuId\":14749},{\"skuId\":14750},{\"skuId\":14862},{\"skuId\":14863},{\"skuId\":15099},{\"skuId\":15104},{\"skuId\":15328},{\"skuId\":15474},{\"skuId\":15941},{\"skuId\":16010},{\"skuId\":16012},{\"skuId\":16013},{\"skuId\":16180},{\"skuId\":16181},{\"skuId\":16182},{\"skuId\":16187},{\"skuId\":16188},{\"skuId\":16230},{\"skuId\":16231},{\"skuId\":16329},{\"skuId\":16330},{\"skuId\":16482},{\"skuId\":16483},{\"skuId\":16897},{\"skuId\":16898},{\"skuId\":16899},{\"skuId\":16900},{\"skuId\":16901},{\"skuId\":16902},{\"skuId\":16903},{\"skuId\":16904},{\"skuId\":16905},{\"skuId\":16906},{\"skuId\":17327},{\"skuId\":17330},{\"skuId\":17331},{\"skuId\":17332},{\"skuId\":17560},{\"skuId\":17561},{\"skuId\":19847},{\"skuId\":10011},{\"skuId\":10013},{\"skuId\":10050},{\"skuId\":10093},{\"skuId\":10104},{\"skuId\":10105},{\"skuId\":10217},{\"skuId\":10218},{\"skuId\":11044},{\"skuId\":11162},{\"skuId\":11186},{\"skuId\":11305},{\"skuId\":12816},{\"skuId\":13628},{\"skuId\":13732},{\"skuId\":15441}]}";
        GeneralCpCreateDTO generalCpCreateDTO = JSON.parseObject(json2, new TypeReference<GeneralCpCreateDTO>() {
        });
        generalCpCreateDTO.setOperatorId(1);
        generalCpCreateDTO.setOperatorName("测试");

        String uri = "/platform/cp/general/createCommand";
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(generalCpCreateDTO), uri);
    }

    @Test
    public void test_create_general_cp_create_exception() {
        String s = "{\"activityName\":\"总部改价---001\",\"beginPlanCpActivityTime\":1599494400000,\"endPlanCpActivityTime\":1599667200000,\"shopIds\":[99,128],\"skuCps\":[{\"skuId\":10006,\"rulePrice\":\"2\"},{\"skuId\":10012,\"rulePrice\":\"3\"}]}";
        GeneralCpCreateDTO generalCpCreateDTO = JSON.parseObject(s, new TypeReference<GeneralCpCreateDTO>() {
        });
        generalCpCreateDTO.setOperatorId(1);
        generalCpCreateDTO.setOperatorName("测试");

        String uri = "/platform/cp/general/createCommand";
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(generalCpCreateDTO), uri);
    }

    @Test
    public void test_create_general_cp_create_a_general_cp_when_shop_cp_is_going() {
        String s = "{\"activityName\":\"总部改价—薄皮青椒\",\"beginPlanCpActivityTime\":1599408000000,\"endPlanCpActivityTime\":1599580800000,\"operatorId\":1,\"operatorName\":\"sys_test1\",\"shopId\":10,\"shopIds\":[88],\"skuCps\":[{\"rulePrice\":4,\"skuId\":10020}]}";
        GeneralCpCreateDTO generalCpCreateDTO = JSON.parseObject(s, new TypeReference<GeneralCpCreateDTO>() {
        });
        generalCpCreateDTO.setOperatorId(1);
        generalCpCreateDTO.setOperatorName("测试");

        String uri = "/platform/cp/general/createCommand";
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(generalCpCreateDTO), uri);
    }

    @Test
    public void test_general_cp_info() {
        GeneralCpQueryDTO generalCpQueryDTO = new GeneralCpQueryDTO();
        generalCpQueryDTO.setPageNum(1);
        generalCpQueryDTO.setPageSize(20);
        generalCpQueryDTO.setActivityId(778L);
        String uri = "/platform/cp/general/info";
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(generalCpQueryDTO), uri);
    }

    @Test
    public void test_platform_cp_shop_page() {
        String uri = "/platform/cp/shop/page";
        ShopCpQueryDTO shopCpQueryDTO = new ShopCpQueryDTO();
        shopCpQueryDTO.setPageNum(1);
        shopCpQueryDTO.setPageSize(20);
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(shopCpQueryDTO), uri);

    }

    /**
     * 商品详情编辑
     */
    @Test
    public void test_general_cp_sku_edit() {
        GeneralCpEditSkuDTO generalCpEditSkuDTO = new GeneralCpEditSkuDTO();
        generalCpEditSkuDTO.setActivityId(94L);
        generalCpEditSkuDTO.setOperatorType(1);
        generalCpEditSkuDTO.setOperatorName("测试");
        generalCpEditSkuDTO.setOperatorId(1);
        generalCpEditSkuDTO.setSkuCp(CpItemDTO.create(19143, new BigDecimal("1.502")));

        String uri = "/platform/cp/general/sku/edit";
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(generalCpEditSkuDTO), uri);
    }

    @Test
    public void test_page_general_cp_skus() {

        GeneralCpSkuQueryDTO skuQueryDTO = new GeneralCpSkuQueryDTO();
        skuQueryDTO.setActivityId(1171L);
        skuQueryDTO.setPageNum(1);
        skuQueryDTO.setPageSize(10);
        skuQueryDTO.setClass3Name("花果");
        skuQueryDTO.setSkuId(10020);

        String uri = "/platform/cp/general/sku/page";
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(skuQueryDTO), uri);
    }

    /**
     * 1. 进行中不可编辑
     * 2. 最后一个商品
     * 3. 商品不存在
     */
    @Test
    public void test_edit_sku_of_general_cp_activity_edit() {
        GeneralCpEditSkuDTO generalCpEditSkuDTO = new GeneralCpEditSkuDTO();
        generalCpEditSkuDTO.setOperatorType(1);
        generalCpEditSkuDTO.setSkuCp(CpItemDTO.create(20056, new BigDecimal("15.0")));
        generalCpEditSkuDTO.setActivityId(140L);
        generalCpEditSkuDTO.setOperatorId(1023);
        generalCpEditSkuDTO.setOperatorName("测试");
        String uri = "/platform/cp/general/sku/edit";
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(generalCpEditSkuDTO), uri);
    }

    @Test
    public void test_edit_sku_of_general_cp_activity_del() {
        GeneralCpEditSkuDTO generalCpEditSkuDTO = new GeneralCpEditSkuDTO();
        generalCpEditSkuDTO.setOperatorType(2);
        generalCpEditSkuDTO.setSkuCp(CpItemDTO.create(19143, new BigDecimal("12.3")));
        generalCpEditSkuDTO.setActivityId(94L);
        generalCpEditSkuDTO.setOperatorId(1023);
        generalCpEditSkuDTO.setOperatorName("测试");
        String uri = "/platform/cp/general/sku/edit";
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(generalCpEditSkuDTO), uri);

    }

    @Test
    public void test_edit_sku_of_general_cp_activity_del_1() {
        String json = "{\"activityId\":1304,\"operatorType\":2,\"skuCp\":{\"rulePrice\":11,\"skuId\":10222}}";
        GeneralCpEditSkuDTO generalCpEditSkuDTO = JSON.parseObject(json, GeneralCpEditSkuDTO.class);
        generalCpEditSkuDTO.setOperatorId(1);
        generalCpEditSkuDTO.setOperatorName("test");
        String uri = "/platform/cp/general/sku/edit";
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(generalCpEditSkuDTO), uri);

    }

    @Test
    public void test_general_cp_create_cp() {
        String json = "{\"activityName\":\"总部改价\",\"beginPlanCpActivityTime\":1599231768000,\"endPlanCpActivityTime\":1599331768000,\"operatorId\":1,\"operatorName\":\"sys_test1\",\"shopId\":10,\"shopIds\":[10,99,128,147,263,332,390,419,447,466,500,514,519,528,529,531],\"skuCps\":[{\"rulePrice\":1,\"skuId\":10006},{\"rulePrice\":1,\"skuId\":10012},{\"rulePrice\":1,\"skuId\":10024},{\"rulePrice\":1,\"skuId\":10026},{\"rulePrice\":1,\"skuId\":10027},{\"rulePrice\":1,\"skuId\":10033},{\"rulePrice\":1,\"skuId\":10034},{\"rulePrice\":1,\"skuId\":10038},{\"rulePrice\":1,\"skuId\":10041},{\"rulePrice\":1,\"skuId\":10055},{\"rulePrice\":1,\"skuId\":10068},{\"rulePrice\":1,\"skuId\":10076},{\"rulePrice\":1,\"skuId\":10087},{\"rulePrice\":1,\"skuId\":10090},{\"rulePrice\":1,\"skuId\":10101},{\"rulePrice\":1,\"skuId\":10102},{\"rulePrice\":1,\"skuId\":10103},{\"rulePrice\":1,\"skuId\":10106},{\"rulePrice\":1,\"skuId\":10107},{\"rulePrice\":1,\"skuId\":10108},{\"rulePrice\":1,\"skuId\":10109},{\"rulePrice\":1,\"skuId\":10385},{\"rulePrice\":1,\"skuId\":11026},{\"rulePrice\":1,\"skuId\":11133},{\"rulePrice\":1,\"skuId\":11136},{\"rulePrice\":1,\"skuId\":11159},{\"rulePrice\":1,\"skuId\":11165},{\"rulePrice\":1,\"skuId\":12136},{\"rulePrice\":1,\"skuId\":12142},{\"rulePrice\":1,\"skuId\":12766},{\"rulePrice\":1,\"skuId\":12847},{\"rulePrice\":1,\"skuId\":13563},{\"rulePrice\":1,\"skuId\":13720},{\"rulePrice\":1,\"skuId\":13792},{\"rulePrice\":1,\"skuId\":14889},{\"rulePrice\":1,\"skuId\":14957},{\"rulePrice\":1,\"skuId\":14961},{\"rulePrice\":1,\"skuId\":14962},{\"rulePrice\":1,\"skuId\":14986},{\"rulePrice\":1,\"skuId\":15015},{\"rulePrice\":1,\"skuId\":15073},{\"rulePrice\":1,\"skuId\":15127},{\"rulePrice\":1,\"skuId\":15143},{\"rulePrice\":1,\"skuId\":15171},{\"rulePrice\":1,\"skuId\":15702},{\"rulePrice\":1,\"skuId\":16104},{\"rulePrice\":1,\"skuId\":16211},{\"rulePrice\":1,\"skuId\":16789},{\"rulePrice\":1,\"skuId\":17402},{\"rulePrice\":1,\"skuId\":17448},{\"rulePrice\":1,\"skuId\":17724},{\"rulePrice\":1,\"skuId\":17962},{\"rulePrice\":1,\"skuId\":18503},{\"rulePrice\":1,\"skuId\":18504},{\"rulePrice\":1,\"skuId\":18612},{\"rulePrice\":1,\"skuId\":18613},{\"rulePrice\":1,\"skuId\":18614},{\"rulePrice\":1,\"skuId\":18645},{\"rulePrice\":1,\"skuId\":18646},{\"rulePrice\":1,\"skuId\":18973},{\"rulePrice\":1,\"skuId\":19254},{\"rulePrice\":1,\"skuId\":19255}]}";
        GeneralCpCreateDTO cpCreateDTO = JSON.parseObject(json, new TypeReference<GeneralCpCreateDTO>() {
        });

        String uri = "/platform/cp/general/createCommand";
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(cpCreateDTO), uri);

    }

    @Test
    public void test_general_cp_shop_page() {
        ShopCpQueryDTO shopCpQueryDTO = new ShopCpQueryDTO();
        shopCpQueryDTO.setPageNum(1);
        shopCpQueryDTO.setPageSize(20);
        shopCpQueryDTO.setActivityId(1514L);
        String uri = "/platform/cp/general/shop/page";
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(shopCpQueryDTO), uri);
    }

    @Test
    public void test_general_cp_auto_create_cp() {
        GeneralCpAutoCreateDTO cpCreateDTO = new GeneralCpAutoCreateDTO();
        cpCreateDTO.setBizCode("123456");
        cpCreateDTO.setBizName("定价策略-111");
        cpCreateDTO.setOperatorName("test");
        cpCreateDTO.setOperatorId(1);
        cpCreateDTO.setPlanCpActivityStartTime(DateTime.now().plusDays(1).getMillis());
        cpCreateDTO.setPlanCpActivityEndTime(DateTime.now().plusDays(2).getMillis());
        cpCreateDTO.setCpItemList(Collections.singletonList(CpItemDTO.create(10, 10006, new BigDecimal("2.0"))));
        String uri = "/platform/cp/general/auto/create";
        Mocks.postRequestBody(mockMvc, JSON.toJSONString(cpCreateDTO), uri);
    }
}
