package com.qmfresh.promotion.platform.interfaces.platform;

import com.qmfresh.promotion.Mocks;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = {"spring.profiles.active=site"})
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class PlatformPromotionPriceControllerTest {

    @Resource
    private MockMvc mockMvc;


    @Test
    public void test_compute_promotion_price() {

        String content = "";
        String uri = "/platform/promotion/price/compute";

        Mocks.postRequestBody(mockMvc, content, uri);

    }
}
