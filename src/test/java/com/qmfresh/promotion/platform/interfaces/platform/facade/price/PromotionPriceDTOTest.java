package com.qmfresh.promotion.platform.interfaces.platform.facade.price;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPrice;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPriceRule;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPriceSpec;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ChangePriceActivityPO;
import org.joda.time.DateTime;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.assertNotNull;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public class PromotionPriceDTOTest {


    @Test
    public void test() {

        ChangePriceActivityPO area = new ChangePriceActivityPO();
        area.setType(CpBizType.CP_AREA.getCode());
        area.setRulePrice(new BigDecimal("12.1"));
        area.setPlanActivityStartTime(new Date());
        area.setPlanActivityEndTime(DateTime.now().plusDays(1).toDate());
        area.setCpType(CpType.DIFF.getCode());
        area.setStatus(3);


        ChangePriceActivityPO general = new ChangePriceActivityPO();
        general.setType(CpBizType.CP_PLATFORM.getCode());
        general.setRulePrice(new BigDecimal("123"));
        general.setPlanActivityStartTime(new Date());
        general.setPlanActivityEndTime(DateTime.now().plusDays(1).toDate());
        general.setCpType(CpType.DIFF.getCode());
        general.setStatus(3);

        ChangePriceActivityPO shop = new ChangePriceActivityPO();
        shop.setType(CpBizType.CP_SHOP.getCode());
        shop.setType(CpBizType.CP_SHOP.getCode());
        shop.setPlanActivityStartTime(new Date());
        shop.setPlanActivityEndTime(DateTime.now().plusDays(1).toDate());
        shop.setCpType(CpType.DIFF.getCode());
        shop.setStatus(3);



        PromotionPrice promotionPrice = PromotionPrice.create(new BigDecimal("12.12"), 10, 10001,
                PromotionPriceSpec.create(
                        PromotionPriceRule.create(ChangePriceActivityPO.generalCpItem(general), new BigDecimal("12.12"), new BigDecimal("-3.0")),
                        PromotionPriceRule.create(ChangePriceActivityPO.generalCpItem(area), new BigDecimal("12.12"), new BigDecimal("123")),
                        PromotionPriceRule.create(ChangePriceActivityPO.shopCpActivity(shop), new BigDecimal("12.12"), new BigDecimal("14")),
                        null
                ));
        PromotionPriceDTO promotionPriceDTO = PromotionPriceDTO.create(promotionPrice, new BigDecimal("12.12"));

        assertNotNull(promotionPriceDTO.getSpec().getGeneral());
        assertNotNull(promotionPriceDTO.getSpec().getShop());
        assertNotNull(promotionPriceDTO.getSpec().getArea());
    }
}
