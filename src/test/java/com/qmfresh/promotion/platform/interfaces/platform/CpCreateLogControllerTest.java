package com.qmfresh.promotion.platform.interfaces.platform;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.Mocks;
import com.qmfresh.promotion.platform.interfaces.platform.facade.log.CpCreateLogQueryDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */

@SpringBootTest(properties = {"spring.profiles.active=site"})
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class CpCreateLogControllerTest {

    @Resource
    private MockMvc mockMvc;

    @Test
    public void test() {
        CpCreateLogQueryDTO queryDTO = new CpCreateLogQueryDTO();
        queryDTO.setPageNum(1);
        queryDTO.setPageSize(5);
        String content = JSON.toJSONString(queryDTO);
        String uri = "/platform/cp/log/page";
        Mocks.postRequestBody(mockMvc, content, uri);
    }
}
