package com.qmfresh.promotion.platform.domain.model.activity.cp.general;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.command.GeneralCpQueryCommand;
import com.qmfresh.promotion.platform.domain.shared.Page;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = "spring.profiles.active=site")
@RunWith(SpringRunner.class)
public class GeneralCpActivityManagerImplTest {

    @Resource
    private GeneralCpActivityManager generalCpActivityManager;

    @Test
    public void test_page_general_cp_activity() {
        GeneralCpQueryCommand generalCpQueryCommand = GeneralCpQueryCommand.builder()
                .pageNum(1)
                .pageSize(20)
//                .shopId(189)
//                .cpStatusList(Arrays.asList(CpStatus.RUNNING))
                .planBeginTimeStart(new DateTime(1600573527000L).minusSeconds(1).toDate())
                .planBeginTimeEnd(new DateTime(1600573527000L).plusSeconds(1).toDate())
                .build();
        Page<GeneralCpActivity> page = generalCpActivityManager.pageGeneralCpActivity(generalCpQueryCommand);
        System.out.println(JSON.toJSONString(page));
    }

    /**
     * 创建总部改价
     */
    @Test
    public void test_create_general_cp_activity() {
//
//        GeneralCpCreateCommand generalCpCreateCommand = new GeneralCpCreateCommand(
//                Operator.createSystemOperator(),
//                "测试总部改价活动",
//                new CpTimeCycle(DateTime.now().plusDays(1).toDate(), DateTime.now().plusDays(2).toDate()),
//                Arrays.asList(10),
//                Arrays.asList(new SkuCpItem(10007, new BigDecimal("12.3"), new BigDecimal("500")))
//        );
//        generalCpActivityManager.createGeneralCpActivity(generalCpCreateCommand);

    }

    @Test
    public void test_create_general_cp_activity_and_start() {

//        GeneralCpCreateCommand generalCpCreateCommand = new GeneralCpCreateCommand(
//                Operator.createSystemOperator(),
//                "测试总部改价活动",
//                new CpTimeCycle(DateTime.now().minusHours(1).toDate(), DateTime.now().plusDays(2).toDate()),
//                Arrays.asList(24),
////                Arrays.asList(new SkuCpItem(16104, new BigDecimal("12.3"), new BigDecimal("500")),
////                        new SkuCpItem(19255, new BigDecimal("1.12"), new BigDecimal("400")
////
////                        ))
//        );
//        generalCpActivityManager.createGeneralCpActivity(generalCpCreateCommand);
//
//        generalCpActivityManager.startGeneralCpActivityByJob();
    }

    @Test
    public void test_general_cp_activity_start() {
        generalCpActivityManager.startGeneralCpActivityByJob();
    }

    @Test
    public void test_close_running_general_cp_activity() {
        generalCpActivityManager.stopGeneralCpActivityByJob();
    }

}
