package com.qmfresh.promotion.platform.domain.model.activity.cp.shop;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = {"spring.profiles.active=site"})
@RunWith(SpringRunner.class)
public class ShopCpActivityManagerImplTest {

    @Resource
    private ShopCpActivityManager shopCpActivityManager;

    @Test
    public void test_start_shop_cp() {
        shopCpActivityManager.startByJob();
    }
}
