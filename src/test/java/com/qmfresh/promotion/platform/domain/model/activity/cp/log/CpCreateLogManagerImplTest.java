package com.qmfresh.promotion.platform.domain.model.activity.cp.log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.command.CpCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.command.CpCreateLogCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.command.CpCreateLogQueryCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.command.CpCreateLogStartCommand;
import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.CpCreateLogPO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = {"spring.profiles.active=site"})
@RunWith(SpringRunner.class)
public class CpCreateLogManagerImplTest {

    @Resource
    private CpCreateLogManager cpCreateLogManager;

    /**
     * 创建总部改价
     */
    @Test
    public void test_create_cp_create_log_with_general() {
        String s = "{\"cpBizType\":\"CP_PLATFORM\",\"logType\":\"MANUAL\",\"operator\":{\"operatorId\":1,\"operatorName\":\"sys_test1\",\"representSubject\":{\"extend\":{},\"type\":\"PLATFORM\"}},\"request\":\"{\\\"activityName\\\":\\\"总部改价测试-100801\\\",\\\"cpBizType\\\":\\\"CP_PLATFORM\\\",\\\"operator\\\":{\\\"operatorId\\\":1,\\\"operatorName\\\":\\\"sys_test1\\\",\\\"representSubject\\\":{\\\"extend\\\":{},\\\"type\\\":\\\"PLATFORM\\\"}},\\\"shopIds\\\":[99,128,147,263,332,390,419,447,466,500,514,519,528,529,531,540,547,548,141,142,143,151,247,248,249,341,350,391,459,520,524,530,51,59,111,112,126,127,377,387,516,53,54,55,101,104,146,357,373,532,48,61,74,208,358,550,96,84,89,91,92,131,235,378,385,418,452,465,508,517,523,533,543,549,49,60,82,116,121,136,223,392,463,501,503,506,511,521,544,67,103,117,129,210,282,348,90,118,119,168,230,386,502,518,102,110,122,153,154,250,319,375,458,464,507,522,13,14,57,93,205,206,242,244,298,388,393],\\\"skuCpItems\\\":[{\\\"rulePrice\\\":1.01,\\\"skuId\\\":10107},{\\\"rulePrice\\\":1.2,\\\"skuId\\\":11026},{\\\"rulePrice\\\":15,\\\"skuId\\\":11136},{\\\"rulePrice\\\":12,\\\"skuId\\\":10006}],\\\"timeCycle\\\":{\\\"beginTime\\\":1602121471321,\\\"endTime\\\":1602259199000}}\"}";
        CpCreateLogCreateCommand createCommand = JSON.parseObject(s, new TypeReference<CpCreateLogCreateCommand>() {
        });
        CpCreateLog cpCreateLog = cpCreateLogManager.create(createCommand);
        assertNotEquals(null, cpCreateLog);
    }

    @Test
    public void test_create_cp_command() {
        String s = "{\"activityName\":\"总部改价测试-100801\",\"cpBizType\":\"CP_PLATFORM\",\"operator\":{\"operatorId\":1,\"operatorName\":\"sys_test1\",\"representSubject\":{\"extend\":{},\"type\":\"PLATFORM\"}},\"shopIds\":[99,128,147,263,332,390,419,447,466,500,514,519,528,529,531,540,547,548,141,142,143,151,247,248,249,341,350,391,459,520,524,530,51,59,111,112,126,127,377,387,516,53,54,55,101,104,146,357,373,532,48,61,74,208,358,550,96,84,89,91,92,131,235,378,385,418,452,465,508,517,523,533,543,549,49,60,82,116,121,136,223,392,463,501,503,506,511,521,544,67,103,117,129,210,282,348,90,118,119,168,230,386,502,518,102,110,122,153,154,250,319,375,458,464,507,522,13,14,57,93,205,206,242,244,298,388,393],\"skuCpItems\":[{\"rulePrice\":1.01,\"skuId\":10107},{\"rulePrice\":1.2,\"skuId\":11026},{\"rulePrice\":15,\"skuId\":11136},{\"rulePrice\":12,\"skuId\":10006}],\"timeCycle\":{\"beginTime\":1602121471321,\"endTime\":1602259199000}}";
        CpCreateCommand cpCreateCommand = JSON.parseObject(s, new TypeReference<CpCreateCommand>() {
        });
        assertTrue(cpCreateCommand.getActivityName() != null);
    }

    /**
     * 创建总部改价
     */
    @Test
    public void test_create_cp_create_log_with_area() {

    }

    /**
     * 分页查询改价创建日志
     */
    @Test
    public void test_page_cp_create_log() {

        CpCreateLogQueryCommand logQueryCommand = CpCreateLogQueryCommand
                .create(1, 2, new ArrayList<>());
        Page<CpCreateLog> cpCreateLogPage = cpCreateLogManager.pageCpCreateLog(logQueryCommand);
        assertTrue(cpCreateLogPage.getRecords().size() == 2);

        System.out.println(JSON.toJSONString(cpCreateLogPage));
    }

    @Test
    public void test_get_cp_create_log() {

        CpCreateLog cpCreateLog = cpCreateLogManager.get(1L);
        assertTrue(cpCreateLog != null);

    }

    @Test
    public void test_start_cp_create_log() {
        cpCreateLogManager.startCpCreateLogJob();
    }


    /**
     * 执行改价活动创建记录
     */
    @Test
    public void test_execute_cp_create_log() {
        Long id = 100002L;
        CpCreateLog cpCreateLog = cpCreateLogManager.get(id);
        cpCreateLogManager.execute(CpCreateLogPO.create(cpCreateLog), CpCreateLogStartCommand.create());

    }

    @Test
    public void test_update_cp_create_log_2048() {
        Long updateId = 100000L;
        String remark2048 ="LBVVJIWLVTVPsS8RVTOl0F5UJa0Y7MMISmrigiMSPBZMgjBQdoC3dS2KWQyE8YNpG9VnDd3QuzuBQp35j0QL0UZrntdbUdeHNKpIiDvgUyMBowHGxrw040n4yyGP56Bh8Kl9RtGYqaMfHHfQKt22hy0uLJp5m8gQiohUNiynwa9WcQMYyRxSmwaN6z8kdL5EqAshM5AOqOj7RMUO6ICRIn5jYjDnJfpTV0HfjTSN5eXkKVpjQyJ7OSP5CYZ89GuFiC85jjenmveVWneHOWgZrNxGciuaAqdJfkmjEznmQ19uO6fzsKSWEsU4h6xOjWQmmjiOLBqkmH1X4EW9oNpVcaUrTHNohZQ2yL1VsvYJchfMZNxCjl6UxkDUjVLYFvxQPAZootvYLDXUSqXo6ZMchEQn4m76YWOWAJzOoUdF2tIwzavM3AcLMWMucTsUWIDqJVa5RsalpKou6Ryj7VUFO4P8AkH4yIu7pGrPDdDzTYVSBcU4BuQdcTS4Dm4zMsRyGUl4fNv3dnJENJGO6QUI0i9motJxX4Vy9chAXOdfSB8EGNzjdT9vJN76c3o2bNwDENQ4Kog6tPcTNzGBwWKhCn3xJLilwpsHOo5YTpUSaIIHVxR5Ow7VnVgw1NvAEjmwFLy8KhhU21RYNKr5TkIrsp6AQDBHVjqvH97rxiYyrh9UM2dRtvSmD8fjLv1OxghDt6qsiPVDFz27csELwYNqIcpj6B4BhUsBxkAjcPgQU87yHPxjUPP8RRhqrTflXgtuZhKUGbOx5fQWx3zAO3roTFVfekBadTnkZMBgfbhWoaGgKWLpnfWn5ntOYAsba1k9LEOFvi29r1Mp0fhgLDajHHivfvdXRE53sSS8f8PDodo5v8lWZ7of05eR7ktUf6pdpbZeCmhlSEM5dvXsJoBcpvz5SyXxzL2ilos61zuTTReJ0nqHmQGmOqI1a1IdtZOZawx52du4yKL3CVeeGSVCId7hVLzhyk2oFAWrxzAKPAu73lqM0Ad6zipAQgJHyZ8Im8qie493xVl673WA1SLg0oL67ie360fNXlyQtsKyuKfDOqJzW5uGZQ44QmxV3OeZ7fpj2auwniJyHAFMDHc047zT6kdSwEk0cdl6sbd2y0SQvqyDpTfaBXJv1532c1TWQTeQKHNG8aTb6qoZzuJaJdU6E1f2iXArQCRgWYiS2896hUTGeba7RKEaYjqX3l432T2fRt5dbMhnmlpNNw20AMHCa1zcRVWJOBhGPEY2b3XigHFWGHrX7Wkbr9RrQNSmneysfsaXyxeIMhbR50qgUEWFB3ummDvgMvwsgCeB9i8FmrpCnQeEZt2wm3motE12giwzfWElHAFg8WPsryfTcdBCcnxTtEIlCRbZqQnknJpNXY13g5Gf4qLlySmxh59BFWj9vmnxblfe401EwcwgEUaWqJNtnJR8MTnWVhn6hUaF8mLfcsjKOG7wvAGv99pxvEbqeoQHQT0mG1zAhvgzlnDWaGSXNaKRbXho5eLNAoikm5buW4K6Rk69ioBIDCiNAf2zg3UXAPv1FXuZwQ3jDhmDcb1rMpuuo5dKPexOxaKKDeybq7RdFGBobnwW9ORBE2ofYvFw4He9SNT95BIO1cnVrwPOdXzCOPzSKRAuwZl5GjcE2TYLC80S8sHX4XjDNV0vNKzBUvy92NkWIzbxrk7c0OaFuC0oGWJiWbPvX9QnoQuWRxpFHpWjH2mevTlgAVR8uBDvYznQpPm0ov7GoE4EKGHRcIhz5QmqBPw8MBzohAidBZi0WevpZEpeypwpSWy2UP5Cxa186K4pyTr6X77uyxsS0SoC7Lps0ocGnI4FDbPWH4yo6Y9tz9jNbToek3EAHDinP3jGZY3FIyQ2O0c0zTl8RVRpy54qZX5khj43KwiV8f4KRAlJdPHrH6xOyghkIWvtdxEXrm3dvDLrqEPaxGocecgHCPebsbq2zXS8FrdxdS7JKETngwQgxVWi14TZAjjz7fNMUrbdOKMRj6Oz7Pz6XlVluOr94cIggRs4sKxzXlBdWZNwTNpyXjls";

        cpCreateLogManager.updateCpCreateLog(
            updateId,
                CpCreateLogCompletedStatus.COMPLETED,
                remark2048
        );
    }

    @Test
    public void test_update_cp_create_log_4099() {
        Long updateId = 100000L;
        String remark4099 ="sF5g6pqXm9CLVdSGGLS8BF20PLPGA7MzKsA2pdO2uL2KdGrL9FWugsTHNtQV7aSpGOjS2p9l5ennloy3hX0tQfpxH7GqHJRo7nXtFXvg6wa24Qscioxr9DcbItbm5RP7vBAUl3UeoWotBqi7yawriOX3PUvplczKFV2AQuCzqGTfOCChoqpoUF2EJHuirZqQOh6Kqf1zGcsagAhmgC1Kfv6lQfqCpzWKDujcUOhRLilHZDHOf598tFVJIB3kJvDBorz5FvOihN1P0qGRXmDbnfAicxW12fZEyHKhPFTNafzxMWgnWuL0j7gM9CaDgDVFwHjkyvVChQ0H6NoeHSNQvANEjENxXXr92RaMGCA6amVbm84e3QoUXKwvVtxt2PQauPRyctg0tEAZAVsWxSTXeB2Br29bBroAdO2jSVCcBffLNeHqGbjIl6dm0fxYionrPRLGWlIc8jv8O9XquLCSJ7dfYSZI24Az0yJL1nP6yRcAf8hQ9FXFeUhkEN4xEPa3fXBiriHDjUzGLoCmtHhaqeZmHhS1bmdQpKEn5PvcJ6PdbSUZF0QFyGvVezJtSg7U4mgsfgQ3TrebRBfdy59MDFQQYft3md8JbxCB9tG1QaTZXeEF7Rd4cBjVYEiEakbH5LcImC7FMdUV798zAIfpWBYGiSTNpWIHMvyeq7Y1aASkWD7OQbGp2uqREFTuUPSJKRZjCxfZJnKdYESH2fNakQTZJQED5aDjr5ybKlYMQYZQkLnAi03XjMoVfLpeGb9j8SUVCvBztPCDPAOCcyL1iO9JIy8aWYKFJb6P8DF2HdWFoxnlQE31riwaBAj5JBynKGVWOWbk9lHtePzU8a23XzclKA8nCPP6T4R7rwK9diGLYDIFz3nEqTEXMXmVfja1kHHL6rvuK9pKgqBwFMPOEhtbz9FXAzQiD2pLKC7fKEyC4Mn1CcYxhiqszk9t2U8cVzWZgNhK5I779g2U5SmD74svXRd9qMlUULaGwItEujpvWsDmkUN4WQ69LAbkZJDrb0AsDaymzXjKbWyZBfqNfh5hXOgJXbuUh94m5RXoYwCEX6HTbdEW9wQ9eaUj09u0BPtOW3BNtdLar8Ymd69I4czyNWYqvK4nyEduS2WD95pmxigkZiEELnslu17mqri4wtU2ug4fRaQCb1TwiyUIf46sFyK5QBKntin8isisg0ErO7zYU4zEtCOqoWaHeEgfwpwsrHKhVEMZS7u76CMVNpa6w5kn5hR3VS4IK8KTcpjjkZaUEikubuh4L8AaiRWCR0bU1KbDEtWXXGeE6KbOK7NuRYByN2904WTecqVg9CPQvUX9a4WKN89JQUwIgTYGL2AKR9PjUqYFaEYB54HNaSBUbMhjOCjPrfoOcytDcYBiLnojYzlzAzbcGDCmV5Ld49DYrY01YpjvnauNc8pkK4DgHUGl4ryvCQgndHSCYcX4GmPtRGGQfOBbwIWndbm1pYGzHh4splUqLGNRggF1EhCC1wicFbP1nbP1kvwBKb4V6uRbUkmEfY4PCxZvzArXwmEP0DyOjN4ADr848LXSPSJxzaNI1mGUho03AyokS8sjV1jtUBwARxVIz4dQe6imQDFxcireC3Zx5W9iX93C3wvfz5m3IuBALfm03NAnVS4WBAbIgYk7FOU39r4rdzZsFtrSx9Rrpw6oemPXbBA6SZCXJh1qFRqjhwFwrJVaPsw1qt9cVDyRKluJFqpNyVZdQYrSjcLsOtmkmV2Gbk7udAfT04JlUvpY8mYd1P8MZA2aC1xUUqYVllfbJXX2iUm9g9Nc5nTAer5PhwdkMDH8Xgf1q5SZjRs1UqogGpueoCnvmh2OQ8R998AeqhDOOjg6JZmWGrZhCyriZbEHC6rO7yYYhjLWfNodrfybuQypxcSx6zB1anZD1mAT45Y1nzunmgopEzxOFZ6Mk521VCNfWBqtykEMIzX2H9H4gnV1PReVZOgmHYr6sNlifLZNlmt2QXaMA4fZyvURQCBzqpRmCK0zOHGqlH1SQGJAp0WPIccS2wH70bZeyQNbYjms7qmvYI5lrRvFauJglMLiHNdGZc14tZbrAHWwKIPcJJdilPCYJnQOXZost8DSQNjnQKrWsU5qUPbAanHmkPkMKPyk5jQwDeEJhsjkbY7PHjGuUjy5BlkjQDbbW29hILoSSOxiEGU5NWHrwhq02GIXRXLyPrubBoaEh9JTsHcBOXaCBYorQeIuU3H6xdxw2yI6QVvKfvAF325OkHCDqCOydlh3MZ7p8eeYc9Y1EIogoqaTcyHLsHQh5GSdJaGflptq2RSsgmUpyROHvINTC2tQU36syJTPgfYNIjKzAzAcbMTBLA7XEWIXFYdizYhYAm8kjTC9z4P2BopJFLUW5z3dhW0ft2NdJSJM5M9KVVKax8tRcZ2CqQrGGAEeSsULPjrRijKW1nvSvjBEIij9U7R9RjKOoPFDZm8j0x5wdyc6XyPEYy1rdZfO29uAfaQk3eueshBMRkiQWL9DwJ7DwjhDMPYdIEQg3MEVYsdky06xGXgRf9itvrKdpabRPLA0U5MJO9muTKmcN1qW1Pa8Dcacz6wbKw9tBjgCdNZRN6HSlSzmohjhU9U6i5n4HUghqiqLNDMK8XsQogTqppKC5RAhBgCEKa7e6pCjmoeLqhcZarH8RkxwEjZyWv5uJ68jErUYA6gJac8fuCDuvO6zGRmEu5Z2IRI7ohtrlyPjuKab4ZgfzRc2dgQkNiFvg1Q0E6vZU0EpVfbtNUylNTPTdWmjfuh0ga1yFElMfxeiV9ctZXoRClw6tKcS2Z6aCkpnmkHHXh9l2y9jvP8ALUkXAQFXmy5qx2N03mLnoPIwXfnmcVJI16YtVxSohWLPh0kOTPlhYilEy08ShfhEpoiLbxkysViD5O1B92j7CM3mojvadke28uKBQxAO1DVkEszGSbwrTqs8cdjxCFKVJVlAxonhej5X8FPNidaZd2rkPj6jZhUD5Wch6M00QgTekb42EtEAV3bORRfvXpLoD6sRQevrAGt3K8rrOyEQhKlmc4lAwUb7f3zgmRILIn0nD10q5YuOyLCs6oreXqLQyCGgdJzQTIRvsZxNOE6isRVbqfVfb6Axze6hTesMrSUlPrV8p2eHCMudkdqpw06qlhVsDbpUWvpPzjQbY7DuSkX4yqRQIqsc3gQDVB1KNTZ7oTUYLzZJBfyDQArrhXHWYlJeLq03GNISzDHptxgHdukMnehOAGcP8o4jBYcCII8KvaB35Wx1S3lS54p7jmXnc43xFtbGC1QqritIrSDruZFr9OpeZ4QVAQOE7xSCVdfMuEFrF7BUVu3Pb2rGgyLu7pGGNGgg0hTnqGiPqWpakeacovFBkYnyKbrMROEMHZKM6XQ2om5Rqd5pQiqeOBPLxYaxK6iw1sj8oB5lAmRXTXu50cBB1rY0opy2LB6SdWnwZueCekrJx9L5vfBp4IASRDYP872N5VPupTz2Bg3gBD0OwnCiKHTu9Fv5POQGlYZayRCylJiclvTdKIULoWJHxCN3taqEvVdhRNMikqoVvzz6n6VOk9CbuFO2YkvsKQqxwU44bgUm7EgxFUC25LIk8o0swfm8n2hBACPtYVIm2CgX6wmpY5Xxp2rRpYXeXOn4pE5MGcfHw8bBtEiu2CyhkBFHEker3bxObs99L5G9l1yvV0lw7Rcu1pvS0xTpV9wtyBGgZ68R7AJLNiKDnxTj3Js283oZbI7O6HEmx5BiBnpbp4H0lziNgOAaqw2lBfoBiPiPA2LzY9Jflte6sOS3W3Gl5RSckfwdDsIRDrqeHwk3vPWx2od3KCgnCFySJtM4lcRz0bUqh1BDSij8kGSrIfqEGJp15EIZTviIHORNduU0qQDgd1uXbz9E72MMJEnbWFO1rnyNjrSG0jZUD5XpJnTuiokOvRVUWKaops3AEN2CFVzml5XuCaYr0rCgWU57b1Ya0nH5Yrw5M7SeSxilhHMtur8LwyfLcTorIZl6Yszichj1sJGd1Jml1u7rISSfcM0fzJuJ0VK2Mes3i9xUblvvgxXaIbfet5qABPUh1IIEiUSKBaGHcgodEaYutCEOaqD5g6u2G0qt7ew";
        cpCreateLogManager.updateCpCreateLog(
                updateId,
                CpCreateLogCompletedStatus.COMPLETED,
                remark4099
        );
    }

}
