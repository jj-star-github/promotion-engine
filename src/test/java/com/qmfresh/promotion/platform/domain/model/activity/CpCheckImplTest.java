package com.qmfresh.promotion.platform.domain.model.activity;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.SkuCpItem;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = "spring.profiles.active=site")
@RunWith(SpringRunner.class)
public class CpCheckImplTest {

    @Resource
    private CpCheck cpCheck;
    private DateTime dateTime = new DateTime(1601222400000L);

    @Test
    public void test_cp_check() {
        int i = cpCheck.cpActivitySkuNum(331L);
        System.out.println(i);
    }

    /**
     * c    d
     * a    b
     * false
     */
    @Test
    public void test_cp_area_check_equals_bound() {
        Shop shop = Shop.create(88);
        SkuCpItem sci = SkuCpItem.create(10006, new BigDecimal("123.10"), new BigDecimal("1000"));
        Date start = dateTime.toDate();
        Date end = dateTime.plusDays(3).minusSeconds(1).toDate();
        CpTimeCycle cpTimeCycle = CpTimeCycle.create(start, end);
        boolean result = cpCheck.cpCreateCheck(shop, sci, cpTimeCycle, CpBizType.CP_AREA);
        assertFalse(result);
    }

    /**
     * c        d
     * a   b
     * false
     */
    @Test
    public void test_cp_area_check_equals_equals() {
        Shop shop = Shop.create(88);
        SkuCpItem sci = SkuCpItem.create(10006, new BigDecimal("123.10"), new BigDecimal("1000"));
        Date start = dateTime.plusDays(1).toDate();
        Date end = dateTime.plusDays(2).toDate();
        CpTimeCycle cpTimeCycle = CpTimeCycle.create(start, end);
        boolean result = cpCheck.cpCreateCheck(shop, sci, cpTimeCycle, CpBizType.CP_AREA);
        assertFalse(result);
    }

    /**
     * c   d
     * a         b
     * false
     */
    @Test
    public void test_cp_area_check_outer() {
        Shop shop = Shop.create(88);
        SkuCpItem sci = SkuCpItem.create(10006, new BigDecimal("123.10"), new BigDecimal("1000"));
        Date start = dateTime.minusDays(1).toDate();
        Date end = dateTime.plusDays(4).toDate();
        CpTimeCycle cpTimeCycle = CpTimeCycle.create(start, end);
        boolean result = cpCheck.cpCreateCheck(shop, sci, cpTimeCycle, CpBizType.CP_AREA);
        assertFalse(result);
    }

    /**
     * c   d
     * a    b
     * false
     */
    @Test
    public void test_cp_area_check_outer_1() {
        Shop shop = Shop.create(88);
        SkuCpItem sci = SkuCpItem.create(10006, new BigDecimal("123.10"), new BigDecimal("1000"));
        Date start = dateTime.minusDays(1).toDate();
        Date end = dateTime.plusDays(2).toDate();
        CpTimeCycle cpTimeCycle = CpTimeCycle.create(start, end);
        boolean result = cpCheck.cpCreateCheck(shop, sci, cpTimeCycle, CpBizType.CP_AREA);
        assertFalse(result);
    }

    /**
     * c   d
     * a    b
     * false
     */
    @Test
    public void test_cp_area_check_outer_2() {
        Shop shop = Shop.create(88);
        SkuCpItem sci = SkuCpItem.create(10006, new BigDecimal("123.10"), new BigDecimal("1000"));
        Date start = dateTime.plusDays(1).toDate();
        Date end = dateTime.plusDays(4).toDate();
        CpTimeCycle cpTimeCycle = CpTimeCycle.create(start, end);
        boolean result = cpCheck.cpCreateCheck(shop, sci, cpTimeCycle, CpBizType.CP_AREA);
        assertFalse(result);
    }

    /**
     * c   d
     * a    b
     * true
     */
    @Test
    public void test_cp_area_check_outer_3() {
        Shop shop = Shop.create(88);
        SkuCpItem sci = SkuCpItem.create(10006, new BigDecimal("123.10"), new BigDecimal("1000"));
        Date start = dateTime.minusMinutes(1).toDate();
        Date end = dateTime.toDate();
        CpTimeCycle cpTimeCycle = CpTimeCycle.create(start, end);
        boolean result = cpCheck.cpCreateCheck(shop, sci, cpTimeCycle, CpBizType.CP_AREA);
        assertTrue(result);
    }

    /**
     * c   d
     * a    b
     * true
     */
    @Test
    public void test_cp_area_check_outer_4() {
        Shop shop = Shop.create(88);
        SkuCpItem sci = SkuCpItem.create(10006, new BigDecimal("123.10"), new BigDecimal("1000"));
        Date start = dateTime.plusDays(3).minusSeconds(1).toDate();
        Date end = dateTime.plusDays(4).toDate();
        CpTimeCycle cpTimeCycle = CpTimeCycle.create(start, end);
        boolean result = cpCheck.cpCreateCheck(shop, sci, cpTimeCycle, CpBizType.CP_AREA);
        assertTrue(result);
    }


}
