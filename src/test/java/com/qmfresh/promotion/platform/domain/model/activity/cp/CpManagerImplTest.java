package com.qmfresh.promotion.platform.domain.model.activity.cp;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.activity.cp.command.ShopQueryCommand;
import com.qmfresh.promotion.platform.domain.shared.Page;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static junit.framework.TestCase.*;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = {"spring.profiles.active=site"})
@RunWith(SpringRunner.class)
public class CpManagerImplTest {

    @Resource
    private CpManager cpManager;


    @Test
    public void test_shop_cp_with_platform_with_2() {
        ShopQueryCommand shopQueryCommand = ShopQueryCommand.create(1, 20, 1514L, CpBizType.CP_PLATFORM);
        Page<ShopCpResult> shopCpResultPage = cpManager.pageShopCp(shopQueryCommand);
        assertNotNull(shopCpResultPage);
        assertEquals(3, shopCpResultPage.getRecords().size());
        System.out.println(JSON.toJSONString(shopCpResultPage));
    }

    @Test
    public void test_shop_cp_with_platform_with_empty() {
        ShopQueryCommand shopQueryCommand = ShopQueryCommand.create(1, 20, -1L, CpBizType.CP_PLATFORM);
        Page<ShopCpResult> shopCpResultPage = cpManager.pageShopCp(shopQueryCommand);
        assertNotNull(shopCpResultPage);
        assertEquals(0, shopCpResultPage.getRecords().size());
        System.out.println(JSON.toJSONString(shopCpResultPage));
    }

    @Test
    public void test_shop_cp_with_area_with_one() {
        ShopQueryCommand shopQueryCommand = ShopQueryCommand.create(1, 20, 1578L, CpBizType.CP_AREA);
        Page<ShopCpResult> shopCpResultPage = cpManager.pageShopCp(shopQueryCommand);
        assertNotNull(shopCpResultPage);
        assertEquals(1, shopCpResultPage.getRecords().size());
        System.out.println(JSON.toJSONString(shopCpResultPage));
    }

    @Test
    public void test_shop_cp_with_area_with_empty() {
        ShopQueryCommand shopQueryCommand = ShopQueryCommand.create(1, 20, -1L, CpBizType.CP_AREA);
        Page<ShopCpResult> shopCpResultPage = cpManager.pageShopCp(shopQueryCommand);
        assertNotNull(shopCpResultPage);
        assertEquals(0, shopCpResultPage.getRecords().size());
        System.out.println(JSON.toJSONString(shopCpResultPage));
    }
}
