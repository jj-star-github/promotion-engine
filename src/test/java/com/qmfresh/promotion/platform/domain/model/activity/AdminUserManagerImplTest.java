package com.qmfresh.promotion.platform.domain.model.activity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.assertTrue;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = {"spring.profiles.active=test"})
@RunWith(SpringRunner.class)
public class AdminUserManagerImplTest {

    @Resource
    private AdminUserManager adminUserManager;

    @Test
    public void test_admin_user() {
       AdminUser adminUser = adminUserManager.adminUser(1023);
        assertTrue(adminUser.getName().equals("9902"));
    }
}
