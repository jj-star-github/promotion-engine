package com.qmfresh.promotion.platform.domain.model.activity.cp.price;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.Sku;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.AreaCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.command.CpActivitySpec;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.command.PromotionPriceCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import com.qmfresh.promotion.platform.interfaces.common.BizCode;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * 3 *
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public class PromotionPriceComputeImplTest {

    private PromotionPriceCompute promotionPriceCompute = new PromotionPriceComputeImpl();

    private Sku sku = null;
    private Shop shop = null;
    private GeneralCpItem general_ten = null;
    private GeneralCpItem general_eight = null;
    private AreaCpItem areaCpItem_nine = null;
    private AreaCpItem areaCpItem_six = null;
    private ShopCpActivity shopCpActivity_four = null;
    private ShopCpActivity shopCpActivity_seven = null;
    private ShopCpActivity shopCpActivity_eleven = null;

    {
        CpTimeCycle cpTimeCycle = CpTimeCycle.create(DateTime.now().toDate(), DateTime.now().plusDays(1).toDate());
        sku = new Sku();
        sku.setSkuId(10001);
        shop = Shop.builder().shopId(10).build();

        general_ten = GeneralCpItem.builder()
                .activityId(1L)
                .activityCpId(10L)
                .shop(shop)
                .sku(sku)
                .rulePrice(new BigDecimal("10"))
                .cpTimeCycle(cpTimeCycle)
                .cpBizType(CpBizType.CP_PLATFORM)
                .cpType(CpType.DIFF)
                .lastOperatorName("general_ten")
                .build();

        general_eight = GeneralCpItem.builder()
                .activityId(2L)
                .activityCpId(20L)
                .shop(shop)
                .sku(sku)
                .rulePrice(new BigDecimal("8"))
                .cpTimeCycle(cpTimeCycle)
                .cpBizType(CpBizType.CP_PLATFORM)
                .cpType(CpType.DIFF)
                .lastOperatorName("general_eight")
                .build();

        areaCpItem_nine = AreaCpItem.builder()
                .activityId(3L)
                .activityCpId(30L)
                .shop(shop)
                .sku(sku)
                .rulePrice(new BigDecimal("9"))
                .cpTimeCycle(cpTimeCycle)
                .cpBizType(CpBizType.CP_AREA)
                .cpType(CpType.DIFF)
                .lastOperatorName("areaCpItem_nine")
                .build();
        areaCpItem_six = AreaCpItem.builder()
                .activityId(4L)
                .activityCpId(40L)
                .shop(shop)
                .sku(sku)
                .rulePrice(new BigDecimal("6"))
                .cpTimeCycle(cpTimeCycle)
                .cpBizType(CpBizType.CP_AREA)
                .cpType(CpType.DIFF)
                .lastOperatorName("areaCpItem_six")
                .build();
        shopCpActivity_eleven = ShopCpActivity.builder()
                .activityId(5L)
                .activityCpId(50L)
                .shop(shop)
                .sku(sku)
                .rulePrice(new BigDecimal("11"))
                .planActivityStartTime(cpTimeCycle.getBeginTime())
                .planActivityEndTime(cpTimeCycle.getEndTime())
                .cpType(CpType.DIFF)
                .bizType(CpBizType.CP_SHOP)
                .lastOperatorPerson("shopCpActivity_eleven")
                .build();
        shopCpActivity_seven = ShopCpActivity.builder()
                .activityId(6L)
                .activityCpId(60L)
                .shop(shop)
                .sku(sku)
                .rulePrice(new BigDecimal("7"))
                .planActivityStartTime(cpTimeCycle.getBeginTime())
                .planActivityEndTime(cpTimeCycle.getEndTime())
                .cpType(CpType.DIFF)
                .bizType(CpBizType.CP_SHOP)
                .lastOperatorPerson("shopCpActivity_eleven")
                .build();
        shopCpActivity_four = ShopCpActivity.builder()
                .activityId(7L)
                .activityCpId(70L)
                .shop(shop)
                .sku(sku)
                .rulePrice(new BigDecimal("4"))
                .planActivityStartTime(cpTimeCycle.getBeginTime())
                .planActivityEndTime(cpTimeCycle.getEndTime())
                .cpType(CpType.DIFF)
                .bizType(CpBizType.CP_SHOP)
                .lastOperatorPerson("shopCpActivity_four")
                .build();
    }

    @Test
    public void test_compute_promotion_price_without_activity() {
        PromotionPriceCommand priceCommand = PromotionPriceCommand.createWithoutActivity(new BigDecimal("10.01"), 10, 11);
        PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
        assertTrue(promotionPrice.getLastPromotionPrice().equals(new BigDecimal("10.01")));
    }

    /**
     * 只有总部
     */
    @Test
    public void test_compute_promotion_price_when_only_general_cp() {
        PromotionPriceCommand priceCommand =
                PromotionPriceCommand.create(10, 11, new BigDecimal("15"),
                        CpActivitySpec.spec(
                                general_ten,
                                null,
                                null,
                                null
                        )
                );
        PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
        assertNotNull(promotionPrice.getSpec().getGeneralPromotion());
        assertEquals(promotionPrice.getLastPromotionPrice(), new BigDecimal("10"));

    }

    /**
     * 只有门店改价
     */
    @Test
    public void test_compute_promotion_price_when_only_shop_cp() {
        PromotionPriceCommand priceCommand =
                PromotionPriceCommand.create(10, 11, new BigDecimal("15"),
                        CpActivitySpec.spec(
                                null,
                                null,
                                shopCpActivity_eleven,
                                null
                        )
                );
        PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
        assertNotNull(promotionPrice.getSpec().getShopPromotion());
        assertTrue(promotionPrice.getSpec().getShopPromotion().getMarketingDiffFee().equals(new BigDecimal("-4")));
        assertEquals(promotionPrice.getLastPromotionPrice(), new BigDecimal("11"));

    }

    /**
     * 存在门店改价和总部改价
     */
    @Test
    public void test_compute_promotion_price_when_shop_cp_and_general_cp() {
        PromotionPriceCommand priceCommand =
                PromotionPriceCommand.create(10, 11, new BigDecimal("15"),
                        CpActivitySpec.spec(
                                general_ten,
                                null,
                                shopCpActivity_seven,
                                null
                        )
                );
        PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
        System.out.println(JSON.toJSONString(promotionPrice));
        assertNotNull(promotionPrice.getSpec().getShopPromotion());
        assertNotNull(promotionPrice.getSpec().getGeneralPromotion());
        assertEquals(promotionPrice.getLastPromotionPrice(), new BigDecimal("7"));

    }

    @Test
    public void test_compute_promotion_price_when_general_area_and_shop() {
        PromotionPriceCommand priceCommand =
                PromotionPriceCommand.create(10, 11, new BigDecimal("15"),
                        CpActivitySpec.spec(
                                general_ten,
                                areaCpItem_nine,
                                shopCpActivity_seven,
                                null
                        )
                );
        PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
        System.out.println(JSON.toJSONString(promotionPrice));
        assertNotNull(promotionPrice.getSpec().getShopPromotion());
        assertNotNull(promotionPrice.getSpec().getAreaPromotion());
        assertEquals(promotionPrice.getLastPromotionPrice(), new BigDecimal("7"));

    }

    @Test
    public void test_compute_promotion_price_when_general_area_and_shop_max() {
        PromotionPriceCommand priceCommand =
                PromotionPriceCommand.create(10, 11, new BigDecimal("15"),
                        CpActivitySpec.spec(
                                general_eight,
                                areaCpItem_nine,
                                shopCpActivity_seven,
                                null
                        )
                );
        PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
        System.out.println(JSON.toJSONString(promotionPrice));
        assertNotNull(promotionPrice.getSpec().getShopPromotion());
        assertNotNull(promotionPrice.getSpec().getGeneralPromotion());
        assertEquals(promotionPrice.getSpec().getGeneralPromotion().getMarketingDiffFee(), new BigDecimal("-7"));
        assertEquals(promotionPrice.getLastPromotionPrice(), new BigDecimal("7"));
    }


    /**
     * 原价提升
     */
    @Test
    public void test_compute_promotion_price_when_only_general_cp_with_original_price_up() {

    }

    /**
     * 原价提升
     */
    @Test
    public void test_compute_promotion_price_when_only_shop_cp_with_original_price_up() {
        PromotionPriceCommand priceCommand =
                PromotionPriceCommand.create(10, 11, new BigDecimal("8"),
                        CpActivitySpec.spec(
                                general_ten,
                                areaCpItem_nine,
                                shopCpActivity_eleven,
                                null
                        )
                );
        PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
        System.out.println(JSON.toJSONString(promotionPrice));
//        assertNotNull(promotionPrice.getSpec().getShopPromotion());
        assertNotNull(promotionPrice.getSpec().getAreaPromotion());
        assertEquals(promotionPrice.getLastPromotionPrice(), new BigDecimal("9"));
    }

    /**
     * 原价提升
     */
    @Test
    public void test_compute_promotion_price_when_shop_cp_and_general_cp_with_origin_price_up() {

    }

    /**
     * 原价下降
     */
    @Test
    public void test_compute_promotion_price_when_only_general_cp_with_original_price_down() {

        PromotionPriceCommand priceCommand =
                PromotionPriceCommand.create(10, 11, new BigDecimal("7"),
                        CpActivitySpec.spec(
                                null,
                                areaCpItem_nine,
                                null,
                                null
                        )
                );
        PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
        System.out.println(JSON.toJSONString(promotionPrice));

    }

    /**
     * 原价下降
     */
    @Test
    public void test_compute_promotion_price_when_only_shop_cp_with_original_price_down() {

    }

    /**
     * 原价下架
     */
    @Test
    public void test_compute_promotion_price_when_shop_cp_and_general_cp_with_origin_price_down() {

    }
}
