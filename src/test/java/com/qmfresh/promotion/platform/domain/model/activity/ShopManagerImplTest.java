package com.qmfresh.promotion.platform.domain.model.activity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = "spring.profiles.active=test")
@RunWith(SpringRunner.class)
public class ShopManagerImplTest {

    @Resource
    private ShopManager shopManager;

    @Test
    public void test_shop() {
        Shop shop = shopManager.shop(350);
        assertNotNull(shop.getShopName());
    }

    @Test
    public void test_shops() {
        List<Shop> shops = shopManager.shops(Arrays.asList(10,350));
        assertTrue(shops.size() ==2);
    }
}
