package com.qmfresh.promotion.platform.domain.model.activity.cp.area;

import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.command.AreaCpCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.command.AreaCpQueryCommand;
import com.qmfresh.promotion.platform.domain.shared.Page;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = "spring.profiles.active=site")
@RunWith(SpringRunner.class)
public class AreaCpActivityManagerImplTest {

    @Resource
    private AreaCpActivityManager areaCpActivityManager;


    /**
     * 大区活动
     */
    @Test
    public void test_area_cp_activity() {

//        AreaCpCreateCommand cpCreateCommand = AreaCpCreateCommand
//                .create(Operator.createSystemOperator(),
//                        "大区测试");
//        areaCpActivityManager.createCommand();

    }

    @Test
    public void test_area_cp_page_activity() {
        AreaCpQueryCommand queryCommand = AreaCpQueryCommand.builder()
                .pageNum(1)
                .pageSize(10)
                .build();
        Page<AreaCpActivity> areaCpActivities = areaCpActivityManager.pageAreaCpList(queryCommand);

    }
}
