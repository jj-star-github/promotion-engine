package com.qmfresh.promotion.platform.domain.model.activity;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootTest(properties = "spring.profiles.active=site")
@RunWith(SpringRunner.class)
public class SkuManagerImplTest {

    @Resource
    private SkuManager skuManager;

    /**
     * {"success":true,"errorCode":0,"message":"","body":[{"id":407,"ssuId":408,"cityId":0,"areaId":0,"price":1290,"vipPrice":0.00,"skuId":10007,"userId":0,"isDeleted":0,"isOnline":0,"isThirdOnline":0,"isLock":0,"lockPrice":0.00,"priceApplyId":0,"isStore":0,"shopId":5,"isCommon":0,"formatId":6643,"updater":null,"updateDepartment":null,"advisePrice":1290,"lastPrice":null,"reasonType":null,"optTime":0,"channel":2,"productSsu":null,"sku":null,"pluCode":null,"salesUnit":null,"internationalCode":null,"userName":null,"realName":null,"shopName":null,"oldOptTime":null,"ct":1527067824,"ut":1596175734}]}
     */
    @Test
    public void test_get_current_price() {
        SkuPrice price = skuManager.shopSkuPrice(5, 10007);
    }

    @Test
    public void test_get_sku() {
        Sku sku = skuManager.sku(10007);
        if (sku.getSkuId().equals(10007)) {

        }

    }

    @Test
    public void test_sku() {
        List<Sku> sku = skuManager.sku(Arrays.asList(10007, 10008, 10010));
        assertTrue(sku.size() > 2);
    }

    @Test
    public void test_sku_ext() {
        List<SkuExt> skuExts = skuManager.skuExt(24,Arrays.asList(10007, 10008, 10010));
        System.out.println(JSON.toJSONString(skuExts));
    }

    @Test
    public void test_sku_ext_2() {
        //shopId= 5 skuIds=[10014,10006,10051,10038]
        List<SkuExt> skuExts = skuManager.skuExt(24,Arrays.asList(10007, 10008, 10010));
        System.out.println(JSON.toJSONString(skuExts));

    }
}
