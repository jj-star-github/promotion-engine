package com.qmfresh.promotion;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public class Mocks {

    public static void postRequestBody(MockMvc mockMvc, String content, String uri) {
        postRequestBody(mockMvc, null, content, uri);
    }

    public static void postRequestBody(MockMvc mockMvc, Map<String, Object> header, String content, String uri) {

        try {

            MockHttpServletRequestBuilder builder = post(uri)
                    .content(content)
                    .contentType(MediaType.APPLICATION_JSON);
            if (header != null) {
                for (String key : header.keySet()) {
                    builder.header(key, header.get(key));
                }
            }
            mockMvc.perform(
                    builder
            )
                    .andDo(print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
