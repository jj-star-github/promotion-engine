= 促销平台


[[_overview]]
== Overview
提供平台所有API接口文档


=== Version information
[%hardbreaks]
__Version__ : 1.0


=== URI scheme
[%hardbreaks]
__Host__ : localhost:8160
__BasePath__ : /


=== Tags

* APP端申请营销经费相关接口 : Funds Apply Controller
* marketing-record-controller : 门店营销费用记录
* shop-rule-controller : 门店营销费用规则
* 促销价计算服务 : Platform Promotion Price Controller
* 促销活动模块 : Promotion Activity Controller
* 创建改价日志接口 : Cp Create Log Controller
* 大区活动改价接口 : Area Cp Controller
* 活动接口 : Prom Activity Controller
* 管理后台改价相关接口 : General Cp Controller
* 管理后台活动接口 : Platform Prom Activity Controller
* 管理后台营销账户 : Platform Account Controller
* 管理后台费用相关接口 : Platform Funds Controller
* 运营后台管理经费申请相关接口 : Funds Apply Check Controller
* 门店改价接口 : Shop App Cp Controller
* 门店费用接口 : Shop App Funds Controller



