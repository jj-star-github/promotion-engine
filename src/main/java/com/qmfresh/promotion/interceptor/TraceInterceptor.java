package com.qmfresh.promotion.interceptor;

import com.qmfresh.promotion.common.utils.TraceIdUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * 设置TRACE_ID
 */
@Slf4j
public class TraceInterceptor implements HandlerInterceptor {

    private static Logger logger = LoggerFactory.getLogger(TraceInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        MDC.put(TraceIdUtils.TRACE_ID, TraceIdUtils.getTraceId());

        //todo 这个有bug，假如参数过大会怎么样
//        try {
//            StringBuilder sb = new StringBuilder(1000);
//            if (handler instanceof HandlerMethod) {
//                HandlerMethod h = (HandlerMethod) handler;
//                sb.append("请求类:").append(h.getBean().getClass().getName()).append("\n");
//                sb.append("请求方法:").append(h.getMethod().getName()).append("\n");
//                sb.append("参数:").append(getParamString(request.getParameterMap())).append("\n");
//                sb.append("请求地址:").append(request.getRequestURI()).append("\n");
//                logger.info(sb.toString());
//            }
//        } catch (Exception e) {
//            log.warn("preHandle处理异常", e);
//        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        MDC.remove(TraceIdUtils.TRACE_ID);
    }

    private String getParamString(Map<String, String[]> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String[]> e : map.entrySet()) {
            sb.append(e.getKey()).append("=");
            String[] value = e.getValue();
            if (value != null && value.length == 1) {
                sb.append(value[0]).append("\t");
            } else {
                sb.append(Arrays.toString(value)).append("\t");
            }
        }
        return sb.toString();
    }
}
