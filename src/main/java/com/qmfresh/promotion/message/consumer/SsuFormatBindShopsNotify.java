//package com.qmfresh.promotion.message.consumer;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.TypeReference;
//import com.qmfresh.promotion.bean.sku.SsuFormatBindInfo;
//import com.qmfresh.promotion.message.consumer.handle.SsuFormatBindShopsNotifyHandle;
//import java.util.List;
//import javax.annotation.Resource;
//
//import org.apache.rocketmq.common.message.MessageExt;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//
///**
// * Created by wyh on 2019/8/9. SSU绑定门店、区域
// *
// * @author wyh
// */
//@Service
//public class SsuFormatBindShopsNotify implements MessageListener {
//    private static final Logger logger = LoggerFactory.getLogger(PromotionOrderNotify.class);
//
//    @Value("${mq.topic.ssuFormatBindShops}")
//    private String topic;
//
//    @Resource
//    private SsuFormatBindShopsNotifyHandle handle;
//
//    @Override
//    public void handleMessage(List<MessageExt> msgs) {
//        logger.info("PromotionOrderNotify handle ...");
//        try {
//            for (MessageExt message : msgs) {
//                String msg = new String(message.getBody(), "UTF-8");
//                logger.info("msg param:{}", msg);
//                List<SsuFormatBindInfo> list = JSON.parseObject(msg, new TypeReference<List<SsuFormatBindInfo>>() {
//                });
//                handle.handleMessage(list);
//            }
//        } catch (Exception e) {
//            logger.warn("PromotionOrderNotify error", e);
//        }
//    }
//
//    @Override
//    public String getTopic() {
//        return topic;
//    }
//}
