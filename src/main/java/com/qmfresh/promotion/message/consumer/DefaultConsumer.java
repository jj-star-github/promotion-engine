//package com.qmfresh.promotion.message.consumer;
//
//
//import com.qmfresh.promotion.message.RocketMqConfig;
//import com.qmfresh.promotion.message.SpringBeanUtil;
//import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
//import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
//import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
//import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
//import org.apache.rocketmq.client.exception.MQClientException;
//import org.apache.rocketmq.common.message.MessageExt;
//import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.DisposableBean;
//import org.springframework.beans.factory.SmartInitializingSingleton;
//
//import javax.annotation.Resource;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Objects;
//import java.util.stream.Collectors;
//
///**
// * Created by wyh on 2018/9/6.
// *
// * @author wyh
// */
//public class DefaultConsumer implements SmartInitializingSingleton, DisposableBean {
//    private static final Logger logger = LoggerFactory.getLogger(DefaultConsumer.class);
//    private Map<String, MessageListener> listenMap;
//    @Resource
//    private RocketMqConfig config;
//    private DefaultMQPushConsumer defaultConsumer;
//
//    private class DefaultMessageListenerConcurrently implements MessageListenerConcurrently {
//
//        @Override
//        public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
//            logger.info(Thread.currentThread().getName() + " Receive New Messages: " + msgs);
//            Map<String, List<MessageExt>> topicMap = msgs.stream().collect(Collectors.groupingBy(MessageExt::getTopic));
//            if (listenMap == null) {
//                listenMap = SpringBeanUtil.getApplicationContext().getBeansOfType(MessageListener.class);
//            }
//            for (Map.Entry<String, MessageListener> entry : listenMap.entrySet()) {
//                MessageListener consumer = SpringBeanUtil.getBean(entry.getKey());
//                List<MessageExt> subMsgs = topicMap.get(consumer.getTopic());
//                if (subMsgs != null && !subMsgs.isEmpty()) {
//                    try {
//                        consumer.handleMessage(subMsgs);
//                    } catch (Exception e) {
//                        logger.warn("消息处理异常:" + e.getMessage(), e);
//                        return ConsumeConcurrentlyStatus.RECONSUME_LATER;
//                    }
//                }
//            }
//            return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
//        }
//    }
//
//    @Override
//    public void destroy() throws Exception {
//        if (Objects.nonNull(defaultConsumer)) {
//            defaultConsumer.shutdown();
//            System.out.println(" consumer shutdown");
//        }
//    }
//
//    @Override
//    public void afterSingletonsInstantiated() {
//        logger.info("start consumer.....");
//        defaultConsumer = new DefaultMQPushConsumer();
//        defaultConsumer.setConsumerGroup(config.getAppId());
//        defaultConsumer.setMessageModel(MessageModel.CLUSTERING);
//        defaultConsumer.setNamesrvAddr(config.getServerAddress());
//        if (listenMap == null) {
//            listenMap = SpringBeanUtil.getApplicationContext().getBeansOfType(MessageListener.class);
//        }
//        // 设置订阅topic
//        Map<String, String> subscription = new HashMap<>(16);
//        for (Map.Entry<String, MessageListener> entry : listenMap.entrySet()) {
//            MessageListener consumer = SpringBeanUtil.getBean(entry.getKey());
//            subscription.put(consumer.getTopic(), "*");
//        }
//        defaultConsumer.setSubscription(subscription);
//        defaultConsumer.registerMessageListener(new DefaultMessageListenerConcurrently());
//        try {
//            defaultConsumer.start();
//        } catch (MQClientException e) {
//            logger.error("defaultConsumer start error", e);
//        }
//        logger.info("DefaultConsumer Started.");
//    }
//}
