package com.qmfresh.promotion.message.consumer.handle;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.order.PromotionOrderDto;
import com.qmfresh.promotion.service.IPromotionOrderService;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/6/14.
 *
 * @author wyh
 */
@Service
public class PromotionOrderHandle {
    private static final Logger logger = LoggerFactory.getLogger(PromotionOrderHandle.class);

    @Resource
    private IPromotionOrderService promotionOrderService;

    public void handleMessage(PromotionOrderDto param) {
        logger.info("创建促销订单，参数:" + JSON.toJSONString(param));
        promotionOrderService.createPromotionOrder(param);
    }
}
