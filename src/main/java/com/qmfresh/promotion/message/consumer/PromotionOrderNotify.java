//package com.qmfresh.promotion.message.consumer;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.TypeReference;
//import com.qmfresh.promotion.bean.order.PromotionOrderDto;
//import com.qmfresh.promotion.message.consumer.handle.PromotionOrderHandle;
//import javax.annotation.Resource;
//
//import org.apache.rocketmq.common.message.MessageExt;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
///**
// * Created by wyh on 2019/6/14.
// *
// * @author wyh
// */
//@Service
//public class PromotionOrderNotify implements MessageListener {
//    private static final Logger logger = LoggerFactory.getLogger(PromotionOrderNotify.class);
//
//    @Value("${mq.topic.promotionOrderNotify}")
//    private String topic;
//
//    @Resource
//    private PromotionOrderHandle handle;
//
//    @Override
//    public void handleMessage(List<MessageExt> msgs) {
//        logger.info("PromotionOrderNotify handle ...");
//        try {
//            for (MessageExt message : msgs) {
//                String msg = new String(message.getBody(), "UTF-8");
//                logger.info("msg param:{}", msg);
//                PromotionOrderDto bean = JSON.parseObject(msg, new TypeReference<PromotionOrderDto>() {
//                });
//                handle.handleMessage(bean);
//            }
//        } catch (Exception e) {
//            logger.warn("PromotionOrderNotify error", e);
//        }
//    }
//
//    @Override
//    public String getTopic() {
//        return topic;
//    }
//}
