package com.qmfresh.promotion.message.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.bean.sku.SsuFormatBindInfo;
import com.qmfresh.promotion.enums.ChannelTypeEnums;
import com.qmfresh.promotion.message.consumer.handle.SsuFormatBindShopsNotifyHandle;
import com.qmfresh.promotion.service.IPromotionProductMarkService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
@RocketMQMessageListener(consumerGroup = "${spring.application.name}SsuFormatConsumer", topic = "${mq.topic.ssuFormatBindShops}")
public class SsuFormatBindShopsConsumer extends AbstractRocketMQListener {

    @Resource
    private IPromotionProductMarkService iPromotionProductMarkService;

    @Override
    void handleMessage(String topic, String tags, String content, String msgId) {

        try {
            List<SsuFormatBindInfo> list = JSON.parseObject(content, new TypeReference<List<SsuFormatBindInfo>>() {
            });
            log.info("SSU绑定门店、区域，参数:" + JSON.toJSONString(list));

            Map<Integer, List<SsuFormatBindInfo>> ssuFormatMap = list.stream().collect(Collectors.groupingBy(SsuFormatBindInfo::getSsuFormatId));
            ssuFormatMap.entrySet().forEach(
                    item -> {
                        List<SsuFormatBindInfo> ssuFormatBindInfos = item.getValue();
                        iPromotionProductMarkService.markProduct(ChannelTypeEnums.C_ONLINE.getCode(), item.getKey(), ssuFormatBindInfos.get(0).getClass2Id(),
                                ssuFormatBindInfos.stream().map(SsuFormatBindInfo::getShopId).collect(Collectors.toList()));
                    }
            );
        } catch (Exception e) {
            log.error("消息体JSON解析异常：{} {} {}", topic, msgId, content, e);
        }

    }
}
