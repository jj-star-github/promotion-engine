package com.qmfresh.promotion.message.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.bean.order.PromotionOrderDto;
import com.qmfresh.promotion.message.consumer.handle.PromotionOrderHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
@RocketMQMessageListener(consumerGroup = "${spring.application.name}PromotionOrderConsumer", topic = "${mq.topic.promotionOrderNotify}")
public class PromotionOrderConsumer extends AbstractRocketMQListener {

    @Resource
    private PromotionOrderHandle promotionOrderHandle;

    @Override
    void handleMessage(String topic, String tags, String content, String msgId) {
        PromotionOrderDto promotionOrderDto;
        try {
            promotionOrderDto = JSON.parseObject(content, new TypeReference<PromotionOrderDto>() {
            });
        } catch (Exception e) {
            log.error("解析JSON报错：{} {} {}", topic, msgId, content, e);
            return;
        }
        promotionOrderHandle.handleMessage(promotionOrderDto);
    }
}
