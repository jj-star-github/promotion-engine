package com.qmfresh.promotion.message.consumer.handle;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.sku.SsuFormatBindInfo;
import com.qmfresh.promotion.enums.ActivityStatusTypeEnums;
import com.qmfresh.promotion.enums.ChannelTypeEnums;
import com.qmfresh.promotion.service.IPromotionProductMarkService;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/6/12.
 *
 * @author wyh
 */
@Service
public class SsuFormatBindShopsNotifyHandle {
    private static final Logger logger = LoggerFactory.getLogger(SsuFormatBindShopsNotifyHandle.class);

    @Resource
    private IPromotionProductMarkService productMarkService;

    public void handleMessage(List<SsuFormatBindInfo> list) {
        logger.info("SSU绑定门店、区域，参数:" + JSON.toJSONString(list));

        Map<Integer, List<SsuFormatBindInfo>> ssuFormatMap = list.stream().collect(Collectors.groupingBy(SsuFormatBindInfo::getSsuFormatId));
        ssuFormatMap.entrySet().forEach(
            item -> {
                List<SsuFormatBindInfo> ssuFormatBindInfos = item.getValue();
                productMarkService.markProduct(ChannelTypeEnums.C_ONLINE.getCode(), item.getKey(), ssuFormatBindInfos.get(0).getClass2Id(),
                    ssuFormatBindInfos.stream().map(SsuFormatBindInfo::getShopId).collect(Collectors.toList()));
            }
        );
    }
}
