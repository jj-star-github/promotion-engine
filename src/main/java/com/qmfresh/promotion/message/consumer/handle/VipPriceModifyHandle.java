package com.qmfresh.promotion.message.consumer.handle;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.price.ModifyVipPriceBean;
import com.qmfresh.promotion.service.IPromotionService;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/6/12.
 *
 * @author wyh
 */
@Service
public class VipPriceModifyHandle {
    private static final Logger logger = LoggerFactory.getLogger(VipPriceModifyHandle.class);

    @Resource
    private IPromotionService promotionService;

    public void handleMessage(ModifyVipPriceBean bean) {
        logger.info("修改会员价，参数:" + JSON.toJSONString(bean));
        promotionService.modifyVipPrice(bean);
    }
}
