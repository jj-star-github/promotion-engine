package com.qmfresh.promotion.message;

import lombok.Data;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by wyh on 2018/9/15.
 *
 * @author wyh
 */
@Component
@Getter
public class TopicNameConfig {

	private String vipPriceModifyNotifyTopic;

	private String ssuFormatPromotionNotifyTopic;

	private String cmallPriceModifyNotifyTopic;

	private String pushSellOutWarningDetailToShopNotifyTopic;

	private String shopChangePriceAuthorityCheckTopic;

	private String modifyVipPriceTopic;

	private String modifyChangePriceTopic;

	private String deleteShopChangePriceTopic;
	private String activityModifyTopic;

	@Value("${mq.topic.activity.modify}")
	public void setActivityModifyTopic(String activityModifyTopic) {
		this.activityModifyTopic = activityModifyTopic;
	}

	@Value("${mq.topic.vipPriceModifyNotify}")
	public void setVipPriceModifyNotifyTopic(String url) {
		vipPriceModifyNotifyTopic = url;
	}

	@Value("${mq.topic.ssuFormatPromotionNotify}")
	public void setSsuFormatPromotionNotifyTopic(String url) {
		ssuFormatPromotionNotifyTopic = url;
	}

	@Value("${mq.topic.cmallPriceModifyNotify}")
	public void setCmallPriceModifyNotifyTopic(String url) {
		cmallPriceModifyNotifyTopic = url;
	}

	@Value("${mq.topic.pushSellOutWarningDetailToShopNotify}")
	public void setPushSellOutWarningDetailToShop(String url) {
		pushSellOutWarningDetailToShopNotifyTopic = url;
	}

	@Value("${mq.topic.shopChangePriceAuthorityCheck}")
	public void setShopChangePriceAuthorityCheckTopic(String shopChangePriceAuthorityCheckTopic) {
		this.shopChangePriceAuthorityCheckTopic = shopChangePriceAuthorityCheckTopic;
	}

	@Value("${mq.topic.modifyVipChangePrice}")
	public void setModifyVipPriceTopic(String modifyVipPriceTopic) {
		this.modifyVipPriceTopic = modifyVipPriceTopic;
	}

	@Value("${mq.topic.modifyChangePrice}")
	public void setModifyChangePriceTopic(String modifyChangePriceTopic) {
		this.modifyChangePriceTopic = modifyChangePriceTopic;
	}

	@Value("${mq.topic.deleteShopChangePrice}")
	public void setDeleteShopChangePriceTopic(String deleteShopChangePriceTopic) {
		this.deleteShopChangePriceTopic = deleteShopChangePriceTopic;
	}

}
