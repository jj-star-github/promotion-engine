package com.qmfresh.promotion.message.producer;

import com.qmfresh.promotion.bean.mark.SsuformatMark;
import com.qmfresh.promotion.bean.price.ModifyPriceResult;
import com.qmfresh.promotion.bean.price.ModifyVipPriceResult;
import com.qmfresh.promotion.bean.sellout.SkuMessage;
import com.qmfresh.promotion.dto.*;
import com.qmfresh.promotion.message.TopicNameConfig;
import com.qmfresh.promotion.message.sendbody.ActivityCreatBody;
import com.qmfresh.promotion.message.sendbody.ActivityModifyBody;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by wyh on 2018/12/26.
 *
 * @author wyh
 */
@Component
public class ProducerDispatcher {

    @Resource
    private DefaultMQProducer producer;

    @Resource
    private TopicNameConfig topicNameConfig;

    public void vipPriceModifyNotify(ModifyVipPriceResult param) {
        RocketMQSender.doSend(producer, topicNameConfig.getVipPriceModifyNotifyTopic(), param);
    }

    public void ssuFormatPromotionNotify(List<SsuformatMark> list) {
        RocketMQSender.doSend(producer, topicNameConfig.getSsuFormatPromotionNotifyTopic(), list);
    }

    public void cMallPriceModifyNotify(List<ModifyPriceResult> list) {
        RocketMQSender.doSend(producer, topicNameConfig.getCmallPriceModifyNotifyTopic(), list);
    }

    public void pushSellOutWarningToShopNotify(List<SkuMessage> list) {
        RocketMQSender.doSend(producer, topicNameConfig.getPushSellOutWarningDetailToShopNotifyTopic(), list);
    }

    public void shopChangePriceAuthorityCheck(List<ShopCheckAuthorityParam> list) {
        RocketMQSender.doSend(producer, topicNameConfig.getShopChangePriceAuthorityCheckTopic(), list);
    }

    public void modifyVipChangePrice(ModifyPriceParam<SendVipPriceParam> param) {
        RocketMQSender.doSend(producer, topicNameConfig.getModifyVipPriceTopic(), param);
    }

    public void modifyChangePrice(ModifyPriceParam<SendPromotionPriceParam> param) {
        RocketMQSender.doSend(producer, topicNameConfig.getModifyChangePriceTopic(), param);
    }

    public void deleteShopChangePrice(CreateCpPromotionParam param) {
        RocketMQSender.doSend(producer, topicNameConfig.getDeleteShopChangePriceTopic(), param);
    }

    public void activityModifyTopic(List<ActivityModifyBody> list) {
        RocketMQSender.doSend(producer, topicNameConfig.getActivityModifyTopic(), list);
    }
}
