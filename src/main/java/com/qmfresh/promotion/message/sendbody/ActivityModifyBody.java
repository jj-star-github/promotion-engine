package com.qmfresh.promotion.message.sendbody;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.qmfresh.promotion.common.exception.BusinessException;
import com.qmfresh.promotion.message.producer.ProducerDispatcher;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.List;


/**
 * @author xzw
 */
@Data
@ToString
@Slf4j
public class ActivityModifyBody implements Serializable {
    private String billId;

    private Integer shopId;

    private Integer skuId;

    private Integer changeSource;
    /**
     * 修改类型,1:生效,2:失效
     */
    private Integer modifyType;

    private Integer updId;

    private String updName;
    /**
     * 业务操作时间毫秒
     */
    private Long bussinessTime;

    /**
     * 发送修改活动信息到mq
     *
     * @param producerDispatcher               消息发送者
     * @param topic                  topic
     * @param activityModifyBodyList 消息列表
     */
    public static void send(ProducerDispatcher producerDispatcher, String topic, List<ActivityModifyBody> activityModifyBodyList) {
        if (StringUtils.isBlank(topic)
                || producerDispatcher == null || CollectionUtils.isEmpty(activityModifyBodyList)) {
            throw new BusinessException("发送活动修改参数异常");
        }
        log.info("这个总共修改的改价条数是： 活动id={} nums={}", activityModifyBodyList.get(0).getBillId(), activityModifyBodyList.size());
        List<List<ActivityModifyBody>> tmps = Lists.partition(activityModifyBodyList, 99);
        for (List<ActivityModifyBody> tmp : tmps) {
            log.info("开始发送活动对应消息体：活动id= {} nums= {} 内容= {}", activityModifyBodyList.get(0).getBillId(), tmp.size(), JSON.toJSONString(tmp));
            producerDispatcher.activityModifyTopic(tmp);
        }
    }

}
