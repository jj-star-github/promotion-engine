package com.qmfresh.promotion.message.sendbody;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @author xzw
 */
@Data
@ToString
public class EffectTimeInfoDto implements Serializable {
    /**
     * 时间类型：1.时间段，2.时间戳，3.立即生效
     */
    private Integer timeType;
    /**
     * 时间段
     */
    private List<EffectTime> timeDurationValue;

}
