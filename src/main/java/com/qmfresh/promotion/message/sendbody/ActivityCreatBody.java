package com.qmfresh.promotion.message.sendbody;

import java.io.Serializable;
import java.math.BigDecimal;

public class ActivityCreatBody implements Serializable {
	/**
     * 平台商品id
     */
    private Integer platformSkuId;
    /**
     * sku_id
     */
    private Integer skuId;
    /**
     * 平台商品类型：1.sku，2.ssu，3.套装
     */
    private Integer platformSkuType;
    /**
     * 来源业务id
     */
    private String billId;
    /**
     * 优先级
     */
    private Integer priority;
    /**
     * 门店id
     */
    private Integer shopId;
    /**
     * 价格数据类型：1.数值，2.百分比
     */
    private Integer priceDataType;
    /**
     * 价格数据
     */
    private BigDecimal priceData;
    /**
     * 作用渠道：1.门店，2.线上，3.外卖
     */
    private Integer affectChannel;
    /**
     * time_type：时间类型 time_duration_value：时间段 time_start_value：时间戳
     */
    private EffectTimeInfoDto effectTimeInfo;
    /**
     * 变化源：1.促销，2.中台，3.门店，4.线上，5.外卖
     */
    private Integer changeSource;
    /**
     * 价格类型：1.原价，2.活动价，3.出清价，4.会员价
     */
    private Integer priceType;
    /**
     * 状态：0.失效，1.生效
     */
    private Integer status;
    private Integer userId;
    private String userName;
    private String reason;
    private Long bussinessTime;
    private ActivityModifyBody updateDto;
    
	public ActivityModifyBody getUpdateDto() {
		return updateDto;
	}
	public void setUpdateDto(ActivityModifyBody updateDto) {
		this.updateDto = updateDto;
	}
	public Long getBussinessTime() {
		return bussinessTime;
	}
	public void setBussinessTime(Long bussinessTime) {
		this.bussinessTime = bussinessTime;
	}
	public Integer getPlatformSkuId() {
		return platformSkuId;
	}
	public void setPlatformSkuId(Integer platformSkuId) {
		this.platformSkuId = platformSkuId;
	}
	public Integer getSkuId() {
		return skuId;
	}
	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}
	public Integer getPlatformSkuType() {
		return platformSkuType;
	}
	public void setPlatformSkuType(Integer platformSkuType) {
		this.platformSkuType = platformSkuType;
	}
	public String getBillId() {
		return billId;
	}
	public void setBillId(String billId) {
		this.billId = billId;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public Integer getShopId() {
		return shopId;
	}
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
	public Integer getPriceDataType() {
		return priceDataType;
	}
	public void setPriceDataType(Integer priceDataType) {
		this.priceDataType = priceDataType;
	}
	public BigDecimal getPriceData() {
		return priceData;
	}
	public void setPriceData(BigDecimal priceData) {
		this.priceData = priceData;
	}
	public Integer getAffectChannel() {
		return affectChannel;
	}
	public void setAffectChannel(Integer affectChannel) {
		this.affectChannel = affectChannel;
	}
	public EffectTimeInfoDto getEffectTimeInfo() {
		return effectTimeInfo;
	}
	public void setEffectTimeInfo(EffectTimeInfoDto effectTimeInfo) {
		this.effectTimeInfo = effectTimeInfo;
	}
	public Integer getChangeSource() {
		return changeSource;
	}
	public void setChangeSource(Integer changeSource) {
		this.changeSource = changeSource;
	}
	public Integer getPriceType() {
		return priceType;
	}
	public void setPriceType(Integer priceType) {
		this.priceType = priceType;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	@Override
	public String toString() {
		return "ActivityCreatBody [platformSkuId=" + platformSkuId + ", skuId=" + skuId + ", platformSkuType="
				+ platformSkuType + ", billId=" + billId + ", priority=" + priority + ", shopId=" + shopId
				+ ", priceDataType=" + priceDataType + ", priceData=" + priceData + ", affectChannel=" + affectChannel
				+ ", effectTimeInfo=" + effectTimeInfo + ", changeSource=" + changeSource + ", priceType=" + priceType
				+ ", status=" + status + ", userId=" + userId + ", userName=" + userName + ", reason=" + reason
				+ ", bussinessTime=" + bussinessTime + "]";
	}   
}
