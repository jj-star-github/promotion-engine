package com.qmfresh.promotion.message.sendbody;


import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author xzw
 */
@Data
@ToString
public class EffectTime implements Serializable {
	 /**
     * 生效起始时间
     */
    private Integer beginTime;
    /**
     * 生效截止时间
     */
    private Integer endTime;
}
