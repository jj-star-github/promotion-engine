package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.PromotionSellOutShopWarning;

public interface IPromotionSellOutShopWarningManager extends IService<PromotionSellOutShopWarning> {

}
