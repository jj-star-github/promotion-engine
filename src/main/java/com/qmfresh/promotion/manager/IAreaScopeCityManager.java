package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.AreaScopeCity;

import java.util.List;

/**
 * <p>
 * 城市区域 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface IAreaScopeCityManager extends IService<AreaScopeCity> {


    List<AreaScopeCity> areaScopeCity(Long areaScopeId);

    List<AreaScopeCity> areaScopeCity(List<Long> areaScopeId);
	
}
