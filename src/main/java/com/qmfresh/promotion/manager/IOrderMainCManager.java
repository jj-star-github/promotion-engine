package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.OrderMainC;

/**
 * <p>
 * C端订单主表 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
public interface IOrderMainCManager extends IService<OrderMainC> {
	
}
