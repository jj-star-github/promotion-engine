package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.ActivityOnlineTypeEnums;
import com.qmfresh.promotion.enums.PromotionTypeEnums;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface IPromotionRuleManager extends IService<PromotionRule> {

    /**
     * 查询线上
     *
     * @param promotionActivityIds 活动id列表
     * @param onlineType           活动规则上线/下线枚举
     * @return 活动对应的规则
     */
    List<PromotionRule> promotionRules(List<Long> promotionActivityIds, ActivityOnlineTypeEnums onlineType);

    /**
     * @param promotionActivityIds 促销活动ids
     * @param promotionTypeEnums    促销模块id
     * @return 促销规则
     */
    List<PromotionRule> promotionRules(List<Long> promotionActivityIds, PromotionTypeEnums promotionTypeEnums);

    List<PromotionRule> promotionRules(List<Long> promotionActivityIds);

    /**
     * 查询促销规则列表
     *
     * @param ids 促销规则id
     * @return 促销规则列表
     */
    List<PromotionRule> filterPromotionRules(List<Integer> ids);


}
