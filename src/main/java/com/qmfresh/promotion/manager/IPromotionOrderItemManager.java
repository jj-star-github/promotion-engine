package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.PromotionOrderItem;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-13
 */
public interface IPromotionOrderItemManager extends IService<PromotionOrderItem> {
	
}
