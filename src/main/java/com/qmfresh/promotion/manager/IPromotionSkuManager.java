package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.PromotionSku;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xbb
 * @since 2019-11-29
 */
public interface IPromotionSkuManager extends IService<PromotionSku> {
	
}
