package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.PromotionActivityTime;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface IPromotionActivityTimeManager extends IService<PromotionActivityTime> {
    /**
     * 获取门店的有效活动
     *
     * @param shopIds   门店Ids
     * @param startTime 活动开始时间
     * @param endTime   活动截止时间
     * @return
     */
    List<PromotionActivityTime> getActByShopIds(List<Integer> shopIds, Integer startTime, Integer endTime);

    /**
     * 获取当前有效的活动，结束时间>=当前时间，开始时间<=当前时间
     *
     * @param promotionModuleIds 活动类型id
     * @param startTime          开始时间
     * @param endTime            结束时间
     * @return
     */
    List<PromotionActivityTime> getCurrentActiveAct(List<Integer> promotionModuleIds, Integer startTime, Integer endTime);

    /**
     * 查询促销活动对应的活动时间
     *
     * @param promotionActivityIds 促销活动ids
     * @return 促销活动对应的时间
     */
    List<PromotionActivityTime> promotionActivityTimes(List<Long> promotionActivityIds);
}
