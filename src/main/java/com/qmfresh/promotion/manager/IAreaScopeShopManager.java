package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.AreaScopeShop;

import java.util.List;

/**
 * <p>
 * 门店区域 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface IAreaScopeShopManager extends IService<AreaScopeShop> {

    /**
     * 查询活动对应区域shop
     *
     * @param promotionActivityIds 促销活动id列表
     * @return 区域活动列表
     */
    List<AreaScopeShop> areaScopeShop(List<Long> promotionActivityIds);

    List<AreaScopeShop> areaScopeShop(List<Long> promotionActivityIds, Integer shopId);

    /**
     * 查询区域门店id
     *
     * @param areaScopeShopId 区域范围门店
     * @return 返回区域所对应的门店
     */
    List<AreaScopeShop> areaScope(Long areaScopeShopId);

    List<AreaScopeShop> areaScope(List<Long> areaScopeShopIds);
}
