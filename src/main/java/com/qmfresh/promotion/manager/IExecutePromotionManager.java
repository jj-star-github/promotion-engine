package com.qmfresh.promotion.manager;

import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.dto.PromotionProtocol;

/**
 * Created by wyh on 2019/5/24.
 *
 * @author wyh
 */
public interface IExecutePromotionManager {

    /**
     * 执行促销
     *
     * @param param
     * @return
     */
    PromotionContext execute(PromotionProtocol param);
}
