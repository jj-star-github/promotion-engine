package com.qmfresh.promotion.manager;

import com.qmfresh.promotion.bean.price.ModifyPriceBean;
import com.qmfresh.promotion.bean.price.ModifyPriceResult;
import java.util.List;

/**
 * Created by wyh on 2019/8/10.
 *
 * @author wyh
 */
public interface IPromotionPriceMananger {

    /**
     * 修改价格（原价）
     *
     * @param list
     * @return
     */
    Boolean modifyPrice(List<ModifyPriceBean> list);
}
