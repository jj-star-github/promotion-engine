package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.bean.present.PageQueryParam;
import com.qmfresh.promotion.bean.present.PresentActivityDto;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.PresentActivity;
import com.qmfresh.promotion.entity.PresentSsu;
import com.qmfresh.promotion.enums.PresentStatusEnums;

import java.util.List;

/**
 * <p>
 * 赠品活动 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface IPresentActivityManager extends IService<PresentActivity> {

    /**
     * 列表查询
     *
     * @param param
     * @return
     */
    List<PresentActivityDto> queryList(PageQueryParam param);

    /**
     * 分页查询赠品活动
     *
     * @param param
     * @return
     */
    ListWithPage<PresentActivityDto> pageQuery(PageQueryParam param);

    /**
     * 软删除
     *
     * @param id
     * @return
     */
    boolean update(Integer id);

    /**
     * 查询促销活动对应的赠品活动
     *
     * @param promotionActivityIds 促销活动ids
     * @return 促销活动列表
     */
    List<PresentActivity> presentActivities(List<Long> promotionActivityIds, List<PresentStatusEnums> statusEnums);
    List<PresentActivity> presentActivities(List<Long> promotionActivityIds);

    /**
     * @param presentActivityIds 赠品活动的id
     * @return 赠品
     */
    List<PresentSsu> presentSsu(List<Long> presentActivityIds);
}
