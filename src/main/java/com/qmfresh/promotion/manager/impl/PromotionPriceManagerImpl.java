package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.bean.price.ModifyPriceBean;
import com.qmfresh.promotion.bean.price.ModifyPriceResult;
import com.qmfresh.promotion.bean.promotion.QueryActivityParam;
import com.qmfresh.promotion.cache.service.LCActivityAreaScopeService;
import com.qmfresh.promotion.cache.service.LCAreaScopeShopService;
import com.qmfresh.promotion.cache.service.LCPromotionRuleService;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.entity.*;
import com.qmfresh.promotion.enums.ChannelTypeEnums;
import com.qmfresh.promotion.enums.PriceTypeEnums;
import com.qmfresh.promotion.enums.PromotionTypeEnums;
import com.qmfresh.promotion.manager.IActivityClass2Manager;
import com.qmfresh.promotion.manager.IActivitySsuManager;
import com.qmfresh.promotion.manager.IPromotionPriceMananger;
import com.qmfresh.promotion.message.producer.ProducerDispatcher;
import com.qmfresh.promotion.service.IPromotionActivityService;
import com.qmfresh.promotion.strategy.bean.VipRule;
import com.qmfresh.promotion.strategy.helper.DiscountPriceHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/8/10.
 *
 * @author wyh
 */
@Component
@Slf4j
public class PromotionPriceManagerImpl implements IPromotionPriceMananger {

    @Resource
    private IPromotionActivityService promotionActivityService;

    @Resource
    private IActivitySsuManager activitySsuManager;

    @Resource
    private IActivityClass2Manager activityClass2Manager;

    @Resource
    private LCPromotionRuleService lcPromotionRuleService;

    @Resource
    private LCActivityAreaScopeService lcActivityAreaScopeService;

    @Resource
    private LCAreaScopeShopService lcAreaScopeShopService;

    @Resource
    private ProducerDispatcher producerDispatcher;

    @Override
    public Boolean modifyPrice(List<ModifyPriceBean> list) {
        List<ModifyPriceResult> results = new ArrayList<>(list.size());
        Map<Integer, List<ModifyPriceBean>> ssuMap = list.stream().collect(Collectors.groupingBy(ModifyPriceBean::getSsuFormatId));
        ssuMap.entrySet().forEach(
                item -> results.addAll(this.modifySsuFormatPrice(item.getKey(), item.getValue()))
        );
        if (!ListUtil.isNullOrEmpty(results)) {
            List<List<ModifyPriceResult>> choppedList = ListUtil.chopped(results, 100);
            choppedList.forEach(item -> producerDispatcher.cMallPriceModifyNotify(item));
        }
        return true;
    }

    private List<ModifyPriceResult> modifySsuFormatPrice(Integer ssuFormatId, List<ModifyPriceBean> list) {
        return this.modifySsuFormatPrice(ChannelTypeEnums.C_ONLINE.getCode(), ssuFormatId, list.get(0).getClass2Id(), list);
    }

    private List<ModifyPriceResult> modifySsuFormatPrice(Integer channel, Integer ssuFormatId, Integer class2Id, List<ModifyPriceBean> list) {
        QueryActivityParam query = new QueryActivityParam();
        query.setChannel(channel);
        query.setIsOnline(1);
        query.setShopIds(list.stream().map(ModifyPriceBean::getShopId).collect(Collectors.toList()));
        query.setModuleId(PromotionTypeEnums.VIP.getCode());
        List<PromotionActivity> promotionActivities = promotionActivityService.queryCurrentPromotionActivity(query);
        List<Long> activityIds = promotionActivities.stream().map(PromotionActivity::getId).collect(Collectors.toList());
        // 单品促销
        List<ActivitySsu> activitySsus = activitySsuManager.activitySsu(activityIds, ssuFormatId);
        // 品类促销
        List<ActivityClass2> activityClass2s = activityClass2Manager.activityClass2(activityIds, class2Id);
        List<ModifyPriceResult> results = new ArrayList<>(16);
        if (!ListUtil.isNullOrEmpty(activitySsus)) {
            Long activityId = activitySsus.get(0).getActivityId();
            results = this.modifySsuFormatPrice(channel, lcPromotionRuleService.get(activityId), this.filterShop(activityId, list));
        }

        if (!ListUtil.isNullOrEmpty(activityClass2s)) {
            Long activityId = activityClass2s.get(0).getActivityId();
            results = this.modifySsuFormatPrice(channel, lcPromotionRuleService.get(activityId), this.filterShop(activityId, list));
        }
        return results;
    }

    private List<ModifyPriceBean> filterShop(long activityId, List<ModifyPriceBean> list) {
        ActivityAreaScope areaScope = lcActivityAreaScopeService.get(activityId);
        List<AreaScopeShop> areaScopeShops = lcAreaScopeShopService.get(areaScope.getId());
        List<Integer> shopIds = areaScopeShops.stream().map(AreaScopeShop::getShopId).collect(Collectors.toList());
        return list.stream().filter(item -> shopIds.contains(item.getShopId())).collect(Collectors.toList());
    }

    private List<ModifyPriceResult> modifySsuFormatPrice(Integer channel, PromotionRule promotionRule, List<ModifyPriceBean> modifyPriceBeans) {
        List<ModifyPriceResult> results = new ArrayList<>(16);
        // todo 目前只有会员促销价，待扩展
        VipRule vipRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<VipRule>() {
        });
        modifyPriceBeans.forEach(
                item -> {
                    ModifyPriceResult modifyPriceResult = new ModifyPriceResult();
                    modifyPriceResult.setChannel(channel);
                    modifyPriceResult.setModifyPrice(DiscountPriceHelper.getDiscountPrice(item.getPrice(), vipRule.getDiscountRule()));
                    modifyPriceResult.setPriceId(item.getPriceId());
                    modifyPriceResult.setPrice(item.getPrice());
                    modifyPriceResult.setShopId(item.getShopId());
                    modifyPriceResult.setSsuFormatId(item.getSsuFormatId());
                    // 默认价格类型会员价，后期增加其他价格类型
                    modifyPriceResult.setPriceType(PriceTypeEnums.VIP.getCode());
                    results.add(modifyPriceResult);
                }
        );
        return results;
    }

}
