package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.ActivityAreaScope;
import com.qmfresh.promotion.entity.AreaScopeShop;
import com.qmfresh.promotion.mapper.AreaScopeShopMapper;
import com.qmfresh.promotion.manager.IAreaScopeShopManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 门店区域 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@Service
@Slf4j
public class AreaScopeShopManagerImpl extends ServiceImpl<AreaScopeShopMapper, AreaScopeShop> implements IAreaScopeShopManager {

    /**
     * @param promotionActivityIds 促销活动id列表
     * @return 返回区域门店
     */
    @Override
    public List<AreaScopeShop> areaScopeShop(List<Long> promotionActivityIds) {
        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }
        List<AreaScopeShop> areaScopeShopList = this.list(new LambdaQueryWrapper<AreaScopeShop>()
                .eq(AreaScopeShop::getIsDeleted, 0)
                .in(AreaScopeShop::getActivityId, promotionActivityIds)
        );
        return areaScopeShopList;
    }

    @Override
    public List<AreaScopeShop> areaScopeShop(List<Long> promotionActivityIds, Integer shopId) {
        log.info("areaScopeShop请求参数： {} {}", JSON.toJSONString(promotionActivityIds), shopId);
        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }
        if (shopId == null) {
            return new ArrayList<>();
        }
        List<AreaScopeShop> areaScopeShopList = this.list(
                new LambdaQueryWrapper<AreaScopeShop>()
                        .eq(AreaScopeShop::getIsDeleted, 0)
                        .in(AreaScopeShop::getActivityId, promotionActivityIds)
                        .eq(AreaScopeShop::getShopId, shopId)
        );
        if (CollectionUtils.isEmpty(areaScopeShopList)) {
            return new ArrayList<>();
        }
        return areaScopeShopList;
    }

    @Override
    public List<AreaScopeShop> areaScope(Long areaScopeShopId) {
        if (areaScopeShopId == null) {
            return new ArrayList<>();
        }

        List<AreaScopeShop> areaScopeShopList = this.list(new LambdaQueryWrapper<AreaScopeShop>()
                .eq(AreaScopeShop::getIsDeleted, 0)
                .eq(AreaScopeShop::getAreaScopeId, areaScopeShopId)
        );

        if (CollectionUtils.isEmpty(areaScopeShopList)) {
            return new ArrayList<>();
        }
        return areaScopeShopList;
    }

    @Override
    public List<AreaScopeShop> areaScope(List<Long> areaScopeShopIds) {

        if (CollectionUtils.isEmpty(areaScopeShopIds)) {
            return new ArrayList<>();
        }

        List<AreaScopeShop> areaScopeShopList = this.list(new LambdaQueryWrapper<AreaScopeShop>()
                .eq(AreaScopeShop::getIsDeleted, 0)
                .in(AreaScopeShop::getAreaScopeId, areaScopeShopIds)
        );

        if (CollectionUtils.isEmpty(areaScopeShopIds)) {
            return new ArrayList<>();
        }

        return areaScopeShopList;
    }
}
