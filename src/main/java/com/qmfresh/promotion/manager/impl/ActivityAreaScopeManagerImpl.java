package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.ActivityAreaScope;
import com.qmfresh.promotion.manager.IActivityAreaScopeManager;
import com.qmfresh.promotion.mapper.ActivityAreaScopeMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 活动区域表 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@Service
@Slf4j
public class ActivityAreaScopeManagerImpl extends ServiceImpl<ActivityAreaScopeMapper, ActivityAreaScope> implements IActivityAreaScopeManager {

    @Override
    public ActivityAreaScope activityAreaScope(Long promotionActivityId) {
        if (promotionActivityId == null) {
            return null;
        }

        log.info("查询活动对应的区域param：{}", promotionActivityId);

        ActivityAreaScope activityAreaScope = this.getOne(new LambdaQueryWrapper<ActivityAreaScope>()
                .eq(ActivityAreaScope::getActivityId, promotionActivityId)
                .eq(ActivityAreaScope::getIsDeleted, 0)
        );

        return activityAreaScope;
    }

    @Override
    public List<ActivityAreaScope> activityAreaScope(List<Long> promotionActivityIds) {
        log.info("activityAreaScope参数：{}", JSON.toJSONString(promotionActivityIds));
        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }

        List<ActivityAreaScope> activityAreaScopes = this
                .list(new LambdaQueryWrapper<ActivityAreaScope>()
                        .eq(ActivityAreaScope::getIsDeleted,0)
                        .in(ActivityAreaScope::getActivityId,promotionActivityIds)
                );

        if (CollectionUtils.isEmpty(activityAreaScopes)) {
            return new ArrayList<>();
        }
        return activityAreaScopes;
    }


}
