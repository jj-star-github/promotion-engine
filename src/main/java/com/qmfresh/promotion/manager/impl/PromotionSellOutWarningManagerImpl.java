package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.bean.promotion.PromotionSellOutShopWarningBean;
import com.qmfresh.promotion.bean.promotion.PromotionSellOutSkuWarningBean;
import com.qmfresh.promotion.bean.promotion.PromotionSellOutWarningRuleBean;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.PromotionSellOutWarningReq;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.PromotionSellOutShopWarning;
import com.qmfresh.promotion.entity.PromotionSellOutSkuWarning;
import com.qmfresh.promotion.entity.PromotionSellOutWarningTime;
import com.qmfresh.promotion.enums.IsDeleteEnums;
import com.qmfresh.promotion.enums.PromotionClearingConfigStatus;
import com.qmfresh.promotion.manager.IPromotionSellOutWarningManager;
import com.qmfresh.promotion.manager.IPromotionSellOutWarningTimeManager;
import com.qmfresh.promotion.mapper.PromotionSellOutShopWarningMapper;
import com.qmfresh.promotion.mapper.PromotionSellOutSkuWarningMapper;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Slf4j
public class PromotionSellOutWarningManagerImpl extends ServiceImpl<PromotionSellOutSkuWarningMapper, PromotionSellOutSkuWarning> implements IPromotionSellOutWarningManager {
    @Resource
    private PromotionSellOutSkuWarningMapper promotionSellOutSkuWarningMapper;
    @Resource
    private PromotionSellOutShopWarningMapper promotionSellOutShopWarningMapper;
    @Resource
    private IPromotionSellOutWarningTimeManager promotionSellOutWarningTimeManager;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean add(PromotionSellOutWarningReq req) {
        if (ListUtil.isNullOrEmpty(req.getSkuWarning())) {
            throw new BusinessException("导入的数据不能为空哦");
        }
        Integer now = DateUtil.getCurrentTimeIntValue();
        //按照 门店分组
        Map<Integer, List<PromotionSellOutSkuWarningBean>> groupByShop = req.getSkuWarning().stream().collect(Collectors.groupingBy(PromotionSellOutSkuWarningBean::getShopId));
        for (Map.Entry<Integer, List<PromotionSellOutSkuWarningBean>> entry : groupByShop.entrySet()) {
            if (insertOrUpdateBatch(entry.getValue(), req.getUserId(), req.getUserName())) {
                PromotionSellOutWarningReq query = new PromotionSellOutWarningReq();
                query.setShopId(entry.getKey());
                query.setSkuIds(entry.getValue().stream().map(PromotionSellOutSkuWarningBean::getSkuId).collect(Collectors.toList()));
                List<PromotionSellOutSkuWarning> list = promotionSellOutSkuWarningMapper.queryByCondition(query);
                list.forEach(e -> {
                    e.setUT(now);
                    e.setStatus(PromotionClearingConfigStatus.INVALID.getCode());
                });
                modifyWarningTime(getModifyPromotionSellOutWarningTime(list));
                insertWarningTime(getInsertPromotionSellOutWarningTime(list));
            }
        }
        return true;
    }

    private List<PromotionSellOutWarningTime> getPromotionSellOutWarningTime(Integer id, String rule) {
        Integer now = DateUtil.getCurrentTimeIntValue();
        return parse(rule).stream().map(e -> {
            PromotionSellOutWarningTime t = new PromotionSellOutWarningTime();
            t.setWarningId(id);
            t.setWarningDate(now);
            t.setCT(now);
            t.setUT(now);
            t.setIsDeleted(IsDeleteEnums.NO.getCode());
            t.setWarningTime(e.getTime());
            return t;
        }).collect(Collectors.toList());
    }

    private Boolean insertOrUpdateBatch(List<PromotionSellOutSkuWarningBean> source, Integer userId, String userName) {
        if (ListUtil.isNullOrEmpty(source)) {
            return false;
        }
        Integer now = DateUtil.getCurrentTimeIntValue();
        List<PromotionSellOutSkuWarning> list = source.stream().map(e -> {
            PromotionSellOutSkuWarning t = new PromotionSellOutSkuWarning();
            t.setCityId(e.getCityId());
            t.setCityName(e.getCityName());
            t.setShopId(e.getShopId());
            t.setShopName(e.getShopName());
            t.setShopType(e.getShopType());
            t.setWarehouseId(e.getWarehouseId());
            t.setSkuId(e.getSkuId());
            t.setSkuName(e.getSkuName());
            t.setIsWeight(e.getIsWeight());
            t.setWarningRule(getWarningRule(e.getWarnings()));
            t.setCreatedId(userId);
            t.setCreatedName(userName);
            t.setStatus(PromotionClearingConfigStatus.EFFECTIVE.getCode());
            t.setCT(now);
            t.setUT(now);
            t.setIsDeleted(IsDeleteEnums.NO.getCode());
            return t;
        }).collect(Collectors.toList());
        return promotionSellOutSkuWarningMapper.insertOrUpdateBatch(list);
    }


    @Override
    public ListWithPage<PromotionSellOutSkuWarningBean> search(PromotionSellOutWarningReq req) {
        //分页查询
        List<PromotionSellOutSkuWarning> source = promotionSellOutSkuWarningMapper.pageQuery(req, req.getStart(), req.getEnd());
        //查询符合条件的数据总量
        int count = promotionSellOutSkuWarningMapper.count(req);
        List<PromotionSellOutSkuWarningBean> target = source.stream().map(s -> {
            PromotionSellOutSkuWarningBean t = new PromotionSellOutSkuWarningBean();
            BeanUtils.copyProperties(s, t);
            t.setWarnings(parse(s.getWarningRule()));
            return t;
        }).collect(Collectors.toList());
        ListWithPage<PromotionSellOutSkuWarningBean> res = new ListWithPage<>();
        res.setTotalCount(count);
        res.setListData(target);
        return res;
    }

    @Override
    public Boolean update(PromotionSellOutWarningReq req) {
        Integer now = DateUtil.getCurrentTimeIntValue();
        List<PromotionSellOutSkuWarning> modifyStatusList = new ArrayList<>();
        List<PromotionSellOutSkuWarning> modifyWarningRuleList = new ArrayList<>();
        for (PromotionSellOutSkuWarningBean s : req.getSkuWarning()) {
            if (s.getStatus() != null) {
                PromotionSellOutSkuWarning t = new PromotionSellOutSkuWarning();
                t.setId(s.getId());
                t.setStatus(s.getStatus());
                t.setUT(now);
                modifyStatusList.add(t);
            }
            if (!ListUtil.isNullOrEmpty(s.getWarnings())) {
                PromotionSellOutSkuWarning t = new PromotionSellOutSkuWarning();
                t.setId(s.getId());
                t.setStatus(PromotionClearingConfigStatus.EFFECTIVE.getCode());
                t.setWarningRule(getWarningRule(s.getWarnings()));
                t.setCreatedId(req.getUserId());
                t.setCreatedName(req.getUserName());
                t.setUT(now);
                modifyWarningRuleList.add(t);
            }
        }
        if (!ListUtil.isNullOrEmpty(modifyStatusList)) {
            modifyStatus(modifyStatusList);
        }
        if (!ListUtil.isNullOrEmpty(modifyWarningRuleList)) {
            modifyWarningRule(modifyWarningRuleList);
        }
        return true;
    }

    /**
     * 修改 规则时间详情
     */
    private Boolean modifyWarningTime(List<PromotionSellOutWarningTime> times) {
        return ListUtil.isNullOrEmpty(times) ? false : promotionSellOutWarningTimeManager.updateByCondition(times);
    }

    /**
     * 插入 规则时间详情
     */
    private Boolean insertWarningTime(List<PromotionSellOutWarningTime> times) {
        return ListUtil.isNullOrEmpty(times) ? false : promotionSellOutWarningTimeManager.saveBatch(times, 500);
    }

    /**
     * 修改 状态
     */
    private Boolean modifyStatus(List<PromotionSellOutSkuWarning> modifyStatusList) {
        return updateBatchById(modifyStatusList) ? modifyWarningTime(getModifyPromotionSellOutWarningTime(modifyStatusList)) : false;
    }

    /**
     * 修改预警规则
     */
    private Boolean modifyWarningRule(List<PromotionSellOutSkuWarning> modifyWarningRuleList) {
        if (!updateBatchById(modifyWarningRuleList)) {
            return false;
        }
        return modifyWarningTime(getModifyPromotionSellOutWarningTime(modifyWarningRuleList)) ? insertWarningTime(getInsertPromotionSellOutWarningTime(modifyWarningRuleList)) : false;
    }

    /**
     * 获取修改的时间对象
     */
    private List<PromotionSellOutWarningTime> getModifyPromotionSellOutWarningTime(List<PromotionSellOutSkuWarning> list) {
        return list.stream().map(e -> {
            PromotionSellOutWarningTime time = new PromotionSellOutWarningTime();
            time.setWarningId(e.getId());
            time.setUT(e.getUT());
            time.setIsDeleted(e.getStatus());
            return time;
        }).collect(Collectors.toList());
    }

    /**
     * 获取插入的时间对象
     */
    private List<PromotionSellOutWarningTime> getInsertPromotionSellOutWarningTime(List<PromotionSellOutSkuWarning> list) {
        List<PromotionSellOutWarningTime> times = new ArrayList<>();
        list.forEach(e -> {
            times.addAll(getPromotionSellOutWarningTime(e.getId(), e.getWarningRule()));
        });
        return times;
    }

    @Override
    public Boolean setShopWarning(PromotionSellOutWarningReq req) {
        List<PromotionSellOutShopWarning> list = new ArrayList<>();
        Integer now = DateUtil.getCurrentTimeIntValue();
        for (PromotionSellOutShopWarningBean s : req.getShopWarningSetting()) {
            PromotionSellOutShopWarning t = new PromotionSellOutShopWarning();
            t.setCityId(s.getCityId());
            t.setCityName(s.getCityName());
            t.setShopId(s.getShopId());
            t.setShopName(s.getShopName());
            t.setWarningType(s.getWarningType());
            t.setWarningNum(s.getWarningNum());
            t.setWarningRate(s.getWarningRate());
            t.setCreatedId(req.getUserId());
            t.setCreatedName(req.getUserName());
            t.setCT(now);
            t.setUT(now);
            t.setIsDeleted(0);
            list.add(t);
        }
        return promotionSellOutShopWarningMapper.add(list);
    }

    @Override
    public List<PromotionSellOutSkuWarningBean> matchByTime(String time, Integer shopId) {
        List<PromotionSellOutSkuWarning> source = promotionSellOutSkuWarningMapper.match(time, shopId);
        List<PromotionSellOutSkuWarningBean> target = new ArrayList<>();
        for (PromotionSellOutSkuWarning s : source) {
            PromotionSellOutSkuWarningBean t = new PromotionSellOutSkuWarningBean();
            BeanUtils.copyProperties(s, t);
            t.setWarnings(parse(s.getWarningRule()));
            target.add(t);
        }
        return target;
    }

    @Override
    public List<PromotionSellOutSkuWarningBean> matchById(List<Integer> ids) {
        List<PromotionSellOutSkuWarning> source = promotionSellOutSkuWarningMapper.selectList(new LambdaQueryWrapper<PromotionSellOutSkuWarning>().in(PromotionSellOutSkuWarning::getId, ids));
        List<PromotionSellOutSkuWarningBean> target = new ArrayList<>();
        for (PromotionSellOutSkuWarning s : source) {
            PromotionSellOutSkuWarningBean t = new PromotionSellOutSkuWarningBean();
            BeanUtils.copyProperties(s, t);
            t.setWarnings(parse(s.getWarningRule()));
            target.add(t);
        }
        return target;
    }

    /**
     * 获取门店出清预警对应的时间配置
     */

    private String getWarningRule(List<PromotionSellOutWarningRuleBean> list) {
        if (ListUtil.isNullOrEmpty(list)) {
            return "";
        }
        return JSON.toJSONString(list);
    }

    ;

    private List<PromotionSellOutWarningRuleBean> parse(String json) {
        return StringUtils.isNotBlank(json) ? JSON.parseArray(json, PromotionSellOutWarningRuleBean.class) : new ArrayList<>();
    }


}
