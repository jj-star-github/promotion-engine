package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qmfresh.promotion.bean.mark.*;
import com.qmfresh.promotion.bean.price.CMallPriceInfo;
import com.qmfresh.promotion.bean.price.CMallPriceQuery;
import com.qmfresh.promotion.bean.price.ModifyPriceResult;
import com.qmfresh.promotion.bean.promotion.QueryActivityParam;
import com.qmfresh.promotion.cache.service.*;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.entity.*;
import com.qmfresh.promotion.enums.*;
import com.qmfresh.promotion.external.service.ICmallExternalApiService;
import com.qmfresh.promotion.manager.IPromotionProductMarkManager;
import com.qmfresh.promotion.mapper.ActivityClass2Mapper;
import com.qmfresh.promotion.mapper.ActivitySsuMapper;
import com.qmfresh.promotion.message.producer.ProducerDispatcher;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.service.IPromotionActivityService;
import com.qmfresh.promotion.strategy.bean.BaseRule;
import com.qmfresh.promotion.strategy.bean.MjRule;
import com.qmfresh.promotion.strategy.bean.MzRule;
import com.qmfresh.promotion.strategy.bean.VipRule;
import com.qmfresh.promotion.strategy.helper.DiscountPriceHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/7/29.
 *
 * @author wyh
 */
@Component
@Slf4j
public class PromotionProductMarkManagerImpl implements IPromotionProductMarkManager {

    @Resource
    private LCPromotionActivityService lcPromotionActivityService;

    @Resource
    private LCPromotionActivityTimeService lcPromotionActivityTimeService;

    @Resource
    private LCPromotionRuleService lcPromotionRuleService;

    @Resource
    private LCActivityClass2Service lcActivityClass2Service;

    @Resource
    private LCActivitySsuService lcActivitySsuService;

    @Resource
    private LCActivityAreaScopeService lcActivityAreaScopeService;

    @Resource
    private LCAreaScopeShopService lcAreaScopeShopService;

    @Resource
    private ICmallExternalApiService cmallExternalApiService;

    @Resource
    private ProducerDispatcher producerDispatcher;

    @Resource
    private ActivitySsuMapper activitySsuMapper;

    @Resource
    private ActivityClass2Mapper activityClass2Mapper;

    @Resource
    private IPromotionActivityService promotionActivityService;

    @Override
    public Boolean markProduct(Long activityId, ActivityStatusTypeEnums status) {
        PromotionActivity promotionActivity = lcPromotionActivityService.get(activityId);
        // C端线上进行打标
        if (promotionActivity.getChannel().equals(ChannelTypeEnums.C_OFFLINE.getCode())) {
            return true;
        }
        ActivityAreaScope activityAreaScope = lcActivityAreaScopeService.get(activityId);
        List<AreaScopeShop> areaScopeShops = lcAreaScopeShopService.get(activityAreaScope.getId());
        List<Integer> shopIds = areaScopeShops.stream().map(AreaScopeShop::getShopId).collect(Collectors.toList());

        List<ActivityClass2> activityClass2List = lcActivityClass2Service.get(activityId);
        List<ActivitySsu> activitySsuList = lcActivitySsuService.get(activityId);
        List<ActivitySsu> containSsuList = null;
        List<ActivitySsu> exSsuList = null;
        List<Integer> class2Ids = null;
        if (!ListUtil.isNullOrEmpty(activitySsuList)) {
            Map<Integer, List<ActivitySsu>> ssuTagMap =
                    activitySsuList.stream().collect(Collectors.groupingBy(ActivitySsu::getExTag));
            containSsuList = ssuTagMap.get(SsuTagTypeEnums.CONTAIN.getCode());
            exSsuList = ssuTagMap.get(SsuTagTypeEnums.EXCLUSIVE.getCode());
        }
        if (!ListUtil.isNullOrEmpty(activityClass2List)) {
            class2Ids = activityClass2List.stream().map(ActivityClass2::getClassId).collect(Collectors.toList());
        }

        PromotionRule promotionRule = lcPromotionRuleService.get(activityId);

        // 获取待打标商品
        List<Integer> ssuFormatIds = this.getSsuFormatIds(promotionRule, shopIds, class2Ids, containSsuList, exSsuList);
        this.markProductAndSend(promotionRule, shopIds, ssuFormatIds, status);

        return true;
    }

    @Override
    public Boolean markProduct(Integer channel, Integer ssuFormatId, Integer class2Id, List<Integer> shopIds) {
        QueryActivityParam queryActivityParam = new QueryActivityParam();
        queryActivityParam.setChannel(channel);
        queryActivityParam.setShopIds(shopIds);
        queryActivityParam.setStatusList(Arrays.asList(ActivityStatusTypeEnums.STARTED.getCode(), ActivityStatusTypeEnums.STOP.getCode(),
                ActivityStatusTypeEnums.NOT_START.getCode()));
        List<PromotionActivity> promotionActivities = promotionActivityService.queryByCondition(queryActivityParam);
        if (ListUtil.isNullOrEmpty(promotionActivities)) {
            return true;
        }

        Map<String, PromotionActivity> paMap = ListUtil.listToMap(promotionActivities);
        List<Long> activityIds = promotionActivities.stream().map(PromotionActivity::getId).collect(Collectors.toList());

        List<ActivitySsu> activitySsus = activitySsuMapper.selectList(new LambdaQueryWrapper<ActivitySsu>()
                .in(ActivitySsu::getActivityId, activityIds)
                .eq(ActivitySsu::getSsuFormatId, ssuFormatId)
                .eq(ActivitySsu::getChannel, channel)
                .eq(ActivitySsu::getExTag, 0)
                .eq(ActivitySsu::getIsDeleted, 0));

        List<ActivityClass2> activityClass2s = activityClass2Mapper.selectList(new LambdaQueryWrapper<ActivityClass2>()
                .in(ActivityClass2::getActivityId, activityIds)
                .eq(ActivityClass2::getClassId, class2Id)
                .eq(ActivityClass2::getIsDeleted, 0)
        );

        if (!ListUtil.isNullOrEmpty(activitySsus)) {
            Map<Long, List<ActivitySsu>> activitySsuMap = activitySsus.stream().collect(Collectors.groupingBy(ActivitySsu::getActivityId));
            for (Map.Entry<Long, List<ActivitySsu>> entry : activitySsuMap.entrySet()) {
                List<ActivitySsu> activitySsuList = entry.getValue();
                PromotionActivity promotionActivity = paMap.get(entry.getKey().toString());
                ActivityStatusTypeEnums activityStatusType = ActivityStatusTypeEnums.getEnum(promotionActivity.getStatus());
                this.markProductAndSend(entry.getKey(), activitySsuList.stream().map(ActivitySsu::getSsuFormatId).collect(Collectors.toList()), activityStatusType);
            }
        }

        if (!ListUtil.isNullOrEmpty(activityClass2s)) {
            activityClass2s.stream().map(ActivityClass2::getActivityId).distinct().forEach(
                    item -> {
                        PromotionActivity promotionActivity = paMap.get(item.toString());
                        ActivityStatusTypeEnums activityStatusType = ActivityStatusTypeEnums.getEnum(promotionActivity.getStatus());
                        this.markProductAndSend(item, Collections.singletonList(ssuFormatId), activityStatusType);
                    }
            );
        }
        return true;
    }

    /**
     * 商品打标 by 活动id + ssuFormatIds
     *
     * @param activityId   促销活动id
     * @param ssuFormatIds 商品
     * @param status       活动状态
     */
    private void markProductAndSend(long activityId, List<Integer> ssuFormatIds, ActivityStatusTypeEnums status) {
        ActivityAreaScope areaScope = lcActivityAreaScopeService.get(activityId);
        List<AreaScopeShop> areaScopeShops = lcAreaScopeShopService.get(areaScope.getId());
        List<Integer> shopIds = areaScopeShops.stream().map(AreaScopeShop::getShopId).collect(Collectors.toList());
        this.markProductAndSend(lcPromotionRuleService.get(activityId), shopIds, ssuFormatIds, status);
    }

    private void markProductAndSend(PromotionRule promotionRule, List<Integer> shopIds, List<Integer> ssuFormatIds, ActivityStatusTypeEnums status) {
        List<PromotionActivityTime> patList = lcPromotionActivityTimeService.get(promotionRule.getActivityId());
        PromotionActivityTime promotionActivityTime = patList.get(0);
        PromotionTypeEnums promotionType = PromotionTypeEnums.getEnum(promotionActivityTime.getPromotionModuleId());
        if (promotionType == null) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("促销类型异常");
        }

        List<SsuformatMark> marks = new ArrayList<>(shopIds.size() * ssuFormatIds.size());
        int now = DateUtil.getCurrentTimeIntValue();
        SsuformatMark ssuformatMark = new SsuformatMark();
        ssuformatMark.setUpdateTime(now);
        switch (promotionType) {
            case VIP:
                marks.addAll(this.markVip(shopIds, ssuFormatIds, promotionRule, status));
                break;
            case MZ:
                MzRule mzRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<MzRule>() {
                });
                List<DescriptionMark> mzDescriptions = new ArrayList<>(mzRule.getMzRuleInfoList().size());
                mzRule.getMzRuleInfoList().forEach(
                        item -> {
                            DescriptionMark descriptionMark = new DescriptionMark();
                            descriptionMark.setDescription(item.getDescription());
                            descriptionMark.setSeq(item.getLevel());
                            mzDescriptions.add(descriptionMark);
                        });
                MzMark mzMark = new MzMark();
                mzMark.setActivityId(promotionRule.getActivityId());
                mzMark.setDescriptions(mzDescriptions);
                mzMark.setStatus(status.getCode());
                mzMark.setStartTime(promotionActivityTime.getEffectBeginTime());
                mzMark.setEndTime(promotionActivityTime.getEffectEndTime());
                ssuformatMark.setMzMark(mzMark);
                marks.addAll(this.buildSsuformatMark(ssuformatMark, shopIds, ssuFormatIds));
                break;
            case MJ:
                MjRule mjRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<MjRule>() {
                });
                List<DescriptionMark> mjDescriptions = new ArrayList<>(mjRule.getMjRuleInfoList().size());
                mjRule.getMjRuleInfoList().forEach(
                        item -> {
                            DescriptionMark descriptionMark = new DescriptionMark();
                            descriptionMark.setDescription(item.getDescription());
                            descriptionMark.setSeq(item.getLevel());
                            mjDescriptions.add(descriptionMark);
                        });
                MjMark mjMark = new MjMark();
                mjMark.setActivityId(promotionRule.getActivityId());
                mjMark.setDescriptions(mjDescriptions);
                mjMark.setStatus(status.getCode());
                mjMark.setStartTime(promotionActivityTime.getEffectBeginTime());
                mjMark.setEndTime(promotionActivityTime.getEffectEndTime());
                ssuformatMark.setMjMark(mjMark);
                marks.addAll(this.buildSsuformatMark(ssuformatMark, shopIds, ssuFormatIds));
                break;
            default:
        }
        this.send(marks);

        List<SsuformatMark> vipMarks = marks.stream().filter(item -> item.getVipMark() != null).collect(Collectors.toList());
        if (!ListUtil.isNullOrEmpty(vipMarks)) {
            this.createCmallPrice(promotionRule, shopIds, vipMarks.stream().map(SsuformatMark::getSsuFormatId).collect(Collectors.toList()), status);
        }
    }

    private void send(List<SsuformatMark> marks) {
        List<List<SsuformatMark>> choppedList = ListUtil.chopped(marks, 100);
        if (ListUtil.isNullOrEmpty(choppedList)) {
            return;
        }
        // 发送商品打标消息
        choppedList.forEach(item -> producerDispatcher.ssuFormatPromotionNotify(item));
    }

    private void createCmallPrice(PromotionRule promotionRule, List<Integer> shopIds, List<Integer> ssuFormatIds, ActivityStatusTypeEnums status) {
        // 查看C端商城ssu原价
        CMallPriceQuery cMallPriceQuery = new CMallPriceQuery();
        cMallPriceQuery.setSsuFormatIds(ssuFormatIds);
        cMallPriceQuery.setStatus(2);
        cMallPriceQuery.setShopIds(shopIds);
        List<CMallPriceInfo> cMallPriceInfos = cmallExternalApiService.queryCmallPriceList(cMallPriceQuery);

        VipRule vipRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<VipRule>() {
        });

        List<ModifyPriceResult> modifyPriceResults = new ArrayList<>();
        for (CMallPriceInfo priceInfo : cMallPriceInfos) {
            if (priceInfo.getPrice().compareTo(BigDecimal.ZERO) == 0) {
                continue;
            }
            BigDecimal modifyPrice = DiscountPriceHelper.getDiscountPrice(priceInfo.getPrice(), vipRule.getDiscountRule());
            if (modifyPrice.compareTo(BigDecimal.ZERO) <= 0) {
                log.error("改价金额小于0，不发送改价消息,price:" + priceInfo.getPrice() + ",vipRule:" + JSON.toJSONString(vipRule));
                continue;
            }

            ModifyPriceResult modifyPriceResult = new ModifyPriceResult();
            modifyPriceResult.setPrice(priceInfo.getPrice());
            modifyPriceResult.setModifyPrice(DiscountPriceHelper.getDiscountPrice(priceInfo.getPrice(), vipRule.getDiscountRule()));
            modifyPriceResult.setSsuFormatId(priceInfo.getSsuFormatId());
            modifyPriceResult.setShopId(priceInfo.getShopId());
            modifyPriceResult.setChannel(ChannelTypeEnums.C_ONLINE.getCode());
            modifyPriceResult.setPriceType(PriceTypeEnums.VIP.getCode());
            if (status.equals(ActivityStatusTypeEnums.STARTED)) {
                modifyPriceResult.setStatus(1);
            } else {
                modifyPriceResult.setStatus(0);
            }
            modifyPriceResults.add(modifyPriceResult);
        }
        List<List<ModifyPriceResult>> choppedList = ListUtil.chopped(modifyPriceResults, 100);
        choppedList.forEach(
                list -> producerDispatcher.cMallPriceModifyNotify(list)
        );
    }

    private List<SsuformatMark> buildSsuformatMark(SsuformatMark ssuformatMark, List<Integer> shopIds, List<Integer> ssuFormatIds) {
        List<SsuformatMark> ssuformatMarks = new ArrayList<>(shopIds.size() * ssuFormatIds.size());
        shopIds.forEach(item -> ssuformatMarks.addAll(this.buildSsuformatMark(ssuformatMark, item, ssuFormatIds)));
        return ssuformatMarks;
    }

    private List<SsuformatMark> buildSsuformatMark(SsuformatMark ssuformatMark, Integer shopId, List<Integer> ssuFormatIds) {
        List<SsuformatMark> ssuformatMarks = new ArrayList<>(ssuFormatIds.size());
        ssuFormatIds.forEach(
                item -> {
                    SsuformatMark mark = new SsuformatMark();
                    BeanUtils.copyProperties(ssuformatMark, mark);
                    mark.setShopId(shopId);
                    mark.setSsuFormatId(item);
                    ssuformatMarks.add(mark);
                });
        return ssuformatMarks;
    }

    private List<SsuformatMark> markVip(List<Integer> shopIds, List<Integer> ssuFormatIds, PromotionRule promotionRule, ActivityStatusTypeEnums status) {
        int now = DateUtil.getCurrentTimeIntValue();
        List<SsuformatMark> marks = new ArrayList<>(shopIds.size() * ssuFormatIds.size());
        if (status.equals(ActivityStatusTypeEnums.STARTED)) {
            for (Integer ssuFormatId : ssuFormatIds) {
                for (Integer shopId : shopIds) {
                    marks.add(this.markVip(now, shopId, ssuFormatId, promotionRule, status));
                }
            }
            return marks;
        }

        for (Integer ssuFormatId : ssuFormatIds) {
            for (Integer shopId : shopIds) {
                marks.add(this.markVip(now, shopId, ssuFormatId, promotionRule, status));
            }
        }
        return marks;
    }

    private SsuformatMark markVip(int time, int shopId, int ssuFormatId, PromotionRule promotionRule, ActivityStatusTypeEnums status) {
        BaseRule baseRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<BaseRule>() {
        });

        SsuformatMark ssuformatMark = new SsuformatMark();
        ssuformatMark.setSsuFormatId(ssuFormatId);
        ssuformatMark.setShopId(shopId);
        ssuformatMark.setUpdateTime(time);
        VipMark vipMark = new VipMark();
        vipMark.setActivityId(promotionRule.getActivityId());
        //        if (cMallPriceInfo != null) {
        //            BigDecimal price = cMallPriceInfo.getPrice().divide(BigDecimal.valueOf(1000), 3, RoundingMode.HALF_UP);
        //            vipMark.setVipPrice(DiscountPriceHelper.getDiscountPrice(price, baseRule.getDiscountRule()));
        //        }
        vipMark.setStatus(status.getCode());
        vipMark.setDescription(promotionRule.getDescript());
        if (baseRule.getLimitRule().getLimitType().equals(LimitTypeEnums.SINGLE_DAY_PERSON.getCode())) {
            vipMark.setLimitNum(baseRule.getLimitRule().getSingleDayPersonLimit());
        }
        if (baseRule.getLimitRule().getLimitType().equals(LimitTypeEnums.SINGLE_DAY_SHOP_PERSON.getCode())) {
            vipMark.setLimitNum(baseRule.getLimitRule().getSingleShopDayPersonLimit());
        }
        ssuformatMark.setVipMark(vipMark);
        return ssuformatMark;
    }

    /**
     * 获取ssuFormat
     *
     * @param promotionRule  促销规则
     * @param class2Ids      二级分类
     * @param containSsuList 包含商品列表
     * @param exSsuList      排除商品列表
     * @return
     */
    private List<Integer> getSsuFormatIds(PromotionRule promotionRule, List<Integer> shopIds, List<Integer> class2Ids,
                                          List<ActivitySsu> containSsuList,
                                          List<ActivitySsu> exSsuList) {
        ProductScopeTypeEnums productScopeType = ProductScopeTypeEnums.getEnum(promotionRule.getProductScope());
        if (productScopeType == null) {
            throw new BusinessException("商品范围异常");
        }

        List<Integer> ssuFormatIds = null;

        List<Integer> exSsuFormatIds = null;
        if (!ListUtil.isNullOrEmpty(exSsuList)) {
            exSsuFormatIds = exSsuList.stream().map(ActivitySsu::getSsuFormatId).collect(Collectors.toList());
        }
        switch (productScopeType) {
            case CLASS2:
                CMallPriceQuery cMallPriceQuery = new CMallPriceQuery();
                // 查询所有绑定门店商品
                cMallPriceQuery.setShopIds(shopIds);
                // 1、按条件全部查询 2、查询上线，未删除，价格>0
                cMallPriceQuery.setStatus(1);
                cMallPriceQuery.setClass2Ids(class2Ids);
                if (!ListUtil.isNullOrEmpty(exSsuFormatIds)) {
                    cMallPriceQuery.setExSsuFormatIds(exSsuFormatIds);
                }
                List<CMallPriceInfo> cMallPriceInfos = cmallExternalApiService.queryCmallPriceList(cMallPriceQuery);
                ssuFormatIds = cMallPriceInfos.stream().map(CMallPriceInfo::getSsuFormatId).collect(Collectors.toList());
                break;
            case SSU:
                ssuFormatIds = containSsuList.stream().map(ActivitySsu::getSsuFormatId).collect(Collectors.toList());
                break;
            default:
        }
        return ssuFormatIds;
    }
}
