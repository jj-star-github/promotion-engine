package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.PromotionActivityTime;
import com.qmfresh.promotion.manager.IPromotionActivityTimeManager;
import com.qmfresh.promotion.mapper.PromotionActivityTimeMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@Component
@Slf4j
public class PromotionActivityTimeManagerImpl extends ServiceImpl<PromotionActivityTimeMapper, PromotionActivityTime> implements IPromotionActivityTimeManager {
    @Resource
    private PromotionActivityTimeMapper promotionActivityTimeMapper;

    @Override
    public List<PromotionActivityTime> getActByShopIds(List<Integer> shopIds, Integer startTime, Integer endTime) {
        return promotionActivityTimeMapper.getShopActTimesByShopIds(shopIds, startTime, endTime);
    }

    @Override
    public List<PromotionActivityTime> getCurrentActiveAct(List<Integer> promotionModuleIds, Integer startTime, Integer endTime) {
        return promotionActivityTimeMapper.getCurrentActiveAct(promotionModuleIds, startTime, endTime);
    }

    @Override
    public List<PromotionActivityTime> promotionActivityTimes(List<Long> promotionActivityIds) {
        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }

        List<PromotionActivityTime> promotionActivityTimes = this.list(new LambdaQueryWrapper<PromotionActivityTime>()
                .eq(PromotionActivityTime::getIsDeleted, 0)
                .in(PromotionActivityTime::getActivityId, promotionActivityIds)
        );

        return promotionActivityTimes;
    }


}
