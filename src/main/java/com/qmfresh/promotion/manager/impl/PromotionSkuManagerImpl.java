package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.PromotionSku;

import com.qmfresh.promotion.manager.IPromotionSkuManager;
import com.qmfresh.promotion.mapper.PromotionSkuMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xbb
 * @since 2019-11-29
 */
@Service
public class PromotionSkuManagerImpl extends ServiceImpl<PromotionSkuMapper, PromotionSku> implements IPromotionSkuManager {
	
}
