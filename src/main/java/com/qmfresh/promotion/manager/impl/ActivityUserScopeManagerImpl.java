package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.ActivityUserScope;
import com.qmfresh.promotion.mapper.ActivityUserScopeMapper;
import com.qmfresh.promotion.manager.IActivityUserScopeManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 活动用户作用域 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@Service
@Slf4j
public class ActivityUserScopeManagerImpl extends ServiceImpl<ActivityUserScopeMapper, ActivityUserScope> implements IActivityUserScopeManager {


    @Override
    public List<ActivityUserScope> activityUserScope(List<Long> promotionActivityIds) {
        log.info("activityUserScope请求参数：{}", JSON.toJSONString(promotionActivityIds));

        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }

        List<ActivityUserScope> activityUserScopes = this.list(new LambdaQueryWrapper<ActivityUserScope>()
                .eq(ActivityUserScope::getIsDeleted, 0)
                .in(ActivityUserScope::getActivityId, promotionActivityIds)

        );

        if (CollectionUtils.isEmpty(activityUserScopes)) {
            return new ArrayList<>();
        }

        return activityUserScopes;
    }
}
