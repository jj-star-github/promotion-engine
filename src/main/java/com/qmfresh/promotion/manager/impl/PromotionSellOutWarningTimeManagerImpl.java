package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.PromotionSellOutWarningTime;
import com.qmfresh.promotion.manager.IPromotionSellOutWarningTimeManager;
import com.qmfresh.promotion.mapper.PromotionSellOutWarningTimeMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class PromotionSellOutWarningTimeManagerImpl extends ServiceImpl<PromotionSellOutWarningTimeMapper, PromotionSellOutWarningTime> implements IPromotionSellOutWarningTimeManager {
    @Autowired
    PromotionSellOutWarningTimeMapper promotionSellOutWarningTimeMapper;

    @Override
    public Boolean updateByWarningId(List<Integer> ids) {
        return promotionSellOutWarningTimeMapper.updateByWarningId(ids);
    }

    @Override
    public Boolean updateByCondition(List<PromotionSellOutWarningTime> times) {
        return promotionSellOutWarningTimeMapper.updateByCondition(times);
    }

}
