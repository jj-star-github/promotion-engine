package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.PresentModifyLog;
import com.qmfresh.promotion.manager.IPresentModifyLogManager;
import com.qmfresh.promotion.mapper.PresentModifyLogMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-25
 */
@Service
public class PresentModifyLogManagerImpl extends ServiceImpl<PresentModifyLogMapper, PresentModifyLog> implements IPresentModifyLogManager {
	
}
