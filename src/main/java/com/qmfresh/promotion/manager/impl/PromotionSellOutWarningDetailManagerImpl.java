package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.PromotionSellOutWarningDetail;
import com.qmfresh.promotion.manager.IPromotionSellOutWarningDetailManager;
import com.qmfresh.promotion.mapper.PromotionSellOutWarningDetailMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xbb
 * @since 2019-11-23
 */
@Service
public class PromotionSellOutWarningDetailManagerImpl extends ServiceImpl<PromotionSellOutWarningDetailMapper, PromotionSellOutWarningDetail> implements IPromotionSellOutWarningDetailManager {
	
}
