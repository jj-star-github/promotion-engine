package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.UserCard;
import com.qmfresh.promotion.manager.IUserCardManager;
import com.qmfresh.promotion.mapper.UserCardMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
@Service
public class UserCardManagerImpl extends ServiceImpl<UserCardMapper, UserCard> implements IUserCardManager {
	
}
