package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.bean.promotion.QueryPromotionModuleParam;
import com.qmfresh.promotion.entity.PromotionModule;
import com.qmfresh.promotion.manager.IPromotionModuleManager;
import com.qmfresh.promotion.mapper.PromotionModuleMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@Component
@Slf4j
public class PromotionModuleManagerImpl extends ServiceImpl<PromotionModuleMapper, PromotionModule> implements IPromotionModuleManager {

    @Override
    public List<PromotionModule> queryByCondition(QueryPromotionModuleParam param) {
        LambdaQueryWrapper<PromotionModule> where = new LambdaQueryWrapper<>();
        where.eq(PromotionModule::getIsDeleted, 0);
        if (param.getModuleId() != null) {
            where.eq(PromotionModule::getId, param.getModuleId());
        }
        if (param.getModuleIds() != null) {
            where.in(PromotionModule::getId, param.getModuleIds());
        }
        if (param.getName() != null) {
            where.like(PromotionModule::getName, param.getName());
        }
        if (param.getPriority() != null) {
            where.eq(PromotionModule::getPriority, param.getPriority());
        }
        return this.list(where);
    }

    @Override
    public List<PromotionModule> promotionModule(List<Integer> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return new ArrayList<>();
        }
        log.info("查询促销模板参数： {}", JSON.toJSONString(ids));

        List<PromotionModule> promotionModules =
                this.list(new LambdaQueryWrapper<PromotionModule>()
                        .eq(PromotionModule::getIsDeleted, 0)
                        .in(PromotionModule::getId, ids));

        if (CollectionUtils.isEmpty(promotionModules)) {
            return new ArrayList<>();
        }
        return promotionModules;
    }
}
