package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.AreaScopeCity;
import com.qmfresh.promotion.manager.IAreaScopeCityManager;
import com.qmfresh.promotion.mapper.AreaScopeCityMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 城市区域 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@Service
@Slf4j
public class AreaScopeCityManagerImpl extends ServiceImpl<AreaScopeCityMapper, AreaScopeCity> implements IAreaScopeCityManager {

    @Override
    public List<AreaScopeCity> areaScopeCity(Long areaScopeId) {
        if (areaScopeId == null) {
            return new ArrayList<>();
        }
        log.info("区域城市查询：{}", areaScopeId);
        List<AreaScopeCity> areaScopeCities = this.list(new LambdaQueryWrapper<AreaScopeCity>()
                .eq(AreaScopeCity::getAreaScopeId, areaScopeId)
                .eq(AreaScopeCity::getIsDeleted, 0)
        );

        if (CollectionUtils.isEmpty(areaScopeCities)) {
            return new ArrayList<>();
        }
        return areaScopeCities;
    }

    @Override
    public List<AreaScopeCity> areaScopeCity(List<Long> areaScopeId) {
        log.info("areaScopeCity查询参数：{}", JSON.toJSONString(areaScopeId));
        if (CollectionUtils.isEmpty(areaScopeId)) {
            return new ArrayList<>();
        }
        List<AreaScopeCity> areaScopeCities = this.list(new LambdaQueryWrapper<AreaScopeCity>()
                .in(AreaScopeCity::getAreaScopeId, areaScopeId)
                .eq(AreaScopeCity::getIsDeleted, 0)
        );

        if (CollectionUtils.isEmpty(areaScopeCities)) {
            return new ArrayList<>();
        }
        return areaScopeCities;
    }
}
