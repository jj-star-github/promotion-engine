package com.qmfresh.promotion.manager.impl;

import com.qmfresh.promotion.bean.promotion.QueryActivityParam;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.dto.PromotionProtocol;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.ActivityOnlineTypeEnums;
import com.qmfresh.promotion.enums.ActivityStatusTypeEnums;
import com.qmfresh.promotion.manager.IExecutePromotionManager;
import com.qmfresh.promotion.manager.IPromotionRuleManager;
import com.qmfresh.promotion.service.IPromotionActivityService;
import com.qmfresh.promotion.strategy.facade.PromotionExecuteFacade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wyh on 2019/5/24.
 *
 * @author wyh
 */
@Component
@Slf4j
public class ExecutePromotionManagerImpl implements IExecutePromotionManager {

    @Resource
    private IPromotionActivityService promotionActivityService;

    @Resource
    private IPromotionRuleManager promotionRuleManager;


    @Resource
    private PromotionExecuteFacade facade;

    @Override
    public PromotionContext execute(PromotionProtocol param) {
        return facade.executeStrategy(param);
    }

    /**
     * 获取当前生效状态促销活动
     *
     * @return
     */
    private List<PromotionActivity> getCurrentActivities() {
        QueryActivityParam queryActivityParam = new QueryActivityParam();
        queryActivityParam.setEffectBeginTime(DateUtil.getStartTimeStamp());
        queryActivityParam.setEffectEndTime(DateUtil.getEndTimeStamp());
        queryActivityParam.setIsOnline(ActivityOnlineTypeEnums.ONLINE.getCode());
        queryActivityParam.setStatus(ActivityStatusTypeEnums.STARTED.getCode());
        return promotionActivityService.queryByCondition(queryActivityParam);
    }


    private List<PromotionRule> getPromotionRule(List<Long> activityIds) {
        return promotionRuleManager.promotionRules(activityIds, ActivityOnlineTypeEnums.ONLINE);
    }
}
