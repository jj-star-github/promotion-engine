package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.OrdercReturnItem;
import com.qmfresh.promotion.manager.IOrdercReturnItemManager;
import com.qmfresh.promotion.mapper.OrdercReturnItemMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 退货单详情 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
@Service
public class OrdercReturnItemManagerImpl extends ServiceImpl<OrdercReturnItemMapper, OrdercReturnItem> implements IOrdercReturnItemManager {
	
}
