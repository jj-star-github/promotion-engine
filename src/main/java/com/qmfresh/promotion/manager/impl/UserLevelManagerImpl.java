package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.UserLevel;
import com.qmfresh.promotion.manager.IUserLevelManager;
import com.qmfresh.promotion.mapper.UserLevelMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
@Service
public class UserLevelManagerImpl extends ServiceImpl<UserLevelMapper, UserLevel> implements IUserLevelManager {
	
}
