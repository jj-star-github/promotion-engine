package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.ActivityOnlineTypeEnums;
import com.qmfresh.promotion.enums.PromotionTypeEnums;
import com.qmfresh.promotion.mapper.PromotionRuleMapper;
import com.qmfresh.promotion.manager.IPromotionRuleManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@Service
@Slf4j
public class PromotionRuleManagerImpl extends ServiceImpl<PromotionRuleMapper, PromotionRule> implements IPromotionRuleManager {

    @Override
    public List<PromotionRule> promotionRules(List<Long> promotionActivityIds, ActivityOnlineTypeEnums onlineType) {

        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }
        if (onlineType == null) {
            return new ArrayList<>();
        }

        log.info("促销规则请求参数： {} {}", JSON.toJSONString(promotionActivityIds), onlineType);
        return this.list(new LambdaQueryWrapper<PromotionRule>()
                .eq(PromotionRule::getIsOnline, onlineType.getCode())
                .eq(PromotionRule::getIsDeleted, 0)
                .in(PromotionRule::getActivityId, promotionActivityIds));
    }

    @Override
    public List<PromotionRule> promotionRules(List<Long> promotionActivityIds, PromotionTypeEnums promotionTypeEnums) {
        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }
        if (promotionTypeEnums == null) {
            return new ArrayList<>();
        }

        log.info("请求模板规则参数：filterPromotionActivity={},promotionModuleId = {}", JSON.toJSONString(promotionActivityIds), promotionTypeEnums.getCode());

        List<PromotionRule> promotionRules = this.list(
                new LambdaQueryWrapper<PromotionRule>()
                        .eq(PromotionRule::getIsDeleted, 0)
                        .in(PromotionRule::getActivityId, promotionActivityIds)
                        .eq(PromotionRule::getPromotionModuleId, promotionTypeEnums.getCode())
        );
        if (CollectionUtils.isEmpty(promotionRules)) {
            return new ArrayList<>();
        }
        return promotionRules;
    }

    @Override
    public List<PromotionRule> promotionRules(List<Long> promotionActivityIds) {
        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }

        log.info("查询促销规则的参数:{}", JSON.toJSONString(promotionActivityIds));
        List<PromotionRule> promotionRules = this.list(
                new LambdaQueryWrapper<PromotionRule>()
                        .eq(PromotionRule::getIsDeleted, 0)
                        .in(PromotionRule::getActivityId, promotionActivityIds)
        );

        return promotionRules;
    }


    /**
     * 查询促销规则
     *
     * @param ids 促销规则id
     * @return 返回促销规则
     */
    @Override
    public List<PromotionRule> filterPromotionRules(List<Integer> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return new ArrayList<>();
        }

        log.info("查询促销规则列表：{}", JSON.toJSONString(ids));
        List<PromotionRule> promotionRules = this.list(new LambdaQueryWrapper<PromotionRule>()
                .eq(PromotionRule::getIsDeleted, 0)
                .in(PromotionRule::getId, ids));
        if (CollectionUtils.isEmpty(promotionRules)) {
            return new ArrayList<>();
        }
        return promotionRules;
    }
}
