package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.PromotionOrderItem;
import com.qmfresh.promotion.manager.IPromotionOrderItemManager;
import com.qmfresh.promotion.mapper.PromotionOrderItemMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-13
 */
@Service
public class PromotionOrderItemManagerImpl extends ServiceImpl<PromotionOrderItemMapper, PromotionOrderItem> implements IPromotionOrderItemManager {
	
}
