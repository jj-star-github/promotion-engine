package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.PromotionModifyLog;
import com.qmfresh.promotion.manager.IPromotionModifyLogManager;
import com.qmfresh.promotion.mapper.PromotionModifyLogMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-25
 */
@Service
public class PromotionModifyLogManagerImpl extends ServiceImpl<PromotionModifyLogMapper, PromotionModifyLog> implements IPromotionModifyLogManager {

}
