package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.OrdercReturn;
import com.qmfresh.promotion.manager.IOrdercReturnManager;
import com.qmfresh.promotion.mapper.OrdercReturnMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 退货单主表 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
@Service
public class OrdercReturnManagerImpl extends ServiceImpl<OrdercReturnMapper, OrdercReturn> implements IOrdercReturnManager {
	
}
