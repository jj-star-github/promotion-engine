package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.PromotionSellOutShopWarning;
import com.qmfresh.promotion.manager.IPromotionSellOutShopWarningManager;
import com.qmfresh.promotion.mapper.PromotionSellOutShopWarningMapper;
import org.springframework.stereotype.Component;

@Component
public class PromotionSellOutShopWarningManagerImpl extends ServiceImpl<PromotionSellOutShopWarningMapper, PromotionSellOutShopWarning> implements IPromotionSellOutShopWarningManager{

}
