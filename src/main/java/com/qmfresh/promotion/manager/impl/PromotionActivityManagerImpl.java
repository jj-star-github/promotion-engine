package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.ImmutableList;
import com.qmfresh.promotion.bean.promotion.PageQueryActivityParam;
import com.qmfresh.promotion.bean.promotion.QueryActivityParam;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.common.utils.TraceIdUtils;
import com.qmfresh.promotion.dto.ShopAuthorityResult;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.*;
import com.qmfresh.promotion.enums.*;
import com.qmfresh.promotion.manager.IPromotionActivityManager;
import com.qmfresh.promotion.manager.IPromotionActivityTimeManager;
import com.qmfresh.promotion.mapper.*;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.service.IPromotionProductMarkService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@Component
@Slf4j
public class PromotionActivityManagerImpl extends ServiceImpl<PromotionActivityMapper, PromotionActivity> implements IPromotionActivityManager {

    private static final Logger logger = LoggerFactory.getLogger(PromotionActivityManagerImpl.class);

    @Resource
    private PromotionModuleMapper promotionModuleMapper;
    @Resource
    private PromotionActivityMapper promotionActivityMapper;
    @Resource
    private PromotionActivityTimeMapper promotionActivityTimeMapper;
    @Resource
    private PromotionRuleMapper promotionRuleMapper;
    @Resource
    private ActivityAreaScopeMapper activityAreaScopeMapper;
    @Resource
    private AreaScopeCityMapper areaScopeCityMapper;
    @Resource
    private AreaScopeShopMapper areaScopeShopMapper;
    @Resource
    private ActivityUserScopeMapper activityUserScopeMapper;
    @Resource
    private UserScopeGroupMapper userScopeGroupMapper;
    @Resource
    private ActivityClass2Mapper activityClass2Mapper;
    @Resource
    private ActivitySsuMapper activitySsuMapper;
    @Resource
    private PresentActivityMapper presentActivityMapper;
    @Resource
    private PresentSsuMapper presentSsuMapper;
    @Resource
    private IPromotionProductMarkService productMarkService;
    @Resource
    private IPromotionActivityTimeManager promotionActivityTimeManager;

    @Override
    public ListWithPage<PromotionActivity> pageQuery(PageQueryActivityParam param) {
        ListWithPage<PromotionActivity> pageList = new ListWithPage<>();
        List<PromotionActivity> promotionActivities = null;
        //当入参中的moduleId不是改价对应的id时，就走查询促销类型的逻辑
        if (param.getModuleId() == null || PromotionTypeEnums.CP.getCode() != param.getModuleId()) {
            if (param.getActivityName() != null && !param.getActivityName().isEmpty()) {
                LambdaQueryWrapper<PromotionModule> where = new LambdaQueryWrapper<PromotionModule>();
                where.eq(PromotionModule::getIsDeleted, 0);
                where.like(PromotionModule::getName, param.getActivityName());
                if (!ListUtil.isNullOrEmpty(param.getModuleIds())) {
                    where.in(PromotionModule::getId, param.getModuleIds());
                }
                if (param.getModuleId() != null) {
                    where.eq(PromotionModule::getId, param.getModuleId());
                }
                List<PromotionModule> promotionModules = promotionModuleMapper.selectList(where);
                if (ListUtil.isNullOrEmpty(promotionModules)) {
                    throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("未查询到促销信息");
                }
                param.setModuleIds(promotionModules.stream().map(PromotionModule::getId).collect(Collectors.toList()));
            }
            promotionActivities = promotionActivityMapper.pageQuery(param, param.getStart(), param.getEnd());
            if (promotionActivities == null || promotionActivities.isEmpty()) {
                return pageList;
            }

            int count = promotionActivityMapper.countByCondition(param);
            pageList.setTotalCount(count);
            pageList.setListData(promotionActivities);
            return pageList;
        }

        promotionActivities = promotionActivityMapper.pageQuery4Cp(param, param.getStart(), param.getEnd());
        if (promotionActivities == null || promotionActivities.isEmpty()) {
            return pageList;
        }

        int count = promotionActivityMapper.countByCondition4Cp(param);
        pageList.setTotalCount(count);
        pageList.setListData(promotionActivities);
        return pageList;
    }

    /**
     * 根据门店查询活动信息
     * @param param
     * @return
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<PromotionActivity> queryPromotionActivityByShop(QueryActivityParam param) {
        logger.info("根据门店查询活动信息 traceId={} param={}", TraceIdUtils.getTraceId(), JSON.toJSONString(param));
        if (ListUtil.isNullOrEmpty(param.getShopIds()) && null == param.getShopId()) {
            logger.error("根据门店查询活动信息 门店信息不可为空 traceId={}", TraceIdUtils.getTraceId());
            return new ArrayList<>(1);
        }
        //查询门店的活动信息
        List moduleList = Arrays.asList(1, 2, 3, 4);
        if (null != param.getModuleId()) {
            moduleList = Arrays.asList(param.getModuleId());
        }
        List shopList = new ArrayList();
        if(!ListUtil.isNullOrEmpty(param.getShopIds())) {
            shopList.addAll(param.getShopIds());
        }
        if (null != param.getShopId() && !shopList.contains(param.getShopId())) {
            shopList.add(param.getShopId());
        }
        Integer status = null;
        if (param.getIsOnline() == 1) {
            status = ActivityStatusTypeEnums.STARTED.getCode();
        }
        List<PromotionActivity>  activityList = promotionActivityMapper.queryByShop(shopList, moduleList, status);
        if (ListUtil.isNullOrEmpty(activityList)) {
            logger.info("根据门店查询活动信息 活动信息为空：traceId={} shopList={} moduleList={}", TraceIdUtils.getTraceId(), JSON.toJSONString(shopList), JSON.toJSONString(moduleList));
            return new ArrayList<>(1);
        }

        //查询有效的活动时间
        List<Long> activityIds = activityList.stream().map(PromotionActivity::getId).collect(Collectors.toList());
        List<PromotionActivityTime> activityTimeList = promotionActivityTimeMapper.getActTimesByActIds(activityIds, DateUtil.getCurrentTimeIntValue());
        if (ListUtil.isNullOrEmpty(activityTimeList)) {
            logger.info("根据门店查询活动信息 活动有效时间为空：traceId={} activityIds={}", TraceIdUtils.getTraceId(), JSON.toJSONString(activityIds));
            return new ArrayList<>(1);
        }

        Map<Long, List<PromotionActivityTime>> patMap = activityTimeList.stream().collect(Collectors.groupingBy(PromotionActivityTime::getActivityId));
        List<Long> finalActivityIds = activityTimeList.stream().map(PromotionActivityTime::getActivityId).collect(Collectors.toList());
        int weekDay = DateUtil.getWeekByDate(new Date());
        List<PromotionActivity> filterPa = new ArrayList<>(16);
        //过滤活动weekDay
        activityList.forEach(
                pa -> {
                    if (finalActivityIds.contains(pa.getId())) {
                        if (pa.getPromotionModuleId().equals(PromotionTypeEnums.VIP.getCode())) {
                            List<PromotionActivityTime> patList = patMap.get(pa.getId());
                            for (PromotionActivityTime pat : patList) {
                                List<String> weekDays = ListUtil.stringToList(pat.getWeekDay());
                                if (weekDays.contains(String.valueOf(weekDay))) {
                                    filterPa.add(pa);
                                }
                            }
                        } else {
                            filterPa.add(pa);
                        }
                    }
                }
        );
        return filterPa;
    }

    /**
     * 不查促销改价活动
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<PromotionActivity> queryCurrentPromotionActivity(QueryActivityParam param) {
        int now = DateUtil.getCurrentTimeIntValue();

        List<PromotionActivityTime> promotionActivityTimes = promotionActivityTimeManager.getCurrentActiveAct(null, now, now);

        if (ListUtil.isNullOrEmpty(promotionActivityTimes)) {
            return new ArrayList<>(1);
        }

        LambdaQueryWrapper<PromotionActivity> condition = new LambdaQueryWrapper<>();

        condition.in(PromotionActivity::getId, promotionActivityTimes.stream().map(PromotionActivityTime::getActivityId).collect(Collectors.toList()));
        condition.in(PromotionActivity::getPromotionModuleId, Arrays.asList(1, 2, 3, 4));

        if (param.getIsOnline() == 1) {
            condition.eq(PromotionActivity::getStatus, ActivityStatusTypeEnums.STARTED.getCode());
        }
        if (param.getModuleId() != null) {
            condition.eq(PromotionActivity::getPromotionModuleId, param.getModuleId());
        }
        condition.eq(PromotionActivity::getIsDeleted, 0);
        List<PromotionActivity> promotionActivities = promotionActivityMapper.selectList(condition);
        if (ListUtil.isNullOrEmpty(promotionActivities)) {
            return new ArrayList<>(1);
        }
        LambdaQueryWrapper<AreaScopeShop> shopCondition = new LambdaQueryWrapper<>();
        shopCondition.in(AreaScopeShop::getActivityId, promotionActivities.stream().map(PromotionActivity::getId).collect(Collectors.toList()));
        shopCondition.eq(AreaScopeShop::getIsDeleted, 0);
        if (!ListUtil.isNullOrEmpty(param.getShopIds())) {
            shopCondition.in(AreaScopeShop::getShopId, param.getShopIds());
        }
        if (param.getShopId() != null) {
            shopCondition.eq(AreaScopeShop::getShopId, param.getShopId());
        }
        List<AreaScopeShop> areaScopeShops = areaScopeShopMapper.selectList(shopCondition);
        if (!ListUtil.isNullOrEmpty(areaScopeShops)) {
            List<Long> activityIds = areaScopeShops.stream().map(AreaScopeShop::getActivityId).distinct().collect(Collectors.toList());
            promotionActivities = promotionActivities.stream().filter(item -> activityIds.contains(item.getId())).collect(Collectors.toList());
        } else {
            return new ArrayList<>(1);
        }

        Map<Long, List<PromotionActivityTime>> patMap = promotionActivityTimes.stream().collect(Collectors.groupingBy(PromotionActivityTime::getActivityId));
        int weekDay = DateUtil.getWeekByDate(new Date());
        List<PromotionActivity> filterPa = new ArrayList<>(16);
        promotionActivities.forEach(
                pa -> {
                    if (pa.getPromotionModuleId().equals(PromotionTypeEnums.VIP.getCode())) {
                        List<PromotionActivityTime> patList = patMap.get(pa.getId());
                        for (PromotionActivityTime pat : patList) {
                            List<String> weekDays = ListUtil.stringToList(pat.getWeekDay());
                            if (pat.getEffectBeginTime() > now || pat.getEffectEndTime() < now) {
                                continue;
                            }
                            if (weekDays.contains(String.valueOf(weekDay))) {
                                filterPa.add(pa);
                            }
                        }
                    } else {
                        filterPa.add(pa);
                    }
                }
        );
        return filterPa;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<PromotionActivity> queryByCondition(QueryActivityParam param) {

        Integer startTime = DateUtil.getCurrentTimeIntValue();
        Integer endTime = DateUtil.getCurrentTimeIntValue();
        List<Integer> promotionModuleIds = new ArrayList<>(5);
        if (param.getEffectBeginTime() != null && param.getEffectEndTime() != null) {
            startTime = param.getEffectBeginTime();
            endTime = param.getEffectEndTime();
        }
        if (param.getModuleId() != null) {
            promotionModuleIds.add(param.getModuleId());
        }
        if (!ListUtil.isNullOrEmpty(param.getModuleIds())) {
            promotionModuleIds.addAll(param.getModuleIds());

        } else {
            promotionModuleIds = Arrays.asList(1, 2, 3, 4);
        }

        List<PromotionActivityTime> promotionActivityTimes = promotionActivityTimeManager.getCurrentActiveAct(promotionModuleIds, startTime, endTime);
        logger.info("PromotionActivityTime查询,{},{}", JSON.toJSONString(promotionActivityTimes), JSON.toJSONString(param));

        if (ListUtil.isNullOrEmpty(promotionActivityTimes)) {
            return new ArrayList<>(1);
        }
        LambdaQueryWrapper<PromotionActivity> where = new LambdaQueryWrapper<>();
        where.eq(PromotionActivity::getIsDeleted, 0);
        where.eq(PromotionActivity::getChannel, param.getChannel());
        if (param.getModuleId() != null) {
            where.eq(PromotionActivity::getPromotionModuleId, param.getModuleId());
        }
        if (param.getModuleIds() != null) {
            where.in(PromotionActivity::getPromotionModuleId, param.getModuleIds());
        }
        if (param.getStatus() != null) {
            where.eq(PromotionActivity::getStatus, param.getStatus());
        }
        if (param.getCreatorFrom() != null) {
            where.eq(PromotionActivity::getCreatorFrom, param.getCreatorFrom());
        }
        if (!ListUtil.isNullOrEmpty(param.getStatusList())) {
            where.in(PromotionActivity::getStatus, param.getStatusList());
        }
        if (!ListUtil.isNullOrEmpty(promotionActivityTimes)) {
            where.in(PromotionActivity::getId, promotionActivityTimes.stream().map(PromotionActivityTime::getActivityId).distinct().collect(Collectors.toList()));
        }
        List<PromotionActivity> promotionActivities = promotionActivityMapper.selectList(where);
        if (ListUtil.isNullOrEmpty(promotionActivities)) {
            return new ArrayList<>(1);
        }

        //        List<PromotionActivity> result = new ArrayList<>(16);
        if (param.getShopId() != null) {
            List<AreaScopeShop> areaScopeShops = areaScopeShopMapper.selectList(new LambdaQueryWrapper<AreaScopeShop>()
                    .eq(AreaScopeShop::getShopId, param.getShopId())
                    .in(AreaScopeShop::getActivityId, promotionActivities.stream().map(PromotionActivity::getId).collect(Collectors.toList()))
                    .eq(AreaScopeShop::getIsDeleted, 0)
            );
            if (!ListUtil.isNullOrEmpty(areaScopeShops)) {
                List<Long> activityIds = areaScopeShops.stream().map(AreaScopeShop::getActivityId).distinct().collect(Collectors.toList());
                promotionActivities = promotionActivities.stream().filter(item -> !activityIds.contains(item.getId())).collect(Collectors.toList());
            }
        }
        return promotionActivities;

    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean online(Long activityId, Long userId, String userName) {
        int now = DateUtil.getCurrentTimeIntValue();
        PromotionActivity pa = this.getOne(new LambdaQueryWrapper<PromotionActivity>()
                .eq(PromotionActivity::getId, activityId)
                .eq(PromotionActivity::getIsDeleted, 0));
        if (pa == null || pa.getStatus() == null) {
            logger.warn("未查到促销活动");
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("未查到促销活动");
        }
        if (Integer.valueOf(ActivityStatusTypeEnums.NOT_START.getCode()).equals(pa.getStatus()) ||
                Integer.valueOf(ActivityStatusTypeEnums.STOP.getCode()).equals(pa.getStatus()) ||
                Integer.valueOf(ActivityStatusTypeEnums.APPROVED.getCode()).equals(pa.getStatus())) {
            PromotionActivity updatePa = new PromotionActivity();
            updatePa.setId(pa.getId());
            updatePa.setStatus(ActivityStatusTypeEnums.STARTED.getCode());
            updatePa.setOnlineTime(now);
            updatePa.setUpdateUserId(userId);
            updatePa.setUpdateUserName(userName);
            updatePa.setUT(now);
            promotionActivityMapper.updateById(updatePa);
            logger.info("into method PromotionServiceImpl-online is online");
            if (Integer.valueOf(ChannelTypeEnums.C_ONLINE.getCode()).equals(pa.getChannel())) {
                productMarkService.markProduct(activityId, ActivityStatusTypeEnums.STARTED);
            }
            return true;
        }
        throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("活动上线失败，活动状态异常");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean stop(Long activityId, Long userId, String userName) {
        int now = DateUtil.getCurrentTimeIntValue();
        PromotionActivity pa = this.getOne(new LambdaQueryWrapper<PromotionActivity>()
                .eq(PromotionActivity::getId, activityId)
                .eq(PromotionActivity::getIsDeleted, 0));
        if (pa.getStatus().equals(ActivityStatusTypeEnums.STARTED.getCode()) ||
                pa.getStatus().equals(ActivityStatusTypeEnums.NOT_START.getCode())) {
            PromotionActivity updatePa = new PromotionActivity();
            updatePa.setId(pa.getId());
            updatePa.setStatus(ActivityStatusTypeEnums.STOP.getCode());
            updatePa.setOfflineTime(now);
            updatePa.setUpdateUserId(userId);
            updatePa.setUpdateUserName(userName);
            updatePa.setUT(now);
            promotionActivityMapper.updateById(updatePa);

            if (pa.getChannel().equals(ChannelTypeEnums.C_ONLINE.getCode())) {
                productMarkService.markProduct(activityId, ActivityStatusTypeEnums.STOP);
            }
            return true;
        }
        throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("活动暂停失败，活动状态异常");
    }

    @Override
    public Boolean finish(List<Long> activityIds) {
        PromotionActivity updatePa = new PromotionActivity();
        updatePa.setStatus(ActivityStatusTypeEnums.FINISH.getCode());
        updatePa.setUT(DateUtil.getCurrentTimeIntValue());
        updatePa.setIsDeleted(0);
        promotionActivityMapper.update(updatePa, new LambdaQueryWrapper<PromotionActivity>()
                .in(PromotionActivity::getId, activityIds)
                .eq(PromotionActivity::getIsDeleted, 0));

        List<PromotionActivity> promotionActivities = promotionActivityMapper.selectList(new LambdaQueryWrapper<PromotionActivity>()
                .in(PromotionActivity::getId, activityIds)
                .eq(PromotionActivity::getChannel, ChannelTypeEnums.C_ONLINE.getCode()));

        if (!ListUtil.isNullOrEmpty(promotionActivities)) {
            promotionActivities.forEach(
                    promotionActivity -> productMarkService.markProduct(promotionActivity.getId(), ActivityStatusTypeEnums.FINISH)
            );
        }
        return true;
    }

    private PromotionActivity getOne(Long activityId) {
        if (activityId == null) {
            return null;
        }

        PromotionActivity pa = this.getOne(new LambdaQueryWrapper<PromotionActivity>()
                .eq(PromotionActivity::getId, activityId)
                .eq(PromotionActivity::getIsDeleted, 0)
        );

        return pa;
    }

    @Override
    public Boolean outOfDate(List<Long> activityIds) {
        int now = DateUtil.getCurrentTimeIntValue();
        PromotionActivity updatePa = new PromotionActivity();
        updatePa.setStatus(ActivityStatusTypeEnums.OUT_OF_DATE.getCode());
        updatePa.setOnlineTime(now);
        updatePa.setUT(now);
        promotionActivityMapper.update(updatePa, new LambdaQueryWrapper<PromotionActivity>()
                .in(PromotionActivity::getId, activityIds)
                .eq(PromotionActivity::getStatus, ActivityStatusTypeEnums.UNAUDITED.getCode())
                .eq(PromotionActivity::getIsDeleted, 0));

        List<PromotionActivity> promotionActivities = promotionActivityMapper.selectList(new LambdaQueryWrapper<PromotionActivity>()
                .in(PromotionActivity::getId, activityIds)
                .eq(PromotionActivity::getChannel, ChannelTypeEnums.C_ONLINE.getCode())
                .eq(PromotionActivity::getIsDeleted, IsDeleteEnums.NO.getCode()));

        if (!ListUtil.isNullOrEmpty(promotionActivities)) {
            promotionActivities.forEach(
                    promotionActivity -> productMarkService.markProduct(promotionActivity.getId(), ActivityStatusTypeEnums.OUT_OF_DATE)
            );
        }
        return true;
    }

    @Override
    public Boolean start(List<Long> activityIds) {
        int now = DateUtil.getCurrentTimeIntValue();
        PromotionActivity updatePa = new PromotionActivity();
        updatePa.setStatus(ActivityStatusTypeEnums.STARTED.getCode());
        updatePa.setOnlineTime(now);
        updatePa.setUT(now);
        promotionActivityMapper.update(updatePa, new LambdaQueryWrapper<PromotionActivity>()
                .in(PromotionActivity::getId, activityIds)
                .eq(PromotionActivity::getStatus, ActivityStatusTypeEnums.APPROVED.getCode())
                .eq(PromotionActivity::getIsDeleted, 0));

        List<PromotionActivity> promotionActivities = promotionActivityMapper.selectList(new LambdaQueryWrapper<PromotionActivity>()
                .in(PromotionActivity::getId, activityIds)
                .eq(PromotionActivity::getChannel, ChannelTypeEnums.C_ONLINE.getCode())
                .eq(PromotionActivity::getIsDeleted, IsDeleteEnums.NO.getCode()));

        if (!ListUtil.isNullOrEmpty(promotionActivities)) {
            promotionActivities.forEach(
                    promotionActivity -> productMarkService.markProduct(promotionActivity.getId(), ActivityStatusTypeEnums.STARTED)
            );
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean delete(Long activityId, Long userId, String userName) {
        int now = DateUtil.getCurrentTimeIntValue();
        PromotionActivity pa = this.getOne(activityId);
        if (pa.getStatus().equals(ActivityStatusTypeEnums.NOT_START.getCode())) {
            PromotionActivity updatePa = new PromotionActivity();
            updatePa.setId(pa.getId());
            updatePa.setUpdateUserId(userId);
            updatePa.setUpdateUserName(userName);
            updatePa.setIsDeleted(1);
            updatePa.setUT(now);
            promotionActivityMapper.updateById(updatePa);

            // 删除活动相关表数据
            PromotionActivityTime updatePromotionActivityTime = new PromotionActivityTime();
            updatePromotionActivityTime.setUT(now);
            updatePromotionActivityTime.setIsDeleted(1);
            promotionActivityTimeMapper.update(updatePromotionActivityTime, new LambdaQueryWrapper<PromotionActivityTime>()
                    .eq(PromotionActivityTime::getActivityId, activityId)
                    .eq(PromotionActivityTime::getIsDeleted, 0));

            PromotionRule updatePromotionRule = new PromotionRule();
            updatePromotionRule.setUT(now);
            updatePromotionRule.setIsDeleted(1);
            promotionRuleMapper.update(updatePromotionRule, new LambdaQueryWrapper<PromotionRule>()
                    .eq(PromotionRule::getActivityId, activityId)
                    .eq(PromotionRule::getIsDeleted, 0));

            ActivityAreaScope updateActivityAreaScope = new ActivityAreaScope();
            updateActivityAreaScope.setUT(now);
            updateActivityAreaScope.setIsDeleted(1);
            activityAreaScopeMapper.update(updateActivityAreaScope, new LambdaQueryWrapper<ActivityAreaScope>()
                    .eq(ActivityAreaScope::getActivityId, activityId)
                    .eq(ActivityAreaScope::getIsDeleted, 0));

            AreaScopeCity updateAreaScopeCity = new AreaScopeCity();
            updateAreaScopeCity.setUT(now);
            updateAreaScopeCity.setIsDeleted(1);
            areaScopeCityMapper.update(updateAreaScopeCity, new LambdaQueryWrapper<AreaScopeCity>()
                    .eq(AreaScopeCity::getActivityId, activityId)
                    .eq(AreaScopeCity::getIsDeleted, 0));

            AreaScopeShop updateAreaScopeShop = new AreaScopeShop();
            updateAreaScopeShop.setUT(now);
            updateAreaScopeShop.setIsDeleted(1);
            areaScopeShopMapper.update(updateAreaScopeShop, new LambdaQueryWrapper<AreaScopeShop>()
                    .eq(AreaScopeShop::getAreaScopeId, activityId)
                    .eq(AreaScopeShop::getIsDeleted, 0));

            ActivityUserScope updateActivityUserScope = new ActivityUserScope();
            updateActivityUserScope.setUT(now);
            updateActivityUserScope.setIsDeleted(1);
            activityUserScopeMapper.update(updateActivityUserScope, new LambdaQueryWrapper<ActivityUserScope>()
                    .eq(ActivityUserScope::getActivityId, activityId)
                    .eq(ActivityUserScope::getIsDeleted, 0));

            UserScopeGroup updateUserScopeGroup = new UserScopeGroup();
            updateUserScopeGroup.setUT(now);
            updateUserScopeGroup.setIsDeleted(1);
            userScopeGroupMapper.update(updateUserScopeGroup, new LambdaQueryWrapper<UserScopeGroup>()
                    .eq(UserScopeGroup::getActivityId, activityId)
                    .eq(UserScopeGroup::getIsDeleted, 0));

            ActivityClass2 updateActivityClass2 = new ActivityClass2();
            updateActivityClass2.setUT(now);
            updateActivityClass2.setIsDeleted(1);
            activityClass2Mapper.update(updateActivityClass2, new LambdaQueryWrapper<ActivityClass2>()
                    .eq(ActivityClass2::getActivityId, activityId)
                    .eq(ActivityClass2::getIsDeleted, 0));

            ActivitySsu updateActivitySsu = new ActivitySsu();
            updateActivitySsu.setUT(now);
            updateActivitySsu.setIsDeleted(1);
            activitySsuMapper.update(updateActivitySsu, new LambdaQueryWrapper<ActivitySsu>()
                    .eq(ActivitySsu::getActivityId, activityId)
                    .eq(ActivitySsu::getIsDeleted, 0));

            PresentActivity updatePresentActivity = new PresentActivity();
            updatePresentActivity.setUT(now);
            updatePresentActivity.setIsDeleted(1);
            presentActivityMapper.update(updatePresentActivity, new LambdaQueryWrapper<PresentActivity>()
                    .eq(PresentActivity::getActivityId, activityId)
                    .eq(PresentActivity::getIsDeleted, 0));

            PresentSsu updatePresentSsu = new PresentSsu();
            updatePresentSsu.setUT(now);
            updatePresentSsu.setIsDeleted(1);
            presentSsuMapper.update(updatePresentSsu, new LambdaQueryWrapper<PresentSsu>()
                    .eq(PresentSsu::getActivityId, activityId)
                    .eq(PresentSsu::getIsDeleted, 0));
            return true;
        }
        throw new BusinessException("活动删除失败，活动状态异常");
    }

    @Override
    public Boolean offLine(ShopAuthorityResult shopAuthorityResult, Long activityId) {
        Integer now = DateUtil.getCurrentTimeIntValue();
        Integer before = DateUtil.getStartTimeStamp(DateUtil.getDateBefore(3));
        LambdaQueryWrapper<AreaScopeShop> areaScopeShopWrapper = new LambdaQueryWrapper<AreaScopeShop>();
        areaScopeShopWrapper.eq(AreaScopeShop::getShopId, shopAuthorityResult.getShopId());
        areaScopeShopWrapper.ge(AreaScopeShop::getCT, before);
        areaScopeShopWrapper.le(AreaScopeShop::getCT, now);
        areaScopeShopWrapper.eq(AreaScopeShop::getIsDeleted, 0);
        List<AreaScopeShop> areaScopeShopList = areaScopeShopMapper.selectList(areaScopeShopWrapper);
        if (!CollectionUtils.isEmpty(areaScopeShopList)) {
            List<Long> activityIds = areaScopeShopList.stream().map(AreaScopeShop::getActivityId).distinct().collect(Collectors.toList());
            List<ActivitySsu> activitySsusList = activitySsuMapper.selectList(new LambdaQueryWrapper<ActivitySsu>()
                    .in(ActivitySsu::getActivityId, activityIds)
                    .eq(ActivitySsu::getSkuId, shopAuthorityResult.getSkuId())
                    .eq(ActivitySsu::getIsDeleted, 0)
                    .eq(ActivitySsu::getExTag, 0)
                    .eq(ActivitySsu::getChannel, ChannelTypeEnums.C_OFFLINE.getCode())
            );
            if (!CollectionUtils.isEmpty(activitySsusList)) {
                activityIds = activitySsusList.stream().map(ActivitySsu::getActivityId).distinct().collect(Collectors.toList());
            }
            activityIds = activityIds.stream().filter(id -> !id.equals(activityId)).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(activityIds)) {
                logger.info("当前门店改价无需要失效的活动");
                return true;
            }
            //更新四天内的有效活动t_promotion_activity失效
            PromotionActivity promotionActivity = new PromotionActivity();
            promotionActivity.setStatus(ActivityStatusTypeEnums.DISABLE.getCode());
            promotionActivity.setUT(now);
            logger.info("需要失效的促销活动id:" + activityIds);
            this.update(promotionActivity, new LambdaQueryWrapper<PromotionActivity>()
                    .in(PromotionActivity::getId, activityIds)
                    .in(PromotionActivity::getStatus, Arrays.asList(ActivityStatusTypeEnums.NOT_START.getCode(), ActivityStatusTypeEnums.STARTED.getCode(),
                            ActivityStatusTypeEnums.STOP.getCode()))
                    .in(PromotionActivity::getCreatorFrom, ImmutableList.of(CreatorFromValueTranslateEnums.SHOP_TO_MID_PLATFORM.getCodeOne(), CreatorFromValueTranslateEnums.SHOP_TO_MID_PLATFORM.getCodeTwo()))
                    .eq(PromotionActivity::getIsDeleted, 0));

            return true;
        }

        return false;
    }

    /**
     * 筛选活动id列表
     *
     * @param shopId               shopId
     * @param promotionActivityIds 已有的活动id列表
     * @return
     */
    @Override
    public List<Long> filterPromotionActivity(Integer shopId, List<Long> promotionActivityIds) {

        if (shopId == null) {
            return new ArrayList<>();
        }

        if (org.apache.commons.collections.CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }
        log.info("请求门店对应的促销活动列表：{} {}", shopId, JSON.toJSONString(promotionActivityIds));

        List<AreaScopeShop> areaScopeShopList = areaScopeShopMapper.selectList(new LambdaQueryWrapper<AreaScopeShop>()
                .eq(AreaScopeShop::getShopId, shopId)
                .in(AreaScopeShop::getActivityId, promotionActivityIds)
                .eq(AreaScopeShop::getIsDeleted, 0));

        if (org.apache.commons.collections.CollectionUtils.isEmpty(areaScopeShopList)) {
            log.info("没有找到门店对应的促销活动列表：{} {}", shopId, JSON.toJSONString(promotionActivityIds));
            return new ArrayList<>();
        }

        return areaScopeShopList.stream().map(AreaScopeShop::getActivityId).distinct().collect(Collectors.toList());
    }

    /**
     * 筛选活动id
     *
     * @param promotionActivityIds 筛选的活动id列表
     * @param onlineType           上线状态
     * @param status               活动状态
     * @param promotionType        促销模块类型
     * @param channelType
     * @return 被筛选后的活动id列表
     */
    @Override
    public List<PromotionActivity> filterPromotionActivity(List<Long> promotionActivityIds, ActivityOnlineTypeEnums onlineType, ActivityStatusTypeEnums status, PromotionTypeEnums promotionType, ChannelTypeEnums channelType) {

        if (org.apache.commons.collections.CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }

        log.info("活动状态筛选活动参数：onlineType={} status={},filterPromotionActivity= {}", onlineType, status, JSON.toJSONString(promotionActivityIds));
        List<PromotionActivity> promotionActivities =
                this.list(new LambdaQueryWrapper<PromotionActivity>()
                        .eq(onlineType != null, PromotionActivity::getIsOnline, onlineType != null ? onlineType.getCode() : null)
                        .eq(PromotionActivity::getIsDeleted, 0)
                        .eq(status != null, PromotionActivity::getStatus, status != null ? status.getCode() : null)
                        .in(PromotionActivity::getId, promotionActivityIds)
                        .eq(promotionType != null, PromotionActivity::getPromotionModuleId, promotionType != null?promotionType.getCode():null)
                        .eq(channelType != null, PromotionActivity::getChannel, channelType != null?channelType.getCode():null)
                );
        if (org.apache.commons.collections.CollectionUtils.isEmpty(promotionActivities)) {
            log.info("没有找到状态对应的活动：onlineType={} status={},filterPromotionActivity= {}", onlineType, status, JSON.toJSONString(promotionActivityIds));
            return new ArrayList<>();
        }

        return promotionActivities;
    }

    @Override
    public List<PromotionActivity> promotionActivities(List<ActivityStatusTypeEnums> status) {
        if (status == null) {
            return new ArrayList<>();
        }

        List<PromotionActivity> promotionActivities = this.list(new LambdaQueryWrapper<PromotionActivity>()
                .eq(PromotionActivity::getIsDeleted, 0)
                .in(PromotionActivity::getStatus, status.stream().map(ActivityStatusTypeEnums::getCode).collect(Collectors.toList()))
        );

        if (CollectionUtils.isEmpty(promotionActivities)) {
            return new ArrayList<>();
        }

        return promotionActivities;
    }
}
