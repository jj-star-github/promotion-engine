package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.ImmutableList;
import com.qmfresh.promotion.bean.price.ModifyVipPriceBean;
import com.qmfresh.promotion.bean.price.ModifyVipPriceResult;
import com.qmfresh.promotion.bean.promotion.QueryActivityParam;
import com.qmfresh.promotion.bean.sku.*;
import com.qmfresh.promotion.cache.service.*;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.common.utils.promotion.PromotionCheckUtil;
import com.qmfresh.promotion.dto.*;
import com.qmfresh.promotion.dto.qmpp.QmppSkuDto;
import com.qmfresh.promotion.entity.*;
import com.qmfresh.promotion.enums.*;
import com.qmfresh.promotion.external.service.ICmallExternalApiService;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.manager.*;
import com.qmfresh.promotion.mapper.ActivitySsuMapper;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.strategy.bean.CpRule;
import com.qmfresh.promotion.strategy.bean.DiscountRule;
import com.qmfresh.promotion.strategy.bean.VipRule;
import com.qmfresh.promotion.strategy.helper.DiscountPriceHelper;
import com.qmfresh.promotion.strategy.helper.RuleDescriptionHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/6/5.
 *
 * @author wyh
 */
@Component
@Slf4j
public class PromotionManagerImpl implements IPromotionManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(PromotionManagerImpl.class);

    @Resource
    private IPromotionActivityManager promotionActivityManager;

    @Resource
    private IPromotionActivityTimeManager promotionActivityTimeManager;

    @Resource
    private IActivityUserScopeManager activityUserScopeManager;

    @Resource
    private IUserScopeGroupManager userScopeGroupManager;

    @Resource
    private IActivityAreaScopeManager activityAreaScopeManager;

    @Resource
    private IAreaScopeCityManager areaScopeCityManager;

    @Resource
    private IAreaScopeShopManager areaScopeShopManager;

    @Resource
    private IActivityClass2Manager activityClass2Manager;

    @Resource
    private IActivitySsuManager activitySsuManager;

    @Resource
    private IPresentActivityManager presentActivityManager;

    @Resource
    private IPromotionRuleManager promotionRuleManager;

    @Resource
    private IQmExternalApiService qmExternalApiService;

    @Resource
    private ICmallExternalApiService cmallExternalApiService;

    @Resource
    private ActivitySsuMapper activitySsuMapper;

    @Resource
    private PromotionCheckUtil promotionCheckUtil;

    @Resource
    private LCPromotionActivityService lcPromotionActivityService;
    @Resource
    private LCPromotionActivityTimeService lcPromotionActivityTimeService;
    @Resource
    private LCPromotionRuleService lcPromotionRuleService;
    @Resource
    private LCActivityAreaScopeService lcActivityAreaScopeService;
    @Resource
    private LCAreaScopeCityService lcAreaScopeCityService;
    @Resource
    private LCAreaScopeShopService lcAreaScopeShopService;
    @Resource
    private LCActivityClass2Service lcActivityClass2Service;
    @Resource
    private LCActivitySsuService lcActivitySsuService;
    @Resource
    private LCActivityUserScopeService lcActivityUserScopeService;
    @Resource
    private LCUserScopeGroupService lcUserScopeGroupService;
    @Resource
    private LCPresentActivityService lcPresentActivityService;
    @Resource
    private LCPresentSsuService lcPresentSsuService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteActivity(long activityId) {
        PromotionActivity promotionActivity = promotionActivityManager.getById(activityId);
        if (!promotionActivity.getStatus().equals(ActivityStatusTypeEnums.NOT_START.getCode()) &&
                !promotionActivity.getStatus().equals(ActivityStatusTypeEnums.STOP.getCode())) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("当前活动状态不允许修改");
        }

        return this.deleteActivityAllInfo(activityId);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteActivityAllInfo(Long activityId) {
        int now = DateUtil.getCurrentTimeIntValue();
        PromotionActivity uPa = new PromotionActivity();
        uPa.setId(activityId);
        uPa.setUT(now);
        uPa.setIsDeleted(1);
        promotionActivityManager.updateById(uPa);

        PromotionActivityTime uPat = new PromotionActivityTime();
        uPat.setUT(now);
        uPat.setIsDeleted(1);
        promotionActivityTimeManager.update(uPat, new LambdaQueryWrapper<PromotionActivityTime>()
                .eq(PromotionActivityTime::getActivityId, activityId)
                .eq(PromotionActivityTime::getIsDeleted, 0));

        PromotionRule upr = new PromotionRule();
        upr.setUT(now);
        upr.setIsDeleted(1);
        promotionRuleManager.update(upr, new LambdaQueryWrapper<PromotionRule>()
                .eq(PromotionRule::getActivityId, activityId)
                .eq(PromotionRule::getIsDeleted, 0));

        ActivityAreaScope uaas = new ActivityAreaScope();
        uaas.setUT(now);
        uaas.setIsDeleted(1);
        activityAreaScopeManager.update(uaas, new LambdaQueryWrapper<ActivityAreaScope>()
                .eq(ActivityAreaScope::getActivityId, activityId)
                .eq(ActivityAreaScope::getIsDeleted, 0));

        AreaScopeShop uass = new AreaScopeShop();
        uass.setUT(now);
        uass.setIsDeleted(1);
        areaScopeShopManager.update(uass, new LambdaQueryWrapper<AreaScopeShop>()
                .eq(AreaScopeShop::getActivityId, activityId)
                .eq(AreaScopeShop::getIsDeleted, 0));

        ActivityClass2 uac2 = new ActivityClass2();
        uac2.setUT(now);
        uac2.setIsDeleted(1);
        activityClass2Manager.update(uac2, new LambdaQueryWrapper<ActivityClass2>()
                .eq(ActivityClass2::getActivityId, activityId)
                .eq(ActivityClass2::getIsDeleted, 0));

        ActivitySsu uas = new ActivitySsu();
        uas.setUT(now);
        uas.setIsDeleted(1);
        activitySsuManager.update(uas, new LambdaQueryWrapper<ActivitySsu>()
                .eq(ActivitySsu::getActivityId, activityId)
                .eq(ActivitySsu::getIsDeleted, 0));

        ActivityUserScope uaus = new ActivityUserScope();
        uaus.setUT(now);
        uaus.setIsDeleted(1);
        activityUserScopeManager.update(uaus, new LambdaQueryWrapper<ActivityUserScope>()
                .eq(ActivityUserScope::getActivityId, activityId)
                .eq(ActivityUserScope::getIsDeleted, 0));

        UserScopeGroup uusg = new UserScopeGroup();
        uusg.setUT(now);
        uusg.setIsDeleted(1);
        userScopeGroupManager.update(uusg, new LambdaQueryWrapper<UserScopeGroup>()
                .eq(UserScopeGroup::getActivityId, activityId)
                .eq(UserScopeGroup::getIsDeleted, 0));

        PresentActivity upa = new PresentActivity();
        upa.setUT(now);
        upa.setActivityId(null);
        presentActivityManager.update(upa, new LambdaQueryWrapper<PresentActivity>()
                .eq(PresentActivity::getActivityId, activityId)
                .eq(PresentActivity::getIsDeleted, 0));

        lcPromotionActivityService.getCache().invalidate(activityId);
        LOGGER.info("删除活动Id为：{}", activityId);
        return true;
    }

    @Override
    public PromotionDto getPromotionDetail(Long activityId) {
        PromotionDto promotionDto = new PromotionDto();
        promotionDto.setPromotionActivity(lcPromotionActivityService.get(activityId));
        promotionDto.setPromotionActivityTimes(lcPromotionActivityTimeService.get(activityId));
        promotionDto.setPromotionRule(lcPromotionRuleService.get(activityId));
        ActivityAreaScope activityAreaScope = lcActivityAreaScopeService.get(activityId);
        promotionDto.setActivityAreaScope(activityAreaScope);
        promotionDto.setAreaScopeCities(lcAreaScopeCityService.get(activityAreaScope.getId()));
        promotionDto.setAreaScopeShops(lcAreaScopeShopService.get(activityAreaScope.getId()));
        promotionDto.setActivityClass2s(lcActivityClass2Service.get(activityId));
        promotionDto.setActivitySsus(lcActivitySsuService.get(activityId));
        ActivityUserScope activityUserScope = lcActivityUserScopeService.get(activityId);
        promotionDto.setActivityUserScope(activityUserScope);
        promotionDto.setUserScopeGroups(lcUserScopeGroupService.get(activityId));
        List<PresentActivity> presentActivities = lcPresentActivityService.get(activityId);
        List<PresentSsu> presentSsus = new ArrayList<>(16);
        presentActivities.forEach(
                item -> presentSsus.addAll(lcPresentSsuService.get(item.getId()))
        );
        promotionDto.setPresentSsus(presentSsus);
        return promotionDto;
    }

    @Override
    public List<PromotionActivity> checkActivityTime(Integer channel, PromotionTypeEnums promotionEnmu,
                                                     List<PromotionActivityTime> activityTimes) {
        QueryActivityParam queryActivityParam = new QueryActivityParam();
        queryActivityParam.setModuleId(promotionEnmu.getCode());
        queryActivityParam.setStatusList(Arrays.asList(ActivityStatusTypeEnums.NOT_START.getCode(), ActivityStatusTypeEnums.STARTED.getCode(),
                ActivityStatusTypeEnums.STOP.getCode()));
        queryActivityParam.setChannel(channel);
        List<PromotionActivity> paList = promotionActivityManager.queryByCondition(queryActivityParam);
        if (ListUtil.isNullOrEmpty(paList)) {
            return null;
        }

        List<PromotionActivityTime> patList = promotionActivityTimeManager.promotionActivityTimes(paList.stream().map(PromotionActivity::getId).collect(Collectors.toList()));
        Map<Long, List<PromotionActivityTime>> activityMap = patList.stream().collect(Collectors.groupingBy(PromotionActivityTime::getActivityId));

        List<PromotionActivity> filterPaList = new ArrayList<>(16);
        for (PromotionActivity pa : paList) {
            List<PromotionActivityTime> actList = activityMap.get(pa.getId());
            if (this.haveIntersect(actList, activityTimes)) {
                filterPaList.add(pa);
            }
        }
        return filterPaList;
    }

    @Override
    public List<PromotionActivity> checkActivityTime(Integer channel, PromotionTypeEnums promotionEnmu, List<PromotionActivityTime> activityTimes, Integer creatorFrom) {
        QueryActivityParam queryActivityParam = new QueryActivityParam();
        queryActivityParam.setModuleId(promotionEnmu.getCode());
        queryActivityParam.setStatusList(Arrays.asList(ActivityStatusTypeEnums.NOT_START.getCode(), ActivityStatusTypeEnums.STARTED.getCode(),
                ActivityStatusTypeEnums.STOP.getCode()));
        queryActivityParam.setChannel(channel);
        if (creatorFrom != null) {
            queryActivityParam.setCreatorFrom(creatorFrom);
        }
        List<PromotionActivity> paList = promotionActivityManager.queryByCondition(queryActivityParam);
        if (ListUtil.isNullOrEmpty(paList)) {
            return null;
        }

        List<PromotionActivityTime> patList = promotionActivityTimeManager.promotionActivityTimes(paList.stream().map(PromotionActivity::getId).collect(Collectors.toList()));
        Map<Long, List<PromotionActivityTime>> activityMap = patList.stream().collect(Collectors.groupingBy(PromotionActivityTime::getActivityId));

        List<PromotionActivity> filterPaList = new ArrayList<>(16);
        for (PromotionActivity pa : paList) {
            List<PromotionActivityTime> actList = activityMap.get(pa.getId());
            if (this.haveIntersect(actList, activityTimes)) {
                filterPaList.add(pa);
            }
        }
        return filterPaList;
    }

    private boolean haveIntersect(List<PromotionActivityTime> existList, List<PromotionActivityTime> newList) {
        for (PromotionActivityTime pat : newList) {
            if (this.haveIntersect(existList, pat)) {
                return true;
            }
        }
        return false;
    }

    private boolean haveIntersect(List<PromotionActivityTime> c1List, PromotionActivityTime c2) {
        for (PromotionActivityTime pat : c1List) {
            if (this.haveIntersect(pat, c2)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 验证促销时间是否有交集
     *
     * @param c1
     * @param c2
     * @return 有交集返回true
     */
    private boolean haveIntersect(PromotionActivityTime c1, PromotionActivityTime c2) {
        if (c2.getEffectBeginTime().compareTo(c1.getEffectEndTime()) > 0 || c1.getEffectBeginTime().compareTo(c2.getEffectEndTime()) > 0) {
            return false;
        } else {
            if (StringUtils.isBlank(c1.getWeekDay()) || StringUtils.isBlank(c2.getWeekDay())) {
                return true;
            }
            List<String> retainList = ListUtil.stringToList(c1.getWeekDay());
            retainList.retainAll(ListUtil.stringToList(c2.getWeekDay()));
            return !ListUtil.isNullOrEmpty(retainList);
        }
    }

    @Override
    public List<PromotionActivity> checkActivityTime(Integer channel, PromotionTypeEnums promotionEnmu,
                                                     Integer beginTime,
                                                     Integer endTime) {
        QueryActivityParam queryActivityParam = new QueryActivityParam();
        queryActivityParam.setModuleId(promotionEnmu.getCode());
        queryActivityParam.setStatus(1);
        queryActivityParam.setEffectBeginTime(beginTime);
        queryActivityParam.setEffectEndTime(endTime);
        queryActivityParam.setChannel(channel);
        List<PromotionActivity> paList = promotionActivityManager.queryByCondition(queryActivityParam);
        if (ListUtil.isNullOrEmpty(paList)) {
            return null;
        }

        List<PromotionActivityTime> patList = promotionActivityTimeManager.promotionActivityTimes(paList.stream().map(PromotionActivity::getId).collect(Collectors.toList()));
        Map<Long, List<PromotionActivityTime>> activityMap = patList.stream().collect(Collectors.groupingBy(PromotionActivityTime::getActivityId));

        List<PromotionActivity> filterPaList = new ArrayList<>(16);
        PromotionActivityTime pat = new PromotionActivityTime();
        pat.setEffectBeginTime(beginTime);
        pat.setEffectEndTime(endTime);
        for (PromotionActivity pa : paList) {
            List<PromotionActivityTime> actList = activityMap.get(pa.getId());
            if (this.haveIntersect(actList, pat)) {
                filterPaList.add(pa);
            }
        }
        return filterPaList;
    }

    @Override
    public List<PromotionActivity> checkActivityUser(List<PromotionActivity> promotionActivities,
                                                     List<Integer> crowdTypeList) {
        List<Integer> crowdTypes = promotionCheckUtil.mapCrowdType(crowdTypeList);
        List<ActivityUserScope> ausList = activityUserScopeManager.activityUserScope(promotionActivities.stream().map(PromotionActivity::getId).collect(Collectors.toList()));
        if (ListUtil.isNullOrEmpty(ausList)) {
            return null;
        }
        List<UserScopeGroup> usgList = userScopeGroupManager.list(new LambdaQueryWrapper<UserScopeGroup>()
                .in(UserScopeGroup::getUserScopeId, ausList.stream().map(ActivityUserScope::getId).collect(Collectors.toList()))
                .eq(UserScopeGroup::getIsDeleted, 0));
        List<Long> userScopeIds = usgList.stream().filter(usg -> (crowdTypes.contains(usg.getUserType()) || usg.getUserType() == CrowdTypeEnums.ALL.getCode())).map(UserScopeGroup::getUserScopeId).collect(Collectors.toList());
        if (ListUtil.isNullOrEmpty(userScopeIds)) {
            return null;
        }

        List<Long> activityIds = ausList.stream().filter(aus -> userScopeIds.contains(aus.getId())).map(ActivityUserScope::getActivityId).distinct().collect(Collectors.toList());
        return promotionActivities.stream().filter(item -> activityIds.contains(item.getId())).collect(Collectors.toList());
    }

    @Override
    public List<Long> checkActivityShop(List<PromotionActivity> promotionActivities, List<PromotionAreaDto> areaDtos) {
        List<Long> activityIds = new ArrayList<>(16);
        List<ActivityAreaScope> activityAreaScopes = activityAreaScopeManager.activityAreaScope(promotionActivities.stream().map(PromotionActivity::getId).collect(Collectors.toList()));
        List<AreaScopeShop> areaScopeShops = areaScopeShopManager.areaScope(activityAreaScopes.stream().map(ActivityAreaScope::getId).collect(Collectors.toList()));
        if (ListUtil.isNullOrEmpty(areaScopeShops)) {
            return activityIds;
        }
        List<Integer> shopIds = new ArrayList<>(16);
        areaDtos.forEach(item -> shopIds.addAll(item.getShopIds()));
        List<Long> areaScopeIds =
                areaScopeShops.stream().filter(item -> shopIds.contains(item.getShopId())).map(AreaScopeShop::getAreaScopeId).distinct().collect(Collectors.toList());
        if (!ListUtil.isNullOrEmpty(areaScopeIds)) {
            activityIds = activityAreaScopes.stream().filter(item -> areaScopeIds.contains(item.getId())).map(ActivityAreaScope::getActivityId).collect(Collectors.toList());
        }
        return activityIds;
    }

    @Override
    public List<Long> checkActivityArea(List<PromotionActivity> promotionActivities, List<PromotionAreaDto> areaDtos) {

        List<ActivityAreaScope> activityAreaScopes = activityAreaScopeManager.activityAreaScope(promotionActivities.stream().map(PromotionActivity::getId).collect(Collectors.toList()));

        List<AreaScopeCity> areaScopeCities = areaScopeCityManager.areaScopeCity(activityAreaScopes.stream().map(ActivityAreaScope::getId).collect(Collectors.toList()));

        List<AreaScopeShop> areaScopeShops = areaScopeShopManager
                .areaScope(activityAreaScopes.stream().map(ActivityAreaScope::getId).collect(Collectors.toList()));


        Set<Long> activityIds = new HashSet<>(16);
        areaDtos.forEach(
                area -> {
                    List<AreaScopeCity> filterCities = areaScopeCities.stream().filter(item -> item.getCityId().equals(area.getCityId())).collect(Collectors.toList());
                    if (!ListUtil.isNullOrEmpty(areaScopeShops) && !ListUtil.isNullOrEmpty(filterCities)) {
                        List<Long> areaScopeIds =
                                areaScopeShops.stream().filter(item -> area.getShopIds().contains(item.getShopId())).map(AreaScopeShop::getAreaScopeId).collect(Collectors.toList());
                        if (!ListUtil.isNullOrEmpty(areaScopeIds)) {
                            activityIds.addAll(activityAreaScopes.stream().filter(item -> areaScopeIds.contains(item.getId())).map(ActivityAreaScope::getActivityId).collect(Collectors.toList()));
                        }
                    }
                }
        );
        return new ArrayList<>(activityIds);
    }


    @Override
    public void checkProductClassDto(List<Long> activityIds, PromotionClassDto promotionClassDto) {
        List<ActivityClass2> activityClass2s = activityClass2Manager.activityClass2(activityIds);

        // 已存在分类不为空，新建分类不能为全部
        if (!ListUtil.isNullOrEmpty(activityClass2s) && promotionClassDto.getClass2Ids().contains(-1)) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("已存在分类不为空，新建分类不能为全部，冲突活动ID：" + activityClass2s.get(0).getActivityId());
        }

        // 已存在选择全部分类促销活动
        Optional<ActivityClass2> optionalActivityClass2 = activityClass2s.stream().filter(item -> item.getClassId() == -1).findFirst();
        if (optionalActivityClass2.isPresent()) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("创建促销失败，所有分类已设置促销，冲突活动ID：" + optionalActivityClass2.get().getActivityId());
        }

        // 存在相同分类促销活动
        List<ActivityClass2> filterClass2 = activityClass2s.stream().filter(item -> promotionClassDto.getClass2Ids().contains(item.getClassId())).collect(Collectors.toList());
        if (!ListUtil.isNullOrEmpty(filterClass2)) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("创建促销失败，当前分类已设置促销，冲突活动ID:" + filterClass2.get(0).getActivityId());
        }

        List<ActivitySsu> activitySsus = activitySsuManager.activitySsu(activityIds, SsuTagTypeEnums.CONTAIN);
        if (!ListUtil.isNullOrEmpty(activitySsus)) {
            List<ActivitySsu> filterActivitySsu = activitySsus.stream().
                    filter(item -> promotionClassDto.getClass2Ids().contains(item.getClass2Id()) && (ListUtil.isNullOrEmpty(promotionClassDto.getExSsuFormatIds()) || !promotionClassDto.getExSsuFormatIds().contains(item.getSsuFormatId()))).
                    collect(Collectors.toList());
            if (!ListUtil.isNullOrEmpty(filterActivitySsu)) {
                throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("创建促销失败，当前分类下部分商品已设置促销，冲突活动ID:" + filterActivitySsu.get(0).getActivityId() + "。\n冲突商品：" + String.join(",",
                        filterActivitySsu.stream().map(ActivitySsu::getSsuName).collect(Collectors.toList())));
            }
        }
    }

    @Override
    public void checkProduct(List<Long> activityIds, List<ProductSsuFormatDto> productSsuFormatDtos) {
        List<ActivitySsu> activitySsus = activitySsuManager.activitySsu(activityIds);

        Map<Integer, List<ActivitySsu>> tagSsuMap = activitySsus.stream().collect(Collectors.groupingBy(ActivitySsu::getExTag));

        List<Integer> ssuFormatIds = null;
        if (!ListUtil.isNullOrEmpty(tagSsuMap.get(SsuTagTypeEnums.CONTAIN.getCode()))) {
            ssuFormatIds = tagSsuMap.get(SsuTagTypeEnums.CONTAIN.getCode()).stream().map(ActivitySsu::getSsuFormatId).distinct().collect(Collectors.toList());
        }

        // 校验商品是否已设置促销
        if (!ListUtil.isNullOrEmpty(ssuFormatIds)) {
            for (ProductSsuFormatDto psf : productSsuFormatDtos) {
                if (ssuFormatIds.contains(psf.getSsuFormatId())) {
                    log.warn("当前商品已设置促销 ssuName={} ssufp={}", psf.getSsuName(), psf.getSsuFormat());
                    throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("创建促销失败，当前商品已设置促销，ssu:" + psf.getSsuName() + ",规格ssufp:" + psf.getSsuFormat());
                }
            }
        }

        List<ActivitySsu> exActivitySsu = null;
        if (!ListUtil.isNullOrEmpty(tagSsuMap.get(SsuTagTypeEnums.EXCLUSIVE.getCode()))) {
            exActivitySsu = tagSsuMap.get(SsuTagTypeEnums.EXCLUSIVE.getCode());
        }

        // 校验分类是否已设置促销（考虑分类剔除商品）
        List<ActivityClass2> activityClass2s = activityClass2Manager.activityClass2(activityIds);
        List<Integer> exSsuFormatIds = null;

        if (!ListUtil.isNullOrEmpty(activityClass2s)) {
            for (ProductSsuFormatDto psf : productSsuFormatDtos) {
                List<ActivityClass2> filterAcList = activityClass2s.stream().filter(ac -> ac.getClassId().equals(psf.getClass2Id())).collect(Collectors.toList());
                if (!ListUtil.isNullOrEmpty(filterAcList)) {
                    if (!ListUtil.isNullOrEmpty(exActivitySsu)) {
                        exSsuFormatIds =
                                exActivitySsu.stream().filter(item -> psf.getClass2Id().equals(item.getClass2Id())).map(ActivitySsu::getSsuFormatId).collect(Collectors.toList());
                        if (exSsuFormatIds.contains(psf.getSsuFormatId())) {
                            continue;
                        }
                    }
                    throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("创建促销失败，当前商品所在分类已设置促销,class2:" + psf.getSku().getClass2Name());
                }
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PromotionActivity createPromotionActivity(PromotionTypeEnums promotionType, int channel, String name,
                                                     Long userId, String userName, Integer creatorFrom) {
        int now = DateUtil.getCurrentTimeIntValue();
        PromotionActivity promotionActivity = new PromotionActivity();
        promotionActivity.setPromotionModuleId(promotionType.getCode());
        promotionActivity.setChannel(channel);
        promotionActivity.setActivityName(name);
        promotionActivity.setContents("");
        promotionActivity.setIsOnline(1);
        promotionActivity.setOnlineTime(0);
        promotionActivity.setOfflineTime(0);
        promotionActivity.setCreateUserId(userId);
        promotionActivity.setCreateUserName(userName);
        promotionActivity.setCreatorFrom(creatorFrom);
        promotionActivity.setStatus(ActivityStatusTypeEnums.NOT_START.getCode());
        promotionActivity.setCT(now);
        promotionActivity.setUT(now);
        promotionActivity.setIsDeleted(0);
        promotionActivityManager.save(promotionActivity);

        return promotionActivity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createPromotionActivityTime(PromotionTypeEnums promotionType, PromotionActivity promotionActivity,
                                            int beginTime, int endTime) {
        int now = DateUtil.getCurrentTimeIntValue();

        PromotionActivityTime promotionActivityTime = new PromotionActivityTime();
        promotionActivityTime.setEffectBeginTime(beginTime);
        promotionActivityTime.setEffectEndTime(endTime);
        promotionActivityTime.setActivityId(promotionActivity.getId());
        promotionActivityTime.setPromotionModuleId(promotionType.getCode());
        promotionActivityTime.setWeekDay("");
        promotionActivityTime.setCT(now);
        promotionActivityTime.setUT(now);
        promotionActivityTime.setIsDeleted(0);
        promotionActivityTimeManager.save(promotionActivityTime);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createPromotionActivityTime(PromotionTypeEnums promotionType, PromotionActivity promotionActivity,
                                            List<PromotionActivityTime> activityTimes) {
        int now = DateUtil.getCurrentTimeIntValue();

        List<PromotionActivityTime> promotionActivityTimes = new ArrayList<>(activityTimes.size());
        activityTimes.forEach(
                item -> {
                    PromotionActivityTime promotionActivityTime = new PromotionActivityTime();
                    promotionActivityTime.setEffectBeginTime(item.getEffectBeginTime());
                    promotionActivityTime.setEffectEndTime(item.getEffectEndTime());
                    promotionActivityTime.setActivityId(promotionActivity.getId());
                    promotionActivityTime.setPromotionModuleId(promotionType.getCode());
                    if (item.getWeekDay() != null && !item.getWeekDay().isEmpty()) {
                        promotionActivityTime.setWeekDay(item.getWeekDay());
                    } else {
                        promotionActivityTime.setWeekDay("");
                    }
                    promotionActivityTime.setCT(now);
                    promotionActivityTime.setUT(now);
                    promotionActivityTime.setIsDeleted(0);
                    promotionActivityTimes.add(promotionActivityTime);
                }
        );
        promotionActivityTimeManager.saveBatch(promotionActivityTimes);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createPromotionUser(PromotionActivity promotionActivity, List<Integer> crowdTypes) {
        //        List<ActivityUserScope> activityUserScopes = new ArrayList<>(crowdTypes.size());
        int now = DateUtil.getCurrentTimeIntValue();
        ActivityUserScope activityUserScope = new ActivityUserScope();
        activityUserScope.setActivityId(promotionActivity.getId());
        activityUserScope.setCT(now);
        activityUserScope.setUT(now);
        activityUserScope.setIsDeleted(0);
        activityUserScopeManager.save(activityUserScope);

        List<UserScopeGroup> userScopeGroups = new ArrayList<>(crowdTypes.size());
        crowdTypes.forEach(
                typeCode -> {
                    UserScopeGroup userScopeGroup = new UserScopeGroup();
                    userScopeGroup.setUserScopeId(activityUserScope.getId());
                    userScopeGroup.setActivityId(promotionActivity.getId());
                    userScopeGroup.setName(CrowdTypeEnums.getName(typeCode));
                    userScopeGroup.setUserType(typeCode);
                    userScopeGroup.setCT(now);
                    userScopeGroup.setUT(now);
                    userScopeGroup.setIsDeleted(0);
                    userScopeGroups.add(userScopeGroup);
                }
        );
        userScopeGroupManager.saveBatch(userScopeGroups);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createPromotionShop(PromotionActivity promotionActivity, List<PromotionAreaDto> areaDtos) {
        int now = DateUtil.getCurrentTimeIntValue();
        ActivityAreaScope activityAreaScope = this.createPromotionScope(promotionActivity.getId());
        List<AreaScopeShop> areaScopeShops = new ArrayList<>(16);
        areaDtos.forEach(
                area -> area.getShopIds().forEach(
                        shopId -> {
                            AreaScopeShop areaScopeShop = new AreaScopeShop();
                            areaScopeShop.setActivityId(promotionActivity.getId());
                            areaScopeShop.setAreaScopeId(activityAreaScope.getId());
                            areaScopeShop.setCityId(area.getCityId());
                            areaScopeShop.setShopId(shopId);
                            areaScopeShop.setCT(now);
                            areaScopeShop.setUT(now);
                            areaScopeShop.setIsDeleted(0);
                            areaScopeShops.add(areaScopeShop);
                        }
                )
        );
        areaScopeShopManager.saveBatch(areaScopeShops);
    }

    @Transactional(rollbackFor = Exception.class)
    public ActivityAreaScope createPromotionScope(long activityId) {
        int now = DateUtil.getCurrentTimeIntValue();
        ActivityAreaScope activityAreaScope = new ActivityAreaScope();
        activityAreaScope.setActivityId(activityId);
        activityAreaScope.setCT(now);
        activityAreaScope.setUT(now);
        activityAreaScope.setIsDeleted(0);
        activityAreaScopeManager.save(activityAreaScope);
        return activityAreaScope;
    }

    @Override
    public void createPromotionProduct(long activityId, long ruleId, int channel, PromotionProductDto productDto) {
        if (productDto.getType().equals(ProductScopeTypeEnums.CLASS2.getCode())) {
            this.createPromotionClass(activityId, ruleId, channel, productDto.getPromotionClassDto());
        }
        if (productDto.getType().equals(ProductScopeTypeEnums.SSU.getCode())) {
            this.createPromotionSsu(activityId, ruleId, channel, SsuTagTypeEnums.CONTAIN, productDto.getSsuFormatIds());
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createPromotionClass(long activityId, long ruleId, int channel, PromotionClassDto promotionClassDto) {
        int now = DateUtil.getCurrentTimeIntValue();

        if (!ListUtil.isNullOrEmpty(promotionClassDto.getExSsuFormatIds())) {
            this.createPromotionSsu(activityId, ruleId, channel, SsuTagTypeEnums.EXCLUSIVE, promotionClassDto.getExSsuFormatIds());
        }

        List<ActivityClass2> insertList = new ArrayList<>(promotionClassDto.getClass2Ids().size());
        for (Integer class2Id : promotionClassDto.getClass2Ids()) {
            ActivityClass2 activityClass2 = new ActivityClass2();
            activityClass2.setActivityId(activityId);
            activityClass2.setRuleId(ruleId);
            activityClass2.setClassId(class2Id);
            activityClass2.setCT(now);
            activityClass2.setUT(now);
            activityClass2.setIsDeleted(0);
            insertList.add(activityClass2);
        }
        activityClass2Manager.saveBatch(insertList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createPromotionSsu(long activityId, long ruleId, int channel, SsuTagTypeEnums ssuTagType, List<Integer> ssuFormatIds) {
        int now = DateUtil.getCurrentTimeIntValue();

        List<ActivitySsu> insertList = new ArrayList<>(ssuFormatIds.size());
        if (channel == ChannelTypeEnums.C_OFFLINE.getCode()) {
            QueryProductSsuFormat query = new QueryProductSsuFormat();
            query.setSsuFormatIds(ssuFormatIds);
            query.setSellChannel(2);
            query.setPlatform(2);
            query.setSsuFp(1);
            query.setNeedSku(true);
            List<ProductSsuFormatDto> productSsuFormatDtos = qmExternalApiService.getSsuFormatByCondition(query);
            productSsuFormatDtos.forEach(
                    psf -> {
                        ActivitySsu activitySsu = new ActivitySsu();
                        activitySsu.setActivityId(activityId);
                        activitySsu.setSsuFormatId(psf.getSsuFormatId());
                        activitySsu.setSsuName(psf.getSsuName());
                        activitySsu.setRuleId(ruleId);
                        activitySsu.setClass1Id(psf.getSku().getClass1Id());
                        activitySsu.setClass2Id(psf.getSku().getClass2Id());
                        activitySsu.setClass3Id(psf.getSku().getClass3Id());
                        activitySsu.setSkuId(psf.getSkuId());
                        activitySsu.setChannel(channel);
                        activitySsu.setExTag(ssuTagType.getCode());
                        activitySsu.setCT(now);
                        activitySsu.setUT(now);
                        activitySsu.setIsDeleted(0);
                        insertList.add(activitySsu);
                    }
            );
        }

        if (channel == ChannelTypeEnums.C_ONLINE.getCode()) {
            SsuQuery ssuQuery = new SsuQuery();
            ssuQuery.setSsuFormatIds(ssuFormatIds);
            List<Ssu> ssuList = cmallExternalApiService.querySsu(ssuQuery);
            ssuList.forEach(
                    ssu -> {
                        ActivitySsu activitySsu = new ActivitySsu();
                        activitySsu.setActivityId(activityId);
                        activitySsu.setSsuFormatId(ssu.getSsuFormatId());
                        activitySsu.setSsuName(ssu.getSsuName());
                        activitySsu.setRuleId(ruleId);
                        activitySsu.setClass1Id(ssu.getClass1Id());
                        activitySsu.setClass2Id(ssu.getClass2Id());
                        activitySsu.setClass3Id(0);
                        activitySsu.setSkuId(ssu.getSkuId());
                        activitySsu.setChannel(channel);
                        activitySsu.setExTag(ssuTagType.getCode());
                        activitySsu.setCT(now);
                        activitySsu.setUT(now);
                        activitySsu.setIsDeleted(0);
                        insertList.add(activitySsu);
                    }
            );
        }
        activitySsuMapper.batchInsertActivitySsu(insertList);
//        activitySsuManager.insertBatch(insertList);
    }

    /**
     * 创建改价商品
     *
     * @param activityId 活动id
     * @param ruleId     规则id
     * @param channel    渠道
     * @param ssuTagType 排除标识：0.包含、1.排除
     * @param productDto 促销商品实体
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createCpPromotionSsu(long activityId, long ruleId, int channel, SsuTagTypeEnums ssuTagType, PromotionProductDto productDto) {
        if (CollectionUtils.isEmpty(productDto.getSkuRules())) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("改价规则不能为空");
        }
        Map<Integer, BigDecimal> skuPromotionMap = productDto.getSkuRules().stream().collect(Collectors.toMap(CpRule::getSkuId, CpRule::getPromotionPrice));

        int now = DateUtil.getCurrentTimeIntValue();
        List<ActivitySsu> insertList = new ArrayList<>(productDto.getSsuFormatIds().size());
        if (channel == ChannelTypeEnums.C_OFFLINE.getCode()) {
            QueryProductSsuFormat query = new QueryProductSsuFormat();
            query.setSsuFormatIds(productDto.getSsuFormatIds());
            query.setSellChannel(2);
            query.setPlatform(2);
            query.setSsuFp(1);
            query.setNeedSku(true);
            List<ProductSsuFormatDto> productSsuFormatDtos = qmExternalApiService.getSsuFormatByCondition(query);

            productSsuFormatDtos.forEach(
                    psf -> {
                        ActivitySsu activitySsu = new ActivitySsu();
                        activitySsu.setActivityId(activityId);
                        activitySsu.setSsuFormatId(psf.getSsuFormatId());
                        activitySsu.setSsuName(psf.getSsuName());
                        activitySsu.setRuleId(ruleId);
                        activitySsu.setClass1Id(psf.getSku().getClass1Id());
                        activitySsu.setClass2Id(psf.getSku().getClass2Id());
                        activitySsu.setClass3Id(psf.getSku().getClass3Id());
                        activitySsu.setSkuId(psf.getSkuId());
                        if (skuPromotionMap != null) {
                            activitySsu.setPromotionPrice(skuPromotionMap.get(psf.getSkuId()));
                        }
                        activitySsu.setChannel(channel);
                        activitySsu.setExTag(ssuTagType.getCode());
                        activitySsu.setCT(now);
                        activitySsu.setUT(now);
                        activitySsu.setIsDeleted(0);
                        insertList.add(activitySsu);
                    }
            );
        }
        activitySsuMapper.batchInsertActivitySsu(insertList);
//        activitySsuManager.insertBatch(insertList);

    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public PromotionRule createPromotionRule(PromotionTypeEnums promotionType, PromotionActivity promotionActivity,
                                             AreaScopeTypeEnums areaScopeType, int productScopeType, Object rule) {
        int now = DateUtil.getCurrentTimeIntValue();
        PromotionRule promotionRule = new PromotionRule();
        promotionRule.setActivityId(promotionActivity.getId());
        promotionRule.setPromotionModuleId(promotionType.getCode());
        promotionRule.setIsOnline(1);
        promotionRule.setAreaScope(areaScopeType.getCode());
        promotionRule.setProductScope(productScopeType);
        promotionRule.setDescript(RuleDescriptionHelper.getDescription(promotionType, rule));
        promotionRule.setPromotionRule(JSON.toJSONString(rule));
        promotionRule.setCT(now);
        promotionRule.setUT(now);
        promotionRule.setIsDeleted(0);
        promotionRuleManager.save(promotionRule);
        return promotionRule;
    }

    @Override
    public ModifyVipPriceResult modifyVipPrice(ModifyVipPriceBean modifyVipPriceBean, ProductSsu productSsu) {
        ModifyVipPriceResult modifyVipPriceResult = new ModifyVipPriceResult();
        modifyVipPriceResult.setPriceId(modifyVipPriceBean.getPriceId());
        modifyVipPriceResult.setPrice(modifyVipPriceBean.getPrice());
        modifyVipPriceResult.setModifyPrice(BigDecimal.ZERO);
        modifyVipPriceResult.setDescription("无会员促销");

        QueryActivityParam queryActivityParam = new QueryActivityParam();
        queryActivityParam.setModuleId(PromotionTypeEnums.VIP.getCode());
        queryActivityParam.setChannel(3);
        queryActivityParam.setEffectBeginTime(DateUtil.getStartTimeStamp());
        queryActivityParam.setEffectEndTime(DateUtil.getEndTimeStamp());
        queryActivityParam.setIsOnline(1);
        queryActivityParam.setStatus(1);
        List<PromotionActivity> promotionActivities = promotionActivityManager.queryByCondition(queryActivityParam);
        if (ListUtil.isNullOrEmpty(promotionActivities)) {
            return modifyVipPriceResult;
        }

        List<PromotionRule> promotionRules = promotionRuleManager.promotionRules(promotionActivities.stream().map(PromotionActivity::getId).collect(Collectors.toList()));
        Map<String, PromotionRule> promotionRuleMap = ListUtil.listToMap("activityId", promotionRules);
        if (promotionRuleMap == null || promotionRuleMap.isEmpty()) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("促销规则转换异常");
        }

        for (PromotionActivity activity : promotionActivities) {
            List<AreaScopeShop> lcAreaScopeShops = lcAreaScopeShopService.get(lcActivityAreaScopeService.get(activity.getId()).getId());
            if (ListUtil.isNullOrEmpty(lcAreaScopeShops)) {
                continue;
            }
            List<AreaScopeShop> filterAreaShop = lcAreaScopeShops.stream().filter(item -> item.getShopId().equals(modifyVipPriceBean.getShopId())).collect(Collectors.toList());
            if (ListUtil.isNullOrEmpty(filterAreaShop)) {
                continue;
            }
            PromotionRule promotionRule = promotionRuleMap.get(activity.getId().toString());
            if (promotionRule.getProductScope().equals(ProductScopeTypeEnums.CLASS2.getCode())) {
                List<ActivityClass2> lcActivityClass2s = lcActivityClass2Service.get(activity.getId());
                List<ActivityClass2> activityClass2s =
                        lcActivityClass2s.stream().filter(item -> item.getClassId().equals(productSsu.getSku().getClass2Id()) || item.getClassId() == -1).collect(Collectors.toList());
                if (ListUtil.isNullOrEmpty(activityClass2s)) {
                    continue;
                }
                // 分类下排除商品
                List<ActivitySsu> lcExActivitySsus = lcActivitySsuService.get(activity.getId());
                if (!ListUtil.isNullOrEmpty(lcExActivitySsus)) {
                    Optional<ActivitySsu> optionalActivitySsu = lcExActivitySsus.stream().filter(item -> item.getSsuFormatId().equals(productSsu.getFormatId())).findFirst();
                    if (optionalActivitySsu.isPresent()) {
                        continue;
                    }
                }
            }
            if (promotionRule.getProductScope().equals(ProductScopeTypeEnums.SSU.getCode())) {
                List<ActivitySsu> lcActivitySsus = lcActivitySsuService.get(activity.getId());
                lcActivitySsus = lcActivitySsus.stream().filter(item -> item.getSsuFormatId().equals(productSsu.getFormatId())).collect(Collectors.toList());
                if (ListUtil.isNullOrEmpty(lcActivitySsus)) {
                    continue;
                }
            }

            VipRule vipRule = JSON.parseObject(lcPromotionRuleService.get(activity.getId()).getPromotionRule(), new TypeReference<VipRule>() {
            });
            modifyVipPriceResult.setModifyPrice(DiscountPriceHelper.getDiscountPrice(modifyVipPriceBean.getPrice(), vipRule.getDiscountRule()));
            modifyVipPriceResult.setDescription(promotionRuleMap.get(activity.getId().toString()).getDescript());
            break;
        }
        return modifyVipPriceResult;
    }

    @Override
    public ModifyVipPriceResult modifyVipPrice(ModifyVipPriceBean modifyVipPriceBean, ProductSsuFormatDto ssuFormat) {
        return null;
    }

    @Override
    public List<PromotionActivity> getActivityInfoByShopInfo(Integer shopId, List<PromotionTypeEnums> enums) {
        if (CollectionUtils.isEmpty(enums) || enums.size() == 0) {
            throw new BusinessException("促销类型不能为空");
        }
        int now = DateUtil.getCurrentTimeIntValue();

        List<PromotionActivityTime> promotionActivityTimeList = promotionActivityTimeManager.getActByShopIds(ImmutableList.of(shopId), now, now);
        if (CollectionUtils.isEmpty(promotionActivityTimeList)) {
            return null;
        }
        Set<Long> activityIdTimeList = promotionActivityTimeList.stream().map(PromotionActivityTime::getActivityId).collect(Collectors.toSet());
        List<PromotionActivity> promotionActivityList = promotionActivityManager.list(new LambdaQueryWrapper<PromotionActivity>()
                .eq(PromotionActivity::getStatus, ActivityStatusTypeEnums.STARTED.getCode())
                .eq(PromotionActivity::getIsOnline, ActivityOnlineTypeEnums.ONLINE.getCode())
                .eq(PromotionActivity::getIsDeleted, 0)
                .in(PromotionActivity::getPromotionModuleId, enums.stream().map(PromotionTypeEnums::getCode).collect(Collectors.toList()))
                .in(PromotionActivity::getId, activityIdTimeList));
        if (CollectionUtils.isEmpty(promotionActivityList)) {
            return null;
        }
        List<Long> promotionActivityIdList = promotionActivityList.stream().map(PromotionActivity::getId).collect(Collectors.toList());
        List<AreaScopeShop> areaScopeShopList = areaScopeShopManager.areaScopeShop(promotionActivityIdList, shopId);
        if (CollectionUtils.isEmpty(areaScopeShopList)) {
            return null;
        }
        List<Long> promotionActivityIdShopList = areaScopeShopList.stream().map(AreaScopeShop::getActivityId).collect(Collectors.toList());
        List<PromotionActivity> resultList = promotionActivityList.stream().filter(o -> promotionActivityIdShopList.contains(o.getId())).collect(Collectors.toList());
        return resultList;
    }

    @Override
    public List<QueryPriceChangeResult> getActivitySsuList(Long activityId, Integer shopId, List<Integer> list) {
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        List<ActivitySsu> productList = activitySsuManager.activitySsu(activityId, list, SsuTagTypeEnums.CONTAIN);

        if (CollectionUtils.isEmpty(productList)) {
            return null;
        }
        PromotionActivity promotionActivity = promotionActivityManager.getOne(new LambdaQueryWrapper<PromotionActivity>()
                .eq(PromotionActivity::getId, activityId)
                .eq(PromotionActivity::getIsDeleted, 0));
        if (promotionActivity == null) {
            return null;
        }
        List<QueryPriceChangeResult> resultList = productList.stream().map(o -> {
            QueryPriceChangeResult queryPriceChangeResult = new QueryPriceChangeResult();
            queryPriceChangeResult.setPromotionPrice(o.getPromotionPrice());
            queryPriceChangeResult.setShopId(shopId);
            queryPriceChangeResult.setSkuId(o.getSkuId());
            queryPriceChangeResult.setIsVip(VipTagEnums.NOT_VIP.getCode());
            queryPriceChangeResult.setCreatorFrom(promotionActivity.getCreatorFrom());
            queryPriceChangeResult.setReasonType(CreatorFromValueTranslateEnums.getCode2ByCode1(promotionActivity.getCreatorFrom()));
            return queryPriceChangeResult;
        }).collect(Collectors.toList());
        return resultList;
    }

    /**
     * 获取所有的会员促销活动
     *
     * @param activityId
     * @param shopId
     * @param skuList
     * @return
     */
    @Override
    public List<QueryPriceChangeResult> getActivityClass2List(Long activityId, Integer shopId, List<PriceChangeParam> skuList) {

        if (CollectionUtils.isEmpty(skuList)) {
            return null;
        }

        List<QueryPriceChangeResult> resultList = new ArrayList<>();
        //获取促销规则
        List<PromotionRule> promotionRules = promotionRuleManager.promotionRules(Collections.singletonList(activityId), ActivityOnlineTypeEnums.ONLINE);

        if (CollectionUtils.isEmpty(promotionRules)) {
            return null;
        }

        PromotionRule promotionRule = promotionRules.get(0);
        Integer promotionModuleId = promotionRule.getPromotionModuleId();
        if (!promotionModuleId.equals(PromotionTypeEnums.VIP.getCode())) {
            return null;
        }
        VipRule vipRule = JSONObject.parseObject(promotionRule.getPromotionRule(), VipRule.class);
        if (vipRule == null || vipRule.getDiscountRule() == null) {
            return null;
        }
        List<Integer> skuIds = skuList.stream().map(PriceChangeParam::getSkuId).collect(Collectors.toList());

        if (ProductScopeTypeEnums.SSU.getCode() == promotionRule.getProductScope()) {
            List<ActivitySsu> activitySsuList = activitySsuManager.activitySsu(activityId, skuIds, SsuTagTypeEnums.CONTAIN);
            //查到会员商品，计算会员价
            resultList = this.computeVipPrice(skuList, activitySsuList, vipRule.getDiscountRule());

        } else if (ProductScopeTypeEnums.CLASS2.getCode() == promotionRule.getProductScope()) {
            //根据skuId参数查询参数所属的分类
            List<Integer> skuIdList = skuList.stream().map(PriceChangeParam::getSkuId).collect(Collectors.toList());
            List<Sku> skuInfoList = qmExternalApiService.getSku(skuIdList);
            for (PriceChangeParam priceChangeParam : skuList) {
                for (Sku sku : skuInfoList) {
                    if (sku.getId().equals(priceChangeParam.getSkuId())) {
                        priceChangeParam.setClass2Id(sku.getClass2Id());
                        break;
                    }
                }
            }
            //获取商品信息
            List<ActivityClass2> activityClass2List = activityClass2Manager.activityClass2(Collections.singletonList(activityId));

            resultList = this.computeVipPriceClass2(skuList, activityClass2List, vipRule.getDiscountRule());
        }
        return resultList;
    }

    public List<QueryPriceChangeResult> computeVipPrice(List<PriceChangeParam> skuList, List<ActivitySsu> activitySsuList, DiscountRule discountRule) {
        if (CollectionUtils.isEmpty(skuList) || CollectionUtils.isEmpty(activitySsuList)) {
            return null;
        }
        List<QueryPriceChangeResult> priceResultList = this.copyParam(skuList);
        List<QueryPriceChangeResult> resultList = new ArrayList<>();
        List<Integer> skuIdList = activitySsuList.stream().map(ActivitySsu::getSkuId).collect(Collectors.toList());
        if (DiscountTypeEnums.UNITPRICE.getCode() == discountRule.getType()) {
            resultList = priceResultList.stream().filter(f -> skuIdList.contains(f.getSkuId())).map(o -> {
                o.setIsVip(VipTagEnums.IS_VIP.getCode());
                o.setVipPrice(o.getPrice().subtract(discountRule.getDiscount()));
                return o;
            }).collect(Collectors.toList());
        } else if (DiscountTypeEnums.DISCOUNT.getCode() == discountRule.getType()) {
            resultList = priceResultList.stream().filter(f -> skuIdList.contains(f.getSkuId())).map(o -> {
                o.setIsVip(VipTagEnums.IS_VIP.getCode());
                o.setVipPrice(o.getPrice().multiply(discountRule.getDiscount()).divide(new BigDecimal(100)));
                return o;
            }).collect(Collectors.toList());
        } else if (DiscountTypeEnums.FIXED.getCode() == discountRule.getType()) {
            resultList = priceResultList.stream().filter(f -> skuIdList.contains(f.getSkuId())).map(o -> {
                o.setIsVip(VipTagEnums.IS_VIP.getCode());
                o.setVipPrice(discountRule.getDiscount());
                return o;
            }).collect(Collectors.toList());
        }
        return resultList;
    }

    @Override
    public List<SendVipPriceParam> computeVipPrice(List<QmppSkuDto> skuList, VipRule vipRule) {
        if (CollectionUtils.isEmpty(skuList)) {
            return null;
        }
        List<SendVipPriceParam> priceResultList = new ArrayList<>();
        for (QmppSkuDto qmppSkuDto : skuList) {
            SendVipPriceParam sendVipPriceParam = new SendVipPriceParam();
            sendVipPriceParam.setSkuId(qmppSkuDto.getSkuId());
            sendVipPriceParam.setShopId(qmppSkuDto.getShopId());
            sendVipPriceParam.setPrice(qmppSkuDto.getAdvisePrice().multiply(new BigDecimal("1000")));
            priceResultList.add(sendVipPriceParam);
        }
        if (DiscountTypeEnums.UNITPRICE.getCode() == vipRule.getDiscountRule().getType()) {
            priceResultList.stream().map(o -> {
                o.setVipPrice(o.getPrice().subtract(vipRule.getDiscountRule().getDiscount()));
                return o;
            }).collect(Collectors.toList());
        } else if (DiscountTypeEnums.DISCOUNT.getCode() == vipRule.getDiscountRule().getType()) {
            priceResultList.stream().map(o -> {
                o.setVipPrice(o.getPrice().multiply(vipRule.getDiscountRule().getDiscount()).divide(new BigDecimal(100)));
                return o;
            }).collect(Collectors.toList());
        } else if (DiscountTypeEnums.FIXED.getCode() == vipRule.getDiscountRule().getType()) {
            priceResultList.stream().map(o -> {
                o.setVipPrice(vipRule.getDiscountRule().getDiscount());
                return o;
            }).collect(Collectors.toList());
        }
        return priceResultList;
    }

    private List<QueryPriceChangeResult> computeVipPriceClass2(List<PriceChangeParam> skuList, List<ActivityClass2> activityClass2List, DiscountRule discountRule) {
        if (CollectionUtils.isEmpty(skuList) || CollectionUtils.isEmpty(activityClass2List)) {
            return null;
        }
        List<QueryPriceChangeResult> priceResultList = this.copyParam(skuList);

        List<QueryPriceChangeResult> resultList = new ArrayList<>();
        List<Integer> class2IdList = activityClass2List.stream().map(ActivityClass2::getClassId).distinct().collect(Collectors.toList());
        if (DiscountTypeEnums.UNITPRICE.getCode() == discountRule.getType()) {
            resultList = priceResultList.stream().filter(f -> class2IdList.contains(f.getClass2Id())).map(o -> {
                o.setIsVip(VipTagEnums.IS_VIP.getCode());
                o.setVipPrice(o.getPrice().subtract(discountRule.getDiscount()));
                return o;
            }).collect(Collectors.toList());
        } else if (DiscountTypeEnums.DISCOUNT.getCode() == discountRule.getType()) {
            resultList = priceResultList.stream().filter(f -> class2IdList.contains(f.getClass2Id())).map(o -> {
                o.setIsVip(VipTagEnums.IS_VIP.getCode());
                o.setVipPrice(o.getPrice().multiply(discountRule.getDiscount()).divide(new BigDecimal(100)));
                return o;
            }).collect(Collectors.toList());
        } else if (DiscountTypeEnums.FIXED.getCode() == discountRule.getType()) {
            resultList = priceResultList.stream().filter(f -> class2IdList.contains(f.getClass2Id())).map(o -> {
                o.setIsVip(VipTagEnums.IS_VIP.getCode());
                o.setVipPrice(discountRule.getDiscount());
                return o;
            }).collect(Collectors.toList());
        }
        return resultList;
    }


    @Override
    public List<ProductSsuFormatDto> getProductSsuFormat(List<Integer> ssuFormatIds) {
        QueryProductSsuFormat queryProductSsuFormat = new QueryProductSsuFormat();
        queryProductSsuFormat.setSsuFormatIds(ssuFormatIds);
        queryProductSsuFormat.setSsuFp(1);
        queryProductSsuFormat.setPlatform(2);
        queryProductSsuFormat.setSellChannel(2);
        queryProductSsuFormat.setNeedSku(true);
        return qmExternalApiService.getSsuFormatByCondition(queryProductSsuFormat);
    }

    @Override
    public List<ProductSsuFormatDto> checkProductWithResult(List<Long> activityIds, List<ProductSsuFormatDto> productSsuFormatDtos) {
        List<ProductSsuFormatDto> result = new ArrayList<>();
        List<ActivitySsu> activitySsus = activitySsuManager.activitySsu(activityIds);
        Map<Integer, List<ActivitySsu>> tagSsuMap = activitySsus.stream().collect(Collectors.groupingBy(ActivitySsu::getExTag));

        List<Integer> ssuFormatIds = null;
        if (!ListUtil.isNullOrEmpty(tagSsuMap.get(SsuTagTypeEnums.CONTAIN.getCode()))) {
            ssuFormatIds = tagSsuMap.get(SsuTagTypeEnums.CONTAIN.getCode()).stream().map(ActivitySsu::getSsuFormatId).distinct().collect(Collectors.toList());
        }

        // 校验商品是否已设置促销
        if (!ListUtil.isNullOrEmpty(ssuFormatIds)) {
            for (ProductSsuFormatDto psf : productSsuFormatDtos) {
                if (!ssuFormatIds.contains(psf.getSsuFormatId())) {
                    result.add(psf);
                }
            }
        }

        List<ActivitySsu> exActivitySsu = null;
        if (!ListUtil.isNullOrEmpty(tagSsuMap.get(SsuTagTypeEnums.EXCLUSIVE.getCode()))) {
            exActivitySsu = tagSsuMap.get(SsuTagTypeEnums.EXCLUSIVE.getCode());
        }

        // 校验分类是否已设置促销（考虑分类剔除商品）
        List<ActivityClass2> activityClass2s = activityClass2Manager.activityClass2(activityIds);
        List<Integer> exSsuFormatIds = null;

        if (!ListUtil.isNullOrEmpty(activityClass2s)) {
            for (ProductSsuFormatDto psf : productSsuFormatDtos) {
                List<ActivityClass2> filterAcList = activityClass2s.stream().filter(ac -> ac.getClassId().equals(psf.getClass2Id())).collect(Collectors.toList());
                if (!ListUtil.isNullOrEmpty(filterAcList)) {
                    if (!ListUtil.isNullOrEmpty(exActivitySsu)) {
                        exSsuFormatIds =
                                exActivitySsu.stream().filter(item -> psf.getClass2Id().equals(item.getClass2Id())).map(ActivitySsu::getSsuFormatId).collect(Collectors.toList());
                        if (exSsuFormatIds.contains(psf.getSsuFormatId())) {
                            result.add(psf);
                            continue;
                        }
                    }
//                    throw new BusinessException("创建促销失败，当前商品所在分类已设置促销,class2:" + psf.getSku().getClass2Name());
                }
            }
        }
        return result.stream().distinct().collect(Collectors.toList());
    }

    @Override
    public List<CreateCpFromShopParam> checkSkuInVipPromotion(List<CreateCpFromShopParam> list) {
        if (CollectionUtils.isEmpty(list)) {
            return list;
        }
        //根据门店id查询会员活动
        List<PromotionActivity> shopVipPromotionList = this.queryVipPromotionActivityListByShopInfo(list.get(0).getShopId());
        if (CollectionUtils.isEmpty(shopVipPromotionList)) {
            return list;
        }
        List<Long> activityIds = shopVipPromotionList.stream().map(PromotionActivity::getId).collect(Collectors.toList());
        return this.findSkuNotInVipPromotion(activityIds, list);
    }

    private List<PromotionActivity> queryVipPromotionActivityListByShopInfo(Integer shopId) {
        int now = DateUtil.getCurrentTimeIntValue();
//        List<PromotionActivityTime> promotionActivityTimeList = promotionActivityTimeManager.selectList(new EntityWrapper<PromotionActivityTime>()
//                .le("effect_begin_time",now)
//                .ge("effect_end_time",now)
//                .eq("promotion_module_id", PromotionTypeEnums.VIP.getCode())
//                .eq("is_deleted",0));

        List<PromotionActivityTime> promotionActivityTimeList = promotionActivityTimeManager.getCurrentActiveAct(ImmutableList.of(PromotionTypeEnums.VIP.getCode()), now, now);

        if (CollectionUtils.isEmpty(promotionActivityTimeList)) {
            return null;
        }
        List<Long> activityIdTimeList = promotionActivityTimeList.stream().map(PromotionActivityTime::getActivityId).collect(Collectors.toList());

        List<PromotionActivity> promotionActivityList = promotionActivityManager.list(new LambdaQueryWrapper<PromotionActivity>()
                .eq(PromotionActivity::getStatus, ActivityStatusTypeEnums.STARTED.getCode())
                .eq(PromotionActivity::getIsOnline, ActivityOnlineTypeEnums.ONLINE.getCode())
                .eq(PromotionActivity::getIsDeleted, 0)
                .eq(PromotionActivity::getPromotionModuleId, PromotionTypeEnums.VIP.getCode())
                .in(PromotionActivity::getId, activityIdTimeList));
        if (CollectionUtils.isEmpty(promotionActivityList)) {
            return null;
        }
        List<Long> promotionActivityIdList = promotionActivityList.stream().map(PromotionActivity::getId).collect(Collectors.toList());
        List<AreaScopeShop> areaScopeShopList = areaScopeShopManager.areaScopeShop(promotionActivityIdList,shopId);
        if (CollectionUtils.isEmpty(areaScopeShopList)) {
            return null;
        }
        List<Long> promotionActivityIdShopList = areaScopeShopList.stream().map(AreaScopeShop::getActivityId).collect(Collectors.toList());
        List<PromotionActivity> resultList = promotionActivityList.stream().filter(o -> promotionActivityIdShopList.contains(o.getId())).collect(Collectors.toList());
        return resultList;
    }

    private List<CreateCpFromShopParam> findSkuNotInVipPromotion(List<Long> activityIds, List<CreateCpFromShopParam> list) {
        List<Integer> skuInVipList = new ArrayList<>();
        //获取促销规则
        List<PromotionRule> promotionRules = promotionRuleManager.promotionRules(activityIds, ActivityOnlineTypeEnums.ONLINE);


        List<Integer> skuIds = list.stream().map(CreateCpFromShopParam::getSkuId).collect(Collectors.toList());
        for (PromotionRule promotionRule : promotionRules) {
            if (promotionRule.getProductScope().equals(ProductScopeTypeEnums.SSU.getCode())) {
                List<ActivitySsu> activitySsuList = activitySsuManager.activitySsu(promotionRule.getActivityId(),skuIds,SsuTagTypeEnums.CONTAIN);
                if (CollectionUtils.isEmpty(activitySsuList)) {
                    continue;
                }
                List<Integer> skuIdList = activitySsuList.stream().map(ActivitySsu::getSkuId).collect(Collectors.toList());
                List<Integer> tmpSkuInVipList = list.stream().filter(o -> skuIdList.contains(o.getSkuId())).map(CreateCpFromShopParam::getSkuId).collect(Collectors.toList());
                skuInVipList.addAll(tmpSkuInVipList);

            } else if (promotionRule.getProductScope().equals(ProductScopeTypeEnums.CLASS2.getCode())) {
                //根据skuId参数查询参数所属的分类
                List<Integer> skuIdList = list.stream().map(CreateCpFromShopParam::getSkuId).collect(Collectors.toList());
                List<Sku> skuInfoList = qmExternalApiService.getSku(skuIdList);
                for (CreateCpFromShopParam priceChangeParam : list) {
                    for (Sku sku : skuInfoList) {
                        if (sku.getId().equals(priceChangeParam.getSkuId())) {
                            priceChangeParam.setClass2Id(sku.getClass2Id());
                            continue;
                        }
                    }
                }
                //获取商品信息
                List<ActivityClass2> activityClass2List = activityClass2Manager.activityClass2(promotionRule.getActivityId());
                if (CollectionUtils.isEmpty(activityClass2List)) {
                    continue;
                }
                List<Integer> class2IdList = activityClass2List.stream().map(ActivityClass2::getClassId).collect(Collectors.toList());
                List<Integer> tmpSkuInVipList = list.stream().filter(o -> class2IdList.contains(o.getClass2Id())).map(CreateCpFromShopParam::getSkuId).collect(Collectors.toList());
                skuInVipList.addAll(tmpSkuInVipList);
            }
        }
        return list.stream().filter(o -> !skuInVipList.contains(o.getSkuId())).collect(Collectors.toList());
    }

    private List<QueryPriceChangeResult> copyParam(List<PriceChangeParam> skuList) {
        List<QueryPriceChangeResult> priceResultList = new ArrayList<>();
        for (PriceChangeParam priceChangeParam : skuList) {
            QueryPriceChangeResult queryPriceChangeResult = new QueryPriceChangeResult();
            queryPriceChangeResult.setShopId(priceChangeParam.getShopId());
            queryPriceChangeResult.setSkuId(priceChangeParam.getSkuId());
            queryPriceChangeResult.setClass2Id(priceChangeParam.getClass2Id());
            queryPriceChangeResult.setPrice(priceChangeParam.getPrice());
            priceResultList.add(queryPriceChangeResult);
        }
        return priceResultList;
    }

}
