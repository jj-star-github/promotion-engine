package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.UserScopeGroup;
import com.qmfresh.promotion.mapper.UserScopeGroupMapper;
import com.qmfresh.promotion.manager.IUserScopeGroupManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 用户作用域 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@Service
@Slf4j
public class UserScopeGroupManagerImpl extends ServiceImpl<UserScopeGroupMapper, UserScopeGroup> implements IUserScopeGroupManager {

    @Override
    public List<UserScopeGroup> userScopeGroup(List<Long> userScopeIds) {
        log.info("userScopeGroup请求参数：{}", JSON.toJSONString(userScopeIds));

        List<UserScopeGroup> userScopeGroups = this.list(new LambdaQueryWrapper<UserScopeGroup>()
                .in(UserScopeGroup::getActivityId, userScopeIds)
                .eq(UserScopeGroup::getIsDeleted, 0)
        );
        if (CollectionUtils.isEmpty(userScopeGroups)) {
            return new ArrayList<>();
        }

        return userScopeGroups;
    }
}
