package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qmfresh.promotion.bean.order.QueryOrderParam;
import com.qmfresh.promotion.bean.order.UserOrderItemSummary;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.entity.OrderItemC;
import com.qmfresh.promotion.entity.OrderMainC;
import com.qmfresh.promotion.entity.OrdercReturn;
import com.qmfresh.promotion.entity.OrdercReturnItem;
import com.qmfresh.promotion.manager.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/6/3.
 *
 * @author wyh
 */
@Component
public class SYOrderManagerImpl implements ISYOrderManager {

    @Resource
    private IOrderMainCManager orderMainCManager;

    @Resource
    private IOrderItemCManager orderItemCManager;

    @Resource
    private IOrdercReturnManager ordercReturnManager;

    @Resource
    private IOrdercReturnItemManager ordercReturnItemManager;

    @Override
    public List<UserOrderItemSummary> queryOrder(QueryOrderParam param) {
        List<OrderMainC> orderMainCList = orderMainCManager.list(new LambdaQueryWrapper<OrderMainC>()
                .eq(OrderMainC::getUserId, param.getUserId())
                .ge(OrderMainC::getCT, param.getBeginTime())
                .lt(OrderMainC::getCT, param.getEndTime())
                .ge(OrderMainC::getStatus, 1));
        if (ListUtil.isNullOrEmpty(orderMainCList)) {
            return null;
        }
        List<Long> orderIds = orderMainCList.stream().map(OrderMainC::getId).collect(Collectors.toList());
        List<OrderItemC> orderItemCList = orderItemCManager.list(new LambdaQueryWrapper<OrderItemC>()
                .in(OrderItemC::getOrderId, orderIds));
        Map<Integer, List<OrderItemC>> ssuOrderItemMap = orderItemCList.stream().collect(Collectors.groupingBy(OrderItemC::getSsuId));

        List<OrdercReturn> ordercReturns = ordercReturnManager.list(new LambdaQueryWrapper<OrdercReturn>()
                .eq(OrdercReturn::getUserId, param.getUserId())
                .eq(OrdercReturn::getStatus, 1)
                .eq(OrdercReturn::getIsDeleted, 0));

        Map<Integer, List<OrdercReturnItem>> ssuOrderReturnMap = null;
        if (!ListUtil.isNullOrEmpty(ordercReturns)) {
            List<Long> returnOrderIds = ordercReturns.stream().map(OrdercReturn::getId).collect(Collectors.toList());
            List<OrdercReturnItem> ordercReturnItems = ordercReturnItemManager.list(new LambdaQueryWrapper<OrdercReturnItem>()
                    .in(OrdercReturnItem::getOrderReturnId, returnOrderIds)
                    .eq(OrdercReturnItem::getIsDeleted, 0));
            ssuOrderReturnMap = ordercReturnItems.stream().collect(Collectors.groupingBy(OrdercReturnItem::getSsuId));
        }

        List<UserOrderItemSummary> summaries = new ArrayList<>(16);
        for (Map.Entry<Integer, List<OrderItemC>> entry : ssuOrderItemMap.entrySet()) {
            int ssuId = entry.getKey();
            List<OrderItemC> oiList = entry.getValue();
            List<OrdercReturnItem> oriList = null;
            UserOrderItemSummary summary = new UserOrderItemSummary();
            summary.setUserId(param.getUserId());
            if (ssuOrderReturnMap != null && !ListUtil.isNullOrEmpty(ssuOrderReturnMap.get(ssuId))) {
                oriList = ssuOrderReturnMap.get(ssuId);
            }
            summaries.addAll(this.getSummaryByShop(param.getUserId(), oiList, oriList));
        }
        return summaries;
    }

    private List<UserOrderItemSummary> getSummaryByShop(long userId, List<OrderItemC> oiList,
                                                        List<OrdercReturnItem> oriList) {
        List<UserOrderItemSummary> summaries = new ArrayList<>(oiList.size());

        Map<Integer, List<OrdercReturnItem>> oriMap = null;
        if (!ListUtil.isNullOrEmpty(oriList)) {
            oriMap = oriList.stream().collect(Collectors.groupingBy(OrdercReturnItem::getShopId));
        }

        Map<Integer, List<OrderItemC>> shopMap = oiList.stream().collect(Collectors.groupingBy(OrderItemC::getShopId));
        for (Map.Entry<Integer, List<OrderItemC>> entry : shopMap.entrySet()) {
            int shopId = entry.getKey();
            int ssuId = entry.getValue().get(0).getSsuId();

            UserOrderItemSummary summary = new UserOrderItemSummary();
            summary.setUserId(userId);
            summary.setShopId(shopId);
            BigDecimal price = BigDecimal.ZERO;
            BigDecimal amount = BigDecimal.ZERO;
            BigDecimal returnAmount = BigDecimal.ZERO;
            BigDecimal returnPrice = BigDecimal.ZERO;
            entry.getValue().forEach(
                    oi -> {
                        price.add(oi.getRealPrice());
                        amount.add(oi.getTotal());
                    }
            );
            summary.setAmount(amount);
            summary.setPrice(price);
            summary.setSsuId(ssuId);
            summary.setReturnPrice(BigDecimal.ZERO);
            summaries.add(summary);
            if (oriMap != null && !ListUtil.isNullOrEmpty(oriMap.get(shopId))) {
                oriMap.get(shopId).forEach(
                        ori -> {
                            returnPrice.add(ori.getSubTotal());
                            returnAmount.add(ori.getTotal());
                        }
                );
                summary.setReturnPrice(returnPrice);
                summary.setReturnAmount(returnAmount);
            }
            summary.setTotalPrice(price.subtract(returnPrice));
            summary.setTotalAmount(amount.subtract(returnAmount));
            summaries.add(summary);
        }
        return summaries;
    }
}
