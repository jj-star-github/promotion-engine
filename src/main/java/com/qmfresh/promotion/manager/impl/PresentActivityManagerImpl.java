package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.bean.present.PageQueryParam;
import com.qmfresh.promotion.bean.present.PresentActivityDto;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.PresentActivity;
import com.qmfresh.promotion.entity.PresentSsu;
import com.qmfresh.promotion.enums.PresentStatusEnums;
import com.qmfresh.promotion.manager.IPresentActivityManager;
import com.qmfresh.promotion.mapper.PresentActivityMapper;
import com.qmfresh.promotion.mapper.PresentSsuMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 赠品活动 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@Service
@Slf4j
public class PresentActivityManagerImpl extends ServiceImpl<PresentActivityMapper, PresentActivity> implements IPresentActivityManager {

    @Resource
    private PresentActivityMapper presentActivityMapper;

    @Resource
    private PresentSsuMapper presentSsuMapper;

    @Override
    public List<PresentActivityDto> queryList(PageQueryParam param) {
        int now = DateUtil.getCurrentTimeIntValue();
        param.setNowTime(now);
        List<PresentActivity> presentActivities = presentActivityMapper.queryList(param);
        List<PresentSsu> presentSsus = this.presentSsus(presentActivities.stream().map(PresentActivity::getId).collect(Collectors.toList()));
        Map<Long, List<PresentSsu>> presentSsuMap = presentSsus.stream().collect(Collectors.groupingBy(PresentSsu::getPresentId));
        List<PresentActivityDto> dtos = new ArrayList<>(presentActivities.size());
        presentActivities.forEach(
                pa -> {
                    PresentActivityDto presentActivityDto = new PresentActivityDto();
                    presentActivityDto.setPresentActivity(pa);
                    presentActivityDto.setPresentSsus(presentSsuMap.get(pa.getId()));
                    dtos.add(presentActivityDto);
                }
        );
        return dtos;
    }

    @Override
    public ListWithPage<PresentActivityDto> pageQuery(PageQueryParam param) {
        ListWithPage<PresentActivityDto> pageList = new ListWithPage<>();

        List<PresentActivity> presentActivities = presentActivityMapper.pageQuery(param, param.getStart(), param.getEnd());
        if (presentActivities == null || presentActivities.isEmpty()) {
            return pageList;
        }

        List<PresentSsu> presentSsus = this.presentSsus(presentActivities.stream().map(PresentActivity::getId).collect(Collectors.toList()));

        Map<Long, List<PresentSsu>> presentSsuMap = presentSsus.stream().collect(Collectors.groupingBy(PresentSsu::getPresentId));

        List<PresentActivityDto> dtos = new ArrayList<>(presentActivities.size());
        presentActivities.forEach(
                pa -> {
                    PresentActivityDto presentActivityDto = new PresentActivityDto();
                    presentActivityDto.setPresentActivity(pa);
                    presentActivityDto.setPresentSsus(presentSsuMap.get(pa.getId()));
                    dtos.add(presentActivityDto);
                }
        );

        pageList.setTotalCount(presentActivityMapper.countByCondition(param));
        pageList.setListData(dtos);
        return pageList;
    }

    @Override
    public boolean update(Integer id) {

        return presentActivityMapper.updateStatus(id);
    }

    List<PresentSsu> presentSsus(List<Long> presentActivityIds) {
        if (CollectionUtils.isEmpty(presentActivityIds)) {
            return new ArrayList<>();
        }
        List<PresentSsu> presentSsus = presentSsuMapper.selectList(new LambdaQueryWrapper<PresentSsu>()
                .in(PresentSsu::getPresentId, presentActivityIds)
                .eq(PresentSsu::getIsDeleted, 0)
        );
        if (CollectionUtils.isEmpty(presentSsus)) {
            return new ArrayList<>();
        }
        return presentSsus;
    }

    @Override
    public List<PresentActivity> presentActivities(List<Long> promotionActivityIds, List<PresentStatusEnums> statusEnums) {
        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }
        if (CollectionUtils.isEmpty(statusEnums)) {
            return new ArrayList<>();
        }
        log.info("赠品活动参数：filterPromotionActivity= {} status = {}", JSON.toJSONString(promotionActivityIds), statusEnums);
        List<PresentActivity> presentActivities = this.list(new LambdaQueryWrapper<PresentActivity>().eq(PresentActivity::getIsDeleted, 0)
                .in(PresentActivity::getActivityId, promotionActivityIds)
                .in(PresentActivity::getStatus, statusEnums.stream().map(PresentStatusEnums::getCode).distinct().collect(Collectors.toList()))
        );
        if (CollectionUtils.isEmpty(presentActivities)) {
            log.info("无法查询到赠品活动：filterPromotionActivity= {}", JSON.toJSONString(promotionActivityIds));
            return new ArrayList<>();
        }

        return presentActivities;
    }

    @Override
    public List<PresentActivity> presentActivities(List<Long> promotionActivityIds) {

        log.info("presentActivities请求参数:{}", JSON.toJSONString(promotionActivityIds));
        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }

        List<PresentActivity> presentActivities = this.list(
                new LambdaQueryWrapper<PresentActivity>()
                        .in(PresentActivity::getActivityId, promotionActivityIds)
                        .eq(PresentActivity::getIsDeleted, 0)
        );

        if (CollectionUtils.isEmpty(presentActivities)) {
            return new ArrayList<>();
        }
        return presentActivities;
    }

    @Override
    public List<PresentSsu> presentSsu(List<Long> presentActivityIds) {
        if (CollectionUtils.isEmpty(presentActivityIds)) {
            return new ArrayList<>();
        }

        log.info("presentSsu查询参数：{}", JSON.toJSONString(presentActivityIds));

        List<PresentSsu> presentSsus =
                this.presentSsuMapper.selectList(new LambdaQueryWrapper<PresentSsu>()
                        .eq(PresentSsu::getIsDeleted, 0)
                        .in(PresentSsu::getPresentId, presentActivityIds)
                );

        if (CollectionUtils.isEmpty(presentSsus)) {
            return new ArrayList<>();
        }

        return presentSsus;
    }
}
