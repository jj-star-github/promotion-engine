package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.ActivityClass2;
import com.qmfresh.promotion.mapper.ActivityClass2Mapper;
import com.qmfresh.promotion.manager.IActivityClass2Manager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 促销活动分类 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@Service
@Slf4j
public class ActivityClass2ManagerImpl extends ServiceImpl<ActivityClass2Mapper, ActivityClass2> implements IActivityClass2Manager {

    @Override
    public List<Integer> filterClassIds(List<Long> promotionActivityIds, List<Integer> classIds) {

        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }
        if (CollectionUtils.isEmpty(classIds)) {
            return new ArrayList<>();
        }
        log.info("过滤活动中的类目参数：filterPromotionActivity = {} classIds = {}",
                JSON.toJSONString(promotionActivityIds), JSON.toJSONString(classIds));


        List<ActivityClass2> class2 = this.list(new LambdaQueryWrapper<ActivityClass2>()
                .eq(ActivityClass2::getIsDeleted, 0)
                .in(ActivityClass2::getActivityId, promotionActivityIds)
                .in(ActivityClass2::getClassId, classIds)

        );

        if (CollectionUtils.isEmpty(class2)) {
            log.info("过滤活动中，类目列表为空：filterPromotionActivity = {} classIds = {}",
                    JSON.toJSONString(promotionActivityIds), JSON.toJSONString(classIds));
            return new ArrayList<>();
        }

        return class2.stream().map(ActivityClass2::getClassId).distinct().collect(Collectors.toList());
    }

    @Override
    public List<ActivityClass2> activityClass2(List<Long> promotionActivityIds) {

        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }
        log.info("查询促销活动对应的活动类目参数：{}", JSON.toJSONString(promotionActivityIds));
        List<ActivityClass2> class2s = this.list(new LambdaQueryWrapper<ActivityClass2>()
                .eq(ActivityClass2::getIsDeleted, 0)
                .in(ActivityClass2::getActivityId, promotionActivityIds)
        );
        if (CollectionUtils.isEmpty(class2s)) {
            log.info("没有查询到对应活动类目参数：{}", JSON.toJSONString(promotionActivityIds));
            return new ArrayList<>();
        }

        return class2s;
    }

    @Override
    public List<ActivityClass2> activityClass2(List<Long> promotionActivityIds, Integer classId) {
        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }
        if (classId == null) {
            return new ArrayList<>();
        }
        log.info("查询促销活动对应的活动类目参数：{}", JSON.toJSONString(promotionActivityIds), classId);
        List<ActivityClass2> class2s = this.list(new LambdaQueryWrapper<ActivityClass2>()
                .in(ActivityClass2::getActivityId, promotionActivityIds)
                .eq(ActivityClass2::getIsDeleted, 0)
                .eq(ActivityClass2::getClassId, classId)
        );
        if (CollectionUtils.isEmpty(class2s)) {
            return new ArrayList<>();
        }
        return class2s;
    }

    @Override
    public List<ActivityClass2> activityClass2(Long promotionActivityId, Integer classId) {
        log.info("activityClass2请求参数：{} {}", promotionActivityId, classId);
        if (promotionActivityId == null) {
            return new ArrayList<>();
        }
        if (classId == null) {
            return new ArrayList<>();
        }

        List<ActivityClass2> activityClass2s = this.list(new LambdaQueryWrapper<ActivityClass2>()
                .eq(ActivityClass2::getActivityId, promotionActivityId)
                .eq(ActivityClass2::getIsDeleted, 0)
                .eq(ActivityClass2::getClassId, classId)
        );

        if (CollectionUtils.isEmpty(activityClass2s)) {
            return new ArrayList<>();
        }

        return activityClass2s;
    }

    @Override
    public List<ActivityClass2> activityClass2(Long promotionActivityId) {
        log.info("activityClass2请求参数：{}", promotionActivityId);
        if (promotionActivityId == null) {
            return new ArrayList<>();
        }

        List<ActivityClass2> activityClass2s = this.list(new LambdaQueryWrapper<ActivityClass2>()
                .eq(ActivityClass2::getActivityId, promotionActivityId)
                .eq(ActivityClass2::getIsDeleted, 0)
        );

        if (CollectionUtils.isEmpty(activityClass2s)) {
            return new ArrayList<>();
        }

        return activityClass2s;
    }
}
