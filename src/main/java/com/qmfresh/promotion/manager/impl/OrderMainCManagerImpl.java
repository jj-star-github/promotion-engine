package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.OrderMainC;
import com.qmfresh.promotion.manager.IOrderMainCManager;
import com.qmfresh.promotion.mapper.OrderMainCMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * C端订单主表 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
@Service
public class OrderMainCManagerImpl extends ServiceImpl<OrderMainCMapper, OrderMainC> implements IOrderMainCManager {
	
}
