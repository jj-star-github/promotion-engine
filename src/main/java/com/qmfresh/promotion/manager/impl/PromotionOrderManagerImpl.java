package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.bean.order.PromotionOrderDto;
import com.qmfresh.promotion.bean.order.QueryOrderParam;
import com.qmfresh.promotion.bean.sku.ProductSsuFormatDto;
import com.qmfresh.promotion.bean.sku.QueryProductSsuFormat;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.entity.PromotionOrder;
import com.qmfresh.promotion.entity.PromotionOrderItem;
import com.qmfresh.promotion.enums.ChannelTypeEnums;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.manager.IPromotionOrderItemManager;
import com.qmfresh.promotion.manager.IPromotionOrderManager;
import com.qmfresh.promotion.mapper.PromotionOrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-13
 */
@Service
@Slf4j
public class PromotionOrderManagerImpl extends ServiceImpl<PromotionOrderMapper, PromotionOrder> implements IPromotionOrderManager {


    @Resource
    private IPromotionOrderItemManager promotionOrderItemManager;

    @Resource
    private IQmExternalApiService qmExternalApiService;

    @Override
    public boolean createPromotionOrder(PromotionOrderDto param) {
        if (param.getUserId() == null || param.getUserId().compareTo(0L) <= 0) {
            log.info("无user不生成订单" + JSON.toJSONString(param));
            return true;
        }
        List<PromotionOrder> promotionOrders = this.list(new LambdaQueryWrapper<PromotionOrder>()
                .eq(PromotionOrder::getOrderCode, param.getOrderId())
                .eq(PromotionOrder::getIsDeleted, 0));
        if (!ListUtil.isNullOrEmpty(promotionOrders)) {
            log.info("促销订单已生成,PromotionOrderDto:" + JSON.toJSONString(param));
            return true;
        }

        int now = DateUtil.getCurrentTimeIntValue();
        PromotionOrder promotionOrder = new PromotionOrder();
        BeanUtils.copyProperties(param, promotionOrder);
        promotionOrder.setOrderCode(param.getOrderId());
        promotionOrder.setCT(now);
        promotionOrder.setUT(now);
        promotionOrder.setIsDeleted(0);
        this.save(promotionOrder);

        List<PromotionOrderItem> poiList = new ArrayList<>(param.getItems().size());
        param.getItems().forEach(
                item -> {
                    PromotionOrderItem poi = new PromotionOrderItem();
                    BeanUtils.copyProperties(item, poi);
                    poi.setUserId(param.getUserId());
                    poi.setShopId(param.getShopId());
                    poi.setChannel(param.getChannel());
                    poi.setOrderId(promotionOrder.getId());
                    poi.setOrderCode(promotionOrder.getOrderCode());
                    poi.setOrderItemId(item.getOrderItemId());
                    poi.setCreateDate(param.getCreateDate());
                    poi.setOccurrenceTime(param.getOccurrenceTime());
                    poi.setStatus(1);
                    poi.setCT(now);
                    poi.setUT(now);
                    poi.setIsDeleted(0);
                    poiList.add(poi);
                }
        );

        if (param.getChannel().equals(ChannelTypeEnums.C_OFFLINE.getCode())) {
            QueryProductSsuFormat queryProductSsuFormat = new QueryProductSsuFormat();
            queryProductSsuFormat.setSkuIds(param.getItems().stream().map(PromotionOrderItem::getSkuId).distinct().collect(Collectors.toList()));
            queryProductSsuFormat.setPlatform(2);
            queryProductSsuFormat.setSellChannel(2);
            queryProductSsuFormat.setSsuFp(1);
            List<ProductSsuFormatDto> productSsuFormatDtos = qmExternalApiService.getSsuFormatByCondition(queryProductSsuFormat);
            Map<Integer, ProductSsuFormatDto> skuProductSsuFormatMap = new HashMap<>(productSsuFormatDtos.size());
            productSsuFormatDtos.forEach(item -> skuProductSsuFormatMap.put(item.getSkuId(), item));
            poiList.forEach(
                    poi -> {
                        ProductSsuFormatDto productSsuFormatDto = skuProductSsuFormatMap.get(poi.getSkuId());
                        if (productSsuFormatDto != null) {
                            poi.setSsuFormatId(productSsuFormatDto.getSsuFormatId());
                        }
                    }
            );
        }
        promotionOrderItemManager.saveBatch(poiList);
        return true;
    }

    @Override
    public List<PromotionOrderItem> queryOrderItem(QueryOrderParam queryOrderParam) {
        LambdaQueryWrapper<PromotionOrderItem> where = new LambdaQueryWrapper<PromotionOrderItem>();
        if (queryOrderParam.getUserId() != null) {
            where.eq(PromotionOrderItem::getUserId, queryOrderParam.getUserId());
        }

        if (queryOrderParam.getShopId() != null) {
            where.eq(PromotionOrderItem::getShopId, queryOrderParam.getShopId());
        }

        if (queryOrderParam.getSkuId() != null) {
            where.eq(PromotionOrderItem::getSkuId, queryOrderParam.getSkuId());
        }

      /*  if(queryOrderParam.getIsPresent() != null) {
            where.eq("is_present", queryOrderParam.getIsPresent());
        }*/

        where.le(PromotionOrderItem::getCT, queryOrderParam.getEndTime());
        where.ge(PromotionOrderItem::getCT, queryOrderParam.getBeginTime());
        where.eq(PromotionOrderItem::getChannel, queryOrderParam.getChannel());
        where.eq(PromotionOrderItem::getIsDeleted, 0);

        return promotionOrderItemManager.list(where);
    }
}
