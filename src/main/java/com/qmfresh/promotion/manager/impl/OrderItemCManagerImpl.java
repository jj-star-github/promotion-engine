package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.OrderItemC;
import com.qmfresh.promotion.manager.IOrderItemCManager;
import com.qmfresh.promotion.mapper.OrderItemCMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单详情 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
@Service
public class OrderItemCManagerImpl extends ServiceImpl<OrderItemCMapper, OrderItemC> implements IOrderItemCManager {
	
}
