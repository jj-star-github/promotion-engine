package com.qmfresh.promotion.manager.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.PresentSsu;
import com.qmfresh.promotion.mapper.PresentSsuMapper;
import com.qmfresh.promotion.manager.IPresentSsuManager;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 赠品商品配置 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@Service
public class PresentSsuManagerImpl extends ServiceImpl<PresentSsuMapper, PresentSsu> implements IPresentSsuManager {
	
}
