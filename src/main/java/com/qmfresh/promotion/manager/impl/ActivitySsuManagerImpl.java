package com.qmfresh.promotion.manager.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.ActivitySsu;
import com.qmfresh.promotion.enums.SsuTagTypeEnums;
import com.qmfresh.promotion.manager.IActivitySsuManager;
import com.qmfresh.promotion.mapper.ActivitySsuMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 促销活动ssu 服务实现类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@Service
@Slf4j
public class ActivitySsuManagerImpl extends ServiceImpl<ActivitySsuMapper, ActivitySsu> implements IActivitySsuManager {

    @Override
    public List<Integer> filterSkuIds(List<Long> promotionActivityIds, List<Integer> skuIds, SsuTagTypeEnums ssuTag) {
        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }
        if (CollectionUtils.isEmpty(skuIds)) {
            return new ArrayList<>();
        }
        if (ssuTag == null) {
            return new ArrayList<>();
        }

        log.info("筛选活动对应的skuIds列表参数：filterPromotionActivity ={} skuIds = {} ssuTag ={}", JSON.toJSONString(promotionActivityIds), JSON.toJSONString(skuIds), ssuTag);

        List<ActivitySsu> ssus = this.list(
                new LambdaQueryWrapper<ActivitySsu>()
                        .in(ActivitySsu::getActivityId, promotionActivityIds)
                        .in(ActivitySsu::getSkuId, skuIds)
                        .eq(ActivitySsu::getIsDeleted, 0)
                        .eq(ActivitySsu::getExTag, ssuTag.getCode())
        );
        if (CollectionUtils.isEmpty(ssus)) {
            log.info("无法找到筛选活动对应的skuIds：filterPromotionActivity ={} skuIds = {} ssuTag = {}", JSON.toJSONString(promotionActivityIds), JSON.toJSONString(skuIds), ssuTag);
            return new ArrayList<>();
        }

        return ssus.stream().map(ActivitySsu::getSkuId).distinct().collect(Collectors.toList());
    }

    @Override
    public List<ActivitySsu> filterActivitySsu(List<Long> promotionActivityIds, List<Integer> skuIds) {
        log.info("filterActivitySsu请求参数:{} {}", JSON.toJSONString(promotionActivityIds), JSON.toJSONString(skuIds));
        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }
        if (CollectionUtils.isEmpty(skuIds)) {
            return new ArrayList<>();
        }
        List<ActivitySsu> ssus = this.list(new LambdaQueryWrapper<ActivitySsu>()
                .in(ActivitySsu::getActivityId, promotionActivityIds)
                .in(ActivitySsu::getSkuId, skuIds)
                .eq(ActivitySsu::getIsDeleted, 0)

        );
        if (CollectionUtils.isEmpty(ssus)) {
            return new ArrayList<>();
        }

        return ssus;
    }

    @Override
    public List<ActivitySsu> activitySsu(List<Long> promotionActivityIds) {

        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }

        log.info("查询活动商品参数：{}", JSON.toJSONString(promotionActivityIds));
        List<ActivitySsu> activitySsus = this.list(
                new LambdaQueryWrapper<ActivitySsu>()
                        .eq(ActivitySsu::getIsDeleted, 0)
                        .in(ActivitySsu::getActivityId, promotionActivityIds)
        );

        if (CollectionUtils.isEmpty(activitySsus)) {
            log.info("无法查询到活动商品参数：{}", JSON.toJSONString(promotionActivityIds));
            return new ArrayList<>();
        }

        return activitySsus;
    }

    @Override
    public List<ActivitySsu> activitySsu(List<Long> promotionActivityIds, SsuTagTypeEnums ssuTagTypeEnums) {
        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }
        if (ssuTagTypeEnums == null) {
            return new ArrayList<>();
        }
        log.info("查询活动商品ssu参数：filterPromotionActivity = {} ,ssuTagTypeEnums = {}", JSON.toJSONString(promotionActivityIds), ssuTagTypeEnums);
        List<ActivitySsu> activitySsus = this.list(new LambdaQueryWrapper<ActivitySsu>()
                .in(ActivitySsu::getActivityId, promotionActivityIds)
                .eq(ActivitySsu::getIsDeleted, 0)
                .eq(ActivitySsu::getExTag, ssuTagTypeEnums.getCode())
        );
        if (CollectionUtils.isEmpty(activitySsus)) {
            return new ArrayList<>();
        }
        return activitySsus;
    }

    @Override
    public List<ActivitySsu> activitySsu(List<Long> promotionActivityIds, Integer ssuFormatId) {
        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }
        if (ssuFormatId == null) {
            return new ArrayList<>();
        }

        log.info("查询活动商品ssu参数：promotionActivityIds={} ssuFormatId = {}", JSON.toJSONString(promotionActivityIds), ssuFormatId);
        List<ActivitySsu> activitySsus = this.list(
                new LambdaQueryWrapper<ActivitySsu>()
                        .in(ActivitySsu::getActivityId, promotionActivityIds)
                        .eq(ActivitySsu::getSsuFormatId, ssuFormatId)
                        .eq(ActivitySsu::getIsDeleted, 0)
        );

        if (CollectionUtils.isEmpty(activitySsus)) {
            return new ArrayList<>();
        }

        return activitySsus;
    }

    @Override
    public List<ActivitySsu> activitySsu(Long promotionActivityId, List<Integer> skuId, SsuTagTypeEnums tagTypeEnums) {
        log.info("请求activitySsu参数：promotionActivityId ={}  skuIds= {} {}", promotionActivityId, JSON.toJSONString(skuId), tagTypeEnums);
        if (promotionActivityId == null) {
            return new ArrayList<>();
        }
        if (CollectionUtils.isEmpty(skuId)) {
            return new ArrayList<>();
        }
        if (tagTypeEnums == null) {
            return new ArrayList<>();
        }

        List<ActivitySsu> activitySsus = this.list(
                new LambdaQueryWrapper<ActivitySsu>()
                        .eq(ActivitySsu::getActivityId, promotionActivityId)
                        .in(ActivitySsu::getSkuId, skuId)
                        .eq(ActivitySsu::getExTag, tagTypeEnums.getCode())
                        .eq(ActivitySsu::getIsDeleted, 0)
        );
        if (CollectionUtils.isEmpty(activitySsus)) {
            return new ArrayList<>();
        }
        return activitySsus;
    }

    @Override
    public List<ActivitySsu> activitySSu(List<Long> promotionActivityIds, List<Integer> skuIds) {
        log.info("activitySSu请求参数： {} {}", JSON.toJSONString(promotionActivityIds), JSON.toJSONString(skuIds));
        if (CollectionUtils.isEmpty(promotionActivityIds)) {
            return new ArrayList<>();
        }
        if (CollectionUtils.isEmpty(skuIds)) {
            return new ArrayList<>();
        }

        List<ActivitySsu> activitySsus = this.list(
                new LambdaQueryWrapper<ActivitySsu>()
                        .in(ActivitySsu::getActivityId, promotionActivityIds)
                        .in(ActivitySsu::getSkuId, skuIds)
                        .eq(ActivitySsu::getIsDeleted, 0)
        );

        if (CollectionUtils.isEmpty(activitySsus)) {
            return new ArrayList<>();
        }

        return activitySsus;
    }
}
