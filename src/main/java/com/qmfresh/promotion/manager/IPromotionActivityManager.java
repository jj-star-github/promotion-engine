package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.bean.promotion.PageQueryActivityParam;
import com.qmfresh.promotion.bean.promotion.QueryActivityParam;
import com.qmfresh.promotion.dto.ShopAuthorityResult;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.enums.ActivityOnlineTypeEnums;
import com.qmfresh.promotion.enums.ActivityStatusTypeEnums;
import com.qmfresh.promotion.enums.ChannelTypeEnums;
import com.qmfresh.promotion.enums.PromotionTypeEnums;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface IPromotionActivityManager extends IService<PromotionActivity> {
    /**
     * 查询促销活动
     *
     * @param param
     */
    ListWithPage<PromotionActivity> pageQuery(PageQueryActivityParam param);

    /**
     * 查看当前促销活动
     *
     * @param param 查询条件
     * @return 活动列表
     */
    List<PromotionActivity> queryCurrentPromotionActivity(QueryActivityParam param);

    /**
     * 根据门店查询活动信息
     * @param param
     * @return
     */
    List<PromotionActivity> queryPromotionActivityByShop(QueryActivityParam param);

    /**
     * 查询促销活动
     *
     * @param param
     * @return
     */
    List<PromotionActivity> queryByCondition(QueryActivityParam param);

    /**
     * 活动上线
     *
     * @param activityId
     * @param userId
     * @param userName
     * @return
     */
    Boolean online(Long activityId, Long userId, String userName);

    /**
     * 活动暂停
     *
     * @param activityId
     * @param userId
     * @param userName
     * @return
     */
    Boolean stop(Long activityId, Long userId, String userName);

    /**
     * 删除活动
     *
     * @param activityId
     * @param userId
     * @param userName
     * @return
     */
    Boolean delete(Long activityId, Long userId, String userName);

    /**
     * 活动结束
     *
     * @param activityIds
     * @return
     */
    Boolean finish(List<Long> activityIds);

    Boolean outOfDate(List<Long> activityIds);

    Boolean start(List<Long> activityIds);

    /**
     * 失效之前的活动，按商品+店的维度
     *
     * @param shopAuthorityResult
     * @param activityId
     * @return
     */
    Boolean offLine(ShopAuthorityResult shopAuthorityResult, Long activityId);

    /**
     * 查询门店对应的活动id
     *
     * @param shopId               shopId
     * @param promotionActivityIds 已有的活动id列表
     * @return 促销活动的id列表
     */
    List<Long> filterPromotionActivity(Integer shopId, List<Long> promotionActivityIds);

    /**
     * 过滤促销活动列表
     *
     * @param promotionActivityIds 促销活动列表
     * @param onlineType           上线/下线状态
     * @param status               促销订单状态
     * @param promotionType
     * @param channelType
     * @return 过滤后的活动列表
     */
    List<PromotionActivity> filterPromotionActivity(List<Long> promotionActivityIds, ActivityOnlineTypeEnums onlineType,
                                                    ActivityStatusTypeEnums status, PromotionTypeEnums promotionType, ChannelTypeEnums channelType);


    /**
     * 各种状态的促销互动
     *
     * @param status 筛选的活动状态列表
     * @return 返回活动列表
     */
    List<PromotionActivity> promotionActivities(List<ActivityStatusTypeEnums> status);

}
