package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.OrderItemC;

/**
 * <p>
 * 订单详情 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
public interface IOrderItemCManager extends IService<OrderItemC> {
	
}
