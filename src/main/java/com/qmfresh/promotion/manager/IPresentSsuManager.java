package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.PresentSsu;

/**
 * <p>
 * 赠品商品配置 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface IPresentSsuManager extends IService<PresentSsu> {
	
}
