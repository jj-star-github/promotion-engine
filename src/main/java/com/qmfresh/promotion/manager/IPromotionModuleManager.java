package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.bean.promotion.QueryPromotionModuleParam;
import com.qmfresh.promotion.entity.PromotionModule;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface IPromotionModuleManager extends IService<PromotionModule> {
    /**
     * 查询促销模板
     *
     * @param param
     * @return
     */
    List<PromotionModule> queryByCondition(QueryPromotionModuleParam param);

    /**
     * 查询促销模块id
     *
     * @param ids 促销模块id
     * @return 促销模块列表
     */
    List<PromotionModule> promotionModule(List<Integer> ids);
}
