package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.UserScopeGroup;

import java.util.List;

/**
 * <p>
 * 用户作用域 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface IUserScopeGroupManager extends IService<UserScopeGroup> {

    List<UserScopeGroup> userScopeGroup(List<Long> userScopeIds);

}
