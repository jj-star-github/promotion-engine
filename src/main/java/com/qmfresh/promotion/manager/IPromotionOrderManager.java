package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.bean.order.PromotionOrderDto;
import com.qmfresh.promotion.bean.order.QueryOrderParam;
import com.qmfresh.promotion.entity.PromotionOrder;
import com.qmfresh.promotion.entity.PromotionOrderItem;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-13
 */
public interface IPromotionOrderManager extends IService<PromotionOrder> {
    /**
     * 创建促销订单
     *
     * @param param
     * @return
     */
    boolean createPromotionOrder(PromotionOrderDto param);



    /**
     * 查看促销订单详情
     *
     * @param queryOrderParam
     * @return
     */
    List<PromotionOrderItem> queryOrderItem(QueryOrderParam queryOrderParam);
}
