package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.ActivityUserScope;

import java.util.List;

/**
 * <p>
 * 活动用户作用域 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface IActivityUserScopeManager extends IService<ActivityUserScope> {


    List<ActivityUserScope> activityUserScope(List<Long> promotionActivityIds);

}
