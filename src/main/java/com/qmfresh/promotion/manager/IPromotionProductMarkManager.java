package com.qmfresh.promotion.manager;

import com.qmfresh.promotion.enums.ActivityStatusTypeEnums;
import java.util.List;

/**
 * Created by wyh on 2019/7/29.
 *
 * @author wyh
 */
public interface IPromotionProductMarkManager {

    /**
     * 商品打标 by 活动id
     *
     * @param activityId
     * @param status
     * @return
     */
    Boolean markProduct(Long activityId, ActivityStatusTypeEnums status);

    /**
     * 商品打标
     *
     * @param channel
     * @param ssuFormatId
     * @param class2Id
     * @param shopIds
     * @return
     */
    Boolean markProduct(Integer channel, Integer ssuFormatId, Integer class2Id, List<Integer> shopIds);
}
