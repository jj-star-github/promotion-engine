package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.PromotionSellOutWarningDetail;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xbb
 * @since 2019-11-23
 */
public interface IPromotionSellOutWarningDetailManager extends IService<PromotionSellOutWarningDetail> {
	
}
