package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.PromotionSellOutWarningTime;

import java.util.List;

public interface IPromotionSellOutWarningTimeManager extends IService<PromotionSellOutWarningTime> {

    Boolean updateByWarningId(List<Integer> ids);

    Boolean updateByCondition(List<PromotionSellOutWarningTime> times);
}
