package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.ActivityAreaScope;

import java.util.List;

/**
 * <p>
 * 活动区域表 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface IActivityAreaScopeManager extends IService<ActivityAreaScope> {

    /**
     * 根据促销活动，获取区域id
     *
     * @param promotionActivityId 促销活动id
     * @return 返回活动区域对象
     */
    ActivityAreaScope activityAreaScope(Long promotionActivityId);

    List<ActivityAreaScope> activityAreaScope(List<Long> promotionActivityIds);

}
