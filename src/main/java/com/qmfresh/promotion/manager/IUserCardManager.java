package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.UserCard;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
public interface IUserCardManager extends IService<UserCard> {
	
}
