package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.ActivitySsu;
import com.qmfresh.promotion.enums.SsuTagTypeEnums;

import java.util.List;

/**
 * <p>
 * 促销活动ssu 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface IActivitySsuManager extends IService<ActivitySsu> {

    /**
     * 筛选活动对应的skuIds
     *
     * @param promotionActivityIds 活动列表
     * @param skuIds               skuId列表
     * @param ssuTag               0=包含 1=排除
     * @return 筛选后的商品列表
     */
    List<Integer> filterSkuIds(List<Long> promotionActivityIds, List<Integer> skuIds, SsuTagTypeEnums ssuTag);

    List<ActivitySsu> filterActivitySsu(List<Long> promotionActivityIds,List<Integer> skuIds);

    /**
     * 查询促销活动关联的商品列表
     *
     * @param promotionActivityIds 促销活动ids
     * @return 活动商品列表
     */
    List<ActivitySsu> activitySsu(List<Long> promotionActivityIds);


    List<ActivitySsu> activitySsu(List<Long> promotionActivityIds, SsuTagTypeEnums ssuTagTypeEnums);

    List<ActivitySsu> activitySsu(List<Long> promotionActivityIds, Integer ssuFormatId);

    List<ActivitySsu> activitySsu(Long promotionActivityId, List<Integer> skuId, SsuTagTypeEnums tagTypeEnums);

    List<ActivitySsu> activitySSu(List<Long> promotionActivityIds,List<Integer> skuIds);
}
