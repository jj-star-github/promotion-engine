package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.OrdercReturnItem;

/**
 * <p>
 * 退货单详情 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
public interface IOrdercReturnItemManager extends IService<OrdercReturnItem> {
	
}
