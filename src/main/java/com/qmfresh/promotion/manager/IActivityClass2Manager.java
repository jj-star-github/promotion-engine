package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.ActivityClass2;

import java.util.List;

/**
 * <p>
 * 促销活动分类 服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface IActivityClass2Manager extends IService<ActivityClass2> {

    /**
     * 筛选出在促销活动中的类目列表
     *
     * @param promotionActivityIds 活动列表
     * @param classIds             类目id
     * @return 返回筛选后的类目列表
     */
    List<Integer> filterClassIds(List<Long> promotionActivityIds, List<Integer> classIds);

    /**
     * 促销活动的对应活动类目
     *
     * @param promotionActivityIds 促销活动ids
     * @return 促销活动对应的类目
     */
    List<ActivityClass2> activityClass2(List<Long> promotionActivityIds);

    List<ActivityClass2> activityClass2(List<Long> promotionActivityIds, Integer classId);

    List<ActivityClass2> activityClass2(Long promotionActivityId, Integer classId);

    List<ActivityClass2> activityClass2(Long promotionActivityId);


}
