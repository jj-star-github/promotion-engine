package com.qmfresh.promotion.manager;

import com.qmfresh.promotion.bean.price.ModifyVipPriceBean;
import com.qmfresh.promotion.bean.price.ModifyVipPriceResult;
import com.qmfresh.promotion.bean.sku.ProductSsu;
import com.qmfresh.promotion.bean.sku.ProductSsuFormatDto;
import com.qmfresh.promotion.dto.*;
import com.qmfresh.promotion.dto.qmpp.QmppSkuDto;
import com.qmfresh.promotion.entity.*;
import com.qmfresh.promotion.enums.AreaScopeTypeEnums;
import com.qmfresh.promotion.enums.PromotionTypeEnums;
import com.qmfresh.promotion.enums.SsuTagTypeEnums;
import com.qmfresh.promotion.strategy.bean.VipRule;

import java.util.List;

/**
 * Created by wyh on 2019/6/5.
 *
 * @author wyh
 */
public interface IPromotionManager {

    Boolean deleteActivity(long activityId);

    Boolean deleteActivityAllInfo(Long activityId);
    /**
     * 获取促销详情
     *
     * @param activityId
     * @return
     */
    PromotionDto getPromotionDetail(Long activityId);

    /**
     * 校验会员活动时间
     *
     * @param activityTimes
     * @return
     */
    List<PromotionActivity> checkActivityTime(Integer channel, PromotionTypeEnums promotionEnmu,
        List<PromotionActivityTime> activityTimes);

    /**
     * 校验改价活动时间（区分门店和运营）
     *
     * @param activityTimes
     * @return
     */
    List<PromotionActivity> checkActivityTime(Integer channel, PromotionTypeEnums promotionEnmu,
                                              List<PromotionActivityTime> activityTimes,Integer creatorFrom);
//    List<PromotionActivity> checkActivityTime(PromotionTypeEnums promotionEnmu,
//        List<PromotionActivityTime> activityTimes);

    /**
     * 校验是否有重复促销活动
     *
     * @param channel 渠道
     * @param promotionEnmu
     * @param beginTime
     * @param endTime
     * @return 已存在活动列表
     */
    List<PromotionActivity> checkActivityTime(Integer channel, PromotionTypeEnums promotionEnmu, Integer beginTime, Integer endTime);

    /**
     * 校验是否有重复城市
     *
     * @param promotionActivities
     * @param areaDtos
     * @return 已存在活动id
     */
    List<Long> checkActivityShop(List<PromotionActivity> promotionActivities, List<PromotionAreaDto> areaDtos);

    List<PromotionActivity> checkActivityUser(List<PromotionActivity> promotionActivities, List<Integer> crowdTypes);

    /**
     * 校验是否有重复城市区域
     *
     * @param promotionActivities
     * @param areaDtos
     * @return 已存在活动城市id
     */
    List<Long> checkActivityArea(List<PromotionActivity> promotionActivities, List<PromotionAreaDto> areaDtos);

//    /**
//     * 校验活动下是否有重复商品
//     *
//     * @param activityIds
//     * @param ssuId
//     */
//    ProductSsu checkProduct(List<Long> activityIds, Integer ssuId);

    /**
     * 校验是否存在重复分类
     *
     * @param activityIds
     * @param promotionClassDto
     */
    void checkProductClassDto(List<Long> activityIds, PromotionClassDto promotionClassDto);

    /**
     * 校验是否有重复商品
     *
     * @param activityIds
     * @param productSsuFormatDtos
     */
    void checkProduct(List<Long> activityIds, List<ProductSsuFormatDto> productSsuFormatDtos);

    /**
     * 创建促销基本信息
     *
     * @param promotionType
     * @param channel
     * @param name
     * @param userId
     * @param userName
     * @return
     */
    PromotionActivity createPromotionActivity(PromotionTypeEnums promotionType, int channel, String name, Long userId,
        String userName,Integer creatorFrom);

    /**
     * 创建促销时间信息
     *
     * @param promotionType
     * @param promotionActivity
     * @param beginTime
     * @param endTime
     */
    void createPromotionActivityTime(PromotionTypeEnums promotionType, PromotionActivity promotionActivity,
        int beginTime, int endTime);

    /**
     * 创建促销时间信息
     *
     * @param promotionType
     * @param promotionActivity
     * @param activityTimes
     */
    void createPromotionActivityTime(PromotionTypeEnums promotionType, PromotionActivity promotionActivity,
        List<PromotionActivityTime> activityTimes);

//    /**
//     * 创建促销区域
//     *
//     * @param promotionActivity
//     * @param areaDtos
//     */
//    void createPromotionArea(PromotionActivity promotionActivity, List<PromotionAreaDto> areaDtos);

    /**
     * 创建促销用户作用域
     *
     * @param promotionActivity
     * @param crowdTypes
     */
    void createPromotionUser(PromotionActivity promotionActivity, List<Integer> crowdTypes);

    void createPromotionShop(PromotionActivity promotionActivity, List<PromotionAreaDto> areaDtos);

    void createPromotionProduct(long activityId, long ruleId, int channel, PromotionProductDto productDto);

    List<ProductSsuFormatDto> getProductSsuFormat(List<Integer> ssuFormatIds);

    void createPromotionClass(long activityId, long ruleId, int channel, PromotionClassDto promotionClassDto);

    /**
     * 创建促销商品
     *
     * @param activityId 活动id
     * @param ruleId 规则id
     * @param channel 渠道
     * @param ssuTagType 排除标识：0.包含、1.排除
     * @param ssuFormatIds ssu列表信息
     */
    void createPromotionSsu(long activityId, long ruleId, int channel, SsuTagTypeEnums ssuTagType, List<Integer> ssuFormatIds);
    /**
     * 创建改价促销商品
     *
     * @param activityId 活动id
     * @param ruleId 规则id
     * @param channel 渠道
     * @param ssuTagType 排除标识：0.包含、1.排除
     * @param productDto 促销商品实体
     */
    void createCpPromotionSsu(long activityId, long ruleId, int channel, SsuTagTypeEnums ssuTagType,PromotionProductDto productDto);
    /**
     * 创建促销规则
     *
     * @param promotionType 促销类型
     * @param promotionActivity 促销活动信息
     * @param areaScopeType 区域类型
     * @param productScopeType 商品类型
     * @param rule 促销规则
     * @return
     */
    PromotionRule createPromotionRule(PromotionTypeEnums promotionType, PromotionActivity promotionActivity,
        AreaScopeTypeEnums areaScopeType, int productScopeType, Object rule);

    /**
     * 修改会员价格 todo 根据ssuId找到ssuFormatId，查找到对应会员折扣，再进行改价
     *
     * @param modifyVipPriceBean
     * @param productSsu
     * @return
     */
    ModifyVipPriceResult modifyVipPrice(ModifyVipPriceBean modifyVipPriceBean, ProductSsu productSsu);

    ModifyVipPriceResult modifyVipPrice(ModifyVipPriceBean modifyVipPriceBean, ProductSsuFormatDto ssuFormat);
//    /**
//     * 校验分类
//     *
//     * @param promotionClassDto
//     */
//    void checkPromotionClassWithDp(PromotionClassDto promotionClassDto);

//    /**
//     * 校验分类
//     *
//     * @param productSsuFormatDtos
//     */
//    void checkProductWithVip(List<ProductSsuFormatDto> productSsuFormatDtos);

    /**
     * 根据门店id获取活动列表
     * @param shopId
     * @return
     */
    List<PromotionActivity> getActivityInfoByShopInfo(Integer shopId,List<PromotionTypeEnums> enums);

    List<QueryPriceChangeResult> getActivitySsuList(Long activityId,Integer shopId,List<Integer> list);

    List<QueryPriceChangeResult> getActivityClass2List(Long activityId,Integer shopId,List<PriceChangeParam> skuList);

    List<SendVipPriceParam> computeVipPrice(List<QmppSkuDto> skuList, VipRule vipRule);

    /**
     * 校验是否有重复商品
     *
     * @param activityIds
     * @param productSsuFormatDtos
     */
    List<ProductSsuFormatDto> checkProductWithResult(List<Long> activityIds, List<ProductSsuFormatDto> productSsuFormatDtos);


    /**
     * 校验商品是否参加会员活动
     * @param list
     * @return
     */
    List<CreateCpFromShopParam> checkSkuInVipPromotion(List<CreateCpFromShopParam> list);
}
