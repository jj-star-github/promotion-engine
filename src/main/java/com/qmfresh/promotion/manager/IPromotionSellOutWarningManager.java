package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.bean.promotion.PromotionSellOutSkuWarningBean;
import com.qmfresh.promotion.dto.PromotionSellOutWarningReq;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.PromotionSellOutSkuWarning;

import java.util.List;

public interface IPromotionSellOutWarningManager extends IService<PromotionSellOutSkuWarning> {

    Boolean add(PromotionSellOutWarningReq req);

    ListWithPage<PromotionSellOutSkuWarningBean> search(PromotionSellOutWarningReq req);

    Boolean update(PromotionSellOutWarningReq req);

    Boolean setShopWarning(PromotionSellOutWarningReq req);

    List<PromotionSellOutSkuWarningBean> matchByTime(String time, Integer shopId);

    List<PromotionSellOutSkuWarningBean> matchById(List<Integer> ids);
}
