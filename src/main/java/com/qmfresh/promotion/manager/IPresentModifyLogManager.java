package com.qmfresh.promotion.manager;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.entity.PresentModifyLog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-25
 */
public interface IPresentModifyLogManager extends IService<PresentModifyLog> {
	
}
