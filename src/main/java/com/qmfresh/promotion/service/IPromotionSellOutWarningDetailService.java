package com.qmfresh.promotion.service;

import com.qmfresh.promotion.bean.promotion.PromotionSellOutWarningDetailBean;
import com.qmfresh.promotion.dto.QueryPromotionSellOutWarningDetailParam;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.PromotionSellOutWarningDetail;

import java.util.List;

/**
 * @program: promotion-engine
 * @description:
 * @author: xbb
 * @create: 2019-11-23 14:21
 **/
public interface IPromotionSellOutWarningDetailService {

    ListWithPage<PromotionSellOutWarningDetailBean> getPageList(QueryPromotionSellOutWarningDetailParam param);

    Boolean insert(List<PromotionSellOutWarningDetail> details);

    Boolean update(List<PromotionSellOutWarningDetail> details);

    /**
     * 查询昨天出清预警详情表发送门店改价成功的数据
     * @return
     */
    List<PromotionSellOutWarningDetail> queryWarningDetailList();
}
