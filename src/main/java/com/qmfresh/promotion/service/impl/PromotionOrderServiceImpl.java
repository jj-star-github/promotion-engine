package com.qmfresh.promotion.service.impl;

import com.qmfresh.promotion.bean.order.PromotionOrderDto;
import com.qmfresh.promotion.bean.order.QueryOrderParam;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.entity.PromotionOrderItem;
import com.qmfresh.promotion.manager.IPromotionOrderManager;
import com.qmfresh.promotion.service.IPromotionOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wyh on 2019/6/14.
 *
 * @author wyh
 */
@Service
@Slf4j
public class PromotionOrderServiceImpl implements IPromotionOrderService {

    @Resource
    private IPromotionOrderManager promotionOrderManager;

    @Override
    public boolean createPromotionOrder(PromotionOrderDto param) {
        Assert.notNull(param.getOrderId(), "订单id不能为空");
        Assert.notNull(param.getChannel(), "渠道不能为空");
        Assert.notNull(param.getUserId(), "用户id不能为空");
        Assert.notNull(param.getShopId(), "门店id不能为空");
        Assert.notNull(param.getCreateDate(), "创建日期不能为空");
        Assert.notNull(param.getOccurrenceTime(), "业务发生时间不能为空");
        Assert.notNull(param.getOrderType(), "订单类型不能为空");
        Assert.notNull(param.getStatus(), "订单状态不能为空");
        Assert.notNull(param.getOriginTotalPrice(), "原金额不能为空");
        Assert.notNull(param.getPreferentialPrice(), "优惠金额不能为空");
        Assert.notNull(param.getTotalPrice(), "总金额不能为空");
        if (ListUtil.isNullOrEmpty(param.getItems())) {
            return false;
        }
        param.getItems().forEach(
            item -> {
                Assert.notNull(item.getOrderItemId(), "订单行id不能为空");
                Assert.notNull(item.getSkuId(), "商品id不能为空");
                Assert.notNull(item.getSkuName(), "商品名称不能为空");
                Assert.notNull(item.getAmount(), "订单量不能为空");
                Assert.notNull(item.getUnitPrice(), "单价不能为空");
                Assert.notNull(item.getPrice(), "订单行总价不能为空");
                Assert.notNull(item.getIsPresent(), "是否赠品不能为空");
            }
        );
        return promotionOrderManager.createPromotionOrder(param);
    }

    @Override
    public List<PromotionOrderItem> queryOrderItem(QueryOrderParam queryOrderParam) {
        return promotionOrderManager.queryOrderItem(queryOrderParam);
    }
}
