package com.qmfresh.promotion.service.impl;

import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.enums.ActivityStatusTypeEnums;
import com.qmfresh.promotion.manager.IPromotionProductMarkManager;
import com.qmfresh.promotion.service.IPromotionProductMarkService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wyh on 2019/7/29.
 *
 * @author wyh
 */
@Service
@Slf4j
public class PromotionProductMarkServiceImpl implements IPromotionProductMarkService {

    @Resource
    private IPromotionProductMarkManager productMarkManager;

    @Override
    public Boolean markProduct(Long activityId, ActivityStatusTypeEnums status) {
        Assert.notNull(activityId, "活动id不能为空");
        Assert.isTrue(status != null, "活动状态不能为空");
        return productMarkManager.markProduct(activityId, status);
    }

    @Override
    public Boolean markProduct(Integer channel, Integer ssuFormatId, Integer class2Id, List<Integer> shopIds) {
        Assert.notNull(channel, "渠道不能为空");
        Assert.notNull(ssuFormatId, "ssu商品不能为空");
        Assert.notNull(class2Id, "ssu商品分类不能为空");
        Assert.isTrue(!ListUtil.isNullOrEmpty(shopIds), "门店不能为空");
        return productMarkManager.markProduct(channel, ssuFormatId, class2Id, shopIds);
    }
}
