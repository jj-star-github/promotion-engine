package com.qmfresh.promotion.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.ImmutableList;
import com.qmfresh.promotion.bean.gis.Shop;
import com.qmfresh.promotion.bean.price.*;
import com.qmfresh.promotion.bean.sku.QueryProductSsuPrice;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.ModifyPriceParam;
import com.qmfresh.promotion.dto.SendPromotionPriceParam;
import com.qmfresh.promotion.dto.SendVipPriceParam;
import com.qmfresh.promotion.dto.qmpp.QmppSkuDto;
import com.qmfresh.promotion.entity.*;
import com.qmfresh.promotion.enums.*;
import com.qmfresh.promotion.external.service.ICmallExternalApiService;
import com.qmfresh.promotion.external.service.IModifyPriceExternalApiService;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.manager.*;
import com.qmfresh.promotion.message.producer.ProducerDispatcher;
import com.qmfresh.promotion.service.IPromotionTaskServie;
import com.qmfresh.promotion.strategy.bean.VipRule;
import com.qmfresh.promotion.strategy.helper.DiscountPriceHelper;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/6/12.
 *
 * @author wyh
 */
@Service
@Slf4j
public class PromotionTaskServiceImpl implements IPromotionTaskServie {
    private static final Logger logger = LoggerFactory.getLogger(PromotionTaskServiceImpl.class);

    @Resource
    private IPromotionActivityManager promotionActivityManager;

    @Resource
    private IPromotionActivityTimeManager promotionActivityTimeManager;

    @Resource
    private IPromotionRuleManager promotionRuleManager;

    @Resource
    private IActivityClass2Manager activityClass2Manager;

    @Resource
    private IActivitySsuManager activitySsuManager;

    @Resource
    private IAreaScopeShopManager areaScopeShopManager;

    @Resource
    private IQmExternalApiService qmExternalApiService;

    @Resource
    private ICmallExternalApiService cmallExternalApiService;

    @Resource
    private ProducerDispatcher producerDispatcher;

    @Resource
    private IModifyPriceExternalApiService modifyPriceExternalApiService;

    @Override
    public Boolean createCmallVipPrice() {
        // 获取当前生效促销活动
        List<Long> activityIds = getCurrentVipPromotionActivity(ChannelTypeEnums.C_ONLINE);
        if (ListUtil.isNullOrEmpty(activityIds)) {
            return true;
        }

        Map<Long, List<AreaScopeShop>> activityShopMap = areaScopeShopManager.areaScopeShop(activityIds)
                .stream().collect(Collectors.groupingBy(AreaScopeShop::getActivityId));

        Map<Long, List<ActivityClass2>> activityClass2Map = activityClass2Manager.activityClass2(activityIds).stream().collect(Collectors.groupingBy(ActivityClass2::getActivityId));

        List<ActivitySsu> activitySsus = activitySsuManager.activitySsu(activityIds);
        Map<Long, List<ActivitySsu>> activitySsuMap = !ListUtil.isNullOrEmpty(activitySsus) ? activitySsus.stream().collect(Collectors.groupingBy(ActivitySsu::getActivityId)) :
                new HashMap<>(2);

        List<PromotionRule> promotionRules = promotionRuleManager.promotionRules(activityIds, PromotionTypeEnums.VIP);
        Map<String, PromotionRule> promotionRuleMap = ListUtil.listToMap("activityId", promotionRules);

        activityIds.forEach(
                activityId -> this.createCmallVipPrice(activityShopMap.get(activityId), activityClass2Map.get(activityId), activitySsuMap.get(activityId),
                        promotionRuleMap.get(activityId.toString()))
        );
        return true;
    }

    @Override
    public Boolean createCpPrice() {

        List<PromotionActivity> activityList = getCurrentCpPromotionActivity();
        if (CollectionUtils.isEmpty(activityList)) {
            return true;
        }
        List<Long> activityIds = activityList.stream().map(PromotionActivity::getId).collect(Collectors.toList());
        List<AreaScopeShop> areaScopeShopList = areaScopeShopManager.
                areaScopeShop(activityIds);

        if (CollectionUtils.isEmpty(areaScopeShopList)) {
            logger.info("当前时间没有参与改价促销的门店");
            return true;
        }
        List<AreaScopeShop> newAreaScopeShopList = new ArrayList<>();
        List<Integer> whiteShopIds = new ArrayList<>();
        Shop shop = new Shop();
        shop.setPromotionGrayType(1);
        List<Shop> shopList = qmExternalApiService.getShopByCondition(shop);
        if (!CollectionUtils.isEmpty(shopList)) {
            shopList.forEach(shop1 -> {
                whiteShopIds.add(shop1.getId().intValue());
            });
        }
        for (AreaScopeShop scopeShop : areaScopeShopList) {
                if (whiteShopIds.contains(scopeShop.getShopId())) {
                    continue;
                }
            newAreaScopeShopList.add(scopeShop);
        }
        if (CollectionUtils.isEmpty(newAreaScopeShopList)) {
            logger.info("当前时间没有参与改价促销的门店-门店白名单过滤后");
            return true;
        }

        List<ActivitySsu> activitySsuList = activitySsuManager.
                activitySsu(activityIds, SsuTagTypeEnums.CONTAIN);
        if (CollectionUtils.isEmpty(activitySsuList)) {
            logger.info("当前时间没有参与改价促销的商品");
            return true;
        }
        //门店和门店活动mapping
        Map<Integer, List<AreaScopeShop>> shopIdMapActivity = newAreaScopeShopList.stream().collect(Collectors.groupingBy(AreaScopeShop::getShopId));
        //活动和商品mapping
        Map<Long, List<ActivitySsu>> activitySsuMap = activitySsuList.stream().collect(Collectors.groupingBy(ActivitySsu::getActivityId));
        //活动id和活动内容mapping
        Map<Long, PromotionActivity> activityIdMappingActivityName = activityList.stream().collect(Collectors.toMap(PromotionActivity::getId, Function.identity()));
        if (shopIdMapActivity == null || activitySsuMap == null) {
            logger.info("没有找到改价信息");
            return true;
        }
        //编辑改价促销信息，并分批发送
        this.sendCpPromotionInfo(shopIdMapActivity, activitySsuMap, activityIdMappingActivityName);

        return true;
    }

    @Override
    public Boolean createVipPrice() {

        List<Long> activityIds = getCurrentVipPromotionActivity(ChannelTypeEnums.C_OFFLINE);
        if (ListUtil.isNullOrEmpty(activityIds)) {
            return true;
        }

        Map<Long, List<AreaScopeShop>> activityShopMap = areaScopeShopManager
                .areaScopeShop(activityIds).stream().collect(Collectors.groupingBy(AreaScopeShop::getActivityId));

//        Map<Long, List<ActivityClass2>> activityClass2Map = activityClass2Manager
//                .activityClass2(activityIds)
//                .stream().collect(Collectors.groupingBy(ActivityClass2::getActivityId));

        List<ActivitySsu> activitySsus = activitySsuManager
                .activitySsu(activityIds);
        Map<Long, List<ActivitySsu>> activitySsuMap = !ListUtil.isNullOrEmpty(activitySsus) ? activitySsus.stream().collect(Collectors.groupingBy(ActivitySsu::getActivityId)) :
                new HashMap<>(2);

        List<PromotionRule> promotionRules = promotionRuleManager.
                promotionRules(activityIds,PromotionTypeEnums.VIP);
        Map<String, PromotionRule> promotionRuleMap = ListUtil.listToMap("activityId", promotionRules);

        activityIds.forEach(
                activityId -> this.createVipPrice(activityShopMap.get(activityId), activitySsuMap.get(activityId),
                        promotionRuleMap.get(activityId.toString()))
        );
        return true;
    }

    private void createCmallVipPrice(List<AreaScopeShop> areaScopeShops, List<ActivityClass2> activityClass2s, List<ActivitySsu> activitySsus, PromotionRule promotionRule) {
        CMallPriceQuery cMallPriceQuery = new CMallPriceQuery();
        List<Integer> shopIds = areaScopeShops.stream().map(AreaScopeShop::getShopId).collect(Collectors.toList());
        cMallPriceQuery.setShopIds(shopIds);
        // 分类促销
        if (promotionRule.getProductScope().equals(ProductScopeTypeEnums.CLASS2.getCode())) {
            List<Integer> class2Ids = activityClass2s.stream().map(ActivityClass2::getClassId).collect(Collectors.toList());
            cMallPriceQuery.setClass2Ids(class2Ids);
            if (!ListUtil.isNullOrEmpty(activitySsus)) {
                List<Integer> exSsuFormatIds =
                        activitySsus.stream().filter(item -> item.getExTag().equals(SsuTagTypeEnums.EXCLUSIVE.getCode())).map(ActivitySsu::getSsuFormatId).collect(Collectors.toList());
                if (!ListUtil.isNullOrEmpty(exSsuFormatIds)) {
                    cMallPriceQuery.setExSsuFormatIds(exSsuFormatIds);
                }
            }
        }

        // 单品促销
        if (promotionRule.getProductScope().equals(ProductScopeTypeEnums.SSU.getCode())) {
            if (!ListUtil.isNullOrEmpty(activitySsus)) {
                List<Integer> ssuFormatIds =
                        activitySsus.stream().filter(item -> item.getExTag().equals(SsuTagTypeEnums.CONTAIN.getCode())).map(ActivitySsu::getSsuFormatId).collect(Collectors.toList());
                if (!ListUtil.isNullOrEmpty(ssuFormatIds)) {
                    cMallPriceQuery.setSsuFormatIds(ssuFormatIds);
                }
            }
        }
        // 查看C端商城ssu原价
        cMallPriceQuery.setStatus(2);
        List<CMallPriceInfo> cMallPriceInfos = cmallExternalApiService.queryCmallPriceList(cMallPriceQuery);

        // 创建并发送改价消息
        this.createCmallVipPrice(promotionRule, cMallPriceInfos);
    }

    private void createVipPrice(List<AreaScopeShop> areaScopeShops, List<ActivitySsu> activitySsus, PromotionRule promotionRule) {

        // 分类促销 SSU是排除
//        if (promotionRule.getProductScope().equals(ProductScopeTypeEnums.CLASS2.getCode())) {
//            Map<Integer, List<ActivitySsu>> c2SsuMap = !ListUtil.isNullOrEmpty(activitySsus) ?
//                    activitySsus.stream().filter(item -> item.getExTag().equals(SsuTagTypeEnums.EXCLUSIVE.getCode())).collect(Collectors.groupingBy(ActivitySsu::getClass2Id)) :
//                    new HashMap<>(1);
//            for (ActivityClass2 ac2 : activityClass2s) {
//                this.getShopProductSsuPriceList(promotionRule, ac2.getClassId(), c2SsuMap.getOrDefault(ac2.getClassId(), new ArrayList<>(16)),
//                        areaScopeShops.stream().map(AreaScopeShop::getShopId).collect(Collectors.toList()),
//                        100, 1);
//            }
//        }

        // 单品促销
        if (!promotionRule.getProductScope().equals(ProductScopeTypeEnums.SSU.getCode())) {
            logger.info("VIP改价通知-暂只支持单品促销 activityId={}", promotionRule.getActivityId());
            return;
        }
        this.getShopProductSsuPriceList(promotionRule, activitySsus, areaScopeShops.stream().map(AreaScopeShop::getShopId).collect(Collectors.toList()));

    }

    /**
     * 处理单品会员促销，按门店商品修改会员价格
     *
     * @param promotionRule 促销规则
     * @param activitySsus  单品促销商品列表
     * @param shopIds       门店
     */
    private void getShopProductSsuPriceList(PromotionRule promotionRule, List<ActivitySsu> activitySsus, List<Integer> shopIds) {
        if (CollectionUtils.isEmpty(activitySsus)) {
            logger.info("VIP改价通知-活动商品信息为空 activityId={}", promotionRule.getActivityId());
            return;
        }
        List<Integer> skuIds = activitySsus.stream().map(ActivitySsu::getSkuId).collect(Collectors.toList());
        List<QmppSkuDto> modifyVipList = new ArrayList<>();
        for(Integer shopId : shopIds) {
            QueryProductSsuPrice productSsuPrice = new QueryProductSsuPrice();
            productSsuPrice.setShopId(shopId);
            productSsuPrice.setSkuIds(skuIds);
            List<QmppSkuDto> skuDtoList = qmExternalApiService.getProductSsuPriceListByCondition(productSsuPrice);
            modifyVipList.addAll(skuDtoList);
        }
        this.sendChangeVipPriceInfo(modifyVipList, promotionRule);
    }

    private void sendChangeVipPriceInfo(List<QmppSkuDto> qmppSkuDtoList, PromotionRule promotionRule) {
        if (CollectionUtils.isEmpty(qmppSkuDtoList)) {
            logger.warn("会员价修改信息为空-门店商品信息为空");
            return;
        }
        List<SendVipPriceParam> sendVipPriceParamList = qmppSkuDtoList.stream().map(o -> bindVipPriceParam(promotionRule, o)).filter(f -> f != null).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(sendVipPriceParamList)) {
            logger.warn("会员价修改信息为空");
            return;
        }
        ModifyPriceParam<SendVipPriceParam> modifyPriceParam = new ModifyPriceParam<>();
        modifyPriceParam.setReqList(sendVipPriceParamList);
        logger.info("定时同步会员价格，参数：" + JSON.toJSONString(modifyPriceParam));
        modifyPriceExternalApiService.modifyVipPrice(modifyPriceParam);
        logger.info("定时同步会员价格结束");
    }

    private SendVipPriceParam bindVipPriceParam(PromotionRule promotionRule, QmppSkuDto skuDto) {
        if (skuDto.getAdvisePrice().compareTo(new BigDecimal("0")) == 0) {
            logger.info("原价为0无法同步会员价,{}", JSON.toJSONString(skuDto));
            return null;
        }
        VipRule vipRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<VipRule>() {
        });
        BigDecimal price = skuDto.getAdvisePrice().multiply(new BigDecimal("1000"));
        BigDecimal modifyPrice = DiscountPriceHelper.getDiscountPrice(price, vipRule.getDiscountRule());
        if (modifyPrice.compareTo(BigDecimal.ZERO) <= 0) {
            logger.error("改价金额小于0，不发送改价消息,price:" + price + ",vipRule:" + JSON.toJSONString(vipRule));
            return null;
        }
        SendVipPriceParam sendVipPriceParam = new SendVipPriceParam();
        sendVipPriceParam.setVipPrice(modifyPrice);
        sendVipPriceParam.setShopId(skuDto.getShopId());
        sendVipPriceParam.setSkuId(skuDto.getSkuId().intValue());
        sendVipPriceParam.setUserId(0L);
        sendVipPriceParam.setUserName("定时系统");
        return sendVipPriceParam;
    }

    private void createVipPrice(PromotionRule promotionRule, ProductSsuPriceResult productSsuPrice) {
        if (productSsuPrice.getPrice().compareTo(0L) == 0) {
            return;
        }
        VipRule vipRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<VipRule>() {
        });
        BigDecimal price = BigDecimal.valueOf(productSsuPrice.getPrice()).divide(BigDecimal.valueOf(1000), 3, RoundingMode.HALF_UP);
        BigDecimal modifyPrice = DiscountPriceHelper.getDiscountPrice(price, vipRule.getDiscountRule());
        if (modifyPrice.compareTo(BigDecimal.ZERO) <= 0) {
            logger.error("改价金额小于0，不发送改价消息,price:" + price + ",vipRule:" + JSON.toJSONString(vipRule));
            return;
        }

        ModifyPriceParam<SendVipPriceParam> modifyPriceParam = new ModifyPriceParam<>();
        SendVipPriceParam sendVipPriceParam = new SendVipPriceParam();
        List<SendVipPriceParam> sendVipPriceParamList = new ArrayList<>();
        sendVipPriceParam.setPrice(price.setScale(2, RoundingMode.HALF_UP));
        sendVipPriceParam.setVipPrice(DiscountPriceHelper.getDiscountPrice(price, vipRule.getDiscountRule()));
        sendVipPriceParamList.add(sendVipPriceParam);
        modifyPriceParam.setReqList(sendVipPriceParamList);
        logger.info("定时同步会员价格开始");
        logger.info("定时同步会员价格，参数：" + JSON.toJSONString(modifyPriceParam));
        modifyPriceExternalApiService.modifyVipPrice(modifyPriceParam);
        logger.info("定时同步会员价格结束");

    }

    private void createCmallVipPrice(PromotionRule promotionRule, List<CMallPriceInfo> priceInfos) {
        VipRule vipRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<VipRule>() {
        });

        List<ModifyPriceResult> modifyPriceResults = new ArrayList<>();
        for (CMallPriceInfo priceInfo : priceInfos) {
            if (priceInfo.getPrice().compareTo(BigDecimal.ZERO) == 0) {
                continue;
            }
//            BigDecimal price = priceInfo.getPrice().divide(BigDecimal.valueOf(1000), 3, RoundingMode.HALF_UP);
            BigDecimal modifyPrice = DiscountPriceHelper.getDiscountPrice(priceInfo.getPrice(), vipRule.getDiscountRule());
            if (modifyPrice.compareTo(BigDecimal.ZERO) <= 0) {
                logger.error("改价金额小于0，不发送改价消息,price:" + priceInfo.getPrice() + ",vipRule:" + JSON.toJSONString(vipRule));
                continue;
            }

            ModifyPriceResult modifyPriceResult = new ModifyPriceResult();
            modifyPriceResult.setPrice(priceInfo.getPrice());
            modifyPriceResult.setModifyPrice(DiscountPriceHelper.getDiscountPrice(priceInfo.getPrice(), vipRule.getDiscountRule()));
            modifyPriceResult.setSsuFormatId(priceInfo.getSsuFormatId());
            modifyPriceResult.setShopId(priceInfo.getShopId());
            modifyPriceResult.setChannel(ChannelTypeEnums.C_ONLINE.getCode());
            modifyPriceResult.setPriceType(PriceTypeEnums.VIP.getCode());
            modifyPriceResult.setStatus(1);
            modifyPriceResults.add(modifyPriceResult);
        }
        List<List<ModifyPriceResult>> choppedList = ListUtil.chopped(modifyPriceResults, 100);
        choppedList.forEach(
                list -> producerDispatcher.cMallPriceModifyNotify(list)
        );
    }

    private List<PromotionActivity> getCurrentCpPromotionActivity() {
        int now = DateUtil.getCurrentTimeIntValue();
        List<PromotionActivityTime> promotionActivityTimes = promotionActivityTimeManager.getCurrentActiveAct(ImmutableList.of(PromotionTypeEnums.CP.getCode()), now, now);

        if (ListUtil.isNullOrEmpty(promotionActivityTimes)) {
            logger.info("当前时间无改价促销活动");
            return null;
        }

        List<Long> activityIds = promotionActivityTimes.stream().map(PromotionActivityTime::getActivityId).collect(Collectors.toList());
        List<PromotionActivity> promotionActivities = promotionActivityManager.
                filterPromotionActivity(activityIds,null,ActivityStatusTypeEnums.STARTED,PromotionTypeEnums.CP, null);

        if (ListUtil.isNullOrEmpty(promotionActivities)) {
            logger.info("当前时间无改价促销活动");
            return null;
        }

        return promotionActivities;
    }

    private List<Long> getCurrentVipPromotionActivity(ChannelTypeEnums channel) {
        int now = DateUtil.getCurrentTimeIntValue();
        int weekOfDay = DateUtil.getWeekByDate(new Date());
        List<PromotionActivityTime> promotionActivityTimes = promotionActivityTimeManager.list(new LambdaQueryWrapper<PromotionActivityTime>()
                .eq(PromotionActivityTime::getPromotionModuleId, PromotionTypeEnums.VIP.getCode())
                .le(PromotionActivityTime::getEffectBeginTime, now)
                .gt(PromotionActivityTime::getEffectEndTime, now)
                .like(PromotionActivityTime::getWeekDay, String.valueOf(weekOfDay))
                .eq(PromotionActivityTime::getIsDeleted, 0)
        );

        if (ListUtil.isNullOrEmpty(promotionActivityTimes)) {
            logger.info("当前时间无会员促销活动");
            return null;
        }

        List<Long> activityIds = promotionActivityTimes.stream().map(PromotionActivityTime::getActivityId).collect(Collectors.toList());
        List<PromotionActivity> promotionActivities = promotionActivityManager.
                filterPromotionActivity(activityIds,null,ActivityStatusTypeEnums.STARTED,PromotionTypeEnums.VIP,channel);
        if (ListUtil.isNullOrEmpty(promotionActivities)) {
            logger.info("当前时间无会员促销活动");
            return null;
        }

        return promotionActivities.stream().map(PromotionActivity::getId).collect(Collectors.toList());
    }

    private void sendCpPromotionInfo(Map<Integer, List<AreaScopeShop>> shopIdMapActivity, Map<Long, List<ActivitySsu>> activitySsuMap, Map<Long, PromotionActivity> activityIdMappingActivity) {
        logger.info("定时同步改价促销价，开始");
        //组装shopId和ssu
        for (Map.Entry<Integer, List<AreaScopeShop>> shopActivitiesMapping : shopIdMapActivity.entrySet()) {
            if (CollectionUtils.isEmpty(shopActivitiesMapping.getValue())) {
                continue;
            }
            //获取门店下的所有改价sku
            List<Long> activityIdList = shopActivitiesMapping.getValue().stream().map(AreaScopeShop::getActivityId).collect(Collectors.toList());
            Integer shopId = shopActivitiesMapping.getKey();
            ModifyPriceParam<SendPromotionPriceParam> sendInfo = new ModifyPriceParam<>();
            List<SendPromotionPriceParam> realInfo = new ArrayList<>();

            for (Long activityId : activityIdList) {
                List<ActivitySsu> skuList = activitySsuMap.get(activityId);
                if (CollectionUtils.isEmpty(skuList)) {
                    logger.info("定时批量同步改价促销价格为空，参数活动ID：" + activityId);

                    continue;
                }
                logger.info("定时批量同步改价促销价格-封装请求数据, activityId={} shopId={} skuList={}", activityId, shopId, JSON.toJSONString(skuList));
                for (ActivitySsu activitySsu : skuList) {
                    SendPromotionPriceParam cpParam = new SendPromotionPriceParam();
                    cpParam.setSkuId(activitySsu.getSkuId());
                    cpParam.setPrice(activitySsu.getPromotionPrice());
                    cpParam.setShopId(shopId);
                    cpParam.setReasonType(CreatorFromValueTranslateEnums.HEAD_OFFICE_TO_MID_PLATFORM.getCodeTwo());
                    cpParam.setUserName(getCreatorName(activityIdMappingActivity, activityId));
                    cpParam.setUserId(getCreatorId(activityIdMappingActivity, activityId));
                    realInfo.add(cpParam);
                }
            }
            sendInfo.setReqList(realInfo);
            logger.info("定时批量同步改价促销价格，参数：" + JSON.toJSONString(sendInfo));
            modifyPriceExternalApiService.modifyPromotionPrice(sendInfo);
        }
        logger.info("定时同步改价促销价，结束");
    }

    private String getCreatorName(Map<Long, PromotionActivity> activityIdMappingActivity, Long activityId) {
        String creatorName = activityIdMappingActivity.get(activityId).getCreateUserName();
        Boolean flag = activityIdMappingActivity.get(activityId) != null && !StringUtils.isEmpty(creatorName);
        return flag ? activityIdMappingActivity.get(activityId).getCreateUserName() : "系统";
    }

    private Integer getCreatorId(Map<Long, PromotionActivity> activityIdMappingActivity, Long activityId) {
        Long creatorId = activityIdMappingActivity.get(activityId).getCreateUserId();
        Boolean flag = activityIdMappingActivity.get(activityId) != null && creatorId != null;
        return flag ? creatorId.intValue() : -1;
    }
}
