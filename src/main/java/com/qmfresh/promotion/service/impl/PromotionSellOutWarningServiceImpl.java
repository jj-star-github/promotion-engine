package com.qmfresh.promotion.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qmfresh.promotion.bean.gis.Shop;
import com.qmfresh.promotion.bean.promotion.PromotionSellOutSkuWarningBean;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.common.utils.SpringContextUtil;
import com.qmfresh.promotion.dto.PromotionSellOutWarningReq;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.PromotionSellOutSkuWarning;
import com.qmfresh.promotion.entity.PromotionSellOutWarningDetail;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.manager.IPromotionSellOutWarningDetailManager;
import com.qmfresh.promotion.manager.IPromotionSellOutWarningManager;
import com.qmfresh.promotion.service.IPromotionSellOutWarningService;
import com.qmfresh.promotion.thread.CreateSellOutWarningDetailThread;
import com.qmfresh.promotion.thread.PushSellOutWarningDetailToShopThread;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PromotionSellOutWarningServiceImpl implements IPromotionSellOutWarningService {

    @Autowired
    private IPromotionSellOutWarningManager promotionSellOutWarningManager;
    @Autowired
    private IPromotionSellOutWarningDetailManager promotionSellOutWarningDetailManager;

    @Autowired
    private IQmExternalApiService iQmExternalApiService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean add(PromotionSellOutWarningReq req) {
        return promotionSellOutWarningManager.add(req);
    }

    @Override
    public ListWithPage<PromotionSellOutSkuWarningBean> search(PromotionSellOutWarningReq req) {
        return promotionSellOutWarningManager.search(req);
    }

    @Override
    public Boolean update(PromotionSellOutWarningReq req) {
        return promotionSellOutWarningManager.update(req);
    }

    @Override
    public Boolean setShopWarning(PromotionSellOutWarningReq req) {
        return promotionSellOutWarningManager.setShopWarning(req);
    }

    @Override
    public Boolean updateSkuWarning(List<PromotionSellOutSkuWarning> warnings) {
        return promotionSellOutWarningManager.updateBatchById(warnings);
    }

    @Override
    public void createWarningDetail(String time) {
        // 查询所有门店信息
        Shop shop = new Shop();
        shop.setShopType(1);
        List<Shop> shopList = iQmExternalApiService.getShopByCondition(shop);
        if (ListUtil.isNullOrEmpty(shopList)) {
            log.warn("未获取到门店列表");
        }
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(1);
        pool.setMaxPoolSize(1);
        pool.initialize();
        try {
            final CountDownLatch latch = new CountDownLatch(shopList.size());
            for (Shop shopBean : shopList) {
                CreateSellOutWarningDetailThread task = SpringContextUtil.getBean("createSellOutWarningDetailThread");
                task.setShop(shopBean);
                task.setTime(time);
                task.setLatch(latch);
                pool.execute(task);
            }
            latch.await();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.shutdown();
        }
    }

    @Override
    public void pushWarningDetailToShop() {
        //查询所有没有发送给门店的预警详情,每个门店分批推送
        List<PromotionSellOutWarningDetail> source = promotionSellOutWarningDetailManager.list(new LambdaQueryWrapper<PromotionSellOutWarningDetail>()
                .eq(PromotionSellOutWarningDetail::getWarningStatus, 0)
                .eq(PromotionSellOutWarningDetail::getIsDeleted, 0)
        );
        //按门店分组
        Map<Integer, List<PromotionSellOutWarningDetail>> map = source.stream().collect(Collectors.groupingBy(PromotionSellOutWarningDetail::getShopId));
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(10);
        pool.setMaxPoolSize(10);
        pool.initialize();
        try {
            final CountDownLatch latch = new CountDownLatch(map.keySet().size());
            List<PromotionSellOutWarningDetail> subList;
            for (Integer shopId : map.keySet()) {
                if (map.containsKey(shopId)) {
                    subList = map.get(shopId);
                    PushSellOutWarningDetailToShopThread task = SpringContextUtil.getBean("pushSellOutWarningDetailToShopThread");
                    task.setShopId(shopId);
                    task.setDetails(subList);
                    task.setLatch(latch);
                    pool.execute(task);
                }
            }
            latch.await();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.shutdown();
        }
    }


}
