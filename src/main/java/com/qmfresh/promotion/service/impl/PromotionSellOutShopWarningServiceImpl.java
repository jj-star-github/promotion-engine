package com.qmfresh.promotion.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qmfresh.promotion.entity.PromotionSellOutShopWarning;
import com.qmfresh.promotion.manager.IPromotionSellOutShopWarningManager;
import com.qmfresh.promotion.service.IPromotionSellOutShopWarningService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PromotionSellOutShopWarningServiceImpl implements IPromotionSellOutShopWarningService {
    @Autowired
    IPromotionSellOutShopWarningManager promotionSellOutShopWarningManager;

    @Override
    public PromotionSellOutShopWarning getShopWarningSettingByShopId(Integer shopId) {
        return promotionSellOutShopWarningManager.getOne(new LambdaQueryWrapper<PromotionSellOutShopWarning>().eq(PromotionSellOutShopWarning::getShopId, shopId).eq(PromotionSellOutShopWarning::getIsDeleted, 0));
    }
}
