package com.qmfresh.promotion.service.impl;

import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.TimeUtil;
import com.qmfresh.promotion.dto.CreateCpPromotionParam;
import com.qmfresh.promotion.dto.CreateVipPromotionParam;
import com.qmfresh.promotion.dto.DataAndWeek;
import com.qmfresh.promotion.entity.ActivitySsu;
import com.qmfresh.promotion.entity.AreaScopeShop;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionActivityTime;
import com.qmfresh.promotion.enums.DiscountTypeEnums;
import com.qmfresh.promotion.enums.PriceDataTypeEnums;
import com.qmfresh.promotion.enums.PriceResourseEnums;
import com.qmfresh.promotion.manager.IActivitySsuManager;
import com.qmfresh.promotion.manager.IAreaScopeShopManager;
import com.qmfresh.promotion.manager.IPromotionActivityManager;
import com.qmfresh.promotion.manager.IPromotionActivityTimeManager;
import com.qmfresh.promotion.mapper.PromotionActivityTimeMapper;
import com.qmfresh.promotion.message.sendbody.ActivityCreatBody;
import com.qmfresh.promotion.message.sendbody.ActivityModifyBody;
import com.qmfresh.promotion.message.sendbody.EffectTime;
import com.qmfresh.promotion.message.sendbody.EffectTimeInfoDto;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.service.IActivityMessageService;
import com.qmfresh.promotion.strategy.bean.CpRule;
import com.qmfresh.promotion.strategy.bean.DiscountRule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author xzw
 */
@Service
@Slf4j
public class ActivityMessageServiceImpl implements IActivityMessageService {
    @Resource
    private IAreaScopeShopManager areaScopeShopManager;
    @Resource
    private IPromotionActivityManager promotionActivityManager;
    @Resource
    private IPromotionActivityTimeManager promotionActivityTimeManager;

    @Resource
    private PromotionActivityTimeMapper promotionActivityTimeMapper;

    @Resource
    private IActivitySsuManager activitySsuManager;

    @Override
    public List<ActivityCreatBody> buildVipActivityBody(Long activityId, CreateVipPromotionParam param) {
        // 只会传一个时间段
        PromotionActivityTime promotionActivityTime = param.getActivityTimes().get(0);
        List<String> weekDayList = TimeUtil.stringToList(promotionActivityTime.getWeekDay());
        List<DataAndWeek> dataAndWeek = new ArrayList<>();
        try {
            dataAndWeek = TimeUtil.getDataAndWeek(promotionActivityTime.getEffectBeginTime(),
                    promotionActivityTime.getEffectEndTime());
        } catch (ParseException e) {
            throw new BusinessException("时间段转化为日期有误");
        }
        List<DataAndWeek> collect = dataAndWeek.stream().filter(d -> weekDayList.contains(d.getWeek()))
                .collect(Collectors.toList());
        List<PromotionActivityTime> actTimeList = new ArrayList<>();
        for (DataAndWeek dw : collect) {
            PromotionActivityTime activityTime = new PromotionActivityTime();
            activityTime.setEffectBeginTime(dw.getStartTime());
            activityTime.setEffectEndTime(dw.getEndTime());
            actTimeList.add(activityTime);
        }
        Long userId = param.getUserId() != null ? param.getUserId() : -1;

        List<Integer> skuIds = param.getPromotionProductDto().getSkuIds();
        List<Integer> shopIds = param.getAreaDtos().get(0).getShopIds();
        DiscountRule discountRule = param.getVipRule().getDiscountRule();
        List<ActivityCreatBody> params = this.getParams(activityId, actTimeList, skuIds, shopIds, discountRule, null,
                PriceResourseEnums.VIP.getCode(), Integer.parseInt(userId.toString()), param.getUserName());
        return params;
    }

    @Override
    public List<ActivityCreatBody> buildCpActivityBody(Long activityId, CreateCpPromotionParam param) {
        PromotionActivityTime promotionActivityTime = param.getActivityTimes().get(0);
        List<PromotionActivityTime> patList = new ArrayList<>();
        patList.add(promotionActivityTime);
        Long userId = param.getUserId() != null ? param.getUserId() : -1;
        List<Integer> skuIds = param.getPromotionProductDto().getSkuIds();
        List<Integer> shopIds = param.getAreaDtos().get(0).getShopIds();
        List<CpRule> skuRules = param.getPromotionProductDto().getSkuRules();
        List<ActivityCreatBody> params = this.getParams(activityId, patList, skuIds, shopIds, null, skuRules,
                PriceResourseEnums.CP.getCode(), Integer.parseInt(userId.toString()), param.getUserName());
        return params;
    }

    @Override
    public List<ActivityCreatBody> buildCpForShopActivityBody(List<Long> activityIdList) {
        List<PromotionActivity> promotionActivityList = promotionActivityManager.filterPromotionActivity(activityIdList,null,null,null,null);
        if (promotionActivityList.isEmpty()) {
            return null;
        }
        Map<Long, PromotionActivity> promotionActivityMap = promotionActivityList.stream().collect(Collectors.toMap(PromotionActivity::getId, tivity -> tivity));

        List<PromotionActivityTime> promotionActivityTimeList =promotionActivityTimeManager.promotionActivityTimes(activityIdList);
        if (promotionActivityTimeList.isEmpty()) {
            return null;
        }
        Map<Long, PromotionActivityTime> promotionActivityTimeMap = promotionActivityTimeList.stream().collect(Collectors.toMap(PromotionActivityTime::getActivityId, promotionActivityTime -> promotionActivityTime));
        List<ActivitySsu> activitySsuList = activitySsuManager.activitySsu(activityIdList);
        if (activitySsuList.isEmpty()) {
            return null;
        }
        List<AreaScopeShop> areaScopeShopList = areaScopeShopManager.areaScopeShop(activityIdList);

        if (areaScopeShopList.isEmpty()) {
            return null;
        }
        Map<Long, AreaScopeShop> areaScopeShopMap = areaScopeShopList.stream().collect(Collectors.toMap(AreaScopeShop::getActivityId, areaScopeShop -> areaScopeShop));
        List<ActivityCreatBody> activityCreatBodyList = new ArrayList<>();
        //有多少个商品就有多少个消息对象
        for (ActivitySsu ssu : activitySsuList) {
            ActivityCreatBody activityCreatBody = new ActivityCreatBody();
            activityCreatBody.setPlatformSkuId(ssu.getSkuId());
            activityCreatBody.setSkuId(ssu.getSkuId());
            activityCreatBody.setPlatformSkuType(1);
            activityCreatBody.setBillId(ssu.getActivityId().toString());
            activityCreatBody.setPriority(1);
            activityCreatBody.setShopId(areaScopeShopMap.get(ssu.getActivityId()).getShopId());
            activityCreatBody.setPriceDataType(PriceDataTypeEnums.FIXED.getCode());
            activityCreatBody.setPriceData(ssu.getPromotionPrice());
            activityCreatBody.setAffectChannel(1);
            EffectTimeInfoDto effectTimeInfoDto = new EffectTimeInfoDto();
            effectTimeInfoDto.setTimeType(1);
            EffectTime effectTime = new EffectTime();
            effectTime.setBeginTime(promotionActivityTimeMap.get(ssu.getActivityId()).getEffectBeginTime());
            effectTime.setEndTime(promotionActivityTimeMap.get(ssu.getActivityId()).getEffectEndTime());
            List<EffectTime> effectTimeList = new ArrayList<>();
            effectTimeList.add(effectTime);
            effectTimeInfoDto.setTimeDurationValue(effectTimeList);
            activityCreatBody.setEffectTimeInfo(effectTimeInfoDto);
            activityCreatBody.setChangeSource(1);
            activityCreatBody.setPriceType(PriceResourseEnums.CP.getCode());
            activityCreatBody.setStatus(1);
            activityCreatBody.setUserId(Integer.parseInt(promotionActivityMap.get(ssu.getActivityId()).getCreateUserId().toString()));
            activityCreatBody.setUserName(promotionActivityMap.get(ssu.getActivityId()).getCreateUserName());
            activityCreatBody.setReason("门店改价");
            activityCreatBody.setBussinessTime(DateUtil.getTimeStamp());
            activityCreatBodyList.add(activityCreatBody);
        }
        return activityCreatBodyList;
    }

    private List<ActivityCreatBody> getParams(Long activityId, List<PromotionActivityTime> promotionActivityTimes,
                                              List<Integer> skuIds, List<Integer> shopIds, DiscountRule discountRule, List<CpRule> skuRules,
                                              Integer priceResourse, Integer userId, String userName) {
        List<ActivityCreatBody> activityCreatList = new ArrayList<>();
        List<EffectTimeInfoDto> EffectTimeInfoDtoList = new ArrayList<>();
        Map<Integer, CpRule> cpRuleMap = null;
        if (skuRules != null) {
            cpRuleMap = skuRules.stream().collect(Collectors.toMap(CpRule::getSkuId, cpRule -> cpRule));
        }

        for (PromotionActivityTime pat : promotionActivityTimes) {
            EffectTimeInfoDto effectTimeInfoDto = new EffectTimeInfoDto();
            effectTimeInfoDto.setTimeType(1);
            EffectTime effectTime = new EffectTime();
            effectTime.setBeginTime(pat.getEffectBeginTime());
            effectTime.setEndTime(pat.getEffectEndTime());
            List<EffectTime> effectTimeList = new ArrayList<>();
            effectTimeList.add(effectTime);
            effectTimeInfoDto.setTimeDurationValue(effectTimeList);
            EffectTimeInfoDtoList.add(effectTimeInfoDto);
        }

        for (Integer shopId : shopIds) {
            for (Integer skuId : skuIds) {
                ActivityCreatBody activityCreatBody = new ActivityCreatBody();
                activityCreatBody.setPlatformSkuId(skuId);
                activityCreatBody.setSkuId(skuId);
                activityCreatBody.setPlatformSkuType(1);
                activityCreatBody.setBillId(activityId.toString());
                activityCreatBody.setPriority(1);
                activityCreatBody.setShopId(shopId);
                if (priceResourse.equals(PriceResourseEnums.VIP.getCode())) {
                    activityCreatBody.setPriceDataType(discountRule.getType() == DiscountTypeEnums.FIXED.getCode()
                            ? PriceDataTypeEnums.FIXED.getCode()
                            : PriceDataTypeEnums.DISCOUNT.getCode());
                    activityCreatBody.setPriceData(discountRule.getDiscount());
                } else if (priceResourse.equals(PriceResourseEnums.CP.getCode())) {
                    activityCreatBody.setPriceDataType(PriceDataTypeEnums.FIXED.getCode());
                    activityCreatBody.setPriceData(cpRuleMap.get(skuId).getPromotionPrice());
                }

                activityCreatBody.setAffectChannel(1);
                activityCreatBody.setChangeSource(1);
                activityCreatBody.setPriceType(priceResourse);
                activityCreatBody.setStatus(1);
                activityCreatBody.setUserId(userId);
                activityCreatBody.setUserName(userName);
                activityCreatBody.setReason(priceResourse.equals(PriceResourseEnums.CP.getCode()) ? "总部促销" : "会员活动");
                activityCreatBody.setBussinessTime(DateUtil.getTimeStamp());
                activityCreatList.add(activityCreatBody);
            }
        }
        List<ActivityCreatBody> LastactivityCreatBodyList = new ArrayList<>();

        for (EffectTimeInfoDto eti : EffectTimeInfoDtoList) {
            for (ActivityCreatBody acb : activityCreatList) {
                ActivityCreatBody activityCreatBody = new ActivityCreatBody();
                BeanUtils.copyProperties(acb, activityCreatBody);
                activityCreatBody.setEffectTimeInfo(eti);
                LastactivityCreatBodyList.add(activityCreatBody);
            }
        }
        return LastactivityCreatBodyList;
    }

    @Override
    public List<ActivityModifyBody> buildActivityModifyBody(List<Long> activityIds, Integer userId, String userName,
                                                            Integer operate) {
        List<ActivitySsu> activitySsuList = activitySsuManager.activitySsu(activityIds);
        List<AreaScopeShop> areaScopeShopList = areaScopeShopManager.areaScopeShop(activityIds);

        List<ActivityModifyBody> activityModifyBodyList = new ArrayList<>();
        for (ActivitySsu ssu : activitySsuList) {
            for (AreaScopeShop shop : areaScopeShopList) {
                if (ssu.getActivityId().equals(shop.getActivityId())) {
                    ActivityModifyBody activityModifyBody = new ActivityModifyBody();
                    activityModifyBody.setBillId(shop.getActivityId().toString());
                    activityModifyBody.setChangeSource(1);
                    activityModifyBody.setModifyType(operate);
                    activityModifyBody.setShopId(shop.getShopId());
                    activityModifyBody.setSkuId(ssu.getSkuId());

                    activityModifyBody.setUpdId(userId == null ? -1 : userId);
                    activityModifyBody.setUpdName(userName == null ? "系统" : userName);
                    activityModifyBody.setBussinessTime(DateUtil.getTimeStamp());
                    activityModifyBodyList.add(activityModifyBody);
                }
            }
        }
        return activityModifyBodyList;
    }


}
