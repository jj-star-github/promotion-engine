package com.qmfresh.promotion.service.impl;

import com.qmfresh.promotion.dto.PromotionProtocol;
import com.qmfresh.promotion.dto.PromotionSsuContext;
import com.qmfresh.promotion.service.IExecutePromotionService;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/5/24.
 *
 * @author wyh
 */
@Service
@Slf4j
public class ExecutePromotionServiceImpl implements IExecutePromotionService {

    @Override
    public List<PromotionSsuContext> execute(PromotionProtocol param) {
        return null;
    }
}
