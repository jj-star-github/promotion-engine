package com.qmfresh.promotion.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qmfresh.promotion.bean.gis.Shop;
import com.qmfresh.promotion.bean.promotion.PageQueryActivityParam;
import com.qmfresh.promotion.bean.promotion.PromotionActivityBean;
import com.qmfresh.promotion.bean.promotion.QueryActivityParam;
import com.qmfresh.promotion.bean.sku.Sku;
import com.qmfresh.promotion.cache.service.LCPromotionActivityService;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.*;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.*;
import com.qmfresh.promotion.enums.ActivityModifTypeEnums;
import com.qmfresh.promotion.enums.SsuTagTypeEnums;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.manager.IActivitySsuManager;
import com.qmfresh.promotion.manager.IPromotionActivityManager;
import com.qmfresh.promotion.manager.IPromotionActivityTimeManager;
import com.qmfresh.promotion.manager.IPromotionRuleManager;
import com.qmfresh.promotion.mapper.ActivityClass2Mapper;
import com.qmfresh.promotion.mapper.ActivitySsuMapper;
import com.qmfresh.promotion.message.TopicNameConfig;
import com.qmfresh.promotion.message.producer.ProducerDispatcher;
import com.qmfresh.promotion.message.sendbody.ActivityModifyBody;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.service.IActivityMessageService;
import com.qmfresh.promotion.service.IPromotionActivityService;
import com.qmfresh.promotion.template.SearchVipSkuTem;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/5/24.
 *
 * @author wyh
 */
@Service
@Slf4j
public class PromotionActivityServiceImpl implements IPromotionActivityService {
    private static final Logger logger = LoggerFactory.getLogger(PromotionActivityServiceImpl.class);
    @Resource
    private IPromotionActivityManager promotionActivityManager;

    @Resource
    private IPromotionActivityTimeManager promotionActivityTimeManager;

    @Resource
    private IPromotionRuleManager promotionRuleManager;

    @Resource
    private LCPromotionActivityService lcPromotionActivityService;

    @Resource
    private IActivitySsuManager activitySsuManager;
    @Resource
    private IQmExternalApiService iQmExternalApiService;
    @Resource
    private SearchVipSkuTem searchVipSkuTem;
    @Resource
    private ActivityClass2Mapper activityClass2Mapper;
    @Resource
    private ActivitySsuMapper activitySsuMapper;
    @Value("${vip.support.day.num}")
    private Integer supportNum;
    @Resource
    private IActivityMessageService activityMessageService;
    @Resource
    private ProducerDispatcher producerDispatcher;
    @Resource
    private TopicNameConfig topicConfig;

    @Override
    public PromotionActivity queryPromotionActivityById(Long activityId) {
        return lcPromotionActivityService.get(activityId);
    }

    @Override
    public ListWithPage<PromotionActivityBean> pageQuery(PageQueryActivityParam param) {
        ListWithPage<PromotionActivity> activityPage = promotionActivityManager.pageQuery(param);
        log.info("活动查询：param={} activity={}", JSON.toJSONString(param), JSON.toJSONString(activityPage));
        if (activityPage == null || ListUtil.isNullOrEmpty(activityPage.getListData())) {
            return new ListWithPage<>();
        }
        List<PromotionActivity> promotionActivities = activityPage.getListData();
        // 促销规则
        List<Long> activityIds = promotionActivities.stream().map(PromotionActivity::getId)
                .collect(Collectors.toList());
        List<PromotionRule> promotionRules = promotionRuleManager
                .list(new LambdaQueryWrapper<PromotionRule>()
                        .in(PromotionRule::getActivityId, activityIds).eq(PromotionRule::getIsDeleted, 0));
        Map<String, PromotionRule> prMap = ListUtil.listToMap("activityId", promotionRules);
        // 促销时间
        List<PromotionActivityTime> patList = promotionActivityTimeManager.list(
                new LambdaQueryWrapper<PromotionActivityTime>().in(PromotionActivityTime :: getActivityId, activityIds).eq(PromotionActivityTime::getIsDeleted, 0));
        Map<Long, List<PromotionActivityTime>> patMap = patList.stream()
                .collect(Collectors.groupingBy(PromotionActivityTime::getActivityId));
        // 促销商品
        List<ActivitySsu> promotionSsuList = activitySsuManager.list(new LambdaQueryWrapper<ActivitySsu>()
                .in(ActivitySsu::getActivityId, activityIds).eq(ActivitySsu::getIsDeleted, 0));
        Map<Long, List<ActivitySsu>> skuMap = promotionSsuList.stream()
                .collect(Collectors.groupingBy(ActivitySsu::getActivityId));
        // 促销门店
        List<Integer> shopIds = promotionActivities.stream().map(PromotionActivity::getShopId).distinct()
                .collect(Collectors.toList());
        List<Shop> shopList = iQmExternalApiService.selectShopByIds(shopIds);
        Map<Long, String> shopIdNameMap = shopList.stream().collect(Collectors.toMap(Shop::getId, Shop::getShopName));

        ListWithPage<PromotionActivityBean> padPage = new ListWithPage<>();
        List<PromotionActivityBean> padList = new ArrayList<>(promotionActivities.size());
        promotionActivities.forEach(pa -> {
            PromotionActivityBean promotionActivityBean = new PromotionActivityBean();
            promotionActivityBean.setPromotionActivityId(pa.getId());
            promotionActivityBean.setActivityName(pa.getActivityName());
            promotionActivityBean.setPromotionType(pa.getPromotionModuleId());
            promotionActivityBean.setChannel(pa.getChannel());
            promotionActivityBean.setRuleDescription(prMap.get(pa.getId().toString()).getDescript());
            promotionActivityBean.setEffectTime(this.getEffectTime(patMap.get(pa.getId())));
            promotionActivityBean.setStatus(pa.getStatus());
            promotionActivityBean.setCreatorFrom(pa.getCreatorFrom());
            promotionActivityBean.setcT(pa.getCT());
            promotionActivityBean.setuT(pa.getUT());
            promotionActivityBean.setCreateUserName(pa.getCreateUserName());
            if(CollectionUtils.isNotEmpty(skuMap.get(pa.getId()))){
                promotionActivityBean.setSkuIds(skuMap.get(pa.getId()).stream().map(ActivitySsu::getSkuId).collect(Collectors.toList()));
                promotionActivityBean.setSkuNames(skuMap.get(pa.getId()).stream().map(ActivitySsu::getSsuName).collect(Collectors.toList()));
            }
            promotionActivityBean.setShopId(pa.getShopId());
            if (shopIdNameMap != null) {
                promotionActivityBean.setShopName(shopIdNameMap.get(getLongId(pa.getShopId())));
            }
            padList.add(promotionActivityBean);
        });

        padPage.setListData(padList);
        padPage.setTotalCount(activityPage.getTotalCount());
        return padPage;
    }

    private String getEffectTime(List<PromotionActivityTime> promotionActivityTimes) {
        String effectTime = "";
        for (PromotionActivityTime act : promotionActivityTimes) {
            if (!effectTime.isEmpty()) {
                effectTime = effectTime.concat("\n");
            }
            effectTime = effectTime.concat(DateUtil.getDateStringByTimeStamp(act.getEffectBeginTime(), DateUtil.YMDHMS))
                    .concat(" - ")
                    .concat(DateUtil.getDateStringByTimeStamp(act.getEffectEndTime(), DateUtil.YMDHMS));
            if (StringUtils.isNotBlank(act.getWeekDay())) {
                List<Integer> weekDays = ListUtil.stringToIntegerList(act.getWeekDay());
                effectTime = effectTime.concat("  ");
                String effectWeekDayTime = "";
                for (Integer weekDay : weekDays) {
                    if (!StringUtils.isBlank(effectWeekDayTime)) {
                        effectWeekDayTime = effectWeekDayTime.concat("、");
                    }
                    effectWeekDayTime = effectWeekDayTime.concat(DateUtil.getWeekDayStr(weekDay));
                }
                effectTime = effectTime.concat(effectWeekDayTime);
            }
        }
        return effectTime;
    }


    @Override
    public List<PromotionActivity> queryByCondition(QueryActivityParam param) {
        return promotionActivityManager.queryByCondition(param);
    }

    @Override
    public Boolean online(PromotionActivity promotionActivity) {
        Assert.notNull(promotionActivity.getId(), "活动id不能为空");
        Assert.notNull(promotionActivity.getUpdateUserId(), "更新人id不能为空");
        Assert.notNull(promotionActivity.getUpdateUserName(), "更新人不能为空");
        Boolean online = promotionActivityManager.online(promotionActivity.getId(), promotionActivity.getUpdateUserId(),
                promotionActivity.getUpdateUserName());
        if (online) {
            List<Long> activityIdList = new ArrayList<>();
            activityIdList.add(promotionActivity.getId());
            List<ActivityModifyBody> buildActivityModifyBody = activityMessageService.buildActivityModifyBody(
                    activityIdList, Integer.parseInt(promotionActivity.getUpdateUserId().toString()),
                    promotionActivity.getUpdateUserName(), ActivityModifTypeEnums.EFFECT.getCode());
            if (!buildActivityModifyBody.isEmpty()) {
                ActivityModifyBody.send(producerDispatcher, topicConfig.getActivityModifyTopic(), buildActivityModifyBody);
            }
        }
        return online;
    }

    @Override
    public Boolean stop(PromotionActivity promotionActivity) {
        Assert.notNull(promotionActivity.getId(), "活动id不能为空");
        Assert.notNull(promotionActivity.getUpdateUserId(), "更新人id不能为空");
        Assert.notNull(promotionActivity.getUpdateUserName(), "更新人不能为空");
        Boolean stop = promotionActivityManager.stop(promotionActivity.getId(), promotionActivity.getUpdateUserId(),
                promotionActivity.getUpdateUserName());
        if (stop) {
            List<Long> activityIdList = new ArrayList<>();
            activityIdList.add(promotionActivity.getId());
            List<ActivityModifyBody> buildActivityModifyBody = activityMessageService.buildActivityModifyBody(
                    activityIdList, Integer.parseInt(promotionActivity.getUpdateUserId().toString()),
                    promotionActivity.getUpdateUserName(), ActivityModifTypeEnums.INVALID.getCode());
            if (!buildActivityModifyBody.isEmpty()) {
                ActivityModifyBody.send(producerDispatcher, topicConfig.getActivityModifyTopic(), buildActivityModifyBody);
            }
        }
        return stop;
    }

    @Override
    public Boolean delete(PromotionActivity promotionActivity) {
        Assert.notNull(promotionActivity.getId(), "活动id不能为空");
        Assert.notNull(promotionActivity.getUpdateUserId(), "更新人id不能为空");
        Assert.notNull(promotionActivity.getUpdateUserName(), "更新人不能为空");
        Boolean delete = promotionActivityManager.delete(promotionActivity.getId(), promotionActivity.getUpdateUserId(),
                promotionActivity.getUpdateUserName());
        if (delete) {
            List<Long> activityIdList = new ArrayList<>();
            activityIdList.add(promotionActivity.getId());
            List<ActivityModifyBody> buildActivityModifyBody = activityMessageService.buildActivityModifyBody(
                    activityIdList, Integer.parseInt(promotionActivity.getUpdateUserId().toString()),
                    promotionActivity.getUpdateUserName(), ActivityModifTypeEnums.INVALID.getCode());
            if (!buildActivityModifyBody.isEmpty()) {
                ActivityModifyBody.send(producerDispatcher, topicConfig.getActivityModifyTopic(), buildActivityModifyBody);
            }
        }
        return delete;
    }

    @Override
    public Boolean finish(List<Long> activityIds) {
        Assert.isTrue(!ListUtil.isNullOrEmpty(activityIds), "活动不能为空");
        return promotionActivityManager.finish(activityIds);
    }

    @Override
    public Boolean outOfDate(List<Long> activityIds) {
        Assert.isTrue(!ListUtil.isNullOrEmpty(activityIds), "活动不能为空");
        return promotionActivityManager.outOfDate(activityIds);
    }

    @Override
    public Boolean start(List<Long> activityIds) {
        Assert.isTrue(!ListUtil.isNullOrEmpty(activityIds), "活动不能为空");
        return promotionActivityManager.start(activityIds);
    }

    @Override
    public List<PromotionActivity> queryCurrentPromotionActivity(QueryActivityParam param) {
        return promotionActivityManager.queryCurrentPromotionActivity(param);
    }

    @Override
    public List<PromotionActivity> queryPromotionActivityByShop(QueryActivityParam param) {
        return promotionActivityManager.queryPromotionActivityByShop(param);
    }

    private Long getLongId(Integer id) {
        return id == null ? 0L : id.longValue();
    }

    @Override
    public List<VipSkuVo> searchVipSku(SearchVipActParam param) {
        logger.info("会员商品查询参数" + param.toString());
        // 参数校验
        if (param.getShopId() == null) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("请传入门店Id");
        }
        if (param.getSkus().isEmpty()) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("请传入商品");
        }
        // 查询sku对应的class2Id
        List<SearchVipSkuParam> skus = param.getSkus();
        List<Integer> skuIdList = skus.stream().map(SearchVipSkuParam::getSkuId).collect(Collectors.toList());
        List<Sku> bySkuIds = iQmExternalApiService.getSku(skuIdList);
        Map<Integer, Sku> map = bySkuIds.stream().collect(Collectors.toMap(Sku::getId, sku -> sku));
        for (SearchVipSkuParam ps : skus) {
            Sku sku = map.get(ps.getSkuId());
            if (sku == null) {
                throw new com.qmfresh.promotion.platform.domain.shared.BusinessException(param.getShopId() + "门店没有" + ps.getSkuId() + "商品");
            }
            if (sku.getClass2Id() == null) {
                throw new com.qmfresh.promotion.platform.domain.shared.BusinessException(param.getShopId() + "门店商品" + ps.getSkuId() + "没有二级分类");
            }
            ps.setClass2Id(sku.getClass2Id());
        }
        List<VipSkuVo> vipSkuVoList = searchVipSkuTem.execute(param);
        return vipSkuVoList;
    }

    @Override
    public List<Integer> getAfterVipdays(SearchAfterVipParam param) {
        logger.info("门店商品会员日查询参数" + param.toString());
        // 校验参数
        if (param.getShopId() == null) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("请传入门店Id");
        }
        if (param.getSkuId() == null) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("请传入商品");
        }
        if (param.getDays().isEmpty()) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("请传入需要查询的日期");
        }
        if (param.getDays().size() > supportNum) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("只支持查询后三天的会员日");
        }
        List<Integer> skuIds = new ArrayList<>();
        skuIds.add(param.getSkuId());
        List<Sku> bySkuIds = iQmExternalApiService.getSku(skuIds);
        Map<Integer, Integer> map = bySkuIds.stream().collect(Collectors.toMap(Sku::getId, Sku::getClass2Id));
        if (map.get(param.getSkuId()) == null) {
            throw new BusinessException(param.getSkuId() + "没有二级分类");
        }
        param.setClass2Id(map.get(param.getSkuId()));
        // 查看门店指定时间的活动
        List<SearchAfterDay> days = param.getDays();
        Integer skuId = param.getSkuId();
        Integer class2Id = param.getClass2Id();
        List<Integer> vipDays = new ArrayList<>();
        // for循环了存在性能问题,目前需求查看后三天的会员日
        for (SearchAfterDay day : days) {
            List<Long> actbyTime = searchVipSkuTem.getActbyTime(day.getStartTime(), param.getShopId());
            if (actbyTime.isEmpty()) {
                continue;
            }
            // 生效的活动是否有指定商品,如果有将日期放入vipDays
            boolean actSkuBySkuId = this.getActSkuBySkuId(actbyTime, skuId, class2Id);
            if (actSkuBySkuId) {
                vipDays.add(day.getStartTime());
            }
        }
        return vipDays;
    }

    private boolean getActSkuBySkuId(List<Long> actIds, Integer skuId, Integer class2Id) {
        LambdaQueryWrapper<ActivitySsu> entityWrapper = new LambdaQueryWrapper<>();
        entityWrapper.eq(ActivitySsu::getSkuId, skuId);
        entityWrapper.eq(ActivitySsu::getIsDeleted, 0);
        entityWrapper.in(ActivitySsu::getActivityId, actIds);
        entityWrapper.eq(ActivitySsu::getExTag, SsuTagTypeEnums.CONTAIN.getCode());
        List<ActivitySsu> activitySsuList = activitySsuMapper.selectList(entityWrapper);
        if (!activitySsuList.isEmpty()) {
            return true;
        } else {
            LambdaQueryWrapper<ActivitySsu> entity = new LambdaQueryWrapper<>();
            entityWrapper.eq(ActivitySsu::getSkuId, skuId);
            entityWrapper.eq(ActivitySsu::getIsDeleted, 0);
            entityWrapper.in(ActivitySsu::getActivityId, actIds);
            entityWrapper.eq(ActivitySsu::getExTag, SsuTagTypeEnums.EXCLUSIVE.getCode());
            List<ActivitySsu> notVipList = activitySsuMapper.selectList(entityWrapper);
            if (!notVipList.isEmpty()) {
                return false;
            } else {
                LambdaQueryWrapper<ActivityClass2> wrapper = new LambdaQueryWrapper<>();
                wrapper.in(ActivityClass2::getActivityId, actIds);
                wrapper.eq(ActivityClass2::getClassId, class2Id);
                wrapper.eq(ActivityClass2::getIsDeleted, 0);
                List<ActivityClass2> activityClass2List = activityClass2Mapper.selectList(wrapper);
                if (activityClass2List.isEmpty()) {
                    return false;
                }
                return true;
            }

        }
    }

    @Override
    public boolean isActGood(IsActGoodParam param) {
        boolean shopActbyShop = searchVipSkuTem.getShopActbyShop(param);
        return shopActbyShop;
    }
}
