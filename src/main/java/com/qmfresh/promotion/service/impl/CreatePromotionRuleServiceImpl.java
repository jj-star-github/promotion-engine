package com.qmfresh.promotion.service.impl;

import com.qmfresh.promotion.service.ICreatePromotionRuleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/5/23.
 *
 * @author wyh
 */
@Service
@Slf4j
public class CreatePromotionRuleServiceImpl implements ICreatePromotionRuleService {
}
