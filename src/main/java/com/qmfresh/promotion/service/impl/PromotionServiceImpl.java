package com.qmfresh.promotion.service.impl;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.price.ModifyVipPriceBean;
import com.qmfresh.promotion.bean.price.ModifyVipPriceResult;
import com.qmfresh.promotion.bean.sku.Ssu;
import com.qmfresh.promotion.bean.sku.SsuQueryForOnline;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.dto.*;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.enums.ActivityModifTypeEnums;
import com.qmfresh.promotion.enums.CreatorFromValueTranslateEnums;
import com.qmfresh.promotion.enums.PromotionCreatorTypeEnums;
import com.qmfresh.promotion.enums.StatusValueTranslateEnums;
import com.qmfresh.promotion.external.service.ICmallExternalApiService;
import com.qmfresh.promotion.external.service.IModifyPriceExternalApiService;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.manager.IPromotionActivityManager;
import com.qmfresh.promotion.manager.IPromotionManager;
import com.qmfresh.promotion.message.producer.ProducerDispatcher;
import com.qmfresh.promotion.message.sendbody.ActivityModifyBody;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.service.IActivityMessageService;
import com.qmfresh.promotion.service.IPromotionActivityService;
import com.qmfresh.promotion.service.IPromotionService;
import com.qmfresh.promotion.strategy.facade.CreatePromotionExecuteFacade;
import com.qmfresh.promotion.strategy.rule.create.CreatePromotionStrategyExecutor;
import com.qmfresh.promotion.thread.QueryCpPriceThread;
import com.qmfresh.promotion.thread.factory.NamedThreadFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/6/4.
 *
 * @author wyh
 */
@Service
@Slf4j
public class PromotionServiceImpl implements IPromotionService {

	@Resource
	private IPromotionManager promotionManager;

	@Resource
	private IQmExternalApiService qmExternalApiService;

	@Resource
	private IPromotionActivityManager promotionActivityManager;

	@Resource
	private ICmallExternalApiService cmallExternalApiService;

	@Resource
	private ProducerDispatcher producerDispatcher;

	@Resource
	private IModifyPriceExternalApiService modifyPriceExternalApiService;

	@Resource
	private CreatePromotionExecuteFacade createPromotionExecuteFacade;
	@Resource
	private IPromotionActivityService promotionActivityService;
	@Resource
	private QueryCpPriceThread queryCpPriceThread;
	@Autowired
	private IActivityMessageService activityMessageService;

	private static ThreadPoolExecutor pool = new ThreadPoolExecutor(5, 5, 0, TimeUnit.SECONDS,
			new LinkedBlockingDeque<>(100), new NamedThreadFactory("query_change_price"),
			new ThreadPoolExecutor.CallerRunsPolicy());

	@Override
	public PromotionDto getPromotionDetail(Long activityId) {
		Assert.notNull(activityId, "促销活动id不能为空");
		return promotionManager.getPromotionDetail(activityId);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean createBzPromotion(CreateBzPromotionParam param) {
		param.setStrategyName(CreatePromotionStrategyExecutor.BZ.getValue());
		return createPromotionExecuteFacade.executeCreateStrategy(param);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean createDpPromotion(CreateDpPromotionParam param) {
		param.setStrategyName(CreatePromotionStrategyExecutor.DP.getValue());
		return createPromotionExecuteFacade.executeCreateStrategy(param);
		// todo 目前单品促销只作用于单个ssu上，后续可扩展
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean createMzPromotion(CreateMzPromotionParam param) {
		param.setStrategyName(CreatePromotionStrategyExecutor.MZ.getValue());
		return createPromotionExecuteFacade.executeCreateStrategy(param);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean createMjPromotion(CreateMjPromotionParam param) {
		param.setStrategyName(CreatePromotionStrategyExecutor.MJ.getValue());
		return createPromotionExecuteFacade.executeCreateStrategy(param);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean createVipPromotion(CreateVipPromotionParam param) {
		param.setStrategyName(CreatePromotionStrategyExecutor.VIP.getValue());
		return createPromotionExecuteFacade.executeCreateStrategy(param);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean createCpPromotion(CreateCpPromotionParam param) {
		param.setStrategyName(CreatePromotionStrategyExecutor.CP.getValue());
		return createPromotionExecuteFacade.executeCreateStrategy(param);
	}

	@Override
	public ModifyVipPriceResult modifyVipPrice(ModifyVipPriceBean param) {
		return promotionManager.modifyVipPrice(param, qmExternalApiService.getProductSsuById(param.getSsuId()));
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean modifyBzPromotion(CreateBzPromotionParam param) {
		promotionManager.deleteActivity(param.getActivityId());
		this.createBzPromotion(param);
		return true;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean modifyDpPromotion(CreateDpPromotionParam param) {
		promotionManager.deleteActivity(param.getActivityId());
		this.createDpPromotion(param);
		return true;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean modifyMzPromotion(CreateMzPromotionParam param) {
		promotionManager.deleteActivity(param.getActivityId());
		this.createMzPromotion(param);
		return true;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean modifyVipPromotion(CreateVipPromotionParam param) {
		promotionManager.deleteActivity(param.getActivityId());
		this.createVipPromotion(param);
		return true;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean modifyCpPromotion(CreateCpPromotionParam param) {
		Boolean deleteActivityAllInfo = promotionManager.deleteActivityAllInfo(param.getActivityId());
		//if (deleteActivityAllInfo) {
		List<Long> activityIdList = new ArrayList<>();
		activityIdList.add(param.getActivityId());
		List<ActivityModifyBody> buildActivityModifyBody = activityMessageService.buildActivityModifyBody(
				activityIdList, Integer.parseInt(param.getUserId().toString()), param.getUserName(),
				ActivityModifTypeEnums.INVALID.getCode());
		//修改总部活动，先删除原来的活动，然后新建活动，将删除的活动信息带到参数中，合并两个消息
		param.setActivityModifyList(buildActivityModifyBody);
		Boolean result = this.createCpPromotion(param);
		if (result && param.getCreatorFrom() != null
				&& param.getCreatorFrom() == PromotionCreatorTypeEnums.SHOP.getCode()) {
			producerDispatcher.deleteShopChangePrice(param);
		}

		return result;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean deleteCpPromotion(CreateCpPromotionParam param) {
		Boolean result = promotionManager.deleteActivityAllInfo(param.getActivityId());
		return result;
	}

	@Override
	public List<Ssu> getSsuListByParam(SsuQueryForOnline query) {
		return cmallExternalApiService.getSsuListByParam(query);
	}

	@Override
	public List<QueryPriceChangeResult> queryChangePrice(List<PriceChangeParam> list) {
		log.info("改价信息查询方法：queryChangePrice，入参为：" + JSON.toJSONString(list));
		List<QueryPriceChangeResult> finalResultList = new ArrayList<>();
		// 1.根据shopId和当前时间查询获取activityId
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		Map<Integer, List<PriceChangeParam>> paramMap = list.stream()
				.collect(Collectors.groupingBy(PriceChangeParam::getShopId));
		List<Integer> shopIdList = list.stream().map(PriceChangeParam::getShopId).distinct()
				.collect(Collectors.toList());

		for (Integer shopId : shopIdList) {
			List<PriceChangeParam> shopPriceChangeList = paramMap.get(shopId);
			queryCpPriceThread.setShopId(shopId);
			queryCpPriceThread.setShopPriceChangeList(shopPriceChangeList);
			try {
				List<QueryPriceChangeResult> changeResults = (List<QueryPriceChangeResult>) queryCpPriceThread.call();
				log.info("查询改价,{},{}", shopId, JSON.toJSONString(changeResults));
				if (!CollectionUtils.isEmpty(changeResults)) {
					finalResultList.addAll(changeResults);
				}
			} catch (Exception e) {
				log.error("查询改价失败,{}", shopId);
			}
		}
		return finalResultList;
	}

	@Override
	public Boolean modifyCpPromotionStatus(ModifyPriceParam<ActivateCpPromotionParam> param) {
		List<ActivateCpPromotionParam> reqList2 = param.getReqList();
		log.info("审核后发过来的活动" + reqList2.toString());
		if (param == null || !this.batchUpdateStatus(param.getReqList())) {
			throw new BusinessException("批量更新审核状态失败");
		}
		// 通知中台改价
		// 开始时间是否是当天，是的话立即把促销价发给中台
		ModifyPriceParam<SendPromotionPriceParam> promotionParam = new ModifyPriceParam<>();
		// 组织参数
		List<SendPromotionPriceParam> reqList = new ArrayList<>();
		for (ActivateCpPromotionParam acpp : param.getReqList()) {
			// 根据状态判断是否向中台发送改价信息
			// 如果活动审批通过
			if (StatusValueTranslateEnums.APPROVED_TO_MIDPLATFORM.getCodeTwo().equals(acpp.getStatus())) {
				// 当前时间活动已开始
				Integer now = DateUtil.getCurrentTimeIntValue();
				if (acpp.getStartTime() != null && acpp.getEndTime() != null && now >= acpp.getStartTime()
						&& now <= acpp.getEndTime()) {
					// 活动上线
					promotionActivityManager.online(acpp.getActivityId(), acpp.getUserId(), acpp.getUserName());
					SendPromotionPriceParam sendPromotionPriceParam = new SendPromotionPriceParam();
					sendPromotionPriceParam.setPrice(acpp.getPromotionPrice());
					sendPromotionPriceParam
							.setReasonType(CreatorFromValueTranslateEnums.SHOP_TO_MID_PLATFORM.getCodeTwo());
					sendPromotionPriceParam.setShopId(acpp.getShopId());
					sendPromotionPriceParam.setSkuId(acpp.getSkuId());
					sendPromotionPriceParam.setUserId(acpp.getUserId().intValue());
					sendPromotionPriceParam.setUserName(acpp.getUserName());
					reqList.add(sendPromotionPriceParam);
					promotionParam.setReqList(reqList);
				}
			}
		}
		log.info("into method PromotionServiceImpl-modifyCpPromotionStatus reqList:" + JSON.toJSONString(reqList));
		if (!CollectionUtils.isEmpty(promotionParam.getReqList())) {
			//先注释
			modifyPriceExternalApiService.modifyPromotionPrice(promotionParam);
		}
		return true;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean batchUpdateStatus(List<ActivateCpPromotionParam> list) {
		if (CollectionUtils.isEmpty(list)) {
			return false;
		}
		List<PromotionActivity> promotionActivityList = new ArrayList<>();
		for (ActivateCpPromotionParam acpp : list) {
			PromotionActivity promotionActivity = new PromotionActivity();
			promotionActivity.setId(acpp.getActivityId());
			promotionActivity.setStatus(StatusValueTranslateEnums.getCode1ByCode2(acpp.getStatus()));
			promotionActivity.setUpdateUserId(acpp.getUserId());
			promotionActivity.setUpdateUserName(acpp.getUserName());
			promotionActivityList.add(promotionActivity);
		}
		return promotionActivityManager.updateBatchById(promotionActivityList);
	}

	@Override
	public List<CreateCpFromShopParam> checkSkuInVipPromotion(List<CreateCpFromShopParam> list) {
		return promotionManager.checkSkuInVipPromotion(list);
	}

	@Override
	public Boolean checkVipPromotionExist(Integer startTime, Integer endTime, List<CreateCpFromShopParam> list) {
		Boolean flag = false;
		List<String> dateList = DateUtil.getBetweenDates(startTime, endTime);
		if (dateList.size() == 1) {
			return true;
		}
		List<SearchAfterDay> afterDays = new ArrayList<>(dateList.size() - 1);
		for (int j = 1; j < dateList.size(); j++) {
			SearchAfterDay searchAfterDay = new SearchAfterDay();
			searchAfterDay.setStartTime(DateUtil.getStartTimeStamp(DateUtil.parseDate(dateList.get(j))));
			searchAfterDay.setEndTime(DateUtil.getEndTimeStamp(DateUtil.parseDate(dateList.get(j))));
			afterDays.add(searchAfterDay);
		}
		List<Integer> rsVipDays = new ArrayList<>();
		for (CreateCpFromShopParam createCpFromShopParam : list) {
			SearchAfterVipParam searchAfterVipParam = new SearchAfterVipParam();
			searchAfterVipParam.setClass2Id(createCpFromShopParam.getClass2Id());
			searchAfterVipParam.setDays(afterDays);
			searchAfterVipParam.setShopId(createCpFromShopParam.getShopId());
			searchAfterVipParam.setSkuId(createCpFromShopParam.getSkuId());
			List<Integer> vipDays = promotionActivityService.getAfterVipdays(searchAfterVipParam);
			if (!CollectionUtils.isEmpty(vipDays)) {
				rsVipDays.addAll(vipDays);
				log.info("门店改价活动与会员活动冲突{}", JSON.toJSON(searchAfterVipParam));
				break;
			}
		}
		flag = CollectionUtils.isEmpty(rsVipDays) ? true : false;

		return flag;
	}

}
