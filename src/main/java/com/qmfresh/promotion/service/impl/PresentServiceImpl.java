package com.qmfresh.promotion.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qmfresh.promotion.bean.present.*;
import com.qmfresh.promotion.bean.sku.ProductSsuFormatDto;
import com.qmfresh.promotion.bean.sku.QueryProductSsuFormat;
import com.qmfresh.promotion.common.exception.BusinessException;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.PresentActivity;
import com.qmfresh.promotion.entity.PresentModifyLog;
import com.qmfresh.promotion.entity.PresentSsu;
import com.qmfresh.promotion.enums.PresentStatusEnums;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.manager.IPresentActivityManager;
import com.qmfresh.promotion.manager.IPresentModifyLogManager;
import com.qmfresh.promotion.manager.IPresentSsuManager;
import com.qmfresh.promotion.service.IPresentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by wyh on 2019/6/11.
 *
 * @author wyh
 */
@Service
@Slf4j
public class PresentServiceImpl implements IPresentService {

    private final static Integer ONLINE_TYPE = 1;

    @Resource
    private IPresentSsuManager presentSsuManager;

    @Resource
    private IPresentActivityManager presentActivityManager;

    @Resource
    private IQmExternalApiService qmExternalApiService;

    @Resource
    private IPresentModifyLogManager presentModifyLogManager;

    @Override
    public List<PresentActivityDto> queryList(PageQueryParam param) {
        return presentActivityManager.queryList(param);
    }

    @Override
    public ListWithPage<PresentActivityDto> pageQuery(PageQueryParam param) {
        return presentActivityManager.pageQuery(param);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean createPresentActivity(CreatePresentActivity param) {
        // 有数据先删除
        if (param.getId() != null) {
            presentActivityManager.update(param.getId());
        }
        int now = DateUtil.getCurrentTimeIntValue();
        PresentActivity presentActivity = new PresentActivity();
        presentActivity.setActivityType(param.getActivityType());
        presentActivity.setName(param.getName());
        presentActivity.setEffectBeginTime(param.getEffectBeginTime());
        presentActivity.setEffectEndTime(param.getEffectEndTime());
        presentActivity.setSendNum(BigDecimal.ZERO);
        presentActivity.setStatus(1);
        presentActivity.setCreateUserId(param.getCreateUserId());
        presentActivity.setCreateUserName(param.getCreateUserName());
        presentActivity.setEffectBeginTime(param.getEffectBeginTime());
        presentActivity.setEffectEndTime(param.getEffectEndTime());
        presentActivity.setCT(now);
        presentActivity.setUT(now);
        presentActivity.setIsDeleted(0);
        presentActivityManager.save(presentActivity);

        PresentSsu presentSsu = new PresentSsu();
//        presentSsu.setSkuId();
        QueryProductSsuFormat query = new QueryProductSsuFormat();
        query.setSsuFormatIds(Collections.singletonList(param.getSsuFormatId()));
        presentSsu.setPresentId(presentActivity.getId());
        presentSsu.setSsuFormatId(param.getSsuFormatId());
        if (param.getChannelType().equals(ONLINE_TYPE)) {
            presentSsu.setSkuId(param.getSkuId());
            presentSsu.setSsuName(param.getSsuName());
        } else {
            ProductSsuFormatDto productSsuFormatDto = qmExternalApiService.getSsuFormatByCondition(query).get(0);
            presentSsu.setSkuId(productSsuFormatDto.getSkuId());
            presentSsu.setSsuName(productSsuFormatDto.getSsuName());
        }
        presentSsu.setGiveNum(BigDecimal.ONE);
        presentSsu.setLimitNum(param.getLimitNum());
        presentSsu.setPicture("");
        presentSsu.setStatus(1);
        presentSsu.setCT(now);
        presentSsu.setUT(now);
        presentSsu.setIsDeleted(0);
        presentSsuManager.save(presentSsu);
        return true;
    }

    @Override
    public Boolean delPresentActivity(DelPresentActivity param) {
        if (param.getId() != null) {
            presentActivityManager.update(param.getId());
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean bindPresent(Long activityId, List<Long> presentIds) {
        Assert.notNull(activityId, "促销活动不能为空");
        Assert.isTrue(!ListUtil.isNullOrEmpty(presentIds), "赠品活动不能为空");

        List<PresentActivity> existPas = presentActivityManager.list(new LambdaQueryWrapper<PresentActivity>()
                .in(PresentActivity::getId, presentIds)
                .isNotNull(PresentActivity::getActivityId)
                .eq(PresentActivity::getStatus, PresentStatusEnums.VALID.getCode())
                .eq(PresentActivity::getIsDeleted, 0));
        if (!ListUtil.isNullOrEmpty(existPas)) {
            throw new BusinessException("绑定赠品失败，赠品已绑定其他促销");
        }

        List<PresentActivity> updatePresentActivity = new ArrayList<>(presentIds.size());
        presentIds.forEach(
                presentId -> {
                    PresentActivity presentActivity = new PresentActivity();
                    presentActivity.setId(presentId);
                    presentActivity.setActivityId(activityId);
                    presentActivity.setStatus(PresentStatusEnums.VALID.getCode());
                    presentActivity.setUT(DateUtil.getCurrentTimeIntValue());
                    updatePresentActivity.add(presentActivity);
                }
        );
        presentActivityManager.updateBatchById(updatePresentActivity);

        List<PresentSsu> existPresentSsus = presentActivityManager.presentSsu(presentIds);

        int now = DateUtil.getCurrentTimeIntValue();
        List<PresentSsu> updatePresentSsu = new ArrayList<>(existPresentSsus.size());
        existPresentSsus.forEach(
                existPresentSsu -> {
                    PresentSsu presentSsu = new PresentSsu();
                    presentSsu.setId(existPresentSsu.getId());
                    presentSsu.setActivityId(activityId);
                    presentSsu.setUT(now);
                    updatePresentSsu.add(presentSsu);
                }
        );

        presentSsuManager.updateBatchById(updatePresentSsu);
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean modifyPresent(UpdatePresentActivity param) {
        PresentActivity presentActivity = presentActivityManager.getById(param.getPresentId());
        if (presentActivity.getStatus().equals(PresentStatusEnums.VALID.getCode())) {
            throw new BusinessException("修改赠品失败，当前赠品已绑定促销活动，活动id:" + presentActivity.getActivityId());
        }

        this.deletePresent(param.getPresentId());

        // 插入修改赠品活动记录
        int now = DateUtil.getCurrentTimeIntValue();
        PresentModifyLog presentModifyLog = new PresentModifyLog();
        presentModifyLog.setPresentId(param.getPresentId());
        presentModifyLog.setStatus(-1);
        presentModifyLog.setUserId(param.getUpdateUserId());
        presentModifyLog.setUserName(param.getUpdateUserName());
        presentModifyLog.setCT(now);
        presentModifyLog.setUT(now);
        presentModifyLog.setIsDeleted(0);
        presentModifyLogManager.save(presentModifyLog);
        if (param.getStatus().equals(-1)) {
            return true;
        }
        CreatePresentActivity createPresentActivity = new CreatePresentActivity();
        BeanUtils.copyProperties(param, createPresentActivity);
        createPresentActivity.setCreateUserId(param.getUpdateUserId());
        createPresentActivity.setCreateUserName(param.getUpdateUserName());
        this.createPresentActivity(createPresentActivity);
        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    public Boolean deletePresent(Long presentId) {
        PresentActivity presentActivity = presentActivityManager.getById(presentId);
        if (presentActivity.getStatus().equals(PresentStatusEnums.VALID.getCode())) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("删除赠品失败，当前赠品已绑定促销活动，活动id:" + presentActivity.getActivityId());
        }

        int now = DateUtil.getCurrentTimeIntValue();
        PresentActivity updatePresentActivity = new PresentActivity();
        updatePresentActivity.setId(presentId);
        updatePresentActivity.setUT(now);
        updatePresentActivity.setIsDeleted(1);
        presentActivityManager.updateById(updatePresentActivity);

        PresentSsu updatePresentSsu = new PresentSsu();
        updatePresentSsu.setUT(now);
        updatePresentSsu.setIsDeleted(1);
        presentSsuManager.update(updatePresentSsu, new LambdaQueryWrapper<PresentSsu>()
                .eq(PresentSsu::getId, presentActivity.getId())
                .eq(PresentSsu::getIsDeleted, 0));

        return true;
    }
}
