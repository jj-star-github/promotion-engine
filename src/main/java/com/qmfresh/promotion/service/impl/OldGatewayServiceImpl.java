package com.qmfresh.promotion.service.impl;

import com.qmfresh.promotion.bean.gis.Shop;
import com.qmfresh.promotion.bean.price.CMallPriceInfo;
import com.qmfresh.promotion.bean.price.CMallPriceQuery;
import com.qmfresh.promotion.bean.sku.ProductSsuFormatDto;
import com.qmfresh.promotion.bean.sku.QueryProductSsuFormat;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.CreateCpFromShopParam;
import com.qmfresh.promotion.dto.CreateCpPromotionParam;
import com.qmfresh.promotion.dto.PromotionAreaDto;
import com.qmfresh.promotion.dto.PromotionProductDto;
import com.qmfresh.promotion.entity.PromotionActivityTime;
import com.qmfresh.promotion.enums.ChannelTypeEnums;
import com.qmfresh.promotion.external.service.ICmallExternalApiService;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.service.IOldGatewayService;
import com.qmfresh.promotion.service.IPromotionService;
import com.qmfresh.promotion.strategy.bean.CpRule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
@Slf4j
public class OldGatewayServiceImpl implements IOldGatewayService {

    @Resource
    private ICmallExternalApiService cmallExternalApiService;
    @Resource
    private IQmExternalApiService qmExternalApiService;
    @Resource
    private IPromotionService promotionService;

    @Override
    public List<PromotionAreaDto> getAreaDto(List<Integer> shopIds) {
        List<Shop> shops = qmExternalApiService.selectShopByIds(shopIds);
        Map<Integer, List<Shop>> cityMap = shops.stream().collect(Collectors.groupingBy(Shop::getCityId));
        List<PromotionAreaDto> areaDtos = new ArrayList<>(cityMap.size());
        cityMap.entrySet().forEach(
                item -> {
                    PromotionAreaDto promotionAreaDto = new PromotionAreaDto();
                    promotionAreaDto.setCityId(item.getKey());
                    promotionAreaDto.setShopIds(item.getValue().stream().map(s -> s.getId().intValue()).collect(Collectors.toList()));
                    areaDtos.add(promotionAreaDto);
                }
        );
        return areaDtos;
    }

    @Override
    public PromotionProductDto convertSkuToSsuFormat(List<Integer> shopIds, Integer channel, PromotionProductDto productDto) {
        // 2.按分类,4.按商品
        if (productDto.getType().equals(2)) {
            if (!ListUtil.isNullOrEmpty(productDto.getPromotionClassDto().getExSkuIds())) {
                productDto.getPromotionClassDto().setExSsuFormatIds(this.getSsuFormatBySkuIds(shopIds, channel, productDto.getPromotionClassDto().getExSkuIds()));
            }
        } else if (productDto.getType().equals(4)) {
            productDto.setSsuFormatIds(this.getSsuFormatBySkuIds(shopIds, channel, productDto.getSkuIds()));

        }
        return productDto;
    }

    /**
     * 根据skuIds获取不同渠道ssuFormat
     *
     * @param channel 渠道
     * @param skuIds 商品
     * @return
     */
    @Override
    public List<Integer> getSsuFormatBySkuIds(List<Integer> shopIds, Integer channel, List<Integer> skuIds) {
        List<Integer> ssuFormatIds = new ArrayList<>(16);
        if (channel.equals(ChannelTypeEnums.C_OFFLINE.getCode())) {
            QueryProductSsuFormat query = new QueryProductSsuFormat();
            query.setSkuIds(skuIds);
            query.setPlatform(2);
            query.setSellChannel(2);
            query.setSsuFp(1);
            List<ProductSsuFormatDto> productSsuFormatDtos = qmExternalApiService.getSsuFormatByCondition(query);
            if (ListUtil.isNullOrEmpty(productSsuFormatDtos)) {
                throw new BusinessException("未查询到sku对应ssu_format信息");
            }
            ssuFormatIds = productSsuFormatDtos.stream().map(ProductSsuFormatDto::getSsuFormatId).collect(Collectors.toList());
        }

        if (channel.equals(ChannelTypeEnums.C_ONLINE.getCode())) {
            CMallPriceQuery cMallPriceQuery = new CMallPriceQuery();
            cMallPriceQuery.setSkuIds(skuIds);
            cMallPriceQuery.setShopIds(shopIds);
            cMallPriceQuery.setStatus(1);
            List<CMallPriceInfo> cMallPriceInfos = cmallExternalApiService.queryCmallPriceList(cMallPriceQuery);
            if (ListUtil.isNullOrEmpty(cMallPriceInfos)) {
                throw new BusinessException("未查询到sku对应ssu_format信息");
            }
            ssuFormatIds = cMallPriceInfos.stream().map(CMallPriceInfo::getSsuFormatId).collect(Collectors.toList());
        }
        return ssuFormatIds;
    }

    @Override
    public CreateCpPromotionParam buildParam(List<CreateCpFromShopParam> list){
        CreateCpPromotionParam param = new CreateCpPromotionParam();
        param.setOriginparam(list);
        param.setChannel(3);
        param.setCreatorFrom(2);
        param.setUserId(list.get(0).getUserId().longValue());
        param.setUserName(list.get(0).getUserName());
        param.setActivityName(list.get(0).getActivityName());
        //设置改价原因
        param.setApplyReasonCode(list.get(0).getApplyReasonCode());
        param.setApplyReasonName(list.get(0).getApplyReasonName());
        List<Integer> skuIds = list.stream().map(CreateCpFromShopParam::getSkuId).collect(Collectors.toList());
        List<Integer> shopIds = list.stream().map(CreateCpFromShopParam::getShopId).collect(Collectors.toList());
        List<CpRule> skuRules = list.stream().map(o -> {
            CpRule cpRule = new CpRule();
            cpRule.setPromotionPrice(o.getPrice());
            cpRule.setSkuId(o.getSkuId());
            return cpRule;
        }).collect(Collectors.toList());

        List<Integer> crowdTypes = new ArrayList<>();
        crowdTypes.add(1);
        param.setCrowdTypes(crowdTypes);
        //设置促销区域
        List<PromotionAreaDto> areaDtos = new ArrayList<>();
        PromotionAreaDto areaDto = new PromotionAreaDto();
        areaDto.setShopIds(shopIds);
        areaDtos.add(areaDto);
        param.setAreaDtos(areaDtos);
        //设置促销商品信息
        PromotionProductDto promotionProductDto = new PromotionProductDto();
        promotionProductDto.setSkuIds(skuIds);
        promotionProductDto.setSkuRules(skuRules);
        promotionProductDto.setType(4);
        param.setPromotionProductDto(promotionProductDto);
        //设置活动时间
        Integer startTime = list.get(0).getStartTime();
        Integer endTime = list.get(0).getEndTime();
        List<String> dateList = DateUtil.getBetweenDates(startTime,endTime);
        Boolean flag = promotionService.checkVipPromotionExist(startTime,endTime,list);
        if(!flag){
            throw new BusinessException("门店改价时间范围内有会员活动");
        }
        PromotionActivityTime promotionActivityTime = new PromotionActivityTime();
        promotionActivityTime.setEffectBeginTime(startTime);
        promotionActivityTime.setEffectEndTime(endTime);
        List<PromotionActivityTime> timeList = new ArrayList<>();
        timeList.add(promotionActivityTime);
        param.setActivityTimes(timeList);

        return param;
    }

    @Override
    public void buildFullInfo(CreateCpPromotionParam param) {
        param.setChannel(ChannelTypeEnums.C_OFFLINE.getCode());
        param.setAreaDtos(this.getAreaDto(param.getAreaDtos().get(0).getShopIds()));
        param.setPromotionProductDto(this.convertSkuToSsuFormat(param.getAreaDtos().get(0).getShopIds(),param.getChannel(),param.getPromotionProductDto()));
        if(param.getUserId() == null || param.getUserId() == 0){
            param.setUserId(-1L);
        }
        if(StringUtils.isEmpty(param.getUserName())){
            param.setUserName("系统");
        }
    }

}
