package com.qmfresh.promotion.service.impl;

import com.qmfresh.promotion.bean.order.QueryOrderParam;
import com.qmfresh.promotion.bean.order.UserOrderItemSummary;
import com.qmfresh.promotion.manager.ISYOrderManager;
import com.qmfresh.promotion.service.ISYOrderService;
import java.util.List;
import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/6/3.
 *
 * @author wyh
 */
@Service
@Slf4j
public class SYOrderServiceImpl implements ISYOrderService {

    @Resource
    private ISYOrderManager syOrderManager;

    @Override
    public List<UserOrderItemSummary> queryOrder(QueryOrderParam param) {
        return syOrderManager.queryOrder(param);
    }
}
