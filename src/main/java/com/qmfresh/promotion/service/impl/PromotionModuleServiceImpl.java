package com.qmfresh.promotion.service.impl;

import com.qmfresh.promotion.bean.promotion.QueryPromotionModuleParam;
import com.qmfresh.promotion.entity.PromotionModule;
import com.qmfresh.promotion.manager.IPromotionModuleManager;
import com.qmfresh.promotion.service.IPromotionModuleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wyh on 2019/5/24.
 *
 * @author wyh
 */
@Service
@Slf4j
public class PromotionModuleServiceImpl implements IPromotionModuleService {

    @Resource
    private IPromotionModuleManager promotionModuleManager;

    @Override
    public List<PromotionModule> queryByCondition(QueryPromotionModuleParam param) {
        return promotionModuleManager.queryByCondition(param);
    }
}
