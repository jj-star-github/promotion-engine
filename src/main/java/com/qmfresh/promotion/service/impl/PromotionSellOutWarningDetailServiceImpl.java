package com.qmfresh.promotion.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qmfresh.promotion.bean.promotion.PromotionSellOutWarningDetailBean;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.dto.QueryPromotionSellOutWarningDetailParam;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.PromotionSellOutWarningDetail;
import com.qmfresh.promotion.enums.IsDeleteEnums;
import com.qmfresh.promotion.enums.SellOutWarningEnums;
import com.qmfresh.promotion.manager.IPromotionSellOutWarningDetailManager;
import com.qmfresh.promotion.service.IPromotionSellOutWarningDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @program: promotion-engine
 * @description: 出清预警详情
 * @author: xbb
 * @create: 2019-11-23 14:23
 **/
@Service
@Slf4j
public class PromotionSellOutWarningDetailServiceImpl implements IPromotionSellOutWarningDetailService {

    @Autowired
    IPromotionSellOutWarningDetailManager promotionSellOutWarningDetailManager;

    @Override
    public ListWithPage<PromotionSellOutWarningDetailBean> getPageList(QueryPromotionSellOutWarningDetailParam param) {

        Page<PromotionSellOutWarningDetail> page = new Page<>(param.getPageIndex(), param.getPageSize());
        LambdaQueryWrapper<PromotionSellOutWarningDetail> entityWrapper = new LambdaQueryWrapper<>();
        if (param.getCityId() != null) {
            entityWrapper.eq(PromotionSellOutWarningDetail::getCityId, param.getCityId());
        }
        if (param.getShopId() != null) {
            entityWrapper.eq(PromotionSellOutWarningDetail::getShopId, param.getShopId());
        }
        if (param.getWarningLevel() != null) {
            entityWrapper.eq(PromotionSellOutWarningDetail::getWarningLevel, param.getWarningLevel());
        }
        if (param.getSkuId() != null) {
            entityWrapper.eq(PromotionSellOutWarningDetail::getSkuId, param.getSkuId());
        }
        if (param.getRemovalRateThreshold() != null) {
            entityWrapper.eq(PromotionSellOutWarningDetail::getRemovalRateThreshold, param.getRemovalRateThreshold());
        }
        if (param.getStartTime() != null) {
            entityWrapper.ge(PromotionSellOutWarningDetail::getCT, param.getStartTime());
        }
        if (param.getStartTime() != null) {
            entityWrapper.lt(PromotionSellOutWarningDetail::getCT, param.getEndTime());
        }
        if (param.getShopTypes() != null) {
            entityWrapper.in(PromotionSellOutWarningDetail::getShopType, param.getShopTypes());
        }
        entityWrapper.eq(PromotionSellOutWarningDetail::getWarningStatus, SellOutWarningEnums.ALREADY_SEND.getCode());
        entityWrapper.orderBy(true, false, PromotionSellOutWarningDetail::getId);
        Page<PromotionSellOutWarningDetail> pageResult = promotionSellOutWarningDetailManager.page(page, entityWrapper);
        if (pageResult == null) {
            pageResult = new Page<>();
        }
        if (pageResult.getRecords() == null) {
            pageResult.setRecords(new ArrayList<>());
        }
        Page<PromotionSellOutWarningDetailBean> pageResultBean = new Page<>();
        BeanUtils.copyProperties(pageResult, pageResultBean);
        ListWithPage<PromotionSellOutWarningDetailBean> result = new ListWithPage<>();
        result.setListData(pageResultBean.getRecords());
        result.setTotalCount((int) pageResultBean.getTotal());
        return result;
    }

    @Override
    public Boolean insert(List<PromotionSellOutWarningDetail> details) {
        return promotionSellOutWarningDetailManager.saveBatch(details);
    }

    @Override
    public Boolean update(List<PromotionSellOutWarningDetail> details) {
        return promotionSellOutWarningDetailManager.updateBatchById(details);
    }

    @Override
    public List<PromotionSellOutWarningDetail> queryWarningDetailList() {
        // 昨天
        Date beginDate = DateUtil.getDateBefore(1);
        return promotionSellOutWarningDetailManager.list(new LambdaQueryWrapper<PromotionSellOutWarningDetail>()
                .eq(PromotionSellOutWarningDetail::getWarningStatus, SellOutWarningEnums.ALREADY_SEND.getCode())
                .ge(PromotionSellOutWarningDetail::getCT, DateUtil.getStartTimeStamp(beginDate))
                .le(PromotionSellOutWarningDetail::getCT, DateUtil.getEndTimeStamp(beginDate))
                .eq(PromotionSellOutWarningDetail::getIsDeleted, IsDeleteEnums.NO.getCode())
        );
    }

}
