package com.qmfresh.promotion.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.dto.CreatePromotionSkuParam;
import com.qmfresh.promotion.dto.PromotionSkuDto;
import com.qmfresh.promotion.dto.QueryPromotionSkuParam;
import com.qmfresh.promotion.dto.UpdatePromotionSkuParam;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.PromotionSku;
import com.qmfresh.promotion.manager.IPromotionSkuManager;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.service.IPromotionSkuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: promotion-engine
 * @description:
 * @author: xbb
 * @create: 2019-11-29 16:40
 **/
@Service
@Slf4j
public class PromotionSkuServiceImpl implements IPromotionSkuService {

    @Autowired
    private IPromotionSkuManager promotionSkuManager;

    @Override
    public ListWithPage<PromotionSkuDto> getPageList(QueryPromotionSkuParam param) {

        Page<PromotionSku> page = new Page<>(param.getPageIndex(), param.getPageSize());

        LambdaQueryWrapper<PromotionSku> entityWrapper = new LambdaQueryWrapper<>();
        if (param.getSkuId() != null) {
            entityWrapper.eq(PromotionSku::getSkuId, param.getSkuId());
        }
        if (param.getCityId() != null) {
            entityWrapper.eq(PromotionSku::getCityId, param.getCityId());
        }
        if (param.getShopId() != null) {
            entityWrapper.eq(PromotionSku::getShopId, param.getShopId());
        }
        if (param.getActivityId() != null) {
            entityWrapper.eq(PromotionSku::getActivityId, param.getActivityId());
        }
        if (param.getClass2Id() != null) {
            entityWrapper.eq(PromotionSku::getClass2Id, param.getClass2Id());
        }
        if (!CollectionUtils.isEmpty(param.getClass2Ids())) {
            entityWrapper.in(PromotionSku::getClass2Id, param.getClass2Ids());
        }
        if (param.getActivityStatus() != null) {
            entityWrapper.eq(PromotionSku::getActivityStatus, param.getActivityStatus());
        }
        if (param.getActivityStartTime() != null) {
            entityWrapper.ge(PromotionSku::getActivityEndTime, param.getActivityStartTime());
        }
        if (param.getActivityEndTime() != null) {
            entityWrapper.le(PromotionSku::getActivityStartTime, param.getActivityEndTime());
        }
        if (!StringUtils.isEmpty(param.getActivityName())) {
            entityWrapper.like(PromotionSku::getActivityName, param.getActivityName() + "%");
        }
        entityWrapper.eq(PromotionSku::getIsDeleted, 0);
        entityWrapper.orderBy(true, false, PromotionSku::getId);
        IPage<PromotionSku> pageResult = promotionSkuManager.page(page, entityWrapper);
        Page<PromotionSkuDto> dtoPageResult = new Page<>();
        BeanUtils.copyProperties(pageResult, dtoPageResult);
        ListWithPage<PromotionSkuDto> result = new ListWithPage<>();
        result.setTotalCount((int) dtoPageResult.getTotal());
        result.setListData(dtoPageResult.getRecords());
        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean createPromotionSku(CreatePromotionSkuParam param) {
        //入参数据类型转换
        List<UpdatePromotionSkuParam> updateParams = castCreate2Update(param);
        //校验入参
        validateParam(updateParams);
        List<PromotionSku> paramList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(param.getShopList())) {
            Integer now = DateUtil.getCurrentTimeIntValue();
            for (CreatePromotionSkuParam.Address address : param.getShopList()) {
                PromotionSku promotionSku = new PromotionSku();
                BeanUtils.copyProperties(param, promotionSku);
                promotionSku.setCityId(address.getCityId());
                promotionSku.setCityName(address.getCityName());
                promotionSku.setShopId(address.getShopId());
                promotionSku.setShopName(address.getShopName());
                promotionSku.setCT(now);
                promotionSku.setUT(now);
                promotionSku.setIsDeleted(0);
                paramList.add(promotionSku);
            }
            if (promotionSkuManager.saveBatch(paramList)) {
                return true;
            } else {
                throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("请补全所有信息");
            }
        } else {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("请补全所有信息");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updatePromotionSku(List<UpdatePromotionSkuParam> params) {
        //校验参数
        validateParam(params);
        for (UpdatePromotionSkuParam param : params) {
            PromotionSku promotionSku = new PromotionSku();
            BeanUtils.copyProperties(param, promotionSku);
            promotionSku.setUT(DateUtil.getCurrentTimeIntValue());
            if (promotionSkuManager.updateById(promotionSku)) {
                continue;
            } else {
                throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("修改促销活动失败");
            }
        }
        return true;
    }

    @Override
    public List<Integer> checkSkuExistOrNot(QueryPromotionSkuParam param) {
        if (CollectionUtils.isEmpty(param.getSkuIds())) {
            return param.getSkuIds();
        }
        int now = DateUtil.getCurrentTimeIntValue();
        LambdaQueryWrapper<PromotionSku> entityWrapper = new LambdaQueryWrapper<PromotionSku>();
        if (param.getShopId() != null) {
            entityWrapper.eq(PromotionSku::getShopId, param.getShopId());
        }
        if (!CollectionUtils.isEmpty(param.getSkuIds())) {
            entityWrapper.in(PromotionSku::getSkuId, param.getSkuIds());
        }
        entityWrapper.le(PromotionSku::getActivityStartTime, now);
        entityWrapper.ge(PromotionSku::getActivityEndTime, now);
        List<PromotionSku> skuExist = promotionSkuManager.list(entityWrapper);
        if (CollectionUtils.isEmpty(skuExist)) {
            return param.getSkuIds();
        }
        List<Integer> skuIdExist = skuExist.stream().map(PromotionSku::getSkuId).distinct().collect(Collectors.toList());
        List<Integer> skuIdNotExist = param.getSkuIds().stream().filter(o -> !skuIdExist.contains(o)).collect(Collectors.toList());
        return skuIdNotExist;
    }

    private void validateParam(List<UpdatePromotionSkuParam> params) {
        if (CollectionUtils.isEmpty(params)) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("入参不能为空");
        }
        for (UpdatePromotionSkuParam param : params) {
            if (param.getActivityStartTime() == null || param.getActivityStartTime() == 0) {
                throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("请输入活动开始时间");
            }
            if (param.getActivityEndTime() == null || param.getActivityEndTime() == 0) {
                throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("请输入活动结束时间");
            }
            if (param.getActivityStartTime() != null
                    && param.getActivityEndTime() != null
                    && param.getActivityStartTime() > param.getActivityEndTime()) {
                throw new BusinessException("活动开始时间不能大于结束时间");
            }
        }
    }

    private List<UpdatePromotionSkuParam> castCreate2Update(CreatePromotionSkuParam param) {
        UpdatePromotionSkuParam updateParam = new UpdatePromotionSkuParam();
        BeanUtils.copyProperties(param, updateParam);
        List<UpdatePromotionSkuParam> updateParams = new ArrayList<>();
        updateParams.add(updateParam);
        return updateParams;
    }
}
