package com.qmfresh.promotion.service.impl;

import com.qmfresh.promotion.bean.price.ModifyPriceBean;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.manager.IPromotionPriceMananger;
import com.qmfresh.promotion.service.IPromotionPriceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wyh on 2019/8/10.
 *
 * @author wyh
 */
@Service
@Slf4j
public class PromotionPriceServiceImpl implements IPromotionPriceService {

    @Resource
    private IPromotionPriceMananger promotionPriceMananger;

    @Override
    public Boolean modifyPrice(List<ModifyPriceBean> param) {
        Assert.isTrue(!ListUtil.isNullOrEmpty(param), "参数不能为空");
        for (ModifyPriceBean modifyPriceBean : param) {
            Assert.notNull(modifyPriceBean.getChannel(), "渠道不能为空");
            Assert.notNull(modifyPriceBean.getShopId(), "门店不能为空");
            Assert.notNull(modifyPriceBean.getPrice(), "原价不能为空");
            Assert.notNull(modifyPriceBean.getPriceId(), "价格id不能为空");
            Assert.notNull(modifyPriceBean.getSsuFormatId(), "ssuFormatId不能为空");
            Assert.notNull(modifyPriceBean.getClass2Id(), "二级分类id不能为空");
        }
        return promotionPriceMananger.modifyPrice(param);
    }
}
