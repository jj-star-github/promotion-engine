package com.qmfresh.promotion.service;

import com.qmfresh.promotion.bean.promotion.PageQueryActivityParam;
import com.qmfresh.promotion.bean.promotion.PromotionActivityBean;
import com.qmfresh.promotion.bean.promotion.QueryActivityParam;
import com.qmfresh.promotion.dto.IsActGoodParam;
import com.qmfresh.promotion.dto.SearchAfterVipParam;
import com.qmfresh.promotion.dto.SearchVipActParam;
import com.qmfresh.promotion.dto.VipSkuVo;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.PromotionActivity;
import java.util.List;

/**
 * Created by wyh on 2019/5/24.
 *
 * @author wyh
 */
public interface IPromotionActivityService {
    /**
     * 根据活动id获取促销活动信息，外部调用：C端商城
     *
     * @param activityId
     * @return
     */
    PromotionActivity queryPromotionActivityById(Long activityId);

    /**
     * 分页查询促销活动
     *
     * @param param
     */
    ListWithPage<PromotionActivityBean> pageQuery(PageQueryActivityParam param);

    /**
     * 查询促销活动
     *
     * @param param
     * @return
     */
    List<PromotionActivity> queryByCondition(QueryActivityParam param);

    /**
     * 活动上线
     *
     * @param promotionActivity
     * @return
     */
    Boolean online(PromotionActivity promotionActivity);

    /**
     * 活动下线
     *
     * @param promotionActivity
     * @return
     */
    Boolean stop(PromotionActivity promotionActivity);

    /**
     * 活动删除
     *
     * @param promotionActivity
     * @return
     */
    Boolean delete(PromotionActivity promotionActivity);

    /**
     * 活动结束
     *
     * @param activityIds
     * @return
     */
    Boolean finish(List<Long> activityIds);

    /**
     * 活动过期（针对门店创建的未进行审核的活动）
     * @param activityIds
     * @return
     */
    Boolean outOfDate(List<Long> activityIds);

    /**
     * 定时开启审核通过的活动（针对门店创建的已审核通过的活动）
     * @param activityIds
     * @return
     */
    Boolean start(List<Long> activityIds);

    /**
     * 查看当前促销活动
     *
     * @param query 查询条件
     * @return 活动列表
     */
    List<PromotionActivity> queryCurrentPromotionActivity(QueryActivityParam query);
    /**
     * 查看门店当前促销活动
     *
     * @param query 查询条件
     * @return 活动列表
     */
    List<PromotionActivity> queryPromotionActivityByShop(QueryActivityParam query);
    //门店商品当前时间是否有会员活动
    List<VipSkuVo> searchVipSku(SearchVipActParam param);
    //门店商品后三天的会员活动日
    List<Integer> getAfterVipdays(SearchAfterVipParam param);
    //查看门店商品有没有参加活动
    boolean isActGood(IsActGoodParam param);
}
