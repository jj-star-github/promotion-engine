package com.qmfresh.promotion.service;

import com.qmfresh.promotion.bean.order.QueryOrderParam;
import com.qmfresh.promotion.bean.order.UserOrderItemSummary;
import com.qmfresh.promotion.dto.CreateCpFromShopParam;
import com.qmfresh.promotion.dto.CreateCpPromotionParam;
import com.qmfresh.promotion.dto.PromotionAreaDto;
import com.qmfresh.promotion.dto.PromotionProductDto;

import java.util.List;

/**
 * 老网关业务处理迁移
 */
public interface IOldGatewayService {

    List<PromotionAreaDto> getAreaDto(List<Integer> shopIds);

    PromotionProductDto convertSkuToSsuFormat(List<Integer> shopIds, Integer channel, PromotionProductDto productDto);

    List<Integer> getSsuFormatBySkuIds(List<Integer> shopIds, Integer channel, List<Integer> skuIds);

    CreateCpPromotionParam buildParam(List<CreateCpFromShopParam> list);

    void buildFullInfo(CreateCpPromotionParam param);

}
