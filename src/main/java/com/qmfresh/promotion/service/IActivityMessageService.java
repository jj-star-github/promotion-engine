package com.qmfresh.promotion.service;

import java.util.List;

import com.qmfresh.promotion.dto.CreateCpPromotionParam;
import com.qmfresh.promotion.dto.CreateVipPromotionParam;
import com.qmfresh.promotion.message.sendbody.ActivityCreatBody;
import com.qmfresh.promotion.message.sendbody.ActivityModifyBody;

public interface IActivityMessageService {
	List<ActivityCreatBody> buildVipActivityBody(Long activityId,CreateVipPromotionParam param);
	List<ActivityCreatBody> buildCpActivityBody(Long activityId,CreateCpPromotionParam param);
	List<ActivityModifyBody> buildActivityModifyBody(List<Long> activityIds,Integer userId,String userName,Integer operate);
	List<ActivityCreatBody> buildCpForShopActivityBody(List<Long> activityIdList);
}
