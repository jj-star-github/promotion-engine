package com.qmfresh.promotion.service;

import com.qmfresh.promotion.bean.promotion.PromotionSellOutSkuWarningBean;
import com.qmfresh.promotion.dto.PromotionSellOutWarningReq;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.PromotionSellOutSkuWarning;

import java.util.List;

public interface IPromotionSellOutWarningService {

    Boolean add(PromotionSellOutWarningReq req);

    ListWithPage<PromotionSellOutSkuWarningBean> search(PromotionSellOutWarningReq req);

    Boolean update(PromotionSellOutWarningReq req);

    Boolean setShopWarning(PromotionSellOutWarningReq req);

    Boolean updateSkuWarning(List<PromotionSellOutSkuWarning> warnings);

    void createWarningDetail(String time);

    void pushWarningDetailToShop();


}
