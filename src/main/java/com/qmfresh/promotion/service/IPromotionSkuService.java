package com.qmfresh.promotion.service;

import com.qmfresh.promotion.dto.CreatePromotionSkuParam;
import com.qmfresh.promotion.dto.PromotionSkuDto;
import com.qmfresh.promotion.dto.QueryPromotionSkuParam;
import com.qmfresh.promotion.dto.UpdatePromotionSkuParam;
import com.qmfresh.promotion.dto.base.ListWithPage;

import java.util.List;

/**
 * @program: promotion-engine
 * @description: 促销商品管理服务
 * @author: xbb
 * @create: 2019-11-29 16:40
 **/
public interface IPromotionSkuService {

    /**
     * 分页查询
     * @param param
     * @return
     */
    ListWithPage<PromotionSkuDto> getPageList(QueryPromotionSkuParam param);

    /**
     * 创建商品促销活动
     * @param param
     * @return
     */
    Boolean createPromotionSku(CreatePromotionSkuParam param);

    /**
     * 更新商品促销活动
     * @param param
     * @return
     */
    Boolean updatePromotionSku(List<UpdatePromotionSkuParam> param);

    /**
     * 校验sku有没有参与促销(api)
     * @return
     */
    List<Integer> checkSkuExistOrNot(QueryPromotionSkuParam param);
}
