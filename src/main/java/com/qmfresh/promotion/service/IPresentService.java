package com.qmfresh.promotion.service;

import com.qmfresh.promotion.bean.present.*;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.PresentActivity;
import java.util.List;

/**
 * Created by wyh on 2019/6/11.
 *
 * @author wyh
 */
public interface IPresentService {

    List<PresentActivityDto> queryList(PageQueryParam param);

    ListWithPage<PresentActivityDto> pageQuery(PageQueryParam param);

    /**
     * 创建赠品
     *
     * @param param
     */
    Boolean createPresentActivity(CreatePresentActivity param);
    
    /**
     * 删除赠品
     *
     * @param param
     */
    Boolean delPresentActivity(DelPresentActivity param);
    
    /**
     * 活动绑定赠品
     *
     * @param activityId
     * @param presentIds
     * @return
     */
    Boolean bindPresent(Long activityId, List<Long> presentIds);

    /**
     * 修改赠品
     *
     * @param param
     * @return
     */
    Boolean modifyPresent(UpdatePresentActivity param);

}
