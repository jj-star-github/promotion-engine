package com.qmfresh.promotion.service;

import com.qmfresh.promotion.entity.PromotionSellOutShopWarning;

public interface IPromotionSellOutShopWarningService {

    PromotionSellOutShopWarning getShopWarningSettingByShopId(Integer shopId);
}
