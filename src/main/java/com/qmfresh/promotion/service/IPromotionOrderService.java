package com.qmfresh.promotion.service;

import com.qmfresh.promotion.bean.order.PromotionOrderDto;
import com.qmfresh.promotion.bean.order.QueryOrderParam;
import com.qmfresh.promotion.entity.PromotionOrderItem;
import java.util.List;

/**
 * Created by wyh on 2019/6/14.
 *
 * @author wyh
 */
public interface IPromotionOrderService {

    /**
     * 创建促销订单
     *
     * @param param
     * @return
     */
    boolean createPromotionOrder(PromotionOrderDto param);

    /**
     * 查看促销订单详情
     *
     * @param queryOrderParam
     * @return
     */
    List<PromotionOrderItem> queryOrderItem(QueryOrderParam queryOrderParam);
}
