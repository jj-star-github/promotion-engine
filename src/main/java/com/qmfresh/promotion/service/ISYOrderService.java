package com.qmfresh.promotion.service;

import com.qmfresh.promotion.bean.order.QueryOrderParam;
import com.qmfresh.promotion.bean.order.UserOrderItemSummary;
import java.util.List;

/**
 * Created by wyh on 2019/6/3.
 *
 * @author wyh
 */
public interface ISYOrderService {
    /**
     * 查看用户订单行汇总
     *
     * @param param
     * @return
     */
    List<UserOrderItemSummary> queryOrder(QueryOrderParam param);
}
