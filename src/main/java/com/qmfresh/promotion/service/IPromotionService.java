package com.qmfresh.promotion.service;

import com.qmfresh.promotion.bean.price.ModifyVipPriceBean;
import com.qmfresh.promotion.bean.price.ModifyVipPriceResult;
import com.qmfresh.promotion.bean.sku.Ssu;
import com.qmfresh.promotion.bean.sku.SsuQueryForOnline;
import com.qmfresh.promotion.dto.*;

import java.util.List;

/**
 * Created by wyh on 2019/6/4.
 *
 * @author wyh
 */
public interface IPromotionService {

    /**
     * 获取促销详情
     *
     * @param activityId
     * @return
     */
    PromotionDto getPromotionDetail(Long activityId);

    /**
     * 创建买赠规则
     *
     * @param param
     * @return
     */
    Boolean createBzPromotion(CreateBzPromotionParam param);

    /**
     * 创建单品促销规则
     *
     * @param param
     * @return
     */
    Boolean createDpPromotion(CreateDpPromotionParam param);

    /**
     * 创建满赠规则
     *
     * @param param
     * @return
     */
    Boolean createMzPromotion(CreateMzPromotionParam param);

    /**
     * 创建满减促销
     *
     * @param param
     * @return
     */
    Boolean createMjPromotion(CreateMjPromotionParam param);

    /**
     * 创建会员促销规则
     *
     * @param param
     * @return
     */
    Boolean createVipPromotion(CreateVipPromotionParam param);

    /**
     * 创建改价促销活动
     * @param param
     * @return
     */
    Boolean createCpPromotion(CreateCpPromotionParam param);

    /**
     * 修改会员价格
     *
     * @param param
     * @return
     */
    ModifyVipPriceResult modifyVipPrice(ModifyVipPriceBean param);

    /**
     * 修改买赠促销
     *
     * @param param
     * @return
     */
    Boolean modifyBzPromotion(CreateBzPromotionParam param);

    /**
     * 修改单品促销
     *
     * @param param
     * @return
     */
    Boolean modifyDpPromotion(CreateDpPromotionParam param);

    /**
     * 修改满赠促销
     *
     * @param param
     * @return
     */
    Boolean modifyMzPromotion(CreateMzPromotionParam param);

    /**
     * 修改会员促销
     *
     * @param param
     * @return
     */
    Boolean modifyVipPromotion(CreateVipPromotionParam param);

    /**
     * 修改改价促销活动
     * @param param
     * @return
     */
    Boolean modifyCpPromotion(CreateCpPromotionParam param);

    /**
     * 删除改价促销活动
     * @param param
     * @return
     */
    Boolean deleteCpPromotion(CreateCpPromotionParam param);

    List<Ssu> getSsuListByParam(SsuQueryForOnline query);

    List<QueryPriceChangeResult> queryChangePrice(List<PriceChangeParam> list);

    Boolean modifyCpPromotionStatus(ModifyPriceParam<ActivateCpPromotionParam> param);

    /**
     * 批量更新数据状态
     */
    Boolean batchUpdateStatus(List<ActivateCpPromotionParam> list);

    /**
     * 校验商品是否参加会员活动
     * @param list
     * @return
     */
    List<CreateCpFromShopParam> checkSkuInVipPromotion(List<CreateCpFromShopParam> list);

    Boolean checkVipPromotionExist(Integer startTime,Integer endTime,List<CreateCpFromShopParam> list);
}
