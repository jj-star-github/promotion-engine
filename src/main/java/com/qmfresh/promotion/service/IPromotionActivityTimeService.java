package com.qmfresh.promotion.service;

import com.qmfresh.promotion.entity.PromotionActivity;
import java.util.List;

/**
 * Created by wyh on 2019/6/15.
 *
 * @author wyh
 */
public interface IPromotionActivityTimeService {
    List<PromotionActivity> queryByCondition();
}
