package com.qmfresh.promotion.service;

/**
 * Created by wyh on 2019/6/12.
 *
 * @author wyh
 */
public interface IPromotionTaskServie {

    /**
     * 批量修改会员价
     *
     * @return
     */
    Boolean createVipPrice();

    /**
     * 批量修改C端商城会员价
     * @return
     */
    Boolean createCmallVipPrice();

    /**
     * 批量修改
     * @return
     */
    Boolean createCpPrice();

}
