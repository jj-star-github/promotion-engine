package com.qmfresh.promotion.service;

import com.qmfresh.promotion.dto.PromotionProtocol;
import com.qmfresh.promotion.dto.PromotionSsuContext;
import java.util.List;

/**
 * Created by wyh on 2019/5/24.
 *
 * @author wyh
 */
public interface IExecutePromotionService {

    /**
     * 执行促销
     *
     * @param param
     * @return
     */
    List<PromotionSsuContext> execute(PromotionProtocol param);
}
