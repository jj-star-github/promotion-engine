package com.qmfresh.promotion.service;

import com.qmfresh.promotion.bean.promotion.QueryPromotionModuleParam;
import com.qmfresh.promotion.entity.PromotionModule;
import java.util.List;

/**
 * Created by wyh on 2019/5/24.
 *
 * @author wyh
 */
public interface IPromotionModuleService {

    /**
     * 查询促销模板
     *
     * @param param
     * @return
     */
    List<PromotionModule> queryByCondition(QueryPromotionModuleParam param);
}
