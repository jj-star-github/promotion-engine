package com.qmfresh.promotion.dto;

public class SearchVipSkuParam {
	private Integer skuId;
	private Integer class2Id;
	public Integer getSkuId() {
		return skuId;
	}
	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}
	public Integer getClass2Id() {
		return class2Id;
	}
	public void setClass2Id(Integer class2Id) {
		this.class2Id = class2Id;
	}
	@Override
	public String toString() {
		return "SearchVipSkuParam [skuId=" + skuId + ", class2Id=" + class2Id + "]";
	}
	
}
