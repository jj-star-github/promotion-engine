package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.entity.PromotionActivityTime;

import java.math.BigDecimal;
import java.util.List;

/**
 * @ClassName ShopCheckAuthorityParam
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/6 16:22
 */
public class ShopCheckAuthorityParam {

    private Integer skuId;

    private String skuName;

    private BigDecimal promotionPrice;

    private Integer shopId;

    private Integer shopName;

    private Long activityId;

    private Long userId;

    private String userName;

    private Integer applyReasonCode;

    private String applyReasonName;

    private Integer class1Id;
    //实时库存
    private BigDecimal realTimeInventory;
    //毛利率
    private BigDecimal profitRate;
    

  public BigDecimal getRealTimeInventory() {
		return realTimeInventory;
	}

	public void setRealTimeInventory(BigDecimal realTimeInventory) {
		this.realTimeInventory = realTimeInventory;
	}

	public BigDecimal getProfitRate() {
		return profitRate;
	}

	public void setProfitRate(BigDecimal profitRate) {
		this.profitRate = profitRate;
	}

private List<PromotionActivityTime> promotionActivityTimeList;

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getShopName() {
        return shopName;
    }

    public void setShopName(Integer shopName) {
        this.shopName = shopName;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public List<PromotionActivityTime> getPromotionActivityTimeList() {
        return promotionActivityTimeList;
    }

    public void setPromotionActivityTimeList(List<PromotionActivityTime> promotionActivityTimeList) {
        this.promotionActivityTimeList = promotionActivityTimeList;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getApplyReasonCode() {
        return applyReasonCode;
    }

    public void setApplyReasonCode(Integer applyReasonCode) {
        this.applyReasonCode = applyReasonCode;
    }

    public String getApplyReasonName() {
        return applyReasonName;
    }

    public void setApplyReasonName(String applyReasonName) {
        this.applyReasonName = applyReasonName;
    }

    public Integer getClass1Id() {
        return class1Id;
    }

    public void setClass1Id(Integer class1Id) {
        this.class1Id = class1Id;
    }
}
