package com.qmfresh.promotion.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class CreateCpFromShopParam implements Serializable {

    private static final long serialVersionUID = 1270563653519167485L;

    /**
     * 活动名称
     */
    private String activityName;

    private Integer shopId;

    private Integer skuId;

    private BigDecimal price;

    private Integer startTime;

    private Integer endTime;

    private String userName;

    private Integer userId;

    private Integer applyReasonCode;

    private String applyReasonName;

    private Integer class2Id;
    //实时库存
    private BigDecimal realTimeInventory;
//毛利率
    private BigDecimal profitRate;

    public BigDecimal getRealTimeInventory() {
		return realTimeInventory;
	}

	public void setRealTimeInventory(BigDecimal realTimeInventory) {
		this.realTimeInventory = realTimeInventory;
	}

	public BigDecimal getProfitRate() {
		return profitRate;
	}

	public void setProfitRate(BigDecimal profitRate) {
		this.profitRate = profitRate;
	}

	public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Integer getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getApplyReasonCode() {
        return applyReasonCode;
    }

    public void setApplyReasonCode(Integer applyReasonCode) {
        this.applyReasonCode = applyReasonCode;
    }

    public String getApplyReasonName() {
        return applyReasonName;
    }

    public void setApplyReasonName(String applyReasonName) {
        this.applyReasonName = applyReasonName;
    }

    public Integer getClass2Id() {
        return class2Id;
    }

    public void setClass2Id(Integer class2Id) {
        this.class2Id = class2Id;
    }

    @Override
    public String toString() {
        return "CreateCpFromShopParam{" +
                "activityName='" + activityName + '\'' +
                ", shopId=" + shopId +
                ", skuId=" + skuId +
                ", price=" + price +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", userName='" + userName + '\'' +
                ", userId=" + userId +
                ", applyReasonCode=" + applyReasonCode +
                ", applyReasonName='" + applyReasonName + '\'' +
                ", class2Id=" + class2Id +
                '}';
    }
}
