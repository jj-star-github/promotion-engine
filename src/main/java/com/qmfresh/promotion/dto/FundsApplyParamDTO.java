package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.strategy.bean.BzRule;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
@Data
@ApiModel("申请营销经费入参")
public class FundsApplyParamDTO extends BaseRequestParam implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;
    //店铺编码
    @ApiModelProperty("店铺编码")
    private  Long shopId;
    @ApiModelProperty("店铺名称")
    private  String shopName;
    //申请人编号
    @ApiModelProperty("申请人编号")
    private  Long applyUserId;
    @ApiModelProperty("申请人名称")
    private String applyUserName;
    //申请金额
    @ApiModelProperty("申请金额")
    private BigDecimal applyAmount;
    //申请理由
    @ApiModelProperty("申请理由")
    private  String applyRemark;
    //申请人编号
    @ApiModelProperty("userId")
    private  Long userId;
    @ApiModelProperty("userName")
    private String userName;

}
