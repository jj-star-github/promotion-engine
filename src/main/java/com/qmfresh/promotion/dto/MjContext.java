package com.qmfresh.promotion.dto;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by wyh on 2019/5/28.
 *
 * @author wyh
 */
public class MjContext {

    /**
     * 促销活动id
     */
    private Long activityId;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 满金额
     */
    private BigDecimal meetMoney;

    /**
     * 直减金额
     */
    private BigDecimal reductionMoney;

    /**
     * 规则id
     */
    private Long promotionRuleId;

    /**
     * 促销描述
     */
    private String description;

    private List<MeetInfo> meetInfos;

    /**
     * 促销轨迹
     */
    private PromotionTrace promotionTrace;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public BigDecimal getMeetMoney() {
        return meetMoney;
    }

    public void setMeetMoney(BigDecimal meetMoney) {
        this.meetMoney = meetMoney;
    }

    public BigDecimal getReductionMoney() {
        return reductionMoney;
    }

    public void setReductionMoney(BigDecimal reductionMoney) {
        this.reductionMoney = reductionMoney;
    }

    public Long getPromotionRuleId() {
        return promotionRuleId;
    }

    public void setPromotionRuleId(Long promotionRuleId) {
        this.promotionRuleId = promotionRuleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MeetInfo> getMeetInfos() {
        return meetInfos;
    }

    public void setMeetInfos(List<MeetInfo> meetInfos) {
        this.meetInfos = meetInfos;
    }

    public PromotionTrace getPromotionTrace() {
        return promotionTrace;
    }

    public void setPromotionTrace(PromotionTrace promotionTrace) {
        this.promotionTrace = promotionTrace;
    }
}
