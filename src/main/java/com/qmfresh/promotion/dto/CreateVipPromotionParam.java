package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.entity.PromotionActivityTime;
import com.qmfresh.promotion.strategy.bean.VipRule;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wyh on 2019/6/5.
 *
 * @author wyh
 */
public class CreateVipPromotionParam extends BaseRequestParam implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    private Long activityId;
    /**
     * 0所有平台,1.B端,2.C端线上,3.C端线下
     */
    private Integer channel;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 活动时间
     */
    private List<PromotionActivityTime> activityTimes;

    /**
     * 折扣规则
     */
    private VipRule vipRule;

    /**
     * 用户类型（1.全部，2.普通会员，10.黄金会员）
     */
    private List<Integer> crowdTypes;

    /**
     * 区域、门店
     */
    private List<PromotionAreaDto> areaDtos;

    /**
     * 单品促销适用商品
     */
    private PromotionProductDto promotionProductDto;

    private Long userId;

    private String userName;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public List<PromotionActivityTime> getActivityTimes() {
        return activityTimes;
    }

    public void setActivityTimes(List<PromotionActivityTime> activityTimes) {
        this.activityTimes = activityTimes;
    }

    public VipRule getVipRule() {
        return vipRule;
    }

    public void setVipRule(VipRule vipRule) {
        this.vipRule = vipRule;
    }

    public List<Integer> getCrowdTypes() {
        return crowdTypes;
    }

    public void setCrowdTypes(List<Integer> crowdTypes) {
        this.crowdTypes = crowdTypes;
    }

    public List<PromotionAreaDto> getAreaDtos() {
        return areaDtos;
    }

    public void setAreaDtos(List<PromotionAreaDto> areaDtos) {
        this.areaDtos = areaDtos;
    }

    public PromotionProductDto getPromotionProductDto() {
        return promotionProductDto;
    }

    public void setPromotionProductDto(PromotionProductDto promotionProductDto) {
        this.promotionProductDto = promotionProductDto;
    }

    public Long getUserId() {
        return null != userId ? userId : this.getOperatorId();
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return StringUtils.isNotEmpty(userName) ? userName : this.getOperatorName();
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    private Long operatorId;
    private String operatorName;

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }
}
