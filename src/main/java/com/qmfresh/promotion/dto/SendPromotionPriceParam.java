package com.qmfresh.promotion.dto;

import java.math.BigDecimal;

/**
 * @ClassName SendPromotionPriceParam
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/6 17:32
 */
public class SendPromotionPriceParam {

    private Integer shopId;

    private Integer skuId;

    private BigDecimal price;

    private Integer userId;

    private String userName;

    /**
     * 活动创建来源，和本系统的creatorFrom的含义一样，只是对应值不同（1：总部，2：门店）
     * 此值：2.总部促销改价 3.门店促销改价
     */
    private Integer reasonType;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getReasonType() {
        return reasonType;
    }

    public void setReasonType(Integer reasonType) {
        this.reasonType = reasonType;
    }
}
