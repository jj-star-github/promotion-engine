package com.qmfresh.promotion.dto;

import java.math.BigDecimal;

/**
 * @ClassName SendVipPriceParam
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/6 17:33
 */
public class SendVipPriceParam {

    private Integer shopId;

    private Integer skuId;

    private BigDecimal price;

    private BigDecimal vipPrice;

    private Long userId;

    private String userName;

    /**
     * 商品售价，3位小数
     */
    private BigDecimal salePrice;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getVipPrice() {
        return vipPrice;
    }

    public void setVipPrice(BigDecimal vipPrice) {
        this.vipPrice = vipPrice;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }
}
