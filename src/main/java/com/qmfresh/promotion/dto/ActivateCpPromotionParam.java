package com.qmfresh.promotion.dto;

import java.math.BigDecimal;

/**
 * @ClassName ActivateCpPromotionParam
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/7 9:20
 */
public class ActivateCpPromotionParam {

    private Long activityId;

    private Integer shopId;

    private Integer skuId;

    private BigDecimal promotionPrice;

    private Integer status;

    private Integer startTime;

    private Integer endTime;

    private Long userId;

    private String userName;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Integer getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

	@Override
	public String toString() {
		return "ActivateCpPromotionParam [activityId=" + activityId + ", shopId=" + shopId + ", skuId=" + skuId
				+ ", promotionPrice=" + promotionPrice + ", status=" + status + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", userId=" + userId + ", userName=" + userName + "]";
	}
    
}
