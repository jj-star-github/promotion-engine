package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.entity.PresentActivity;
import com.qmfresh.promotion.entity.PresentSsu;

/**
 * Created by wyh on 2019/5/31.
 *
 * @author wyh
 */
public class BzContext {

    /**
     * 促销活动id
     */
    private Long activityId;

    private Boolean isMeet;

    /**
     * 赠品活动
     */
    private PresentActivity presentActivity;

    /**
     * 赠品
     */
    private PresentSsu presentSsu;

    /**
     * 促销描述
     */
    private String description;

    /**
     * 下一级促销描述
     */
    private String nextLevelDescrition;

    /**
     * 促销轨迹
     */
    private PromotionTrace promotionTrace;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Boolean getMeet() {
        return isMeet;
    }

    public void setMeet(Boolean meet) {
        isMeet = meet;
    }

    public PresentActivity getPresentActivity() {
        return presentActivity;
    }

    public void setPresentActivity(PresentActivity presentActivity) {
        this.presentActivity = presentActivity;
    }

    public PresentSsu getPresentSsu() {
        return presentSsu;
    }

    public void setPresentSsu(PresentSsu presentSsu) {
        this.presentSsu = presentSsu;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNextLevelDescrition() {
        return nextLevelDescrition;
    }

    public void setNextLevelDescrition(String nextLevelDescrition) {
        this.nextLevelDescrition = nextLevelDescrition;
    }

    public PromotionTrace getPromotionTrace() {
        return promotionTrace;
    }

    public void setPromotionTrace(PromotionTrace promotionTrace) {
        this.promotionTrace = promotionTrace;
    }
}
