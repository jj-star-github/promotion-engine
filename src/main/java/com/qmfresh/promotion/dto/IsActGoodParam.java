package com.qmfresh.promotion.dto;

import java.util.List;

public class IsActGoodParam {
	private List<Integer> skuIds;
	private Integer shopId;
	
	public List<Integer> getSkuIds() {
		return skuIds;
	}
	public void setSkuIds(List<Integer> skuIds) {
		this.skuIds = skuIds;
	}
	public Integer getShopId() {
		return shopId;
	}
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
	
}
