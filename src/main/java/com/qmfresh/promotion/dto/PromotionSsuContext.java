package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.platform.domain.shared.BigDecimalUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by wyh on 2019/5/24.
 *
 * @author wyh
 */
@ApiModel(value = "PromotionSsuContext", description = "订单行信息")
public class PromotionSsuContext {

    private Long activityId;

    private Integer labelId;

    /**
     * 是否满足促销
     */
    private Boolean isMeet;

    private Integer ssuFormatId;

    private Integer skuId;

    private Integer class2Id;

    private Integer class1Id;

    private String ssuName;

    /**
     * 下单量
     */
    @ApiModelProperty("下单量")
    private BigDecimal amount;

    /**
     * 原始金额
     */
    @ApiModelProperty("原价")
    private BigDecimal originPrice;

    /**
     * 会员价
     */
    @ApiModelProperty("会员价")
    private BigDecimal vipPrice;

    /**
     * 折后价
     */
    @ApiModelProperty("折后价")
    private BigDecimal priceAfterDiscount;

    /**
     * 优惠后金额
     */
    @ApiModelProperty("优惠后价格-终价")
    private BigDecimal preferentialPrice;

    /**
     * 促销执行轨迹
     */
    @ApiModelProperty("会员价的改价轨迹")
    private PromotionTrace promotionTrace;

    /**
     * 商品类型：1.普通商品，2.会员商品，3.改价商品，4.促销商品
     */
    @ApiModelProperty("商品类型：1.普通商品，2.会员商品，3.改价商品，4.促销商品")
    private Integer priceType;
    @ApiModelProperty("活动信息")
    private List<ActivityModule> activityModules;
    /**
     * 现价，现价*数量
     */
    @ApiModelProperty("实收金额 应收(preferentialPrice*amount)-优惠券分摊-番茄币均摊-抹零分摊")
    private BigDecimal realMoney = BigDecimal.ZERO;
    /**
     * 优惠券优惠金额
     */
    @ApiModelProperty("优惠券优惠金额")
    private BigDecimal couponMoney = BigDecimal.ZERO;
    /**
     * 番茄币优惠金额
     */
    @ApiModelProperty("番茄币优惠金额")
    private BigDecimal coinToMoney = BigDecimal.ZERO;
    /**
     * 抹零优惠金额
     */
    @ApiModelProperty("抹零优惠金额")
    private BigDecimal freeMoney = BigDecimal.ZERO;

    /**
     * 满减金额
     */
    @ApiModelProperty("满减金额")
    private BigDecimal mjMoney = BigDecimal.ZERO;

    /**
     * 单品优惠价格 （原价-终价）*重量
     */
    @ApiModelProperty("单品优惠价格 （原价-终价）*重量")
    private BigDecimal preferentialMoney = BigDecimal.ZERO;

    /**
     * 小计金额 原价*重量
     */
    @ApiModelProperty("小计金额 原价*重量")
    private BigDecimal originMoney = BigDecimal.ZERO;

    /**
     * 现总金额： 终价*数量
     */
    @ApiModelProperty("现总价 终价*数量")
    private BigDecimal finalMoney = BigDecimal.ZERO;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Integer getLabelId() {
        return labelId;
    }

    public void setLabelId(Integer labelId) {
        this.labelId = labelId;
    }

    public Boolean getMeet() {
        return isMeet;
    }

    public void setMeet(Boolean meet) {
        isMeet = meet;
    }

    public Integer getSsuFormatId() {
        return ssuFormatId;
    }

    public void setSsuFormatId(Integer ssuFormatId) {
        this.ssuFormatId = ssuFormatId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public Integer getClass2Id() {
        return class2Id;
    }

    public void setClass2Id(Integer class2Id) {
        this.class2Id = class2Id;
    }

    public Integer getClass1Id() {
        return class1Id;
    }

    public void setClass1Id(Integer class1Id) {
        this.class1Id = class1Id;
    }

    public String getSsuName() {
        return ssuName;
    }

    public void setSsuName(String ssuName) {
        this.ssuName = ssuName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(BigDecimal originPrice) {
        this.originPrice = originPrice;
    }

    public BigDecimal getVipPrice() {
        return vipPrice;
    }

    public void setVipPrice(BigDecimal vipPrice) {
        this.vipPrice = vipPrice;
    }

    public BigDecimal getPriceAfterDiscount() {
        return priceAfterDiscount;
    }

    public void setPriceAfterDiscount(BigDecimal priceAfterDiscount) {
        this.priceAfterDiscount = priceAfterDiscount;
    }

    public BigDecimal getPreferentialPrice() {
        return preferentialPrice;
    }

    public void setPreferentialPrice(BigDecimal preferentialPrice) {
        this.preferentialPrice = preferentialPrice;
    }

    public PromotionTrace getPromotionTrace() {
        return promotionTrace;
    }

    public void setPromotionTrace(PromotionTrace promotionTrace) {
        this.promotionTrace = promotionTrace;
    }

    public Integer getPriceType() {
        return priceType;
    }

    public void setPriceType(Integer priceType) {
        this.priceType = priceType;
    }

    public List<ActivityModule> getActivityModules() {
        return activityModules;
    }

    public void setActivityModules(List<ActivityModule> activityModules) {
        this.activityModules = activityModules;
    }

    public BigDecimal getRealMoney() {
        return realMoney;
    }

    public void setRealMoney(BigDecimal realMoney) {
        this.realMoney = realMoney;
    }

    public BigDecimal getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(BigDecimal couponMoney) {
        this.couponMoney = couponMoney;
    }

    public BigDecimal getCoinToMoney() {
        return coinToMoney;
    }

    public void setCoinToMoney(BigDecimal coinToMoney) {
        this.coinToMoney = coinToMoney;
    }

    public BigDecimal getFreeMoney() {
        return freeMoney;
    }

    public void setFreeMoney(BigDecimal freeMoney) {
        this.freeMoney = freeMoney;
    }

    public BigDecimal getMjMoney() {
        return mjMoney;
    }

    public void setMjMoney(BigDecimal mjMoney) {
        this.mjMoney = mjMoney;
    }

    public BigDecimal getPreferentialMoney() {
        return preferentialMoney;
    }

    public void setPreferentialMoney(BigDecimal preferentialMoney) {
        this.preferentialMoney = preferentialMoney;
    }

    public BigDecimal getOriginMoney() {
        return originMoney;
    }

    public void setOriginMoney(BigDecimal originMoney) {
        this.originMoney = originMoney;
    }

    public BigDecimal getFinalMoney() {
        return finalMoney;
    }

    public void setFinalMoney(BigDecimal finalMoney) {
        this.finalMoney = finalMoney;
    }
}
