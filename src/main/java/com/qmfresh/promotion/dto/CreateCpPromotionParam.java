package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.entity.PromotionActivityTime;
import com.qmfresh.promotion.message.sendbody.ActivityModifyBody;
import com.qmfresh.promotion.strategy.bean.CpRule;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName CreateCpPromotionParam
 * @Description TODO
 * @Author xbb
 * @Date 2020/2/25 10:38
 */
public class CreateCpPromotionParam extends BaseRequestParam implements Serializable {

    private Long activityId;
    /**
     * 0所有平台,1.B端,2.C端线上,3.C端线下
     */
    private Integer channel;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 活动时间
     */
    private List<PromotionActivityTime> activityTimes;

    /**
     * 改价规则
     */
    private CpRule cpRule;

    /**
     * 用户类型（1.全部，2.普通会员，10.黄金会员）
     */
    private List<Integer> crowdTypes;

    /**
     * 区域、门店
     */
    private List<PromotionAreaDto> areaDtos;

    /**
     * 单品促销适用商品
     */
    private PromotionProductDto promotionProductDto;

    /**
     * 改价活动来源 1：运营，2：门店
     */
    private Integer creatorFrom;

    private Long userId;

    private String userName;

    /**
     * 活动开始时间（兼容门店端的接口传参）
     */
    private Integer startTime;
    /**
     * 活动结束时间（兼容门店端的接口传参）
     */
    private Integer endTime;
    /**
     * 改价原因code
     */
    private Integer applyReasonCode;
    /**
     * 改价原因
     */
    private String applyReasonName;
    //原始参数
    private List<CreateCpFromShopParam> originparam;
    //修改活动的参数,仅用于总部修改活动的参数
    private List<ActivityModifyBody>  activityModifyList;
    
    public List<ActivityModifyBody> getActivityModifyList() {
		return activityModifyList;
	}

	public void setActivityModifyList(List<ActivityModifyBody> activityModifyList) {
		this.activityModifyList = activityModifyList;
	}

	public List<CreateCpFromShopParam> getOriginparam() {
		return originparam;
	}

	public void setOriginparam(List<CreateCpFromShopParam> originparam) {
		this.originparam = originparam;
	}

	public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public List<PromotionActivityTime> getActivityTimes() {
        return activityTimes;
    }

    public void setActivityTimes(List<PromotionActivityTime> activityTimes) {
        this.activityTimes = activityTimes;
    }

    public CpRule getCpRule() {
        return cpRule;
    }

    public void setCpRule(CpRule cpRule) {
        this.cpRule = cpRule;
    }

    public List<Integer> getCrowdTypes() {
        return crowdTypes;
    }

    public void setCrowdTypes(List<Integer> crowdTypes) {
        this.crowdTypes = crowdTypes;
    }

    public List<PromotionAreaDto> getAreaDtos() {
        return areaDtos;
    }

    public void setAreaDtos(List<PromotionAreaDto> areaDtos) {
        this.areaDtos = areaDtos;
    }

    public PromotionProductDto getPromotionProductDto() {
        return promotionProductDto;
    }

    public void setPromotionProductDto(PromotionProductDto promotionProductDto) {
        this.promotionProductDto = promotionProductDto;
    }

    public Long getUserId() {
        return null != userId ? userId : this.getOperatorId();
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return StringUtils.isNotEmpty(userName) ? userName : getOperatorName();
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getCreatorFrom() {
        return creatorFrom;
    }

    public void setCreatorFrom(Integer creatorFrom) {
        this.creatorFrom = creatorFrom;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Integer getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public Integer getApplyReasonCode() {
        return applyReasonCode;
    }

    public void setApplyReasonCode(Integer applyReasonCode) {
        this.applyReasonCode = applyReasonCode;
    }

    public String getApplyReasonName() {
        return applyReasonName;
    }

    public void setApplyReasonName(String applyReasonName) {
        this.applyReasonName = applyReasonName;
    }

    private Long operatorId;
    private String operatorName;

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }
}
