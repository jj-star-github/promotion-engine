package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.bean.coupon.CouponInfo;
import com.qmfresh.promotion.entity.PresentSsu;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by wyh on 2019/5/28.
 *
 * @author wyh
 */
@ApiModel(value = "MzContext", description = "满赠活动信息")
public class MzContext {

    /**
     * 促销活动id
     */
    @ApiModelProperty("促销活动id-预占使用")
    private Long activityId;

    /**
     * 活动名称
     */
    @ApiModelProperty("活动名称")
    private String activityName;

    /**
     * 是否满足促销
     */
    @ApiModelProperty("是否满足促销")
    private Boolean isMeet;

    /**
     * 满金额
     */
    @ApiModelProperty("满金额")
    private BigDecimal meetMoney;

    /**
     * 赠品类型
     */
    @ApiModelProperty("赠品类型")
    private Integer presentType;

    /**
     * 赠品信息
     */
    private PresentSsu presentSsu;

    /**
     * 赠券信息
     */
    @ApiModelProperty("优惠券信息")
    private CouponInfo couponInfo;

    /**
     * 规则id
     */
    private Long promotionRuleId;

    /**
     * 促销描述
     */
    private String description;

//    /**
//     * 下一级活动满足金额
//     */
//    private BigDecimal nextMeetMoney;
//
//    /**
//     * 满足下一级活动差异金额
//     */
//    private BigDecimal nextDiffMoney;

    /**
     * 下一级
     */
    @ApiModelProperty("下一级")
    private List<MeetInfo> meetInfos;

    /**
     * 促销轨迹
     */
    private PromotionTrace promotionTrace;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Boolean getMeet() {
        return isMeet;
    }

    public void setMeet(Boolean meet) {
        isMeet = meet;
    }

    public BigDecimal getMeetMoney() {
        return meetMoney;
    }

    public void setMeetMoney(BigDecimal meetMoney) {
        this.meetMoney = meetMoney;
    }

    public Integer getPresentType() {
        return presentType;
    }

    public void setPresentType(Integer presentType) {
        this.presentType = presentType;
    }

    public PresentSsu getPresentSsu() {
        return presentSsu;
    }

    public void setPresentSsu(PresentSsu presentSsu) {
        this.presentSsu = presentSsu;
    }

    public CouponInfo getCouponInfo() {
        return couponInfo;
    }

    public void setCouponInfo(CouponInfo couponInfo) {
        this.couponInfo = couponInfo;
    }

    public Long getPromotionRuleId() {
        return promotionRuleId;
    }

    public void setPromotionRuleId(Long promotionRuleId) {
        this.promotionRuleId = promotionRuleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MeetInfo> getMeetInfos() {
        return meetInfos;
    }

    public void setMeetInfos(List<MeetInfo> meetInfos) {
        this.meetInfos = meetInfos;
    }

    public PromotionTrace getPromotionTrace() {
        return promotionTrace;
    }

    public void setPromotionTrace(PromotionTrace promotionTrace) {
        this.promotionTrace = promotionTrace;
    }
}
