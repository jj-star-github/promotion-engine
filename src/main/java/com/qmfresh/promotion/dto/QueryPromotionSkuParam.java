package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.dto.base.PageQuery;

import java.util.List;

/**
 * @program: promotion-engine
 * @description:
 * @author: xbb
 * @create: 2019-11-30 09:58
 **/
public class QueryPromotionSkuParam extends PageQuery {

    /**
     * 城市id
     */
    private Integer cityId;
    /**
     * 城市名称
     */
    private String cityName;
    /**
     * 门店id
     */
    private Integer shopId;
    /**
     * 门店名称
     */
    private String shopName;
    /**
     * 商品id
     */
    private Integer skuId;
    /**
     * 商品名
     */
    private String skuName;
    /**
     * 活动id(暂不使用，后期关联t_promotion_activity表)
     */
    private Long activityId;
    /**
     * 活动名称
     */
    private String activityName;
    /**
     * 活动内容
     */
    private String activityContent;

    /**
     * 商品二级分类id
     */
    private Integer class2Id;
    /**
     * 商品二级分类名
     */
    private String class2Name;
    /**
     * 活动开始时间
     */
    private Integer activityStartTime;
    /**
     * 活动结束时间
     */
    private Integer activityEndTime;
    /**
     * 活动状态(0:有效，1:失效)
     */
    private Integer activityStatus;

    /**
     * skuId数组（入参）
     */
    private List<Integer> skuIds;

    /**
     * 二级分类数组（入参）
     */
    private List<Integer> class2Ids;

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityContent() {
        return activityContent;
    }

    public void setActivityContent(String activityContent) {
        this.activityContent = activityContent;
    }

    public Integer getClass2Id() {
        return class2Id;
    }

    public void setClass2Id(Integer class2Id) {
        this.class2Id = class2Id;
    }

    public String getClass2Name() {
        return class2Name;
    }

    public void setClass2Name(String class2Name) {
        this.class2Name = class2Name;
    }

    public Integer getActivityStartTime() {
        return activityStartTime;
    }

    public void setActivityStartTime(Integer activityStartTime) {
        this.activityStartTime = activityStartTime;
    }

    public Integer getActivityEndTime() {
        return activityEndTime;
    }

    public void setActivityEndTime(Integer activityEndTime) {
        this.activityEndTime = activityEndTime;
    }

    public Integer getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(Integer activityStatus) {
        this.activityStatus = activityStatus;
    }

    public List<Integer> getSkuIds() {
        return skuIds;
    }

    public void setSkuIds(List<Integer> skuIds) {
        this.skuIds = skuIds;
    }

    public List<Integer> getClass2Ids() {
        return class2Ids;
    }

    public void setClass2Ids(List<Integer> class2Ids) {
        this.class2Ids = class2Ids;
    }
}
