package com.qmfresh.promotion.dto;

import java.math.BigDecimal;

/**
 * Created by wyh on 2019/8/22.
 *
 * @author wyh
 */
public class MeetInfo {

    /**
     * 阶梯层级
     */
    private Integer level;

    /**
     * 当前阶梯是否满足
     */
    private Boolean isMeet;

    /**
     * 满足金额
     */
    private BigDecimal meetMoney;

    /**
     * 下一级活动满足金额
     */
    private BigDecimal nextMeetMoney;

    /**
     * 满足下一级活动差异金额
     */
    private BigDecimal nextDiffMoney;

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Boolean getMeet() {
        return isMeet;
    }

    public void setMeet(Boolean meet) {
        isMeet = meet;
    }

    public BigDecimal getMeetMoney() {
        return meetMoney;
    }

    public void setMeetMoney(BigDecimal meetMoney) {
        this.meetMoney = meetMoney;
    }

    public BigDecimal getNextMeetMoney() {
        return nextMeetMoney;
    }

    public void setNextMeetMoney(BigDecimal nextMeetMoney) {
        this.nextMeetMoney = nextMeetMoney;
    }

    public BigDecimal getNextDiffMoney() {
        return nextDiffMoney;
    }

    public void setNextDiffMoney(BigDecimal nextDiffMoney) {
        this.nextDiffMoney = nextDiffMoney;
    }
}
