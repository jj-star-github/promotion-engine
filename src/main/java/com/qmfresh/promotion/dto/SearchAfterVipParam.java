package com.qmfresh.promotion.dto;

import java.util.List;

public class SearchAfterVipParam {
	private Integer shopId;
	private Integer skuId;
	private Integer class2Id;
	private List<SearchAfterDay> days;
	public Integer getShopId() {
		return shopId;
	}
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
	public Integer getSkuId() {
		return skuId;
	}
	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}
	
	public Integer getClass2Id() {
		return class2Id;
	}
	public void setClass2Id(Integer class2Id) {
		this.class2Id = class2Id;
	}
	public List<SearchAfterDay> getDays() {
		return days;
	}
	public void setDays(List<SearchAfterDay> days) {
		this.days = days;
	}
	@Override
	public String toString() {
		return "SearchAfterVipParam [shopId=" + shopId + ", skuId=" + skuId + ", class2Id=" + class2Id + ", days="
				+ days + "]";
	}
	
	
}
