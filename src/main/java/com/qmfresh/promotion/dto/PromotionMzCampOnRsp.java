package com.qmfresh.promotion.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 满赠预占接口
 */
@Data
@ApiModel(value = "PromotionMzCampOnRsp", description = "预占响应")
public class PromotionMzCampOnRsp implements Serializable {

    /**
     * 活动ID
     */
    private Long activityId;

    /**
     * 优惠券ID
     */
    private Long couponId;

    /**
     * 优惠券名称
     */
    private String couponName;

    /**
     * 预占信息
     */
    private List<String> couponCodeList;

    /**
     * 使用说明
     */
    private String useInstruction;

    /**
     * 过期时间
     */
    private String validityPeriod;

}
