package com.qmfresh.promotion.dto;

import java.util.List;

/**
 * @ClassName ModifyPriceParam
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/6 17:46
 */
public class ModifyPriceParam<T> {

    private List<T> reqList;

    public List<T> getReqList() {
        return reqList;
    }

    public void setReqList(List<T> reqList) {
        this.reqList = reqList;
    }
}
