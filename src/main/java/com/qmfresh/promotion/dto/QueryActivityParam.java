package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.strategy.bean.BzRule;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wyh on 2019/6/5.
 *
 * @author wyh
 */
@Data
public class QueryActivityParam extends BaseRequestParam implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    private Long activityId;

}
