package com.qmfresh.promotion.dto.qmpp;

/**
 * @ClassName AuthenParam
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/11 15:25
 */
public class AuthenParam {

    private Integer shopId;

    private Integer class1Id;

    private Integer skuId;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getClass1Id() {
        return class1Id;
    }

    public void setClass1Id(Integer class1Id) {
        this.class1Id = class1Id;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }
}
