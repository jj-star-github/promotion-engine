package com.qmfresh.promotion.dto.qmpp;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName QmppSkuDto
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/7 17:19
 */
public class QmppSkuDto {

    /**
     * 商品id
     */
    private Integer skuId;

    /**
     * 门店id
     */
    private Integer shopId;

    /**
     * 商品售价，3位小数
     */
    private BigDecimal salePrice;

    /**
     * 商品原价
     */
    private BigDecimal price;

    /**
     * 建议价
     */
    private BigDecimal advisePrice;

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAdvisePrice() {
        return advisePrice;
    }

    public void setAdvisePrice(BigDecimal advisePrice) {
        this.advisePrice = advisePrice;
    }
}
