package com.qmfresh.promotion.dto.qmpp;

import java.io.Serializable;

public class Class2Result implements Serializable {
    private static final long serialVersionUID = -2783573559226515367L;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getClass1Id() {
        return class1Id;
    }

    public void setClass1Id(Integer class1Id) {
        this.class1Id = class1Id;
    }

    public Integer getIsNew() {
		return isNew;
	}

	public void setIsNew(Integer isNew) {
		this.isNew = isNew;
	}

	private Integer class1Id;

    private Integer id;
    private String name;
    private Integer isNew;
}
