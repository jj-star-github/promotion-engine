package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.strategy.bean.MjRule;
import com.qmfresh.promotion.strategy.bean.MzRule;
import java.io.Serializable;
import java.util.List;

/**
 * Created by wyh on 2019/6/5.
 *
 * @author wyh
 */
public class CreateMjPromotionParam extends BaseRequestParam implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;
    private Long activityId;

    /**
     * 0所有平台,1.B端,2.C端线上,3.C端线下
     */
    private Integer channel;

    /**
     * 活动名称
     */
    private String activityName;

    /**
     * 活动开始时间
     */
    private Integer effectBeginTime;
    /**
     * 活动结束时间
     */
    private Integer effectEndTime;

    /**
     * 满赠规则
     */
    private MjRule mjRule;

    /**
     * 用户类型（1.全部，2.普通会员，10.黄金会员）
     */
    private List<Integer> crowdTypes;

    /**
     * 区域、门店
     */
    private List<PromotionAreaDto> areaDtos;

    /**
     * 促销适用商品
     */
    private PromotionProductDto promotionProductDto;

    private Long userId;

    private String userName;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Integer getEffectBeginTime() {
        return effectBeginTime;
    }

    public void setEffectBeginTime(Integer effectBeginTime) {
        this.effectBeginTime = effectBeginTime;
    }

    public Integer getEffectEndTime() {
        return effectEndTime;
    }

    public void setEffectEndTime(Integer effectEndTime) {
        this.effectEndTime = effectEndTime;
    }

    public MjRule getMjRule() {
        return mjRule;
    }

    public void setMjRule(MjRule mjRule) {
        this.mjRule = mjRule;
    }

    public List<Integer> getCrowdTypes() {
        return crowdTypes;
    }

    public void setCrowdTypes(List<Integer> crowdTypes) {
        this.crowdTypes = crowdTypes;
    }

    public List<PromotionAreaDto> getAreaDtos() {
        return areaDtos;
    }

    public void setAreaDtos(List<PromotionAreaDto> areaDtos) {
        this.areaDtos = areaDtos;
    }

    public PromotionProductDto getPromotionProductDto() {
        return promotionProductDto;
    }

    public void setPromotionProductDto(PromotionProductDto promotionProductDto) {
        this.promotionProductDto = promotionProductDto;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
