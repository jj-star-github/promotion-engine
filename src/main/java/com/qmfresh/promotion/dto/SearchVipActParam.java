package com.qmfresh.promotion.dto;

import java.util.List;

public class SearchVipActParam {
	private Integer shopId;
	private List<SearchVipSkuParam> skus;
	public Integer getShopId() {
		return shopId;
	}
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
	public List<SearchVipSkuParam> getSkus() {
		return skus;
	}
	public void setSkus(List<SearchVipSkuParam> skus) {
		this.skus = skus;
	}
	@Override
	public String toString() {
		return "SearchVipActParam [shopId=" + shopId + ", skus=" + skus + "]";
	}
	
}
