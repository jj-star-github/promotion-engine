package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.dto.base.PageQuery;

import java.math.BigDecimal;
import java.util.List;

/**
 * @program: promotion-engine
 * @description: 查询出清预警入参
 * @author: xbb
 * @create: 2019-11-23 14:30
 **/
public class QueryPromotionSellOutWarningDetailParam extends PageQuery {

    /**
     * 城市ID
     */
    private Integer cityId;

    /**
     * 门店ID
     */
    private Integer shopId;

    /**
     * SKU ID
     */
    private Integer skuId;

    /**
     * 去化率阈值
     */
    private BigDecimal removalRateThreshold;

    /**
     * 预警 具体时间
     */
    private Integer warningTime;
    /**
     * 预警 日期
     */
    private Integer warningDate;
    /**
     * 预警 级别
     */
    private Integer warningLevel;
    /**
     * 预警 方式 0、广播播报 1、电视播报
     */
    private Integer warningWay;

    /**
     * 创建时间
     */
    private Integer cT;
    /**
     * 更新时间
     */
    private Integer uT;
    /**
     * 是否删除
     */
    private Integer isDeleted;

    /**
     * 起始时间
     */
    private Integer startTime;

    /**
     * 截止时间
     */
    private Integer endTime;

    private List<Integer> shopTypes;

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getRemovalRateThreshold() {
        return removalRateThreshold;
    }

    public void setRemovalRateThreshold(BigDecimal removalRateThreshold) {
        this.removalRateThreshold = removalRateThreshold;
    }

    public Integer getWarningTime() {
        return warningTime;
    }

    public void setWarningTime(Integer warningTime) {
        this.warningTime = warningTime;
    }

    public Integer getWarningDate() {
        return warningDate;
    }

    public void setWarningDate(Integer warningDate) {
        this.warningDate = warningDate;
    }

    public Integer getWarningLevel() {
        return warningLevel;
    }

    public void setWarningLevel(Integer warningLevel) {
        this.warningLevel = warningLevel;
    }

    public Integer getWarningWay() {
        return warningWay;
    }

    public void setWarningWay(Integer warningWay) {
        this.warningWay = warningWay;
    }

    public Integer getcT() {
        return cT;
    }

    public void setcT(Integer cT) {
        this.cT = cT;
    }

    public Integer getuT() {
        return uT;
    }

    public void setuT(Integer uT) {
        this.uT = uT;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Integer getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public List<Integer> getShopTypes() {
        return shopTypes;
    }

    public void setShopTypes(List<Integer> shopTypes) {
        this.shopTypes = shopTypes;
    }
}
