package com.qmfresh.promotion.dto;

public class ProblemShopSkuVo {
	private Integer skuId;
	private Integer shopId;
	private String shopName;
	private String skuName;
	
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getSkuName() {
		return skuName;
	}
	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}
	public Integer getSkuId() {
		return skuId;
	}
	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}
	public Integer getShopId() {
		return shopId;
	}
	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}
	@Override
	public String toString() {
		return "{skuId=" + skuId + ", shopId=" + shopId + ", shopName=" + shopName + ", skuName="
				+ skuName + "}";
	}

	
}
