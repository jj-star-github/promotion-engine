package com.qmfresh.promotion.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wyh on 2019/6/5.
 *
 * @author wyh
 */
public class PromotionClassDto implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    private List<Integer> class2Ids;

    private List<Integer> exSsuFormatIds;

    private List<Integer> exSkuIds;

    public List<Integer> getClass2Ids() {
        return class2Ids;
    }

    public void setClass2Ids(List<Integer> class2Ids) {
        this.class2Ids = class2Ids;
    }

    public List<Integer> getExSsuFormatIds() {
        return exSsuFormatIds;
    }

    public void setExSsuFormatIds(List<Integer> exSsuFormatIds) {
        this.exSsuFormatIds = exSsuFormatIds;
    }

    public List<Integer> getExSkuIds() {
        return exSkuIds;
    }

    public void setExSkuIds(List<Integer> exSkuIds) {
        this.exSkuIds = exSkuIds;
    }
}
