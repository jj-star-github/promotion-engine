package com.qmfresh.promotion.dto;

/**
 * Created by wyh on 2019/8/2.
 *
 * @author wyh
 */
public class ActivityModule {

    /**
     * 活动id
     */
    private Long activityId;

    /**
     * 促销模板id
     */
    private Integer moduleId;

    /**
     * 优先级
     */
    private Integer priority;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
