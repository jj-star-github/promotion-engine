package com.qmfresh.promotion.dto;

public class DataAndWeek {
	private  String week;
	private Integer startTime;
	private Integer endTime;
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public Integer getStartTime() {
		return startTime;
	}
	public void setStartTime(Integer startTime) {
		this.startTime = startTime;
	}
	public Integer getEndTime() {
		return endTime;
	}
	public void setEndTime(Integer endTime) {
		this.endTime = endTime;
	}
	@Override
	public String toString() {
		return "DataAndWeek [week=" + week + ", startTime=" + startTime + ", endTime=" + endTime + "]";
	}

	
}
