package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.bean.promotion.PromotionSellOutShopWarningBean;
import com.qmfresh.promotion.bean.promotion.PromotionSellOutSkuWarningBean;
import com.qmfresh.promotion.dto.base.PageQuery;

import java.io.Serializable;
import java.util.List;

/**
 * 出清预警请求实体类
 */
public class PromotionSellOutWarningReq extends PageQuery implements Serializable{

    private Integer userId;

    private String userName;

    private Integer cityId;

    private Integer shopId;

    private Integer skuId;

    private List<Integer> shopTypes;

    private List<PromotionSellOutSkuWarningBean> skuWarning;

    private List<PromotionSellOutShopWarningBean> shopWarningSetting;

    private List<Integer> skuIds;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public List<PromotionSellOutSkuWarningBean> getSkuWarning() {
        return skuWarning;
    }

    public void setSkuWarning(List<PromotionSellOutSkuWarningBean> skuWarning) {
        this.skuWarning = skuWarning;
    }

    public List<PromotionSellOutShopWarningBean> getShopWarningSetting() {
        return shopWarningSetting;
    }

    public void setShopWarningSetting(List<PromotionSellOutShopWarningBean> shopWarningSetting) {
        this.shopWarningSetting = shopWarningSetting;
    }

    public List<Integer> getShopTypes() {
        return shopTypes;
    }

    public void setShopTypes(List<Integer> shopTypes) {
        this.shopTypes = shopTypes;
    }

    public List<Integer> getSkuIds() {
        return skuIds;
    }

    public void setSkuIds(List<Integer> skuIds) {
        this.skuIds = skuIds;
    }
}
