package com.qmfresh.promotion.dto;

import java.math.BigDecimal;

/**
 * @ClassName PriceChangeParam
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/4 15:21
 */
public class PriceChangeParam {

    private Integer shopId;

    private Integer skuId;

    private BigDecimal price;

    private Integer class2Id;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getClass2Id() {
        return class2Id;
    }

    public void setClass2Id(Integer class2Id) {
        this.class2Id = class2Id;
    }
}
