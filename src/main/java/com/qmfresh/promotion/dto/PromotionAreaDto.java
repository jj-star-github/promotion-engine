package com.qmfresh.promotion.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wyh on 2019/6/5.
 *
 * @author wyh
 */
public class PromotionAreaDto implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    private Integer cityId;

    private List<Integer> shopIds;

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public List<Integer> getShopIds() {
        return shopIds;
    }

    public void setShopIds(List<Integer> shopIds) {
        this.shopIds = shopIds;
    }
}
