package com.qmfresh.promotion.dto;

public class VipSkuVo {
	private Integer skuId;
	private Integer class2Id;
	private Integer isVip;
	public Integer getSkuId() {
		return skuId;
	}
	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}
	
	public Integer getClass2Id() {
		return class2Id;
	}
	public void setClass2Id(Integer class2Id) {
		this.class2Id = class2Id;
	}
	public Integer getIsVip() {
		return isVip;
	}
	public void setIsVip(Integer isVip) {
		this.isVip = isVip;
	}
	
}
