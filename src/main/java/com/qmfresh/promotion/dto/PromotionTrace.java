package com.qmfresh.promotion.dto;

/**
 * Created by wyh on 2019/5/25.
 *
 * @author wyh
 */
public class PromotionTrace {

    /**
     * 促销顺序
     */
    private Integer sequence;

    /**
     * 促销模板id
     */
    private Integer promotionModuleId;

    /**
     * 促销模板名称
     */
    private String promotionModuleName;

    /**
     * 促销活动id
     */
    private Long promotionActivityId;

    /**
     * 促销活动名称
     */
    private String promotionActivityName;

    /**
     * 促销规则id
     */
    private Long promotionRuleId;

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Integer getPromotionModuleId() {
        return promotionModuleId;
    }

    public void setPromotionModuleId(Integer promotionModuleId) {
        this.promotionModuleId = promotionModuleId;
    }

    public String getPromotionModuleName() {
        return promotionModuleName;
    }

    public void setPromotionModuleName(String promotionModuleName) {
        this.promotionModuleName = promotionModuleName;
    }

    public Long getPromotionActivityId() {
        return promotionActivityId;
    }

    public void setPromotionActivityId(Long promotionActivityId) {
        this.promotionActivityId = promotionActivityId;
    }

    public String getPromotionActivityName() {
        return promotionActivityName;
    }

    public void setPromotionActivityName(String promotionActivityName) {
        this.promotionActivityName = promotionActivityName;
    }

    public Long getPromotionRuleId() {
        return promotionRuleId;
    }

    public void setPromotionRuleId(Long promotionRuleId) {
        this.promotionRuleId = promotionRuleId;
    }

}
