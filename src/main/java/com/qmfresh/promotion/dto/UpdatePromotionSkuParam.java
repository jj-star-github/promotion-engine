package com.qmfresh.promotion.dto;

/**
 * @program: promotion-engine
 * @description:
 * @author: xbb
 * @create: 2019-12-03 15:44
 **/
public class UpdatePromotionSkuParam {

    /**
     * 商品促销记录id
     */
    private Long id;
    /**
     * 商品id
     */
    private Integer skuId;
    /**
     * 商品名
     */
    private String skuName;

    /**
     * 活动名称
     */
    private String activityName;
    /**
     * 活动内容
     */
    private String activityContent;

    /**
     * 活动开始时间
     */
    private Integer activityStartTime;
    /**
     * 活动结束时间
     */
    private Integer activityEndTime;
    /**
     * 活动状态(0:有效，1:失效)
     */
    private Integer activityStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityContent() {
        return activityContent;
    }

    public void setActivityContent(String activityContent) {
        this.activityContent = activityContent;
    }

    public Integer getActivityStartTime() {
        return activityStartTime;
    }

    public void setActivityStartTime(Integer activityStartTime) {
        this.activityStartTime = activityStartTime;
    }

    public Integer getActivityEndTime() {
        return activityEndTime;
    }

    public void setActivityEndTime(Integer activityEndTime) {
        this.activityEndTime = activityEndTime;
    }

    public Integer getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(Integer activityStatus) {
        this.activityStatus = activityStatus;
    }
}
