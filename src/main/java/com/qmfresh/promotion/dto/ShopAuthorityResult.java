package com.qmfresh.promotion.dto;

/**
 * @ClassName ShopAuthorityResult
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/6 14:33
 */
public class ShopAuthorityResult {

    private Integer shopId;

    private Integer class1Id;

    private Integer skuId;

    /**
     * 1:门店有权限，2：门店无权限
     */
    private Integer changePriceAuthority;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getClass1Id() {
        return class1Id;
    }

    public void setClass1Id(Integer class1Id) {
        this.class1Id = class1Id;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public Integer getChangePriceAuthority() {
        return changePriceAuthority;
    }

    public void setChangePriceAuthority(Integer changePriceAuthority) {
        this.changePriceAuthority = changePriceAuthority;
    }
}
