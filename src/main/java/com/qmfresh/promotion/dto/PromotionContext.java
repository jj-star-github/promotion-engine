package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.CouponCheckResultDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by wyh on 2019/5/25.
 *
 * @author wyh
 */
@ApiModel(value = "PromotionContext", description = "活动执行响应")
public class PromotionContext implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    private List<BzContext> bzContexts;

    private List<MzContext> mzContexts;

    private List<MjContext> mjContexts;

    private List<PromotionSsuContext> ssuContexts;

    /**
     * 优惠券优惠金额
     */
    @ApiModelProperty("优惠券优惠金额")
    private BigDecimal orderCouponMoney = BigDecimal.ZERO;
    /**
     * 番茄币优惠金额
     */
    @ApiModelProperty("番茄币优惠金额")
    private BigDecimal orderCoinToMoney = BigDecimal.ZERO;
    /**
     * 抹零优惠金额
     */
    @ApiModelProperty("抹零优惠金额")
    private BigDecimal orderFreeMoney = BigDecimal.ZERO;

    /**
     * 优惠券的信息
     */
    @ApiModelProperty("优惠券信息")
    private CouponCheckResultDTO  couponCheckInfo;

    public List<BzContext> getBzContexts() {
        return bzContexts;
    }

    public void setBzContexts(List<BzContext> bzContexts) {
        this.bzContexts = bzContexts;
    }

    public List<MzContext> getMzContexts() {
        return mzContexts;
    }

    public void setMzContexts(List<MzContext> mzContexts) {
        this.mzContexts = mzContexts;
    }

    public List<MjContext> getMjContexts() {
        return mjContexts;
    }

    public void setMjContexts(List<MjContext> mjContexts) {
        this.mjContexts = mjContexts;
    }

    public List<PromotionSsuContext> getSsuContexts() {
        return ssuContexts;
    }

    public void setSsuContexts(List<PromotionSsuContext> ssuContexts) {
        this.ssuContexts = ssuContexts;
    }

    public BigDecimal getOrderCouponMoney() {
        return orderCouponMoney;
    }

    public void setOrderCouponMoney(BigDecimal orderCouponMoney) {
        this.orderCouponMoney = orderCouponMoney;
    }

    public BigDecimal getOrderCoinToMoney() {
        return orderCoinToMoney;
    }

    public void setOrderCoinToMoney(BigDecimal orderCoinToMoney) {
        this.orderCoinToMoney = orderCoinToMoney;
    }

    public BigDecimal getOrderFreeMoney() {
        return orderFreeMoney;
    }

    public void setOrderFreeMoney(BigDecimal orderFreeMoney) {
        this.orderFreeMoney = orderFreeMoney;
    }

    public CouponCheckResultDTO getCouponCheckInfo() {
        return couponCheckInfo;
    }

    public void setCouponCheckInfo(CouponCheckResultDTO couponCheckInfo) {
        this.couponCheckInfo = couponCheckInfo;
    }
}
