package com.qmfresh.promotion.dto;

import java.util.List;

/**
 * 门店相关信息请求实体类
 */
public class SkuStockInShopQuery {
    //虚拟仓ID
    private Long warehouseId;
    //SKU 集
    private List<Long> skuIds;

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public List<Long> getSkuIds() {
        return skuIds;
    }

    public void setSkuIds(List<Long> skuIds) {
        this.skuIds = skuIds;
    }
}
