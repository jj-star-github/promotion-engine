package com.qmfresh.promotion.dto;

public class SearchAfterDay {
	private Integer startTime;
	private Integer endTime;
	public Integer getStartTime() {
		return startTime;
	}
	public void setStartTime(Integer startTime) {
		this.startTime = startTime;
	}
	public Integer getEndTime() {
		return endTime;
	}
	public void setEndTime(Integer endTime) {
		this.endTime = endTime;
	}
	@Override
	public String toString() {
		return "SearchAfterDay [startTime=" + startTime + ", endTime=" + endTime + "]";
	}
	
}
