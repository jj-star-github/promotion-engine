package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.bean.promotion.MzCampOn;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 满赠预占接口
 */
@Data
public class PromotionMzCampOnReq implements Serializable {

    /**
     * 订单ID
     */
    private String orderCode;

    /**
     * 门店ID
     */
    private Integer shopId;

    /**
     * 预占信息
     */
    private List<MzCampOn> campOnList;

    /**
     * 用户ID
     */
    private Integer userId;
}
