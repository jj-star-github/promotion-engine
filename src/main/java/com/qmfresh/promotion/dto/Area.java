package com.qmfresh.promotion.dto;

/**
 * Created by wyh on 2019/5/25.
 *
 * @author wyh
 */
public class Area {

    private Integer shopId;

    private Integer cityId;

    /**
     * 区域范围：1.全国，2.城市，3.门店
     */
    private Integer areaScope;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getAreaScope() {
        return areaScope;
    }

    public void setAreaScope(Integer areaScope) {
        this.areaScope = areaScope;
    }
}
