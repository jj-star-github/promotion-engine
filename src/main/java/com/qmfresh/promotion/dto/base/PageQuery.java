package com.qmfresh.promotion.dto.base;

import java.io.Serializable;

public class PageQuery implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;


    private  int start;

    private  int totalCount;

    private int end;

    private  int pageSize;
    private  int pageIndex;

    public int getTotalCount() {
        return totalCount;
    }

    public int getEnd() {
        return pageSize;
    }


    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getStart() {

        return (pageIndex-1)*pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public void setStart(int start) {
        this.start = start;
    }
    public PageQuery(){
        this.start  = 0;
        this.end = 20;
        this.pageIndex = 1;
        this.pageSize = 20;
    }
}
