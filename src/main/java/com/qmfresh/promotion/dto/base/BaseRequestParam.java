package com.qmfresh.promotion.dto.base;

/**
 * @version v1.0
 * @ProjectName: promotion-engine
 * @ClassName: BaseRequestParam
 * @Description: 用于优化代码，此类不加任何字段
 * @Author: xbb
 * @Date: 2020/3/8 10:19
 */
public class BaseRequestParam {

    private String strategyName;

    public String getStrategyName() {
        return strategyName;
    }

    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }
}
