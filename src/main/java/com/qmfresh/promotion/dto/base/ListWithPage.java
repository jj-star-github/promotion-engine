package com.qmfresh.promotion.dto.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 分页数据展示
 */
public class ListWithPage<T> implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    private List<T> listData;
    private int totalCount;

    public ListWithPage() {
        this.totalCount = 0;
        this.listData = new ArrayList<>(0);
    }

    public List<T> getListData() {
        return listData;
    }

    public void setListData(List<T> listData) {
        this.listData = listData;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

}
