package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.entity.ActivityAreaScope;
import com.qmfresh.promotion.entity.ActivityClass2;
import com.qmfresh.promotion.entity.ActivitySsu;
import com.qmfresh.promotion.entity.ActivityUserScope;
import com.qmfresh.promotion.entity.AreaScopeCity;
import com.qmfresh.promotion.entity.AreaScopeShop;
import com.qmfresh.promotion.entity.PresentActivity;
import com.qmfresh.promotion.entity.PresentSsu;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionActivityTime;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.entity.UserScopeGroup;
import java.io.Serializable;
import java.util.List;

/**
 * Created by wyh on 2019/6/11.
 *
 * @author wyh
 */
public class PromotionDto implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    private PromotionActivity promotionActivity;

    private List<PromotionActivityTime> promotionActivityTimes;

    private PromotionRule promotionRule;

    private ActivityAreaScope activityAreaScope;

    private List<AreaScopeCity> areaScopeCities;

    private List<AreaScopeShop> areaScopeShops;

    private List<ActivityClass2> activityClass2s;

    private List<ActivitySsu> activitySsus;

    private ActivityUserScope activityUserScope;

    private List<UserScopeGroup> userScopeGroups;

    private List<PresentActivity> presentActivities;

    private List<PresentSsu> presentSsus;

    public PromotionActivity getPromotionActivity() {
        return promotionActivity;
    }

    public void setPromotionActivity(PromotionActivity promotionActivity) {
        this.promotionActivity = promotionActivity;
    }

    public List<PromotionActivityTime> getPromotionActivityTimes() {
        return promotionActivityTimes;
    }

    public void setPromotionActivityTimes(
        List<PromotionActivityTime> promotionActivityTimes) {
        this.promotionActivityTimes = promotionActivityTimes;
    }

    public PromotionRule getPromotionRule() {
        return promotionRule;
    }

    public void setPromotionRule(PromotionRule promotionRule) {
        this.promotionRule = promotionRule;
    }

    public ActivityAreaScope getActivityAreaScope() {
        return activityAreaScope;
    }

    public void setActivityAreaScope(ActivityAreaScope activityAreaScope) {
        this.activityAreaScope = activityAreaScope;
    }

    public List<AreaScopeCity> getAreaScopeCities() {
        return areaScopeCities;
    }

    public void setAreaScopeCities(List<AreaScopeCity> areaScopeCities) {
        this.areaScopeCities = areaScopeCities;
    }

    public List<AreaScopeShop> getAreaScopeShops() {
        return areaScopeShops;
    }

    public void setAreaScopeShops(List<AreaScopeShop> areaScopeShops) {
        this.areaScopeShops = areaScopeShops;
    }

    public List<ActivityClass2> getActivityClass2s() {
        return activityClass2s;
    }

    public void setActivityClass2s(List<ActivityClass2> activityClass2s) {
        this.activityClass2s = activityClass2s;
    }

    public List<ActivitySsu> getActivitySsus() {
        return activitySsus;
    }

    public void setActivitySsus(List<ActivitySsu> activitySsus) {
        this.activitySsus = activitySsus;
    }

    public ActivityUserScope getActivityUserScope() {
        return activityUserScope;
    }

    public void setActivityUserScope(ActivityUserScope activityUserScope) {
        this.activityUserScope = activityUserScope;
    }

    public List<UserScopeGroup> getUserScopeGroups() {
        return userScopeGroups;
    }

    public void setUserScopeGroups(List<UserScopeGroup> userScopeGroups) {
        this.userScopeGroups = userScopeGroups;
    }

    public List<PresentActivity> getPresentActivities() {
        return presentActivities;
    }

    public void setPresentActivities(List<PresentActivity> presentActivities) {
        this.presentActivities = presentActivities;
    }

    public List<PresentSsu> getPresentSsus() {
        return presentSsus;
    }

    public void setPresentSsus(List<PresentSsu> presentSsus) {
        this.presentSsus = presentSsus;
    }
}
