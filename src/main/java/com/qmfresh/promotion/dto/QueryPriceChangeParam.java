package com.qmfresh.promotion.dto;

import java.util.List;

/**
 * @ClassName QueryPriceChangeParam
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/4 11:01
 */
public class QueryPriceChangeParam {

    private List<PriceChangeParam> changePriceParams;

    public List<PriceChangeParam> getChangePriceParams() {
        return changePriceParams;
    }

    public void setChangePriceParams(List<PriceChangeParam> changePriceParams) {
        this.changePriceParams = changePriceParams;
    }


}
