package com.qmfresh.promotion.dto;

import java.math.BigDecimal;

/**
 * @ClassName QueryPriceChangeResult
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/4 11:03
 */
public class QueryPriceChangeResult {

    private Integer shopId;

    private Integer skuId;

    private BigDecimal price;

    private BigDecimal promotionPrice;

    private BigDecimal vipPrice;

    private Integer isVip;

    private Integer class2Id;

    /**
     * 1:总部 2：门店
     */
    private Integer creatorFrom;

    /**
     * 2：总部 3：门店
     * reasonType和creatorFrom的含义相同，
     * reasonType用于新改价平台，creatorFrom用于促销系统
     */
    private Integer reasonType;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public BigDecimal getVipPrice() {
        return vipPrice;
    }

    public void setVipPrice(BigDecimal vipPrice) {
        this.vipPrice = vipPrice;
    }

    public Integer getIsVip() {
        return isVip;
    }

    public void setIsVip(Integer isVip) {
        this.isVip = isVip;
    }

    public Integer getClass2Id() {
        return class2Id;
    }

    public void setClass2Id(Integer class2Id) {
        this.class2Id = class2Id;
    }

    public Integer getCreatorFrom() {
        return creatorFrom;
    }

    public void setCreatorFrom(Integer creatorFrom) {
        this.creatorFrom = creatorFrom;
    }

    public Integer getReasonType() {
        return reasonType;
    }

    public void setReasonType(Integer reasonType) {
        this.reasonType = reasonType;
    }
}
