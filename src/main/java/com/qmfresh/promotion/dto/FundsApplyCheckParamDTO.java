package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel("运营申请营销经费入参")
public class FundsApplyCheckParamDTO extends BaseDTO implements Serializable {
    //店铺编码
/*    @ApiModelProperty("店铺编码")
    private  Long shopId;*/
    @ApiModelProperty("店铺名称")
    private  String shopName;
    //申请人编号
    @ApiModelProperty("申请人编号")
    private  Long applyUserId;
    @ApiModelProperty("申请人名称")
    private String applyUserName;
    //申请金额
    @ApiModelProperty("申请金额")
    private BigDecimal applyAmount;
    //申请理由
    @ApiModelProperty("申请理由")
    private  String applyRemark;
    //申请人编号
    @ApiModelProperty("userId")
    private  Long userId;
    @ApiModelProperty("userName")
    private String userName;
    //1 门店 2总部
    @ApiModelProperty("申请类型")
    private  Integer applyType;

    @ApiModelProperty("申请的门店ID")
    private Integer realShopId;
    @ApiModelProperty("大区ID")
    private Integer areaId;
    @ApiModelProperty("大区名称")
    private  String areaName;
}
