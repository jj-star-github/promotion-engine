package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.bean.sku.ProductSsuFormatDto;
import com.qmfresh.promotion.strategy.bean.CpRule;

import java.util.List;
import java.util.Map;

/**
 * Created by wyh on 2019/6/9.
 *
 * @author wyh
 */
public class PromotionProductDto {

    /**
     * 商品范围：2.按二级分类,4.按商品
     */
    private Integer type;

    private PromotionClassDto promotionClassDto;

    private List<Integer> ssuFormatIds;

    private List<ProductSsuFormatDto> productSsuFormatDtos;

    /**
     * 促销规则
     */
    private List<CpRule> skuRules;

    /**
     * 商品id列表
     */
    private List<Integer> skuIds;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public PromotionClassDto getPromotionClassDto() {
        return promotionClassDto;
    }

    public void setPromotionClassDto(PromotionClassDto promotionClassDto) {
        this.promotionClassDto = promotionClassDto;
    }

    public List<Integer> getSsuFormatIds() {
        return ssuFormatIds;
    }

    public void setSsuFormatIds(List<Integer> ssuFormatIds) {
        this.ssuFormatIds = ssuFormatIds;
    }

    public List<ProductSsuFormatDto> getProductSsuFormatDtos() {
        return productSsuFormatDtos;
    }

    public void setProductSsuFormatDtos(List<ProductSsuFormatDto> productSsuFormatDtos) {
        this.productSsuFormatDtos = productSsuFormatDtos;
    }

    public List<CpRule> getSkuRules() {
        return skuRules;
    }

    public void setSkuRules(List<CpRule> skuRules) {
        this.skuRules = skuRules;
    }

    public List<Integer> getSkuIds() {
        return skuIds;
    }

    public void setSkuIds(List<Integer> skuIds) {
        this.skuIds = skuIds;
    }
}
