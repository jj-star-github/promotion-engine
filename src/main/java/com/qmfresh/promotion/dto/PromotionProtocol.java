package com.qmfresh.promotion.dto;

import com.qmfresh.promotion.bean.promotion.PromotionSsu;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wyh on 2019/5/24.
 *
 * @author wyh
 */
@ApiModel(value = "PromotionProtocol", description = "活动执行请求")
public class PromotionProtocol implements Serializable {

    /**
     * 区域（线下都是已门店为主，就是平台活动，也分摊到门店）
     */
    private Area area;

    /**
     * 用户
     */
    private User user;

    /**
     * 渠道 小程序/门店）
     */
    private Integer channel;

    /**
     * 商品列表
     */
    private List<PromotionSsu> promotionSsuList;

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public List<PromotionSsu> getPromotionSsuList() {
        return promotionSsuList;
    }

    public void setPromotionSsuList(List<PromotionSsu> promotionSsuList) {
        this.promotionSsuList = promotionSsuList;
    }
}
