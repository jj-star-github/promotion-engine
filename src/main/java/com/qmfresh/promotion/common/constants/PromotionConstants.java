package com.qmfresh.promotion.common.constants;

/**
 * @ClassName PromotionConstants
 * @Description TODO
 * @Author xbb
 * @Date 2020/4/7 18:45
 */
public class PromotionConstants {
    /**
     * 未删除
     */
    public static final Integer IS_NOT_DELETED = 0;
    /**
     * 已删除
     */
    public static final Integer IS_DELETED = 1;

}
