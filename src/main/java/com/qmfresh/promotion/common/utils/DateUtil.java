package com.qmfresh.promotion.common.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public final class DateUtil {

    private static SimpleDateFormat dateformater;

    public static final String YMD = "yyyy-MM-dd";
    public static final String YMDHMS = "yyyy-MM-dd HH:mm:ss";
    static SimpleDateFormat sdfShort = new SimpleDateFormat("yyyyMMdd");
    static SimpleDateFormat sdfLong = new SimpleDateFormat("yyyy-MM-dd");
    static SimpleDateFormat sdfLongTime = new SimpleDateFormat("yyyyMMddHHmmss");
    static SimpleDateFormat sdfLongTimePlus = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    static SimpleDateFormat sdfLongTimePlusMill = new SimpleDateFormat("yyyyMMddHHmmssSSSS");
    private static long DAY_IN_MILLISECOND = 0x5265c00L;

    /**
     * 根据时间戳获取时间字符串
     *
     * @param timeStamp 时间戳（int格式）
     * @param format 返回时间格式
     * @return string
     * @throws ParseException
     */
    public static String getDateStringByTimeStamp(Integer timeStamp, String format) {
        SimpleDateFormat f = new SimpleDateFormat(format);
        String d = "";
        if (timeStamp != null) {
            d = f.format(((long) timeStamp) * 1000);
        }
        return d;
    }

    /**
     * 根据时间戳获取时间
     *
     * @param timeStamp 时间戳（int格式）
     * @param format 返回时间格式
     * @return
     * @throws ParseException
     */
    public static Date getDateByTimeStamp(int timeStamp, String format) throws ParseException {
        SimpleDateFormat f = new SimpleDateFormat(format);
        String d = f.format(((long) timeStamp) * 1000);
        Date date = f.parse(d);
        return date;
    }

    /**
     * 根据时间戳获取零点时间戳
     *
     * @param timeStamp
     * @return
     */
    public static int getMorningTimeByTimeStamp(int timeStamp) {
        try {
            Date date = DateUtil.getDateByTimeStamp(timeStamp, DateUtil.YMD);
            return DateUtil.getStartTimeStamp(date);
        } catch (Exception e) {
            throw new RuntimeException("日期转换错误", e);
        }
    }

    /**
     * @return long    返回类型
     */
    public static long getTimeStamp() {
        return System.currentTimeMillis();
    }

    /**
     * @return String    返回类型
     */
    public static String dateToStr(Date date, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return sdf.format(date);
        } catch (Exception e) {

            return null;
        }
    }

    /**
     * @param strDate 型如 "yyyy-M-dd" 的日期字符串
     * @return 转换后的java.util.Date对象；转换失败返回null
     */
    public static Date parseDate(String strDate) {
        Date date = null;
        try {
            date = getDateFormater().parse(strDate);
        } catch (Exception ex) {
            // System.err.println(ex.getMessage());
        }
        return date;
    }


    public static List<String> getBetweenDates(long time_start, long time_end) {
            List<String> list = new ArrayList<String>();
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date c_start = dateFormat.parse(dateFormat.format(new Date(time_start * 1000).getTime()));
            Date c_end = dateFormat.parse(dateFormat.format(new Date(time_end * 1000).getTime()));
            Calendar tempStart = Calendar.getInstance();
            tempStart.setTime(c_start);
            Calendar tempEnd = Calendar.getInstance();
            tempEnd.setTime(c_end);
            tempEnd.add(Calendar.DATE, +1);
            while (tempStart.before(tempEnd)) {
                list.add(dateFormat.format(tempStart.getTime()));
                tempStart.add(Calendar.DAY_OF_YEAR, 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 获取指定日期前的日期
     * @param d
     * @param day
     * @return
     */
    public static Date getDateBefore(Date d,int day){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar now = Calendar.getInstance();
        now.setTime(d);
        now.set(Calendar.DATE, now.get(Calendar.DATE) - day);
        return now.getTime();
    }

    /**
     * @return Date    返回类型
     */
    public static Date parseDate(String strDate, String formate) {
        Date date = null;
        try {
            date = new SimpleDateFormat(formate).parse(strDate);
        } catch (Exception ex) {
            // System.err.println(ex.getMessage());
        }
        return date;
    }

    private static DateFormat getDateFormater() {
        if (dateformater == null)
            dateformater = new SimpleDateFormat("yyyy-M-dd");
        return dateformater;
    }

    /**
     * @return Date    返回类型
     */
    public static Date dateAddDays(Date date, int days) {
        long now = date.getTime() + (long) days * DAY_IN_MILLISECOND;
        return new Date(now);
    }

    /**
     * @return String    返回字符串类型
     */
    public static String getStringOfFirstDayInMonth() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        String temp = sdf.format(date);
        String firstDayInMoth = "";
        firstDayInMoth = temp + "-01";

        return firstDayInMoth;

    }
    /**
     * @return Date    java.util.Date
     */
    public static Date getDateOfFirstDayInMonth() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        String temp = sdf.format(date);
        String firstDayInMoth = "";
        firstDayInMoth = temp + "-01";
        return DateUtil.parseDate(firstDayInMoth);

    }

    /**
     * @return String    字符串
     */
    public static String getPlusTime2(Date date) {

        if (date == null)
            return null;
        try {
            String nowDate = sdfLongTimePlus.format(date);
            return nowDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * @return String    返回类型
     */
    public static String getWeekOfDate(Date dt) {
        String[] weekDays = {"7", "1", "2", "3", "4", "5", "6"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    /**
     * @return Date    返回类型
     */
    public static Date beforeSomeHour(Date date, int hours, int mm) {
        try {
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.HOUR, -hours);
            c.add(Calendar.MINUTE, -mm);
            date = c.getTime();
            return date;
        } catch (Exception e1) {
            e1.printStackTrace();
            return null;
        }
    }

    /**
     * 获取指定格式的UTC时间
     *
     * @param simpleDateFormat 指定格式
     * @return 返回字符串类型的当前UTC时间
     */
    public static String getUTC(String simpleDateFormat) {
        SimpleDateFormat df = new SimpleDateFormat(simpleDateFormat);
        Date date = getUTC();
        return df.format(date);
    }

    /**
     * 获取标准的UTC时间。格式为：yyyy-MM-dd'T'HH:mm:ss'Z'
     *
     * @return 返回字符串类型的标准格式的UTC时间
     */
    public static String getStandardUTC() {
        return getUTC("yyyy-MM-dd'T'HH:mm:ss'Z'");
    }

    /**
     * 获取UTC时间
     *
     * @return 返回当前时间的UTC时间，日期类型
     */
    public static Date getUTC() {
        Calendar cal = Calendar.getInstance();
        //取得时间偏移量
        int zoneOffset = cal.get(Calendar.ZONE_OFFSET);
        //取得夏令时差
        int dstOffset = cal.get(Calendar.DST_OFFSET);
        //从本地时间里扣除这些差量，即可以取得UTC时间
        cal.add(Calendar.MILLISECOND, -(zoneOffset + dstOffset));
        return cal.getTime();
    }

    public static Date gStopTime(Date date, Integer addMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.add(Calendar.MONTH, addMonth);

        return calendar.getTime();
    }

    /**
     * 判断特定时间是否在一个时间范围内
     */
    public static Boolean isInTheTime(Date time, Date starttime, Date endtime) {

        if (time == null || starttime == null || endtime == null) {
            return false;
        }

        if ((time.compareTo(starttime) >= 0) && (time.compareTo(endtime) <= 0)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取两个时间差
     *
     * @param endDate
     * @param nowDate
     * @return
     */
    public static Long getDatePoor(Date endDate, Date nowDate) {

        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
//		if(day==0){
//			return  hour + ":" + min + ":00";
//		}
//		return day + "天" + hour + ":" + min + ":00";
        return diff;
    }

    /*
     *** 获取时长 s- 开始时间，e - 结束时间
     * 返回小时
     **/
    public static Integer getDtDurationH(Date s, Date e) {
        long courseDuration = e.getTime() - s.getTime();
        //long durationD=courseDuration/(24*60*60*1000); //没跨天，所以不用考虑天数因素
        long durationH = courseDuration / (60 * 60 * 1000);
        return (int) durationH;
    }

    /**
     * 获取当期属于星期几
     */
    public static Integer getWeekByDate(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int intWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (intWeek == 0) {
            return 7;
        } else {
            return intWeek;
        }
    }
    public static List<Date> dateSplit(Date startDate, Date endDate) {

        Long spi = endDate.getTime() - startDate.getTime();
        Long step = spi / (24 * 60 * 60 * 1000);// 相隔天数

        List<Date> dateList = new ArrayList<Date>();
        dateList.add(startDate);
        for (int i = 1; i <= step; i++) {
            dateList.add(DateUtil.dateAddDays(startDate, i));
        }
        return dateList;
    }

    /**
     * 2017.7.5日上午 8:00 -10:00
     *
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return 2017.7.5日上午 8:00 -10:00
     */
    public static String formartContent(Date startTime, Date endTime) {
        String content = new SimpleDateFormat("yyyy.MM.dd a").format(startTime);
        content += new SimpleDateFormat(" hh:mm ").format(startTime) + "-" + new SimpleDateFormat(" hh:mm ").format(endTime);
        return content;
    }

    public static Date Now() {
        return new Date();
    }

    public static Date formatYMD(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
        return parseDate(df.format(date));
    }

    /**
     * 某个时间之前的时间
     *
     * @param date
     * @param minutes
     * @return
     */
    public static Date getPreDateForMinutes(Date date, Integer minutes) {
        Calendar c = new GregorianCalendar();
        c.setTime(date);//设置参数时间
        c.add(Calendar.MINUTE, (0 - minutes));
        return c.getTime();
    }

    /**
     * 某个时间之后的时间
     *
     * @param date
     * @param minutes
     * @return
     */
    public static Date getNextDateForMinutes(Date date, Integer minutes) {
        Calendar c = new GregorianCalendar();
        c.setTime(date);//设置参数时间
        c.add(Calendar.MINUTE, (minutes));
        return c.getTime();
    }

    public static Date buildDate(String d, String t) throws ParseException {
        String datetiem = d + " " + t;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.parse(datetiem);
    }

    public static Date buildDate(Long ms) throws Exception {

        Timestamp timestamp = new Timestamp(ms);
        Date date = new Date(timestamp.getTime());
        ;
        return date;

    }

    /**
     * 获取当前时间的时间戳
     *
     * @return int值
     */
    public static int getCurrentTimeIntValue() {
        return (int) (System.currentTimeMillis() / 1000);

    }

    public static int addDaysTimeStamp(int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, days);
        return (int) (cal.getTimeInMillis() / 1000);
    }

    public static int addDaysTimeStamp(int days, Integer baseMs) throws Exception {
        long timeStamp = Long.valueOf(baseMs.toString());
        timeStamp = timeStamp * 1000;
        Date startDate = buildDate(timeStamp);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        cal.add(Calendar.DATE, days);
        return (int) (cal.getTimeInMillis() / 1000);
    }

    public static Calendar getCalByTimestamp(Integer ms) {
        try {
            long timeStamp = Long.valueOf(ms.toString());
            timeStamp = timeStamp * 1000;
            Date startDate = buildDate(timeStamp);
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            return cal;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public static int getStartTimeStamp() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        long time = todayStart.getTimeInMillis() / 1000;
        return (int) time;
    }

    public static int getEndTimeStamp() {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        long time = todayEnd.getTimeInMillis() / 1000;
        return (int) time;
    }

    public static int getStartTimeStamp(Date date) {
        Calendar todayStart = Calendar.getInstance();
        todayStart.setTime(date);
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        long time = todayStart.getTimeInMillis() / 1000;
        return (int) time;
    }

    public static int getEndTimeStamp(Date date) {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.setTime(date);
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        long time = todayEnd.getTimeInMillis() / 1000;
        return (int) time;
    }

    /**
     * 当前时间的第二天某个时间点
     *
     * @param hour
     * @param minutes
     * @return
     */
    public static int buildDaysOfHmBaseToday(int hour, int minutes) {
        Calendar todayStart = Calendar.getInstance();
        todayStart.add(Calendar.DAY_OF_YEAR, 1);
        todayStart.set(Calendar.HOUR_OF_DAY, hour);
        todayStart.set(Calendar.MINUTE, minutes);
        todayStart.set(Calendar.SECOND, 0);
        long time = todayStart.getTimeInMillis() / 1000;
        return (int) time;
    }

    public static int buildDayOfHourM(int hour, int minutes) {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, hour);
        todayStart.set(Calendar.MINUTE, minutes);
        todayStart.set(Calendar.SECOND, 0);
        long time = todayStart.getTimeInMillis() / 1000;
        return (int) time;
    }

    public static int getPreTime(int s) {
        Calendar todayEnd = Calendar.getInstance();

        todayEnd.set(Calendar.SECOND, 0 - s);

        long time = todayEnd.getTimeInMillis() / 1000;
        return (int) time;
    }

    /**
     * 格式化时间
     *
     * @param calendar
     * @param format 格式化yyyy-MM-dd
     * @return
     */
    public static String buildFormat(Calendar calendar, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.CHINA);
        return simpleDateFormat.format(calendar.getTime());
    }

    /**
     * 计算几天前的日期
     *
     * @param day
     * @return
     */
    public static Date getDateBefore(int day) {
        Calendar calendarBegin = Calendar.getInstance();
        calendarBegin.add(Calendar.DATE, -day);
        Date beginDate = calendarBegin.getTime();
        return beginDate;
    }

    public static String getWeekDayStr(int weekDay) {
        if (weekDay == 1){
            return "周一";
        }
        if (weekDay == 2) {
            return "周二";
        }
        if (weekDay == 3) {
            return "周三";
        }
        if (weekDay == 4) {
            return "周四";
        }
        if (weekDay == 5) {
            return "周五";
        }
        if (weekDay == 6) {
            return "周六";
        }
        if (weekDay == 7) {
            return "周日";
        }
        return "";
    }

    /**
     * yyyy-mm-dd
     * @return
     */
    public static String formatDate() {
        DateFormat defaultFormat = new SimpleDateFormat(YMD);
        return defaultFormat.format(new Date());
    }

}
