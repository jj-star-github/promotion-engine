package com.qmfresh.promotion.common.utils;

import com.alibaba.fastjson.JSON;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.entity.ContentType;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UtilMethods {
	private static final Log logger = LogFactory.getLog(UtilMethods.class);

	public static int getNowWithSecond() {
		return new Long(System.currentTimeMillis() / 1000).intValue();
	}

	/**
	 * post请求(参数以流的形式传递）
	 * 
	 * @param urlStr
	 * @param paramString
	 * @return
	 * @throws HttpException
	 * @throws IOException
	 */
	public static String post(String urlStr, String paramString) throws HttpException, IOException {

		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod(urlStr);
		post.addRequestHeader("Connection", "close");
		post.getParams().setCookiePolicy(CookiePolicy.IGNORE_COOKIES);
		post.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(0, false));
		post.addRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);// 过期时间
		client.getHttpConnectionManager().getParams().setSoTimeout(30000);// 过期时间
		RequestEntity requestEntity = new StringRequestEntity(paramString, ContentType.APPLICATION_JSON.getMimeType(),
				"utf-8");
		post.setRequestEntity(requestEntity);
		try {
			int statusCode = client.executeMethod(post);
			if (statusCode != HttpStatus.SC_OK) {
				return null;
			}
			return new String(post.getResponseBody());

		} finally {
			post.releaseConnection();
		}
	}

	/**
	 * post请求(参数以流的形式传递）
	 *
	 * @param url
	 * @param param
	 * @return
	 * @throws HttpException
	 * @throws IOException
	 */
	public static String post(String url, String param, Charset charset, Header... header) {

		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod(url);
		if (header != null)
			for (Header h : header)
				post.addRequestHeader(h);
		post.addRequestHeader("Connection", "close");
		post.getParams().setCookiePolicy(CookiePolicy.IGNORE_COOKIES);
		post.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(0, false));
		client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);// 过期时间
		client.getHttpConnectionManager().getParams().setSoTimeout(30000);// 过期时间
		try {
			RequestEntity requestEntity = new StringRequestEntity(param, ContentType.APPLICATION_JSON.getMimeType(),
					charset.name());
			post.setRequestEntity(requestEntity);
			int statusCode = client.executeMethod(post);
			if (statusCode != HttpStatus.SC_OK) {
				return null;
			}
			String response = new String(post.getResponseBody());
			if (logger.isDebugEnabled())
				logger.error("url:" + url + "\nparam:" + param + "\nresponse:" + response);
			return response;
		} catch (IOException e) {
			// 此项目不需要处理超时异常，所以抛出的异常改为RuntimeException
			throw new RuntimeException("url:" + url + ", param:" + param, e);
		} finally {
			post.releaseConnection();
		}
	}

	/**
	 * post请求(参数以流的形式传递）
	 *
	 * @param urlStr
	 * @param paramString
	 * @return
	 * @throws HttpException
	 * @throws IOException
	 */
	public static String postJson(String urlStr, String paramString) {
		Header h = new Header("Content-Type", "application/json");
		return post(urlStr, paramString, Charset.defaultCharset(), h);
	}

	/**
	 * 发送NSQ 消息
	 *
	 * @param url
	 * @param topic
	 * @param params
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static void sendMessage(String url, String topic, Map<String, String> params) {
		url = url + "?" + "topic" + "=" + topic;
		Map<String, Object> messageBody = new HashMap<>();
		messageBody.put("message", params);
		messageBody.put("send_time", UtilMethods.getNowWithSecond());
		String resultMessage = UtilMethods.postJson(url, JSON.toJSONString(messageBody));
		if (StringUtils.isBlank(resultMessage)) {
			logger.warn("send message failed {" + params + "}");
		}
		if (!"OK".equalsIgnoreCase(resultMessage)) {
			logger.warn("send message failed {" + params + "}");
		}
	}

	/**
	 * 连接url的两部分
	 * 
	 * @param urlSegmen1
	 * @param urlSegmen2
	 * @return
	 */
	public static String concatUrl(String urlSegmen1, String urlSegmen2) {
		String url = null;
		if (urlSegmen1 != null && urlSegmen2 != null) {
			if (urlSegmen1.endsWith("/"))
				url = urlSegmen1 + urlSegmen2;
			else
				url = urlSegmen1 + "/" + urlSegmen2;
		}
		return url;
	}

	public static <T> List<List<T>> getSubList(List<T> list, int len) {
		if (list == null || list.size() == 0 || len < 1) {
			return null;
		}
		List<List<T>> resultList = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			if (i % len == 0) {
				int count = i / len;
				List subList = (List) list.stream().limit((count + 1) * len).skip(count * len)
						.collect(Collectors.toList());
				resultList.add(subList);
			}
		}
		return resultList;
	}
}
