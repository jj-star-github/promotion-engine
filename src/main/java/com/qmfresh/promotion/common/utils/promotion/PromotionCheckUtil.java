package com.qmfresh.promotion.common.utils.promotion;

import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.PromotionAreaDto;
import com.qmfresh.promotion.dto.PromotionProductDto;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionActivityTime;
import com.qmfresh.promotion.enums.CrowdTypeEnums;
import com.qmfresh.promotion.enums.ProductScopeTypeEnums;
import com.qmfresh.promotion.enums.PromotionTypeEnums;
import com.qmfresh.promotion.manager.IPromotionManager;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.strategy.bean.BaseRule;
import com.qmfresh.promotion.strategy.bean.CpRule;
import com.qmfresh.promotion.strategy.helper.MutexCheckHelper;
import com.qmfresh.promotion.thread.factory.NamedThreadFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @ClassName PromotionCheckUtil
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/19 9:13
 */
@Component
@Slf4j
public class PromotionCheckUtil {

    @Resource
    private IPromotionManager promotionManager;


    @Resource
    private MutexCheckHelper mutexCheckHelper;



    private static ThreadPoolExecutor pool = new ThreadPoolExecutor(5,5,0
            , TimeUnit.SECONDS,new LinkedBlockingDeque<>(100),new NamedThreadFactory("query_change_price"));

    private static final Pattern pattern = Pattern.compile("^[0-9]+(.[0-9]{1,3})?$");

    public void checkVipTimeConflict(List<PromotionActivityTime> times) {
        for(PromotionActivityTime promotionActivityTime : times){
            if(promotionActivityTime.getEffectBeginTime() == null || promotionActivityTime.getEffectEndTime() == null){
                throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("活动开始时间和结束时间不能为空");
            }
            if(promotionActivityTime.getEffectEndTime() < promotionActivityTime.getEffectBeginTime()){
                throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("活动开始时间不能大于结束时间");
            }
            if(promotionActivityTime.getEffectEndTime() < DateUtil.getStartTimeStamp()){
                throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("活动结束时间不能小于当天开始时间");
            }
        }
        PromotionActivityTime pat = times.get(0);
        for (int i = 1; i < times.size(); i++) {
            PromotionActivityTime comparePat = times.get(i);
            if (pat.getEffectBeginTime() > comparePat.getEffectEndTime() || pat.getEffectEndTime() < comparePat.getEffectBeginTime()) {
                continue;
            }
            List<String> weekDays = ListUtil.stringToList(pat.getWeekDay());
            List<String> compareWeekDays = ListUtil.stringToList(comparePat.getWeekDay());
            compareWeekDays.forEach(
                    item -> {
                        if (weekDays.contains(item)) {
                            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("存在重复时间");
                        }
                    }
            );
        }
    }

    // todo 开启定时任务

    public void checkMutex(Integer channel, PromotionTypeEnums promotionType, PromotionTypeEnums createType, int beginTime, int endTime,
                            List<Integer> crowdTypes,
                            List<PromotionAreaDto> areaDtos, PromotionProductDto promotionProductDto, BaseRule baseRule) {
        PromotionActivityTime promotionActivityTime = new PromotionActivityTime();
        promotionActivityTime.setEffectBeginTime(beginTime);
        promotionActivityTime.setEffectEndTime(endTime);
        this.checkMutex(channel, promotionType, createType, Collections.singletonList(promotionActivityTime), crowdTypes, areaDtos, promotionProductDto, baseRule);
    }

    public void checkMutex(Integer channel, PromotionTypeEnums promotionType, PromotionTypeEnums createType,
                            List<PromotionActivityTime> times,
                            List<Integer> crowdTypes, List<PromotionAreaDto> areaDtos, PromotionProductDto promotionProductDto,
                            BaseRule baseRule) {
        List<Long> activityIds = this.getOverlapActivity(channel, promotionType, times, crowdTypes, areaDtos, promotionProductDto);
        if (ListUtil.isNullOrEmpty(activityIds)) {
            return;
        }
        mutexCheckHelper.mutexCheck(activityIds, createType, promotionProductDto, baseRule);
    }

    /**
     * 校验改价活动是否冲突（区分创建活动来源）
     * @param channel
     * @param promotionType
     * @param times
     * @param crowdTypes
     * @param areaDtos
     * @param promotionProductDto
     * @param creatorFrom
     */
    public void checkCp(Integer channel, PromotionTypeEnums promotionType, List<PromotionActivityTime> times, List<Integer> crowdTypes,
                         List<PromotionAreaDto> areaDtos, PromotionProductDto promotionProductDto,Integer creatorFrom){

        List<Long> activityIds = this.getOverlapActivity(channel, promotionType, times, crowdTypes, areaDtos, promotionProductDto,creatorFrom);
        if (ListUtil.isNullOrEmpty(activityIds)) {
            return;
        }

        // 校验分类
        if (promotionProductDto.getType().equals(ProductScopeTypeEnums.CLASS2.getCode())) {
            promotionManager.checkProductClassDto(activityIds, promotionProductDto.getPromotionClassDto());
            return;
        }

        // 校验商品
        if (promotionProductDto.getType().equals(ProductScopeTypeEnums.SSU.getCode())) {
            promotionManager.checkProduct(activityIds, promotionProductDto.getProductSsuFormatDtos());
        }
    }

    /**
     * @param promotionType 促销类型
     * @param beginTime 开始时间
     * @param endTime 结束时间
     * @param crowdTypes 人群
     * @param areaDtos 区域
     * @param promotionProductDto 商品
     * @return
     */
    public void check(Integer channel, PromotionTypeEnums promotionType, int beginTime, int endTime, List<Integer> crowdTypes,
                       List<PromotionAreaDto> areaDtos, PromotionProductDto promotionProductDto) {
        PromotionActivityTime promotionActivityTime = new PromotionActivityTime();
        promotionActivityTime.setEffectBeginTime(beginTime);
        promotionActivityTime.setEffectEndTime(endTime);
        this.check(channel, promotionType, Collections.singletonList(promotionActivityTime), crowdTypes, areaDtos, promotionProductDto);
    }

    /**
     * @param channel 渠道
     * @param promotionType 促销类型
     * @param times 时间段
     * @param crowdTypes 人群
     * @param areaDtos 区域
     * @param promotionProductDto 商品
     */
    //创建vip校验
    public void check(Integer channel, PromotionTypeEnums promotionType, List<PromotionActivityTime> times, List<Integer> crowdTypes,
                       List<PromotionAreaDto> areaDtos, PromotionProductDto promotionProductDto) {
        List<Long> activityIds = this.getOverlapActivity(channel, promotionType, times, crowdTypes, areaDtos, promotionProductDto);
        if (ListUtil.isNullOrEmpty(activityIds)) {
            return;
        }
       
        // 校验分类
        if (promotionProductDto.getType().equals(ProductScopeTypeEnums.CLASS2.getCode())) {
            promotionManager.checkProductClassDto(activityIds, promotionProductDto.getPromotionClassDto());
            return;
        }

        // 校验商品
        if (promotionProductDto.getType().equals(ProductScopeTypeEnums.SSU.getCode())) {
            promotionManager.checkProduct(activityIds, promotionProductDto.getProductSsuFormatDtos());
        }
    }

    public List<Long> getOverlapActivity(Integer channel, PromotionTypeEnums promotionType, List<PromotionActivityTime> times,
                                          List<Integer> crowdTypes,
                                          List<PromotionAreaDto> areaDtos, PromotionProductDto promotionProductDto) {
        List<PromotionActivity> promotionActivities = promotionManager.checkActivityTime(channel, promotionType, times);

        if (ListUtil.isNullOrEmpty(promotionActivities)) {
            return null;
        }
        // 校验人群
        promotionActivities = promotionManager.checkActivityUser(promotionActivities, crowdTypes);
        if (ListUtil.isNullOrEmpty(promotionActivities)) {
            return null;
        }

        // 校验区域
        List<Long> activityIds = promotionManager.checkActivityShop(promotionActivities, areaDtos);
        if (ListUtil.isNullOrEmpty(activityIds)) {
            return null;
        }

        ProductScopeTypeEnums productScopeType = ProductScopeTypeEnums.getEnum(promotionProductDto.getType());
        if (productScopeType == null) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("商品范围异常");
        }
        return activityIds;
    }

    /**
     * 校验重复活动（区分创建来源：运营或门店）
     * @param channel
     * @param promotionType
     * @param times
     * @param crowdTypes
     * @param areaDtos
     * @param promotionProductDto
     * @param creatorFrom
     * @return
     */
    public List<Long> getOverlapActivity(Integer channel, PromotionTypeEnums promotionType, List<PromotionActivityTime> times,
                                          List<Integer> crowdTypes,
                                          List<PromotionAreaDto> areaDtos, PromotionProductDto promotionProductDto,Integer creatorFrom){
        List<PromotionActivity> promotionActivities = promotionManager.checkActivityTime(channel, promotionType, times,creatorFrom);
        if (ListUtil.isNullOrEmpty(promotionActivities)) {
            return null;
        }
        // 校验人群
        promotionActivities = promotionManager.checkActivityUser(promotionActivities, crowdTypes);
        if (ListUtil.isNullOrEmpty(promotionActivities)) {
            return null;
        }

        // 校验区域
        List<Long> activityIds = promotionManager.checkActivityShop(promotionActivities, areaDtos);
        if (ListUtil.isNullOrEmpty(activityIds)) {
            return null;
        }

        ProductScopeTypeEnums productScopeType = ProductScopeTypeEnums.getEnum(promotionProductDto.getType());
        if (productScopeType == null) {
            throw new BusinessException("商品范围异常");
        }
        return activityIds;
    }

    public Boolean validateCpPromotionPrice(List<CpRule> list){

        if(CollectionUtils.isEmpty(list)){
            for(CpRule cpRule : list){
                if(cpRule.getPromotionPrice() != null && !this.judgeDecimal(cpRule.getPromotionPrice().doubleValue())){
                    return false;
                }
            }
        }
        return true;
    }

    //三位小数金额校验
    public boolean judgeDecimal(Object obj){
        boolean flag = false;
        try {
            if (obj != null) {
                String source = obj.toString();
                // 判断是否是整数或者是携带一位到三位的小数
                if (pattern.matcher(source).matches()) {
                    flag = true;
                }
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return flag;
    }

    public List<Integer> mapCrowdType(List<Integer> crowTypes){
        List<Integer> crowdTypeList = new ArrayList<>();
        if(CollectionUtils.isEmpty(crowTypes)){
            return crowdTypeList;
        }
        if(crowTypes.contains(CrowdTypeEnums.ALL.getCode())){
            crowdTypeList.addAll(Arrays.asList(CrowdTypeEnums.NOT_VIP.getCode(),CrowdTypeEnums.NORMAL_VIP.getCode()));
        }
        crowdTypeList.addAll(crowTypes.stream().distinct().collect(Collectors.toList()));
        return crowdTypeList.stream().distinct().collect(Collectors.toList());
    }
}
