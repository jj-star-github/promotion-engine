package com.qmfresh.promotion.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.common.business.ServiceResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class RequestUtil {
    protected static final Logger log = LoggerFactory.getLogger(RequestUtil.class.getName());

    public static <T> ServiceResult<T> post(String url, Map<String, Object> params, TypeReference<ServiceResult<T>> t) throws Exception {

        return RequestUtil.request(url, params, t, 1);
    }

    public static <T> ServiceResult<T> postList(String url, List<Map<String, Object>> params, TypeReference<ServiceResult<T>> t) throws Exception {

        return RequestUtil.requestList(url, params, t);
    }

    public static <T> ServiceResult<T> get(String url, Map<String, Object> params, TypeReference<ServiceResult<T>> t) throws Exception {

        return RequestUtil.request(url, params, t, 2);
    }

    public static <T> ServiceResult<T> postJson(String url, String params, TypeReference<ServiceResult<T>> t) throws Exception {

        return RequestUtil.requestJson(url, params, t, 1);
    }

    public static <T> ServiceResult<T> getJson(String url, String params, TypeReference<ServiceResult<T>> t) throws Exception {

        return RequestUtil.requestJson(url, params, t, 2);
    }

    private static <T> ServiceResult<T> request(String url, Map<String, Object> params, TypeReference<ServiceResult<T>> t, int requestType) throws Exception {


        try {
            String result;
            if (1 == requestType) {
                String param = JSON.toJSONString(params, true);
                System.out.println(param);
                result = HttpClientUtil.postJson(url, param);
                log.info(url + JSON.toJSONString(params) + result);
            } else {

                url = buildQueryParams(url, params);
                result = HttpUtil.get(url);
            }
//            System.out.println(url);
            log.info("请求地址：{}", url);
//            System.out.println(result);
            ServiceResult<T> resultDomain = JSON.parseObject(result, t);

            return resultDomain;

        } catch (Exception e) {
            String param = JSON.toJSONString(params);
            String message = "url:" + url + ",params:" + param;
            throw new Exception(message, e);
        }
    }

    private static <T> ServiceResult<T> requestList(String url, List<Map<String, Object>> params, TypeReference<ServiceResult<T>> t) throws Exception {


        try {
            String result;
            String param = JSON.toJSONString(params);
            result = HttpClientUtil.postJson(url, param);
            log.info(url + JSON.toJSONString(params) + result);
            ServiceResult<T> resultDomain = JSON.parseObject(result, t);

            return resultDomain;

        } catch (Exception e) {
            String param = JSON.toJSONString(params);
            String message = "url:" + url + ",params:" + param;
            throw new Exception(message, e);
        }
    }

    private static <T> ServiceResult<T> requestJson(String url, String jsonParams, TypeReference<ServiceResult<T>> t, int requestType) throws Exception {
        try {
            String result;
            if (1 == requestType) {
                result = HttpClientUtil.postJson(url, jsonParams);
            } else {
                if (url.indexOf("?") > 0) {
                    url += "&" + jsonParams;
                } else {
                    url += "?" + jsonParams;
                }
                result = HttpUtil.get(url);
            }
            log.info(url + jsonParams + result);
            ServiceResult<T> resultDomain = JSON.parseObject(result, t);

            return resultDomain;

        } catch (Exception e) {

            String message = "url:" + url + ",params:" + jsonParams;
            throw new Exception(message, e);
        }
    }

    private static String buildQueryParams(String url, Map<String, Object> params) {
        String strParams = "";
        for (Map.Entry<String, Object> item : params.entrySet()) {
            strParams += item.getKey() + "=" + item.getValue() + "&";
        }
        if (url.indexOf("?") > 0) {
            url += "&" + strParams;
        } else {
            url += "?" + strParams;
        }
        return url;
    }


}
