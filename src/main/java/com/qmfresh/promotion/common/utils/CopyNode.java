package com.qmfresh.promotion.common.utils;

import java.util.List;

/**
 * @ClassName CopyNode
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/11 9:00
 */
public class CopyNode<T> {

    private List<T> list;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
