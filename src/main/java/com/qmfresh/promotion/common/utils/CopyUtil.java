package com.qmfresh.promotion.common.utils;

import org.springframework.beans.BeanUtils;

import java.util.List;

public class CopyUtil<T,E> {

    public List<E> copy(List<T> sourceList,List<E> targetList){
        CopyNode<T> sourceObj = new CopyNode<>();
        sourceObj.setList(sourceList);
        CopyNode<E> targetObj = new CopyNode<>();
        targetObj.setList(targetList);
        BeanUtils.copyProperties(sourceObj,targetObj);
        return targetObj.getList();
    }
}
