package com.qmfresh.promotion.common.utils;

import com.qmfresh.promotion.bean.promotion.PromotionSellOutSkuWarningBean;
import com.qmfresh.promotion.bean.promotion.PromotionSellOutWarningRuleBean;
import com.qmfresh.promotion.bean.sellout.SkuPrice;
import com.qmfresh.promotion.bean.sellout.SkuSale;
import com.qmfresh.promotion.bean.sellout.SkuStock;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;

/**
 * Created by wyh on 2019/12/14.
 *
 * @author wyh
 */
@Slf4j
public class SellOutUtil {

    /**
     * 实时销量/（实时库存+实时销量）->去化率
     */
    public static BigDecimal getRemovalRate(SkuSale skuSale, SkuStock skuStock) {
        if(getAmount(skuStock).add(getTotal(skuSale)).compareTo(BigDecimal.ZERO) == 0) {
            return BigDecimal.ZERO;
        }
        return getTotal(skuSale).divide(getAmount(skuStock).add(getTotal(skuSale)), 3, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 计算销量
     */
    public static BigDecimal getTotal(SkuSale skuSale){
        return skuSale == null? BigDecimal.ZERO:skuSale.getTotal();
    }
    /**
     * 计算库存
     */
    public static BigDecimal getAmount(SkuStock skuStock) {
        return skuStock == null? BigDecimal.ZERO:skuStock.getIsWeight().equals(1) ? skuStock.getAmountFormat() : skuStock.getAmount();
    }

    /**
     * 计算折后价
     */
    public static BigDecimal getDiscountPrice(SkuPrice skuPrice, PromotionSellOutWarningRuleBean rule) {
        return skuPrice ==null ? BigDecimal.ZERO : skuPrice.getPrice().multiply(rule.getWarningDiscount());
    }

    /**
     * 返回预警值：实时库存*（售价 * 折扣率）
     */
    public static BigDecimal getWarningValue(SkuStock skuStock, SkuPrice skuPrice, PromotionSellOutWarningRuleBean rule) {
        return SellOutUtil.getAmount(skuStock).multiply(SellOutUtil.getDiscountPrice(skuPrice, rule));
    }

    public static String createErrorMsg(PromotionSellOutSkuWarningBean warningBean, SkuSale skuSale, SkuStock skuStock, SkuPrice skuPrice, PromotionSellOutWarningRuleBean rule) {
        String error = "";
        if(skuStock == null) {
            error += ",未获取到实时库存";
        }

        if(skuPrice == null) {
            error += ",未获取到售价";
        }

        BigDecimal removalRate = SellOutUtil.getRemovalRate(skuSale, skuStock);
        if (removalRate.compareTo(rule.getRemovalRateThreshold()) > 0) {
            error += ",去化率(" + removalRate + ")比阈值(" + rule.getRemovalRateThreshold()+")大";
        }
        if(StringUtils.isBlank(error)){
            return  null;
        }
        return createErrorMsg(warningBean,error);
    }

    public static String createErrorMsg(PromotionSellOutSkuWarningBean warningBean, String error) {
        return warningBean.getShopName() +
            "(" + warningBean.getShopId() +
            ") " + warningBean.getSkuName() +
            "(" + warningBean.getSkuId() + ")" +
            error;
    }
}
