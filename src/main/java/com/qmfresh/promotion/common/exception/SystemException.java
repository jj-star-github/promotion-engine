package com.qmfresh.promotion.common.exception;

/**
 * 系统错误类
 *
 * @author wyh
 */
public class SystemException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public SystemException(String msg) {
        super(msg);
    }

    public SystemException(String msg, Throwable t) {
        super(msg, t);
    }
}
