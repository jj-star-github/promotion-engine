package com.qmfresh.promotion.common.exception;

/**
 * 这个业务异常类已经废弃，使用@see所指定的业务异常类
 * 业务异常类
 *
 * @author wyh
 * @see com.qmfresh.promotion.platform.domain.shared.BusinessException
 */
@Deprecated
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = -6003868869041167435L;

    private int errorCode;
    private String errorMsg;
    private Throwable t;

    public BusinessException(int errorCode) {
        this.errorCode = errorCode;
    }

    public BusinessException(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public BusinessException(int errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public BusinessException(int errorCode, Throwable t) {
        this.errorCode = errorCode;
        this.t = t;
    }

    public BusinessException(int errorCode, String errorMsg, Throwable t) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
        this.t = t;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Throwable getT() {
        return t;
    }

    public void setT(Throwable t) {
        this.t = t;
    }

}
