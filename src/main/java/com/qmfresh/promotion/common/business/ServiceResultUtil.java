package com.qmfresh.promotion.common.business;

import com.qmfresh.promotion.enums.ResponseCode;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;

/**
 * 通用返回工具类
 *
 * @author wyh
 */
public class ServiceResultUtil {
    public static <T> ServiceResult<T> resultError(int errorCode, String errorMsg) {
        return MessageResponseUtils.resultError(errorCode, errorMsg);
    }

    public static <T> ServiceResult<T> resultError(ResponseCode code, String errorMsg) {
        return MessageResponseUtils.resultError(code.getCode(), errorMsg);
    }

    public static <T> ServiceResult<T> resultError(ResponseCode code) {
        return MessageResponseUtils.resultError(code.getCode(), code.getDetail());
    }

    public static <T> ServiceResult<T> resultError(String errorMsg) {
        return MessageResponseUtils.resultError(0, errorMsg);
    }

    public static <T> ServiceResult<T> resultSuccess(T t, String msg) {
        return MessageResponseUtils.resultSuccess(msg, t);
    }

    public static <T> ServiceResult<T> resultSuccess(T t) {
        ServiceResult<T> ret = new ServiceResult<>();
        ret.setSuccess(true);
        ret.setErrorCode(ResponseCode.SUCCESS.getCode());
        ret.setMessage("");
        ret.setBody(t);
        return ret;
    }

    public static <T> T check(ServiceResult<T> sr) {
        if (sr == null) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException(0, "调用服务异常");
        }
        if (!sr.getSuccess()) {
            throw new BusinessException(0, "调用服务异常，" + sr.getMessage());
        }
        return sr.getBody();
    }
}
