package com.qmfresh.promotion.common.business;

/**
 * 返回结果包装类
 *
 * @author wyh
 */
public class MessageResponseUtils {

    public static <T> ServiceResult<T> resultError(int errorCode, String errorMsg) {
        ServiceResult<T> ret = new ServiceResult<>();
        ret.setSuccess(false);
        ret.setErrorCode(errorCode);
        ret.setMessage(errorMsg);
        return ret;
    }

    public static <T> ServiceResult<T> resultSuccess(String msg) {
        ServiceResult<T> ret = new ServiceResult<>();
        ret.setSuccess(true);
        ret.setErrorCode(0);
        ret.setMessage(msg);
        return ret;
    }

    public static <T> ServiceResult<T> resultSuccess(String msg, T data) {
        ServiceResult<T> ret = new ServiceResult<>();
        ret.setSuccess(true);
        ret.setErrorCode(0);
        ret.setMessage(msg);
        ret.setBody(data);
        return ret;
    }

    public static <T> ServiceResult<T> resultSuccess(T data) {
        ServiceResult<T> ret = new ServiceResult<>();
        ret.setSuccess(true);
        ret.setErrorCode(0);
        ret.setBody(data);
        return ret;
    }

}
