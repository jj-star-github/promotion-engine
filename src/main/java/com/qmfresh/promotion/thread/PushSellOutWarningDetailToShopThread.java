package com.qmfresh.promotion.thread;

import com.qmfresh.promotion.bean.promotion.PromotionSellOutSkuWarningBean;
import com.qmfresh.promotion.bean.promotion.PromotionSellOutWarningRuleBean;
import com.qmfresh.promotion.bean.sellout.AutomaticDiscountReq;
import com.qmfresh.promotion.bean.sellout.DiscountModifyPriceDto;
import com.qmfresh.promotion.bean.sellout.SkuMessage;
import com.qmfresh.promotion.bean.sellout.SkuPrice;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.entity.PromotionSellOutShopWarning;
import com.qmfresh.promotion.entity.PromotionSellOutWarningDetail;
import com.qmfresh.promotion.external.service.IQmppExternalApiService;
import com.qmfresh.promotion.manager.IPromotionSellOutWarningManager;
import com.qmfresh.promotion.message.producer.ProducerDispatcher;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.service.IPromotionSellOutShopWarningService;
import com.qmfresh.promotion.service.IPromotionSellOutWarningDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

/**
 * 每个门店，推送预警至门店子任务线程
 */
@Component
@Scope("prototype")
public class PushSellOutWarningDetailToShopThread implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(PushSellOutWarningDetailToShopThread.class);
    @Autowired
    private IPromotionSellOutShopWarningService promotionSellOutShopWarningService;

    @Autowired
    ProducerDispatcher producerDispatcher;

    @Autowired
    private IPromotionSellOutWarningDetailService promotionSellOutWarningDetailService;

    @Autowired
    private IPromotionSellOutWarningManager promotionSellOutWarningManager;
    @Autowired
    private IQmppExternalApiService qmppExternalApiService;


    private Integer shopId;

    private List<PromotionSellOutWarningDetail> details;

    private static final String TITLE = "欢乐番茄限时促销";

    private CountDownLatch latch;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public List<PromotionSellOutWarningDetail> getDetails() {
        return details;
    }

    public void setDetails(List<PromotionSellOutWarningDetail> details) {
        this.details = details;
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void run() {
        //查询该门店的预警设置
        PromotionSellOutShopWarning setting = promotionSellOutShopWarningService.getShopWarningSettingByShopId(shopId);
        //预警值逆向排序
        Collections.sort(details, (k1, k2) -> k2.getWarningValue().compareTo(k1.getWarningValue()));
        Integer limit = getLimit(setting);
        try {
            if (!ListUtil.isNullOrEmpty(details.stream().limit(limit).collect(Collectors.toList()))) {
                // 通知中台改价格
                qmppExternalApiService.noticeCenterUpdateSkuPrice(this.getModifyPriceDto(details.stream().limit(limit).collect(Collectors.toList())));
                //推送改价信息至收银端
                producerDispatcher.pushSellOutWarningToShopNotify(getSkuMessage(details.stream().limit(limit).collect(Collectors.toList())));
            }
        } catch (Exception e) {
            throw new BusinessException("更新门店(" + shopId + ")价格 或 推送消息至收银端异常:" + e.getMessage() + ",时间戳:" + DateUtil.getCurrentTimeIntValue());
        } finally {
            updateWarningDetail(setting, limit);
            this.getLatch().countDown();
        }
    }


    /**
     * 构造调用中台改价服务
     * @param details
     * @return
     */
    private DiscountModifyPriceDto getModifyPriceDto(List<PromotionSellOutWarningDetail> details) {
        DiscountModifyPriceDto reqDto = new DiscountModifyPriceDto();
        List<AutomaticDiscountReq> reqList = new ArrayList<>();
        for (PromotionSellOutWarningDetail detail : details) {
            AutomaticDiscountReq req = new AutomaticDiscountReq();
            req.setShopId(detail.getShopId());
            req.setSkuId(detail.getSkuId());
            req.setPrice(detail.getWarningPrice().divide(BigDecimal.valueOf(1000).setScale(3,RoundingMode.HALF_UP)));
            req.setUserId(detail.getCreatedId());
            req.setUserName(detail.getCreatedName());
            reqList.add(req);
        }
        reqDto.setReqList(reqList);
        return reqDto;
    }

    private Integer getLimit(PromotionSellOutShopWarning setting) {
        Integer limit;
        //按照数量触发预警 例如：数量为5 那么获取前5条预警
        if (Objects.equals(0, setting.getWarningType())) {
            limit = setting.getWarningNum();
        } else {//按照比例触发预警,例如：比例为0.4 那么触发前40%的预警
            Integer size = details.size();
            BigDecimal threshold = new BigDecimal(size).multiply(setting.getWarningRate());
            //例如：集合总量为7，40%*7 =2.8 那么前2.8个 都是符合规定的，向下取整
            limit = threshold.setScale(0, BigDecimal.ROUND_DOWN).intValue();
        }
        return limit;
    }

    private void updateWarningDetail(PromotionSellOutShopWarning setting, Integer limit) {
        List<PromotionSellOutWarningDetail> updateDetails = new ArrayList<>();
        int now = DateUtil.getCurrentTimeIntValue();
        for (int i = 0; i < details.size(); i++) {
            PromotionSellOutWarningDetail t = new PromotionSellOutWarningDetail();
            t.setId(details.get(i).getId());
            //前limit个元素 都是预警成功
            if (i < limit) {
                t.setWarningStatus(1);
            } else {
                t.setWarningStatus(2);
                t.setMsg("该预警未达到可触发条件,即不是前" + (setting.getWarningType() == 0 ? limit + "个" : setting.getWarningRate().multiply(new BigDecimal(100)) + "%") + ",时间戳:" + now);
            }
            t.setUT(now);
            updateDetails.add(t);
        }
        promotionSellOutWarningDetailService.update(updateDetails);
    }

    /**
     * 获取修改门店价格请求体
     */
    private List<SkuPrice> getSkuPrice(List<PromotionSellOutWarningDetail> details) {
        return details.stream().map(s -> {
            SkuPrice t = new SkuPrice();
            t.setShopId(s.getShopId());
            t.setSkuId(s.getSkuId());
            t.setPrice(s.getWarningPrice());
            return t;
        }).collect(Collectors.toList());
    }


    /**
     * 获取推送给收银端的预警消息
     */
    private List<SkuMessage> getSkuMessage(List<PromotionSellOutWarningDetail> details) {
        List<PromotionSellOutSkuWarningBean> warningBeanList = promotionSellOutWarningManager.matchById(details.stream().map(PromotionSellOutWarningDetail::getSellOutWarningId).collect(Collectors.toList()));
        //预警配置主键 -> 每个时间点的预警规则
        Map<Integer, List<PromotionSellOutWarningRuleBean>> map = warningBeanList.stream().collect(Collectors.toMap(PromotionSellOutSkuWarningBean::getId, PromotionSellOutSkuWarningBean::getWarnings, (k1, k2) -> k1));
        List<SkuMessage> skuMessages = new ArrayList<>();
        for (PromotionSellOutWarningDetail detail : details) {
            Integer[] times = getTimeStamp(map.get(detail.getSellOutWarningId()).stream().map(PromotionSellOutWarningRuleBean::getTime).collect(Collectors.toList()), detail.getWarningTime());
            Optional<PromotionSellOutWarningRuleBean> optional = map.get(detail.getSellOutWarningId()).stream().filter(e -> Objects.equals(detail.getWarningTime(), e.getTime())).findFirst();
            optional.ifPresent(e -> {
                SkuMessage skuMessage = new SkuMessage();
                skuMessage.setShopId(detail.getShopId());
                skuMessage.setShopName(detail.getShopName());
                skuMessage.setSkuId(detail.getSkuId());
                skuMessage.setSkuName(detail.getSkuName());
                skuMessage.setPrice(detail.getWarningPrice());
                skuMessage.setWarningLevel(detail.getWarningLevel());
                skuMessage.setWarningWay(detail.getWarningWay());
                skuMessage.setWarningDetail(getWarningDetail(detail.getSkuName(), detail.getWarningPrice(), detail.getWarningDetail()));
                skuMessage.setTime(e.getTime());
                skuMessage.setWarningDiscount(e.getWarningDiscount());
                skuMessage.setStart(times[0]);
                skuMessage.setEnd(times[1]);
                skuMessage.setWord(TITLE);
                skuMessages.add(skuMessage);
            });
        }
        return skuMessages;
    }

    /**
     * 例如: 本次预警触发时间14:00 此时对应的时间戳是播放开始时间，下一次预警是16:00 那么16:00对应的时间戳 就是播放结束时间
     * 若入参是 20:00 那么下一次没有预警了(这一天预警已经结束了)，那么20:00 往后推延一个小时(21:00对应的时间戳)作为播放的结束时间
     * 根据时分字符串，获取当天此时此分对应的时间戳，例如：14:00 获取 2020.2.15 14:00:00对应的时间戳 1581746400
     */
    private Integer[] getTimeStamp(List<String> hms, String targetHm) {
        Integer[] arr = {0, 0};
        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            StringBuilder sb = new StringBuilder();
            String ymdhm = sb.append(calendar.get(Calendar.YEAR))
                    .append("-")
                    .append(calendar.get(Calendar.MONTH) + 1)
                    .append("-")
                    .append(calendar.get(Calendar.DAY_OF_MONTH))
                    .append(" ")
                    .append(targetHm)
                    .toString();
            //本次预警触发时间对应的时间戳，作为播报的开始时间
            Integer start = (int) (sdf.parse(ymdhm).getTime() / 1000);
            //下一次预警触发时间对应的时间戳，作为播报的结束时间
            Integer end = null;
            List<Integer> times = new ArrayList<>();
            //将预警时间字符串 转化成当天对应的时间戳
            for (String hm : hms) {
                sb.setLength(0);
                ymdhm = sb.append(calendar.get(Calendar.YEAR))
                        .append("-")
                        .append(calendar.get(Calendar.MONTH) + 1)
                        .append("-")
                        .append(calendar.get(Calendar.DAY_OF_MONTH))
                        .append(" ")
                        .append(hm)
                        .toString();
                times.add((int) (sdf.parse(ymdhm).getTime() / 1000));
            }
            //此时对应的时间戳刚好大于本次预警对应的时间戳，这个就是下一次预警的时间戳了，找到直接退出遍历
            Optional<Integer> optional = times.stream().sorted().filter(e -> e > start).findFirst();
            if (optional.isPresent()) {
                end = optional.get();
            } else { //本次预警是这天最后一次预警了
                end = start + 3600;
            }
            arr[0] = start;
            arr[1] = end;
        } catch (Exception e) {
            logger.error("解析时间字符串异常 :" + e.getMessage());
        }
        return arr;
    }

    /**
     * 生成预警详情，将运营自定义的预警详情，替换掉SKU 、价格
     */
    private String getWarningDetail(String skuName, BigDecimal price, String warningDetail) {
        BigDecimal rate = new BigDecimal(1000);
        BigDecimal discountPrice = price.divide(rate, 2, RoundingMode.DOWN);
        if (warningDetail.contains("skuname") && warningDetail.contains("skuprice")) {
            return warningDetail.replaceAll("skuname", skuName).replaceAll("skuprice", discountPrice.toString());
        }
        return skuName + " " + discountPrice + "元";
    }

}
