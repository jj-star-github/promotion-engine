package com.qmfresh.promotion.thread;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.gis.Shop;
import com.qmfresh.promotion.bean.promotion.PromotionSellOutSkuWarningBean;
import com.qmfresh.promotion.bean.promotion.PromotionSellOutWarningRuleBean;
import com.qmfresh.promotion.bean.sellout.SkuPrice;
import com.qmfresh.promotion.bean.sellout.SkuSale;
import com.qmfresh.promotion.bean.sellout.SkuStock;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.common.utils.SellOutUtil;
import com.qmfresh.promotion.dto.SkuStockInShopQuery;
import com.qmfresh.promotion.entity.PromotionSellOutSkuWarning;
import com.qmfresh.promotion.entity.PromotionSellOutWarningDetail;
import com.qmfresh.promotion.external.service.ICashExternalApiService;
import com.qmfresh.promotion.external.service.IInventoryService;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.manager.IPromotionSellOutWarningManager;
import com.qmfresh.promotion.service.IPromotionSellOutWarningDetailService;
import com.qmfresh.promotion.service.IPromotionSellOutWarningService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

/**
 * 每个门店，创建预警主任务线程
 */
@Component
@Scope("prototype")
public class CreateSellOutWarningDetailThread implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(CreateSellOutWarningDetailThread.class);
    @Autowired
    private IPromotionSellOutWarningService promotionSellOutWarningService;
    @Autowired
    private IInventoryService inventoryService;
    @Autowired
    private IPromotionSellOutWarningDetailService promotionSellOutWarningDetailService;
    @Autowired
    private IPromotionSellOutWarningManager promotionSellOutWarningManager;
    @Autowired
    private IQmExternalApiService qmExternalApiService;

    @Autowired
    private ICashExternalApiService cashExternalApiService;

    //预警触发时间
    private String time;

    private Shop shop;

    private CountDownLatch latch;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void run() {
        logger.info("开始执行预警，预警时间：" + time + ",门店：" + shop.getId() + "-" + shop.getName());
        try {
            //获取与当前时间匹配的预警
            List<PromotionSellOutSkuWarningBean> list = promotionSellOutWarningManager.matchByTime(time, shop.getId().intValue());
            if (ListUtil.isNullOrEmpty(list)) {
                this.getLatch().countDown();
                return;
            }
            List<Integer> skuIds = list.stream().map(PromotionSellOutSkuWarningBean::getSkuId).distinct().collect(Collectors.toList());
            //门店+SKU ->实时销量
            Map<String, SkuSale> salesVolumeMap = querySkuSalesVolumeMap(skuIds);
            //门店+SKU ->实时价格
            Map<String, SkuPrice> skuPriceMap = querySkuPriceMap(list);
            //SKU ->实时库存
            Map<String, SkuStock> stockMap = querySkuStockMap(list, skuIds);

            logger.info("预警内容：" + JSON.toJSONString(list) + ",销量：" + JSON.toJSONString(salesVolumeMap) + ",售价：" + JSON.toJSONString(skuPriceMap) + ",库存：" + JSON.toJSONString(stockMap));

            List<PromotionSellOutWarningDetail> warningDetails = new ArrayList<>();
            for (PromotionSellOutSkuWarningBean warningBean : list) {
                String skuId = warningBean.getSkuId().toString();
                PromotionSellOutWarningRuleBean rule = getSkuRuleMap(warningBean.getWarnings());
                if (rule == null) {
                    logger.info("未获取到预警规则，预警时间:" + time + ",shop:" + warningBean.getShopId() + "--" + warningBean.getShopName() + ",sku:" + warningBean.getSkuId() + "--" + warningBean.getSkuName());
                    continue;
                }
                warningDetails.add(this.createWarningDetail(warningBean, salesVolumeMap.get(skuId), stockMap.get(skuId), skuPriceMap.get(skuId)));
            }
            if (!ListUtil.isNullOrEmpty(warningDetails)) {
                //出清预警记录创建成功
                logger.info("插入预警内容：" + JSON.toJSONString(warningDetails));
                insertWarningDetail(warningDetails);
                updateSkuWarning(list);
            }
            logger.info("预警执行结束，预警时间：" + time + ",门店：" + shop.getId() + "--" + shop.getName());
        } catch (Exception e) {
            logger.warn("生成预警详情异常，预警时间：" + time + ",门店：" + shop.getId() + "--" + shop.getName(), e);
        } finally {
            this.getLatch().countDown();
        }
    }

    /**
     * 创建预警详情对象
     */
    private PromotionSellOutWarningDetail createWarningDetail(PromotionSellOutSkuWarningBean warningBean, SkuSale skuSale, SkuStock skuStock, SkuPrice skuPrice) {
        logger.info("构建预警内容：" + JSON.toJSONString(warningBean));
        PromotionSellOutWarningRuleBean rule = getSkuRuleMap(warningBean.getWarnings());
        int now = DateUtil.getCurrentTimeIntValue();
        PromotionSellOutWarningDetail detail = new PromotionSellOutWarningDetail();
        detail.setCityId(warningBean.getCityId());
        detail.setCityName(warningBean.getCityName());
        detail.setShopId(warningBean.getShopId());
        detail.setShopName(warningBean.getShopName());
        detail.setShopType(warningBean.getShopType() == null ? 0 : warningBean.getShopType());
        detail.setSkuId(warningBean.getSkuId());
        detail.setSkuName(warningBean.getSkuName());
        detail.setIsWeight(warningBean.getIsWeight());
        detail.setSellOutWarningId(warningBean.getId());
        detail.setWarningTime(time);
        detail.setWarningDate(now);
        detail.setRemovalRateThreshold(rule.getRemovalRateThreshold());
        detail.setWarningLevel(rule.getWarningLevel());
        detail.setWarningWay(rule.getWarningWay());
        detail.setWarningDetail(rule.getWarningDetail());
        detail.setShopSkuStock(SellOutUtil.getAmount(skuStock));
        detail.setWarningValue(SellOutUtil.getWarningValue(skuStock, skuPrice, rule));
        detail.setWarningPrice(SellOutUtil.getDiscountPrice(skuPrice, rule));
        String errorMsg = SellOutUtil.createErrorMsg(warningBean, skuSale, skuStock, skuPrice, rule);
        detail.setMsg(errorMsg);
        if (errorMsg == null) {
            detail.setWarningStatus(0);
        } else {
            logger.info(errorMsg);
            detail.setWarningStatus(2);
        }
        detail.setCT(now);
        detail.setUT(now);
        detail.setIsDeleted(0);
        // 创建人id
        detail.setCreatedId(warningBean.getCreatedId());
        detail.setCreatedName(warningBean.getCreatedName());
        logger.info("单条预警内容构建完成:" + JSON.toJSONString(warningBean));
        return detail;
    }

    /**
     * 根据time找到匹配的预警规则
     */
    private PromotionSellOutWarningRuleBean getSkuRuleMap(List<PromotionSellOutWarningRuleBean> source) {
        List<PromotionSellOutWarningRuleBean> warningRuleBeans = source.stream().filter(item -> item.getTime().equals(time)).collect(Collectors.toList());
        return warningRuleBeans.get(0);
    }


    /**
     * 生成预警详情信息
     */
    public boolean insertWarningDetail(List<PromotionSellOutWarningDetail> details) {
        return promotionSellOutWarningDetailService.insert(details);
    }

    /**
     * 根据主键更新主预警信息信息，主要是未触发该预警的错误原因，还有更新对应状态值
     */
    public boolean updateSkuWarning(List<PromotionSellOutSkuWarningBean> warnings) {
        Integer now = DateUtil.getCurrentTimeIntValue();
        List<PromotionSellOutSkuWarning> updateList = new ArrayList<>(warnings.size());
        warnings.forEach(
                item -> {
                    PromotionSellOutSkuWarning updateWarning = new PromotionSellOutSkuWarning();
                    updateWarning.setId(item.getId());
                    updateWarning.setLastWarningTime(now);
                    updateWarning.setUT(now);
                    updateList.add(updateWarning);
                }
        );
        return promotionSellOutWarningService.updateSkuWarning(updateList);
    }

    /**
     * 门店的SKU ->实时销量
     */
    Map<String, SkuSale> querySkuSalesVolumeMap(List<Integer> skuIds) {
        SkuSale query = new SkuSale();
        query.setShopId(shop.getId().intValue());
        query.setSkuIds(skuIds);
        List<SkuSale> skuSaleVolumeRes = cashExternalApiService.querySkuSalesVolumeInShop(query);
        return ListUtil.isNullOrEmpty(skuSaleVolumeRes) ? new HashMap<>(16) : ListUtil.listToMap("skuId", skuSaleVolumeRes);
    }

    /**
     * 门店的SKU ->实时价格
     */
    Map<String, SkuPrice> querySkuPriceMap(List<PromotionSellOutSkuWarningBean> list) {
        List<SkuPrice> skuPriceParam = list.stream().map(e -> {
            SkuPrice t = new SkuPrice();
            t.setShopId(e.getShopId());
            t.setSkuId(e.getSkuId());
            return t;
        }).collect(Collectors.toList());
        List<SkuPrice> skuPriceList = qmExternalApiService.querySkuPriceInShop(skuPriceParam);
        return ListUtil.isNullOrEmpty(skuPriceList) ? new HashMap<>(16) : ListUtil.listToMap("skuId", skuPriceList);

    }

    /**
     * 门店的SKU ->实时库存
     */
    private Map<String, SkuStock> querySkuStockMap(List<PromotionSellOutSkuWarningBean> warningBeans, List<Integer> skuIds) {
        List<Long> skuIdLs = skuIds.stream().map(e -> Long.valueOf(e)).collect(Collectors.toList());
        SkuStockInShopQuery query = new SkuStockInShopQuery();
        query.setWarehouseId(shop.getWarehouseId().longValue());
        query.setSkuIds(skuIdLs);
        List<SkuStock> stocks = inventoryService.querySkuStockInShop(query);

        Map<String, PromotionSellOutSkuWarningBean> warningMap = ListUtil.listToMap("skuId", warningBeans);
        Map<String, SkuStock> skuStockMap = new HashMap<>(16);
        if (!ListUtil.isNullOrEmpty(stocks)) {
            stocks.forEach(
                    stock -> {
                        PromotionSellOutSkuWarningBean skuWarningBean = warningMap.get(stock.getSkuId().toString());
                        stock.setIsWeight(skuWarningBean.getIsWeight());
                        skuStockMap.put(stock.getSkuId().toString(), stock);
                    }
            );
        }

        return skuStockMap;
    }

}
