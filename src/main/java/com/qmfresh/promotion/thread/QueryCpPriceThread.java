package com.qmfresh.promotion.thread;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.common.utils.CopyUtil;
import com.qmfresh.promotion.dto.PriceChangeParam;
import com.qmfresh.promotion.dto.QueryPriceChangeResult;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.enums.PromotionCreatorTypeEnums;
import com.qmfresh.promotion.enums.PromotionTypeEnums;
import com.qmfresh.promotion.manager.IPromotionManager;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

/**
 * @ClassName QueryCpPriceThread
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/11 17:59
 */
@Component
@Scope("prototype")
@Slf4j
public class QueryCpPriceThread implements Callable {


    @Resource
    private IPromotionManager promotionManager;

    private Integer shopId;

    private List<PriceChangeParam> shopPriceChangeList;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public List<PriceChangeParam> getShopPriceChangeList() {
        return shopPriceChangeList;
    }

    public void setShopPriceChangeList(List<PriceChangeParam> shopPriceChangeList) {
        this.shopPriceChangeList = shopPriceChangeList;
    }

    @Override
    public Object call() throws Exception {
        log.info("批量同步改价数据开始");
        log.info("批量同步改价数据，参数：" + JSON.toJSONString(shopPriceChangeList));
        List<QueryPriceChangeResult> finalResultList = new ArrayList<>();
        List<PromotionActivity> promotionActivityList = promotionManager.getActivityInfoByShopInfo(shopId, Arrays.asList(PromotionTypeEnums.VIP, PromotionTypeEnums.CP));
        List<Integer> skuIdList = shopPriceChangeList.stream().map(PriceChangeParam::getSkuId).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(promotionActivityList)) {
            //此处应用多线程处理
            //2.根据activityId获取t_activity_ssu的商品信息
            List<QueryPriceChangeResult> vipResultList = new ArrayList<>();
            List<QueryPriceChangeResult> cpHeadOfficeResultList = new ArrayList<>();
            List<QueryPriceChangeResult> cpShopResultList = new ArrayList<>();
            for (PromotionActivity promotionActivity : promotionActivityList) {
                if (promotionActivity.getPromotionModuleId().equals(PromotionTypeEnums.VIP.getCode())) {
                    //获取所有会员价格
                    List<QueryPriceChangeResult> tmpResult = promotionManager.getActivityClass2List(promotionActivity.getId(), shopId, shopPriceChangeList);
                    if (!CollectionUtils.isEmpty(tmpResult)) {
                        vipResultList.addAll(tmpResult);
                    }
                } else if (promotionActivity.getPromotionModuleId().equals(PromotionTypeEnums.CP.getCode())) {
                    if (promotionActivity.getCreatorFrom().equals(PromotionCreatorTypeEnums.HEAD_OFFICE.getCode())) {
                        //总部改价
                        List<QueryPriceChangeResult> tmpResult = promotionManager.getActivitySsuList(promotionActivity.getId(), shopId,skuIdList);
                        if (!CollectionUtils.isEmpty(tmpResult)) {
                            cpHeadOfficeResultList.addAll(tmpResult);
                        }
                    }
                    //门店改价
                    if (promotionActivity.getCreatorFrom().equals(PromotionCreatorTypeEnums.SHOP.getCode())) {
                        List<QueryPriceChangeResult> tmpResult = promotionManager.getActivitySsuList(promotionActivity.getId(), shopId,skuIdList);
                        if (!CollectionUtils.isEmpty(tmpResult)) {
                            cpShopResultList.addAll(tmpResult);
                        }
                    }
                }
            }
            //3.根据活动类型获取最低促销价，或者，根据会员活动返回会员价
            //关键逻辑，比较改价商品最低价
            List<QueryPriceChangeResult> combineResultList = new ArrayList<>();
            List<QueryPriceChangeResult> midResultList = new ArrayList<>();

            if(!CollectionUtils.isEmpty(cpHeadOfficeResultList) && !CollectionUtils.isEmpty(cpShopResultList)){
                midResultList = cpShopResultList;
                for (QueryPriceChangeResult cpHeadOfficeResult : cpHeadOfficeResultList) {
                    Boolean flag = true;
                    for (QueryPriceChangeResult shopResult : cpShopResultList) {
                    	//如果总部的价格比门店的价格低取总部价格
                        if (cpHeadOfficeResult.getSkuId().equals(shopResult.getSkuId())
                                && cpHeadOfficeResult.getPromotionPrice().compareTo(shopResult.getPromotionPrice()) < 0) {
                            shopResult.setPromotionPrice(cpHeadOfficeResult.getPromotionPrice());
                            shopResult.setCreatorFrom(cpHeadOfficeResult.getCreatorFrom());
                            flag = false;
                            break;
                        }
                      //如果总部的价格比门店的价格高取门店价格
                        else if(cpHeadOfficeResult.getSkuId().equals(shopResult.getSkuId())
                                && cpHeadOfficeResult.getPromotionPrice().compareTo(shopResult.getPromotionPrice()) >= 0) {
                        	shopResult.setPromotionPrice(shopResult.getPromotionPrice());
                            shopResult.setCreatorFrom(shopResult.getCreatorFrom());
                            flag = false;
                            break;
                        }
                    }
                    if(flag){
                        midResultList.add(cpHeadOfficeResult);
                    }
                }
            }else if(CollectionUtils.isEmpty(cpHeadOfficeResultList) && !CollectionUtils.isEmpty(cpShopResultList)){
                midResultList.addAll(cpShopResultList);
            }else if(!CollectionUtils.isEmpty(cpHeadOfficeResultList) && CollectionUtils.isEmpty(cpShopResultList)){
                midResultList.addAll(cpHeadOfficeResultList);
            }

            combineResultList = new CopyUtil<QueryPriceChangeResult, QueryPriceChangeResult>().copy(midResultList, combineResultList);
            //目前会员活动和改价活动互斥，所以可以直接在后面添加会员价格
            if (!CollectionUtils.isEmpty(vipResultList)) {
                combineResultList.addAll(vipResultList);
            }
            this.buildResult(shopPriceChangeList,combineResultList,finalResultList);
            return finalResultList;
        }
        this.buildResult(shopPriceChangeList,new ArrayList<>(),finalResultList);
        log.info("批量同步改价数据结束");
        return finalResultList;
    }

    private void buildResult( List<PriceChangeParam> shopPriceChangeList,List<QueryPriceChangeResult> combineResultList,List<QueryPriceChangeResult> finalResultList){
        for(PriceChangeParam priceChangeParam : shopPriceChangeList){
            QueryPriceChangeResult changeResult = new QueryPriceChangeResult();
            BeanUtils.copyProperties(priceChangeParam,changeResult);
            for(QueryPriceChangeResult queryPriceChangeResult : combineResultList){
                if(queryPriceChangeResult.getSkuId().equals(changeResult.getSkuId())){
                    BeanUtils.copyProperties(queryPriceChangeResult,changeResult);
                    break;
                }
            }
            finalResultList.add(changeResult);
        }
        log.info("批量改价成功，改价结果：" + JSON.toJSONString(finalResultList));
    }
}
