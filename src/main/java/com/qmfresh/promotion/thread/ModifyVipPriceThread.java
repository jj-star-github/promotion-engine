package com.qmfresh.promotion.thread;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.dto.ModifyPriceParam;
import com.qmfresh.promotion.dto.SendVipPriceParam;
import com.qmfresh.promotion.external.service.IModifyPriceExternalApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @ClassName ModifyVipPriceThread
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/11 16:11
 */
@Component
@Scope("prototype")
public class ModifyVipPriceThread implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(ModifyVipPriceThread.class);

    @Resource
    private IModifyPriceExternalApiService modifyPriceExternalApiService;

    ModifyPriceParam<SendVipPriceParam> vipParam;

    public ModifyPriceParam<SendVipPriceParam> getVipParam() {
        return vipParam;
    }

    public void setVipParam(ModifyPriceParam<SendVipPriceParam> vipParam) {
        this.vipParam = vipParam;
    }

    @Override
    public void run() {
        logger.info("实时同步会员价格开始");
        logger.info("实时同步会员价格，参数：" + JSON.toJSONString(vipParam));
        modifyPriceExternalApiService.modifyVipPrice(vipParam);
        logger.info("实时同步会员价格结束");
    }
}
