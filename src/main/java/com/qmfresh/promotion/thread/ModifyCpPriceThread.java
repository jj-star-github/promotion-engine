package com.qmfresh.promotion.thread;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.dto.ModifyPriceParam;
import com.qmfresh.promotion.dto.SendPromotionPriceParam;
import com.qmfresh.promotion.external.service.IModifyPriceExternalApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @ClassName ModifyPriceThread
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/11 14:38
 */
@Component
@Scope("prototype")
public class ModifyCpPriceThread implements Runnable{

    private static final Logger logger = LoggerFactory.getLogger(ModifyCpPriceThread.class);

    @Resource
    private IModifyPriceExternalApiService modifyPriceExternalApiService;

    ModifyPriceParam<SendPromotionPriceParam> promotionParam;

    public ModifyPriceParam<SendPromotionPriceParam> getPromotionParam() {
        return promotionParam;
    }

    public void setPromotionParam(ModifyPriceParam<SendPromotionPriceParam> promotionParam) {
        this.promotionParam = promotionParam;
    }

    @Override
    public void run() {
        logger.info("实时同步改价促销价，开始");
        logger.info("实时同步改价促销价格，参数：" + JSON.toJSONString(promotionParam));
        modifyPriceExternalApiService.modifyPromotionPrice(promotionParam);
        logger.info("实时同步改价促销价，结束");
    }
}
