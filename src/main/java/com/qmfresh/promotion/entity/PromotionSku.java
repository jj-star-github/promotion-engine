package com.qmfresh.promotion.entity;



import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author xbb
 * @since 2019-11-29
 */
@TableName("t_promotion_sku")
@Data
@ToString
public class PromotionSku implements Serializable {

	private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Long id;
	/**
	 * 城市id
	 */
	@TableField("city_id")
	private Integer cityId;
	/**
	 * 城市名称
	 */
	@TableField("city_name")
	private String cityName;
	/**
	 * 门店id
	 */
	@TableField("shop_id")
	private Integer shopId;
	/**
	 * 门店名称
	 */
	@TableField("shop_name")
	private String shopName;
	/**
	 * 商品id
	 */
	@TableField("sku_id")
	private Integer skuId;
	/**
	 * 商品名
	 */
	@TableField("sku_name")
	private String skuName;
	/**
	 * 活动id(暂不使用，后期关联t_promotion_activity表)
	 */
	@TableField("activity_id")
	private Long activityId;
	/**
	 * 活动名称
	 */
	@TableField("activity_name")
	private String activityName;
	/**
	 * 活动内容
	 */
	@TableField("activity_content")
	private String activityContent;
	/**
	 * 商品一级分类id
	 */
	@TableField("class1_id")
	private Integer class1Id;
	/**
	 * 商品一级分类名
	 */
	@TableField("class1_name")
	private String class1Name;
	/**
	 * 商品二级分类id
	 */
	@TableField("class2_id")
	private Integer class2Id;
	/**
	 * 商品二级分类名
	 */
	@TableField("class2_name")
	private String class2Name;
	/**
	 * 活动开始时间
	 */
	@TableField("activity_start_time")
	private Integer activityStartTime;
	/**
	 * 活动结束时间
	 */
	@TableField("activity_end_time")
	private Integer activityEndTime;
	/**
	 * 活动状态(0:有效，1:失效)
	 */
	@TableField("activity_status")
	private Integer activityStatus;
	/**
	 * 创建时间
	 */
	@TableField("c_t")
	private Integer cT;
	/**
	 * 更新时间
	 */
	@TableField("u_t")
	private Integer uT;
	/**
	 * 删除标记（0：有效，1：删除）
	 */
	@TableField("is_deleted")
	private Integer isDeleted;



}
