package com.qmfresh.promotion.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-13
 */
@TableName("t_promotion_order_item")
@Data
@ToString
public class PromotionOrderItem implements Serializable {

    private static final long serialVersionUID = 1L;

	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	@TableField("user_id")
	private Long userId;
	/**
	 * 门店id
	 */
	@TableField("shop_id")
	private Integer shopId;
    /**
     * 渠道：1.B端,2.C端线上,3.C端线下
     */
	private Integer channel;
    /**
     * 主订单id
     */
	@TableField("order_id")
	private Long orderId;
	/**
	 * 业务订单号
	 */
	@TableField("order_code")
	private String orderCode;
    /**
     * 业务订单详情id
     */
	@TableField("order_item_id")
	private String orderItemId;
    /**
     * 商品id
     */
	@TableField("sku_id")
	private Integer skuId;
    /**
     * 商品名称
     */
	@TableField("sku_name")
	private String skuName;
	/**
	 * ssuid
	 */
	@TableField("ssu_format_id")
	private Integer ssuFormatId;
    /**
     * 订单量
     */
	private BigDecimal amount;
    /**
     * 单价
     */
	@TableField("unit_price")
	private BigDecimal unitPrice;
    /**
     * 订单行总价
     */
	private BigDecimal price;
    /**
     * 是否赠品:0.非赠品,1.赠品
     */
	@TableField("is_present")
	private Integer isPresent;
    /**
     * 创建日期
     */
	@TableField("create_date")
	private String createDate;
    /**
     * 业务发生时间
     */
	@TableField("occurrence_time")
	private Integer occurrenceTime;
    /**
     * 订单状态:0.无效，1.有效
     */
	private Integer status;
	@TableField("c_t")
	private Integer cT;
	@TableField("u_t")
	private Integer uT;
	@TableField("is_deleted")
	private Integer isDeleted;



}
