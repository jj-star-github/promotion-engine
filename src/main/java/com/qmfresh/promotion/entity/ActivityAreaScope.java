package com.qmfresh.promotion.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 活动区域表
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@TableName("t_activity_area_scope")
@Data
@ToString
public class ActivityAreaScope  implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 可能包括cityId，shopId
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("activity_id")
    private Long activityId;
    @TableField("c_t")
    private Integer cT;
    @TableField("u_t")
    private Integer uT;
    @TableField("is_deleted")
    private Integer isDeleted;

}
