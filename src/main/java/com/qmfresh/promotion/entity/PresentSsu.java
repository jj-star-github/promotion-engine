package com.qmfresh.promotion.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 赠品商品配置
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@TableName("t_present_ssu")
@Data
@ToString
public class PresentSsu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("activity_id")
    private Long activityId;
    /**
     * 满赠id
     * 赠品活动id
     */
    @TableField("present_id")
    private Long presentId;
    @TableField("sku_id")
    private Integer skuId;
    @TableField("ssu_format_id")
    private Integer ssuFormatId;
    @TableField("ssu_name")
    private String ssuName;
    /**
     * 赠送量
     */
    @TableField("give_num")
    private BigDecimal giveNum;
    /**
     * 限量
     */
    @TableField("limit_num")
    private BigDecimal limitNum;
    /**
     * 赠品图片
     */
    private String picture;
    /**
     * 赠品状态
     */
    private Integer status;
    @TableField("c_t")
    private Integer cT;
    @TableField("u_t")
    private Integer uT;
    @TableField("is_deleted")
    private Integer isDeleted;



}
