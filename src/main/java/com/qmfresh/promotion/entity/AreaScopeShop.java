package com.qmfresh.promotion.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 门店区域
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@TableName("t_area_scope_shop")
@Data
@ToString
public class AreaScopeShop implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("activity_id")
    private Long activityId;
    @TableField("area_scope_id")
    private Long areaScopeId;
    @TableField("city_id")
    private Integer cityId;
    @TableField("shop_id")
    private Integer shopId;
    @TableField("c_t")
    private Integer cT;
    @TableField("u_t")
    private Integer uT;
    @TableField("is_deleted")
    private Integer isDeleted;

    @TableField(exist = false)
    private String cityName;
    @TableField(exist = false)
    private String shopName;


}
