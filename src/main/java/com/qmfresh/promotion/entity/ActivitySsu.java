package com.qmfresh.promotion.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 促销活动ssu
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@TableName("t_activity_ssu")
@Data
@ToString
public class ActivitySsu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("ssu_name")
    private String ssuName;
    /**
     * 活动id
     */
    @TableField("activity_id")
    private Long activityId;
    /**
     * 规则id
     */
    @TableField("rule_id")
    private Long ruleId;
    /**
     * 一级分类
     */
    @TableField("class1_id")
    private Integer class1Id;
    /**
     * 二级分类
     */
    @TableField("class2_id")
    private Integer class2Id;
    /**
     * 三级分类
     */
    @TableField("class3_id")
    private Integer class3Id;
    @TableField("sku_id")
    private Integer skuId;
    @TableField("ssu_format_id")
    private Integer ssuFormatId;
    /**
     * 平台
     */
    private Integer channel;
    /**
     * @see com.qmfresh.promotion.enums.SsuTagTypeEnums
     */
    @TableField("ex_tag")
    private Integer exTag;
    @TableField("promotion_price")
    private BigDecimal promotionPrice;
    @TableField("c_t")
    private Integer cT;
    @TableField("u_t")
    private Integer uT;
    @TableField("is_deleted")
    private Integer isDeleted;


}
