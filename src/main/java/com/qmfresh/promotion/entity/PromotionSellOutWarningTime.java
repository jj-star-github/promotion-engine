package com.qmfresh.promotion.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * 出清预警 时间明细
 * @author xzw
 */
@TableName("t_promotion_sell_out_warning_time")
@Slf4j
@Data
@ToString
public class PromotionSellOutWarningTime  implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @TableId(value="id", type= IdType.AUTO)
    private Integer id;

    /**
     * 出清预警主信息表 外键
     */
    @TableField("warning_id")
    private Integer warningId;

    /**
     * 出清预警具体时间
     */
    @TableField("warning_time")
    private String warningTime;

    /**
     * 出清预警日期 包含了年月日  时分秒
     */
    @TableField("warning_date")
    private Integer warningDate;

    /**
     * 创建时间
     */
    @TableField("c_t")
    private Integer cT;
    /**
     * 更新时间
     */
    @TableField("u_t")
    private Integer uT;
    /**
     * 是否删除
     */
    @TableField("is_deleted")
    private Integer isDeleted;

}
