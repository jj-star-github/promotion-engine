package com.qmfresh.promotion.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ToString
public class PromotionActivityCombination implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
    * id
    */
    private Long id;

    /**
    * 活动id
    */
    private Long activityId;

    /**
    * 活动类型1-门店改价，2总部促销，3会员
    */
    private Integer activityType;

    /**
    * 活动门店id
    */
    private Integer shopId;

    /**
    * 活动商品
    */
    private Integer skuId;

    /**
    * 活动价
    */
    private BigDecimal activityPrice;

    /**
    * 价格类型0-指定价格，1-打折
    */
    private Integer priceType;

    /**
    * 活动开始时间
    */
    private Integer activityStartTime;

    /**
    * 活动结束时间
    */
    private Integer activityEndTime;

    /**
    * c_t
    */
    private Integer cT;

    /**
    * u_t
    */
    private Integer uT;

    /**
    * is_deleted
    */
    private Integer isDeleted;




}
