package com.qmfresh.promotion.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@TableName("t_promotion_sell_out_shop_warning")
@Data
@ToString
public class PromotionSellOutShopWarning implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @TableId(value="id", type= IdType.AUTO)
    private Integer id;

    /**
     * 城市ID
     */
    @TableField("city_id")
    private Integer cityId;

    /**
     * 城市名称
     */
    @TableField("city_name")
    private String cityName;

    /**
     * 门店ID
     */
    @TableField("shop_id")
    private Integer shopId;

    /**
     * 门店名称
     */
    @TableField("shop_name")
    private String shopName;

    /**
     * 预警级别方式 0、数量 1、比例
     */
    @TableField("warning_type")
    private Integer warningType;
    /**
     * 预警级别数量
     */
    @TableField("warning_num")
    private Integer warningNum;
    /**
     * 预警级别比例
     */
    @TableField("warning_rate")
    private BigDecimal warningRate;
    /**
     * 操作人ID
     */
    @TableField("created_id")
    private Integer createdId;
    /**
     * 操作人名称
     */
    @TableField("created_name")
    private String createdName;

    /**
     * 创建时间
     */
    @TableField("c_t")
    private Integer cT;
    /**
     * 更新时间
     */
    @TableField("u_t")
    private Integer uT;
    /**
     * 是否删除
     */
    @TableField("is_deleted")
    private Integer isDeleted;
}
