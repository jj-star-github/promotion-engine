package com.qmfresh.promotion.entity;



import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * 出清预警主信息表
 */
@TableName("t_promotion_sell_out_sku_warning")
@ToString
@Data
public class PromotionSellOutSkuWarning implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value="id", type= IdType.AUTO)
    private Integer id;

    /**
     * 城市ID
     */
    @TableField("city_id")
    private Integer cityId;

    /**
     * 城市名称
     */
    @TableField("city_name")
    private String cityName;
    /**
     * 虚拟仓ID
     */
    @TableField("warehouse_id")
    private Integer warehouseId;

    /**
     * 门店ID
     */
    @TableField("shop_id")
    private Integer shopId;

    /**
     * 门店名称
     */
    @TableField("shop_name")
    private String shopName;
    /**
     * 门店类型
     */
    @TableField("shop_type")
    private Integer shopType;
    /**
     * SKU ID
     */
    @TableField("sku_id")
    private Integer skuId;
    /**
     * SKU 名称
     */
    @TableField("sku_name")
    private String skuName;
    /**
     * 是否计重
     */
    @TableField("is_weight")
    private Integer isWeight;
    /**
     * 上次预警时间
     */
    @TableField("last_warning_time")
    private Integer lastWarningTime;
    /**
     * 预警具体的规则明细
     */
    @TableField("warning_rule")
    private String warningRule;
    /**
     * 操作人ID
     */
    @TableField("created_id")
    private Integer createdId;
    /**
     * 操作人名称
     */
    @TableField("created_name")
    private String createdName;
    /**
     * 状态
     */
    @TableField("status")
    private Integer status;
    /**
     * 创建时间
     */
    @TableField("c_t")
    private Integer cT;
    /**
     * 更新时间
     */
    @TableField("u_t")
    private Integer uT;
    /**
     * 是否删除
     */
    @TableField("is_deleted")
    private Integer isDeleted;



}
