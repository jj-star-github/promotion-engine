package com.qmfresh.promotion.entity;

import java.util.List;

/**
 * @author wyh
 * @since 2018-06-01
 */
public class WmsReceive {

    private Long id;
    /**
     * 入库单号
     */
    private String code;
    /**
     * 采购单号[PO]
     */
    private Long procurementOrderId;
    /**
     * 订单属性[1-采购 2-越库 3-调拨]
     */
    private Integer orderType;
    /**
     * 采购类型[1-预采 2-晨采 3-补采]
     */
    private Integer purchaseType;
    /**
     * 来源 1.B端 2.C端 3.BC端合并 4.店仓调拨
     */
    private Integer applySource;
    /**
     * 配送日期
     */
    private Integer deliveryTime;

    private Integer shopId;

    private String shopName;

    /**
     * 仓库唯一id
     */
    private Long warehouseId;

    private String warehouseName;
    /**
     * 供应商id
     */
    private Long supplierId;
    /**
     * 供应商
     */
    private String supplierName;
    
    /**
     * 供货方式：0供应商直供，1仓库供货
     */
    private Integer supplyType;
    /**
     * 预计到货时间
     */
    private Integer appointArrivalTime;
    /**
     * 采购员
     */
    private Long purchaserId;
    /**
     * 采购员姓名
     */
    private String purchaserName;
    /**
     * 送货人
     */
    private Long senderId;
    /**
     * 送货人姓名
     */
    private String senderName;
    /**
     * 到货登记人
     */
    private Long arrivalRegistrantId;
    /**
     * 到货登记人姓名
     */
    private String arrivalRegistrantName;
    /**
     * 实际到货时间-点击确认收货时
     */
    private Integer actualArrivalTime;
    /**
     * 开始收货人
     */
    private Long consigneeId;
    /**
     * 开始收货人姓名
     */
    private String consigneeName;
    /**
     * 收货时间
     */
    private Integer receiveTime;
    /**
     * 创建人
     */
    private Long createrId;
    /**
     * 创建人姓名
     */
    private String createrName;
    /**
     * 编辑人
     */
    private Long updaterId;
    /**
     * 更新人姓名
     */
    private String updaterName;
    /**
     * 备注
     */
    private String remark;
    
    /**
     * 商品件数（收货明细数）
     */
    private Integer goodsNum;
    /**
     * 状态（0.未完成 1.部分收货 2.全部收货，11.账单待审核，12.审核通过，13.审核失败，20.已收款）
     */
    private Integer status;
    
    private Integer confirmStatus;
    
    private String confirmPhone;
    
    private Integer confirmTime;

    /**
     * 收货单创建时间
     */
    private Integer cT;

    private List<WmsReceiveDetail> wmsReceiveDetailList;

    private Integer startTime;

    private List<Integer> statusList;

    private Integer deliveryStartTime;

    private Integer deliveryEndTime;

    public Integer getConfirmStatus() {
		return confirmStatus;
	}

	public void setConfirmStatus(Integer confirmStatus) {
		this.confirmStatus = confirmStatus;
	}

	public String getConfirmPhone() {
		return confirmPhone;
	}

	public void setConfirmPhone(String confirmPhone) {
		this.confirmPhone = confirmPhone;
	}

	public Integer getConfirmTime() {
		return confirmTime;
	}

	public void setConfirmTime(Integer confirmTime) {
		this.confirmTime = confirmTime;
	}

	public Integer getGoodsNum() {
		return goodsNum;
	}

	public void setGoodsNum(Integer goodsNum) {
		this.goodsNum = goodsNum;
	}

	public Integer getSupplyType() {
        return supplyType;
    }

    public void setSupplyType(Integer supplyType) {
        this.supplyType = supplyType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getProcurementOrderId() {
        return procurementOrderId;
    }

    public void setProcurementOrderId(Long procurementOrderId) {
        this.procurementOrderId = procurementOrderId;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Integer getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(Integer purchaseType) {
        this.purchaseType = purchaseType;
    }

    public Integer getApplySource() {
        return applySource;
    }

    public void setApplySource(Integer applySource) {
        this.applySource = applySource;
    }

    public Integer getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Integer deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public Integer getAppointArrivalTime() {
        return appointArrivalTime;
    }

    public void setAppointArrivalTime(Integer appointArrivalTime) {
        this.appointArrivalTime = appointArrivalTime;
    }

    public Long getPurchaserId() {
        return purchaserId;
    }

    public void setPurchaserId(Long purchaserId) {
        this.purchaserId = purchaserId;
    }

    public String getPurchaserName() {
        return purchaserName;
    }

    public void setPurchaserName(String purchaserName) {
        this.purchaserName = purchaserName;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public Long getArrivalRegistrantId() {
        return arrivalRegistrantId;
    }

    public void setArrivalRegistrantId(Long arrivalRegistrantId) {
        this.arrivalRegistrantId = arrivalRegistrantId;
    }

    public String getArrivalRegistrantName() {
        return arrivalRegistrantName;
    }

    public void setArrivalRegistrantName(String arrivalRegistrantName) {
        this.arrivalRegistrantName = arrivalRegistrantName;
    }

    public Integer getActualArrivalTime() {
        return actualArrivalTime;
    }

    public void setActualArrivalTime(Integer actualArrivalTime) {
        this.actualArrivalTime = actualArrivalTime;
    }

    public Long getConsigneeId() {
        return consigneeId;
    }

    public void setConsigneeId(Long consigneeId) {
        this.consigneeId = consigneeId;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public Integer getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(Integer receiveTime) {
        this.receiveTime = receiveTime;
    }

    public Long getCreaterId() {
        return createrId;
    }

    public void setCreaterId(Long createrId) {
        this.createrId = createrId;
    }

    public String getCreaterName() {
        return createrName;
    }

    public void setCreaterName(String createrName) {
        this.createrName = createrName;
    }

    public Long getUpdaterId() {
        return updaterId;
    }

    public void setUpdaterId(Long updaterId) {
        this.updaterId = updaterId;
    }

    public String getUpdaterName() {
        return updaterName;
    }

    public void setUpdaterName(String updaterName) {
        this.updaterName = updaterName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getcT() {
        return cT;
    }

    public void setcT(Integer cT) {
        this.cT = cT;
    }

    public List<WmsReceiveDetail> getWmsReceiveDetailList() {
        return wmsReceiveDetailList;
    }

    public void setWmsReceiveDetailList(List<WmsReceiveDetail> wmsReceiveDetailList) {
        this.wmsReceiveDetailList = wmsReceiveDetailList;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public List<Integer> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Integer> statusList) {
        this.statusList = statusList;
    }

    public Integer getDeliveryStartTime() {
        return deliveryStartTime;
    }

    public void setDeliveryStartTime(Integer deliveryStartTime) {
        this.deliveryStartTime = deliveryStartTime;
    }

    public Integer getDeliveryEndTime() {
        return deliveryEndTime;
    }

    public void setDeliveryEndTime(Integer deliveryEndTime) {
        this.deliveryEndTime = deliveryEndTime;
    }
}
