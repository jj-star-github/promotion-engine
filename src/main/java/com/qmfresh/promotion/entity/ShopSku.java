package com.qmfresh.promotion.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class ShopSku implements Serializable {
    private Integer shopId;
    private Integer skuId;
}
