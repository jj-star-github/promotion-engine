package com.qmfresh.promotion.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by wyh on 2019/6/4.
 *
 * @author wyh
 */
@TableName("t_promotion_activity_time")
@ToString
@Data
public class PromotionActivityTime implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 促销活动id
     */
    @TableField("activity_id")
    private Long activityId;

    /**
     * 促销模板id
     */
    @TableField("promotion_module_id")
    private Integer promotionModuleId;

    /**
     * 活动开始时间
     */
    @TableField("effect_begin_time")
    private Integer effectBeginTime;
    /**
     * 活动结束时间
     */
    @TableField("effect_end_time")
    private Integer effectEndTime;

    @TableField("week_day")
    private String weekDay;

    @TableField("c_t")
    private Integer cT;
    @TableField("u_t")
    private Integer uT;
    @TableField("is_deleted")
    private Integer isDeleted;
    @TableField(exist = false)
    private Integer shopId;
    

}
