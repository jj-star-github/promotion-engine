package com.qmfresh.promotion.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 城市区域
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@TableName("t_area_scope_city")
@Data
@ToString
public class AreaScopeCity implements Serializable {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Long id;
	@TableField("activity_id")
	private Long activityId;
    /**
     * 区域配置id
     */
	@TableField("area_scope_id")
	private Long areaScopeId;
    /**
     * 城市id
     */
	@TableField("city_id")
	private Integer cityId;
    /**
     * 排除城市
     */
	@TableField("ex_shop")
	private String exShop;
	@TableField("c_t")
	private Integer cT;
	@TableField("u_t")
	private Integer uT;
	@TableField("is_deleted")
	private Integer isDeleted;
}
