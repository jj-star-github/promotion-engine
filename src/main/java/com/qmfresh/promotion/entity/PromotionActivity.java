package com.qmfresh.promotion.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qmfresh.promotion.enums.ActivityStatusTypeEnums;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@TableName("t_promotion_activity")
@Data
@ToString
public class PromotionActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 促销模板id
     */
    @TableField("promotion_module_id")
    private Integer promotionModuleId;
    /**
     * 促销渠道:0全部,1.B端,2.C端线上,3.C端线下
     */
    private Integer channel;
    /**
     * 活动名称
     */
    @TableField("activity_name")
    private String activityName;
    /**
     * 活动描述
     */
    private String contents;

    /**
     * 是否上线
     * @see com.qmfresh.promotion.enums.ActivityOnlineTypeEnums
     */
    @TableField("is_online")
    private Integer isOnline;
    /**
     * 上线时间
     */
    @TableField("online_time")
    private Integer onlineTime;
    /**
     * 下线时间
     */
    @TableField("offline_time")
    private Integer offlineTime;
    /**
     * 生效状态:0.未进行,1.进行中，10已结束，11.已暂停
     * @see ActivityStatusTypeEnums
     */
    private Integer status;

    /**
     * 创建活动来源
     */
    @TableField("creator_from")
    private Integer creatorFrom;
    /**
     * 创建人id
     */
    @TableField("create_user_id")
    private Long createUserId;
    /**
     * 创建人
     */
    @TableField("create_user_name")
    private String createUserName;
    /**
     * 修改人id
     */
    @TableField("update_user_id")
    private Long updateUserId;
    /**
     * 修改人
     */
    @TableField("update_user_name")
    private String updateUserName;
    @TableField("c_t")
    private Integer cT;
    @TableField("u_t")
    private Integer uT;
    @TableField("is_deleted")
    private Integer isDeleted;

    @TableField(exist = false)
    private Integer shopId;
    @TableField(exist = false)
    private Long operatorId;
    @TableField(exist = false)
    private String operatorName;
}
