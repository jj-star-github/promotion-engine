package com.qmfresh.promotion.entity;

import java.util.List;

public class AreaConditionQuery {

	private String areaName;

	private Integer isDeleted;
	private Integer areaId;

	private List<Integer> areaIds;

	private List<Long> shopIds;

	private Integer pid;

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public List<Integer> getAreaIds() {
		return areaIds;
	}

	public void setAreaIds(List<Integer> areaIds) {
		this.areaIds = areaIds;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public List<Long> getShopIds() {
		return shopIds;
	}

	public void setShopIds(List<Long> shopIds) {
		this.shopIds = shopIds;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}
}
