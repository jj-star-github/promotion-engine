package com.qmfresh.promotion.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 用户作用域
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@TableName("t_user_scope_group")
@Data
@ToString
public class UserScopeGroup implements Serializable{

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("activity_id")
    private Long activityId;
    /**
     * 名称
     */
    private String name;

    /**
     * 用户作用域
     */
    @TableField("user_scope_id")
    private Long userScopeId;
    /**
     * 用户类型（1.全部，2.普通会员，10.黄金会员）
     */
    @TableField("user_type")
    private Integer userType;
    /**
     * 用户规则
     */
    @TableField("user_rule")
    private String userRule;
    @TableField("c_t")
    private Integer cT;
    @TableField("u_t")
    private Integer uT;
    @TableField("is_deleted")
    private Integer isDeleted;

}
