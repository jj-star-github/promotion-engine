package com.qmfresh.promotion.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
public class ShopDetail extends Model<ShopDetail> {

    private Long id;
    private Integer shopType;
    private String shopName;
    private Integer cityId;
    private String shopDesc;
    private Integer uT;
    private Integer cT;
    private Integer isDeleted;
    private Integer areaId;
    private Integer baseAreaId;
    /**
     * 售卖渠道
     */
    private Integer sellChannel;
    private Integer status;
    private String locationLan;
    private String locationLgt;
    /**
     * 开始售卖时间
     */
    private String startTime;
    /**
     * 截单时间
     */
    private String endTime;
    /**
     * 是否开放下单
     */
    private Integer isOrder;

    private BigDecimal startSendMoney;

    private String address;
    private String name;
    private String phone;
    private String cityIds;
    private Integer advancePayment;
    private Integer notType;
    private Integer notSellChannel;
    private Integer warehouseId;
    private String warehouseName;
    private Integer deliveryModel;
    private String bank;
    private String openingBank;
    private String bankCard;
    private String cardholder;
    private BigDecimal aliServiceCharge;
    private BigDecimal wxServiceCharge;
    private BigDecimal cashCardCharge;
    private MultipartFile multiFile;
    private List<String> pics;
    private String picsName;
    private String openDate;
    private Integer gisAreaId;
    private Integer isLeaShopUpdate;
    private String province;
    private String city;
    private Integer shopLevel;

    public Integer getShopLevel() {
        return shopLevel;
    }

    public void setShopLevel(Integer shopLevel) {
        this.shopLevel = shopLevel;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getShopType() {
        return shopType;
    }

    public void setShopType(Integer shopType) {
        this.shopType = shopType;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getShopDesc() {
        return shopDesc;
    }

    public void setShopDesc(String shopDesc) {
        this.shopDesc = shopDesc;
    }

    public Integer getuT() {
        return uT;
    }

    public void setuT(Integer uT) {
        this.uT = uT;
    }

    public Integer getcT() {
        return cT;
    }

    public void setcT(Integer cT) {
        this.cT = cT;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getSellChannel() {
        return sellChannel;
    }

    public void setSellChannel(Integer sellChannel) {
        this.sellChannel = sellChannel;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getLocationLan() {
        return locationLan;
    }

    public void setLocationLan(String locationLan) {
        this.locationLan = locationLan;
    }

    public String getLocationLgt() {
        return locationLgt;
    }

    public void setLocationLgt(String locationLgt) {
        this.locationLgt = locationLgt;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getIsOrder() {
        return isOrder;
    }

    public void setIsOrder(Integer isOrder) {
        this.isOrder = isOrder;
    }

    public BigDecimal getStartSendMoney() {
        return startSendMoney;
    }

    public void setStartSendMoney(BigDecimal startSendMoney) {
        this.startSendMoney = startSendMoney;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCityIds() {
        return cityIds;
    }

    public void setCityIds(String cityIds) {
        this.cityIds = cityIds;
    }

    public Integer getAdvancePayment() {
        return advancePayment;
    }

    public void setAdvancePayment(Integer advancePayment) {
        this.advancePayment = advancePayment;
    }

    public Integer getNotType() {
        return notType;
    }

    public void setNotType(Integer notType) {
        this.notType = notType;
    }

    public Integer getNotSellChannel() {
        return notSellChannel;
    }

    public void setNotSellChannel(Integer notSellChannel) {
        this.notSellChannel = notSellChannel;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Integer getDeliveryModel() {
        return deliveryModel;
    }

    public void setDeliveryModel(Integer deliveryModel) {
        this.deliveryModel = deliveryModel;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getOpeningBank() {
        return openingBank;
    }

    public void setOpeningBank(String openingBank) {
        this.openingBank = openingBank;
    }

    public String getBankCard() {
        return bankCard;
    }

    public void setBankCard(String bankCard) {
        this.bankCard = bankCard;
    }

    public String getCardholder() {
        return cardholder;
    }

    public void setCardholder(String cardholder) {
        this.cardholder = cardholder;
    }

    public BigDecimal getAliServiceCharge() {
        return aliServiceCharge;
    }

    public void setAliServiceCharge(BigDecimal aliServiceCharge) {
        this.aliServiceCharge = aliServiceCharge;
    }

    public BigDecimal getWxServiceCharge() {
        return wxServiceCharge;
    }

    public void setWxServiceCharge(BigDecimal wxServiceCharge) {
        this.wxServiceCharge = wxServiceCharge;
    }

    public BigDecimal getCashCardCharge() {
        return cashCardCharge;
    }

    public void setCashCardCharge(BigDecimal cashCardCharge) {
        this.cashCardCharge = cashCardCharge;
    }

    public MultipartFile getMultiFile() {
        return multiFile;
    }

    public void setMultiFile(MultipartFile multiFile) {
        this.multiFile = multiFile;
    }

    public List<String> getPics() {
        return pics;
    }

    public void setPics(List<String> pics) {
        this.pics = pics;
    }

    public String getPicsName() {
        return picsName;
    }

    public void setPicsName(String picsName) {
        this.picsName = picsName;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public Integer getGisAreaId() {
        return gisAreaId;
    }

    public void setGisAreaId(Integer gisAreaId) {
        this.gisAreaId = gisAreaId;
    }

    public Integer getIsLeaShopUpdate() {
        return isLeaShopUpdate;
    }

    public void setIsLeaShopUpdate(Integer isLeaShopUpdate) {
        this.isLeaShopUpdate = isLeaShopUpdate;
    }

    public Integer getBaseAreaId() {
        return baseAreaId;
    }

    public void setBaseAreaId(Integer baseAreaId) {
        this.baseAreaId = baseAreaId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
