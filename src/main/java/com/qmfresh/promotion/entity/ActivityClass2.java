package com.qmfresh.promotion.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 促销活动分类
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@TableName("t_activity_class2")
@Data
@ToString
public class ActivityClass2 implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 活动id
     */
    @TableField("activity_id")
    private Long activityId;
    /**
     * 规则id
     */
    @TableField("rule_id")
    private Long ruleId;
    /**
     * 售卖二级分类
     */
    @TableField("class_id")
    private Integer classId;
    @TableField("c_t")
    private Integer cT;
    @TableField("u_t")
    private Integer uT;
    @TableField("is_deleted")
    private Integer isDeleted;
    @TableField(exist = false)
    private String className;



}
