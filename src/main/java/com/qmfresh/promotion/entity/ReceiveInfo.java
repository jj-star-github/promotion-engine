package com.qmfresh.promotion.entity;

import java.util.List;

/**
 * Created by wyh on 2018/9/20.
 *
 * @author wyh
 */
public class ReceiveInfo {

    private WmsReceive wmsReceive;

    private List<WmsReceiveDetail> details;

    public WmsReceive getWmsReceive() {
        return wmsReceive;
    }

    public void setWmsReceive(WmsReceive wmsReceive) {
        this.wmsReceive = wmsReceive;
    }

    public List<WmsReceiveDetail> getDetails() {
        return details;
    }

    public void setDetails(List<WmsReceiveDetail> details) {
        this.details = details;
    }
}
