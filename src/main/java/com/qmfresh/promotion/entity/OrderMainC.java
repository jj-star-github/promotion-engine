package com.qmfresh.promotion.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * C端订单主表
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
@TableName("t_order_main_c")
@Data
@ToString
public class OrderMainC implements Serializable {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Long id;
	@TableField("order_id")
	private Long orderId;
    /**
     * -1已取消0未确认1商家已确认2后台已确认3已发运4配送中5已收货6对账完成
     */
	private Integer status;
    /**
     * 支付方式 2:支付宝，3微信，4现金
     */
	@TableField("pay_type")
	private Integer payType;
    /**
     * 下单用户id
     */
	@TableField("user_id")
	private Integer userId;
    /**
     * 商品的总价格
     */
	@TableField("goods_amount")
	private BigDecimal goodsAmount;
    /**
     * 收银折扣优惠金额
     */
	@TableField("discount_price")
	private BigDecimal discountPrice;
    /**
     * 优惠券优惠金额
     */
	@TableField("coupon_price")
	private BigDecimal couponPrice;
    /**
     * 优惠券id
     */
	@TableField("coupon_id")
	private Long couponId;
	@TableField("shop_id")
	private Integer shopId;
    /**
     * 客户端订单号,系统判断是否重复
     */
	@TableField("order_code")
	private String orderCode;
    /**
     * 产品现价合计价格 （原价格）
     */
	@TableField("goods_price")
	private BigDecimal goodsPrice;
	@TableField("create_time")
	private Long createTime;
    /**
     * 抹零
     */
	@TableField("free_price")
	private BigDecimal freePrice;
    /**
     * 挂单人
     */
	@TableField("guadan_id")
	private Integer guadanId;
    /**
     * 抵扣积分
     */
	@TableField("deduct_credit")
	private Long deductCredit;
	@TableField("passport_id")
	private Integer passportId;
    /**
     * 创建人
     */
	@TableField("created_by")
	private Integer createdBy;
    /**
     * 支付id
     */
	@TableField("pay_id")
	private String payId;
    /**
     * 订单类型：0，pc端订单；1，移动端订单
     */
	@TableField("order_type")
	private Integer orderType;
	@TableField("delivery_start_time")
	private Long deliveryStartTime;
	@TableField("delivery_end_time")
	private Long deliveryEndTime;
	@TableField("c_t")
	private Long cT;
    /**
     * 0正常下单7部分退款8全部退款
     */
	@TableField("refund_status")
	private BigDecimal refundStatus;


}
