package com.qmfresh.promotion.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ShopSkuSellOrStock implements Serializable {
    private Integer shopId;
    private Integer skuId;
    private String skuName;
    private BigDecimal saleNum;
    private BigDecimal saleAmt;
    private BigDecimal realtimeStock;
}
