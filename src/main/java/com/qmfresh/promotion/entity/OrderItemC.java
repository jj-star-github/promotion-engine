package com.qmfresh.promotion.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

import java.io.Serializable;

/**
 * <p>
 * 订单详情
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
@TableName("t_order_item_c")
@Data
@ToString
public class OrderItemC implements Serializable {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 订单主表外键
     */
	@TableField("order_id")
	private Long orderId;
	@TableField("sku_id")
	private Integer skuId;
    /**
     * 产品小计价格，实际价格*数量
     */
	@TableField("sub_total")
	private BigDecimal subTotal;
    /**
     * 数量（重量）
     */
	private BigDecimal total;
    /**
     * 规格，传0
     */
	@TableField("option_id")
	private Integer optionId;
    /**
     * 现价，现价*数量
     */
	@TableField("real_price")
	private BigDecimal realPrice;
    /**
     * 优惠价格
     */
	private BigDecimal discount;
	@TableField("shop_id")
	private Integer shopId;
    /**
     * 均摊金额
     */
	@TableField("order_discount")
	private BigDecimal orderDiscount;
	@TableField("ssu_id")
	private Integer ssuId;
}
