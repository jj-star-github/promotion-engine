package com.qmfresh.promotion.entity;

import lombok.Data;
import lombok.ToString;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiaowei
 * @since 2018-05-02
 */
@Data
@ToString
public class AdminUser {

	private Integer id;
	private String userName;
	private String userPwd;
	private String realName;
	private Integer status;
	private String token;
    /**
     * 0普通1销售
     */
	private Integer roleId;
	private Integer shopId;

}
