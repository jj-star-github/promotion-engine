package com.qmfresh.promotion.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 退货单详情
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
@TableName("t_orderc_return_item")
@Data
@ToString
public class OrdercReturnItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 退货主表外键
     */
    @TableField("order_return_id")
    private Long orderReturnId;
    /**
     * 收银系统的退货ID
     */
    @TableField("refunds_id")
    private Long refundsId;
    /**
     * 退货单号
     */
    @TableField("refunds_code")
    private String refundsCode;
    @TableField("ssu_id")
    private Integer ssuId;
    @TableField("sku_id")
    private Integer skuId;
    /**
     * 产品小计价格，实际价格*数量
     */
    @TableField("sub_total")
    private BigDecimal subTotal;
    /**
     * 门店ID
     */
    @TableField("shop_id")
    private Integer shopId;
    /**
     * 数量（重量）
     */
    private BigDecimal total;
    @TableField("c_t")
    private Integer cT;
    @TableField("u_t")
    private Integer uT;
    @TableField("is_deleted")
    private Integer isDeleted;

}
