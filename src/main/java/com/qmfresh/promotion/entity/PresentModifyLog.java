package com.qmfresh.promotion.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-25
 */
@TableName("t_present_modify_log")
@Data
@ToString
public class PresentModifyLog implements Serializable {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Long id;
	@TableField("present_id")
	private Long presentId;
    /**
     * 赠品活动状态
     */
	private Integer status;
    /**
     * 修改人id
     */
	@TableField("user_id")
	private Long userId;
    /**
     * 修改人名称
     */
	@TableField("user_name")
	private String userName;
	@TableField("c_t")
	private Integer cT;
	@TableField("u_t")
	private Integer uT;
	@TableField("is_deleted")
	private Integer isDeleted;


}
