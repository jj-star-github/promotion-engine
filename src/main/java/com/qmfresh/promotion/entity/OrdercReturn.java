package com.qmfresh.promotion.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

import java.io.Serializable;

/**
 * <p>
 * 退货单主表
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
@TableName("t_orderc_return")
@Data
@ToString
public class OrdercReturn implements Serializable{

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 收银系统退货ID
     */
	@TableField("refunds_id")
	private Long refundsId;
    /**
     * 退货单号
     */
	private String refundsCode;
    /**
     * 退货用户id（没有先传0）
     */
	@TableField("user_id")
	private Long userId;
    /**
     * 1为余额，2为支付宝，3微信， 4 现金（返还用户的方式）
     */
	@TableField("pay_type")
	private Integer payType;
    /**
     * 店仓ID
     */
	@TableField("shop_id")
	private Integer shopId;
    /**
     * 退货金额
     */
	@TableField("goods_amount")
	private BigDecimal goodsAmount;
    /**
     * 退货状态 0:未完成;1:完成
     */
	private Integer status;
	@TableField("c_t")
	private Integer cT;
	@TableField("u_t")
	private Integer uT;
	@TableField("is_deleted")
	private Integer isDeleted;
    /**
     * 创建人
     */
	@TableField("created_by")
	private Integer createdBy;
    /**
     * 订单编号
     */
	@TableField("order_code")
	private String orderCode;




}
