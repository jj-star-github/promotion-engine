package com.qmfresh.promotion.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-13
 */
@TableName("t_promotion_order")
@Data
@ToString
public class PromotionOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 主订单号
     */
    @TableField("order_code")
    private String orderCode;
    /**
     * 渠道
     */
    private Integer channel;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;
    /**
     * 门店id
     */
    @TableField("shop_id")
    private Integer shopId;
    /**
     * 创建日期
     */
    @TableField("create_date")
    private String createDate;
    /**
     * 业务发生时间
     */
    @TableField("occurrence_time")
    private Integer occurrenceTime;
    /**
     * 订单类型:1.正向，2.逆向
     */
    @TableField("order_type")
    private Integer orderType;
    /**
     * 订单状态:0.无效，1.有效
     */
    private Integer status;
    /**
     * 原金额
     */
    @TableField("origin_total_price")
    private BigDecimal originTotalPrice;
    /**
     * 优惠金额（抹零、使用优惠券等优惠金额）
     */
    @TableField("preferential_price")
    private BigDecimal preferentialPrice;
    /**
     * 总金额（实际支付）
     */
    @TableField("total_price")
    private BigDecimal totalPrice;
    @TableField("c_t")
    private Integer cT;
    @TableField("u_t")
    private Integer uT;
    @TableField("is_deleted")
    private Integer isDeleted;

}
