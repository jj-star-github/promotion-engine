package com.qmfresh.promotion.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@TableName("t_promotion_rule")
@Data
@ToString
public class PromotionRule implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("activity_id")
    private Long activityId;
    /**
     * 促销模板id
     */
    @TableField("promotion_module_id")
    private Integer promotionModuleId;
    /**
     * @see com.qmfresh.promotion.enums.ActivityOnlineTypeEnums
     */
    @TableField("is_online")
    private Integer isOnline;
    /**
     * 区域范围：1.全国，2.城市，3.门店
     */
    @TableField("area_scope")
    private Integer areaScope;
    /**
     * 商品作用范围:1.一级分类,2.二级分类,3.三级分类,4.ssu,10.全场
     */
    @TableField("product_scope")
    private Integer productScope;
    /**
     * 促销规则
     */
    @TableField("promotion_rule")
    private String promotionRule;
    private String descript;
    @TableField("c_t")
    private Integer cT;
    @TableField("u_t")
    private Integer uT;
    @TableField("is_deleted")
    private Integer isDeleted;



}
