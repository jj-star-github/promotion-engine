package com.qmfresh.promotion.entity;

import com.qmfresh.promotion.enums.AviatorRuleReturnEnums;
import lombok.Data;
import lombok.ToString;

/**
 * Created by wyh on 2019/5/14.
 *
 * @author wyh
 */
@Data
@ToString
public class AviatorRuleInfo {

    /**
     * 规则id
     */
    private Long ruleId;
    /**
     * 所属规则集id
     */
    private Long ruleGroupId;
    /**
     * 规则集名称
     */
    private String ruleGroupName;
    /**
     * 规则名称
     */
    private String ruleName;
    /**
     * 规则表达式
     */
    private String ruleExpression;
    /**
     * 规则优先级（值越大优先级越高）
     */
    private Integer level;
    /**
     * 规则执行动作（1：必须满足 2：满足跳过后面的验证 3：不满足跳过，只要后面有规则满足）
     */
    private Integer action;

    /**
     * 返回类型（1.布尔类型,2.数值型,3.自定义,4.AviatorObject）
     */
    private AviatorRuleReturnEnums returnType;


}
