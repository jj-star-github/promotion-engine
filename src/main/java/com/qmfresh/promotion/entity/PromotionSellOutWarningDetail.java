package com.qmfresh.promotion.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 出清预警 发送门店明细表，同时也记录了促销预警的门店信息
 */
@TableName("t_promotion_sell_out_warning_detail")
@ToString
@Data
public class PromotionSellOutWarningDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value="id", type= IdType.AUTO)
    private Long id;
    /**
     * 城市ID
     */
    @TableField("city_id")
    private Integer cityId;
    /**
     * 城市名称
     */
    @TableField("city_name")
    private String cityName;
    /**
     * 门店ID
     */
    @TableField("shop_id")
    private Integer shopId;
    /**
     * 门店名称
     */
    @TableField("shop_name")
    private String shopName;
    /**
     * 门店类型
     */
    @TableField("shop_type")
    private Integer shopType;
    /**
     * SKU ID
     */
    @TableField("sku_id")
    private Integer skuId;
    /**
     * SKU 名称
     */
    @TableField("sku_name")
    private String skuName;
    /**
     * 是否计重
     */
    @TableField("is_weight")
    private Integer isWeight;
    /**
     * 出清预警主表 主键ID
     */
    @TableField("sell_out_warning_id")
    private Integer sellOutWarningId;
    /**
     * 预警数值
     */
    @TableField("warning_value")
    private BigDecimal warningValue;
    /**
     * 实时库存
     */
    @TableField("shop_sku_stock")
    private BigDecimal shopSkuStock;
    /**
     * 去化率阈值
     */
    @TableField("removal_rate_threshold")
    private BigDecimal removalRateThreshold;

    /**
     * 预警后价格
     */
    @TableField("warning_price")
    private BigDecimal warningPrice;

    /**
     * 预警 具体时间
     */
    @TableField("warning_time")
    private String warningTime;
    /**
     * 预警 日期
     */
    @TableField("warning_date")
    private Integer warningDate;
    /**
     * 预警 级别
     */
    @TableField("warning_level")
    private Integer warningLevel;
    /**
     * 预警 方式 0、广播播报 1、电视播报
     */
    @TableField("warning_way")
    private Integer warningWay;
    /**
     * 预警 内容
     */
    @TableField("warning_detail")
    private String warningDetail;
    /**
     * 0、未发送给门店 1、已经发送门店 2、发送门店失败
     */
    @TableField("warning_status")
    private Integer warningStatus;
    /**
     * 未触发预警的原因
     */
    @TableField("msg")
    private String msg;
    /**
     * 创建时间
     */
    @TableField("c_t")
    private Integer cT;
    /**
     * 更新时间
     */
    @TableField("u_t")
    private Integer uT;
    /**
     * 是否删除
     */
    @TableField("is_deleted")
    private Integer isDeleted;

    /**
     * 创建人id
     */
    @TableField("created_id")
    private Integer createdId;
    
    /**
     * 创建人名称
     */
    @TableField("created_name")
    private String createdName;


}
