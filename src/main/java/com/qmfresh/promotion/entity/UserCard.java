package com.qmfresh.promotion.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
@TableName("t_user_card")
@Data
@ToString
public class UserCard implements Serializable {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
	@TableField("user_id")
	private Integer userId;
    /**
     * 卡号/卡名
     */
	@TableField("card_code")
	private String cardCode;
	@TableField("c_t")
	private Integer cT;
    /**
     *  0不锁定，1，锁定
     */
	@TableField("is_lock")
	private Integer isLock;
    /**
     *  条码卡
     */
	@TableField("card_type")
	private Integer cardType;
    /**
     * 卡面号
     */
	@TableField("face_code")
	private String faceCode;


}
