package com.qmfresh.promotion.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 业务区域表
 * </p>
 *
 * @author lt
 * @since 2019-09-06
 */
public class BaseArea {

    /**
     * 业务区id
     */
	private Integer id;
    /**
     * 父级id
     */
	private Integer pid;
    /**
     * 区域名
     */
	private String areaName;
    /**
     * 区域码
     */
	private String areaCode;
    /**
     * 创建时间
     */
	private Integer cT;

	private Integer level;

    /**
     * 0  有效   1失效
     */
	private Integer isDeleted;

	private Integer time;

	/**
	 * 子区域
	 */
	private List<BaseArea> children;

	/**
	 * 展示名
	 */
	private String label;

	/**
	 * 展示名
	 */
	private Integer shopId;

	public Integer getShopId() {
		return shopId;
	}

	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}

	public BaseArea(){
		children = new ArrayList<>();
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public Integer getCT() {
		return cT;
	}

	public void setCT(Integer cT) {
		this.cT = cT;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public List<BaseArea> getChildren() {
		return children;
	}

	public void setChildren(List<BaseArea> children) {
		this.children = children;
	}

	public Integer getTime() {
		return time;
	}

	public void setTime(Integer time) {
		this.time = time;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}
}
