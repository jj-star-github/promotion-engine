package com.qmfresh.promotion.entity;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author wyh
 * @since 2018-06-01
 */
public class WmsReceiveDetail {

	private Long id;
	/**
	 * 采购行与入库行关联码[入库单去掉”SH”+流水号001]
	 */
	private String code;
	/**
	 * 入库单id
	 */
	private Long wmsReceiveId;

	/**
	 * 采购单id
	 */
	private Long procurementOrderId;
	/**
	 * 对应采购行ID[PI]
	 */
	private Long procurementItemId;

	private String orderItemIds;

	/**
	 * 来源 1.B端 2.C端 3.BC端合并 4.店仓调拨
	 */
	private Integer applySource;

	/**
	 * 商品id
	 */
	private Long skuId;
	/**
	 * 商品名
	 */
	private String skuName;
	/**
	 * 是否称重
	 */
	private Integer isWeight;
	/**
	 * SSU包规数值
	 */
	private BigDecimal packageFormatNum;
	/**
	 * 是否分拣越库（0.未分拣 1.分拣）
	 */
	private Integer isSorted;
	/**
	 * 采购数量
	 */
	private BigDecimal expectNum;
	/**
	 * 采购重量
	 */
	private BigDecimal expectWeight;
	/**
	 * 收货数量
	 */
	private BigDecimal receiveNum;
	/**
	 * 收货重量
	 */
	private BigDecimal receiveWeight;

	/**
	 * 调整数量
	 */
	private BigDecimal adjustNum;

	/**
	 * 调整重量
	 */
	private BigDecimal adjustWeight;

	/**
	 * 物理单位
	 */
	private String physicalUnit;
	/**
	 * 计价单位
	 */
	private String priceUnit;
	/**
	 * 单价
	 */
	private BigDecimal unitPrice;

	/**
	 * 价格批次ID
	 */
	private Long priceId;
	/**
	 * 等级
	 */
	private String level;
	/**
	 * 品牌
	 */
	private String ownBrand;
	/**
	 * 规格
	 */
	private String format;
	/**
	 * 收货时间
	 */
	private Integer receiveTime;
	/**
	 * 质检结果[11-质检通过 12-质检不通过]
	 */
	private Integer inspectionResult;
	/**
	 * 质检时间
	 */
	private Integer inspectionTime;
	/**
	 * 质检人
	 */
	private Long inspectorId;
	/**
	 * 质检人姓名
	 */
	private String inspectorName;
	/**
	 * 收货完成时间－行状态变为收货完成
	 */
	private Integer receiveCompleteTime;
	/**
	 * 是否带包装物
	 */
	private Integer isWithPackage;
	/**
	 * 创建人
	 */
	private Long createrId;
	/**
	 * 创建人姓名
	 */
	private String createrName;
	/**
	 * 编辑人
	 */
	private Long updaterId;
	/**
	 * 更新人姓名
	 */
	private String updaterName;
	/**
	 * 仓库ID
	 */
	private Long warehouseId;

	private Integer shopId;
	/**
	 * 采购预约到货时间
	 */
	private Integer appointArrivalTime;
	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 收货操作（0.未知，1.扫码收货，2.pc端收货，3.同步供应商出货，4.同步分拣）
	 */
	private Integer receiveType;
	/**
	 * 状态[0-未完成(默认) 1.包裹已确认 2.已完成 -1-取消]
	 */
	private Integer status;

	/**
	 * 采购价
	 */
	private BigDecimal purchasePrice;
	private Integer cT;

	private Integer uT;

    /**
     * 配送时间（关联主单查询）
     */
    private Integer deliveryTime;
    /**
     * 采购类型
     */
    private Integer purchaseType;
    private Integer supplyType;
    private Integer supplierId;
    private String supplierName;
	/**
	 * 关联主单状态
	 */
	private Integer mainStatus;

	private Integer skuType;

	private List<Long> wmsReceiveIds;

	private List<Integer> statusList;

	public Integer getShopId() {
		return shopId;
	}

	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}

	public Long getPriceId() {
		return priceId;
	}

	public void setPriceId(Long priceId) {
		this.priceId = priceId;
	}

	public String getOrderItemIds() {
		return orderItemIds;
	}

	public void setOrderItemIds(String orderItemIds) {
		this.orderItemIds = orderItemIds;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getWmsReceiveId() {
		return wmsReceiveId;
	}

	public void setWmsReceiveId(Long wmsReceiveId) {
		this.wmsReceiveId = wmsReceiveId;
	}

	public Long getProcurementOrderId() {
		return procurementOrderId;
	}

	public void setProcurementOrderId(Long procurementOrderId) {
		this.procurementOrderId = procurementOrderId;
	}

	public Long getProcurementItemId() {
		return procurementItemId;
	}

	public void setProcurementItemId(Long procurementItemId) {
		this.procurementItemId = procurementItemId;
	}

	public Integer getApplySource() {
		return applySource;
	}

	public void setApplySource(Integer applySource) {
		this.applySource = applySource;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public Integer getIsWeight() {
		return isWeight;
	}

	public void setIsWeight(Integer isWeight) {
		this.isWeight = isWeight;
	}

	public BigDecimal getPackageFormatNum() {
		return packageFormatNum;
	}

	public void setPackageFormatNum(BigDecimal packageFormatNum) {
		this.packageFormatNum = packageFormatNum;
	}

	public Integer getIsSorted() {
		return isSorted;
	}

	public void setIsSorted(Integer isSorted) {
		this.isSorted = isSorted;
	}

	public BigDecimal getExpectNum() {
		return expectNum;
	}

	public void setExpectNum(BigDecimal expectNum) {
		this.expectNum = expectNum;
	}

	public BigDecimal getExpectWeight() {
		return expectWeight;
	}

	public void setExpectWeight(BigDecimal expectWeight) {
		this.expectWeight = expectWeight;
	}

	public BigDecimal getReceiveNum() {
		return receiveNum;
	}

	public void setReceiveNum(BigDecimal receiveNum) {
		this.receiveNum = receiveNum;
	}

	public BigDecimal getReceiveWeight() {
		return receiveWeight;
	}

	public void setReceiveWeight(BigDecimal receiveWeight) {
		this.receiveWeight = receiveWeight;
	}

	public BigDecimal getAdjustNum() {
		return adjustNum;
	}

	public void setAdjustNum(BigDecimal adjustNum) {
		this.adjustNum = adjustNum;
	}

	public BigDecimal getAdjustWeight() {
		return adjustWeight;
	}

	public void setAdjustWeight(BigDecimal adjustWeight) {
		this.adjustWeight = adjustWeight;
	}

	public String getPhysicalUnit() {
		return physicalUnit;
	}

	public void setPhysicalUnit(String physicalUnit) {
		this.physicalUnit = physicalUnit;
	}

	public String getPriceUnit() {
		return priceUnit;
	}

	public void setPriceUnit(String priceUnit) {
		this.priceUnit = priceUnit;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getOwnBrand() {
		return ownBrand;
	}

	public void setOwnBrand(String ownBrand) {
		this.ownBrand = ownBrand;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Integer getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(Integer receiveTime) {
		this.receiveTime = receiveTime;
	}

	public Integer getInspectionResult() {
		return inspectionResult;
	}

	public void setInspectionResult(Integer inspectionResult) {
		this.inspectionResult = inspectionResult;
	}

	public Integer getInspectionTime() {
		return inspectionTime;
	}

	public void setInspectionTime(Integer inspectionTime) {
		this.inspectionTime = inspectionTime;
	}

	public Long getInspectorId() {
		return inspectorId;
	}

	public void setInspectorId(Long inspectorId) {
		this.inspectorId = inspectorId;
	}

	public String getInspectorName() {
		return inspectorName;
	}

	public void setInspectorName(String inspectorName) {
		this.inspectorName = inspectorName;
	}

	public Integer getReceiveCompleteTime() {
		return receiveCompleteTime;
	}

	public void setReceiveCompleteTime(Integer receiveCompleteTime) {
		this.receiveCompleteTime = receiveCompleteTime;
	}

	public Integer getIsWithPackage() {
		return isWithPackage;
	}

	public void setIsWithPackage(Integer isWithPackage) {
		this.isWithPackage = isWithPackage;
	}

	public Long getCreaterId() {
		return createrId;
	}

	public void setCreaterId(Long createrId) {
		this.createrId = createrId;
	}

	public String getCreaterName() {
		return createrName;
	}

	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}

	public Long getUpdaterId() {
		return updaterId;
	}

	public void setUpdaterId(Long updaterId) {
		this.updaterId = updaterId;
	}

	public String getUpdaterName() {
		return updaterName;
	}

	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName;
	}

	public Long getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public Integer getAppointArrivalTime() {
		return appointArrivalTime;
	}

	public void setAppointArrivalTime(Integer appointArrivalTime) {
		this.appointArrivalTime = appointArrivalTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getReceiveType() {
		return receiveType;
	}

	public void setReceiveType(Integer receiveType) {
		this.receiveType = receiveType;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public Integer getcT() {
		return cT;
	}

	public void setcT(Integer cT) {
		this.cT = cT;
	}

	public Integer getuT() {
		return uT;
	}

	public void setuT(Integer uT) {
		this.uT = uT;
	}

    public Integer getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Integer deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Integer getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(Integer purchaseType) {
        this.purchaseType = purchaseType;
    }

    public Integer getSupplyType() {
        return supplyType;
    }

    public void setSupplyType(Integer supplyType) {
        this.supplyType = supplyType;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

	public Integer getMainStatus() {
		return mainStatus;
	}

	public void setMainStatus(Integer mainStatus) {
		this.mainStatus = mainStatus;
	}

	public Integer getSkuType() {
		return skuType;
	}

	public void setSkuType(Integer skuType) {
		this.skuType = skuType;
	}

	public List<Integer> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<Integer> statusList) {
		this.statusList = statusList;
	}

	public List<Long> getWmsReceiveIds() {
		return wmsReceiveIds;
	}

	public void setWmsReceiveIds(List<Long> wmsReceiveIds) {
		this.wmsReceiveIds = wmsReceiveIds;
	}
}
