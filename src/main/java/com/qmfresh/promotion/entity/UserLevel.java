package com.qmfresh.promotion.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
@TableName("t_user_level")
@Data
@ToString
public class UserLevel implements Serializable {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
	@TableField("user_id")
	private Integer userId;
    /**
     * 0普通会员，1vip会员
     */
	private Integer level;
	@TableField("c_t")
	private Integer cT;
	@TableField("u_t")
	private Integer uT;
    /**
     * 等级名称
     */
	@TableField("level_name")
	private String levelName;
    /**
     * 0.年费会员1.充值会员
     */
	private Integer type;
    /**
     *  0可用1，不可用
     */
	private Integer status;
	@TableField("start_time")
	private Integer startTime;
	@TableField("end_time")
	private Integer endTime;


}
