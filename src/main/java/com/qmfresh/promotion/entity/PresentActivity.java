package com.qmfresh.promotion.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 赠品活动
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@TableName("t_present_activity")
@Data
@ToString
public class PresentActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 促销活动id
     */
    @TableField("activity_id")
    private Long activityId;

    /**
     * 赠品活动类型：1.促销活动，2.积分兑换活动
     */
    @TableField("activity_type")
    private Integer activityType;
    /**
     * 赠品活动名称
     */
    private String name;

    /**
     * 活动开始时间
     */
    @TableField("effect_begin_time")
    private Integer effectBeginTime;
    /**
     * 活动结束时间
     */
    @TableField("effect_end_time")
    private Integer effectEndTime;

    /**
     * 已赠送量
     */
    @TableField("send_num")
    private BigDecimal sendNum;
    /**
     * 状态:0.失效，1.新建，2.生效(已绑定活动)
     *
     * @see com.qmfresh.promotion.enums.PresentStatusEnums
     */
    private Integer status;
    /**
     * 创建人id
     */
    @TableField("create_user_id")
    private Long createUserId;
    /**
     * 创建人
     */
    @TableField("create_user_name")
    private String createUserName;
    @TableField("c_t")
    private Integer cT;
    @TableField("u_t")
    private Integer uT;
    @TableField("is_deleted")
    private Integer isDeleted;


}
