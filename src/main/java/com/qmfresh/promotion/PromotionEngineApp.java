package com.qmfresh.promotion;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@SpringBootApplication(scanBasePackages = {"com.qmfresh.promotion"})
@MapperScan(basePackages = {"com.qmfresh.promotion.mapper",
        "com.qmfresh.promotion.platform.infrastructure.mapper",

})
public class PromotionEngineApp {

    public static void main(String[] args) {
        SpringApplication.run(PromotionEngineApp.class);
    }

    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        paginationInterceptor.setLimit(-1);
        return paginationInterceptor;
    }
}
