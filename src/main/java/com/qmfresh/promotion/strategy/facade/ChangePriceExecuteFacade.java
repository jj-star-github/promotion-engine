package com.qmfresh.promotion.strategy.facade;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.strategy.bean.cp.StrategyParam;
import com.qmfresh.promotion.strategy.rule.cp.ChangePriceStrategyExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @version v1.0
 * @ProjectName: promotion-engine
 * @ClassName: ChangePriceExecuteFacade
 * @Description: TODO
 * @Author: xbb
 * @Date: 2020/3/8 10:08
 */
@Service
public class ChangePriceExecuteFacade {

    private static final Logger logger = LoggerFactory.getLogger(ChangePriceExecuteFacade.class);

    public void executeSendInfoStrategy(StrategyParam strategyParam){
        logger.info("ChangePriceExecuteFacade.executeSendInfoStrategy strategyParam:" + JSON.toJSONString(strategyParam));
        if(strategyParam == null || strategyParam.getPromotionParam() == null
                || strategyParam.getPromotionActivity() == null || StringUtils.isEmpty(strategyParam.getStrategyName())){
            logger.warn("入参信息不合法：",JSON.toJSONString(strategyParam));
            return;
        }

        ChangePriceStrategyExecutor executor = ChangePriceStrategyExecutor.getEnum(strategyParam.getStrategyName());
        if (executor == null) {
            throw new BusinessException("未获取到策略");
        }
        executor.execute(strategyParam.getPromotionParam(),strategyParam.getPromotionActivity());

    }
}
