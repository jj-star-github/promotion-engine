package com.qmfresh.promotion.strategy.facade;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.strategy.rule.create.CreatePromotionStrategyExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @ClassName CreatePromotionExecuteFacade
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/19 11:03
 */
@Service
@Slf4j
public class CreatePromotionExecuteFacade {


    public Boolean executeCreateStrategy(BaseRequestParam baseRequestParam){
        log.info("CreatePromotionExecuteFacade.executeCreateStrategy strategyParam:" + JSON.toJSONString(baseRequestParam));
        CreatePromotionStrategyExecutor executor = CreatePromotionStrategyExecutor.getEnum(baseRequestParam.getStrategyName());
        if (executor == null) {
            throw new BusinessException("未获取到策略");
        }
        return executor.execute(baseRequestParam);
    }
}
