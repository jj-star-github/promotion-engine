package com.qmfresh.promotion.strategy.facade;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qmfresh.promotion.bean.coupon.CouponQuery;
import com.qmfresh.promotion.bean.promotion.CartContext;
import com.qmfresh.promotion.bean.promotion.MzCampOn;
import com.qmfresh.promotion.bean.promotion.PromotionSsu;
import com.qmfresh.promotion.bean.promotion.QueryActivityParam;
import com.qmfresh.promotion.bean.sku.ProductSsuFormatDto;
import com.qmfresh.promotion.bean.sku.QueryProductSsuFormat;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.*;
import com.qmfresh.promotion.entity.*;
import com.qmfresh.promotion.enums.*;
import com.qmfresh.promotion.external.service.ICouponExternalApiService;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.manager.*;
import com.qmfresh.promotion.mapper.ActivityAreaScopeMapper;
import com.qmfresh.promotion.mapper.AreaScopeShopMapper;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.service.IPromotionActivityService;
import com.qmfresh.promotion.strategy.rule.StrategyExecutor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/5/27.
 *
 * @author wyh
 */
@Service
@Slf4j
public class PromotionExecuteFacade {
    private static final Logger logger = LoggerFactory.getLogger(PromotionExecuteFacade.class);

    private static final String FILTER_AREA_KEY = "PROMOTION-ENGINE:FILTER_AREA_KEY";

    @Resource
    private IPromotionActivityService promotionActivityService;
    @Resource
    private IPromotionRuleManager promotionRuleManager;
    @Resource
    private IPromotionModuleManager promotionModuleManager;
    @Resource
    private IAreaScopeShopManager areaScopeShopManager;
    @Resource
    private IActivityAreaScopeManager iActivityAreaScopeManager;
    @Resource
    private IAreaScopeCityManager iAreaScopeCityManager;
    @Resource
    private AreaScopeShopMapper areaScopeShopMapper;
    @Resource
    private ActivityAreaScopeMapper activityAreaScopeMapper;
    @Resource
    private IQmExternalApiService qmExternalApiService;
    @Resource
    private ICouponExternalApiService couponExternalApiService;
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    private static final Integer CP_MODULE_ID = 6;


    public List<PromotionMzCampOnRsp> mzPromotionCampOn(PromotionMzCampOnReq protocol){
        //根据活动ID查询活动信息
        List<MzCampOn> mzCampOnList = protocol.getCampOnList();
        List<PromotionMzCampOnRsp> rsp = new ArrayList<>();
        mzCampOnList.forEach(mzCampOn -> {
            if (null == mzCampOn.getActivityId() || null == mzCampOn.getCouponId() || null == mzCampOn.getGivingNum()) {
                throw new BusinessException("预占信息有误");
            }
            //获取优惠券码
            CouponQuery couponQuery = new CouponQuery();
            couponQuery.setActivityId(mzCampOn.getCouponId());
            couponQuery.setSourceShopId(protocol.getShopId());
            couponQuery.setSendNum(mzCampOn.getGivingNum());
            PromotionMzCampOnRsp couponCodeBean = couponExternalApiService.queryCouponInfo(couponQuery);
            if (null == couponCodeBean) {
                throw new BusinessException("优惠券信息调用异常");
            }
            couponCodeBean.setActivityId(mzCampOn.getActivityId());
            rsp.add(couponCodeBean);
        });
        return rsp;
    }

    public PromotionContext executeStrategy(PromotionProtocol protocol) {
        logger.info("开始执行PromotionExecuteFacade.executeStrategy protocal:" + JSON.toJSONString(protocol));
        PromotionContext ctx = null;
        if (protocol.getChannel().equals(ChannelTypeEnums.C_OFFLINE.getCode())) {
            protocol.setPromotionSsuList(this.mergeOrder(protocol.getUser(), protocol.getPromotionSsuList()));
            ctx = this.initPromotionContext(protocol);
        } else {
            ctx = this.initCmallPromotionContext(protocol);
        }
        logger.info("入参初始化 protocal:" + JSON.toJSONString(protocol));

        // 根据渠道获取当前生效促销活动
        List<PromotionActivity> promotionActivities = this.getCurrentActivities(protocol.getChannel());
        if (ListUtil.isNullOrEmpty(promotionActivities)) {
            logger.info("未查询到促销活动");
            return ctx;
        }

        List<PromotionRule> rules = this.getPromotionRule(promotionActivities.stream().map(PromotionActivity::getId).collect(Collectors.toList()));

        List<PromotionRule> promotionRules = this.filter(rules, protocol);
        logger.info("过滤后促销规则promotionRules:" + JSON.toJSONString(promotionRules));
        if (ListUtil.isNullOrEmpty(promotionRules)) {
            logger.info("过滤后促销规则为空:" + JSON.toJSONString(protocol));
            return ctx;
        }

        List<Integer> moduleIds = promotionRules.stream().map(PromotionRule::getPromotionModuleId).distinct().collect(Collectors.toList());
        if (CollectionUtils.isEmpty(moduleIds)) {
            logger.info("过滤后促销规则的活动模型空" + JSON.toJSONString(moduleIds));
            return ctx;
        }
        List<Integer> listExcludeCpModuleId = moduleIds.stream().filter(o -> !o.equals(CP_MODULE_ID)).collect(Collectors.toList());
        List<PromotionModule> promotionModules = promotionModuleManager.promotionModule(listExcludeCpModuleId);

        Map<String, PromotionModule> moduleMap = ListUtil.listToMap(promotionModules);

        Map<Integer, List<PromotionRule>> ruleMap = promotionRules.stream().collect(Collectors.groupingBy(PromotionRule::getPromotionModuleId));
        // 促销执行顺序按priority排序
        promotionModules.sort(Comparator.comparing(PromotionModule::getPriority).reversed());
        for (PromotionModule promotionModule : promotionModules) {
            if (ListUtil.isNullOrEmpty(ruleMap.get(promotionModule.getId().intValue()))) {
                logger.info("活动模型-没有对应的互动规则" + promotionModule.getId());
                continue;
            }

            // 顺序执行促销
            List<PromotionRule> promotionRuleList = ruleMap.get(promotionModule.getId().intValue());
            logger.info("ruleMap:{}", JSON.toJSONString(ruleMap));
            StrategyExecutor executor = StrategyExecutor.getEnum(moduleMap.get(promotionModule.getId().toString()).getContext());
            if (executor == null) {
                logger.info("未获取到策略");
                throw new BusinessException(JSON.toJSONString(protocol) + "未获取到策略");
            }
            CartContext cartContext = new CartContext();
            cartContext.setShopId(protocol.getArea().getShopId());
            cartContext.setChannel(protocol.getChannel());
            cartContext.setUserId(protocol.getUser().getUserId().longValue());
            cartContext.setPromotionSsus(protocol.getPromotionSsuList());
            cartContext.setUserLevel(protocol.getUser().getLevel());
            executor.execute(promotionRuleList, cartContext, ctx);
        }
        return ctx;
    }

    /**
     * 获取当前生效状态促销活动
     *
     * @return
     */
    private List<PromotionActivity> getCurrentActivities(Integer channel) {
        QueryActivityParam queryActivityParam = new QueryActivityParam();
        queryActivityParam.setEffectBeginTime(DateUtil.getCurrentTimeIntValue());
        queryActivityParam.setEffectEndTime(DateUtil.getCurrentTimeIntValue());
        queryActivityParam.setIsOnline(ActivityOnlineTypeEnums.ONLINE.getCode());
        queryActivityParam.setStatus(ActivityStatusTypeEnums.STARTED.getCode());
        queryActivityParam.setChannel(channel);
        return promotionActivityService.queryByCondition(queryActivityParam);
    }

    private List<PromotionRule> getPromotionRule(List<Long> activityIds) {
        return promotionRuleManager.list(new QueryWrapper<PromotionRule>()
                .eq("is_online", 1)
                .eq("is_deleted", 0)
                .in("activity_id", activityIds));


    }

    private List<PromotionRule> filter(List<PromotionRule> rules, PromotionProtocol protocol) {
        return this.filterArea(rules, protocol);
    }

    private List<PromotionRule> filterArea(List<PromotionRule> rules, PromotionProtocol protocol) {
        return rules.stream().filter(item -> filterArea(item, protocol)).collect(Collectors.toList());
    }

    private boolean filterArea(PromotionRule rule, PromotionProtocol protocol) {
        AreaScopeTypeEnums areaScope = AreaScopeTypeEnums.getEnum(rule.getAreaScope());
        if (areaScope == null) {
            throw new BusinessException("区域范围类型异常,rule:" + JSON.toJSONString(rule));
        }

        /**
         * 获取活动区域信息
         */
        ActivityAreaScope activityAreaScope = iActivityAreaScopeManager.activityAreaScope(rule.getActivityId());

        switch (areaScope) {
            case ALL:
                return true;
            case CITY:
                List<AreaScopeCity> areaScopeCities = iAreaScopeCityManager.areaScopeCity(activityAreaScope.getId());
                if (ListUtil.isNullOrEmpty(areaScopeCities)) {
                    logger.info("未获取到促销城市列表,area_scope_id:" + activityAreaScope.getId());
                    return false;
                }
                Optional<AreaScopeCity> optionalAreaScopeCity = areaScopeCities.stream().filter(item -> item.getCityId().equals(protocol.getArea().getCityId())).findFirst();
                if (optionalAreaScopeCity.isPresent()) {
                    AreaScopeCity areaScopeCity = optionalAreaScopeCity.get();
                    if (areaScopeCity.getExShop() != null && !areaScopeCity.getExShop().isEmpty()) {
                        List<Integer> exShops = ListUtil.stringToIntegerList(areaScopeCity.getExShop());
                        if (exShops.contains(protocol.getArea().getShopId())) {
                            // 促销城市不包含当前门店
                            return false;
                        }
                    }
                } else {
                    // 门店所在城市不在促销城市范围内
                    return false;
                }
                return true;
            case SHOP:
                String result = (String) redisTemplate.opsForHash().get(FILTER_AREA_KEY, activityAreaScope.getId() + "");
                List<AreaScopeShop> areaScopeShops;
                if (StringUtils.isEmpty(result)) {
                    logger.info("FILTER_AREA_KEY 没有缓存 {}", activityAreaScope.getId());
                    areaScopeShops = putIfNotExist(activityAreaScope.getId());
                } else {
                    logger.info("有缓存：FILTER_AREA_KEY {}", activityAreaScope.getId());
                    areaScopeShops = JSON.parseObject(result, new TypeReference<List<AreaScopeShop>>() {
                    });
                }
                Optional<AreaScopeShop> optionalAreaScopeShop = areaScopeShops.stream().filter(item -> item.getShopId().equals(protocol.getArea().getShopId())).findFirst();
                return optionalAreaScopeShop.isPresent();
            default:
                return false;
        }
    }

    /**
     * 根据scopeId获取门店
     *
     * @param scopeId 区域id
     * @return
     */
    private List<AreaScopeShop> putIfNotExist(Long scopeId) {
        List<AreaScopeShop> scopeShops = areaScopeShopManager.areaScope(scopeId);
        redisTemplate.opsForHash().put(FILTER_AREA_KEY, scopeId + "", JSON.toJSONString(scopeShops));
        return scopeShops;
    }


    public List<PromotionSsu> mergeOrder(User user, List<PromotionSsu> cartSsu) {
        List<PromotionSsu> mergedPromotionSsu = new ArrayList<>(cartSsu.size());

        Map<Integer, List<PromotionSsu>> skuCartMap = cartSsu.stream().collect(Collectors.groupingBy(PromotionSsu::getSkuId));
        for (Map.Entry<Integer, List<PromotionSsu>> entry : skuCartMap.entrySet()) {
            List<PromotionSsu> promotionSsus = entry.getValue();
            PromotionSsu basePromotionSsu = new PromotionSsu();
            BeanUtils.copyProperties(promotionSsus.get(0), basePromotionSsu);
            basePromotionSsu.setAmount(BigDecimal.ZERO);
            mergedPromotionSsu.addAll(user.getUserId() == 0 ? this.mergeNormalPromotionSsu(basePromotionSsu, entry.getValue()) : this.mergePromotionSsu(basePromotionSsu,
                    entry.getValue()));
        }
        return mergedPromotionSsu;
    }

    /**
     * 非会员，合并普通商品和改价商品
     *
     * @param basePromotionSsu
     * @param promotionSsus
     * @return
     */
    private List<PromotionSsu> mergeNormalPromotionSsu(PromotionSsu basePromotionSsu,
                                                       List<PromotionSsu> promotionSsus) {
        List<PromotionSsu> psList = new ArrayList<>(promotionSsus.size());
        PromotionSsu normalPromotionSsu = new PromotionSsu();
        BeanUtils.copyProperties(basePromotionSsu, normalPromotionSsu);
        for (PromotionSsu cs : promotionSsus) {
            //验证价格是否异常
            if (cs.getOriginPrice().compareTo(BigDecimal.ZERO) <= 0 && cs.getPriceAfterDiscount().compareTo(BigDecimal.ZERO) <= 0) {
                log.warn("商品价格异常 sku={}", JSON.toJSONString(cs));
                throw new BusinessException("商品价格异常");
            }
            //获取非0 的最小价格
            if(cs.getOriginPrice().compareTo(BigDecimal.ZERO) <= 0) {
                cs.setPriceAfterDiscount(cs.getPriceAfterDiscount());
            } else if (cs.getPriceAfterDiscount().compareTo(BigDecimal.ZERO) <= 0) {
                cs.setPriceAfterDiscount(cs.getOriginPrice());
            } else {
                //计算最小价格
                cs.setPriceAfterDiscount(cs.getOriginPrice().compareTo(cs.getPriceAfterDiscount()) <= 0 ? cs.getOriginPrice() : cs.getPriceAfterDiscount());
            }

            // 合并原价商品
            if (cs.getPriceAfterDiscount().compareTo(cs.getOriginPrice()) == 0) {
                normalPromotionSsu.setAmount(normalPromotionSsu.getAmount().add(cs.getAmount()));
                normalPromotionSsu.setPriceType(PriceTypeEnums.NORMAL.getCode());
                normalPromotionSsu.setPriceAfterDiscount(cs.getPriceAfterDiscount());
                continue;
            }

            cs.setPriceType(PriceTypeEnums.MODIFY.getCode());
            psList.add(cs);
        }

        if (normalPromotionSsu.getAmount().compareTo(BigDecimal.ZERO) > 0) {
            psList.add(normalPromotionSsu);
        }
        return psList;
    }

    /**
     * 会员合并普通商品、会员商品、改价商品
     *
     * @param basePromotionSsu
     * @param promotionSsus
     * @return
     */
    private List<PromotionSsu> mergePromotionSsu(PromotionSsu basePromotionSsu, List<PromotionSsu> promotionSsus) {
        List<PromotionSsu> psList = new ArrayList<>(promotionSsus.size());
        PromotionSsu vipPromotionSsu = new PromotionSsu();
        PromotionSsu normalPromotionSsu = new PromotionSsu();
        BeanUtils.copyProperties(basePromotionSsu, vipPromotionSsu);
        BeanUtils.copyProperties(basePromotionSsu, normalPromotionSsu);
        for (PromotionSsu cs : promotionSsus) {
            //验证价格是否异常
            if (cs.getOriginPrice().compareTo(BigDecimal.ZERO) <= 0 && cs.getPriceAfterDiscount().compareTo(BigDecimal.ZERO) <= 0
                    && cs.getVipPrice().compareTo(BigDecimal.ZERO) <= 0) {
                log.warn("商品价格异常 sku={}", JSON.toJSONString(cs));
                throw new BusinessException("商品价格异常");
            }
            //获取非0 的最小价格
            BigDecimal price = BigDecimal.ZERO;
            if (cs.getOriginPrice().compareTo(BigDecimal.ZERO) > 0) {
                price = cs.getOriginPrice();
            }
            if (cs.getVipPrice().compareTo(BigDecimal.ZERO) > 0) {
                price = price.compareTo(BigDecimal.ZERO) == 0 ? cs.getVipPrice() : price.compareTo(cs.getVipPrice()) <= 0 ? price : cs.getVipPrice();
            }
            if (cs.getPriceAfterDiscount().compareTo(BigDecimal.ZERO) > 0) {
                price = price.compareTo(BigDecimal.ZERO) == 0 ? cs.getPriceAfterDiscount() : price.compareTo(cs.getPriceAfterDiscount()) <= 0 ? price : cs.getPriceAfterDiscount();
            }

            //验证是否为原价商品
            if(price.compareTo(cs.getOriginPrice()) == 0) {
                normalPromotionSsu.setAmount(normalPromotionSsu.getAmount().add(cs.getAmount()));
                normalPromotionSsu.setPriceType(PriceTypeEnums.NORMAL.getCode());
                normalPromotionSsu.setPriceAfterDiscount(price);
                continue;
            }

            //验证是否为会员价
            if(price.compareTo(cs.getVipPrice()) == 0) {
                vipPromotionSsu.setAmount(vipPromotionSsu.getAmount().add(cs.getAmount()));
                vipPromotionSsu.setPriceType(PriceTypeEnums.VIP.getCode());
                vipPromotionSsu.setPriceAfterDiscount(price);
                continue;
            }

            cs.setPriceType(PriceTypeEnums.MODIFY.getCode());
            cs.setPriceAfterDiscount(price);
            psList.add(cs);
            continue;
        }
        if (vipPromotionSsu.getAmount().compareTo(BigDecimal.ZERO) > 0) {
            psList.add(vipPromotionSsu);
        }

        if (normalPromotionSsu.getAmount().compareTo(BigDecimal.ZERO) > 0) {
            psList.add(normalPromotionSsu);
        }
        return psList;
    }

    public PromotionContext initCmallPromotionContext(PromotionProtocol protocol) {
        PromotionContext promotionContext = new PromotionContext();
        List<PromotionSsuContext> promotionSsuContexts = new ArrayList<>(protocol.getPromotionSsuList().size());
        int labelId = 1;
        for (PromotionSsu promotionSsu : protocol.getPromotionSsuList()) {
            promotionSsu.setLabelId(labelId);
            PromotionSsuContext psc = new PromotionSsuContext();
            psc.setLabelId(labelId);
            psc.setMeet(false);
            psc.setSsuFormatId(promotionSsu.getSsuFormatId());
            psc.setSkuId(promotionSsu.getSkuId());
            psc.setClass1Id(promotionSsu.getClass1Id());
            psc.setClass2Id(promotionSsu.getClass2Id());
            //            psc.setSsuName(promotionSsu());
            psc.setAmount(promotionSsu.getAmount());
            psc.setOriginPrice(promotionSsu.getOriginPrice());
            psc.setVipPrice(promotionSsu.getVipPrice());
            psc.setPriceAfterDiscount(promotionSsu.getPriceAfterDiscount());
            psc.setPreferentialPrice(promotionSsu.getPriceAfterDiscount());
            psc.setPriceType(promotionSsu.getPriceType());
            promotionSsuContexts.add(psc);
            labelId++;
        }
        promotionContext.setSsuContexts(promotionSsuContexts);
        promotionContext.setMzContexts(new ArrayList<>(16));
        promotionContext.setMjContexts(new ArrayList<>(16));
        promotionContext.setBzContexts(new ArrayList<>(16));
        return promotionContext;
    }

    public PromotionContext initPromotionContext(PromotionProtocol protocol) {
        PromotionContext promotionContext = new PromotionContext();
        QueryProductSsuFormat queryProductSsuFormat = new QueryProductSsuFormat();
        queryProductSsuFormat.setSkuIds(protocol.getPromotionSsuList().stream().map(PromotionSsu::getSkuId).distinct().collect(Collectors.toList()));
        queryProductSsuFormat.setPlatform(2);
        queryProductSsuFormat.setSellChannel(2);
        queryProductSsuFormat.setSsuFp(1);
        List<ProductSsuFormatDto> productSsuFormatDtos = qmExternalApiService.getSsuFormatByCondition(queryProductSsuFormat);
        Map<String, ProductSsuFormatDto> productSsuFormatMap = ListUtil.listToMap("skuId", productSsuFormatDtos);

        List<PromotionSsuContext> promotionSsuContexts = new ArrayList<>(protocol.getPromotionSsuList().size());
        int labelId = 1;
        for (PromotionSsu promotionSsu : protocol.getPromotionSsuList()) {
            promotionSsu.setLabelId(labelId);
            PromotionSsuContext psc = new PromotionSsuContext();
            ProductSsuFormatDto productSsuFormatDto = productSsuFormatMap.get(promotionSsu.getSkuId().toString());
            psc.setLabelId(labelId);
            psc.setMeet(false);
            promotionSsu.setSsuFormatId(productSsuFormatDto.getSsuFormatId());
            psc.setSsuFormatId(productSsuFormatDto.getSsuFormatId());
            psc.setSkuId(promotionSsu.getSkuId());
            psc.setClass1Id(productSsuFormatDto.getClass1Id());
            psc.setClass2Id(productSsuFormatDto.getClass2Id());
            psc.setSsuName(productSsuFormatDto.getSsuName());
            psc.setAmount(promotionSsu.getAmount());
            psc.setOriginPrice(promotionSsu.getOriginPrice());
            psc.setVipPrice(promotionSsu.getVipPrice());
            psc.setPriceAfterDiscount(promotionSsu.getPriceAfterDiscount());
            psc.setPreferentialPrice(promotionSsu.getPriceAfterDiscount());
            psc.setPriceType(promotionSsu.getPriceType());
            promotionSsuContexts.add(psc);
            labelId++;
        }
        promotionContext.setSsuContexts(promotionSsuContexts);
        promotionContext.setMzContexts(new ArrayList<>(16));
        promotionContext.setMjContexts(new ArrayList<>(16));
        promotionContext.setBzContexts(new ArrayList<>(16));
        return promotionContext;
    }

    /**
     * 全局刷新缓存
     *
     * @param refreshAll 刷新
     */
    public void initFilterAreaCache(boolean refreshAll, Long id) {
        LambdaQueryWrapper<AreaScopeShop> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(AreaScopeShop::getIsDeleted, 0);
        int pageNum = 1;
        int pageSize = 1000;
        logger.info("缓存数据开始");

        //单个刷新
        if (!refreshAll) {
            if (id == null) {
                logger.warn("必须设置id");
                return;
            }
            ActivityAreaScope activityAreaScope = activityAreaScopeMapper.selectById(id);
            if (activityAreaScope == null) {
                logger.info("门店数据不存在 {}", id);
                return;
            }
            initActivityAreaScope(activityAreaScope);
            return;
        }

        //todo check this code
//        List<ActivityAreaScope> list = activityAreaScopeMapper.selectPage(new RowBounds((pageNum - 1) * pageSize, pageSize), wrapper);
//        while (CollectionUtils.isNotEmpty(list)) {
//            for (ActivityAreaScope a : list) {
//                initActivityAreaScope(a);
//            }
//            pageNum = pageNum + 1;
//            logger.info("成功缓存数据：{}", list.size());
//            list = activityAreaScopeMapper.selectPage(new RowBounds((pageNum - 1) * pageSize, pageSize), wrapper);
//            try {
//                Thread.sleep(10);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

        logger.info("缓存数据结束");

    }

    private void initActivityAreaScope(ActivityAreaScope a) {
        LambdaQueryWrapper<AreaScopeShop> tmp = new LambdaQueryWrapper<>();
        tmp.eq(AreaScopeShop::getIsDeleted, 0);
        tmp.eq(AreaScopeShop::getAreaScopeId, a.getId());
        List<AreaScopeShop> areaScopeShopList = areaScopeShopMapper.selectList(tmp);
        if (CollectionUtils.isEmpty(areaScopeShopList)) {
            logger.info("没有找到门店区域 {}", a.getId());
        } else {
            redisTemplate.opsForHash().put(FILTER_AREA_KEY, a.getId() + "", JSON.toJSONString(areaScopeShopList));
            logger.info("缓存数据：{}", a.getId());
        }
    }

    public String get(Long id) {
        if (id == null) {
            return "";
        }
        logger.info("查询缓存中的数据：{}", id);
        String result = (String) redisTemplate.opsForHash().get(FILTER_AREA_KEY, id + "");
        return result;
    }
}
