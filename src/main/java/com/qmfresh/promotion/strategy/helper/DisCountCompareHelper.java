package com.qmfresh.promotion.strategy.helper;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.common.exception.BusinessException;
import com.qmfresh.promotion.entity.ActivityClass2;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.PromotionTypeEnums;
import com.qmfresh.promotion.strategy.bean.BaseRule;

/**
 * Created by wyh on 2019/7/22.
 *
 * @author wyh
 */
public class DisCountCompareHelper {

    // 待创建促销规则
    private PromotionTypeEnums createType;

    // 比对促销规则
    private PromotionTypeEnums compareType;

    private BaseRule createRule;

    private BaseRule conflictRule;

    private Long confilctActivityId;

    public DisCountCompareHelper(PromotionTypeEnums createType, BaseRule createBaseRule,
        PromotionRule comparePromotionRule) {
        PromotionTypeEnums compareType = PromotionTypeEnums.getEnum(comparePromotionRule.getPromotionModuleId());
        if (createType == null || compareType == null) {
            throw new BusinessException("获取促销类型异常");
        }
        this.createType = createType;
        this.compareType = compareType;
        this.createRule = createBaseRule;
        this.conflictRule = JSON.parseObject(comparePromotionRule.getPromotionRule(), new TypeReference<BaseRule>() {
        });
        this.confilctActivityId = comparePromotionRule.getActivityId();
    }

    public void compareClassDiscount(Integer class2Id) {
        String errorDescription = this.compareDiscount();
        if (errorDescription.length() > 0) {
            throw new BusinessException("待创建分类:" + class2Id + "--" + errorDescription);
        }
    }

    public void compareSsuDiscount(Integer ssuFormatId) {
        String errorDescription = this.compareDiscount();
        if (errorDescription.length() > 0) {
            throw new BusinessException("待创建ssuFormatId:" + ssuFormatId + "--" + errorDescription);
        }
    }

    /**
     * 比较促销折扣力度
     *
     * @return 比较折扣力度，如果实际折扣力度与默认折扣力度不符，返回错误描述
     */
    public String compareDiscount() {
        String errorDescription = "";
        if (!conflictRule.getDiscountRule().getType().equals(createRule.getDiscountRule().getType())) {
            errorDescription = errorDescription.concat("折扣类型不一致");
        }

        if (compareDiscountLevel(createType, compareType) && createRule.getDiscountRule().getDiscount().compareTo(conflictRule.getDiscountRule().getDiscount()) < 0) {
            errorDescription = errorDescription.concat(createType.getDetail() + "折扣力度不能小于" + compareType.getDetail() + ",冲突活动id:" + confilctActivityId);
            return errorDescription;
        }
        if (!compareDiscountLevel(createType, compareType) && createRule.getDiscountRule().getDiscount().compareTo(conflictRule.getDiscountRule().getDiscount()) > 0) {
            errorDescription = errorDescription.concat(createType.getDetail() + "折扣力度不能大于" + compareType.getDetail() + ",冲突活动id:" + confilctActivityId);
            return errorDescription;
        }
        return errorDescription;
    }

    /**
     * 获取折扣力度，折扣力度大返回true，反之false
     *
     * @param createType 待创建促销类型
     * @param compareType 已创建促销类型
     * @return
     */
    private static boolean compareDiscountLevel(PromotionTypeEnums createType, PromotionTypeEnums compareType) {
        switch (createType) {
            case VIP:
                if (compareType.equals(PromotionTypeEnums.DP)) {
                    return true;
                }
            case DP:
                if (compareType.equals(PromotionTypeEnums.VIP)) {
                    return false;
                }
            default:
                return true;
        }
    }
}
