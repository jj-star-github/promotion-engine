package com.qmfresh.promotion.strategy.helper;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.enums.DiscountTypeEnums;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.strategy.bean.DiscountRule;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by wyh on 2019/6/16.
 *
 * @author wyh
 */
public class DiscountPriceHelper {

    /**
     * 计算折后价
     *
     * @param price
     * @param discountRule
     * @return
     */
    public static BigDecimal getDiscountPrice(BigDecimal price, DiscountRule discountRule) {
        DiscountTypeEnums discountType = DiscountTypeEnums.getEnum(discountRule.getType());
        if (discountType == null) {
            throw new BusinessException("折扣类型不存在,discountRule:" + JSON.toJSONString(discountRule));
        }
        BigDecimal discountPrice = BigDecimal.ZERO;
        switch (discountType) {
            case FIXED:
                discountPrice = discountRule.getDiscount();
                break;
            case DISCOUNT:
                discountPrice = price.multiply(discountRule.getDiscount().divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP));
                break;
            case UNITPRICE:
                discountPrice = price.subtract(discountRule.getDiscount());
                break;
            default:
        }
        return discountPrice.setScale(2, RoundingMode.HALF_UP);
    }

    public static String getDiscountRuleDescription(DiscountRule discountRule) {
        DiscountTypeEnums discountType = DiscountTypeEnums.getEnum(discountRule.getType());
        if (discountType == null) {
            throw new BusinessException("折扣类型不存在,discountRule:" + JSON.toJSONString(discountRule));
        }

        return discountType.getDetail().concat(discountRule.getDiscount().toString());
    }
}
