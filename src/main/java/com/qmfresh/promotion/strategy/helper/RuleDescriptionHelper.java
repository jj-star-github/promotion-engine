package com.qmfresh.promotion.strategy.helper;

import com.qmfresh.promotion.enums.MJTypeEnums;
import com.qmfresh.promotion.enums.MZTypeEnums;
import com.qmfresh.promotion.enums.PresentTypeEnums;
import com.qmfresh.promotion.enums.PromotionTypeEnums;
import com.qmfresh.promotion.strategy.bean.BzRule;
import com.qmfresh.promotion.strategy.bean.DpRule;
import com.qmfresh.promotion.strategy.bean.MjRule;
import com.qmfresh.promotion.strategy.bean.MjRuleInfo;
import com.qmfresh.promotion.strategy.bean.MzRule;
import com.qmfresh.promotion.strategy.bean.MzRuleInfo;
import com.qmfresh.promotion.strategy.bean.VipRule;

/**
 * Created by wyh on 2019/6/16.
 *
 * @author wyh
 */
public class RuleDescriptionHelper {

    public static String getDescription(PromotionTypeEnums promotionType, Object o) {
        switch (promotionType) {
            case VIP:
                return getVipRuleDescription((VipRule) o);
            case BZ:
                return getBzRuleDescription((BzRule) o);
            case DP:
                return getDpRuleDescription((DpRule) o);
            case MZ:
                return getMzRuleDescription((MzRule) o);
            case MJ:
                return getMjRuleDescription((MjRule) o);
            default:
                return "";
        }
    }

    public static String getVipRuleDescription(VipRule vipRule) {
        return DiscountPriceHelper.getDiscountRuleDescription(vipRule.getDiscountRule())
            .concat("\n" + LimitRuleHelper.getLimitRuleDescription(vipRule.getLimitRule()));
    }

    public static String getBzRuleDescription(BzRule bzRule) {
        return "";
    }

    public static String getDpRuleDescription(DpRule dpRule) {
        return DiscountPriceHelper.getDiscountRuleDescription(dpRule.getDiscountRule())
            .concat("\n" + LimitRuleHelper.getLimitRuleDescription(dpRule.getLimitRule()));
    }

    public static String getMzRuleDescription(MzRule mzRule) {
        String description = "";
        if (mzRule.getType().equals(MZTypeEnums.SATISFY_SKIP_LATER_CHECK.getCode())) {
            description = description.concat("每");
        }
        for (MzRuleInfo rule : mzRule.getMzRuleInfoList()) {
            String eachRule = "";
            if (rule.getPresentType().equals(PresentTypeEnums.PRODUCT.getCode())) {
                eachRule = "满" + rule.getMeetMoney() + "赠" + rule.getSsuName();
                description = description.concat(eachRule + "\n");
            }
            if (rule.getPresentType().equals(PresentTypeEnums.COUPON.getCode())) {
                eachRule = "满" + rule.getMeetMoney() + "赠" + rule.getCouponName();
                description = description.concat(eachRule + "\n");
            }
            rule.setDescription(eachRule);
        }
        return description;
    }

    public static String getMjRuleDescription(MjRule mjRule) {
        String description = "";
        if (mjRule.getType().equals(MJTypeEnums.SATISFY_SKIP_LATER_CHECK.getCode())) {
            //            description = description.concat(MZTypeEnums.MUST_SATISFY.getDetail() + ":");
            description = description.concat("每");
        }
        for (MjRuleInfo rule : mjRule.getMjRuleInfoList()) {
            String eachRule = "满" + rule.getMeetMoney() + "减" + rule.getReductionMoney();
            description = description.concat(eachRule + "\n");
            //            description = description.concat("\n" + "满" + rule.getMeetMoney() + "减" + rule.getReductionMoney());
            rule.setDescription(eachRule);
        }
        return description;
    }
}
