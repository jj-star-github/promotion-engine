package com.qmfresh.promotion.strategy.helper;

import com.qmfresh.promotion.bean.sku.ProductSsuFormatDto;
import com.qmfresh.promotion.cache.service.LCPromotionActivityService;
import com.qmfresh.promotion.cache.service.LCPromotionRuleService;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.PromotionClassDto;
import com.qmfresh.promotion.dto.PromotionProductDto;
import com.qmfresh.promotion.entity.ActivityClass2;
import com.qmfresh.promotion.entity.ActivitySsu;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.ProductScopeTypeEnums;
import com.qmfresh.promotion.enums.PromotionTypeEnums;
import com.qmfresh.promotion.enums.SsuTagTypeEnums;
import com.qmfresh.promotion.manager.IActivityClass2Manager;
import com.qmfresh.promotion.manager.IActivitySsuManager;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.strategy.bean.BaseRule;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/7/24.
 *
 * @author wyh
 */
@Component
public class MutexCheckHelper {

    @Resource
    private LCPromotionActivityService lcPromotionActivityService;
    @Resource
    private LCPromotionRuleService lcPromotionRuleService;
    @Resource
    private IActivityClass2Manager activityClass2Manager;
    @Resource
    private IActivitySsuManager activitySsuManager;

    /**
     * 商品分类互斥校验
     *
     * @param activityIds         已存在活动id
     * @param promotionProductDto 待创建商品分类数据
     */
    public void mutexCheck(List<Long> activityIds, PromotionTypeEnums createType,
                           PromotionProductDto promotionProductDto, BaseRule baseRule) {
        List<ActivityClass2> activityClass2s = activityClass2Manager.activityClass2(activityIds);
        List<ActivitySsu> activitySsus = activitySsuManager.activitySsu(activityIds);

        if (promotionProductDto.getType().equals(ProductScopeTypeEnums.CLASS2.getCode())) {
            this.classMutexCheck(activityClass2s, activitySsus, createType, promotionProductDto.getPromotionClassDto(), baseRule);
        }
        if (promotionProductDto.getType().equals(ProductScopeTypeEnums.SSU.getCode())) {
            this.ssuMutexCheck(activityClass2s, activitySsus, createType, promotionProductDto.getProductSsuFormatDtos(), baseRule);
        }
    }

    /**
     * 分类互斥校验（比对创建分类与已存在活动分类、ssu分类）
     *
     * @param activityClass2s   已存在活动分类
     * @param activitySsus      已存在ssu分类
     * @param createType        待创建促销类型
     * @param promotionClassDto 待创建分类
     * @param baseRule          待创建规则
     */
    private void classMutexCheck(List<ActivityClass2> activityClass2s, List<ActivitySsu> activitySsus,
                                 PromotionTypeEnums createType, PromotionClassDto promotionClassDto, BaseRule baseRule) {
        List<ActivitySsu> containActivitySsus = null;
        if (!ListUtil.isNullOrEmpty(activitySsus)) {
            Map<Integer, List<ActivitySsu>> activitySsuTag = activitySsus.stream().collect(Collectors.groupingBy(ActivitySsu::getExTag));
            containActivitySsus = activitySsuTag.getOrDefault(SsuTagTypeEnums.CONTAIN.getCode(), new ArrayList<>(16));
        }

        if (!ListUtil.isNullOrEmpty(activityClass2s)) {
            // 存在相同分类促销活动
            List<ActivityClass2> filterClass2 = activityClass2s.stream().filter(item -> promotionClassDto.getClass2Ids().contains(item.getClassId())).collect(Collectors.toList());
            if (!ListUtil.isNullOrEmpty(filterClass2)) {
                PromotionActivity promotionActivity = lcPromotionActivityService.get(filterClass2.get(0).getActivityId());
                PromotionRule conflictPromotionRule = lcPromotionRuleService.get(filterClass2.get(0).getActivityId());
                PromotionTypeEnums promotionType = PromotionTypeEnums.getEnum(promotionActivity.getPromotionModuleId());
                if (promotionType == null) {
                    throw new BusinessException("未获取到促销类型");
                }

                // 比较折扣方式
                DisCountCompareHelper disCountCompareHelper = new DisCountCompareHelper(createType, baseRule, conflictPromotionRule);
                disCountCompareHelper.compareClassDiscount(filterClass2.get(0).getClassId());
            }
        }

        if (!ListUtil.isNullOrEmpty(containActivitySsus)) {
            List<ActivitySsu> filterActivitySsuClass2 = containActivitySsus.stream().filter(item -> promotionClassDto.getClass2Ids().contains(item.getClass2Id())).collect(Collectors.toList());

            PromotionActivity promotionActivity = lcPromotionActivityService.get(filterActivitySsuClass2.get(0).getActivityId());
            PromotionRule conflictPromotionRule = lcPromotionRuleService.get(filterActivitySsuClass2.get(0).getActivityId());
            PromotionTypeEnums promotionType = PromotionTypeEnums.getEnum(promotionActivity.getPromotionModuleId());
            if (promotionType == null) {
                throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("未获取到促销类型");
            }

            // 比较折扣方式
            DisCountCompareHelper disCountCompareHelper = new DisCountCompareHelper(createType, baseRule, conflictPromotionRule);
            disCountCompareHelper.compareClassDiscount(filterActivitySsuClass2.get(0).getClass2Id());
        }

    }

    /**
     * 商品互斥校验（比对创建ssu与已存在活动商品、分类下商品）
     *
     * @param activityClass2s      已存在活动分类
     * @param activitySsus         已存在ssu分类
     * @param createType           待创建促销类型
     * @param productSsuFormatDtos 待创建ssu
     * @param baseRule             待创建规则
     */
    private void ssuMutexCheck(List<ActivityClass2> activityClass2s, List<ActivitySsu> activitySsus,
                               PromotionTypeEnums createType, List<ProductSsuFormatDto> productSsuFormatDtos, BaseRule baseRule) {
        Map<Integer, List<ActivitySsu>> activitySsuTag = activitySsus.stream().collect(Collectors.groupingBy(ActivitySsu::getExTag));
        List<ActivitySsu> containActivitySsus = activitySsuTag.get(SsuTagTypeEnums.CONTAIN.getCode());
        if (!ListUtil.isNullOrEmpty(containActivitySsus)) {
            List<Integer> ssuFormatIds = productSsuFormatDtos.stream().map(ProductSsuFormatDto::getSsuFormatId).collect(Collectors.toList());
            List<ActivitySsu> filterSsus = containActivitySsus.stream().filter(item -> ssuFormatIds.contains(item.getSsuFormatId())).collect(Collectors.toList());
            if (!ListUtil.isNullOrEmpty(filterSsus)) {
                PromotionRule conflictPromotionRule = lcPromotionRuleService.get(filterSsus.get(0).getActivityId());
                // 比较折扣方式
                DisCountCompareHelper disCountCompareHelper = new DisCountCompareHelper(createType, baseRule, conflictPromotionRule);
                disCountCompareHelper.compareSsuDiscount(filterSsus.get(0).getSsuFormatId());
//                disCountCompareHelper.compareClassDiscount(filterSsus.get(0));
            }
        }

        if (!ListUtil.isNullOrEmpty(activityClass2s)) {
            List<ActivitySsu> exActivitySsus = activitySsuTag.get(SsuTagTypeEnums.EXCLUSIVE.getCode());
            List<Integer> exSsuFormatIds = ListUtil.isNullOrEmpty(exActivitySsus) ? new ArrayList<>(16) : exActivitySsus.stream().map(ActivitySsu::getSsuFormatId).collect(Collectors.toList());
            for (ProductSsuFormatDto productSsuFormat : productSsuFormatDtos) {
                Optional<ActivityClass2> oac2 = activityClass2s.stream().filter(item -> item.getClassId().equals(productSsuFormat.getClass2Id())).findFirst();
                if (oac2.isPresent() && !exSsuFormatIds.contains(productSsuFormat.getSsuFormatId())) {
                    PromotionRule conflictPromotionRule = lcPromotionRuleService.get(oac2.get().getActivityId());
                    // 比较折扣方式
                    DisCountCompareHelper disCountCompareHelper = new DisCountCompareHelper(createType, baseRule, conflictPromotionRule);
                    disCountCompareHelper.compareSsuDiscount(productSsuFormat.getSsuFormatId());
                }
            }
        }

    }

}
