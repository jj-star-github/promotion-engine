package com.qmfresh.promotion.strategy.helper;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.enums.LimitTypeEnums;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.strategy.bean.LimitRule;

/**
 * Created by wyh on 2019/6/16.
 *
 * @author wyh
 */
public class LimitRuleHelper {

    public static String getLimitRuleDescription(LimitRule limitRule) {
        LimitTypeEnums limitTypeEnums = LimitTypeEnums.getEnum(limitRule.getLimitType());
        if (limitTypeEnums == null) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("限购类型不存在,limitRule:" + JSON.toJSONString(limitRule));
        }
        String description = "";
        switch (limitTypeEnums) {
            case SINGLE_DAY_PERSON:
                if(limitRule.getSingleDayPersonLimit() == null){
                    throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("限购数量不能为空");
                }
                description = limitTypeEnums.getDetail().concat(limitRule.getSingleDayPersonLimit().toString());
                break;
            case SINGLE_DAY_SHOP_PERSON:
                if(limitRule.getSingleShopDayPersonLimit() == null){
                    throw new BusinessException("限购数量不能为空");
                }
                description = limitTypeEnums.getDetail().concat(limitRule.getSingleShopDayPersonLimit().toString());
                break;
            default:
        }
        return description;
    }
}
