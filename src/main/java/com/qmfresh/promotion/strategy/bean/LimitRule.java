package com.qmfresh.promotion.strategy.bean;

import java.math.BigDecimal;

/**
 * Created by wyh on 2019/5/31.
 *
 * @author wyh
 */
public class LimitRule {
    /**
     * 1.单日单人限购，2.单店单日单人限购
     */
    private Integer limitType;

    /**
     * 单店单日限购量
     */
    private BigDecimal singleDayPersonLimit;

    /**
     * 单店单日单人限购量
     */
    private BigDecimal singleShopDayPersonLimit;

    public Integer getLimitType() {
        return limitType;
    }

    public void setLimitType(Integer limitType) {
        this.limitType = limitType;
    }

    public BigDecimal getSingleDayPersonLimit() {
        return singleDayPersonLimit;
    }

    public void setSingleDayPersonLimit(BigDecimal singleDayPersonLimit) {
        this.singleDayPersonLimit = singleDayPersonLimit;
    }

    public BigDecimal getSingleShopDayPersonLimit() {
        return singleShopDayPersonLimit;
    }

    public void setSingleShopDayPersonLimit(BigDecimal singleShopDayPersonLimit) {
        this.singleShopDayPersonLimit = singleShopDayPersonLimit;
    }
}
