package com.qmfresh.promotion.strategy.bean;

import java.math.BigDecimal;

/**
 * Created by wyh on 2019/5/23.
 *
 * @author wyh
 */
public class PresentRule {
    /**
     * 满件数
     */
    private Integer meetNum;

    /**
     * 满金额
     */
    private BigDecimal meetMoney;

    /**
     * 赠品id
     */
    private Long presentId;

    /**
     * 赠品
     */
    private Integer ssuFormatId;

    /**
     * 限量
     */
    private Integer limitNum;

    public Integer getMeetNum() {
        return meetNum;
    }

    public void setMeetNum(Integer meetNum) {
        this.meetNum = meetNum;
    }

    public BigDecimal getMeetMoney() {
        return meetMoney;
    }

    public void setMeetMoney(BigDecimal meetMoney) {
        this.meetMoney = meetMoney;
    }

    public Long getPresentId() {
        return presentId;
    }

    public void setPresentId(Long presentId) {
        this.presentId = presentId;
    }
}
