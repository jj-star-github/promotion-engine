package com.qmfresh.promotion.strategy.bean;

import java.math.BigDecimal;

/**
 * Created by wyh on 2019/7/25.
 *
 * @author wyh
 */
public class MjRuleInfo {

    /**
     * 满金额
     */
    private BigDecimal meetMoney;

    /**
     * 满减金额
     */
    private BigDecimal reductionMoney;

    /**
     * 规则分级级别，从小到大
     */
    private Integer level;

    /**
     * 规则描述
     */
    private String description;

    public BigDecimal getMeetMoney() {
        return meetMoney;
    }

    public void setMeetMoney(BigDecimal meetMoney) {
        this.meetMoney = meetMoney;
    }

    public BigDecimal getReductionMoney() {
        return reductionMoney;
    }

    public void setReductionMoney(BigDecimal reductionMoney) {
        this.reductionMoney = reductionMoney;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
