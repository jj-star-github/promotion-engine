package com.qmfresh.promotion.strategy.bean;

import java.math.BigDecimal;

/**
 * @ClassName CpRule
 * @Description TODO
 * @Author xbb
 * @Date 2020/2/25 10:39
 */
public class CpRule {
    /**
     * skuID
     */
    private Integer skuId;

    /**
     * 促销价格
     */
    private BigDecimal promotionPrice;

    /**
     * 一级分类
     */
    private Integer class1Id;

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public Integer getClass1Id() {
        return class1Id;
    }

    public void setClass1Id(Integer class1Id) {
        this.class1Id = class1Id;
    }
}
