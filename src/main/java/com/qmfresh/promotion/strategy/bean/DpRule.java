package com.qmfresh.promotion.strategy.bean;

import java.util.List;

/**
 * Created by wyh on 2019/5/31.
 *
 * @author wyh
 */
public class DpRule extends BaseRule {

    private List<Integer> ssuFormatIds;

    public List<Integer> getSsuFormatIds() {
        return ssuFormatIds;
    }

    public void setSsuFormatIds(List<Integer> ssuFormatIds) {
        this.ssuFormatIds = ssuFormatIds;
    }
}
