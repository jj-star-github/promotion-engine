package com.qmfresh.promotion.strategy.bean;

import java.math.BigDecimal;

/**
 * Created by wyh on 2019/6/5.
 *
 * @author wyh
 */
public class DiscountRule {

    /**
     * 1.按单价减少，2.折扣，3.固定金额
     */
    private Integer type;

    private BigDecimal discount;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }
}
