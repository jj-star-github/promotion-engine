package com.qmfresh.promotion.strategy.bean;

import java.math.BigDecimal;

/**
 * Created by wyh on 2019/5/28.
 *
 * @author wyh
 */
public class MzRuleInfo {

    /**
     * 满金额
     */
    private BigDecimal meetMoney;

    /**
     * 赠品类型:1.赠商品，2.赠券
     */
    private Integer presentType;

    /**
     * 赠品id
     */
    private Long presentId;

    /**
     * 赠品名称
     */
    private String ssuName;

    /**
     * 优惠券id
     */
    private Long couponId;

    /**
     * 优惠券名称
     */
    private String couponName;

    /**
     * 规则分级级别，从小到大
     */
    private Integer level;

    /**
     * 规则描述
     */
    private String description;

    public BigDecimal getMeetMoney() {
        return meetMoney;
    }

    public void setMeetMoney(BigDecimal meetMoney) {
        this.meetMoney = meetMoney;
    }

    public Integer getPresentType() {
        return presentType;
    }

    public void setPresentType(Integer presentType) {
        this.presentType = presentType;
    }

    public Long getPresentId() {
        return presentId;
    }

    public void setPresentId(Long presentId) {
        this.presentId = presentId;
    }

    public String getSsuName() {
        return ssuName;
    }

    public void setSsuName(String ssuName) {
        this.ssuName = ssuName;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
