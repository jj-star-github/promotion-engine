package com.qmfresh.promotion.strategy.bean;

/**
 * Created by wyh on 2019/7/20.
 *
 * @author wyh
 */
public class BaseRule {

    /**
     * 折扣规则
     */
    private DiscountRule discountRule;

    /**
     * 限购规则
     */
    private LimitRule limitRule;

    public DiscountRule getDiscountRule() {
        return discountRule;
    }

    public void setDiscountRule(DiscountRule discountRule) {
        this.discountRule = discountRule;
    }

    public LimitRule getLimitRule() {
        return limitRule;
    }

    public void setLimitRule(LimitRule limitRule) {
        this.limitRule = limitRule;
    }
}
