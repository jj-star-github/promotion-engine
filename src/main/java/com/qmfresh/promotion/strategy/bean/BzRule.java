package com.qmfresh.promotion.strategy.bean;

import java.math.BigDecimal;

/**
 * Created by wyh on 2019/5/31.
 *
 * @author wyh
 */
public class BzRule {

    /**
     * 购买数量
     */
    private BigDecimal buyAmount;

    /**
     * 赠送数量
     */
    private BigDecimal givingAmount;

    /**
     * 赠品id
     */
    private Long presentId;

    public BigDecimal getBuyAmount() {
        return buyAmount;
    }

    public void setBuyAmount(BigDecimal buyAmount) {
        this.buyAmount = buyAmount;
    }

    public BigDecimal getGivingAmount() {
        return givingAmount;
    }

    public void setGivingAmount(BigDecimal givingAmount) {
        this.givingAmount = givingAmount;
    }

    public Long getPresentId() {
        return presentId;
    }

    public void setPresentId(Long presentId) {
        this.presentId = presentId;
    }
}
