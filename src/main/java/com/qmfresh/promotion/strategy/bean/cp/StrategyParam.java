package com.qmfresh.promotion.strategy.bean.cp;

import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.entity.PromotionActivity;

/**
 * @version v1.0
 * @ProjectName: promotion-engine
 * @ClassName: StrategyParam
 * @Description: TODO
 * @Author: xbb
 * @Date: 2020/3/8 12:24
 */
public class StrategyParam {
    /**
     * 创建促销或改价活动的入参
     */
    private BaseRequestParam promotionParam;

    /**
     * 促销活动实体
     */
    private PromotionActivity promotionActivity;

    private String strategyName;

    public BaseRequestParam getPromotionParam() {
        return promotionParam;
    }

    public void setPromotionParam(BaseRequestParam promotionParam) {
        this.promotionParam = promotionParam;
    }

    public PromotionActivity getPromotionActivity() {
        return promotionActivity;
    }

    public void setPromotionActivity(PromotionActivity promotionActivity) {
        this.promotionActivity = promotionActivity;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }
}
