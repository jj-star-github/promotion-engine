package com.qmfresh.promotion.strategy.bean;

import java.util.List;

/**
 * Created by wyh on 2019/5/23.
 *
 * @author wyh
 */
public class MzRule {

    /**
     * 满赠类型:1.阶梯满赠，2.每满赠
     */
    private Integer type;

    private List<MzRuleInfo> mzRuleInfoList;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<MzRuleInfo> getMzRuleInfoList() {
        return mzRuleInfoList;
    }

    public void setMzRuleInfoList(List<MzRuleInfo> mzRuleInfoList) {
        this.mzRuleInfoList = mzRuleInfoList;
    }
}
