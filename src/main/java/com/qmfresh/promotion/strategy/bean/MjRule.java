package com.qmfresh.promotion.strategy.bean;

import java.util.List;

/**
 * Created by wyh on 2019/7/25.
 *
 * @author wyh
 */
public class MjRule {

    /**
     * 满减类型:1.阶梯满减，2.每满减
     */
    private Integer type;

    private List<MjRuleInfo> mjRuleInfoList;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<MjRuleInfo> getMjRuleInfoList() {
        return mjRuleInfoList;
    }

    public void setMjRuleInfoList(List<MjRuleInfo> mjRuleInfoList) {
        this.mjRuleInfoList = mjRuleInfoList;
    }
}
