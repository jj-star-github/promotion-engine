package com.qmfresh.promotion.strategy.rule;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.dto.MeetInfo;
import com.qmfresh.promotion.dto.MjContext;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.MJTypeEnums;
import com.qmfresh.promotion.strategy.bean.MjRule;
import com.qmfresh.promotion.strategy.bean.MjRuleInfo;
import com.qmfresh.promotion.strategy.rule.base.AbstractMjStrategy;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/5/28.
 *
 * @author wyh
 */
@Service("LadderMjStrategy")
public class LadderMjStrategy extends AbstractMjStrategy {
    private static final Logger logger = LoggerFactory.getLogger(LadderMjStrategy.class);

    /**
     * 执行满减，设置上下文
     *
     * @param promotionRule
     * @param totalPrice
     * @return
     */
    @Override
    public void execute(PromotionRule promotionRule, BigDecimal totalPrice, PromotionContext promotionContext) {
        MjRule mjRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<MjRule>() {
        });

        if (!mjRule.getType().equals(MJTypeEnums.MUST_SATISFY.getCode())) {
            return;
        }

        // 满金额
        BigDecimal meetMoney = BigDecimal.ZERO;
        BigDecimal reductionMoney = BigDecimal.ZERO;
        MjRuleInfo mjRuleInfo = null;
        List<MeetInfo> meetInfos = new ArrayList<>(16);
        // 获取满足满减条件中金额最大的金额
        for (int i = 0; i < mjRule.getMjRuleInfoList().size(); i++) {
            MeetInfo meetInfo = new MeetInfo();
            mjRuleInfo = mjRule.getMjRuleInfoList().get(i);
            if (totalPrice.compareTo(mjRuleInfo.getMeetMoney()) < 0) {
                meetInfo.setNextMeetMoney(mjRuleInfo.getMeetMoney());
                meetInfo.setNextDiffMoney(mjRuleInfo.getMeetMoney().subtract(totalPrice));
                meetInfo.setLevel(i + 1);
                meetInfo.setMeet(false);
                meetInfos.add(meetInfo);
                break;
            }
            reductionMoney = mjRule.getMjRuleInfoList().get(i).getReductionMoney();
            meetMoney = mjRule.getMjRuleInfoList().get(i).getMeetMoney();
            if (i < mjRule.getMjRuleInfoList().size()) {
                meetInfo.setMeetMoney(mjRule.getMjRuleInfoList().get(i).getMeetMoney());
                meetInfo.setLevel(i + 1);
                meetInfo.setMeet(true);
                meetInfos.add(meetInfo);
            }
        }
        MjContext mjContext = buildMjContext(promotionRule);
        mjContext.setMeetMoney(meetMoney);
        mjContext.setReductionMoney(reductionMoney);
        mjContext.setMeetInfos(meetInfos);

        promotionContext.getMjContexts().add(mjContext);
    }

//    private MeetInfo buildMeetInfo(BigDecimal totalPrice, BigDecimal meetMoney, BigDecimal reductionMoney, int level) {
//        MeetInfo meetInfo = new MeetInfo();
//        if (totalPrice.compareTo(meetMoney) < 0) {
//            meetInfo.setNextMeetMoney(meetMoney);
//            meetInfo.setNextDiffMoney(meetMoney.subtract(totalPrice));
//            meetInfo.setLevel(level);
//            meetInfo.setMeet(false);
//            return meetInfo;
//        }
//        reductionMoney = mjRule.getMjRuleInfoList().get(i).getReductionMoney();
//        meetMoney = mjRule.getMjRuleInfoList().get(i).getMeetMoney();
//        if (i < mjRule.getMjRuleInfoList().size() - 1) {
//            meetInfo.setMeetMoney(mjRule.getMjRuleInfoList().get(i).getMeetMoney());
//            meetInfo.setLevel(i + 1);
//            meetInfo.setMeet(true);
//
//        }
//    }
}
