package com.qmfresh.promotion.strategy.rule.create;

import com.qmfresh.promotion.common.utils.promotion.PromotionCheckUtil;
import com.qmfresh.promotion.dto.CreateMjPromotionParam;
import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.*;
import com.qmfresh.promotion.manager.IPromotionActivityManager;
import com.qmfresh.promotion.manager.IPromotionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @ClassName CreateMjStrategy
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/18 18:17
 */
@Service("CreateMjStrategy")
public class CreateMjStrategy extends AbstractCreatePromotionStrategy {


    @Resource
    private IPromotionManager promotionManager;

    @Resource
    private IPromotionActivityManager promotionActivityManager;

    @Resource
    private PromotionCheckUtil promotionCheckUtil;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean execute(BaseRequestParam baseParam) {
        if(baseParam instanceof CreateMjPromotionParam){
            CreateMjPromotionParam param = (CreateMjPromotionParam) baseParam;
            if (param.getPromotionProductDto().getType().equals(ProductScopeTypeEnums.SSU.getCode())) {
                param.getPromotionProductDto().setProductSsuFormatDtos(promotionManager.getProductSsuFormat(param.getPromotionProductDto().getSsuFormatIds()));
            }
            promotionCheckUtil.check(param.getChannel(), PromotionTypeEnums.MJ, param.getEffectBeginTime(), param.getEffectEndTime(), param.getCrowdTypes(), param.getAreaDtos(),
                    param.getPromotionProductDto());
            PromotionActivity promotionActivity = promotionManager.createPromotionActivity(PromotionTypeEnums.MJ, param.getChannel(), param.getActivityName(), param.getUserId(),
                    param.getUserName(), PromotionCreatorTypeEnums.HEAD_OFFICE.getCode());
            promotionManager.createPromotionUser(promotionActivity, param.getCrowdTypes());
            promotionManager.createPromotionActivityTime(PromotionTypeEnums.MJ, promotionActivity, param.getEffectBeginTime(), param.getEffectEndTime());
            promotionManager.createPromotionShop(promotionActivity, param.getAreaDtos());
            PromotionRule promotionRule = promotionManager.createPromotionRule(PromotionTypeEnums.MJ, promotionActivity, AreaScopeTypeEnums.SHOP,
                    param.getPromotionProductDto().getType(), param.getMjRule());
            promotionManager.createPromotionProduct(promotionActivity.getId(), promotionRule.getId(), promotionActivity.getChannel(), param.getPromotionProductDto());
            promotionActivityManager.online(promotionActivity.getId(), -1L, "系统");
            return true;
        }
        return false;
    }
}
