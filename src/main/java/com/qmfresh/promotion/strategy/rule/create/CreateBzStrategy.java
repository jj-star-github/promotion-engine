package com.qmfresh.promotion.strategy.rule.create;

import com.qmfresh.promotion.bean.sku.ProductSsuFormatDto;
import com.qmfresh.promotion.common.utils.promotion.PromotionCheckUtil;
import com.qmfresh.promotion.dto.CreateBzPromotionParam;
import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.AreaScopeTypeEnums;
import com.qmfresh.promotion.enums.PromotionCreatorTypeEnums;
import com.qmfresh.promotion.enums.PromotionTypeEnums;
import com.qmfresh.promotion.manager.IPromotionActivityManager;
import com.qmfresh.promotion.manager.IPromotionManager;
import com.qmfresh.promotion.service.IPresentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * @ClassName CreateBzStrategy
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/18 18:16
 */
@Service("CreateBzStrategy")
public class CreateBzStrategy extends AbstractCreatePromotionStrategy {

    @Resource
    private IPromotionManager promotionManager;

    @Resource
    private IPresentService presentService;

    @Resource
    private IPromotionActivityManager promotionActivityManager;

    @Resource
    private PromotionCheckUtil promotionCheckUtil;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean execute(BaseRequestParam baseParam) {
        if (baseParam instanceof CreateBzPromotionParam) {
            CreateBzPromotionParam param = (CreateBzPromotionParam) baseParam;
            List<ProductSsuFormatDto> productSsuFormatDtos = promotionManager.getProductSsuFormat(param.getPromotionProductDto().getSsuFormatIds());
            param.getPromotionProductDto().setProductSsuFormatDtos(productSsuFormatDtos);
            promotionCheckUtil.check(param.getChannel(), PromotionTypeEnums.BZ, param.getEffectBeginTime(), param.getEffectEndTime(), param.getCrowdTypes(), param.getAreaDtos(),
                    param.getPromotionProductDto());

            PromotionActivity promotionActivity = promotionManager.createPromotionActivity(PromotionTypeEnums.BZ, param.getChannel(), param.getActivityName(), param.getUserId(),
                    param.getUserName(), PromotionCreatorTypeEnums.HEAD_OFFICE.getCode());
            promotionManager.createPromotionUser(promotionActivity, param.getCrowdTypes());
            promotionManager.createPromotionActivityTime(PromotionTypeEnums.BZ, promotionActivity, param.getEffectBeginTime(), param.getEffectEndTime());
            promotionManager.createPromotionShop(promotionActivity, param.getAreaDtos());
            PromotionRule promotionRule = promotionManager.createPromotionRule(PromotionTypeEnums.BZ, promotionActivity, AreaScopeTypeEnums.SHOP,
                    param.getPromotionProductDto().getType(), param.getBzRule());
            presentService.bindPresent(promotionActivity.getId(), Collections.singletonList(param.getBzRule().getPresentId()));
            promotionActivityManager.online(promotionActivity.getId(), -1L, "系统");
            return true;
        }
        return false;
    }
}
