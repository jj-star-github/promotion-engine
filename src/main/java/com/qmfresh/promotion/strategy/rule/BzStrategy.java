package com.qmfresh.promotion.strategy.rule;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.aviator.function.FuncPresentMeet;
import com.qmfresh.promotion.bean.promotion.CartContext;
import com.qmfresh.promotion.bean.promotion.PromotionSsu;
import com.qmfresh.promotion.cache.service.LCActivitySsuService;
import com.qmfresh.promotion.cache.service.LCPromotionActivityService;
import com.qmfresh.promotion.cache.service.LCPromotionModuleService;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.BzContext;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.dto.PromotionTrace;
import com.qmfresh.promotion.entity.ActivitySsu;
import com.qmfresh.promotion.entity.PresentActivity;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.strategy.bean.BzRule;
import com.qmfresh.promotion.strategy.rule.base.AbstractStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/5/25.
 *
 * @author wyh
 */
@Service("BzStrategy")
@Slf4j
public class BzStrategy extends AbstractStrategy {

    @Resource
    private LCPromotionModuleService lcPromotionModuleService;

    @Resource
    private LCPromotionActivityService lcPromotionActivityService;

    @Resource
    private FuncPresentMeet funcPresentMeet;

    @Resource
    private LCActivitySsuService lcActivitySsuService;

    /**
     * @param rules 买赠规则，包括买A赠A，买A赠B
     * @param cartContext 购物车上线文
     * @return
     */
    @Override
    public void execute(List<PromotionRule> rules, CartContext cartContext,
        PromotionContext promotionContext) {

        Map<String, PresentActivity> presentMap = getPresentActivity(rules);

        for (PromotionRule promotionRule : rules) {
            BzRule bzRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<BzRule>() {
            });
            List<ActivitySsu> activitySsus = lcActivitySsuService.get(promotionRule.getActivityId());
            if (ListUtil.isNullOrEmpty(activitySsus)) {
                continue;
            }
            List<Integer> activitySsuIds = activitySsus.stream().map(ActivitySsu::getSsuFormatId).collect(Collectors.toList());
            Map<Integer, List<PromotionSsu>> ssuMap = cartContext.getPromotionSsus().stream().collect(Collectors.groupingBy(PromotionSsu::getSsuFormatId));
            ssuMap.entrySet().forEach(
                item -> {
                    if(activitySsuIds.contains(item.getKey())) {
                        promotionContext.getBzContexts().add(this.buildBZContext(promotionRule, presentMap.get(bzRule.getPresentId().toString())));
                    }
                }
            );
        }
    }

    private BzContext buildBZContext(PromotionRule promotionRule, PresentActivity presentActivity) {
        BzContext bzContext = new BzContext();
        //todo meet规则
        bzContext.setMeet(funcPresentMeet.isMeet(presentActivity));
        PromotionTrace promotionTrace = new PromotionTrace();
        promotionTrace.setSequence(lcPromotionModuleService.get(promotionRule.getPromotionModuleId().longValue()).getPriority());
        promotionTrace.setPromotionModuleId(promotionRule.getPromotionModuleId());
        promotionTrace.setPromotionModuleName(lcPromotionModuleService.get(promotionRule.getPromotionModuleId().longValue()).getName());
        promotionTrace.setPromotionActivityId(promotionRule.getActivityId());
        promotionTrace.setPromotionActivityName(lcPromotionActivityService.get(promotionRule.getActivityId()).getActivityName());
        promotionTrace.setPromotionRuleId(promotionRule.getId());
        bzContext.setPromotionTrace(promotionTrace);
        bzContext.setPresentActivity(presentActivity);
        return bzContext;
    }
}
