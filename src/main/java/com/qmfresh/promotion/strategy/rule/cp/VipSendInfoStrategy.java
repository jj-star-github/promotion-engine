package com.qmfresh.promotion.strategy.rule.cp;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.sku.QueryProductSsuPrice;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.common.utils.SpringContextUtil;
import com.qmfresh.promotion.dto.CreateVipPromotionParam;
import com.qmfresh.promotion.dto.ModifyPriceParam;
import com.qmfresh.promotion.dto.PromotionAreaDto;
import com.qmfresh.promotion.dto.SendVipPriceParam;
import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.dto.qmpp.QmppSkuDto;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionActivityTime;
import com.qmfresh.promotion.enums.ProductScopeTypeEnums;
import com.qmfresh.promotion.external.service.IModifyPriceExternalApiService;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.manager.IPromotionManager;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.thread.ModifyVipPriceThread;
import com.qmfresh.promotion.thread.factory.NamedThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @version v1.0
 * @ProjectName: promotion-engine
 * @ClassName: VipSendInfoStrategy
 * @Description: TODO
 * @Author: xbb
 * @Date: 2020/3/8 11:31
 */
@Service("VipSendInfoStrategy")
public class VipSendInfoStrategy extends AbstractChangePriceStrategy {

    private static final Logger logger = LoggerFactory.getLogger(VipSendInfoStrategy.class);

    @Resource
    private IPromotionManager promotionManager;
    @Resource
    private IQmExternalApiService qmExternalApiService;
    @Resource
    private IModifyPriceExternalApiService modifyPriceExternalApiService;

    ThreadPoolExecutor pool = new ThreadPoolExecutor(1,1,0, TimeUnit.SECONDS
            ,new LinkedBlockingDeque<>(100),new NamedThreadFactory("vip_change_price"),new ThreadPoolExecutor.CallerRunsPolicy());

    @Override
    public void execute(BaseRequestParam baseRequestParam, PromotionActivity promotionActivity) {
        if(!(baseRequestParam instanceof CreateVipPromotionParam)){
            logger.info("VIP改价促销-通知，请求参数异常异常");
            return;
        }
        CreateVipPromotionParam param = (CreateVipPromotionParam) baseRequestParam;
        //开始时间是否是当天，是的话立即把促销价发给中台
        if(!this.ifStartNow(param)) {
            logger.info("VIP改价促销-通知，当前非活动期间内");
            return;
        }
        logger.info("VIP改价促销-通知 param={}", JSON.toJSONString(baseRequestParam));
        //若会员活动是按商品设置，先查询中台定价系统获取原价，然后再计算得到会员价
        if (ProductScopeTypeEnums.SSU.getCode() == param.getPromotionProductDto().getType()) { //单品促销
            List<QmppSkuDto> modifyVipList = new ArrayList<>();
            for (PromotionAreaDto promotionAreaDto : param.getAreaDtos()) {
                for (Integer shopId : promotionAreaDto.getShopIds()) {
                    List<Integer> skuIds = param.getPromotionProductDto().getSkuIds();
                    if(CollectionUtils.isEmpty(skuIds)){
                        logger.info("VipSendInfoStrategy - execute 改价SkuIds为空");
                        throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("改价入参为空");
                    }
                    QueryProductSsuPrice productSsuPriceParam = new QueryProductSsuPrice();
                    productSsuPriceParam.setShopId(shopId);
                    productSsuPriceParam.setSkuIds(skuIds);
                    //调用门店查询商品价格信息
                    List<QmppSkuDto> vipSkuList = qmExternalApiService.getProductSsuPriceListByCondition(productSsuPriceParam);
                    logger.info("VIP改价促销-查询门店商品信息 activityId={} request={} resp={}", param.getActivityId(), JSON.toJSONString(productSsuPriceParam), JSON.toJSONString(vipSkuList));
                    if(CollectionUtils.isEmpty(vipSkuList)){
                        throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("门店未查询到指定商品，门店id为：" + shopId);
                    }
                    modifyVipList.addAll(vipSkuList);
                }
            }
            //从中台获取指定商品信息后
            this.sendInfo(param,modifyVipList);
        }

    }
    private Boolean ifStartNow(CreateVipPromotionParam realParam){
        Integer now = DateUtil.getCurrentTimeIntValue();
        for(PromotionActivityTime promotionActivityTime : realParam.getActivityTimes()){
            if(now >= promotionActivityTime.getEffectBeginTime() && now <= promotionActivityTime.getEffectEndTime()){
                return true;
            }
        }
        return false;
    }

    public void sendInfo(CreateVipPromotionParam param, List<QmppSkuDto> vipSkuList){

        if(CollectionUtils.isEmpty(vipSkuList)){
            logger.warn("修改会员价入参为空");
            throw new BusinessException("修改会员价入参为空");
        }
        List<List<QmppSkuDto>> vipSkuListList = ListUtil.chopped(vipSkuList,200);
        for(List<QmppSkuDto> skuList : vipSkuListList){
            List<SendVipPriceParam> result = promotionManager.computeVipPrice(skuList, param.getVipRule());
            logger.info("分批发送改价请求,参数为：{}", JSON.toJSONString(skuList));
            List<SendVipPriceParam> resultList = result.stream().map(o -> {
                o.setPrice(null);
                o.setUserId(param.getUserId());
                o.setUserName(param.getUserName());
                return o;
            }).collect(Collectors.toList());
            ModifyPriceParam<SendVipPriceParam> modifyPriceParam = new ModifyPriceParam<>();
            modifyPriceParam.setReqList(resultList);
            //通知改价方法一：调接口
            ModifyVipPriceThread task = SpringContextUtil.getBean("modifyVipPriceThread");
            task.setVipParam(modifyPriceParam);
            pool.execute(task);
            //通知改价方法二：消息队列
//        producerDispatcher.modifyVipChangePrice(modifyPriceParam);
        }

    }
}
