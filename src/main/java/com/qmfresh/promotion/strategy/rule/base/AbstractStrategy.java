package com.qmfresh.promotion.strategy.rule.base;

import com.qmfresh.promotion.bean.promotion.CartContext;
import com.qmfresh.promotion.bean.promotion.PromotionSsu;
import com.qmfresh.promotion.cache.service.LCPromotionActivityService;
import com.qmfresh.promotion.cache.service.LCPromotionModuleService;
import com.qmfresh.promotion.cache.service.LCUserScopeGroupService;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.ActivityModule;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.dto.PromotionSsuContext;
import com.qmfresh.promotion.dto.PromotionTrace;
import com.qmfresh.promotion.entity.PresentActivity;
import com.qmfresh.promotion.entity.PromotionModule;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.entity.UserScopeGroup;
import com.qmfresh.promotion.enums.ChannelTypeEnums;
import com.qmfresh.promotion.enums.CrowdTypeEnums;
import com.qmfresh.promotion.enums.PresentStatusEnums;
import com.qmfresh.promotion.enums.PriceTypeEnums;
import com.qmfresh.promotion.manager.IPresentActivityManager;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/5/31.
 *
 * @author wyh
 */
public abstract class AbstractStrategy {

    /**
     * 阶梯频数
     */
    protected final int frequency = 3;

    @Resource
    private LCPromotionModuleService lcPromotionModuleService;

    @Resource
    private LCPromotionActivityService lcPromotionActivityService;

    @Resource
    private LCUserScopeGroupService lcUserScopeGroupService;
    @Resource
    private IPresentActivityManager presentActivityManager;

    public abstract void execute(List<PromotionRule> rules, CartContext cartContext,
                                 PromotionContext promotionContext);

    protected Map<String, PresentActivity> getPresentActivity(List<PromotionRule> rules) {
        List<PresentActivity> presentActivities = presentActivityManager.presentActivities(rules.stream().map(PromotionRule::getActivityId).distinct().collect(Collectors.toList()),
                Collections.singletonList(PresentStatusEnums.NEW));
        return ListUtil.listToMap(presentActivities);
    }

    protected void setContext(PromotionContext promotionContext, List<PromotionSsuContext> changeContext) {
        if (ListUtil.isNullOrEmpty(changeContext)) {
            return;
        }

        List<PromotionSsuContext> ssuContexts = new ArrayList<>(16);
        Map<Integer, List<PromotionSsuContext>> contextMap = changeContext.stream().collect(Collectors.groupingBy(PromotionSsuContext::getLabelId));
        for (PromotionSsuContext psc : promotionContext.getSsuContexts()) {
            List<PromotionSsuContext> contexts = contextMap.get(psc.getLabelId());
            if (!ListUtil.isNullOrEmpty(contexts)) {
                contexts.forEach(item -> item.setSsuName(psc.getSsuName()));
                ssuContexts.addAll(contexts);
            } else {
                ssuContexts.add(psc);
            }
        }
        Map<Integer, List<PromotionSsuContext>> skuMap = ssuContexts.stream().collect(Collectors.groupingBy(PromotionSsuContext::getSkuId));

        // 合并返回商品列表
        List<PromotionSsuContext> mergedList = new ArrayList<>(ssuContexts.size());
        for (Map.Entry<Integer, List<PromotionSsuContext>> entry : skuMap.entrySet()) {
            Map<Integer, List<PromotionSsuContext>> priceTypeMap = entry.getValue().stream().collect(Collectors.groupingBy(PromotionSsuContext::getPriceType));
            for (Map.Entry<Integer, List<PromotionSsuContext>> priceEntry : priceTypeMap.entrySet()) {
                if (priceEntry.getKey().equals(PriceTypeEnums.MODIFY.getCode())) {
                    mergedList.addAll(priceEntry.getValue());
                    continue;
                }
                PromotionSsuContext psc = priceEntry.getValue().get(0);
                psc.setAmount(priceEntry.getValue().stream().map(PromotionSsuContext::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add));
                mergedList.add(psc);
            }
        }
        promotionContext.setSsuContexts(mergedList);
    }

    /**
     * 促销上下文商品打活动标记
     *
     * @param promotionRule
     * @param promotionSsus    购物车上下文
     * @param promotionContext 促销上下文
     */
    protected void markContext(PromotionRule promotionRule, List<PromotionSsu> promotionSsus, PromotionContext promotionContext) {

        ActivityModule activityModule = new ActivityModule();
        activityModule.setModuleId(promotionRule.getPromotionModuleId());
        activityModule.setActivityId(promotionRule.getActivityId());
        PromotionModule promotionModule = lcPromotionModuleService.get(promotionRule.getPromotionModuleId().longValue());
        activityModule.setPriority(promotionModule.getPriority());

        List<Integer> ssuFormatIds = promotionSsus.stream().map(PromotionSsu::getSsuFormatId).collect(Collectors.toList());
        for (PromotionSsuContext ctx : promotionContext.getSsuContexts()) {
            if (ListUtil.isNullOrEmpty(ctx.getActivityModules())) {
                ctx.setActivityModules(new ArrayList<>(16));
            }
            if (ssuFormatIds.contains(ctx.getSsuFormatId())) {
                List<ActivityModule> activityModules =
                        ctx.getActivityModules().stream().filter(item -> item.getActivityId().equals(activityModule.getActivityId())).collect(Collectors.toList());
                if (ListUtil.isNullOrEmpty(activityModules)) {
                    ctx.getActivityModules().add(activityModule);
                }
            }
        }
    }

    protected PromotionTrace buildTrace(PromotionRule promotionRule) {
        PromotionTrace promotionTrace = new PromotionTrace();
        promotionTrace.setSequence(lcPromotionModuleService.get(promotionRule.getPromotionModuleId().longValue()).getPriority());
        promotionTrace.setPromotionModuleId(promotionRule.getPromotionModuleId());
        promotionTrace.setPromotionModuleName(lcPromotionModuleService.get(promotionRule.getPromotionModuleId().longValue()).getName());
        promotionTrace.setPromotionActivityId(promotionRule.getActivityId());
        promotionTrace.setPromotionActivityName(lcPromotionActivityService.get(promotionRule.getActivityId()).getActivityName());
        promotionTrace.setPromotionRuleId(promotionRule.getId());
        return promotionTrace;
    }

    /**
     * 是否会员用户，线下根据是否有userId判断是否是会员，线上根据userId和level判断是否是会员
     *
     * @param cartContext
     * @return
     */
    protected boolean isVip(CartContext cartContext) {
        if (cartContext.getChannel().equals(ChannelTypeEnums.C_ONLINE.getCode())) {
            return cartContext.getUserId() > 0 && cartContext.getUserLevel() > 0;
        } else {
            return cartContext.getUserId() > 0;
        }
    }

    /**
     * 用户是否参与当前活动
     *
     * @param promotionRule
     * @param cartContext
     * @return
     */
    protected boolean isMeetUser(PromotionRule promotionRule, CartContext cartContext) {
        List<UserScopeGroup> userScopeGroups = lcUserScopeGroupService.get(promotionRule.getActivityId());
        Map<String, UserScopeGroup> userScopeGroupMap = ListUtil.listToMap("userType", userScopeGroups);
        UserScopeGroup normalUser = userScopeGroupMap.get(String.valueOf(CrowdTypeEnums.NOT_VIP.getCode()));
        UserScopeGroup vipUser = userScopeGroupMap.get(String.valueOf(CrowdTypeEnums.NORMAL_VIP.getCode()));

        return !(normalUser == null && vipUser != null && !isVip(cartContext));
    }
}
