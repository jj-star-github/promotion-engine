package com.qmfresh.promotion.strategy.rule.create;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qmfresh.promotion.bean.gis.Shop;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.TimeUtil;
import com.qmfresh.promotion.common.utils.promotion.PromotionCheckUtil;
import com.qmfresh.promotion.dto.CreateVipPromotionParam;
import com.qmfresh.promotion.dto.ProblemShopSkuVo;
import com.qmfresh.promotion.dto.PromotionAreaDto;
import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.entity.ActivitySsu;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionActivityTime;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.*;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.manager.IActivitySsuManager;
import com.qmfresh.promotion.manager.IPromotionActivityManager;
import com.qmfresh.promotion.manager.IPromotionManager;
import com.qmfresh.promotion.mapper.PromotionActivityMapper;
import com.qmfresh.promotion.mapper.PromotionActivityTimeMapper;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.strategy.bean.cp.StrategyParam;
import com.qmfresh.promotion.strategy.facade.ChangePriceExecuteFacade;
import com.qmfresh.promotion.strategy.rule.cp.ChangePriceStrategyExecutor;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ClassName CreateVipStrategy
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/18 18:18
 */
@Service("CreateVipStrategy")
public class CreateVipStrategy extends AbstractCreatePromotionStrategy {
	private static final Logger logger = LoggerFactory.getLogger(CreateVipStrategy.class);
	@Resource
	private IPromotionManager promotionManager;
	@Resource
	private IPromotionActivityManager promotionActivityManager;
	@Resource
	private ChangePriceExecuteFacade changePriceExecuteFacade;
	@Resource
	private PromotionCheckUtil promotionCheckUtil;
	@Resource
	private PromotionActivityTimeMapper promotionActivityTimeMapper;
	@Resource
	private PromotionActivityMapper promotionActivityMapper;
	@Resource
	private IQmExternalApiService qmExternalApiService;
	@Resource
	private IActivitySsuManager iActivitySsuManager;
	@Value("${vip.activity.max.continued.days}")
	private Integer vipMaxDays;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean execute(BaseRequestParam baseParam) {
		if (baseParam instanceof CreateVipPromotionParam) {
			CreateVipPromotionParam param = (CreateVipPromotionParam) baseParam;
			logger.info("VIP促销创建, param={}", JSON.toJSONString(param));
			promotionCheckUtil.checkVipTimeConflict(param.getActivityTimes());
			ProductScopeTypeEnums productScopeType = ProductScopeTypeEnums
					.getEnum(param.getPromotionProductDto().getType());
			if (productScopeType == null) {
				throw new BusinessException("商品范围异常");
			}
			if (!productScopeType.equals(ProductScopeTypeEnums.SSU)) {
				logger.warn("VIP促销创建失败-暂只支持单品设置 type={}", param.getPromotionProductDto().getType());
				throw new BusinessException("暂只支持单品设置");
			}

			//判断所选时间段不超过一个季度（90天）
			boolean checkTime = this.checkTime(param);
			if(!checkTime) {
				throw new BusinessException("所选时间段不可超过一个季度");
			}
			param.getPromotionProductDto().setProductSsuFormatDtos(
					promotionManager.getProductSsuFormat(param.getPromotionProductDto().getSsuFormatIds()));

			promotionCheckUtil.check(param.getChannel(), PromotionTypeEnums.VIP, param.getActivityTimes(),
					param.getCrowdTypes(), param.getAreaDtos(), param.getPromotionProductDto());
			// promotionCheckUtil.check(param.getChannel(), PromotionTypeEnums.CP,
			// param.getActivityTimes(), param.getCrowdTypes(), param.getAreaDtos(),
			// param.getPromotionProductDto());
			boolean check = this.check(param);
			if (!check) {
				throw new BusinessException("该时间段内有总部促销活动，无法创建会员活动");
			}
			// 会员促销与单品促销分类（商品分类）互斥
			promotionCheckUtil.check(param.getChannel(), PromotionTypeEnums.DP, param.getActivityTimes(),
					param.getCrowdTypes(), param.getAreaDtos(), param.getPromotionProductDto());
			// promotionManager.checkPromotionClassWithDp(param.getPromotionProductDto().getPromotionClassDto());
			PromotionActivity promotionActivity = promotionManager.createPromotionActivity(PromotionTypeEnums.VIP,
					param.getChannel(), param.getActivityName(), param.getUserId(), param.getUserName(),
					PromotionCreatorTypeEnums.HEAD_OFFICE.getCode());
			promotionManager.createPromotionUser(promotionActivity, param.getCrowdTypes());
			promotionManager.createPromotionActivityTime(PromotionTypeEnums.VIP, promotionActivity,
					param.getActivityTimes());
			promotionManager.createPromotionShop(promotionActivity, param.getAreaDtos());
			PromotionRule promotionRule = promotionManager.createPromotionRule(PromotionTypeEnums.VIP,
					promotionActivity, AreaScopeTypeEnums.SHOP, productScopeType.getCode(), param.getVipRule());
			promotionManager.createPromotionProduct(promotionActivity.getId(), promotionRule.getId(),
					promotionActivity.getChannel(), param.getPromotionProductDto());
			promotionActivityManager.online(promotionActivity.getId(), -1L, "系统");
			// 执行发送改价消息逻辑
			//老流程先注释
			StrategyParam strategyParam = new StrategyParam();
			strategyParam.setPromotionActivity(promotionActivity);
			strategyParam.setPromotionParam(param);
			strategyParam.setStrategyName(ChangePriceStrategyExecutor.VIP.getValue());
			changePriceExecuteFacade.executeSendInfoStrategy(strategyParam);
			return true;
		}
		return false;
	}
    //vip活动创建校验与改价活动冲突
    private boolean check(CreateVipPromotionParam param) {
    	List<Integer> skuIds = param.getPromotionProductDto().getSkuIds();
    	if(CollectionUtils.isEmpty(skuIds)) {
			logger.warn("VIP促销创建失败-skuIds不可为空 ");
    		throw new BusinessException("暂不支持按分类创建会员活动");
    	}
    	PromotionActivityTime promotionActivityTime = param.getActivityTimes().get(0);
    	Integer effectBeginTime = promotionActivityTime.getEffectBeginTime();
    	Integer effectEndTime = promotionActivityTime.getEffectEndTime();
    	String weekDay = promotionActivityTime.getWeekDay();
    	List<String> weekDayList = TimeUtil.stringToList(weekDay);
    	List<PromotionAreaDto> areaDtos = param.getAreaDtos();
    	List<Integer> shopIds = new ArrayList<>();
    	for(PromotionAreaDto pad:areaDtos) {
    		shopIds.addAll(pad.getShopIds());
    	}
    	/*PromotionAreaDto promotionAreaDto = areaDtos.get(0);
    	List<Integer> shopIds = promotionAreaDto.getShopIds();*/
    	List<Shop> selectShopByIds = qmExternalApiService.selectShopByIds(shopIds);
    	Map<Long, Shop> mapForShop = selectShopByIds.stream().collect(Collectors.toMap(Shop::getId, shop -> shop,(k1,k2) ->k2));
    	//System.out.println("mapForShop.toString()"+mapForShop.toString());
    	//根据门店和时间查询出活动Id
    	List<PromotionActivityTime> actByShopAndTime = promotionActivityTimeMapper.getActByShopAndTime(effectBeginTime, effectEndTime,shopIds);
    	//获取到活动Id查询出有效的活动
    	if(actByShopAndTime.isEmpty()) {
    		return true;
    	}
    	//活动映射
    	Map<Long, PromotionActivityTime> map = actByShopAndTime.stream().collect(Collectors.toMap(PromotionActivityTime::getActivityId, pat -> pat,(k1,k2) ->k2));
    	//取出门店改价 的活动,即星期为空
    	List<PromotionActivityTime> shopActList = actByShopAndTime.stream().filter(act ->act.getWeekDay().equals("")).collect(Collectors.toList());
    	Map<Long, PromotionActivityTime> shopMap = shopActList.stream().collect(Collectors.toMap(PromotionActivityTime::getActivityId, pat -> pat,(k1,k2) ->k2));
    	List<Long> actIds = actByShopAndTime.stream().map(PromotionActivityTime::getActivityId).collect(Collectors.toList());
    	LambdaQueryWrapper<PromotionActivity> entityWrapper = new LambdaQueryWrapper<>();
    	entityWrapper.eq(PromotionActivity::getIsDeleted, 0);
    	entityWrapper.in(PromotionActivity::getStatus, Arrays.asList(ActivityStatusTypeEnums.NOT_START.getCode(), ActivityStatusTypeEnums.STARTED.getCode(),
                ActivityStatusTypeEnums.STOP.getCode()));
    	entityWrapper.in(PromotionActivity::getId, actIds);
    	List<PromotionActivity> patList = promotionActivityMapper.selectList(entityWrapper);
    	if(patList.isEmpty()) {
    		return true;
    		}
    	List<Long> patIdList = patList.stream().map(PromotionActivity::getId).collect(Collectors.toList());
    	//不考虑按分类创建的会员活动
    	List<ActivitySsu> activitySsuList = iActivitySsuManager.activitySSu(actIds,skuIds);
    	if(activitySsuList.isEmpty()) {
    		return true;
    	}
    	Map<Long, ActivitySsu> mapForSsu = activitySsuList.stream().collect(Collectors.toMap(ActivitySsu::getActivityId, pat -> pat,(k1,k2) ->k2));
    	
    	//查看门店商品活动是否在门店活动中
    	List<ProblemShopSkuVo> problemList = new ArrayList<>();
    	for(ActivitySsu ssu :activitySsuList) {
    		PromotionActivityTime prTime = shopMap.get(ssu.getActivityId());
    		if(prTime!=null) {
    			ProblemShopSkuVo problemShopSkuVo = new ProblemShopSkuVo();
    			problemShopSkuVo.setShopId(prTime.getShopId()); 
    			problemShopSkuVo.setShopName(mapForShop.get(Long.parseLong(prTime.getShopId().toString())).getName());
    			problemShopSkuVo.setSkuId(ssu.getSkuId());
    			problemShopSkuVo.setSkuName(mapForSsu.get(ssu.getActivityId()).getSsuName());
    			problemList.add(problemShopSkuVo);
    		}
    		
    	}
    	if(!problemList.isEmpty()) {
    		logger.warn("VIP促销创建失败-所选的门店和商品其中:"+problemList.toString()+"已被门店改价,不能参与会员活动");
    		throw  new BusinessException("所选的门店和商品其中:"+problemList.toString()+"已被门店改价,不能参与会员活动");
    	}
    	List<Long> ssuActIdList = activitySsuList.stream().map(ActivitySsu::getActivityId).distinct().collect(Collectors.toList());
    	//获取到会员的门店和商品的活动
    	List<PromotionActivityTime> effectActList = new ArrayList<>();
    	for(Long ssuAct:ssuActIdList) {
    		PromotionActivityTime patTime = map.get(ssuAct);
    		if(patTime!=null) {
    			effectActList.add(patTime);
    		}
    	}
    	if(effectActList.isEmpty()) {
    		return true;
    	}
    	//获取所有的星期
    	List<String> weekDayStringList = effectActList.stream().map(PromotionActivityTime::getWeekDay).collect(Collectors.toList());
    	if(weekDayStringList.isEmpty()) {
    		return true;
    	}
    	List<String> finalWeekdayList = getFinalWeekdayList(weekDayStringList);
    	//找出商品的活动日
    	List<String> dataList = new ArrayList<>();   	
    	for(PromotionActivityTime patTime:effectActList) {
    		List<String> betweenDates = DateUtil.getBetweenDates(patTime.getEffectBeginTime(), patTime.getEffectEndTime());
    		dataList.addAll(betweenDates);
    	}
    	List<String> finalDataList = new ArrayList<>();
    	for(String dataString :dataList) {
    		//日期转星期
    		String dateToWeek = TimeUtil.dateToWeek(dataString);
    		if(finalWeekdayList.contains(dateToWeek)) {
    			finalDataList.add(dataString);
    		}
    	}
    	if(finalDataList.isEmpty()) {
    		return true;
    	}
    	//入参的时间日期
    	List<String> paramList = new ArrayList<>();
    	List<String> betweenDates = DateUtil.getBetweenDates(effectBeginTime, effectEndTime);
    	for(String dataString:betweenDates) {
    		String dateToWeek = TimeUtil.dateToWeek(dataString);
    		if(weekDayList.contains(dateToWeek)) {
    			paramList.add(dataString);
    		}
    	}
    	//比较存在活动的日期集合和入参的日期集合
    	for(String s:paramList) {
    		if(finalDataList.contains(s)) {
    			return false;
    		}
    	}
    	return true;
    }
    private List<String> getFinalWeekdayList(List<String> weekDayStringList){
    	List<String>  weekDayS= new ArrayList<>();
    	for(String stringWeek:weekDayStringList) {
    		String[] strs = stringWeek.split(",");                      
    		List<String> weekList = Arrays.asList(strs);  
    		weekDayS.addAll(weekList);
    	}
    	List<String> finalWeekdayList = weekDayS.stream().distinct().collect(Collectors.toList());
    	return  finalWeekdayList;
    }
    //字符串转换为集合
    private List<String> StringToList(String weekDays){
    	String[] strs = weekDays.split(",");                      
		List<String> weekList = Arrays.asList(strs); 
		return weekList;
	}
	private boolean checkTime(CreateVipPromotionParam param) {
		PromotionActivityTime promotionActivityTime = param.getActivityTimes().get(0);
		if(promotionActivityTime.getEffectEndTime()-promotionActivityTime.getEffectBeginTime()>vipMaxDays*24*60*60) {
			return false;
		}
		return true;
	}

}
