package com.qmfresh.promotion.strategy.rule.create;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qmfresh.promotion.bean.gis.Shop;
import com.qmfresh.promotion.common.utils.promotion.PromotionCheckUtil;
import com.qmfresh.promotion.dto.CreateCpPromotionParam;
import com.qmfresh.promotion.dto.ProblemShopSkuVo;
import com.qmfresh.promotion.dto.PromotionAreaDto;
import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.entity.ActivitySsu;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionActivityTime;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.*;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.manager.IActivitySsuManager;
import com.qmfresh.promotion.manager.IPromotionManager;
import com.qmfresh.promotion.mapper.PromotionActivityMapper;
import com.qmfresh.promotion.mapper.PromotionActivityTimeMapper;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.strategy.bean.cp.StrategyParam;
import com.qmfresh.promotion.strategy.facade.ChangePriceExecuteFacade;
import com.qmfresh.promotion.strategy.rule.cp.ChangePriceStrategyExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @ClassName CreateCpStrategy
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/18 19:47
 */
@Service("CreateCpStrategy")
public class CreateCpStrategy extends AbstractCreatePromotionStrategy {
    private static final Logger logger = LoggerFactory.getLogger(CreateCpStrategy.class);

    @Resource
    private IPromotionManager promotionManager;

    @Resource
    private ChangePriceExecuteFacade changePriceExecuteFacade;

    @Resource
    private PromotionCheckUtil promotionCheckUtil;
    @Resource
    private PromotionActivityTimeMapper promotionActivityTimeMapper;
    @Resource
    private PromotionActivityMapper promotionActivityMapper;
    @Resource
    private IActivitySsuManager activitySsuManager;
    @Resource
    private IQmExternalApiService qmExternalApiService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean execute(BaseRequestParam baseParam) {

        CreateCpPromotionParam param = (CreateCpPromotionParam) baseParam;
        logger.info("改价活动创建 param={}", JSON.toJSONString(param));
        ProductScopeTypeEnums productScopeType = ProductScopeTypeEnums
                .getEnum(param.getPromotionProductDto().getType());
        if (productScopeType == null) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("商品范围异常");
        }
        if (productScopeType.equals(ProductScopeTypeEnums.SSU)) {
            param.getPromotionProductDto().setProductSsuFormatDtos(
                    promotionManager.getProductSsuFormat(param.getPromotionProductDto().getSsuFormatIds()));
        }

        // 发起活动为总部促销，校验重复促销活动，门店促销不校验
        if (param.getCreatorFrom().equals(1)) {
            // 门店逻辑
            // 校验入参中促销价是否符合格式要求
            if (!promotionCheckUtil.validateCpPromotionPrice(param.getPromotionProductDto().getSkuRules())) {
                throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("输入的促销价格式不正确，请输入三位以内的小数或整数");
            }

            // 校验同一时间相同门店不可相同商品不可以有多个活动存在
            boolean check = this.check(param);
            if (!check) {
                throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("所选门店和商品部分已创建总部活动，请检查");
            }
            // 校验活动时间是否冲突（和会员时间一致）
            promotionCheckUtil.checkVipTimeConflict(param.getActivityTimes());

            // 校验直降活动，参数多了一个活动创建者来源
            promotionCheckUtil.checkCp(param.getChannel(), PromotionTypeEnums.CP, param.getActivityTimes(),
                    param.getCrowdTypes(), param.getAreaDtos(), param.getPromotionProductDto(), param.getCreatorFrom());
        }

        // 改价促销与会员促销分类互斥
        promotionCheckUtil.check(param.getChannel(), PromotionTypeEnums.VIP, param.getActivityTimes(),
                param.getCrowdTypes(), param.getAreaDtos(), param.getPromotionProductDto());

        PromotionActivity promotionActivity = promotionManager.createPromotionActivity(PromotionTypeEnums.CP,
                param.getChannel(), param.getActivityName(), param.getUserId(), param.getUserName(),
                param.getCreatorFrom());
        promotionManager.createPromotionUser(promotionActivity, param.getCrowdTypes());
        promotionManager.createPromotionActivityTime(PromotionTypeEnums.CP, promotionActivity,
                param.getActivityTimes());
        promotionManager.createPromotionShop(promotionActivity, param.getAreaDtos());
        PromotionRule promotionRule = promotionManager.createPromotionRule(PromotionTypeEnums.CP, promotionActivity,
                AreaScopeTypeEnums.SHOP, productScopeType.getCode(), param.getCpRule());
        promotionManager.createCpPromotionSsu(promotionActivity.getId(), promotionRule.getId(),
                promotionActivity.getChannel(), SsuTagTypeEnums.CONTAIN, param.getPromotionProductDto());
        // 执行发送信息逻辑
        StrategyParam strategyParam = new StrategyParam();
        strategyParam.setPromotionActivity(promotionActivity);
        strategyParam.setPromotionParam(param);
        strategyParam.setStrategyName(ChangePriceStrategyExecutor.CP.getValue());
        changePriceExecuteFacade.executeSendInfoStrategy(strategyParam);
        return true;
    }

    private boolean check(CreateCpPromotionParam param) {
        List<Integer> skuIds = param.getPromotionProductDto().getSkuIds();
        if (skuIds.isEmpty()) {
            throw new BusinessException("请选择商品");
        }
        PromotionActivityTime promotionActivityTime = param.getActivityTimes().get(0);
        Integer effectBeginTime = promotionActivityTime.getEffectBeginTime();
        Integer effectEndTime = promotionActivityTime.getEffectEndTime();
        List<PromotionAreaDto> areaDtos = param.getAreaDtos();
        PromotionAreaDto promotionAreaDto = areaDtos.get(0);
        List<Integer> shopIds = promotionAreaDto.getShopIds();
        List<Shop> selectShopByIds = qmExternalApiService.selectShopByIds(shopIds);
        Map<Long, Shop> mapForShop = selectShopByIds.stream().collect(Collectors.toMap(Shop::getId, shop -> shop));
        // 根据门店和时间查询出活动Id
        List<PromotionActivityTime> actByShopAndTime = promotionActivityTimeMapper.getActByShopAndTime(effectBeginTime,
                effectEndTime, shopIds);
        // 获取到活动Id查询出有效的活动
        if (actByShopAndTime.isEmpty()) {
            return true;
        }
        // 活动映射
        Map<Long, PromotionActivityTime> map = actByShopAndTime.stream()
                .collect(Collectors.toMap(PromotionActivityTime::getActivityId, pat -> pat));
        List<Long> actIds = actByShopAndTime.stream().map(PromotionActivityTime::getActivityId)
                .collect(Collectors.toList());
        LambdaQueryWrapper<PromotionActivity> entityWrapper = new LambdaQueryWrapper<>();
        entityWrapper.eq(PromotionActivity::getIsDeleted, 0);
        entityWrapper.in(PromotionActivity::getStatus, Arrays.asList(ActivityStatusTypeEnums.NOT_START.getCode(),
                ActivityStatusTypeEnums.STARTED.getCode(), ActivityStatusTypeEnums.STOP.getCode()));
        entityWrapper.in(PromotionActivity::getId, actIds);
        List<PromotionActivity> patList = promotionActivityMapper.selectList(entityWrapper);
        if (patList.isEmpty()) {
            return true;
        }
        // 过滤出总部改价活动
        List<PromotionActivity> zbActlist = patList.stream()
                .filter(pat -> pat.getCreatorFrom() == PromotionCreatorTypeEnums.HEAD_OFFICE.getCode()
                        && pat.getPromotionModuleId() == PromotionTypeEnums.CP.getCode())
                .collect(Collectors.toList());
        if (zbActlist.isEmpty()) {
            return true;
        }
        List<Long> patIdList = zbActlist.stream().map(PromotionActivity::getId).collect(Collectors.toList());
        // 查询活动商品

        // 不考虑按分类创建的会员活动
        List<ActivitySsu> activitySsuList = activitySsuManager.filterActivitySsu(patIdList, skuIds);
        if (activitySsuList.isEmpty()) {
            return true;
        }
        // Map<Long, ActivitySsu> mapForSsu =
        // activitySsuList.stream().collect(Collectors.toMap(ActivitySsu::getActivityId,
        // pat -> pat));
        List<ProblemShopSkuVo> problemList = new ArrayList<>();
        for (ActivitySsu ssu : activitySsuList) {
            Integer shopId = map.get(ssu.getActivityId()).getShopId();
            if (shopId == null) {
                continue;
            }
            Shop shop = mapForShop.get(Long.parseLong(shopId.toString()));
            if (shop != null) {
                ProblemShopSkuVo problemShopSkuVo = new ProblemShopSkuVo();
                problemShopSkuVo.setShopId(shopId);
                problemShopSkuVo.setShopName(shop.getName());
                problemShopSkuVo.setSkuId(ssu.getSkuId());
                problemShopSkuVo.setSkuName(ssu.getSsuName());
                problemList.add(problemShopSkuVo);
            }
        }
        if (!problemList.isEmpty()) {
            logger.warn("促销改价-所选的门店和商品其中:" + problemList.toString() + "已参与总部促销,不能重复创建");
            throw new BusinessException("所选的门店和商品其中:" + problemList.toString() + "已参与总部促销,不能重复创建");
        }
        return false;
    }
}
