package com.qmfresh.promotion.strategy.rule;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.promotion.CartContext;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.strategy.rule.base.AbstractStrategy;
import java.util.List;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/5/25.
 *
 * @author wyh
 */
@Service("MzStrategy")
@Slf4j
public class MzStrategy extends AbstractStrategy {
    private static final Logger logger = LoggerFactory.getLogger(MzStrategy.class);

    /**
     * @param rules 满赠规则，包括阶梯满赠、每满赠等。。。 每满赠和阶梯满赠不互斥，分类满赠和全场满赠不互斥
     * @param cartContext 购物车上线文
     * @return
     */
    @Override
    public void execute(List<PromotionRule> rules, CartContext cartContext,
        PromotionContext promotionContext) {

        List<PromotionRule> filterRules = rules.stream().filter(item -> super.isMeetUser(item, cartContext)).collect(Collectors.toList());
        if (ListUtil.isNullOrEmpty(filterRules)) {
            log.info("满赠促销-当前用户不支持 cartContext={}", JSON.toJSONString(cartContext));
            return;
        }

        // 每满赠、阶梯满赠不互斥
        MzStrategyExecutor.EVERY.execute(filterRules, cartContext, promotionContext);
        MzStrategyExecutor.LADDER.execute(filterRules, cartContext, promotionContext);
    }

}
