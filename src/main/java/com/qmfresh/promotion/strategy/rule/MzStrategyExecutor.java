package com.qmfresh.promotion.strategy.rule;

import com.qmfresh.promotion.bean.promotion.CartContext;
import com.qmfresh.promotion.common.utils.SpringContextUtil;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.strategy.rule.base.AbstractStrategy;
import java.util.List;

/**
 * Created by wyh on 2019/5/28.
 *
 * @author wyh
 */
public enum MzStrategyExecutor {
    // EveryMzStrategy:每满赠，LadderMzStrategy：阶梯满赠

    EVERY("EveryMzStrategy") {
        @Override
        public void execute(List<PromotionRule> rules, CartContext cartContext,
            PromotionContext context) {
            AbstractStrategy everyMzStrategy = SpringContextUtil.getBean(getValue());
            everyMzStrategy.execute(rules, cartContext, context);
        }
    },
    LADDER("LadderMzStrategy") {
        @Override
        public void execute(List<PromotionRule> rules, CartContext cartContext,
            PromotionContext context) {
            AbstractStrategy ladderMzStrategy = SpringContextUtil.getBean(getValue());
            ladderMzStrategy.execute(rules, cartContext, context);
        }
    };

    private String value;

    private int code;

    private MzStrategyExecutor(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public abstract void execute(List<PromotionRule> rules, CartContext cartContext,
        PromotionContext context);

    public static StrategyExecutor getEnum(int code) {
        for (StrategyExecutor enums : StrategyExecutor.values()) {
            if (enums.getCode() == code) {
                return enums;
            }
        }
        return null;
    }
}
