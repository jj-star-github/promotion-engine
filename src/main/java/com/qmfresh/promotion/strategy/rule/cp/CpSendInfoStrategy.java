package com.qmfresh.promotion.strategy.rule.cp;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.sku.Sku;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.SpringContextUtil;
import com.qmfresh.promotion.dto.*;
import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.dto.qmpp.AuthenParam;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionActivityTime;
import com.qmfresh.promotion.enums.ActivityStatusTypeEnums;
import com.qmfresh.promotion.enums.CreatorFromValueTranslateEnums;
import com.qmfresh.promotion.enums.PromotionCreatorTypeEnums;
import com.qmfresh.promotion.enums.PromotionTypeEnums;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.external.service.IQmopExternalApiService;
import com.qmfresh.promotion.manager.IPromotionActivityManager;
import com.qmfresh.promotion.manager.IPromotionManager;
import com.qmfresh.promotion.message.producer.ProducerDispatcher;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.strategy.bean.CpRule;
import com.qmfresh.promotion.thread.ModifyCpPriceThread;
import com.qmfresh.promotion.thread.factory.NamedThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @version v1.0
 * @ProjectName: promotion-engine
 * @ClassName: CpSendInfoStrategy
 * @Description: 向新改价系统发送促销或会员价信息
 * @Author: xbb
 * @Date: 2020/3/8 10:00
 */
@Service("CpSendInfoStrategy")
public class CpSendInfoStrategy extends AbstractChangePriceStrategy {

    private static final Logger logger = LoggerFactory.getLogger(CpSendInfoStrategy.class);

    @Resource
    private IPromotionManager promotionManager;

    @Resource
    private IPromotionActivityManager promotionActivityManager;

    @Resource
    private IQmopExternalApiService qmopExternalApiService;

    @Resource
    private ProducerDispatcher producerDispatcher;

    @Resource
    private IQmExternalApiService qmExternalApiService;

    private static final Integer SHOP_WITH_AUTHORITY = 1;

    private static final Integer SHOP_WITHOUT_AUTHORITY = 2;

    ThreadPoolExecutor pool = new ThreadPoolExecutor(1,1,0, TimeUnit.SECONDS
            ,new LinkedBlockingDeque<>(100),new NamedThreadFactory("cp_change_price"),new ThreadPoolExecutor.CallerRunsPolicy());

    @Override
    public void execute(BaseRequestParam param,PromotionActivity promotionActivity) {
        logger.info("执行CpSendInfoStrategy.execute，参数：" + JSON.toJSONString(param));

        CreateCpPromotionParam realParam = ((CreateCpPromotionParam) param);
        //总部活动和门店活动具体分析
        if(PromotionCreatorTypeEnums.HEAD_OFFICE.getCode() == realParam.getCreatorFrom()){
            promotionActivityManager.online(promotionActivity.getId(), realParam.getUserId(), realParam.getUserName());
            //开始时间是否是当天，是的话立即把促销价发给中台
            if(this.ifStartNow(realParam)){
                //查找是否有门店创建的活动
                ModifyPriceParam<SendPromotionPriceParam> promotionParam = new ModifyPriceParam<>();
                //组织参数
                List<SendPromotionPriceParam> reqList = new ArrayList<>();
                for(PromotionAreaDto promotionAreaDto : realParam.getAreaDtos()){
                    for(Integer shopId : promotionAreaDto.getShopIds()){
                        this.buildRequestList(shopId,realParam,reqList);
                    }
                }
                promotionParam.setReqList(reqList);
                //通知改价方法一：调接口
                ModifyCpPriceThread task= SpringContextUtil.getBean("modifyCpPriceThread");
                task.setPromotionParam(promotionParam);
                task.run();
//                pool.execute(task);
                //通知改价方法二：消息队列
                //                    producerDispatcher.modifyChangePrice(promotionParam);
            }

        }else if(PromotionCreatorTypeEnums.SHOP.getCode() == realParam.getCreatorFrom()){
            //1 校验门店是否有创建活动权
            // 根据shopId查询门店是否有创建活动的权限，如果有权限，活动直接上线，如果没有，设置成待审核状态，用接口

            //校验门店是否有权限创建改价活动，校验门店商品分类是否有权限，校验门店商品是否有权限
            List<Integer> shopIdList = new ArrayList<>();
            //以目前的入参格式，多个门店对应的skuid是相同的，所以可以只查一次获取class1Id
            List<Integer> skuIds = realParam.getPromotionProductDto().getSkuIds();
            List<Sku> skuInfos = qmExternalApiService.getSku(skuIds);

            List<AuthenParam> authenParamList = new ArrayList<>();
            for(PromotionAreaDto area : realParam.getAreaDtos()){
                if(CollectionUtils.isEmpty(area.getShopIds()) || CollectionUtils.isEmpty(skuInfos)){
                    continue;
                }
                for(Integer shopId : area.getShopIds()){
                    for(Sku sku : skuInfos){
                        AuthenParam authenParam = new AuthenParam();
                        authenParam.setShopId(shopId);
                        authenParam.setSkuId(sku.getId());
                        authenParam.setClass1Id(sku.getClass1Id());
                        authenParamList.add(authenParam);
                    }
                }
            }


            List<ShopAuthorityResult> shopAuthorityResultList = qmopExternalApiService.findAuthorityByShopIdList(authenParamList);

            if(CollectionUtils.isEmpty(shopAuthorityResultList)){
                throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("门店权限校验失败：校验信息为空");
            }
            for(ShopAuthorityResult shopAuthorityResult : shopAuthorityResultList){
                if(shopAuthorityResult == null){
                    throw new BusinessException("校验失败：校验信息为空");
                }
                //有权限门店直接发送改价数据到中台
                if (SHOP_WITH_AUTHORITY.equals(shopAuthorityResult.getChangePriceAuthority())){
                    logger.info("into method CpSendInfoStrategy-execute has auth");
                    //先上线
                    promotionActivityManager.online(promotionActivity.getId(), realParam.getUserId(), realParam.getUserName());
                    //如果时间条件满足，再发送到中台，否则只上线
                    //开始时间是否是当天，是的话立即把促销价发给中台
                    if(this.ifStartNow(realParam)){
                        logger.info("into method CpSendInfoStrategy-execute activity is today");
                        ModifyPriceParam<SendPromotionPriceParam> promotionParam = new ModifyPriceParam<>();
                        //组织参数
                        List<SendPromotionPriceParam> reqList = new ArrayList<>();
                        //                            for(Integer shopId : shopIdList){
                        this.buildRequestList(shopAuthorityResult.getShopId(),realParam,reqList);
                        //                            }
                        promotionParam.setReqList(reqList);
                        logger.info("into method CpSendInfoStrategy-execute reqList:" + JSON.toJSONString(reqList));
                        //通知改价方法一：调接口
                        ModifyCpPriceThread task= SpringContextUtil.getBean("modifyCpPriceThread");
                        task.setPromotionParam(promotionParam);
                        pool.execute(task);
                        //失效4天前包含今天的活动
                        promotionActivityManager.offLine(shopAuthorityResult,promotionActivity.getId());
                        //通知改价方法二：消息队列
                        //                            producerDispatcher.modifyChangePrice(promotionParam);
                    }
                }else if (SHOP_WITHOUT_AUTHORITY.equals(shopAuthorityResult.getChangePriceAuthority())){
                    //把status设置成待审核状态
                    PromotionActivity pa = new PromotionActivity();
                    pa.setStatus(ActivityStatusTypeEnums.UNAUDITED.getCode());
                    pa.setId(realParam.getActivityId());
                    promotionActivityManager.updateById(pa);

                    //2把数据发送到qmop校验，走消息队列
                    List<ShopCheckAuthorityParam> shopSkuList = new ArrayList<>();
                    if(realParam.getPromotionProductDto() != null && !CollectionUtils.isEmpty(realParam.getPromotionProductDto().getSkuRules())){
                        //查询商品对应的分类
                        List<Integer> skuIdList = realParam.getPromotionProductDto().getSkuRules().stream().map(CpRule::getSkuId).collect(Collectors.toList());
                        List<Sku> skuInfoList = qmExternalApiService.getSku(skuIdList);
                        for(Sku sku : skuInfoList){
                            for(CpRule cpRule : realParam.getPromotionProductDto().getSkuRules()){
                                if(cpRule.getSkuId().equals(sku.getId())){
                                    cpRule.setClass1Id(sku.getClass1Id());
                                    break;
                                }
                            }
                        }
                        List<CreateCpFromShopParam> originparam = realParam.getOriginparam();
                        Map<Integer, CreateCpFromShopParam> originMap = originparam.stream().collect(Collectors.toMap(CreateCpFromShopParam::getSkuId, sku -> sku));
                        
                        for(CpRule cpRule : realParam.getPromotionProductDto().getSkuRules()){
                            ShopCheckAuthorityParam shopCheckAuthorityParam = new ShopCheckAuthorityParam();
                            shopCheckAuthorityParam.setActivityId(promotionActivity.getId());
                            shopCheckAuthorityParam.setPromotionPrice(cpRule.getPromotionPrice());
                            shopCheckAuthorityParam.setSkuId(cpRule.getSkuId());
                            shopCheckAuthorityParam.setShopId(realParam.getAreaDtos().get(0).getShopIds().get(0));
                            shopCheckAuthorityParam.setRealTimeInventory(originMap.get(cpRule.getSkuId()).getRealTimeInventory());
                            shopCheckAuthorityParam.setProfitRate(originMap.get(cpRule.getSkuId()).getProfitRate());
                            shopCheckAuthorityParam.setPromotionActivityTimeList(realParam.getActivityTimes());
                            shopCheckAuthorityParam.setUserId(realParam.getUserId());
                            shopCheckAuthorityParam.setUserName(realParam.getUserName());
                            shopCheckAuthorityParam.setApplyReasonCode(realParam.getApplyReasonCode());
                            shopCheckAuthorityParam.setApplyReasonName(realParam.getApplyReasonName());
                            shopCheckAuthorityParam.setClass1Id(cpRule.getClass1Id());
                            
                            shopSkuList.add(shopCheckAuthorityParam);
                        }
                        //没有创建活动的门店直接通过消息队列把数据传到审核服务
                        producerDispatcher.shopChangePriceAuthorityCheck(shopSkuList);
                    }
                }
            }
        }
    }

    private Boolean ifStartNow(CreateCpPromotionParam realParam){
        Integer now = DateUtil.getCurrentTimeIntValue();
        for(PromotionActivityTime promotionActivityTime : realParam.getActivityTimes()){
            if(now >= promotionActivityTime.getEffectBeginTime() && now <= promotionActivityTime.getEffectEndTime()){
                return true;
            }
        }
        return false;
    }

    private void buildRequestList(Integer shopId,CreateCpPromotionParam realParam,List<SendPromotionPriceParam> reqList) {
        List<PromotionActivity> promotionActivityList = promotionManager.getActivityInfoByShopInfo(shopId, Arrays.asList(PromotionTypeEnums.CP));
        List<Long> promotionActivityIdList = promotionActivityList.stream().map(PromotionActivity::getId).collect(Collectors.toList());

        List<QueryPriceChangeResult> skuPriceList = new ArrayList<>();
        for (Long activityId : promotionActivityIdList) {
            List<QueryPriceChangeResult> skuList = promotionManager.getActivitySsuList(activityId, shopId,realParam.getPromotionProductDto().getSkuIds());
            if (!CollectionUtils.isEmpty(skuList)) {
                skuPriceList.addAll(skuList);
            }
        }

        //由于skuPriceList里可能会有重复的skuId（cpRule本身，和其对应的门店或运营创建的两条数据），所以要单独循环处理
        Map<Integer,QueryPriceChangeResult> resultMap = new HashMap<>();

        for(QueryPriceChangeResult result : skuPriceList){
            for(QueryPriceChangeResult tmp : skuPriceList){
                if(result.getSkuId().equals(tmp.getSkuId())){
//                    if(tmp.getPromotionPrice().compareTo(result.getPromotionPrice()) < 0){
//
//                    }
                    result.setPromotionPrice(tmp.getPromotionPrice());
                    result.setCreatorFrom(tmp.getCreatorFrom());
                }
            }
            if(!resultMap.keySet().contains(result.getSkuId())){
                resultMap.put(result.getSkuId(),result);
            }
        }
        for(Map.Entry<Integer,QueryPriceChangeResult> map : resultMap.entrySet()){
            SendPromotionPriceParam sendPromotionPriceParam = new SendPromotionPriceParam();
            sendPromotionPriceParam.setPrice(map.getValue().getPromotionPrice());
            sendPromotionPriceParam.setShopId(shopId);
            sendPromotionPriceParam.setSkuId(map.getValue().getSkuId());
            sendPromotionPriceParam.setReasonType(CreatorFromValueTranslateEnums.getCode2ByCode1(map.getValue().getCreatorFrom()));
            sendPromotionPriceParam.setUserId(realParam.getUserId().intValue());
            sendPromotionPriceParam.setUserName(realParam.getUserName());
            reqList.add(sendPromotionPriceParam);
        }
    }
}
