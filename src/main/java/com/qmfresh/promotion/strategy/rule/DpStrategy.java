package com.qmfresh.promotion.strategy.rule;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.bean.order.QueryOrderParam;
import com.qmfresh.promotion.bean.order.UserOrderItemSummary;
import com.qmfresh.promotion.bean.promotion.CartContext;
import com.qmfresh.promotion.bean.promotion.PromotionSsu;
import com.qmfresh.promotion.cache.service.LCPromotionActivityService;
import com.qmfresh.promotion.cache.service.LCPromotionModuleService;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.dto.PromotionSsuContext;
import com.qmfresh.promotion.dto.PromotionTrace;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.service.ISYOrderService;
import com.qmfresh.promotion.strategy.bean.DpRule;
import com.qmfresh.promotion.strategy.helper.DiscountPriceHelper;
import com.qmfresh.promotion.strategy.rule.base.AbstractStrategy;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/5/31.
 *
 * @author wyh
 */
@Service("DpStrategy")
public class DpStrategy extends AbstractStrategy {
    private static final Logger logger = LoggerFactory.getLogger(DpStrategy.class);

    @Resource
    private ISYOrderService isyOrderService;

    @Resource
    private LCPromotionModuleService lcPromotionModuleService;

    @Resource
    private LCPromotionActivityService lcPromotionActivityService;

    /**
     * @param rules 单品促销规则
     * @param cartContext 购物车上线文
     * @return
     */
    @Override
    public void execute(List<PromotionRule> rules, CartContext cartContext,
        PromotionContext promotionContext) {
        QueryOrderParam queryOrderParam = new QueryOrderParam();
        queryOrderParam.setUserId(cartContext.getUserId());
        queryOrderParam.setBeginTime(DateUtil.getStartTimeStamp());
        queryOrderParam.setEndTime(DateUtil.getCurrentTimeIntValue());
        List<UserOrderItemSummary> summaries = isyOrderService.queryOrder(queryOrderParam);
        Map<Integer, List<UserOrderItemSummary>> ssuSummaryMap = null;
        if (!ListUtil.isNullOrEmpty(summaries)) {
            ssuSummaryMap = summaries.stream().collect(Collectors.groupingBy(UserOrderItemSummary::getSsuId));
        }

        List<PromotionSsuContext> ssuContexts = new ArrayList<>(16);
        for (PromotionRule promotionRule : rules) {
            DpRule dpRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<DpRule>() {
            });
            List<PromotionSsu> promotionSsus = cartContext.getPromotionSsus().stream().filter(item -> dpRule.getSsuFormatIds().contains(item.getSsuFormatId())).collect(Collectors.toList());
            if (ListUtil.isNullOrEmpty(promotionSsus)) {
                List<PromotionSsuContext> promotionSsuContexts = this.buildPromotionSsuContext(promotionSsus, ssuSummaryMap, promotionRule, dpRule);
                if (!ListUtil.isNullOrEmpty(promotionSsuContexts)) {
                    ssuContexts.addAll(promotionSsuContexts);
                }
            }
        }
        super.setContext(promotionContext, ssuContexts);
    }

    private List<PromotionSsuContext> buildPromotionSsuContext(List<PromotionSsu> promotionSsus,
        Map<Integer, List<UserOrderItemSummary>> ssuSummaryMap, PromotionRule promotionRule, DpRule dpRule) {
//        Map<String, DpRule> ruleMap = ListUtil.listToMap(dpRules);
        int ssuId = promotionSsus.get(0).getSsuId();
        if (ssuSummaryMap == null || ListUtil.isNullOrEmpty(ssuSummaryMap.get(ssuId))) {
            return null;
        }
        List<UserOrderItemSummary> summaries = ssuSummaryMap.get(ssuId);
        BigDecimal historyAmount = summaries.stream().map(UserOrderItemSummary::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);

        List<PromotionSsuContext> contexts = new ArrayList<>(16);
        for (PromotionSsu promotionSsu : promotionSsus) {
            contexts.addAll(this.buildPromotionSsuContext(promotionSsu, historyAmount, promotionRule, dpRule));
            historyAmount = historyAmount.add(promotionSsu.getAmount());
        }
        return contexts;
    }

    private List<PromotionSsuContext> buildPromotionSsuContext(PromotionSsu promotionSsu,
        BigDecimal historyAmount, PromotionRule promotionRule, DpRule dpRule) {
        List<PromotionSsuContext> contexts = new ArrayList<>(16);

        PromotionSsuContext promotionSsuContext = new PromotionSsuContext();
        promotionSsuContext.setLabelId(promotionSsu.getLabelId());
        promotionSsuContext.setMeet(true);
        promotionSsuContext.setSkuId(promotionSsu.getSkuId());
        promotionSsuContext.setSsuFormatId(promotionSsu.getSsuFormatId());
        promotionSsuContext.setAmount(promotionSsu.getAmount());
        promotionSsuContext.setOriginPrice(promotionSsu.getOriginPrice());
        promotionSsuContext.setVipPrice(promotionSsu.getVipPrice());
        promotionSsuContext.setPriceAfterDiscount(promotionSsu.getPriceAfterDiscount());
        if (dpRule == null) {
            promotionSsuContext.setMeet(false);
            promotionSsuContext.setPreferentialPrice(promotionSsu.getPriceAfterDiscount());
            return Collections.singletonList(promotionSsuContext);
        }
        promotionSsuContext.setPreferentialPrice(DiscountPriceHelper.getDiscountPrice(promotionSsu.getPriceAfterDiscount(), dpRule.getDiscountRule()));
        // 目前只有单日单店单人限量
        if (promotionSsu.getAmount().add(historyAmount).compareTo(dpRule.getLimitRule().getSingleShopDayPersonLimit()) > 0) {
            // 拆单,限购量-历史下单量
            promotionSsuContext.setAmount(dpRule.getLimitRule().getSingleShopDayPersonLimit().subtract(historyAmount));

            PromotionTrace promotionTrace = new PromotionTrace();
            promotionTrace.setSequence(lcPromotionModuleService.get(promotionRule.getPromotionModuleId().longValue()).getPriority());
            promotionTrace.setPromotionModuleId(promotionRule.getPromotionModuleId());
            promotionTrace.setPromotionModuleName(lcPromotionModuleService.get(promotionRule.getPromotionModuleId().longValue()).getName());
            promotionTrace.setPromotionActivityId(promotionRule.getActivityId());
            promotionTrace.setPromotionActivityName(lcPromotionActivityService.get(promotionRule.getActivityId()).getActivityName());
            promotionTrace.setPromotionRuleId(promotionRule.getId());
            promotionSsuContext.setPromotionTrace(promotionTrace);
            contexts.add(promotionSsuContext);

            PromotionSsuContext splitPromotionSsuContext = new PromotionSsuContext();
            splitPromotionSsuContext.setMeet(false);
            splitPromotionSsuContext.setSkuId(promotionSsu.getSkuId());
            splitPromotionSsuContext.setSsuFormatId(promotionSsu.getSsuFormatId());
            // 当前下单量+历史下单量-限购量
            splitPromotionSsuContext.setAmount(promotionSsu.getAmount().add(historyAmount).subtract(dpRule.getLimitRule().getSingleShopDayPersonLimit()));
            splitPromotionSsuContext.setOriginPrice(promotionSsu.getOriginPrice());
            splitPromotionSsuContext.setVipPrice(promotionSsu.getVipPrice());
            splitPromotionSsuContext.setPriceAfterDiscount(promotionSsu.getPriceAfterDiscount());
            splitPromotionSsuContext.setPreferentialPrice(promotionSsu.getOriginPrice());
            contexts.add(splitPromotionSsuContext);
        }
        return contexts;
    }

//    private BzContext buildBZContext(PromotionRule promotionRule, PresentActivity presentActivity) {
//        BzContext bzContext = new BzContext();
//        bzContext.setMeet(funcPresentMeet.isMeet(presentActivity));
//        PromotionTrace promotionTrace = new PromotionTrace();
//        promotionTrace.setSequence(lcPromotionModuleService.get(promotionRule.getPromotionModuleId()).getPriority());
//        promotionTrace.setPromotionModuleId(promotionRule.getPromotionModuleId());
//        promotionTrace.setPromotionModuleName(lcPromotionModuleService.get(promotionRule.getPromotionModuleId()).getName());
//        promotionTrace.setPromotionActivityId(promotionRule.getActivityId());
//        promotionTrace.setPromotionActivityName(lcPromotionActivityService.get(promotionRule.getActivityId()).getActivityName());
//        promotionTrace.setPromotionRuleId(promotionRule.getId());
//        bzContext.setPromotionTrace(promotionTrace);
//        bzContext.setPresentActivity(presentActivity);
//        return bzContext;
//    }
}
