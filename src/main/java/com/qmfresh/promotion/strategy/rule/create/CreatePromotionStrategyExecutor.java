package com.qmfresh.promotion.strategy.rule.create;

import com.qmfresh.promotion.common.utils.SpringContextUtil;
import com.qmfresh.promotion.dto.base.BaseRequestParam;

/**
 * @ClassName CreatePromotionStrategyExecutor
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/18 18:28
 */
public enum CreatePromotionStrategyExecutor {

    BZ("CreateBzStrategy"){
        @Override
        public Boolean execute(BaseRequestParam param) {
            CreateBzStrategy createBzStrategy = SpringContextUtil.getBean(getValue());
            return createBzStrategy.execute(param);
        }
    },
    DP("CreateDpStrategy"){
        @Override
        public Boolean execute(BaseRequestParam param) {
            CreateDpStrategy createDpStrategy = SpringContextUtil.getBean(getValue());
            return createDpStrategy.execute(param);
        }
    },
    MJ("CreateMjStrategy"){
        @Override
        public Boolean execute(BaseRequestParam param) {
            CreateMjStrategy createMjStrategy = SpringContextUtil.getBean(getValue());
            return createMjStrategy.execute(param);
        }
    },
    MZ("CreateMzStrategy"){
        @Override
        public Boolean execute(BaseRequestParam param) {
            CreateMzStrategy createMzStrategy = SpringContextUtil.getBean(getValue());
            return createMzStrategy.execute(param);
        }
    },
    VIP("CreateVipStrategy"){
        @Override
        public Boolean execute(BaseRequestParam param) {
            CreateVipStrategy createVipStrategy = SpringContextUtil.getBean(getValue());
            return createVipStrategy.execute(param);
        }
    },
    CP("CreateCpStrategy"){
        @Override
        public Boolean execute(BaseRequestParam param) {
            CreateCpStrategy createCpStrategy = SpringContextUtil.getBean(getValue());
            return createCpStrategy.execute(param);
        }
    };


    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    CreatePromotionStrategyExecutor(String value) {
        this.value = value;
    }

    public abstract Boolean execute(BaseRequestParam param);

    public static CreatePromotionStrategyExecutor getEnum(String value){
        for(CreatePromotionStrategyExecutor executor : CreatePromotionStrategyExecutor.values()){
            if(executor.getValue().equals(value)){
                return executor;
            }
        }
        return null;
    }
}
