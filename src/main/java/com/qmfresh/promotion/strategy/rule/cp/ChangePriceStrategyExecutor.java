package com.qmfresh.promotion.strategy.rule.cp;

import com.qmfresh.promotion.common.utils.SpringContextUtil;
import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.entity.PromotionActivity;

/**
 * @version v1.0
 * @ProjectName: promotion-engine
 * @ClassName: ChangePriceStrategyExecutor
 * @Description: TODO
 * @Author: xbb
 * @Date: 2020/3/8 12:05
 */
public enum ChangePriceStrategyExecutor {

    VIP("VipSendInfoStrategy") {
        @Override
        public void execute(BaseRequestParam param, PromotionActivity promotionActivity) {
            VipSendInfoStrategy vipSendInfoStrategy = SpringContextUtil.getBean(getValue());
            vipSendInfoStrategy.execute(param, promotionActivity);
        }
    },

    CP("CpSendInfoStrategy") {
        @Override
        public void execute(BaseRequestParam param, PromotionActivity promotionActivity) {
            CpSendInfoStrategy cpSendInfoStrategy = SpringContextUtil.getBean(getValue());
            cpSendInfoStrategy.execute(param, promotionActivity);
        }
    };
    private String value;

    private int code;

    ChangePriceStrategyExecutor(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public abstract void execute(BaseRequestParam param, PromotionActivity promotionActivity);

    /**
     * 获取枚举类
     *
     * @param name
     * @return
     */
    public static ChangePriceStrategyExecutor getEnum(String name) {
        for (ChangePriceStrategyExecutor enums : ChangePriceStrategyExecutor.values()) {
            if (enums.getValue().equals(name)) {
                return enums;
            }
        }
        return null;
    }

}
