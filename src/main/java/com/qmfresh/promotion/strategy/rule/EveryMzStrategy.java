package com.qmfresh.promotion.strategy.rule;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.bean.promotion.CartContext;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.MzContext;
import com.qmfresh.promotion.dto.MeetInfo;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.MZTypeEnums;
import com.qmfresh.promotion.strategy.bean.MzRule;
import com.qmfresh.promotion.strategy.bean.MzRuleInfo;
import com.qmfresh.promotion.strategy.rule.base.AbstractMzStrategy;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/5/28.
 */

/**
 * 每满赠策略
 */
@Service("EveryMzStrategy")
public class EveryMzStrategy extends AbstractMzStrategy {
    private static final Logger logger = LoggerFactory.getLogger(EveryMzStrategy.class);

    /**
     * 执行满赠，设置上下文
     *
     * @param promotionRule 促销规则
     * @param totalPrice 总金额
     * @param promotionContext 购物车上下文
     */
    @Override
    public void execute(CartContext cartContext, PromotionRule promotionRule, BigDecimal totalPrice, PromotionContext promotionContext) {
        logger.info("EveryMzStrategy promotionRule: {}", promotionRule.getPromotionRule());
        logger.info("EveryMzStrategy 促销规则是：{}", JSON.toJSONString(promotionRule));
        MzRule mzRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<MzRule>() {
        });
        if (!mzRule.getType().equals(MZTypeEnums.SATISFY_SKIP_LATER_CHECK.getCode())) {
            logger.info("不是every满赠");
            return;
        }
        if (ListUtil.isNullOrEmpty(promotionContext.getMzContexts())) {
            promotionContext.setMzContexts(new ArrayList<>(16));
        }
        MzRuleInfo mzRuleInfo = mzRule.getMzRuleInfoList().get(0);
        BigDecimal everMeetMoney = mzRuleInfo.getMeetMoney();
        int count = 0;
        List<MeetInfo> meetInfos = new ArrayList<>(16);
        // 每满赠次数最多不超过3次
        for (int i = 0; i < frequency; i++) {
            MeetInfo meetInfo = new MeetInfo();
            BigDecimal meetMoney = everMeetMoney.multiply(BigDecimal.valueOf(i + 1));
            if (totalPrice.compareTo(meetMoney) < 0) {
                meetInfo.setNextDiffMoney(meetMoney.subtract(totalPrice));
                meetInfo.setNextMeetMoney(meetMoney);
                meetInfo.setLevel(i + 1);
                meetInfo.setMeet(false);
                meetInfos.add(meetInfo);
                break;
            }
            meetInfo.setMeetMoney(meetMoney);
            meetInfo.setLevel(i + 1);
            meetInfo.setMeet(true);
            meetInfos.add(meetInfo);
            count = count + 1;
        }
        MzContext mzContext = super.buildMzContext(cartContext, mzRuleInfo, promotionRule, count);
        mzContext.setMeetMoney(everMeetMoney.multiply(BigDecimal.valueOf(count)));
        mzContext.setMeetInfos(meetInfos);
        promotionContext.getMzContexts().add(mzContext);
    }

}
