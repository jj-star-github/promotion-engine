package com.qmfresh.promotion.strategy.rule.create;

import com.qmfresh.promotion.bean.sku.ProductSsuFormatDto;
import com.qmfresh.promotion.common.utils.promotion.PromotionCheckUtil;
import com.qmfresh.promotion.dto.CreateDpPromotionParam;
import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.AreaScopeTypeEnums;
import com.qmfresh.promotion.enums.PromotionCreatorTypeEnums;
import com.qmfresh.promotion.enums.PromotionTypeEnums;
import com.qmfresh.promotion.manager.IPromotionActivityManager;
import com.qmfresh.promotion.manager.IPromotionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName CreateDpStrategy
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/18 18:16
 */
@Service("CreateDpStrategy")
public class CreateDpStrategy extends AbstractCreatePromotionStrategy {


    @Resource
    private IPromotionManager promotionManager;

    @Resource
    private IPromotionActivityManager promotionActivityManager;

    @Resource
    private PromotionCheckUtil promotionCheckUtil;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean execute(BaseRequestParam baseParam) {
        if(baseParam instanceof CreateDpPromotionParam){
            CreateDpPromotionParam param = (CreateDpPromotionParam) baseParam;
            // todo 目前单品促销只作用于单个ssu上，后续可扩展
            List<ProductSsuFormatDto> productSsuFormatDtos = promotionManager.getProductSsuFormat(param.getPromotionProductDto().getSsuFormatIds());
            param.getPromotionProductDto().setProductSsuFormatDtos(productSsuFormatDtos);
            promotionCheckUtil.check(param.getChannel(), PromotionTypeEnums.DP, param.getEffectBeginTime(), param.getEffectEndTime(), param.getCrowdTypes(), param.getAreaDtos(),
                    param.getPromotionProductDto());
            //        // 单品促销与会员促销分类互斥
            //        this.check(PromotionTypeEnums.VIP, param.getEffectBeginTime(), param.getEffectEndTime(), param.getCrowdTypes(), param.getAreaDtos(), param
            // .getPromotionProductDto());
            promotionCheckUtil.checkMutex(param.getChannel(), PromotionTypeEnums.VIP, PromotionTypeEnums.DP, param.getEffectBeginTime(), param.getEffectEndTime(), param.getCrowdTypes(),
                    param.getAreaDtos(), param.getPromotionProductDto(), param.getDpRule());
            PromotionActivity promotionActivity = promotionManager.createPromotionActivity(PromotionTypeEnums.DP, param.getChannel(), param.getActivityName(), param.getUserId(),
                    param.getUserName(), PromotionCreatorTypeEnums.HEAD_OFFICE.getCode());
            promotionManager.createPromotionUser(promotionActivity, param.getCrowdTypes());
            promotionManager.createPromotionActivityTime(PromotionTypeEnums.DP, promotionActivity, param.getEffectBeginTime(), param.getEffectEndTime());
            promotionManager.createPromotionShop(promotionActivity, param.getAreaDtos());
            PromotionRule promotionRule = promotionManager.createPromotionRule(PromotionTypeEnums.DP, promotionActivity, AreaScopeTypeEnums.SHOP,
                    param.getPromotionProductDto().getType(), param.getDpRule());
            //        promotionManager.createPromotionSsu(promotionActivity.getId(), promotionRule.getId(), promotionActivity.getChannel(), SsuTagTypeEnums.CONTAIN,
            // productSsuFormatDtos);
            promotionActivityManager.online(promotionActivity.getId(), -1L, "系统");
            return true;
        }
        return false;
    }
}
