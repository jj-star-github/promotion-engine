package com.qmfresh.promotion.strategy.rule.create;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.common.utils.promotion.PromotionCheckUtil;
import com.qmfresh.promotion.dto.CreateMzPromotionParam;
import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.*;
import com.qmfresh.promotion.manager.IPromotionActivityManager;
import com.qmfresh.promotion.manager.IPromotionManager;
import com.qmfresh.promotion.service.IPresentService;
import com.qmfresh.promotion.strategy.bean.MzRuleInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName CreateMzStrategy
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/18 18:17
 */
@Service("CreateMzStrategy")
@Slf4j
public class CreateMzStrategy extends AbstractCreatePromotionStrategy {

    @Resource
    private IPromotionManager promotionManager;

    @Resource
    private IPresentService presentService;

    @Resource
    private IPromotionActivityManager promotionActivityManager;

    @Resource
    private PromotionCheckUtil promotionCheckUtil;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean execute(BaseRequestParam baseParam) {
        if(baseParam instanceof CreateMzPromotionParam){
            CreateMzPromotionParam param = (CreateMzPromotionParam) baseParam;
            log.info("创建满赠活动, param={}", JSON.toJSONString(param));
            if (param.getPromotionProductDto().getType().equals(ProductScopeTypeEnums.SSU.getCode())) {
                param.getPromotionProductDto().setProductSsuFormatDtos(promotionManager.getProductSsuFormat(param.getPromotionProductDto().getSsuFormatIds()));
            }
            promotionCheckUtil.check(param.getChannel(), PromotionTypeEnums.MZ, param.getEffectBeginTime(), param.getEffectEndTime(), param.getCrowdTypes(), param.getAreaDtos(),
                    param.getPromotionProductDto());
            PromotionActivity promotionActivity = promotionManager.createPromotionActivity(PromotionTypeEnums.MZ, param.getChannel(), param.getActivityName(), param.getUserId(),
                    param.getUserName(), PromotionCreatorTypeEnums.HEAD_OFFICE.getCode());
            promotionManager.createPromotionUser(promotionActivity, param.getCrowdTypes());
            promotionManager.createPromotionActivityTime(PromotionTypeEnums.MZ, promotionActivity, param.getEffectBeginTime(), param.getEffectEndTime());
            promotionManager.createPromotionShop(promotionActivity, param.getAreaDtos());
            PromotionRule promotionRule = promotionManager.createPromotionRule(PromotionTypeEnums.MZ, promotionActivity, AreaScopeTypeEnums.SHOP,
                    param.getPromotionProductDto().getType(), param.getMzRule());

            // 绑定满赠赠商品
            List<Long> presentIds =
                    param.getMzRule().getMzRuleInfoList().stream().filter(item -> item.getPresentType().equals(PresentTypeEnums.PRODUCT.getCode())).map(MzRuleInfo::getPresentId).collect(Collectors.toList());
            if (!ListUtil.isNullOrEmpty(presentIds)) {
                presentService.bindPresent(promotionActivity.getId(), presentIds);
            }
            promotionManager.createPromotionProduct(promotionActivity.getId(), promotionRule.getId(), promotionActivity.getChannel(), param.getPromotionProductDto());
            //        promotionManager.createPromotionClass(promotionActivity.getId(), promotionRule.getId(), promotionActivity.getChannel(),
            //            param.getPromotionProductDto().getPromotionClassDto());
            promotionActivityManager.online(promotionActivity.getId(), -1L, "系统");
            return true;
        }
        return false;
    }
}
