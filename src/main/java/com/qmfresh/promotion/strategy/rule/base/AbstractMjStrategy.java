package com.qmfresh.promotion.strategy.rule.base;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.promotion.CartContext;
import com.qmfresh.promotion.bean.promotion.PromotionSsu;
import com.qmfresh.promotion.cache.service.LCActivityClass2Service;
import com.qmfresh.promotion.cache.service.LCActivitySsuService;
import com.qmfresh.promotion.cache.service.LCPromotionActivityService;
import com.qmfresh.promotion.common.exception.BusinessException;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.MjContext;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.entity.ActivityClass2;
import com.qmfresh.promotion.entity.ActivitySsu;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.ProductScopeTypeEnums;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/5/30.
 *
 * @author wyh
 */
@Service
public abstract class AbstractMjStrategy extends AbstractStrategy {

    @Resource
    private LCPromotionActivityService lcPromotionActivityService;

    @Resource
    private LCActivityClass2Service lcActivityClass2Service;

    @Resource
    private LCActivitySsuService lcActivitySsuService;

    /**
     * 促销--每满减
     *
     * @param rules 促销规则集合
     * @param cartContext 购物车上线文
     * @param promotionContext 促销上下文
     * @return
     */
    @Override
    public void execute(List<PromotionRule> rules, CartContext cartContext,
        PromotionContext promotionContext) {
        Map<Integer, List<PromotionRule>> productScopeMap = rules.stream().collect(Collectors.groupingBy(PromotionRule::getProductScope));

        List<PromotionRule> class2PromotionRules = productScopeMap.get(ProductScopeTypeEnums.CLASS2.getCode());
        List<PromotionRule> allPromotionRules = productScopeMap.get(ProductScopeTypeEnums.ALL.getCode());

        // 执行二级分类满减
        if (!ListUtil.isNullOrEmpty(class2PromotionRules)) {
            this.executeClass2(class2PromotionRules, cartContext.getPromotionSsus(), promotionContext);
        }

        // 执行全场满减
        if (!ListUtil.isNullOrEmpty(allPromotionRules)) {
            PromotionRule promotionRule = allPromotionRules.get(0);
            super.markContext(promotionRule, cartContext.getPromotionSsus(), promotionContext);
            BigDecimal totalPrice = cartContext.getPromotionSsus().stream().map(PromotionSsu::getPriceAfterDiscount).reduce(BigDecimal.ZERO, BigDecimal::add);
            this.execute(promotionRule, totalPrice, promotionContext);
        }
    }

    /**
     * 执行二级分类满减
     *
     * @param rules 满减规则
     * @param ssuList 购物车商品
     * @param promotionContext 促销上下文
     */
    private void executeClass2(List<PromotionRule> rules, List<PromotionSsu> ssuList,
        PromotionContext promotionContext) {
        for (PromotionRule promotionRule : rules) {
            this.execute(promotionRule, getTotalPrice(promotionRule, ssuList, promotionContext), promotionContext);
        }
    }

    public abstract void execute(PromotionRule promotionRule, BigDecimal totalPrice, PromotionContext promotionContext);

    /**
     * 设置分类满减时，获取满足条件商品的totalPrice
     *
     * @param promotionRule 满减规则
     * @param ssuList 购物车列表
     * @param promotionContext 促销上下文
     * @return
     */
    private BigDecimal getTotalPrice(PromotionRule promotionRule, List<PromotionSsu> ssuList, PromotionContext promotionContext) {
        Map<String, ActivityClass2> class2Map = ListUtil.listToMap("classId",
            lcActivityClass2Service.get(promotionRule.getActivityId()));
        if (class2Map == null || class2Map.isEmpty()) {
            throw new BusinessException("未获取到活动下的二级分类信息,promotionRule:" + JSON.toJSONString(promotionRule));
        }

        List<ActivitySsu> lcActivitySsus = lcActivitySsuService.get(promotionRule.getActivityId());
        final Map<String, ActivitySsu> exSkuMap = !ListUtil.isNullOrEmpty(lcActivitySsus) ? ListUtil.listToMap("skuId", lcActivitySsus) : null;

        BigDecimal totalPrice = BigDecimal.ZERO;

        List<PromotionSsu> promotionSsuList = new ArrayList<>(16);
        for (PromotionSsu promotionSsu : ssuList) {
            if (class2Map.get("-1") != null) {
                promotionSsuList.add(promotionSsu);
                totalPrice = totalPrice.add(promotionSsu.getAmount().multiply(promotionSsu.getPriceAfterDiscount()));
                continue;
            }
            ActivityClass2 activityClass2 = class2Map.get(promotionSsu.getClass2Id().toString());
            ActivitySsu activitySsu = null;
            if (exSkuMap != null) {
                activitySsu = exSkuMap.get(promotionSsu.getSkuId().toString());
            }
            if (activityClass2 != null && activitySsu == null) {
                promotionSsuList.add(promotionSsu);
                totalPrice = totalPrice.add(promotionSsu.getAmount().multiply(promotionSsu.getPriceAfterDiscount()));
            }
        }
        super.markContext(promotionRule, promotionSsuList, promotionContext);
        return totalPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 构建满减上下文
     *
     * @param promotionRule
     * @return
     */
    protected MjContext buildMjContext(PromotionRule promotionRule) {
        MjContext mjContext = new MjContext();
        mjContext.setActivityId(promotionRule.getActivityId());
        mjContext.setActivityName(lcPromotionActivityService.get(promotionRule.getActivityId()).getActivityName());
        mjContext.setPromotionRuleId(promotionRule.getId());
        mjContext.setDescription(promotionRule.getDescript());
//        if (isMeet) {
//            mjContext.setPromotionTrace(super.buildTrace(promotionRule));
//        }

        return mjContext;
    }
}
