package com.qmfresh.promotion.strategy.rule.base;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.coupon.CouponInfo;
import com.qmfresh.promotion.bean.order.QueryOrderParam;
import com.qmfresh.promotion.bean.promotion.CartContext;
import com.qmfresh.promotion.bean.promotion.PromotionSsu;
import com.qmfresh.promotion.cache.service.*;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.MzContext;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.entity.*;
import com.qmfresh.promotion.enums.ProductScopeTypeEnums;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.service.IPromotionOrderService;
import com.qmfresh.promotion.strategy.bean.MzRuleInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/5/30.
 *
 * @author wyh
 */
@Service
public abstract class AbstractMzStrategy extends AbstractStrategy {

    @Resource
    private LCPromotionActivityService lcPromotionActivityService;

    @Resource
    private LCPromotionActivityTimeService lcPromotionActivityTimeService;

    @Resource
    private LCActivityClass2Service lcActivityClass2Service;

    @Resource
    private LCActivitySsuService lcActivitySsuService;

    @Resource
    private LCPresentSsuService lcPresentSsuService;

    @Resource
    private IPromotionOrderService promotionOrderService;

    /**
     * 促销--每满赠
     *
     * @param rules 促销规则集合
     * @param cartContext 购物车上线文
     * @param promotionContext 促销上下文
     * @return
     */
    @Override
    public void execute(List<PromotionRule> rules, CartContext cartContext,
        PromotionContext promotionContext) {
        Map<Integer, List<PromotionRule>> productScopeMap = rules.stream().collect(Collectors.groupingBy(PromotionRule::getProductScope));
        List<PromotionRule> class2PromotionRules = productScopeMap.get(ProductScopeTypeEnums.CLASS2.getCode());
        List<PromotionRule> ssuPromotionRules = productScopeMap.get(ProductScopeTypeEnums.SSU.getCode());
        List<PromotionRule> allPromotionRules = productScopeMap.get(ProductScopeTypeEnums.ALL.getCode());

        // 执行二级分类满赠
        if (!ListUtil.isNullOrEmpty(class2PromotionRules)) {
            // 分类满赠
            this.executeClass2(cartContext, class2PromotionRules, cartContext.getPromotionSsus(), promotionContext);
        }
        if (!ListUtil.isNullOrEmpty(ssuPromotionRules)) {
            // 品类满赠
            this.executeSsuClass2(cartContext, ssuPromotionRules, cartContext.getPromotionSsus(), promotionContext);
        }
        if (!ListUtil.isNullOrEmpty(allPromotionRules)) {
            // 执行全场满赠
            PromotionRule promotionRule = allPromotionRules.get(0);
            super.markContext(promotionRule, cartContext.getPromotionSsus(), promotionContext);
            BigDecimal totalPrice = cartContext.getPromotionSsus().stream().map(PromotionSsu::getPriceAfterDiscount).reduce(BigDecimal.ZERO, BigDecimal::add);
            this.execute(cartContext, promotionRule, totalPrice, promotionContext);
        }
    }

    /**
     * 执行二级分类满赠
     *
     * @param rules
     * @param ssuList
     * @param promotionContext
     */
    private void executeClass2(CartContext cartContext, List<PromotionRule> rules, List<PromotionSsu> ssuList, PromotionContext promotionContext) {
        for (PromotionRule promotionRule : rules) {
            this.execute(cartContext, promotionRule, getTotalPrice(promotionRule, ssuList, promotionContext), promotionContext);
        }
    }

    /**
     * 执行二级分类满赠
     *
     * @param rules
     * @param ssuList
     * @param promotionContext
     */
    private void executeSsuClass2(CartContext cartContext, List<PromotionRule> rules, List<PromotionSsu> ssuList, PromotionContext promotionContext) {
        for (PromotionRule promotionRule : rules) {
            this.execute(cartContext, promotionRule, getSsuTotalPrice(promotionRule, ssuList, promotionContext), promotionContext);
        }
    }

    public abstract void execute(CartContext cartContext, PromotionRule promotionRule, BigDecimal totalPrice, PromotionContext promotionContext);

    private BigDecimal getSsuTotalPrice(PromotionRule promotionRule, List<PromotionSsu> ssuList, PromotionContext promotionContext) {
        List<ActivitySsu> lcActivitySsus = lcActivitySsuService.get(promotionRule.getActivityId());
        List<Integer> skuIds = lcActivitySsus.stream().map(ActivitySsu::getSkuId).distinct().collect(Collectors.toList());
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (PromotionSsu promotionSsu : ssuList) {
            if (skuIds.contains(promotionSsu.getSkuId())) {
                totalPrice = totalPrice.add(promotionSsu.getAmount().multiply(promotionSsu.getPriceAfterDiscount()));
            }
        }
        return totalPrice;
    }

    /**
     * 设置分类满赠时，获取满足条件商品的totalPrice
     *
     * @param promotionRule
     * @param ssuList
     * @return
     */
    private BigDecimal getTotalPrice(PromotionRule promotionRule, List<PromotionSsu> ssuList, PromotionContext promotionContext) {
        Map<String, ActivityClass2> class2Map = ListUtil.listToMap("classId", lcActivityClass2Service.get(promotionRule.getActivityId()));
        if (class2Map == null || class2Map.isEmpty()) {
            throw new BusinessException("未获取到活动下的二级分类信息,promotionRule:" + JSON.toJSONString(promotionRule));
        }

        List<ActivitySsu> lcActivitySsus = lcActivitySsuService.get(promotionRule.getActivityId());
        final Map<String, ActivitySsu> exSkuMap = !ListUtil.isNullOrEmpty(lcActivitySsus) ? ListUtil.listToMap("skuId", lcActivitySsus) : null;

        BigDecimal totalPrice = BigDecimal.ZERO;

        List<PromotionSsu> promotionSsuList = new ArrayList<>(16);
        for (PromotionSsu promotionSsu : ssuList) {
            if (class2Map.get("-1") != null) {
                promotionSsuList.add(promotionSsu);
                totalPrice = totalPrice.add(promotionSsu.getAmount().multiply(promotionSsu.getPriceAfterDiscount()));
                continue;
            }
            ActivityClass2 activityClass2 = class2Map.get(promotionSsu.getClass2Id().toString());
            ActivitySsu activitySsu = null;
            if (exSkuMap != null) {
                activitySsu = exSkuMap.get(promotionSsu.getSkuId().toString());
            }
            if (activityClass2 != null && activitySsu == null) {
                promotionSsuList.add(promotionSsu);
                totalPrice = totalPrice.add(promotionSsu.getAmount().multiply(promotionSsu.getPriceAfterDiscount()));
            }
        }

        super.markContext(promotionRule, promotionSsuList, promotionContext);
        return totalPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 构建满赠上下文校验是否超出赠品限量
     *
     * @param mzRuleInfo
     * @param promotionRule
     * @return
     */
    protected MzContext buildMzContext(CartContext cartContext, MzRuleInfo mzRuleInfo, PromotionRule promotionRule, int amount) {
        MzContext mzContext = new MzContext();
        mzContext.setActivityId(promotionRule.getActivityId());
        mzContext.setActivityName(lcPromotionActivityService.get(promotionRule.getActivityId()).getActivityName());
        mzContext.setMeetMoney(BigDecimal.ZERO);
        mzContext.setPromotionRuleId(promotionRule.getId());
        mzContext.setDescription(promotionRule.getDescript());
        if (mzRuleInfo != null) {
            mzContext.setPresentType(mzRuleInfo.getPresentType());
            mzContext.setMeetMoney(mzRuleInfo.getMeetMoney());
            // 赠品类型:1.赠商品，2.赠券
            switch (mzRuleInfo.getPresentType()) {
                case 1:
                    PresentSsu presentSsu = lcPresentSsuService.get(mzRuleInfo.getPresentId()).get(0);
                    PromotionActivityTime promotionActivityTime = lcPromotionActivityTimeService.get(promotionRule.getActivityId()).get(0);

                    QueryOrderParam queryOrderParam = new QueryOrderParam();
                    queryOrderParam.setChannel(cartContext.getChannel());
                    queryOrderParam.setShopId(cartContext.getShopId());
                    queryOrderParam.setSkuId(presentSsu.getSkuId());
                    queryOrderParam.setBeginTime(promotionActivityTime.getEffectBeginTime());
                    queryOrderParam.setEndTime(promotionActivityTime.getEffectEndTime());
                    List<PromotionOrderItem> poiList = promotionOrderService.queryOrderItem(queryOrderParam);
                    if (!ListUtil.isNullOrEmpty(poiList)) {
                        BigDecimal historyAmount = poiList.stream().map(PromotionOrderItem::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
                        if (presentSsu.getLimitNum().compareTo(historyAmount) > 0) {
                            presentSsu.setGiveNum(BigDecimal.valueOf(amount));
                        } else {
                            presentSsu.setGiveNum(BigDecimal.ZERO);
                        }
                    }
                    mzContext.setPresentSsu(presentSsu);
                    break;
                case 2:
                    CouponInfo couponInfo = new CouponInfo();
                    couponInfo.setCouponId(mzRuleInfo.getCouponId());
                    couponInfo.setCouponName(mzRuleInfo.getCouponName());
                    couponInfo.setGivingNum(BigDecimal.valueOf(amount));
                    mzContext.setCouponInfo(couponInfo);
                    break;
                default:
            }
        }

        return mzContext;
    }
}
