package com.qmfresh.promotion.strategy.rule;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.bean.order.QueryOrderParam;
import com.qmfresh.promotion.bean.promotion.CartContext;
import com.qmfresh.promotion.bean.promotion.PromotionSsu;
import com.qmfresh.promotion.cache.service.LCActivityClass2Service;
import com.qmfresh.promotion.cache.service.LCActivitySsuService;
import com.qmfresh.promotion.cache.service.LCPromotionActivityService;
import com.qmfresh.promotion.cache.service.LCPromotionModuleService;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.ActivityModule;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.dto.PromotionSsuContext;
import com.qmfresh.promotion.dto.PromotionTrace;
import com.qmfresh.promotion.entity.ActivityClass2;
import com.qmfresh.promotion.entity.ActivitySsu;
import com.qmfresh.promotion.entity.PromotionModule;
import com.qmfresh.promotion.entity.PromotionOrderItem;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.ChannelTypeEnums;
import com.qmfresh.promotion.enums.PriceTypeEnums;
import com.qmfresh.promotion.enums.ProductScopeTypeEnums;
import com.qmfresh.promotion.service.IPromotionOrderService;
import com.qmfresh.promotion.strategy.bean.VipRule;
import com.qmfresh.promotion.strategy.rule.base.AbstractStrategy;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/6/9.
 *
 * @author wyh
 */
@Service("VipStrategy")
public class VipStrategy extends AbstractStrategy {
    private static final Logger logger = LoggerFactory.getLogger(MzStrategy.class);

    @Resource
    private LCActivityClass2Service lcActivityClass2Service;

    @Resource
    private LCActivitySsuService lcActivitySsuService;

    @Resource
    private IPromotionOrderService promotionOrderService;

    @Override
    public void execute(List<PromotionRule> rules, CartContext cartContext, PromotionContext promotionContext) {
        // 如果非会员，则不执行会员促销
        logger.info("1.开始执行会员促销");
        // todo 线上非会员判断
        if (!super.isVip(cartContext)) {
            return;
        }

        QueryOrderParam queryOrderParam = new QueryOrderParam();
        queryOrderParam.setUserId(cartContext.getUserId());
        queryOrderParam.setChannel(cartContext.getChannel());
        queryOrderParam.setBeginTime(DateUtil.getStartTimeStamp());
        queryOrderParam.setEndTime(DateUtil.getCurrentTimeIntValue());
        List<PromotionOrderItem> poiList = promotionOrderService.queryOrderItem(queryOrderParam);
        logger.info("2.获取促销订单信息,param:" + JSON.toJSONString(queryOrderParam));
        Map<Integer, List<PromotionOrderItem>> poiMap = !ListUtil.isNullOrEmpty(poiList) ?
            poiList.stream().collect(Collectors.groupingBy(PromotionOrderItem::getSsuFormatId)) : new HashMap<>(1);

        for (PromotionRule promotionRule : rules) {
            List<PromotionSsu> filterSsus = new ArrayList<>(16);
            if (promotionRule.getProductScope().equals(ProductScopeTypeEnums.CLASS2.getCode())) {
                List<ActivityClass2> activityClass2s = lcActivityClass2Service.get(promotionRule.getActivityId());
                if (ListUtil.isNullOrEmpty(activityClass2s)) {
                    logger.info("当前活动不是品类促销");
                    continue;
                }
                logger.info("按分类过滤会员商品");
                filterSsus = this.getFilterSsuByClass(promotionRule.getActivityId(), cartContext, activityClass2s);
            } else if (promotionRule.getProductScope().equals(ProductScopeTypeEnums.SSU.getCode())) {
                logger.info("按ssu过滤会员商品");
                filterSsus = this.getFilterSsuBySsu(promotionRule.getActivityId(), cartContext);
            }
            logger.info("promotionRule:"+JSON.toJSONString(promotionRule)+",filterSsus:" + JSON.toJSONString(filterSsus));

            List<PromotionSsuContext> promotionSsuContexts = new ArrayList<>(16);
            Map<Integer, List<PromotionSsu>> filterSsuMap = new HashMap<>(16);
            if (cartContext.getChannel().equals(ChannelTypeEnums.C_OFFLINE.getCode())) {
                filterSsuMap = filterSsus.stream().collect(Collectors.groupingBy(PromotionSsu::getSkuId));
                for (Map.Entry<Integer, List<PromotionSsu>> entry : filterSsuMap.entrySet()) {
                    promotionSsuContexts.addAll(this.buildPromotionSsuContext(entry.getValue(), promotionRule, poiMap.get(entry.getKey())));
                }
            } else if (cartContext.getChannel().equals(ChannelTypeEnums.C_ONLINE.getCode())) {
                cartContext.getPromotionSsus().forEach(
                    item -> {
                        if (item.getPriceType().equals(PriceTypeEnums.VIP.getCode())) {
                            item.setVipPrice(item.getPriceAfterDiscount());
                        }
                        promotionSsuContexts.addAll(this.buildCmallPromotionSsuContext(item, promotionRule, poiMap.get(item.getSsuFormatId())));
                    }
                );
            }

            super.setContext(promotionContext, promotionSsuContexts);
            logger.info("会员促销执行完成");
        }
    }

    /**
     * 计算历史订单中商品下单量
     *
     * @param historyPoiList 历史订单
     * @return 历史下单量
     */
    private BigDecimal getHistoryAmount(List<PromotionOrderItem> historyPoiList) {
        BigDecimal historyAmount = BigDecimal.ZERO;
        if (!ListUtil.isNullOrEmpty(historyPoiList)) {
            historyAmount = historyPoiList.stream().map(PromotionOrderItem::getAmount).reduce(BigDecimal.ZERO,
                BigDecimal::add);
        }
        return historyAmount;
    }

    private List<PromotionSsuContext> buildCmallPromotionSsuContext(PromotionSsu promotionSsu, PromotionRule promotionRule, List<PromotionOrderItem> historyPoiList) {
        List<PromotionSsuContext> contexts = new ArrayList<>(16);
        BigDecimal historyAmount = this.getHistoryAmount(historyPoiList);
        if (promotionSsu.getPriceType().equals(PriceTypeEnums.VIP.getCode())) {
            VipRule vipRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<VipRule>() {
            });
            contexts.addAll(this.buildOrSplitSsuContext(promotionSsu, historyAmount, promotionRule, vipRule));
        }
        return contexts;
    }

    /**
     * 按sku/ssu构建促销上下文. <br> 注：如果是改价商品，不走会员促销逻辑(约定改价商品单价小于等于会员价)
     *
     * @param promotionSsus 购物车商品
     * @param promotionRule 促销规则
     * @param historyPoiList 商品订单列表
     * @return 涉及变更促销商品上下文
     */
    private List<PromotionSsuContext> buildPromotionSsuContext(List<PromotionSsu> promotionSsus,
        PromotionRule promotionRule, List<PromotionOrderItem> historyPoiList) {
        logger.info("buildPromotionSsuContext,promotionSsus:" + JSON.toJSONString(promotionSsus) + "promotionRule:" + JSON.toJSONString(promotionRule) + "historyPoiList:" + JSON.toJSONString(historyPoiList));
        List<PromotionSsuContext> contexts = new ArrayList<>(16);
        VipRule vipRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<VipRule>() {
        });
        BigDecimal historyAmount = this.getHistoryAmount(historyPoiList);

        // 购物车商品拆分成会员品、普通品，先处理会员品再处理普通品
        List<PromotionSsu> vipPromotionSsus =
            promotionSsus.stream().filter(ps -> ps.getPriceAfterDiscount().compareTo(ps.getVipPrice()) == 0).collect(Collectors.toList());
        List<PromotionSsu> normalPromotionSsus =
            promotionSsus.stream().filter(ps -> ps.getPriceAfterDiscount().compareTo(ps.getVipPrice()) > 0).collect(Collectors.toList());

        if (!ListUtil.isNullOrEmpty(vipPromotionSsus)) {
            // 合并多条会员商品
            contexts.addAll(this.buildOrSplitSsuContext(vipPromotionSsus.get(0), historyAmount, promotionRule, vipRule));
            historyAmount = historyAmount.add(vipPromotionSsus.get(0).getAmount());
        }
        if (!ListUtil.isNullOrEmpty(normalPromotionSsus)) {
            contexts.addAll(this.buildOrSplitSsuContext(normalPromotionSsus.get(0), historyAmount, promotionRule, vipRule));
        }

        logger.info("buildPromotionSsuContext return contexts:" + JSON.toJSONString(contexts));
        return contexts;
    }

    /**
     * 按活动过滤购物车商品数据
     *
     * @param activityId 活动id
     * @param cartContext 购物车商品
     * @return 处于当前已生效活动下，满足条件的促销商品
     */
    private List<PromotionSsu> getFilterSsuBySsu(long activityId, CartContext cartContext) {
        List<ActivitySsu> activitySsus = lcActivitySsuService.get(activityId);
        List<PromotionSsu> promotionSsus = new ArrayList<>(16);
        List<Integer> ssuFormatIds = activitySsus.stream().map(ActivitySsu::getSsuFormatId).collect(Collectors.toList());
        cartContext.getPromotionSsus().forEach(
            ps -> {
                if (ssuFormatIds.contains(ps.getSsuFormatId())) {
                    promotionSsus.add(ps);
                }
            }
        );
        return promotionSsus;
    }

    /**
     * 按分类过滤会员商品
     *
     * @param activityId 活动id
     * @param cartContext 购物车商品
     * @param activityClass2s 活动二级分类
     * @return 处于当前已生效活动下，满足条件的促销商品
     */
    private List<PromotionSsu> getFilterSsuByClass(long activityId, CartContext cartContext,
        List<ActivityClass2> activityClass2s) {
        Optional<ActivityClass2> oac2 =
            activityClass2s.stream().filter(item -> item.getClassId().equals(-1)).findFirst();
        if (oac2.isPresent()) {
            return cartContext.getPromotionSsus();
        }

        List<ActivitySsu> exActivitySsus = lcActivitySsuService.get(activityId);
        List<PromotionSsu> promotionSsus = new ArrayList<>(16);
        Set<Integer> class2Ids = activityClass2s.stream().map(ActivityClass2::getClassId).collect(Collectors.toSet());
        Set<Integer> ssuFormatIds = !ListUtil.isNullOrEmpty(exActivitySsus) ?
            exActivitySsus.stream().map(ActivitySsu::getSsuFormatId).collect(Collectors.toSet()) : new HashSet<>(16);
        cartContext.getPromotionSsus().forEach(
            ps -> {
                if (class2Ids.contains(ps.getClass2Id()) && !ssuFormatIds.contains(ps.getSsuFormatId())) {
                    promotionSsus.add(ps);
                }
            }
        );
        return promotionSsus;
    }

    private List<PromotionSsuContext> buildOrSplitSsuContext(PromotionSsu promotionSsu, BigDecimal historyAmount,
        PromotionRule promotionRule, VipRule vipRule) {
        List<PromotionSsuContext> contexts = new ArrayList<>(16);

        // 历史购买量 > 促销限购量
        if (historyAmount.compareTo(vipRule.getLimitRule().getSingleDayPersonLimit()) >= 0) {
            contexts.add(this.buildPromotionSsuContext(false, promotionSsu.getAmount(), promotionSsu, null));
            return contexts;
        }

        // 当前购买量 + 历史购买量 > 促销限购量
        if (promotionSsu.getAmount().add(historyAmount).compareTo(vipRule.getLimitRule().getSingleDayPersonLimit()) > 0) {
            contexts.add(this.buildPromotionSsuContext(true,
                vipRule.getLimitRule().getSingleDayPersonLimit().subtract(historyAmount), promotionSsu, promotionRule));
            contexts.add(this.buildPromotionSsuContext(false,
                promotionSsu.getAmount().add(historyAmount).subtract(vipRule.getLimitRule().getSingleDayPersonLimit()), promotionSsu, null, true));
            return contexts;
        }

        // 当前购买量 + 历史购买量 = 促销限购量
        if (promotionSsu.getAmount().add(historyAmount).compareTo(vipRule.getLimitRule().getSingleDayPersonLimit()) == 0) {
            contexts.add(this.buildPromotionSsuContext(true,
                vipRule.getLimitRule().getSingleDayPersonLimit().subtract(historyAmount), promotionSsu, promotionRule));
            return contexts;
        }

        contexts.add(this.buildPromotionSsuContext(true, promotionSsu.getAmount(), promotionSsu, promotionRule));
        return contexts;
    }

    private PromotionSsuContext buildPromotionSsuContext(Boolean meet, BigDecimal amount, PromotionSsu promotionSsu,
        PromotionRule promotionRule) {
        return this.buildPromotionSsuContext(meet, amount, promotionSsu, promotionRule, false);
    }

    private PromotionSsuContext buildPromotionSsuContext(Boolean meet, BigDecimal amount, PromotionSsu promotionSsu,
        PromotionRule promotionRule, Boolean split) {
        PromotionSsuContext promotionSsuContext = new PromotionSsuContext();
        promotionSsuContext.setMeet(meet);
        promotionSsuContext.setAmount(amount);
        promotionSsuContext.setLabelId(promotionSsu.getLabelId());
        promotionSsuContext.setSkuId(promotionSsu.getSkuId());
        promotionSsuContext.setClass1Id(promotionSsu.getClass1Id());
        promotionSsuContext.setClass2Id(promotionSsu.getClass2Id());
        promotionSsuContext.setSsuFormatId(promotionSsu.getSsuFormatId());
        // 价格设置为会员价
        promotionSsuContext.setOriginPrice(promotionSsu.getOriginPrice());
        promotionSsuContext.setVipPrice(promotionSsu.getVipPrice());
        promotionSsuContext.setPriceAfterDiscount(promotionSsu.getPriceAfterDiscount());
        promotionSsuContext.setPriceType(PriceTypeEnums.NORMAL.getCode());

        // 如果是拆单，则促销价设置为原价。非拆单设置为实价
        if (split) {
            promotionSsuContext.setPreferentialPrice(promotionSsu.getOriginPrice());
        } else {
            promotionSsuContext.setPreferentialPrice(promotionSsu.getPriceAfterDiscount());
        }
        if (meet) {
            promotionSsuContext.setVipPrice(promotionSsu.getVipPrice());
            promotionSsuContext.setPriceAfterDiscount(promotionSsu.getPriceAfterDiscount());
            // 如果折后价小于会员价，促销价使用折后价，否则使用会员价
            if (promotionSsu.getPriceAfterDiscount().compareTo(promotionSsu.getVipPrice()) > 0) {
                promotionSsuContext.setPreferentialPrice(promotionSsu.getVipPrice());
            } else {
                promotionSsuContext.setPreferentialPrice(promotionSsu.getPriceAfterDiscount());
            }
            promotionSsuContext.setPromotionTrace(super.buildTrace(promotionRule));
            promotionSsuContext.setPriceType(PriceTypeEnums.VIP.getCode());

            // 设置活动模板信息
            List<ActivityModule> activityModules = new ArrayList<>(16);
            ActivityModule activityModule = new ActivityModule();
            activityModule.setActivityId(promotionRule.getActivityId());
            activityModule.setModuleId(promotionRule.getPromotionModuleId());
            activityModules.add(activityModule);
            promotionSsuContext.setActivityModules(activityModules);
        }
        return promotionSsuContext;
    }
}
