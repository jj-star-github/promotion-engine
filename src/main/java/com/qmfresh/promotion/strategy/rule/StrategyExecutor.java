package com.qmfresh.promotion.strategy.rule;

import com.qmfresh.promotion.bean.promotion.CartContext;
import com.qmfresh.promotion.common.utils.SpringContextUtil;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.strategy.rule.base.AbstractStrategy;
import java.util.List;

/**
 * Created by wyh on 2019/5/27.
 *
 * @author wyh
 */
public enum StrategyExecutor {
    // VipStrategy:会员促销，MzStrategy：满赠，BzStrategy：买赠，DpStrategy：单品促销，MjStrategy：满减

    VIP("VipStrategy") {
        @Override
        public void execute(List<PromotionRule> rules, CartContext cartContext,
            PromotionContext context) {
            AbstractStrategy vipStrategy = SpringContextUtil.getBean(getValue());
            vipStrategy.execute(rules, cartContext, context);
        }
    },

    MZ("MzStrategy") {
        @Override
        public void execute(List<PromotionRule> rules, CartContext cartContext,
            PromotionContext context) {
            AbstractStrategy mzStrategy = SpringContextUtil.getBean(getValue());
            mzStrategy.execute(rules, cartContext, context);
        }
    },
    BZ("BzStrategy") {
        @Override
        public void execute(List<PromotionRule> rules, CartContext cartContext,
            PromotionContext context) {
            AbstractStrategy bzStrategy = SpringContextUtil.getBean(getValue());
            bzStrategy.execute(rules, cartContext, context);
        }
    },
    DP("DpStrategy") {
        @Override
        public void execute(List<PromotionRule> rules, CartContext cartContext,
            PromotionContext context) {
            AbstractStrategy dpStrategy = SpringContextUtil.getBean(getValue());
            dpStrategy.execute(rules, cartContext, context);
        }
    },
    MJ("MjStrategy") {
        @Override
        public void execute(List<PromotionRule> rules, CartContext cartContext,
            PromotionContext context) {
            AbstractStrategy mjStrategy = SpringContextUtil.getBean(getValue());
            mjStrategy.execute(rules, cartContext, context);
        }
    };

    private String value;

    private int code;

    private StrategyExecutor(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public abstract void execute(List<PromotionRule> rules, CartContext cartContext,
        PromotionContext context);

    /**
     * 获取枚举类
     *
     * @param name
     * @return
     */
    public static StrategyExecutor getEnum(String name) {
        for (StrategyExecutor enums : StrategyExecutor.values()) {
            if (enums.getValue().equals(name)) {
                return enums;
            }
        }
        return null;
    }
}
