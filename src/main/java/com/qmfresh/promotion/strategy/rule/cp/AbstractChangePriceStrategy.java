package com.qmfresh.promotion.strategy.rule.cp;

import com.qmfresh.promotion.dto.base.BaseRequestParam;
import com.qmfresh.promotion.entity.PromotionActivity;

/**
 * @version v1.0
 * @ProjectName: promotion-engine
 * @ClassName: AbstractChangePriceStrategy
 * @Description: TODO
 * @Author: xbb
 * @Date: 2020/3/8 10:13
 */
public abstract class AbstractChangePriceStrategy {

    public abstract void execute(BaseRequestParam param, PromotionActivity promotionActivity);
}
