package com.qmfresh.promotion.strategy.rule.create;

import com.qmfresh.promotion.dto.base.BaseRequestParam;

/**
 * @ClassName AbstractCreatePromotionStrategy
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/18 18:14
 */
public abstract class AbstractCreatePromotionStrategy {

    public abstract Boolean execute(BaseRequestParam baseParam);
}
