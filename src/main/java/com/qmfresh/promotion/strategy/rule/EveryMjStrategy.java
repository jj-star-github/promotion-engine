package com.qmfresh.promotion.strategy.rule;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.MeetInfo;
import com.qmfresh.promotion.dto.MjContext;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.MJTypeEnums;
import com.qmfresh.promotion.strategy.bean.MjRule;
import com.qmfresh.promotion.strategy.bean.MjRuleInfo;
import com.qmfresh.promotion.strategy.rule.base.AbstractMjStrategy;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/5/28.
 */

/**
 * 每满赠策略
 */
@Service("EveryMjStrategy")
public class EveryMjStrategy extends AbstractMjStrategy {
    private static final Logger logger = LoggerFactory.getLogger(EveryMjStrategy.class);

    /**
     * 执行满赠，设置上下文
     *
     * @param promotionRule
     * @param totalPrice
     * @param promotionContext
     */
    @Override
    public void execute(PromotionRule promotionRule, BigDecimal totalPrice, PromotionContext promotionContext) {
        MjRule mjRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<MjRule>() {
        });
        if (!mjRule.getType().equals(MJTypeEnums.SATISFY_SKIP_LATER_CHECK.getCode())) {
            return;
        }
        if (ListUtil.isNullOrEmpty(promotionContext.getMjContexts())) {
            promotionContext.setMjContexts(new ArrayList<>(16));
        }
        MjRuleInfo mjRuleInfo = mjRule.getMjRuleInfoList().get(0);
        BigDecimal everMeetMoney = mjRuleInfo.getMeetMoney();
        int count = 0;
        List<MeetInfo> meetInfos = new ArrayList<>(16);
        // 每满赠次数最多不超过3次
        for (int i = 0; i < frequency; i++) {
            BigDecimal meetMoney = everMeetMoney.multiply(BigDecimal.valueOf(i + 1));
            MeetInfo meetInfo = new MeetInfo();
            if (totalPrice.compareTo(meetMoney) < 0) {
                meetInfo.setNextDiffMoney(meetMoney.subtract(totalPrice));
                meetInfo.setNextMeetMoney(meetMoney);
                meetInfo.setLevel(i + 1);
                meetInfo.setMeet(false);
                meetInfos.add(meetInfo);
                break;
            }

            meetInfo.setMeetMoney(meetMoney);
            meetInfo.setLevel(i + 1);
            meetInfo.setMeet(true);
            meetInfos.add(meetInfo);
            count = count + 1;
        }

        MjContext mjContext = buildMjContext(promotionRule);
        mjContext.setMeetMoney(everMeetMoney.multiply(new BigDecimal(count)));
        mjContext.setReductionMoney(mjRuleInfo.getReductionMoney().multiply(new BigDecimal(count)));
        mjContext.setMeetInfos(meetInfos);
        promotionContext.getMjContexts().add(mjContext);
    }

}
