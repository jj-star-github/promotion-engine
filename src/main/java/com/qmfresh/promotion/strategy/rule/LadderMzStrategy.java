package com.qmfresh.promotion.strategy.rule;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.bean.promotion.CartContext;
import com.qmfresh.promotion.dto.MzContext;
import com.qmfresh.promotion.dto.MeetInfo;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.enums.MZTypeEnums;
import com.qmfresh.promotion.strategy.bean.MzRuleInfo;
import com.qmfresh.promotion.strategy.bean.MzRule;
import com.qmfresh.promotion.strategy.rule.base.AbstractMzStrategy;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/5/28.
 *
 * @author wyh
 */
@Service("LadderMzStrategy")
public class LadderMzStrategy extends AbstractMzStrategy {
    private static final Logger logger = LoggerFactory.getLogger(LadderMzStrategy.class);

    /**
     * 执行满赠，设置上下文
     *
     * @param promotionRule
     * @param totalPrice
     * @param promotionContext
     */
    @Override
    public void execute(CartContext cartContext, PromotionRule promotionRule, BigDecimal totalPrice, PromotionContext promotionContext) {
        logger.info("LadderMzStrategy :阶梯满赠： {}", promotionRule.getPromotionRule());
        MzRule mzRule = JSON.parseObject(promotionRule.getPromotionRule(), new TypeReference<MzRule>() {
        });

        if (!mzRule.getType().equals(MZTypeEnums.MUST_SATISFY.getCode())) {
            logger.info("LadderMzStrategy，直接返回");
            return;
        }

        MzRuleInfo mzRuleInfo = null;
        List<MeetInfo> meetInfos = new ArrayList<>(16);
        int count = 0;
        // 获取满足满赠条件中金额最大的赠品
        for (int i = 0; i < mzRule.getMzRuleInfoList().size(); i++) {
            MeetInfo meetInfo = new MeetInfo();
            MzRuleInfo tMzRuleInfo = mzRule.getMzRuleInfoList().get(i);
            logger.info("LadderMzStrategy: totalPrice ={} meetMoney ={}  shopId ={} ssuList={}",
                    totalPrice, tMzRuleInfo.getMeetMoney(),cartContext.getShopId(),JSON.toJSONString(cartContext.getPromotionSsus()));
            if (totalPrice.compareTo(tMzRuleInfo.getMeetMoney()) < 0) {
                meetInfo.setNextMeetMoney(tMzRuleInfo.getMeetMoney());
                meetInfo.setNextDiffMoney(tMzRuleInfo.getMeetMoney().subtract(totalPrice));
                meetInfo.setLevel(i + 1);
                meetInfo.setMeet(false);
                meetInfos.add(meetInfo);
                break;
            }
            mzRuleInfo = tMzRuleInfo;
            if (i < mzRule.getMzRuleInfoList().size()) {
                meetInfo.setMeetMoney(mzRule.getMzRuleInfoList().get(i).getMeetMoney());
                meetInfo.setLevel(i + 1);
                meetInfo.setMeet(true);
                count = 1;
            }
            meetInfos.add(meetInfo);
        }

        MzContext mzContext = super.buildMzContext(cartContext, mzRuleInfo, promotionRule, count);
        mzContext.setMeetInfos(meetInfos);
        promotionContext.getMzContexts().add(mzContext);
    }
}
