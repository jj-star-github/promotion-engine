package com.qmfresh.promotion.template;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.codehaus.groovy.classgen.ReturnAdder;

import com.qmfresh.promotion.dto.IsActGoodParam;
import com.qmfresh.promotion.dto.SearchVipActParam;
import com.qmfresh.promotion.dto.SearchVipSkuParam;
import com.qmfresh.promotion.dto.VipSkuVo;
import com.qmfresh.promotion.entity.PromotionActivityTime;

public abstract class SearchVipSku {
	// 1、查询当日的会员活动
	abstract List<Long> getActTimes();

	// 1.1查询指定时间的会员活动
	abstract List<Long> getActTimesByTime(Integer time);

	// 1.2查询指定门店当日参加活动情况
	abstract List<PromotionActivityTime> getShopActTimesByShop(Integer shopId);

	// 2、查询当日的门店会员活动
	abstract List<Long> getShopAct(Integer shopId, List<Long> ActIds);

	// 3、查询有效的具体活动
	abstract List<Long> getProAct(List<Long> ActIds);

	// 4、根据单品活动规则查询会员商品
	abstract List<Integer> getActSsu(List<Long> ActIds, List<Integer> skuIds);

	// 5、根据分类活动规则查询非会员商品
	abstract List<Integer> getActClassEx(List<Long> ActIds, List<Integer> skuIds);

	// 6、查询会员分类
	abstract List<Integer> getActClass(List<Long> ActIds, List<Integer> class2Ids);

	// 7、会员商品打标
	abstract List<VipSkuVo> markVip(SearchVipActParam pram, List<Integer> vipSkuIds, List<Integer> normalSkuIds,
			List<Integer> class2Ids);

	public final List<VipSkuVo> execute(SearchVipActParam pram) {
		Integer shopId = pram.getShopId();
		List<SearchVipSkuParam> skus = pram.getSkus();
		List<Integer> skuIds = skus.stream().map(SearchVipSkuParam::getSkuId).collect(Collectors.toList());
		List<Integer> class2Ids = skus.stream().map(SearchVipSkuParam::getClass2Id).distinct()
				.collect(Collectors.toList());
		List<VipSkuVo> vipSkuVoList = new ArrayList<>();
		for (SearchVipSkuParam sku : skus) {
			VipSkuVo vipSkuVo = new VipSkuVo();
			vipSkuVo.setIsVip(0);
			vipSkuVo.setSkuId(sku.getSkuId());
			vipSkuVoList.add(vipSkuVo);
		}
		// 1、查询当日的会员活动
		List<Long> actTimes = getActTimes();
		if (actTimes.isEmpty()) {
			return vipSkuVoList;
		}
		// 2、查询当日的门店会员活动
		List<Long> shopAct = getShopAct(shopId, actTimes);
		if (shopAct.isEmpty()) {
			return vipSkuVoList;
		}
		// 3、查询有效的具体活动
		List<Long> proAct = getProAct(shopAct);
		if (proAct.isEmpty()) {
			return vipSkuVoList;
		}
		// 4、根据单品活动规则查询会员商品
		List<Integer> actSsu = getActSsu(proAct, skuIds);

		// 5、根据分类活动规则查询非会员商品
		List<Integer> actClassEx = getActClassEx(proAct, skuIds);

		// 6、查询会员分类
		List<Integer> actClass = getActClass(proAct, class2Ids);
		// 7、会员打标
		List<VipSkuVo> markVip = markVip(pram, actSsu, actClassEx, actClass);
		return markVip;
	}

	// 查询指定时间指定门店存在的会员活动
	public final List<Long> getActbyTime(Integer time, Integer shopId) {
		List<Long> actTimesByTime = getActTimesByTime(time + 1);
		if (actTimesByTime.isEmpty()) {
			return new ArrayList<>();
		}
		// 2、查询当日的门店会员活动
		List<Long> shopAct = getShopAct(shopId, actTimesByTime);
		if (shopAct.isEmpty()) {
			return new ArrayList<>();
		}
		// 3、查询有效的具体活动
		List<Long> proAct = getProAct(shopAct);
		return proAct;
	}

	// 查询指定门店指定商品当前时间有没有参与活动
	public final boolean getShopActbyShop(IsActGoodParam param) {
		// 1、门店当前参与的活动
		List<PromotionActivityTime> shopActTimesByShop = getShopActTimesByShop(param.getShopId());
		if (shopActTimesByShop.isEmpty()) {
			return true;
		}
		List<Long> shopActIdList = shopActTimesByShop.stream().map(PromotionActivityTime::getActivityId)
				.collect(Collectors.toList());
		// 2、查询有效的具体活动
		List<Long> proAct = getProAct(shopActIdList);
		if (proAct.isEmpty()) {
			return true;
		}
		//3.查询商品是否存在活动
		List<Integer> actSsu = getActSsu(proAct,param.getSkuIds());
		if (actSsu.isEmpty()) {
			return true;
		}
		return false;
	}

}
