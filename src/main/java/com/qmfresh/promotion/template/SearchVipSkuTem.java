package com.qmfresh.promotion.template;

import com.qmfresh.promotion.common.utils.TimeUtil;
import com.qmfresh.promotion.dto.SearchVipActParam;
import com.qmfresh.promotion.dto.SearchVipSkuParam;
import com.qmfresh.promotion.dto.VipSkuVo;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionActivityTime;
import com.qmfresh.promotion.enums.ActivityOnlineTypeEnums;
import com.qmfresh.promotion.enums.ActivityStatusTypeEnums;
import com.qmfresh.promotion.enums.PromotionTypeEnums;
import com.qmfresh.promotion.enums.SsuTagTypeEnums;
import com.qmfresh.promotion.manager.IActivityClass2Manager;
import com.qmfresh.promotion.manager.IActivitySsuManager;
import com.qmfresh.promotion.manager.IPromotionActivityManager;
import com.qmfresh.promotion.mapper.PromotionActivityTimeMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author xzw
 */
@Service
@Slf4j
public class SearchVipSkuTem extends SearchVipSku {

    @Resource
    private PromotionActivityTimeMapper promotionActivityTimeMapper;
    @Resource
    private IPromotionActivityManager promotionActivityManager;
    @Resource
    private IActivitySsuManager activitySsuManager;
    @Resource
    private IActivityClass2Manager activityClass2Manager;

    @Override
    List<Long> getActTimes() {
        int secs = TimeUtil.getSecs();
        //今天是星期几
        String dayOfWeek = TimeUtil.getWeekDay();
        List<PromotionActivityTime> actByNowTime = promotionActivityTimeMapper.getActByNowTime(secs, PromotionTypeEnums.VIP.getCode(), dayOfWeek);
        if (actByNowTime.isEmpty()) {
            return new ArrayList<Long>();
        }
        //获取包含今天对应星期的活动

        List<Long> actIds = actByNowTime.stream().map(PromotionActivityTime::getActivityId).collect(Collectors.toList());
        return actIds;
    }

    @Override
    List<Long> getShopAct(Integer shopId, List<Long> actIds) {
        return promotionActivityManager.filterPromotionActivity(shopId, actIds);
    }

    @Override
    List<Long> getProAct(List<Long> actIds) {
        List<PromotionActivity> promotionActivities = promotionActivityManager.filterPromotionActivity(actIds, ActivityOnlineTypeEnums.ONLINE,
                ActivityStatusTypeEnums.STARTED, null, null);
        if (CollectionUtils.isEmpty(promotionActivities)) {
            return new ArrayList<>();
        }
        return promotionActivities.stream().map(PromotionActivity::getId).distinct().collect(Collectors.toList());
    }

    @Override
    List<Integer> getActSsu(List<Long> actIds, List<Integer> skuIds) {
        return activitySsuManager.filterSkuIds(actIds, skuIds, SsuTagTypeEnums.CONTAIN);
    }

    @Override
    List<Integer> getActClassEx(List<Long> actIds, List<Integer> skuIds) {
        return activitySsuManager.filterSkuIds(actIds, skuIds, SsuTagTypeEnums.EXCLUSIVE);
    }

    @Override
    List<Integer> getActClass(List<Long> actIds, List<Integer> class2Ids) {
        return activityClass2Manager.filterClassIds(actIds, class2Ids);
    }

    @Override
    List<VipSkuVo> markVip(SearchVipActParam pram, List<Integer> vipSkuIds, List<Integer> normalSkuIds,
                           List<Integer> class2Ids) {
        //将参数转化为返回对象
        List<SearchVipSkuParam> skus = pram.getSkus();
        List<VipSkuVo> vipSkuVoList = new ArrayList<>();
        //将所有会员分类的商品标记为会员品
        //是会员的分类商品
        List<SearchVipSkuParam> isvips = skus.stream().filter(vipSkuVo -> class2Ids.contains(vipSkuVo.getClass2Id())).collect(Collectors.toList());
        List<SearchVipSkuParam> notVip = isvips.stream().filter(v -> normalSkuIds.contains(v.getSkuId())).collect(Collectors.toList());
        //不是会员的分类商品
        List<SearchVipSkuParam> notVipClass2 = skus.stream().filter(vipSkuVo -> !class2Ids.contains(vipSkuVo.getClass2Id())).collect(Collectors.toList());
        notVip.addAll(notVipClass2);
        List<SearchVipSkuParam> vips = notVip.stream().filter(v -> vipSkuIds.contains(v.getSkuId())).collect(Collectors.toList());
        isvips.addAll(vips);
        notVip.removeAll(vips);
        if (!isvips.isEmpty()) {
            for (SearchVipSkuParam para : isvips) {
                VipSkuVo vipSkuVo = new VipSkuVo();
                vipSkuVo.setIsVip(1);//是会员
                vipSkuVo.setSkuId(para.getSkuId());
                vipSkuVo.setClass2Id(para.getClass2Id());
                vipSkuVoList.add(vipSkuVo);
            }
        }
        if (!notVip.isEmpty()) {
            for (SearchVipSkuParam para : notVip) {
                VipSkuVo vipSkuVo = new VipSkuVo();
                vipSkuVo.setIsVip(0);//不是会员
                vipSkuVo.setSkuId(para.getSkuId());
                vipSkuVo.setClass2Id(para.getClass2Id());
                vipSkuVoList.add(vipSkuVo);
            }
        }
        return vipSkuVoList;
    }

    @Override
    List<Long> getActTimesByTime(Integer time) {
        //时间戳转换为星期
        String timeToWeekday = TimeUtil.timeToWeekday(time);
        List<PromotionActivityTime> actByTime = promotionActivityTimeMapper.getActByTime(time, PromotionTypeEnums.VIP.getCode(), timeToWeekday);
        if (actByTime.isEmpty()) {
            return new ArrayList<Long>();
        }
        List<Long> actIds = actByTime.stream().map(PromotionActivityTime::getActivityId).collect(Collectors.toList());
        return actIds;
    }

    @Override
    List<PromotionActivityTime> getShopActTimesByShop(Integer shopId) {
        List<PromotionActivityTime> shopActTimesByShop = promotionActivityTimeMapper.getShopActTimesByShop(shopId, TimeUtil.getSecs());
        if (shopActTimesByShop.isEmpty()) {
            return new ArrayList<>();
        }
        return shopActTimesByShop;
    }

}
