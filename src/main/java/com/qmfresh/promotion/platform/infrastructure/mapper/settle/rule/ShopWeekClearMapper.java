package com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopWeekClear;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopWeekSum;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

public interface ShopWeekClearMapper extends BaseMapper<ShopWeekClear> {

    /**
     * 查询周进货值
     * @param shopId
     * @param createDayStart
     * @param createDayEnd
     * @return
     */
    ShopWeekSum querySumpriceAmount(@Param("shopId")Integer shopId, @Param("createDayStart")String createDayStart, @Param("createDayEnd")String createDayEnd);
}
