package com.qmfresh.promotion.platform.infrastructure.message;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.support.MqLog;
import com.qmfresh.promotion.platform.domain.shared.BizCode;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.nio.charset.Charset;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Slf4j
public class RocketMQSender {

    /**
     * 发送消息
     *
     * @param producer 消息生产者
     * @param topic    消息主题
     * @param message  消息体内容
     * @param <T>      消息体类型
     */
    public static <T> void doSend(DefaultMQProducer producer, String topic, T message) {
        doSend(producer, topic, message,  new  MqLog.Noop());
    }

    public static <T> void doSend(DefaultMQProducer producer, String topic, T message, MqLog.MessageCallback callback) {
        log.info("消息发送参数：topic = {} message = {}", topic, JSON.toJSONString(message));
        if (producer == null) {
            log.error("必须设置消息生产者：topic = {} message = {}", topic, JSON.toJSONString(message));
            throw new BusinessException(BizCode.ERROR.getCode(), "消息生产者为空");
        }

        Message msg = new Message();
        msg.setTopic(topic);
        msg.setBody(JSON.toJSONString(message).getBytes(Charset.forName("utf-8")));
        try {
            SendResult sendResult = producer.send(msg);
            callback.success(sendResult, msg);
            log.info("消息发送结果：msgId = {} sendResult = {}", sendResult.getMsgId(), JSON.toJSONString(sendResult));
        } catch (MQClientException e) {
            callback.exception(e, msg);
            log.error("发送消息客户端异常：topic = {} message = {}", topic, JSON.toJSONString(message), e);
        } catch (RemotingException e) {
            callback.exception(e, msg);
            log.error("远程消息服务端异常：topic = {} message = {}", topic, JSON.toJSONString(message), e);
        } catch (MQBrokerException e) {
            callback.exception(e, msg);
            log.error("消息broker中间件异常：topic = {} message = {}", topic, JSON.toJSONString(message), e);
        } catch (InterruptedException e) {
            callback.exception(e, msg);
            log.error("rocketmq中断：topic = {}  message = {}", topic, JSON.toJSONString(message), e);
        }
    }
}
