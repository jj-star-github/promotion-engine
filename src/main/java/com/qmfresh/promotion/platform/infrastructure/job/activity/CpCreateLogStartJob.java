package com.qmfresh.promotion.platform.infrastructure.job.activity;

import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLogManager;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
@JobHandler("CpCreateLogStartJob")
public class CpCreateLogStartJob extends IJobHandler {

    @Resource
    private CpCreateLogManager cpCreateLogManager;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        log.info("xxljob开始执行CpCreateLogStartJob：{}", param);
        cpCreateLogManager.startCpCreateLogJob();
        log.info("xxljob开始执行CpCreateLogStartJob结束：{}", param);
        return ReturnT.SUCCESS;
    }
}
