package com.qmfresh.promotion.platform.infrastructure.job.settle;

import com.qmfresh.promotion.platform.domain.model.settle.apportion.ApportionManager;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Author zbr
 * @Date 9:23 2020/8/28
 */
@Component
@Slf4j
@JobHandler("ShopDayReceiveJob")
public class ShopDayReceiveJob extends IJobHandler {

    @Resource
    private ApportionManager apportionManager;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        log.info("开始执行ShopDayReceiveJob...");
        apportionManager.dayWmsReceive();
        log.info("结束执行ShopDayReceiveJob...");
        return ReturnT.SUCCESS;
    }
}
