package com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 门店日流水统计
 */
@Data
public class FundsShopDayChangeStatis implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 总销售额
     */
    private Long accountId;
    /**
     * 营销销售额
     */
    private String createDay;
    /**
     * 新增营销费用
     */
    private BigDecimal newAmount;
    /**
     * 使用营销费用
     */
    private BigDecimal useAmount;

}
