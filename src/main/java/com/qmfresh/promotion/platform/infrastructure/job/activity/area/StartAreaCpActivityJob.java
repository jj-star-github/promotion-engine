package com.qmfresh.promotion.platform.infrastructure.job.activity.area;

import com.qmfresh.promotion.platform.domain.model.activity.cp.area.AreaCpActivityManager;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
@JobHandler("StartAreaCpActivityJob")
public class StartAreaCpActivityJob extends IJobHandler {

    @Resource
    private AreaCpActivityManager areaCpActivityManager;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        log.info("开始启动大区活动...");

        areaCpActivityManager.startAreaCpActivityByJob();
        log.info("结束启动大区活动...");

        return ReturnT.SUCCESS;
    }
}
