package com.qmfresh.promotion.platform.infrastructure.job.activity.shop;

import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.action.StartCpAction;
import com.qmfresh.promotion.platform.domain.model.activity.cp.context.CpContext;
import com.qmfresh.promotion.platform.domain.model.activity.cp.context.CpContextFactory;
import com.qmfresh.promotion.platform.domain.model.activity.cp.context.ShopCpContext;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivityManager;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */

@Component
@Slf4j
@JobHandler("StartShopCpActivityJob")
public class StartShopCpActivityJob extends IJobHandler {

    @Resource
    private CpContextFactory cpContextFactory;

    @Resource
    private ShopCpActivityManager cpActivityManager;


    @Override
    public ReturnT<String> execute(String param) throws Exception {
        log.info("开始执行StartShopCpActivityJob... {}", param);
//        Operator operator = Operator.createSystemOperator();
//        CpContext shopCpContext = cpContextFactory.createShopCp(operator, StartCpAction.createCommand());
//        if (shopCpContext instanceof ShopCpContext) {
//            ShopCpContext cpContext = (ShopCpContext) shopCpContext;
//            cpContext.start(cpContext);
//        }
        cpActivityManager.startByJob();
        log.info("结束执行StartShopCpActivityJob...");
        return ReturnT.SUCCESS;
    }
}
