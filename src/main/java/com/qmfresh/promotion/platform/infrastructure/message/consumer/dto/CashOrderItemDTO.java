package com.qmfresh.promotion.platform.infrastructure.message.consumer.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zbr
 * @Date 15:45 2020/8/22
 */
@Data
public class CashOrderItemDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 订单主表外键
     */
    private Long orderId;
    private Integer skuId;
    /**
     * 优惠价格
     */
    private BigDecimal discount;
    /**
     * 规格，传0
     */
    private Integer optionId;
    /**
     * 均摊金额
     */
    private BigDecimal orderDiscount;
    /**
     * 现价，现价*数量
     */
    private BigDecimal realPrice;
    /**
     * 产品小计价格，实际价格*数量
     * 原金额（应收的）
     */
    private BigDecimal subTotal;
    /**
     * 数量（重量）
     */
    private BigDecimal total;
    /**
     * 抹零分摊
     */
    private BigDecimal freePriceShare;
    /**
     * 番茄币分摊数
     */
    private Integer tomatoCoinShare;
    /**
     * 番茄币分摊
     */
    private BigDecimal tomatoCoinDiscount;
    /**
     * 优惠券分摊
     */
    private BigDecimal couponshare;
    /**
     * 商品原价（初始原价）
     */
    private Long advicePrice;
    /**
     * 商品售价（普通商品收银机显示价格）
     */
    private BigDecimal sellPrice;
    /**
     * 单品价格获取列表字符串（接收方可转活动对象列表）
     */
    private String actList;


}
