package com.qmfresh.promotion.platform.infrastructure.message.consumer.handle;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.settle.apportion.ApportionManager;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.dto.CashOrderDTO;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.dto.WeekClearDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class WeekClearHandle {
    @Resource
    private ApportionManager apportionManager;

    public void handleMessage(WeekClearDTO weekClearDTO) {
        log.info("接收周清业务，参数:" + JSON.toJSONString(weekClearDTO));
        apportionManager.shopWeekClear(weekClearDTO);
        log.info("结束周清业务，参数:" + JSON.toJSONString(weekClearDTO));
    }
}
