package com.qmfresh.promotion.platform.infrastructure.mapper.activity;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityConditionPO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 奖品发放记录
 */
public interface PromActivityConditionMapper extends BaseMapper<PromActivityConditionPO> {

    /**
     * 根据活动查询
     * @param activityId
     * @return
     */
    List<PromActivityConditionPO> queryByActivity(@Param("activityId")Long activityId, @Param("stock") Integer stock);

    /**
     * 根据活动
     * @param activityIdList
     * @return
     */
    List<PromActivityConditionPO> queryByActivityList(@Param("activityIdList")List<Long> activityIdList, @Param("stock") Integer stock);

    /**
     * 跟新奖品库存
     * @param id
     * @param sendNum
     */
    void updateStock(@Param("id")Long id, @Param("sendNum") int sendNum);

    /**
     * 清空库存
     * @param id
     */
    void clearStock(@Param("id")Long id);
}