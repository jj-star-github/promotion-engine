package com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ShopWeekSum implements Serializable {
    private static final long serialVersionUID = 1L;

    private BigDecimal priceAmount;
}
