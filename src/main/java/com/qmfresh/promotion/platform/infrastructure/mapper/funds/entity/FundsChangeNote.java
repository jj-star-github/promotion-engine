package com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 营销费用交易流水
 */
@TableName("t_funds_change_note")
@Data
@ToString
public class FundsChangeNote implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private Long accountId;
    private Integer shopId;
    private Integer shopType;
    private Integer targetShop;
    private BigDecimal changeBefore;
    private BigDecimal changeAmount;
    private Integer changeType;
    private BigDecimal changeAfter;
    private Integer businessType;
    private Long activityId;
    private Integer isSkuClean;
    private String businessId;
    private String businessCode;
    private BigDecimal businessRealAmount;
    private BigDecimal businessMarketingAmount;
    private BigDecimal businessGmv;
    private String createDay;
    private String remark;
    private Date gmtCreate;
    private Date gmtModified;
    private Integer isDeleted;

}
