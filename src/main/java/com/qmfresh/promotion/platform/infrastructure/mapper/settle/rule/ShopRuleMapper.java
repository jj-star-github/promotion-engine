package com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopPromotion;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopPromotionRule;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto.QueryRuleInfoReqDTO;

import java.util.List;

/**
 * @Author zbr
 * @Date 15:46 2020/8/22
 */
public interface ShopRuleMapper extends BaseMapper<ShopPromotionRule> {

    /**
     * 根据条件查询
     */
    List<ShopPromotion> queryRuleInfoList(QueryRuleInfoReqDTO param);
}
