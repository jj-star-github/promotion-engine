package com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qmfresh.promotion.platform.domain.model.activity.Subject;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLog;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLogCompletedStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLogStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLogType;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.CpCreateLogMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.function.BiConsumer;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@TableName("t_cp_create_log")
@Slf4j
public class CpCreateLogPO implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;
    private Integer status;
    private Integer completeStatus;
    private String name;
    private String request;
    private String remark;
    private Date gmtCreate;
    private Date gmtModified;
    private Integer operatorId;
    private String operatorName;
    private Integer type;
    private Long activityId;
    private Integer isDeleted;

    public static CpCreateLogPO create(CpCreateLog cpCreateLog) {
        CpCreateLogPO cpCreateLogPO = new CpCreateLogPO();
        cpCreateLogPO.setStatus(cpCreateLog.getStatus().getCode());
        cpCreateLogPO.setCompleteStatus(cpCreateLog.getCompletedStatus().getCode());
        cpCreateLogPO.setName(cpCreateLog.getName());
        cpCreateLogPO.setRequest(cpCreateLog.getRequest());
        cpCreateLogPO.setGmtCreate(cpCreateLog.getGmtCreate());
        cpCreateLogPO.setGmtModified(cpCreateLog.getGmtModified());
        cpCreateLogPO.setOperatorId(cpCreateLog.getOperator().getOperatorId());
        cpCreateLogPO.setOperatorName(cpCreateLog.getOperator().getOperatorName());
        cpCreateLogPO.setType(cpCreateLog.getLogType().getCode());
        cpCreateLogPO.setActivityId(cpCreateLog.getActivityId());
        cpCreateLogPO.setIsDeleted(0);
        cpCreateLogPO.setId(cpCreateLog.getId());
        return cpCreateLogPO;
    }

    public static CpCreateLog convert(CpCreateLogPO s) {
        return CpCreateLog.builder()
                .id(s.getId())
                .request(s.getRequest())
                .logType(CpCreateLogType.from(s.getType()))
                .activityId(s.getActivityId())
                .operator(Operator.create(s.getOperatorId(), s.getOperatorName(), Subject.create(Subject.Platform.INTANCE)))
                .name(s.getName())
                .gmtCreate(s.getGmtCreate())
                .gmtModified(s.getGmtModified())
                .status(CpCreateLogStatus.from(s.getStatus()))
                .completedStatus(CpCreateLogCompletedStatus.COMPLETED.from(s.getStatus()))
                .remark(s.getRemark())
                .build();
    }

    public static <Context> void visit(CpCreateLogMapper mapper, LambdaQueryWrapper<CpCreateLogPO> queryWrapper,
                                       Context context,
                                       BiConsumer<CpCreateLogPO, Context> biConsumer) {
        queryWrapper.gt(CpCreateLogPO::getId, 0);
        queryWrapper.orderByAsc(CpCreateLogPO::getId);

        //查询500条，根据id排序
        Page<CpCreateLogPO> page = new Page<>(1, 500);
        IPage<CpCreateLogPO> changePriceActivityPOIPage = mapper
                .selectPage(page, queryWrapper);
        while (CollectionUtils.isNotEmpty(changePriceActivityPOIPage.getRecords())
                && changePriceActivityPOIPage.getRecords().size() == 500
        ) {
            //遍历处理业务（业务对象 + 当前上下文）
            for (CpCreateLogPO changePriceActivityPO : changePriceActivityPOIPage.getRecords()) {
                try {
                    biConsumer.accept(changePriceActivityPO, context);
                } catch (Exception e) {
                    log.warn("业务处理失败：{}", JSON.toJSONString(changePriceActivityPO), e);
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            int size = changePriceActivityPOIPage.getRecords().size();
            queryWrapper.ge(CpCreateLogPO::getId, changePriceActivityPOIPage.getRecords().get(size - 1).getId());
            changePriceActivityPOIPage = mapper.selectPage(page, queryWrapper);
        }
        //没有满足500，最后处理一遍
        if (CollectionUtils.isNotEmpty(changePriceActivityPOIPage.getRecords())) {
            for (CpCreateLogPO changePriceActivityPO : changePriceActivityPOIPage.getRecords()) {
                try {
                    biConsumer.accept(changePriceActivityPO, context);
                } catch (Exception e) {
                    log.warn("业务处理失败：{}", JSON.toJSONString(changePriceActivityPO), e);
                }
            }
        }
    }
}
