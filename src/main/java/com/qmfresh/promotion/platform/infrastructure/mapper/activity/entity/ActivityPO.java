package com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qmfresh.promotion.platform.domain.model.activity.Activity;
import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.Sku;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.action.PlanCpAction;
import com.qmfresh.promotion.platform.domain.model.activity.cp.context.ShopCpContext;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@TableName("t_activity")
@Data
public class ActivityPO implements Serializable {

    /**
     * 营销活动id
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 营销活动创建事件
     */
    private Date gmtCreate;
    /**
     * 营销活动修改事件
     */
    private Date gmtModified;
    /**
     * 1=表示已删除
     */
    private Integer isDeleted;
    /**
     * 版本信息
     */
    private Integer version;
    /**
     * @see com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType
     */
    private Integer type;
    /**
     * 营销活动名称
     */
    private String name;
    /**
     * 触发规则
     */
    private String triggerRule;
    /**
     * 限制规则
     */
    private String limitRule;
    /**
     * 结算规则
     */
    private String settleRule;


    /**
     * 产生活动信息
     *
     * @param shopCpContext 活动PO
     * @return 活动
     */
    public static ActivityPO create(ShopCpContext shopCpContext) {
        PlanCpAction planCpAction = (PlanCpAction) shopCpContext.getCpAction();
        ActivityPO activityPO = new ActivityPO();
        activityPO.setSettleRule("");
        activityPO.setLimitRule("");
        activityPO.setTriggerRule("");
        activityPO.setName(planCpAction.getSku().getSkuId() + "-" + planCpAction.getSku().getSkuName());
        activityPO.setGmtCreate(new Date());
        activityPO.setGmtModified(new Date());
        activityPO.setVersion(0);
        activityPO.setIsDeleted(0);
        activityPO.setType(CpBizType.CP_SHOP.getCode());
        return activityPO;
    }

    public static ActivityPO create(String activityName) {
        return c(activityName, CpBizType.CP_PLATFORM);
    }

    public static ActivityPO createAreaCp(String name) {
        return c(name, CpBizType.CP_AREA);
    }

    public static ActivityPO c(String name, CpBizType bizType) {
        ActivityPO activityPO = new ActivityPO();
        activityPO.settleRule = "";
        activityPO.limitRule = "";
        activityPO.triggerRule = "";
        activityPO.name = name;
        activityPO.gmtCreate = new Date();
        activityPO.gmtModified = new Date();
        activityPO.version = 0;
        activityPO.isDeleted = 0;
        activityPO.type = bizType.getCode();
        return activityPO;
    }
}
