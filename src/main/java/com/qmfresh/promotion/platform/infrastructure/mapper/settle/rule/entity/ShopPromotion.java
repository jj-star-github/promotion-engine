package com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@ToString
@Data
public class ShopPromotion implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;
    /**
     * 门店等级(123)
     */
    private Integer shopGrade;
    /**
     * 门店类别(1.A,2.B,3.C)
     */
    private Integer shopType;
    /**
     * 损耗率
     */
    private BigDecimal lossPercent;
    /**
     * 营销费率
     */
    private BigDecimal promotionPercent;
    /**
     * 总部占比
     */
    private BigDecimal basePercent;
    /**
     * 大区占比
     */
    private BigDecimal areaPercent;
    /**
     * 门店占比
     */
    private BigDecimal shopPercent;
    /**
     * 操作人id
     */
    private Integer operatorId;
    /**
     * 操作人名称
     */
    private String operatorName;
    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date gmtModified;

    /**
     * 周清比例
     */
    private BigDecimal weekClearPercent;
    /**
     * 门店周清比例
     */
    private BigDecimal weekClearShopPercent;
    /**
     * 大区周清比例
     */
    private BigDecimal weekClearAreaPercent;
}
