package com.qmfresh.promotion.platform.infrastructure.mapper.activity;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityPO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityQueryDTO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 活动基础信息
 */
public interface PromActivityMapper extends BaseMapper<PromActivityPO> {

    /**
     *
     * @param id
     * @return
     */
    PromActivityPO queryById(@Param("id")Long id);

    /**
     *
     * @param param
     * @return
     */
    Integer queryCountByParam(@Param("p")PromActivityQueryDTO param);

    /**
     *  查询活动信息
     */
    List<PromActivityPO> queryByParam(@Param("p")PromActivityQueryDTO param, @Param("start")Integer start, @Param("length")Integer length);

    /**
     * 查询
     * @param param
     * @return
     */
    List<PromActivityPO> queryActivityIdByParam(@Param("p")PromActivityPO param);

    /**
     * 活动设为无效
     * @param activityId
     * @param lastOperatorName
     * @param lastOperatorId
     * @param gmtModified
     */
    void noAvail(@Param("activityId")Long activityId, @Param("lastOperatorName") String lastOperatorName, @Param("lastOperatorId") Integer lastOperatorId, @Param("gmtModified")Date gmtModified);

    /**
     * 查询有效活动信息
     * @param now
     * @return
     */
    List<PromActivityPO> queryActivity(@Param("now")Date now, @Param("shopType")Integer shopType, @Param("userTypeList")List userTypeList);

    /**
     * 查询过期活动信息
     * @param now
     * @return
     */
    List<PromActivityPO> queryTimeOutActivity(@Param("now")Date now);

    /**
     * 查询未开启的活动信息
     * @param now
     * @return
     */
    List<PromActivityPO> queryNoOpenActivity(@Param("now")Date now);

    /**
     * 批量更新活动状态
     * @param idList
     * @param status
     */
    void updateStatus(@Param("idList")List<Long> idList,@Param("status")Integer status);

}