package com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityTargetShopDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 活动目标门店
 */
@TableName("t_prom_activity_target_shop")
@Data
public class PromActivityTargetShopPO implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 活动ID
     */
    private Long activityId;
    /**
     *  门店ID
     */
    private Integer shopId;

    private String shopName;
    /**
     * 门店类型
     */
    private Integer shopType;

    private Date gmtCreate;
    private Date gmtModified;
    private Integer isDeleted;

    public PromActivityTargetShopPO(PromActivityTargetShopDTO dto) {
        this.setShopId(dto.getShopId());
        this.setShopName(dto.getShopName());
        this.setShopType(dto.getShopType());
        Date now = new Date();
        this.setGmtCreate(now);
        this.setGmtModified(now);
    }

}
