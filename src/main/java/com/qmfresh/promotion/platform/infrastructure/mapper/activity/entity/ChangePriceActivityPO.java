package com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.Sku;
import com.qmfresh.promotion.platform.domain.model.activity.SkuPrice;
import com.qmfresh.promotion.platform.domain.model.activity.cp.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.action.PlanCpAction;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.AreaCpActivity;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.AreaCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.command.AreaCpCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.command.CpCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.context.ShopCpContext;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpActivity;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.SkuCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.command.PromotionPriceCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.ChangePriceActivityMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@TableName("t_activity_change_price")
@Slf4j
public class ChangePriceActivityPO implements Serializable {

    private Long id;
    /**
     * @see com.qmfresh.promotion.platform.domain.model.activity.Activity
     */
    private Long activityId;
    /**
     * @see CpBizType
     */
    private Integer type;
    private String name;
    /**
     * @see CpStatus
     */
    private Integer status;
    /**
     * @see AuditStatus
     */
    private Integer auditStatus;
    private Long auditPersonId;
    private String auditPersonName;
    private String auditRemark;
    /**
     * 计划开始时间
     */
    private Date planActivityStartTime;
    /**
     * 计划结束时间
     */
    private Date planActivityEndTime;
    /**
     * 实际活动开始时间
     */
    private Date realActivityStartTime;
    /**
     * 实际活动结束时间
     */
    private Date realActivityEndTime;
    /**
     * 备注
     */
    private String remark;
    private Date gmtCreate;
    private Date gmtModified;
    private Integer isDeleted;
    private Integer version;
    private Long createPersonId;
    private String createPersonName;
    private Long lastOperatorId;
    private String lastOperatorName;
    private Integer subjectId;

    /**
     * 门店信息
     */
    private Integer shopId;
    private String shopName;
    private Integer shopType;
    private Integer shopCityId;
    private Integer shopAreaId;
    /**
     * 商品id
     */
    private Integer skuId;
    /**
     * 商品名称
     */
    private String skuName;
    /**
     * 商品一个类目
     */
    private String class1Name;
    /**
     * 商品二级类目
     */
    private String class2Name;
    /**
     * 商品三级类目
     */
    private String class3Name;
    /**
     * 商品图片
     */
    private String skuImage;
    /**
     * 商品格式
     */
    private String skuFormat;

    private String unitFormat;

    private BigDecimal rulePrice;
    /**
     * 当前售价
     */
    private BigDecimal currentPrice;
    private BigDecimal originalPrice;
    /**
     * 营销费用差值
     */
    private BigDecimal marketingDiffPrice;
    /**
     * 同步状态
     */
    private Integer synStatus;
    private BigDecimal planMarketingAmount;
    private Integer cpType;

    @TableField(exist = false)
    private Integer skuCount;
    @TableField(exist = false)
    private Integer shopCount;
    @TableField(exist = false)
    private BigDecimal sumPlanMarketingFee;

    private String bizCode;

    /**
     * 从上下文创建改价对象
     *
     * @param cpContext
     * @return
     */
    public static ChangePriceActivityPO create(ShopCpContext cpContext, ActivityPO activityPo) {
        Shop shop = cpContext.getShop();
        Operator operator = cpContext.getOperator();
        PlanCpAction cpAction = (PlanCpAction) cpContext.getCpAction();
        ChangePriceActivityPO changePriceActivityPO = ChangePriceActivityPO.create(activityPo, shop, operator, cpAction);
        return changePriceActivityPO;
    }

    private static ChangePriceActivityPO create(ActivityPO activityPO, Shop shop, Operator operator, PlanCpAction cpAction) {
        ChangePriceActivityPO changePriceActivityPO = new ChangePriceActivityPO();
        changePriceActivityPO.setActivityId(activityPO.getId());

        changePriceActivityPO.setType(CpBizType.CP_SHOP.getCode());
        changePriceActivityPO.setName(activityPO.getName());

        changePriceActivityPO.setStatus(CpStatus.CREATE.getCode());

        changePriceActivityPO.setAuditStatus(AuditStatus.AUDIT_SUCCESS.getCode());
        changePriceActivityPO.setAuditPersonId(operator.getOperatorId().longValue());
        changePriceActivityPO.setAuditPersonName(operator.getOperatorName());
        changePriceActivityPO.setAuditRemark("门店改价默认审核通过");

        changePriceActivityPO.setCreatePersonId(operator.getOperatorId().longValue());
        changePriceActivityPO.setCreatePersonName(operator.getOperatorName());
        changePriceActivityPO.setCurrentPrice(BigDecimal.ZERO);
        changePriceActivityPO.setGmtCreate(new Date());
        changePriceActivityPO.setGmtModified(new Date());
        changePriceActivityPO.setLastOperatorId(operator.getOperatorId().longValue());
        changePriceActivityPO.setLastOperatorName(operator.getOperatorName());
        changePriceActivityPO.setMarketingDiffPrice(BigDecimal.ZERO);
        changePriceActivityPO.setPlanActivityEndTime(cpAction.getCpTimeCycle().getEndTime());
        changePriceActivityPO.setPlanActivityStartTime(cpAction.getCpTimeCycle().getBeginTime());
        changePriceActivityPO.setRulePrice(cpAction.getRulePrice());
        changePriceActivityPO.setCpType(CpType.DIFF.getCode());

        changePriceActivityPO.setSkuId(cpAction.getSku().getSkuId());
        changePriceActivityPO.setSkuName(cpAction.getSku().getSkuName());
        changePriceActivityPO.setSkuFormat(cpAction.getSku().getUnitFormat());
        changePriceActivityPO.setSkuImage(cpAction.getSku().getSkuImage());
        changePriceActivityPO.setClass1Name(cpAction.getSku().getClass1Name());
        changePriceActivityPO.setClass2Name(cpAction.getSku().getClass2Name());
        changePriceActivityPO.setClass3Name(cpAction.getSku().getClass3Name());
        changePriceActivityPO.setUnitFormat(cpAction.getSku().getUnitFormat());

        changePriceActivityPO.setPlanMarketingAmount(cpAction.getPlanMarketingFee());

        changePriceActivityPO.setShopId(shop.getShopId());
        changePriceActivityPO.setShopType(shop.getShopType());
        changePriceActivityPO.setShopCityId(shop.getCityId());
        changePriceActivityPO.setShopAreaId(shop.getAreaId());

        changePriceActivityPO.setOriginalPrice(BigDecimal.ZERO);

        changePriceActivityPO.setSubjectId(shop.getShopId());
        changePriceActivityPO.setSynStatus(0);
        changePriceActivityPO.setShopName(shop.getShopName());

        changePriceActivityPO.setIsDeleted(0);
        return changePriceActivityPO;
    }


    public static Boolean hasClosed(ChangePriceActivityPO changePriceActivityPO) {
        if (
                CpStatus.CANCELED.getCode() == changePriceActivityPO.getStatus()
                        || CpStatus.CLOSED.getCode() == changePriceActivityPO.getStatus()
                        || CpStatus.FINISHED.getCode() == changePriceActivityPO.getStatus()
        ) {
            return true;
        }
        return false;
    }

    /**
     * 将改价持久对象转换为领域对象
     *
     * @param changePriceActivityPO
     * @return
     */
    public static ShopCpActivity shopCpActivity(ChangePriceActivityPO changePriceActivityPO) {
        ShopCpActivity shopCpActivity = ShopCpActivity.builder().build();
        shopCpActivity.setActivityId(changePriceActivityPO.getActivityId());
        shopCpActivity.setActivityCpId(changePriceActivityPO.getId());
        shopCpActivity.setName(changePriceActivityPO.getName());
        shopCpActivity.setBizType(CpBizType.from(changePriceActivityPO.getType()));
        shopCpActivity.setCpType(CpType.from(changePriceActivityPO.getCpType()));
        shopCpActivity.setShop(ChangePriceActivityPO.getShop(changePriceActivityPO));
        shopCpActivity.setSku(getSku(changePriceActivityPO));
        shopCpActivity.setCreatePersonId(changePriceActivityPO.getCreatePersonId());
        shopCpActivity.setCreatePersonName(changePriceActivityPO.getCreatePersonName());
        shopCpActivity.setPlanMarketingFee(changePriceActivityPO.getPlanMarketingAmount());
        shopCpActivity.setPlanActivityStartTime(changePriceActivityPO.getPlanActivityStartTime());
        shopCpActivity.setPlanActivityEndTime(changePriceActivityPO.getPlanActivityEndTime());
        shopCpActivity.setCreateTime(changePriceActivityPO.getGmtCreate());
        shopCpActivity.setCurrentPrice(changePriceActivityPO.getCurrentPrice());
        shopCpActivity.setCpStatus(CpStatus.from(changePriceActivityPO.getStatus()));
        shopCpActivity.setRulePrice(changePriceActivityPO.getRulePrice());
        shopCpActivity.setOriginalPrice(changePriceActivityPO.getOriginalPrice());
        shopCpActivity.setMarketingDiffFee(changePriceActivityPO.getMarketingDiffPrice());
        shopCpActivity.setLastOperatorPerson(changePriceActivityPO.getLastOperatorName());
        return shopCpActivity;
    }

    public static SkuCpItem skuCpItem(ChangePriceActivityPO changePriceActivityPO) {
        return SkuCpItem.create(changePriceActivityPO.getClass1Name(),
                changePriceActivityPO.getClass2Name(),
                changePriceActivityPO.getClass3Name(),
                changePriceActivityPO.getSkuId(),
                changePriceActivityPO.getSkuName(),
                changePriceActivityPO.getRulePrice(),
                changePriceActivityPO.getActivityId());
    }

    /**
     * 从底层数据获取sku信息
     *
     * @param changePriceActivityPO
     * @return
     */
    public static Sku getSku(ChangePriceActivityPO changePriceActivityPO) {
        Sku sku = new Sku();
        sku.setSkuId(changePriceActivityPO.getSkuId());
        sku.setClass1Name(changePriceActivityPO.getClass1Name());
        sku.setClass2Name(changePriceActivityPO.getClass2Name());
        sku.setClass3Name(changePriceActivityPO.getClass3Name());
        sku.setSkuFormat(changePriceActivityPO.getSkuFormat());
        sku.setSkuImage(changePriceActivityPO.getSkuImage());
        sku.setSkuName(changePriceActivityPO.getSkuName());
        sku.setUnitFormat(changePriceActivityPO.getUnitFormat());
        return sku;
    }

    public static Shop getShop(ChangePriceActivityPO changePriceActivityPO) {
        Shop shop = Shop.builder()
                .shopId(changePriceActivityPO.getShopId())
                .shopType(changePriceActivityPO.getShopType())
                .shopName(changePriceActivityPO.getShopName())
                .cityId(changePriceActivityPO.getShopCityId())
                .areaId(changePriceActivityPO.getShopAreaId())
                .build();

        return shop;
    }

    /**
     * 平台改价
     *
     * @param changePriceActivityPO
     * @return
     */
    public static GeneralCpActivity generalCpActivity(ChangePriceActivityPO changePriceActivityPO) {
        GeneralCpActivity generalCpActivity = GeneralCpActivity.builder()
                .activityId(changePriceActivityPO.getActivityId())
                .activityName(changePriceActivityPO.getName())
                .areaId(changePriceActivityPO.getShopAreaId())
                .cpBizType(CpBizType.from(changePriceActivityPO.getType()))
                .cpTimeCycle(new CpTimeCycle(changePriceActivityPO.getPlanActivityStartTime(), changePriceActivityPO.getPlanActivityEndTime()))
                .cpStatus(CpStatus.from(changePriceActivityPO.getStatus()))
                .shopCount(changePriceActivityPO.getShopCount())
                .skuCount(changePriceActivityPO.getSkuCount())
                .sumPlanMarketingFee(changePriceActivityPO.getSumPlanMarketingFee())
                .lastModifiedPerson(changePriceActivityPO.getLastOperatorName())
                .createTime(changePriceActivityPO.getGmtCreate())
                .build();
        return generalCpActivity;
    }

    public static ChangePriceActivityPO createGeneralCp(Operator operator,
                                                        Shop shop,
                                                        Sku sku,
                                                        SkuPrice skuPrice,
                                                        SkuCpItem skuCpItems,
                                                        Long activityId,
                                                        String activityName,
                                                        CpTimeCycle cpTimeCycle) {
        return createGeneralCp(operator,
                shop,
                sku,
                skuPrice,
                skuCpItems,
                activityId,
                activityName,
                cpTimeCycle,
                null);
    }

    public static ChangePriceActivityPO createGeneralCp(Operator operator,
                                                        Shop shop,
                                                        Sku sku,
                                                        SkuPrice skuPrice,
                                                        SkuCpItem skuCpItems,
                                                        Long activityId,
                                                        String activityName,
                                                        CpTimeCycle cpTimeCycle,
                                                        String bizCode) {

        ChangePriceActivityPO changePriceActivityPO = new ChangePriceActivityPO();

        changePriceActivityPO.activityId = activityId;
        changePriceActivityPO.name = activityName;


        changePriceActivityPO.createPersonId = operator.getOperatorId().longValue();
        changePriceActivityPO.createPersonName = operator.getOperatorName();
        changePriceActivityPO.type = CpBizType.CP_PLATFORM.getCode();

        changePriceActivityPO.shopId = shop.getShopId();
        changePriceActivityPO.shopName = shop.getShopName();
        changePriceActivityPO.shopAreaId = shop.getAreaId();
        changePriceActivityPO.shopCityId = shop.getCityId();
        changePriceActivityPO.shopType = shop.getShopType();

        changePriceActivityPO.skuFormat = sku.getSkuFormat();
        changePriceActivityPO.skuId = sku.getSkuId();
        changePriceActivityPO.skuName = sku.getSkuName();
        changePriceActivityPO.skuImage = sku.getSkuImage();
        changePriceActivityPO.class1Name = sku.getClass1Name();
        changePriceActivityPO.class2Name = sku.getClass2Name();
        changePriceActivityPO.class3Name = sku.getClass3Name();

        changePriceActivityPO.rulePrice = skuCpItems.getRulePrice();
        changePriceActivityPO.marketingDiffPrice = BigDecimal.ZERO;
        changePriceActivityPO.cpType = CpType.DIFF.getCode();
        if (skuPrice != null) {
            changePriceActivityPO.originalPrice = skuPrice.getOriginalPrice();
        } else {
            changePriceActivityPO.originalPrice = BigDecimal.ZERO;
        }

        changePriceActivityPO.gmtCreate = new Date();
        changePriceActivityPO.gmtModified = new Date();

        changePriceActivityPO.status = CpStatus.CREATE.getCode();
        changePriceActivityPO.auditPersonId = operator.getOperatorId().longValue();
        changePriceActivityPO.auditPersonName = operator.getOperatorName();
        changePriceActivityPO.remark = "";

        changePriceActivityPO.planActivityStartTime = cpTimeCycle.getBeginTime();
        changePriceActivityPO.planActivityEndTime = cpTimeCycle.getEndTime();

        changePriceActivityPO.auditPersonId = operator.getOperatorId().longValue();
        changePriceActivityPO.auditPersonName = operator.getOperatorName();
        changePriceActivityPO.auditStatus = AuditStatus.AUDIT_SUCCESS.getCode();
        changePriceActivityPO.auditRemark = "总部改价，默认不审核";

        changePriceActivityPO.lastOperatorId = operator.getOperatorId().longValue();
        changePriceActivityPO.lastOperatorName = operator.getOperatorName();
        changePriceActivityPO.currentPrice = BigDecimal.ZERO;
        changePriceActivityPO.planMarketingAmount = skuCpItems.getPlanMarketingFee();
        changePriceActivityPO.unitFormat = sku.getUnitFormat();

        //todo think
        changePriceActivityPO.subjectId = shop.getShopId();

        changePriceActivityPO.isDeleted = 0;
        changePriceActivityPO.bizCode = bizCode;

        return changePriceActivityPO;
    }

    public static GeneralCpItem generalCpItem(ChangePriceActivityPO s) {
        GeneralCpItem generalCpItem = GeneralCpItem.builder()
                .activityId(s.getActivityId())
                .activityCpId(s.getId())
                .activityName(s.getName())
                .cpBizType(CpBizType.from(s.getType()))
                .cpType(CpType.from(s.getCpType()))
                .currentPrice(s.getCurrentPrice())
                .rulePrice(s.getRulePrice())
                .originalPrice(s.getOriginalPrice())
                .marketingDiffFee(s.getMarketingDiffPrice())
                .shop(ChangePriceActivityPO.getShop(s))
                .sku(ChangePriceActivityPO.getSku(s))
                .cpTimeCycle(CpTimeCycle.create(s.getPlanActivityStartTime(), s.getPlanActivityEndTime()))
                .lastOperatorName(s.getLastOperatorName())
                .build();
        return generalCpItem;
    }

    public static AreaCpItem areaCpItem(ChangePriceActivityPO s) {
        AreaCpItem generalCpItem = AreaCpItem.builder()
                .activityId(s.getActivityId())
                .activityCpId(s.getId())
                .activityName(s.getName())
                .cpBizType(CpBizType.from(s.getType()))
                .cpType(CpType.from(s.getCpType()))
                .currentPrice(s.getCurrentPrice())
                .rulePrice(s.getRulePrice())
                .originalPrice(s.getOriginalPrice())
                .marketingDiffFee(s.getMarketingDiffPrice())
                .shop(ChangePriceActivityPO.getShop(s))
                .sku(ChangePriceActivityPO.getSku(s))
                .cpTimeCycle(CpTimeCycle.create(s.getPlanActivityStartTime(), s.getPlanActivityEndTime()))
                .lastOperatorName(s.getLastOperatorName())
                .build();
        return generalCpItem;
    }

    /**
     * 这是一个扫表的业务，
     *
     * @param mapper             mapper对象
     * @param lambdaQueryWrapper 请求条件
     * @param context            当前上下文
     * @param bi                 处理函数
     * @param <Context>          上下文对象
     */
    public static <Context> void visit(ChangePriceActivityMapper mapper,
                                       LambdaQueryWrapper<ChangePriceActivityPO> lambdaQueryWrapper,
                                       Context context,
                                       BiConsumer<ChangePriceActivityPO, Context> bi) {
        lambdaQueryWrapper.gt(ChangePriceActivityPO::getId, 0);
        lambdaQueryWrapper.orderByAsc(ChangePriceActivityPO::getId);

        //查询500条，根据id排序
        Page<ChangePriceActivityPO> page = new Page<>(1, 500);
        IPage<ChangePriceActivityPO> changePriceActivityPOIPage = mapper
                .selectPage(page, lambdaQueryWrapper);
        while (CollectionUtils.isNotEmpty(changePriceActivityPOIPage.getRecords())
                && changePriceActivityPOIPage.getRecords().size() == 500
        ) {
            //遍历处理业务（业务对象 + 当前上下文）
            for (ChangePriceActivityPO changePriceActivityPO : changePriceActivityPOIPage.getRecords()) {
                try {
                    bi.accept(changePriceActivityPO, context);
                } catch (Exception e) {
                    if (e instanceof BusinessException) {
                        log.warn("{}", ((BusinessException) e).getErrorMsg());
                    } else {
                        log.warn("业务处理失败：{}", JSON.toJSONString(changePriceActivityPO), e);
                    }
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            int size = changePriceActivityPOIPage.getRecords().size();
            lambdaQueryWrapper.gt(ChangePriceActivityPO::getId, changePriceActivityPOIPage.getRecords().get(size - 1).getId());
            changePriceActivityPOIPage = mapper.selectPage(page, lambdaQueryWrapper);
        }
        //没有满足500，最后处理一遍
        if (CollectionUtils.isNotEmpty(changePriceActivityPOIPage.getRecords())) {
            for (ChangePriceActivityPO changePriceActivityPO : changePriceActivityPOIPage.getRecords()) {
                try {
                    bi.accept(changePriceActivityPO, context);
                } catch (Exception e) {
                    log.warn("业务处理失败：{}", JSON.toJSONString(changePriceActivityPO),e);
                }
            }
        }
    }


    /**
     * 分页查询的转化
     *
     * @param changePriceActivityPO 改价状态
     * @return 大区改价活动
     */
    public static AreaCpActivity areaCpActivity(ChangePriceActivityPO changePriceActivityPO) {
        AreaCpActivity areaCpActivity = AreaCpActivity.builder()
                .activityId(changePriceActivityPO.getActivityId())
                .activityName(changePriceActivityPO.getName())
                .areaId(changePriceActivityPO.getShopAreaId())
                .cpBizType(CpBizType.from(changePriceActivityPO.getType()))
                .cpTimeCycle(new CpTimeCycle(changePriceActivityPO.getPlanActivityStartTime(), changePriceActivityPO.getPlanActivityEndTime()))
                .cpStatus(CpStatus.from(changePriceActivityPO.getStatus()))
                .shopCount(changePriceActivityPO.getShopCount())
                .skuCount(changePriceActivityPO.getSkuCount())
                .sumPlanMarketingFee(changePriceActivityPO.getSumPlanMarketingFee())
                .lastModifiedPerson(changePriceActivityPO.getLastOperatorName())
                .createTime(changePriceActivityPO.getGmtCreate())
                .build();
        return areaCpActivity;
    }

    /**
     * 创建大区改价明细
     *
     * @param cpCreateCommand 创建活动
     * @param activityPO      活动
     * @param shop            门店
     * @param sku             商品
     * @return 改价明细
     */
    public static ChangePriceActivityPO createCp(AreaCpCreateCommand cpCreateCommand, ActivityPO activityPO, Shop shop, Sku sku, SkuCpItem skuCpItem) {

        ChangePriceActivityPO changePriceActivityPO = new ChangePriceActivityPO();

        changePriceActivityPO.activityId = activityPO.getId();
        changePriceActivityPO.name = cpCreateCommand.getActivityName();


        changePriceActivityPO.createPersonId = cpCreateCommand.getOperator().getOperatorId().longValue();
        changePriceActivityPO.createPersonName = cpCreateCommand.getOperator().getOperatorName();
        changePriceActivityPO.type = CpBizType.CP_AREA.getCode();

        changePriceActivityPO.shopId = shop.getShopId();
        changePriceActivityPO.shopName = shop.getShopName();
        changePriceActivityPO.shopAreaId = shop.getAreaId();
        changePriceActivityPO.shopCityId = shop.getCityId();
        changePriceActivityPO.shopType = shop.getShopType();

        changePriceActivityPO.skuFormat = sku.getSkuFormat();
        changePriceActivityPO.skuId = sku.getSkuId();
        changePriceActivityPO.skuName = sku.getSkuName();
        changePriceActivityPO.skuImage = sku.getSkuImage();
        changePriceActivityPO.class1Name = sku.getClass1Name();
        changePriceActivityPO.class2Name = sku.getClass2Name();
        changePriceActivityPO.class3Name = sku.getClass3Name();

        changePriceActivityPO.rulePrice = skuCpItem.getRulePrice();
        changePriceActivityPO.marketingDiffPrice = BigDecimal.ZERO;
        changePriceActivityPO.cpType = CpType.DIFF.getCode();
        changePriceActivityPO.originalPrice = BigDecimal.ZERO;

        changePriceActivityPO.gmtCreate = new Date();
        changePriceActivityPO.gmtModified = new Date();

        changePriceActivityPO.status = CpStatus.CREATE.getCode();
        changePriceActivityPO.auditPersonId = cpCreateCommand.getOperator().getOperatorId().longValue();
        changePriceActivityPO.auditPersonName = cpCreateCommand.getOperator().getOperatorName();
        changePriceActivityPO.remark = "";

        changePriceActivityPO.planActivityStartTime = cpCreateCommand.getTimeCycle().getBeginTime();
        changePriceActivityPO.planActivityEndTime = cpCreateCommand.getTimeCycle().getEndTime();

        changePriceActivityPO.auditPersonId = cpCreateCommand.getOperator().getOperatorId().longValue();
        changePriceActivityPO.auditPersonName = cpCreateCommand.getOperator().getOperatorName();
        changePriceActivityPO.auditStatus = AuditStatus.AUDIT_SUCCESS.getCode();
        changePriceActivityPO.auditRemark = "大区改价，默认不审核";

        changePriceActivityPO.lastOperatorId = cpCreateCommand.getOperator().getOperatorId().longValue();
        changePriceActivityPO.lastOperatorName = cpCreateCommand.getOperator().getOperatorName();
        changePriceActivityPO.currentPrice = BigDecimal.ZERO;
        changePriceActivityPO.planMarketingAmount = skuCpItem.getPlanMarketingFee();
        changePriceActivityPO.unitFormat = sku.getUnitFormat();

        //todo think
        if (cpCreateCommand.getMarketingAccountId() != null) {
            changePriceActivityPO.subjectId = cpCreateCommand.getMarketingAccountId().intValue();
        } else {
            changePriceActivityPO.subjectId = shop.getShopId();
        }

        changePriceActivityPO.isDeleted = 0;

        return changePriceActivityPO;
    }

    public static ChangePriceActivityPO createCp(CpCreateCommand cpCreateCommand, ActivityPO activityPO, Shop shop, Sku sku, SkuCpItem skuCpItem) {

        ChangePriceActivityPO changePriceActivityPO = new ChangePriceActivityPO();
        changePriceActivityPO.type = cpCreateCommand.getCpBizType().getCode();
        changePriceActivityPO.auditStatus = AuditStatus.AUDIT_SUCCESS.getCode();
        if (CpBizType.CP_AREA.equals(cpCreateCommand.getCpBizType())) {
            changePriceActivityPO.auditRemark = "大区改价，默认不审核";
        }
        if (CpBizType.CP_PLATFORM.equals(cpCreateCommand.getCpBizType())) {
            changePriceActivityPO.auditRemark = "总部改价，默认不审核";
        }
        if (CpBizType.CP_VIP.equals(cpCreateCommand.getCpBizType())) {
            changePriceActivityPO.auditRemark = "VIP改价，默认不审核";
        }

        changePriceActivityPO.activityId = activityPO.getId();
        changePriceActivityPO.name = cpCreateCommand.getActivityName();


        changePriceActivityPO.createPersonId = cpCreateCommand.getOperator().getOperatorId().longValue();
        changePriceActivityPO.createPersonName = cpCreateCommand.getOperator().getOperatorName();


        changePriceActivityPO.shopId = shop.getShopId();
        changePriceActivityPO.shopName = shop.getShopName();
        changePriceActivityPO.shopAreaId = shop.getAreaId();
        changePriceActivityPO.shopCityId = shop.getCityId();
        changePriceActivityPO.shopType = shop.getShopType();

        changePriceActivityPO.skuFormat = sku.getSkuFormat();
        changePriceActivityPO.skuId = sku.getSkuId();
        changePriceActivityPO.skuName = sku.getSkuName();
        changePriceActivityPO.skuImage = sku.getSkuImage();
        changePriceActivityPO.class1Name = sku.getClass1Name();
        changePriceActivityPO.class2Name = sku.getClass2Name();
        changePriceActivityPO.class3Name = sku.getClass3Name();

        changePriceActivityPO.rulePrice = skuCpItem.getRulePrice();
        changePriceActivityPO.marketingDiffPrice = BigDecimal.ZERO;
        changePriceActivityPO.cpType = CpType.DIFF.getCode();
        changePriceActivityPO.originalPrice = BigDecimal.ZERO;

        changePriceActivityPO.gmtCreate = new Date();
        changePriceActivityPO.gmtModified = new Date();

        changePriceActivityPO.status = CpStatus.CREATE.getCode();
        changePriceActivityPO.auditPersonId = cpCreateCommand.getOperator().getOperatorId().longValue();
        changePriceActivityPO.auditPersonName = cpCreateCommand.getOperator().getOperatorName();
        changePriceActivityPO.remark = "";

        changePriceActivityPO.planActivityStartTime = cpCreateCommand.getTimeCycle().getBeginTime();
        changePriceActivityPO.planActivityEndTime = cpCreateCommand.getTimeCycle().getEndTime();

        changePriceActivityPO.auditPersonId = cpCreateCommand.getOperator().getOperatorId().longValue();
        changePriceActivityPO.auditPersonName = cpCreateCommand.getOperator().getOperatorName();


        changePriceActivityPO.lastOperatorId = cpCreateCommand.getOperator().getOperatorId().longValue();
        changePriceActivityPO.lastOperatorName = cpCreateCommand.getOperator().getOperatorName();
        changePriceActivityPO.currentPrice = BigDecimal.ZERO;
        changePriceActivityPO.planMarketingAmount = skuCpItem.getPlanMarketingFee();
        changePriceActivityPO.unitFormat = sku.getUnitFormat();

        //todo think
        if (cpCreateCommand.getMarketingAccountId() != null) {
            changePriceActivityPO.subjectId = cpCreateCommand.getMarketingAccountId().intValue();
        } else {
            changePriceActivityPO.subjectId = shop.getShopId();
        }

        changePriceActivityPO.isDeleted = 0;

        return changePriceActivityPO;
    }

    public static ShopCpResult convert(ChangePriceActivityPO s) {
        ShopCpResult shopCpResult = new ShopCpResult();
        shopCpResult.setActivityId(s.getActivityId());
        shopCpResult.setShopId(s.getShopId());
        return shopCpResult;
    }
}
