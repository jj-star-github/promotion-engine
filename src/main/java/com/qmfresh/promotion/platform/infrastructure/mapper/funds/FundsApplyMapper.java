package com.qmfresh.promotion.platform.infrastructure.mapper.funds;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.bean.promotion.*;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsApply;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsApplyDayStatis;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.FundsApplyCountDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.FundsApplyCountQueryDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.PageQueryFundsApplyCheckParamDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * FundsApplyMapper继承基类
 */
@Repository
public interface FundsApplyMapper extends BaseMapper<FundsApply> {

    int insertFundsApplyBill(FundsApply fundsApply);

    List<FundsApply> pageQueryFundsApply(@Param("condition") PageQueryFundsApplyParamDTO condition, @Param("start") int start, @Param("end") int end);

    int countByCondition(PageQueryFundsApplyParamDTO condition);

    int  countByCheckCondition(@Param("condition") PageQueryFundsApplyCheckParamDTO condition);

    List<FundsApply> pageQueryFundsApplyCheck(@Param("condition") PageQueryFundsApplyCheckParamDTO condition, @Param("start") int start, @Param("end") int end);

    int  countFundsAccountList(@Param("condition") PageQueryFundsApplyCheckParamDTO condition);

    List<FundsApply> fundsAccountList(@Param("condition") PageQueryFundsApplyCheckParamDTO condition, @Param("start") int start, @Param("end") int end);

    FundsApply selectByfundsId(Long Id);

    int upFundsApplyStatus(FundsApply param);

    FundsApplyCountDTO queryCheckApplyCount(FundsApplyCountQueryDTO param);

    List<FundsApply>  hasFundsApplayStatus(@Param("condition") PageQueryFundsApplyParamDTO condition);

    /**
     * 查询门店日统计
     * @param shopId
     * @param shopType
     * @param beginTime
     * @param endTime
     * @return
     */
    List<FundsApplyDayStatis> queryDayStatis(@Param("shopId")Integer shopId, @Param("shopType")Integer shopType, @Param("beginTime") Integer beginTime, @Param("endTime") int endTime);
}