package com.qmfresh.promotion.platform.infrastructure.http;


import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.apache.commons.collections.CollectionUtils;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 基础的http能力
 * <p>
 * 异常返回都为空
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Slf4j
public class Https {

    private static final OkHttpClient NORMAL_CLIENT = new OkHttpClient.Builder()
            .addNetworkInterceptor(new LoggingInterceptor())
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .build();

    /**
     * 请求地址
     *
     * @param url    url地址
     * @param header 请求头
     * @param json   请求参数
     * @return 请求结果
     */
    public static String post(String url, Map<String, String> header, String json) {
        return post(url, header, null, json);
    }

    public static String post(String url, Map<String, String> header, Map<String, String> params, String json) {
        RequestBody requestBody = RequestBody
                .create(MediaType.get("application/json;charset=utf-8"), json);
        String httpUrl = url + uri(params);
        Request.Builder requestBuilder = new Request.Builder().url(httpUrl);
        if (header != null && CollectionUtils.isNotEmpty(header.keySet())) {
            for (String key : header.keySet()) {
                requestBuilder.header(key, header.get(key));
            }
        }
        requestBuilder.post(requestBody);
        Call call = NORMAL_CLIENT.newCall(requestBuilder.build());
        try {
            Response response = call.execute();
            assert response.body() != null;
            return response.body().string();
        } catch (IOException e) {
            log.error("请求出错：{} {} {}", url, JSON.toJSONString(header), json, e);
            return null;
        }
    }

    public static String post(String url, Map<String, String> header, Map<String, String> params) {
        FormBody.Builder formBodyBuilder = new FormBody.Builder();
        Request.Builder requestBuilder = appendHeader(header);
        if (params != null && CollectionUtils.isNotEmpty(params.keySet())) {
            for (String key : params.keySet()) {
                formBodyBuilder.add(key, params.get(key));
            }
        }
        requestBuilder.url(url);
        requestBuilder.post(formBodyBuilder.build());
        return doExecute(url, header, params, requestBuilder);
    }

    /**
     * {"a":"b","c":"d"} -> ?a=b&c=d
     *
     * @param params 请求参数
     * @return uri字符串
     */
    static String uri(Map<String, String> params) {
        if (params == null || CollectionUtils.isEmpty(params.keySet())) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("?");
        for (String key : params.keySet()) {
            sb.append(key).append("=").append(params.get(key)).append("&");
        }
        //删除最后一个字符串
        int index = sb.lastIndexOf("&");
        if (index == -1) {
            return sb.toString();
        }
        return sb.substring(0, index);

    }

    /**
     * http get请求
     *
     * @param url    url地址
     * @param header 请求头
     * @param param  请求参数
     * @return string
     */
    public static String get(String url, Map<String, String> header, Map<String, String> param) {
        String httpUrl = url + uri(param);
        Request.Builder requestBuild = appendHeader(header);
        requestBuild.url(httpUrl).get();
        return doExecute(httpUrl, header, param, requestBuild);
    }

    /**
     * 最简单版本
     *
     * @param url 请求地址
     * @return 字符串
     */
    public static String get(String url) {
        return get(url, null, null);
    }

    private static String doExecute(String url, Map<String, String> header, Map<String, String> param, Request.Builder requestBuild) {
        Call call = NORMAL_CLIENT.newCall(requestBuild.build());
        try {
            Response response = call.execute();
            assert response.body() != null;
            return response.body().string();
        } catch (IOException e) {
            log.error("请求出错：{} {} {}", url, JSON.toJSONString(header), JSON.toJSONString(param), e);
            return null;
        }
    }

    private static Request.Builder appendHeader(Map<String, String> header) {
        Request.Builder requestBuild = new Request.Builder();
        if (header != null && CollectionUtils.isNotEmpty(header.keySet())) {
            for (String key : header.keySet()) {
                requestBuild.header(key, header.get(key));
            }
        }
        return requestBuild;
    }
}
