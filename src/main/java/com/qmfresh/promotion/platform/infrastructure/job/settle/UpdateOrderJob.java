package com.qmfresh.promotion.platform.infrastructure.job.settle;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.qmfresh.promotion.platform.domain.model.settle.apportion.ApportionManager;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.UpdateOrderList;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author zbr
 * @Date 10:00 2020/9/10
 */
@Component
@Slf4j
@JobHandler("UpdateOrderJob")
public class UpdateOrderJob extends IJobHandler {

    @Resource
    private ApportionManager apportionManager;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        log.info("开始执行UpdateOrderJob...");

        List<String> list = new ArrayList<>();
        String str[] = param.split(",");
        list = Arrays.asList(str);
        apportionManager.updateOrder(list);
        log.info("结束执行UpdateOrderJob...");
        return ReturnT.SUCCESS;
    }
}
