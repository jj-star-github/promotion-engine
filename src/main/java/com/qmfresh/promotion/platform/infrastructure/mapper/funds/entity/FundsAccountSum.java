package com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class FundsAccountSum implements Serializable {

    private static final long serialVersionUID = 1L;

    private BigDecimal sumAmount;
    private BigDecimal sumLockAmount;

}
