package com.qmfresh.promotion.platform.infrastructure.mapper.funds;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsLockChange;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 费用锁定流水
 */
public interface FundsLockChangeMapper extends BaseMapper<FundsLockChange> {

    /**
     * 查询门店类型维度日统计数据
     * @param createDay
     * @return
     */
    List<FundsLockChange> queryDaySumSupportShopType(@Param("createDay") String createDay);

    /**
     * 指定商户日锁定数据
     * @param accountId
     * @param createDays
     * @return
     */
    List<FundsLockChange> queryDaySumSupportAccount(@Param("accountId") Long accountId, @Param("createDays") List<String> createDays);

    /**
     *
     * @param accountIdList
     * @param beginTime
     * @param endTime
     * @return
     */
    List<FundsLockChange> queryDaySumSupportAccounts(@Param("accountIdList") List<Long> accountIdList, @Param("beginTime") String beginTime, @Param("endTime") String endTime);

}