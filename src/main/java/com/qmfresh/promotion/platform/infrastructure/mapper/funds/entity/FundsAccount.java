package com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("t_funds_account")
public class FundsAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private Integer shopId;
    private String shopName;
    private Integer shopType;
    private BigDecimal totalAmount;
    private BigDecimal lockAmount;
    private Integer status;
    private Date gmtCreate;
    private Date gmtModified;
    private Integer isDeleted;

    private String shopAccount;
    private BigDecimal minPromPrice;
    private BigDecimal maxPromPrice;
    private String remark;
    private Integer operatorId;
    private String operatorName;
    private Integer areaId;

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }
}
