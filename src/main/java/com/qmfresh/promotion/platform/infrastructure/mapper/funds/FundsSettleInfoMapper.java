package com.qmfresh.promotion.platform.infrastructure.mapper.funds;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsSettleInfo;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsShopDayAmountSum;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;


/**
 * 营销账单信息
 */
public interface FundsSettleInfoMapper extends BaseMapper<FundsSettleInfo> {


    /**
     * 根据账户查询日统计
     * @param accountId
     * @param createDay
     * @return
     */
    FundsShopDayAmountSum queryDayAmount(@Param("accountId")Long accountId,@Param("shopId")Integer shopId, @Param("shopType")Integer shopType, @Param("createDay")String createDay);

    /**
     * 根据截至时间统计
     * @param shopId  账户Id
     * @param endTime    截至时间
     * @return
     */
    List<FundsShopDayAmountSum> queryDayStatis(@Param("shopId") Integer shopId, @Param("shopType")Integer shopType , @Param("endTime")Date endTime);

    /**
     * 根据账户查询明细
     * @param accountId
     * @param createDay
     * @param start
     * @param length
     * @return
     */
    List<FundsSettleInfo> queryDayNote(@Param("accountId") Long accountId, @Param("createDay")String createDay, @Param("start")Integer start, @Param("length")Integer length);

    /**
     * 根据账户查询 总数
     * @param accountId
     * @param createDay
     * @return
     */
    Integer queryDayCount(@Param("accountId") Long accountId, @Param("createDay")String createDay);
    /**
     *
     * 查询日统计信息
     * @param createDay
     * @return
     */
    FundsSettleInfo queryDaySum(@Param("createDay") String createDay);

    /**
     * 查询日指标信息
     * @param beginTime
     * @param endTime
     * @return
     */
    List<FundsSettleInfo> queryDayTarget(@Param("beginTime")String beginTime, @Param("endTime")String endTime, @Param("shopType")Integer shopType, @Param("shopId")Integer shopId,
                                         @Param("accountId")Long accountId, @Param("start")Integer start, @Param("length")Integer length);

    /**
     * 查询日指标信息
     * @param beginTime
     * @param endTime
     * @param shopType
     * @param shopId
     * @param accountId
     * @return
     */
    Integer queryDayTargetCount(@Param("beginTime")String beginTime, @Param("endTime")String endTime, @Param("shopType")Integer shopType, @Param("shopId")Integer shopId,
                            @Param("accountId")Long accountId);

    /**
     * 统计账户明细
     * @param createDay
     * @param start
     * @param length
     * @return
     */
    List<FundsSettleInfo> queryAccountStatis(@Param("createDay")String createDay, @Param("start")Integer start, @Param("length")Integer length);

    /**
     * 查询账户明细总数
     * @param createDay
     * @return
     */
    Integer queryAccountStatisCount(@Param("createDay")String createDay);

    /**
     * 根据门店列表统计
     * @param createDay
     * @param start
     * @param length
     * @return
     */
    List<FundsSettleInfo> queryAccountByShopList(@Param("createDay")String createDay, @Param("start")Integer start, @Param("length")Integer length, @Param("shopIdList")List<Integer> shopIdList, @Param("shopType")Integer shopType);

    /**
     * 根据门店列表 总数
     * @param createDay
     * @return
     */
    Integer queryCountByShopList(@Param("createDay")String createDay,  @Param("shopIdList")List<Integer> shopIdList, @Param("shopType")Integer shopType);

    /**
     * 批量查询日指标信息
     * @param beginTime
     * @param endTime
     * @return
     */
    List<FundsSettleInfo> batchQueryDayTarget(@Param("beginTime")String beginTime, @Param("endTime")String endTime, @Param("shopType")Integer shopType, @Param("shopId")Integer shopId,
                                         @Param("accountId")Long accountId, @Param("start")Integer start, @Param("length")Integer length);

    /**
     * 批量查询日指标信息
     * @param beginTime
     * @param endTime
     * @param shopType
     * @param shopId
     * @param accountId
     * @return
     */
    Integer batchQueryDayTargetCount(@Param("beginTime")String beginTime, @Param("endTime")String endTime, @Param("shopType")Integer shopType, @Param("shopId")Integer shopId,
                                @Param("accountId")Long accountId);

}