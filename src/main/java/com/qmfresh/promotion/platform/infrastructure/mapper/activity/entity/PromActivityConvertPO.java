package com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 活动发放记录
 */
@TableName("t_prom_activity_convert")
@Data
public class PromActivityConvertPO implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 活动名
     */
    private Long activityId;
    /**
     */
    private String businessCode;
    /**
     */
    private Integer businessType;
    /**
     *
     */
    private Integer userId;
    /**
     */
    private Integer shopId;
    /**
     */
    private Long conditionId;
    private Long awardId;
    /**
     */
    private Integer awardType;
    /**
     */
    private Integer num;
    /**
     * 奖品发放状态:1.未发放,2.发放成功,3.发放失败,4.补发成功
     */
    private Integer status;
    /**
     * 活动描述
     */
    private String remark;

    private Date gmtCreate;
    private Date gmtModified;
    private Integer isDeleted;

}
