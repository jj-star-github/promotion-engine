package com.qmfresh.promotion.platform.infrastructure.message.consumer.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CloseShopCpActivityDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 门店类型
     */
    private Integer shopType;
    /**
     * 门店id
     */
    private Integer shopId;

}
