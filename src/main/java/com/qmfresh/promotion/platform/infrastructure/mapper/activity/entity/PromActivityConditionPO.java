package com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qmfresh.promotion.platform.domain.model.promactivity.enums.ConditionTypeEnum;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityConditionDTO;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 活动奖励
 */
@TableName("t_prom_activity_condition")
@Data
public class PromActivityConditionPO implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 活动
     */
    private Long activityId;
    /**
     *
     */
    private Long awardId;
    /**
     * 奖励名
     */
    private String awardName;
    /**
     * 奖励类型
     */
    private Integer awardType;
    /**
     * 满足条件
     */
    private BigDecimal conditionNum;
    /**
     * 等级
     */
    private Integer conditionLevel;
    /**
     * 最大发将数 每满使用
     */
    private Integer maxNum;
    /**
     * 库存
     */
    private Integer stock;

    /**
     * 奖品描述
     */
    private String remark;

    private Date gmtCreate;
    private Date gmtModified;
    private Integer isDeleted;

    public PromActivityConditionPO(){}

    public PromActivityConditionPO(PromActivityConditionDTO dto) {
        this.setAwardId(dto.getConditionId());
        this.setAwardName(dto.getConditionName());
        this.setAwardType(dto.getConditionType());
        this.setConditionNum(dto.getCondition());
        this.setConditionLevel(dto.getLevel());
        this.setStock(dto.getStock());
        this.setMaxNum(dto.getMaxNum());
        if(ConditionTypeEnum.COUPON.getCode() == dto.getConditionType() || null != dto.getConditionCouponDTO()) {
            this.setRemark(JSON.toJSONString(dto.getConditionCouponDTO()));
        }
        Date now = new Date();
        this.setGmtCreate(now);
        this.setGmtModified(now);
    }

}
