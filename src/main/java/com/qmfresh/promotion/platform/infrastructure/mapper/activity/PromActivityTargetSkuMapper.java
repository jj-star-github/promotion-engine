package com.qmfresh.promotion.platform.infrastructure.mapper.activity;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityTargetSkuPO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 活动目标商品信息
 */
public interface PromActivityTargetSkuMapper extends BaseMapper<PromActivityTargetSkuPO> {

    /**
     * 根据活动ID查询商品信息
     */
    List<PromActivityTargetSkuPO> queryByActivity(@Param("activityId") Long activityId);

    /**
     * 批量查询
     * @param activityIdList
     * @return
     */
    List<PromActivityTargetSkuPO> queryByActivityList(@Param("activityIdList") List<Long> activityIdList);

    /**
     * 查询活动目标SKUID
     * @param activityId
     * @return
     */
    List<Integer> querySkuId(@Param("activityId") Long activityId);

    /**
     * 批量删除目标商品信息
     * @param activityId
     * @param skuIdList
     */
    void deletedBatch(@Param("activityId") Long activityId, @Param("skuIdList")List<Integer> skuIdList, @Param("gmtModified")Date gmtModified);

    /**
     * 根据 class2查询活动交集
     * @param class2IdList
     * @param activityList
     * @return
     */
    List<Long> queryActivityIdByClass2(@Param("class2IdList") List<Integer> class2IdList, @Param("activityList")List<Long> activityList);

    /**
     * 根据SKUID获取活动交集
     * @param skuIdList
     * @param activityList
     * @return
     */
    List<Long> queryActivityIdBySkuId(@Param("skuIdList") List<Integer> skuIdList, @Param("activityList")List<Long> activityList, @Param("skuType") Integer skuType);
}