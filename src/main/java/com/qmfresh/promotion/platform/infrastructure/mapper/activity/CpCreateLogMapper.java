package com.qmfresh.promotion.platform.infrastructure.mapper.activity;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.CpCreateLogPO;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface CpCreateLogMapper extends BaseMapper<CpCreateLogPO> {
}
