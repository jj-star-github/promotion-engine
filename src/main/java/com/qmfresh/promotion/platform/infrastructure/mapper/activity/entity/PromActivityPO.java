package com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qmfresh.promotion.platform.domain.model.promactivity.enums.StatusEnum;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityCreateDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 活动基础信息
 */
@TableName("t_prom_activity")
@Data
public class PromActivityPO implements Serializable {

    /**
     * 活动ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 活动名
     */
    private String name;
    /**
     *  活动类型,1:满赠
     */
    private Integer activityType;
    /**
     * 活动规则,类型满赠:1满减 2阶梯
     */
    private Integer ruleType;
    /**
     * 开始时间
     */
    private Date beginTime;
    /**
     * 结束时间
     */
    private Date endTime;
    /**
     * 状态:1.待进行,3.进行中,4.已取消,6=结束中,7=已关闭,8=已结束
     */
    private Integer status;
    /**
     * 目标人群:1.全部,2.会员,3.非会员
     */
    private Integer userType;
    /**
     * 渠道:1.全渠道,2.线上,3.线下
     */
    private Integer channel;
    /**
     * 活动归属:1.总部活动,2.大区活动,3.片区活动,4.门店门店
     */
    private Integer activityBelong;
    /**
     * 活动描述
     */
    private String remark;
    /**
     * 营销经费账号ID
     */
    private Long accountId;
    /**
     * 目标门店,1:全部,2.大区,3.片区,4.门店
     */
    private Integer shopType;
    /**
     * 目标品,1:全品,2:Class1,3:class2,4:sku
     */
    private Integer skuType;
    private Integer lastOperatorId;
    private String lastOperatorName;

    private Date gmtCreate;
    private Date gmtModified;
    private Integer isDeleted;

    private List<Integer> targetSkuId;
    private List<PromActivityConditionPO>  conditionPOList;

    public PromActivityPO() {}

    public PromActivityPO(PromActivityCreateDTO dto) {
        this.setName(dto.getName());
        this.setActivityType(dto.getActivityType());
        this.setRuleType(dto.getRuleType());
        this.setBeginTime(new Date(dto.getBeginTime()));
        this.setEndTime(new Date(dto.getEndTime()));
        this.setStatus(getActivityStatus(this.getBeginTime(), this.getEndTime()));
        this.setUserType(dto.getUserType());
        this.setChannel(dto.getActivityChannel());
        this.setActivityBelong(1);
        this.setRemark(dto.getRemark());
        this.setShopType(dto.getShopType());
        this.setSkuType(dto.getSkuType());
        this.setLastOperatorId(dto.getOperatorId());
        this.setLastOperatorName(dto.getOperatorName());
        this.setAccountId(0l);
        Date now = new Date();
        this.setGmtModified(now);
        this.setGmtCreate(now);
    }

    private Integer getActivityStatus(Date beginTime, Date endTime) {
        Date now = new Date();
        if (now.compareTo(beginTime) < 0) {
            return StatusEnum.CREATE.getCode();
        }
        if (now.compareTo(beginTime) > 0 && now.compareTo(endTime) < 0) {
            return StatusEnum.RUNNING.getCode();
        }
        return StatusEnum.FINISHED.getCode();
    }

    public int checkStatus() {
        if (this.status > 3) {
            return this.status;
        }
        Date now = new Date();
        if (now.compareTo(this.beginTime) < 0) {
            return StatusEnum.CREATE.getCode();
        }
        if (now.compareTo(beginTime) > 0 && now.compareTo(this.endTime) < 0) {
            return StatusEnum.RUNNING.getCode();
        }
        return StatusEnum.FINISHED.getCode();
    }

    public void setTargetSkuId(List<PromActivityTargetSkuPO> targetSkuPOList) {
        List<Integer> targetSkuId = targetSkuPOList.stream().map(PromActivityTargetSkuPO::getSkuId).collect(Collectors.toList());
        this.targetSkuId = targetSkuId;
    }
}
