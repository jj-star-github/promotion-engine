package com.qmfresh.promotion.platform.infrastructure.message.consumer.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class WeekClearDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 标识
     */
    private String UniqueSign;
    /**
     * 门店id
     */
    private Long shopId;
    /**
     * 周清差值  出货量-目标量
     */
    private BigDecimal reward;
    /**
     * 周清值 1正 0负
     */
    private Integer changeType;
    /**
     * 周清结束日期
     */
    private Long voidanceEndTime;
    /**
     * 周清开始日期
     */
    private Long voidanceStartTime;

}
