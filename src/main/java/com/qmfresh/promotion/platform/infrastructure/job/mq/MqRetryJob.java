package com.qmfresh.promotion.platform.infrastructure.job.mq;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
@JobHandler("MqRetryJob")
public class MqRetryJob extends IJobHandler {
    @Override
    public ReturnT<String> execute(String param) throws Exception {
        return null;
    }
}
