package com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 门店费用申请日统计
 */
@Data
public class FundsApplyDayStatis implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 总使用营销费用
     */
    private BigDecimal sumApplayAmount;
    /**
     * 创建时间
     */
    private String createDay;

}
