package com.qmfresh.promotion.platform.infrastructure.message.consumer.handle;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.settle.apportion.ApportionManager;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.dto.CashOrderDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author zbr
 * @Date 15:32 2020/8/22
 */
@Service
@Slf4j
public class CashOrderNormalHandle {
    @Resource
    private ApportionManager apportionManager;

    public void handleMessage(CashOrderDTO orderDTO) {
        log.info("接收订单转换，参数:" + JSON.toJSONString(orderDTO));
        apportionManager.apportion(orderDTO);
    }
}
