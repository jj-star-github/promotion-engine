package com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 营销费用锁定流水信息
 */
@TableName("t_funds_lock_change")
@Data
@ToString
public class FundsLockChange implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private Long businessId;
    private Long accountId;
    private Integer shopId;
    private Integer shopType;
    private BigDecimal changeAmount;
    private Integer changeType;
    private String createDay;
    private Date gmtCreate;
    private Date gmtModified;
    private Integer isDeleted;


}
