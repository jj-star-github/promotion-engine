package com.qmfresh.promotion.platform.infrastructure.message.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.dto.CashOrderDTO;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.handle.CashOrderNormalHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author zbr
 * @Date 15:13 2020/8/22
 */
@Service
@Slf4j
@RocketMQMessageListener(consumerGroup = "${spring.application.name}CashOrder", topic = "${mq.topic.cashOrderNormal}")
public class CashOrderNormalConsumer extends AbstractRocketMQListener {
    @Resource
    private CashOrderNormalHandle cashOrderNormalHandle;

    @Override
    void handleMessage(String topic, String tags, String content, String msgId) {
        CashOrderDTO orderDTO;
        try {
            orderDTO = JSON.parseObject(content, new TypeReference<CashOrderDTO>() {
            });
        } catch (Exception e) {
            log.error("消息体JSON解析异常:topic={} msgId = {} content= {}", topic, msgId, content);
            return;
        }
        cashOrderNormalHandle.handleMessage(orderDTO);
    }
}
