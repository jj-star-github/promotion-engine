package com.qmfresh.promotion.platform.infrastructure.job.funds;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.activity.cp.event.CpEvent;
import com.qmfresh.promotion.platform.domain.model.funds.FundsAccountManager;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsAccount;
import com.qmfresh.promotion.platform.infrastructure.message.RocketMQSender;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.dto.CloseShopCpActivityDTO;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 */
@JobHandler("FundsNotEnoughJob")
@Slf4j
@Component
public class FundsNotEnoughJob extends IJobHandler {

    @Resource
    private FundsAccountManager fundsAccountManager;
    @Resource
    private DefaultMQProducer defaultMQProducer;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        String traceId = UUID.randomUUID().toString().replaceAll("-", "");
        log.info("开始费用不足定时任务...traceId={}", traceId);
        //获取欠费商户信息
        List<FundsAccount> accountList = fundsAccountManager.queryNotEnoughAccount();
        if (CollectionUtils.isNotEmpty(accountList)) {
            List<CloseShopCpActivityDTO> closeShopCpActivityDTOS = new ArrayList<>();
            accountList.forEach(account -> {
                CloseShopCpActivityDTO dto = new CloseShopCpActivityDTO();
                dto.setShopId(account.getShopId());
                dto.setShopType(account.getShopType());
                closeShopCpActivityDTOS.add(dto);
            });
            log.info("费用不足定时任务-通知MQ traceId{} param={}", traceId, JSON.toJSONString(closeShopCpActivityDTOS));
            RocketMQSender.doSend(defaultMQProducer, "close_shop_cpActivity", closeShopCpActivityDTOS);
        }
        log.info("结束费用不足定时任务... traceId={}", traceId);
        return ReturnT.SUCCESS;
    }
}
