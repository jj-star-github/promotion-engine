package com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UpdateOrderList implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<String> list;
}
