package com.qmfresh.promotion.platform.infrastructure.message.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.dto.CashOrderDTO;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.dto.WeekClearDTO;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.handle.CashOrderNormalHandle;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.handle.WeekClearHandle;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.annotation.SelectorType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
@RocketMQMessageListener(consumerGroup = "${spring.application.name}WeekClear", topic = "${mq.topic.opsServiceNotify}",selectorExpression = "VoidanceShopRewardMessage",selectorType = SelectorType.TAG)
public class WeekClearConsumer extends AbstractRocketMQListener {

    @Resource
    private WeekClearHandle weekClearHandle;

    @Override
    void handleMessage(String topic, String tags, String content, String msgId) {
        WeekClearDTO weekClearDTO;
        try {
            weekClearDTO = JSON.parseObject(content, new TypeReference<WeekClearDTO>() {
            });
        } catch (Exception e) {
            log.error("消息体JSON解析异常:topic={} msgId = {} content= {}", topic, msgId, content);
            return;
        }
        weekClearHandle.handleMessage(weekClearDTO);
    }
}
