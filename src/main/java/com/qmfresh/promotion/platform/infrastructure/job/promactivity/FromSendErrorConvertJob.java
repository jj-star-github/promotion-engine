package com.qmfresh.promotion.platform.infrastructure.job.promactivity;

import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityConvertManager;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * 处理发奖失败的订单
 */
@JobHandler("FromSendErrorConvertJob")
@Slf4j
@Component
public class FromSendErrorConvertJob extends IJobHandler {

    @Resource
    private PromActivityConvertManager promActivityConvertManager;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        String traceId = UUID.randomUUID().toString().replaceAll("-", "");
        log.info("处理发奖失败的订单-开始...traceId={} param={}", traceId, param);
        promActivityConvertManager.sendErrorConvert(param, traceId);
        log.info("处理发奖失败的订单-结束... traceId={} param={}", traceId, param);
        return ReturnT.SUCCESS;
    }
}
