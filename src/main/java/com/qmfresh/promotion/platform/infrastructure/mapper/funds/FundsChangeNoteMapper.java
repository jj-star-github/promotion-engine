package com.qmfresh.promotion.platform.infrastructure.mapper.funds;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsChangeNote;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsChangeNoteGMVSum;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsShopDayChangeStatis;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsWeekClear;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 营销费用交易流水
 */
public interface FundsChangeNoteMapper extends BaseMapper<FundsChangeNote> {

    /**
     * 查询门店费用支出商品信息
     * @param accountId
     * @param createDay
     * @return
     */
    List<FundsChangeNote> querySkuByAccount(@Param("accountId")Long accountId, @Param("createDay")String createDay);

    /**
     * 根据业务类型查询支出流水
     * @param accountId
     * @param createDay
     * @param businessTypeList
     * @return
     */
    List<FundsChangeNote> queryPayNote(@Param("accountId")Long accountId, @Param("createDay")String createDay, @Param("businessTypeList")List<Integer> businessTypeList, @Param("businessIdList")List<String> businessIdList, @Param("start")Integer start, @Param("length")Integer length);

    /**
     * 根据业务类型查询支出流水 数量
     * @param accountId
     * @param createDay
     * @param businessType
     * @return
     */
    Integer queryPayNoteCount(@Param("accountId")Long accountId, @Param("createDay")String createDay, @Param("businessType")Integer businessType, @Param("businessId")String businessId);

    /**
     * 总部支出流水 活动维度
     * @param accountId
     * @param createDay
     * @param start
     * @param length
     * @return
     */
    List<FundsChangeNote> queryZbPayStatis(@Param("accountId")Long accountId, @Param("createDay")String createDay, @Param("start")Integer start, @Param("length")Integer length);

    /**
     *  查询总部支出总数
     * @param accountId
     * @param createDay
     * @return
     */
    Integer queryZbPayStatisCount(@Param("accountId")Long accountId, @Param("createDay")String createDay);

    /**
     * 日流水统计
     * @param accountId
     * @param beginTime
     * @param endTime
     * @param start
     * @param length
     * @return
     */
    List<FundsShopDayChangeStatis> shopDayNoteStatis(@Param("accountId")Long accountId, @Param("beginTime")String beginTime, @Param("endTime") String endTime, @Param("shopType")Integer shopType, @Param("start")Integer start, @Param("length")Integer length);

    /**
     * 日流水总数
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return
     */
    Integer shopDayNoteStatisCount(@Param("accountId")Long accountId, @Param("beginTime")String beginTime, @Param("endTime") String endTime, @Param("shopType")Integer shopType);

    /**
     * 周GMV统计
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return
     */
    FundsChangeNoteGMVSum sumGMV(@Param("accountId")Long accountId, @Param("beginTime")String beginTime, @Param("endTime") String endTime);

    /**
     * 查询账户收入的目标账户信息
     * @param accountId
     * @param createDay
     * @return
     */
    List<Integer> queryTargetShopByAccountId(@Param("accountId")Long accountId, @Param("createDay")String createDay);

    /**
     * 查询门店去化奖励
     * @param accountId
     * @param endTime
     * @return
     */
    List<FundsWeekClear> queryWeekClear(@Param("accountId")Long accountId, @Param("endTime")Date endTime);

    /**
     * 查询门店日去化奖励
     * @param accountId
     * @param createDay
     * @return
     */
    FundsWeekClear queryDayWeekClear(@Param("accountId")Long accountId, @Param("createDay")String createDay);

    /**
     * 根据去化奖励标识查询
     * @param businessCode
     * @return
     */
    FundsChangeNote queryByWeekClear(@Param("businessCode")String businessCode);

    /**
     * 后台查询门店日去化奖励
     * @param accountId
     * @param createDay
     * @return
     */
    List<FundsChangeNote> queryWeekClearChangeNote(@Param("accountId")Long accountId, @Param("createDay")String createDay, @Param("start")Integer start, @Param("length")Integer length);

    /**
     * 统计后台查询门店日去化奖励
     * @param accountId
     * @param createDay
     * @return
     */
    Integer countWeekClearChangeNote(@Param("accountId")Long accountId, @Param("createDay")String createDay);
}

