package com.qmfresh.promotion.platform.infrastructure.job.activity.general;

import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpActivityManager;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
@JobHandler("ClosePlatformCpActivityJob")
public class CloseGeneralCpActivityJob extends IJobHandler {

    @Resource
    private GeneralCpActivityManager generalCpActivityManager;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        log.info("关闭总部改价开始....");
        generalCpActivityManager.stopGeneralCpActivityByJob();
        log.info("关闭总部改价结束");
        return ReturnT.SUCCESS;
    }
}
