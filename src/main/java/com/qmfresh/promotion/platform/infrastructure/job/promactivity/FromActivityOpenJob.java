package com.qmfresh.promotion.platform.infrastructure.job.promactivity;

import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityManager;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * 活动开启
 */
@JobHandler("FromActivityOpenJob")
@Slf4j
@Component
public class FromActivityOpenJob extends IJobHandler {

    @Resource
    private PromActivityManager promActivityManager;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        String traceId = UUID.randomUUID().toString().replaceAll("-", "");
        log.info("活动开启定时任务-开始...traceId={}", traceId);
        promActivityManager.openJob();
        log.info("活动开启定时任务-结束... traceId={}", traceId);
        return ReturnT.SUCCESS;
    }
}
