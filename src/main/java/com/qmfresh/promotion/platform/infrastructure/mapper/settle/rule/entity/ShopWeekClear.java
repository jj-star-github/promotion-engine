package com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@ToString
@Data
@TableName("t_shop_week_clear")
public class ShopWeekClear implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 门店id
     */
    @TableField("shop_id")
    private Long shopId;
    /**
     * skuid
     */
    @TableField("sku_id")
    private Long skuId;
    /**
     * 商品每日原价
     */
    @TableField("origin_price")
    private BigDecimal originPrice;
    /**
     * 商品当日实际到货数量
     */
    @TableField("real_amount")
    private BigDecimal realAmount;
    /**
     * 商品实际到货值
     */
    @TableField("price_amount")
    private BigDecimal priceAmount;
    /**
     * sku是否参与周清(0否，1是)
     */
    @TableField("is_skuClear")
    private Integer isSkuClear;
    /**
     * 日期
     */
    @TableField("create_day")
    private String createDay;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;
    /**
     * 删除
     */
    @TableField("is_deleted")
    private Integer isDeleted;
}
