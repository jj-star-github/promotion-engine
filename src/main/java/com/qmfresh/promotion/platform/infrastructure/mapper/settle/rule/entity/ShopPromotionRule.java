package com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@ToString
@Data
@TableName("t_shop_promotion_rule")
public class ShopPromotionRule implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private Long id;
    /**
     * 门店等级(123)
     */
    @TableField("shop_grade")
    private Integer shopGrade;
    /**
     * 门店类别(1.A,2.B,3.C)
     */
    @TableField("shop_type")
    private Integer shopType;
    /**
     * 损耗率
     */
    @TableField("loss_percent")
    private BigDecimal lossPercent;
    /**
     * 营销费率
     */
    @TableField("promotion_percent")
    private BigDecimal promotionPercent;
    /**
     * 总部占比
     */
    @TableField("base_percent")
    private BigDecimal basePercent;
    /**
     * 大区占比
     */
    @TableField("area_percent")
    private BigDecimal areaPercent;
    /**
     * 门店占比
     */
    @TableField("shop_percent")
    private BigDecimal shopPercent;
    /**
     * 操作人id
     */
    @TableField("operator_id")
    private Integer operatorId;
    /**
     * 操作人名称
     */
    @TableField("operator_name")
    private String operatorName;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;
    /**
     * 删除(0:未删除1:已删除)
     */
    @TableField("is_deleted")
    private Integer isDeleted;

    /**
     * 周清比例
     */
    @TableField("weekClear_percent")
    private BigDecimal weekClearPercent;
    /**
     * 门店周清比例
     */
    @TableField("weekClear_shop_percent")
    private BigDecimal weekClearShopPercent;
    /**
     * 大区周清比例
     */
    @TableField("weekClear_area_percent")
    private BigDecimal weekClearAreaPercent;
}
