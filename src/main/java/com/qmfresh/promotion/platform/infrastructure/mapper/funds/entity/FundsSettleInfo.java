package com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 营销费用订单信息
 */
@TableName("t_funds_settle_info")
@Data
@ToString
public class FundsSettleInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private Long accountId;
    private String orderCode;
    private Integer shopId;
    private Integer shopType;
    private BigDecimal realPay;
    private BigDecimal marketingPay;
    private BigDecimal fundsAmount;
    private BigDecimal shopFundsAmount;
    private BigDecimal areaFundsAmount;
    private BigDecimal useAmount;
    private BigDecimal shopUseAmount;
    private BigDecimal areaUseAmount;
    private String createDay;
    private Date gmtCreate;
    private Date gmtModified;
    private Integer isDeleted;

}
