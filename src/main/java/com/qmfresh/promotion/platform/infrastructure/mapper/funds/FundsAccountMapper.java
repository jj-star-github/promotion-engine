package com.qmfresh.promotion.platform.infrastructure.mapper.funds;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsAccount;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsAccountSum;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsChangeNote;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 营销费用账户信息
 */
public interface FundsAccountMapper extends BaseMapper<FundsAccount> {

    /**
     * 根据门店ID和门店类型查询 账户信息
     * @param shopId
     * @param shopType
     * @return
     */
    FundsAccount byShopIdAndType(@Param("shopId") Integer shopId, @Param("shopType") Integer shopType);

    /**
     * 费用更新
     * @param account
     */
    void updateAccount(FundsAccount account);

    /**
     * 根据门店类型计算总营销费用和总锁定费用
     * @param shopType 门店类型
     * @return
     */
    FundsAccountSum getSumBy(@Param("shopType") Integer shopType);

    /**
     * 查询费用小于等于零的商户信息
     * @return
     */
    List<FundsAccount> queryNotEnoughAccount();

    /**
     * 根据业务类型查询账户
     * @param shopTypes
     * @param shopName
     * @param shopId
     * @param areaName
     * @param start
     * @param length
     * @return
     */
    List<FundsAccount> queryAccountList(@Param("shopTypes")List<Integer> shopTypes, @Param("shopName")String shopName,@Param("shopId")Integer shopId,@Param("areaName")String areaName, @Param("start")Integer start, @Param("length")Integer length);

    /**
     * 根据业务类型查询账户数量
     * @param shopTypes
     * @param shopName
     * @param shopId
     * @param areaName
     * @return
     */
    Integer queryAccountListCount(@Param("shopTypes")List<Integer> shopTypes, @Param("shopName")String shopName,@Param("shopId")Integer shopId,@Param("areaName")String areaName);

}