package com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 门店日统计
 */
@Data
public class FundsShopDayAmountSum implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 总销售额
     */
    private BigDecimal sumRealPay;
    /**
     * 营销销售额
     */
    private BigDecimal sumMarketingPay;
    /**
     * 总营新增销费用
     */
    private BigDecimal sumFundsAmount;
    /**
     * 总使用营销费用
     */
    private BigDecimal sumUseAmount;
    /**
     *
     */
    private String createDay;

}
