package com.qmfresh.promotion.platform.infrastructure.job.promactivity;

import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityManager;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * 活动结束
 */
@JobHandler("FromActivityEndJob")
@Slf4j
@Component
public class FromActivityEndJob extends IJobHandler {

    @Resource
    private PromActivityManager promActivityManager;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        String traceId = UUID.randomUUID().toString().replaceAll("-", "");
        log.info("活动结束定时任务-开始...traceId={}", traceId);
        promActivityManager.endJob();
        log.info("活动结束定时任务-结束... traceId={}", traceId);
        return ReturnT.SUCCESS;
    }
}
