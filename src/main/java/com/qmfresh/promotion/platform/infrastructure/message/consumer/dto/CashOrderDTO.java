package com.qmfresh.promotion.platform.infrastructure.message.consumer.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Author zbr
 * @Date 15:44 2020/8/22
 */
@Data
public class CashOrderDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 门店id
     */
    private Integer shopId;
    /**
     * 订单id
     */
    private Long orderId;
    /**
     * -1已取消0未确认1商家已确认2后台已确认3已发运4配送中5已收货6对账完成
     */
    private Integer status;
    /**
     * 支付方式 1为余额，2为支付宝，3微信， 4 现金,5挂单,6饿了么,7美团
     */
    private Integer payChannelType;
    /**
     * 下单用户id
     */
    private Integer userId;
    /**
     * 订单编码
     */
    private String orderCode;
    /**
     * 商交易金额: 用户实际付款金额
     */
    private BigDecimal goodsAmount;
    /**
     * 折扣金额: 单品折扣和+整单折扣
     */
    private BigDecimal discountPrice;
    /**
     * 原交易金额、订单行之和
     */
    private BigDecimal goodsPrice;
    /**
     * 抵扣番茄币数
     */
    private Integer tomatoCoins;
    /**
     * 币抵扣相应的钱
     */
    private BigDecimal coinToMoney;
    /**
     * 优惠劵id，没有不传
     */
    private Long couponId;
    /**
     * 优惠劵价格
     */
    private BigDecimal couponPrice;
    /**
     * 抹零免单金额
     */
    private BigDecimal freePrice;
    /**
     * 订单生成日期，格式时间戳
     */
    private Long createTime;
    /**
     * 挂单人
     */
    private Integer guadanId;
    /**
     * 抵扣积分
     */
    private Long deductCredit;
    /**
     *
     */
    private Integer passportId;
    /**
     * 支付id
     */
    private String payId;
    /**
     * 订单类型：0，pc端订单；1，移动端订单
     */
    private Integer orderType;
    /**
     * 创建人
     */
    private Long createdBy;
    /**
     * 订单上报时间
     */
    private Long cT;
    /**
     * ‘0正常下单7部分退款8全部退款’
     */
    private Integer refundStatus;
    /**
     * 订单明细列表
     */
    private List<CashOrderItemDTO> items;

}
