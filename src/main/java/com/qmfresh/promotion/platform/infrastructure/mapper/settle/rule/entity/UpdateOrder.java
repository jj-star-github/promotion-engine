package com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class UpdateOrder implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 订单编码
     */
    private String orderCode;
}
