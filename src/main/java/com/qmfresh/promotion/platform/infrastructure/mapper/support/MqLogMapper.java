package com.qmfresh.promotion.platform.infrastructure.mapper.support;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.platform.domain.model.support.MqLog;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface MqLogMapper extends BaseMapper<MqLog> {
}
