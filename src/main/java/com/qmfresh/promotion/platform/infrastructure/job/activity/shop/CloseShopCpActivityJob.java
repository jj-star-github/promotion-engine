package com.qmfresh.promotion.platform.infrastructure.job.activity.shop;

import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivityManager;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@JobHandler("CloseShopCpActivityJob")
@Slf4j
@Component
public class CloseShopCpActivityJob extends IJobHandler {

    @Resource
    private ShopCpActivityManager shopCpActivityManager;

    @Override
    public ReturnT<String> execute(String param) throws Exception {

        log.info("开始定时失效门店改价...{}", param);
        shopCpActivityManager.closeByJob();
        log.info("结束定时失效门店改价...{}", param);
        return ReturnT.SUCCESS;
    }
}
