package com.qmfresh.promotion.platform.infrastructure.message.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.dto.CloseShopCpActivityDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 根据shopId关闭对应改价活动
 */
@Service
@Slf4j
@RocketMQMessageListener(consumerGroup = "${spring.application.name}CloseShopCpActivity", topic = "${mq.topic.closeShopCpActivity}")
public class CloseShopCpActivityConsumer extends AbstractRocketMQListener {

    @Override
    void handleMessage(String topic, String tags, String content, String msgId) {
        List<CloseShopCpActivityDTO> orderDTO = JSON.parseObject(content, new TypeReference<List<CloseShopCpActivityDTO>>() {
        });
        log.info("根据shopId关闭对应改价活动 shop={}", JSON.toJSONString(orderDTO));
        return;
    }
}
