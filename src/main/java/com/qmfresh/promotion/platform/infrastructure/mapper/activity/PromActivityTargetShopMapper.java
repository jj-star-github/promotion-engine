package com.qmfresh.promotion.platform.infrastructure.mapper.activity;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityTargetShopPO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 活动基础信息
 */
public interface PromActivityTargetShopMapper extends BaseMapper<PromActivityTargetShopPO> {

    /**
     * 根据活动ID查询门店信息
     */
    List<PromActivityTargetShopPO> queryByActivity(@Param("activityId") Long activityId);

    /**
     * 查询活动目标门店ID
     * @param activityId
     * @return
     */
    List<Integer> queryShopId(@Param("activityId") Long activityId);

    /**
     * 批量删除目标门店信息
     * @param activityId
     * @param shopIdList
     */
    void deletedBatch(@Param("activityId")Long activityId, @Param("shopIdList")List<Integer> shopIdList, @Param("gmtModified")Date gmtModified);


    /**
     * 根据活动ID
     * @param activityList
     * @param shopId
     * @param shopType
     * @return
     */
    List<Long> exchangeActivity(@Param("activityList")List<Long> activityList, @Param("shopId")Integer shopId, @Param("shopType")Integer shopType);

    /**
     * 根据活动ID查询 交集活动
     * @param shopIdList
     * @param shopType
     * @param activityList
     * @return
     */
    List<Long> queryActivityId(@Param("shopIdList")List<Integer> shopIdList, @Param("shopType")Integer shopType, @Param("activityList")List<Long> activityList);

}