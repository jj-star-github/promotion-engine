package com.qmfresh.promotion.platform.infrastructure.mapper.activity;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityConvertPO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 活动基础信息
 */
public interface PromActivityConvertMapper extends BaseMapper<PromActivityConvertPO> {

    /**
     * 根据订单号查询 未领取或领取失败的
     * @return
     */
    List<PromActivityConvertPO> queryByCanSend(@Param("userId")Integer userId, @Param("shopId")Integer shopId, @Param("businessCode")String businessCode, @Param("businessType")Integer businessType);

    /**
     * 根据订单号查询 未领取或领取失败的
     * @return
     */
    List<PromActivityConvertPO> queryByBusiness(@Param("businessCode")String businessCode, @Param("businessType")Integer businessType);

    /**
     * 跟新奖励发放状态
     * @param idList
     * @param status
     */
    void batchUpdateStatus(@Param("idList")List<Long> idList, @Param("status")Integer status);

    /**
     * 查询时间端奖励发放失败数据
     * @param startTime
     * @param endTime
     * @return
     */
    List<PromActivityConvertPO> querySendErrorConvert(@Param("startTime") Date startTime, @Param("endTime")Date endTime);

}