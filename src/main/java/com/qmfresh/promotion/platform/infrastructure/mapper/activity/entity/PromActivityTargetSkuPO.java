package com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityTargetSkuDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 活动目标商品
 */
@TableName("t_prom_activity_target_sku")
@Data
public class PromActivityTargetSkuPO implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 活动ID
     */
    private Long activityId;
    /**
     *  skuId
     */
    private Integer skuId;
    /**
     * 品类型:1:全品,2:Class1,3:class2,4:sku
     */
    private Integer skuType;

    private String skuName;

    private Date gmtCreate;
    private Date gmtModified;
    private Integer isDeleted;

    private Integer class1Id;
    private Integer class2Id;

    public PromActivityTargetSkuPO(){}

    public PromActivityTargetSkuPO(PromActivityTargetSkuDTO dto){
        this.setSkuId(dto.getSkuId());
        this.setSkuName(dto.getSkuName());
        this.setSkuType(dto.getSkuType());
        this.setClass1Id(dto.getClass1Id());
        this.setClass2Id(dto.getClass2Id());
        Date now = new Date();
        this.setGmtCreate(now);
        this.setGmtModified(now);
    }

}
