package com.qmfresh.promotion.platform.infrastructure.message.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.core.RocketMQListener;

import java.nio.charset.Charset;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Slf4j
public abstract class AbstractRocketMQListener implements RocketMQListener<MessageExt> {
    @Override
    public void onMessage(MessageExt message) {
        log.info("接收到消息： topic ={} tags= {}  msgId = {} ", message.getTopic(), message.getTags(), message.getMsgId());
        byte[] body = message.getBody();
        String content = new String(body, Charset.forName("utf-8"));
        String topic = message.getTopic();
        String tags = message.getTags();
        String msgId = message.getMsgId();
        log.info("接收到消息内容:topic={} tags={},content={} msgId={}", topic, tags, content, msgId);
        try {
            handleMessage(topic, tags, content, msgId);
        } catch (Exception e) {
            log.error("消息无法处理: topic = {} tags={} msgId = {} content = {}", topic, tags, msgId, content, e);
        }
    }

    /**
     * 具体处理消息
     *
     * @param topic   消息主题
     * @param tags    消息tags
     * @param content 消息内容
     * @param msgId   msgId
     */
    abstract void handleMessage(String topic, String tags, String content, String msgId);
}
