package com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 门店去化奖励
 */
@Data
public class FundsWeekClear implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 去化奖励额
     */
    private BigDecimal weekClear;
    /**
     * 去化奖励类型  1.加 -1减
     */
    private Integer changeType;
    /**
     *日期
     */
    private String createDay;
    /**
     *备注
     */
    private String remark;

}
