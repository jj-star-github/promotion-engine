package com.qmfresh.promotion.platform.domain.model.promactivity.enums;


import lombok.Getter;

/**
 * 奖品发放状态
 */
@Getter
public enum ConvertSendStatusEnum {

    /**
     * 未发放
     */
    WAIT(1),
    /**
     * 已发放
     */
    SUCCESS(2),
    /**
     * 发放失败
     */
    ERROR(3),
    /**
     * 已取消
     */
    REGAIN(4);

    private Integer code;

    ConvertSendStatusEnum(Integer code) {
        this.code = code;
    }
}
