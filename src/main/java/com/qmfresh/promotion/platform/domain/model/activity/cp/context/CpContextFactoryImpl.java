package com.qmfresh.promotion.platform.domain.model.activity.cp.context;

import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.Subject;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpActivityManager;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivityManager;
import com.qmfresh.promotion.platform.domain.model.activity.cp.action.CpAction;
import com.qmfresh.promotion.platform.domain.model.activity.cp.vip.VipCpActivityManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class CpContextFactoryImpl implements CpContextFactory {


    @Resource
    private ShopCpActivityManager shopCpActivityManager;
    @Resource
    private GeneralCpActivityManager generalCpActivityManager;
    @Resource
    private VipCpActivityManager vipCpActivityManager;

    /**
     * 创建门店改价上下文
     *
     * @param operator 操作者
     * @param cpAction 改价动作
     * @return 改价上下文
     */
    @Override
    public CpContext<ShopCpActivity> createShopCp(Operator operator, CpAction cpAction) {
        ShopCpContext shopCpContext = new ShopCpContext();
        shopCpContext.setOperator(operator);
        shopCpContext.setCpAction(cpAction);
        shopCpContext.setShop((Subject.Type.SHOP.equals(
                operator.getRepresentSubject().getType())) ? (Shop) operator.getRepresentSubject().getExtend() : null);
        shopCpContext.setShopCpActivityManager(shopCpActivityManager);
        return shopCpContext;
    }




    @Override
    public CpContext createPlatformCp(Operator operator, CpAction cpAction) {
        PlatformCpContext platformCpContext = new PlatformCpContext();
        platformCpContext.setGeneralCpActivityManager(generalCpActivityManager);
        return platformCpContext;
    }

    @Override
    public CpContext createVipCp(Operator operator, CpAction cpAction) {
        VipCpContext context = new VipCpContext();
        context.setVipCpActivityManager(vipCpActivityManager);
        return context;
    }


}
