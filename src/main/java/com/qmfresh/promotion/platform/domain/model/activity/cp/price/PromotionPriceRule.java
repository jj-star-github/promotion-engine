package com.qmfresh.promotion.platform.domain.model.activity.cp.price;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class PromotionPriceRule implements Serializable {

    //活动信息
    private Long activityId;
    private Long activityCpId;
    private Integer shopId;
    private Integer skuId;
    private CpBizType cpBizType;
    private Integer type;
    private BigDecimal rulePrice;

    //本次营销费用分摊
    private BigDecimal marketingDiffFee;
    private Long createTimestamp;
    private BigDecimal originalPrice;
    private Integer cpType;
    private Long planStartCpActivityTime;
    private String lastOperatorName;


    public static PromotionPriceRule create(ShopCpActivity shopCpActivity, BigDecimal originalPrice, BigDecimal marketingDiffPrice) {
        PromotionPriceRule promotionRule =
                PromotionPriceRule.builder()
                        .activityId(shopCpActivity.getActivityId())
                        .cpBizType(shopCpActivity.getBizType())
                        .rulePrice(shopCpActivity.getRulePrice())
                        .marketingDiffFee(marketingDiffPrice)
                        .originalPrice(originalPrice)
                        .createTimestamp(System.currentTimeMillis())
                        .cpType(shopCpActivity.getCpType().getCode())
                        .skuId(shopCpActivity.getSku().getSkuId())
                        .shopId(shopCpActivity.getShop().getShopId())
                        .activityCpId(shopCpActivity.getActivityCpId())
                        .planStartCpActivityTime(shopCpActivity.getPlanActivityStartTime().getTime())
                        .lastOperatorName(shopCpActivity.getLastOperatorPerson())
                        .type(shopCpActivity.getBizType().getCode())
                        .build();
        return promotionRule;
    }


    public static PromotionPriceRule create(GeneralCpItem generalCpItem, BigDecimal originalPrice, BigDecimal marketingDiffPrice) {
        PromotionPriceRule promotionRule =
                PromotionPriceRule.builder()
                        .activityId(generalCpItem.getActivityId())
                        .cpBizType(generalCpItem.getCpBizType())
                        .rulePrice(generalCpItem.getRulePrice())
                        .marketingDiffFee(marketingDiffPrice)
                        .originalPrice(originalPrice)
                        .createTimestamp(System.currentTimeMillis())
                        .cpType(generalCpItem.getCpType().getCode())
                        .shopId(generalCpItem.getShop().getShopId())
                        .skuId(generalCpItem.getSku().getSkuId())
                        .planStartCpActivityTime(generalCpItem.getCpTimeCycle().getBeginTime().getTime())
                        .activityCpId(generalCpItem.getActivityCpId())
                        .lastOperatorName(generalCpItem.getLastOperatorName())
                        .type(generalCpItem.getCpBizType().getCode())
                        .build();
        return promotionRule;
    }

}
