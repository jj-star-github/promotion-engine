package com.qmfresh.promotion.platform.domain.model.activity.cp.context;

import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.action.CloseCpAction;
import com.qmfresh.promotion.platform.domain.model.activity.cp.action.CpAction;
import com.qmfresh.promotion.platform.domain.model.activity.cp.action.StartCpAction;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivityManager;
import com.qmfresh.promotion.platform.domain.shared.Page;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@Slf4j
public class ShopCpContext implements CpContext<ShopCpActivity> {
    /**
     * 门店信息
     */
    private Shop shop;
    /**
     * 操作者
     */
    private Operator operator;
    /**
     * 改价计划动作
     */
    private CpAction cpAction;


    //基础依赖
    private ShopCpActivityManager shopCpActivityManager;

    private List<ShopCpActivity> availableShopCpActivities;


    /**
     * 上下文中定义了创建的过程
     *
     * @param cpContext 改价上下文
     */
    @Override
    public ShopCpActivity create(CpContext<ShopCpActivity> cpContext) {
        ShopCpContext shopCpContext = (ShopCpContext) cpContext;
        ShopCpActivity shopCpActivity = ShopCpActivity.create(cpContext);
        return shopCpActivity;
    }

    /**
     * 检查当前是否存在
     *
     * @param cpContext 启动改价
     * @return
     */
    @Override
    public List<ShopCpActivity> start(CpContext<ShopCpActivity> cpContext) {
        ShopCpContext shopCpContext = (ShopCpContext) cpContext;

        //todo 单个活动的启动
        StartCpAction startCpAction = (StartCpAction) shopCpContext.getCpAction();
        if (startCpAction.getActivityId() != null) {
            ShopCpActivity shopCpActivity = shopCpActivityManager.shopCpActivityByActivityId(startCpAction.getActivityId());
            doStartShopCpActivity(shopCpContext, shopCpActivity);
            log.info("直接启动门店改价活动：activityId={} activityCpId={} skuId={} rulePrice={}", shopCpActivity.getActivityId(),
                    shopCpActivity.getActivityCpId(), shopCpActivity.getSku().getSkuId(), shopCpActivity.getRulePrice());
            return Collections.singletonList(shopCpActivity);
        }

        //检查当前是否存在门店改价活动
        Page<ShopCpActivity> availableActivities = shopCpActivityManager.toStartShopCpActivity(1, 100);
        while (availableActivities.getRecords().size() >= 100) {
            //遍历处理门店改价活动
            shopCpContext.availableShopCpActivities = availableActivities.getRecords();
            doStartShopCpActivities(shopCpContext);
            availableActivities = shopCpActivityManager.toStartShopCpActivity(1, 100);
        }
        shopCpContext.availableShopCpActivities = availableActivities.getRecords();
        doStartShopCpActivities(shopCpContext);

        return Collections.EMPTY_LIST;

    }

    private void doStartShopCpActivities(ShopCpContext shopCpContext) {
        for (ShopCpActivity shopCpActivity : shopCpContext.availableShopCpActivities) {
            try {
                doStartShopCpActivity(shopCpContext, shopCpActivity);
            } catch (Exception e) {
                log.warn("启动门店改价活动失败：");
            }
        }


    }

    private void doStartShopCpActivity(ShopCpContext shopCpContext, ShopCpActivity shopCpActivity) {
        shopCpActivity.start(shopCpContext);
    }

    @Override
    public ShopCpActivity close(CpContext<ShopCpActivity> cpContext) {
        //获取当前活动，检查活动当前状态
        ShopCpContext shopCpContext = (ShopCpContext) cpContext;
        CloseCpAction<ShopCpActivity> cpActivityCloseCpAction = (CloseCpAction<ShopCpActivity>)
                shopCpContext.getCpAction();
        //关闭当前活动
        ShopCpActivity shopCpActivity = cpActivityCloseCpAction.getCpActivity();
        log.info("开始关闭门店改价：activityId={} ", shopCpActivity.getActivityId());
        shopCpActivity.close(shopCpContext);
        return shopCpActivity;
    }

    /**
     * 分页查询门店改价
     *
     * @param cpContext
     * @return
     */
    @Override
    public Page<ShopCpActivity> page(CpContext<ShopCpActivity> cpContext) {
        ShopCpContext context = (ShopCpContext) cpContext;
        return shopCpActivityManager.pageShopCpActivity(context);
    }

    /**
     * 重新建上下文，主要替换业务动作
     *
     * @param cpAction 关闭改价动作
     * @return 门店上下文
     */
    public ShopCpContext recrete(CpAction cpAction) {
        ShopCpContext shopCpContext = new ShopCpContext();
        shopCpContext.setShop(this.shop);
        shopCpContext.setOperator(this.operator);
        shopCpContext.setShopCpActivityManager(shopCpActivityManager);
        shopCpContext.setCpAction(cpAction);
        return shopCpContext;
    }


}
