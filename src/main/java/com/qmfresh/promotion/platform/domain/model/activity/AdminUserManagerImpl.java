package com.qmfresh.promotion.platform.domain.model.activity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qmfresh.promotion.platform.infrastructure.http.Https;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class AdminUserManagerImpl implements AdminUserManager {

    @Value("${url.service.sso}")
    private String ssoServiceUrl;

    /**
     * {"success":true,"errorCode":0,"message":"","body":{"id":1023,"userName":"9902","userPwd":"a729d76292a6a72fc99598bbc1e33ae6","status":1,"realName":"南京汇景花园店收银2","token":"","roleId":18,"shopId":99,"googleAuthKey":null,"isShopMaster":0,"needEditPwd":0}}
     *
     * @param userId 用户id
     * @return
     */
    @Override
    public AdminUser adminUser(Integer userId) {
        String result = Https.post(ssoServiceUrl + "/sso/getInfoById", null, JSON.toJSONString(AdminUserParam.create(userId)));

        JSONObject jsonObject = JSON.parseObject(result);
        Boolean success = jsonObject.getBoolean("success");
        if (success == null || !success) {
            log.warn("调用用户信息失败：userId = {},errMesg = {}", userId, jsonObject.getString("message"));
            return null;
        }
        JSONObject body = jsonObject.getJSONObject("body");
        AdminUser adminUser = new AdminUser();
        adminUser.setUserId(body.getInteger("id"));
        adminUser.setName(body.getString("userName"));
        adminUser.setRealName(body.getString("realName"));
        adminUser.setRoleId(body.getInteger("roleId"));
        adminUser.setShopId(body.getInteger("shopId"));
        return adminUser;
    }

    @Data
    public static class AdminUserParam {
        private Integer userId;

        public static AdminUserParam create(Integer userId) {
            AdminUserParam adminUserParam = new AdminUserParam();
            adminUserParam.setUserId(userId);
            return adminUserParam;
        }
    }
}
