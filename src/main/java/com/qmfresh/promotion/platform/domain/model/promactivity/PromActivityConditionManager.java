package com.qmfresh.promotion.platform.domain.model.promactivity;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.platform.domain.model.promactivity.vo.HitTargetConditionVo;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityConditionPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityPO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityCampOnSkuDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityConditionDTO;

import java.util.List;

/**
 * 活动奖品信息
 */
public interface PromActivityConditionManager extends IService<PromActivityConditionPO> {

    /**
     * 根据活动ID查询
     * @param activityId
     * @return
     */
    List<PromActivityConditionPO> queryByActivity(Long activityId);

    /**
     * 根据活动ID批量查询
     * @param activityIdList
     * @return
     */
    List<PromActivityConditionPO> queryByActivityList(List<Long> activityIdList);

    /**
     * 批量插入奖品信息
     * @param activityId
     * @param skuDTOList
     */
    void insertBatch(Long activityId, List<PromActivityConditionDTO> skuDTOList);

    /**
     * 奖品更新
     * @param conditionDto
     */
    void updateCondition(PromActivityConditionDTO conditionDto);

    /**
     *
     * @param skuDTOList
     * @param activityPO
     * @return
     */
    HitTargetConditionVo hitTarget(List<PromActivityCampOnSkuDTO> skuDTOList, PromActivityPO activityPO, String traceId);

    /**
     * 修改奖品库存
     * @param conditionPO  奖品信息
     * @param num           发奖数量
     * @return
     */
    int updateStock(PromActivityConditionPO conditionPO, Integer num);

}
