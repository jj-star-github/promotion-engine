package com.qmfresh.promotion.platform.domain.model.activity.cp.general.command;

import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.Sku;
import com.qmfresh.promotion.platform.domain.model.activity.SkuPrice;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.SkuCpItem;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public class GeneralCpOneShopCreateCommand implements Serializable {

    private Operator operator;
    private String activityName;
    private CpTimeCycle timeCycle;
    private Shop shop;
    private List<Sku> skus;
    private List<SkuCpItem> skuCpItems;
    private Long activityId;
    private List<SkuPrice> skuPrices;

    public GeneralCpOneShopCreateCommand(
            Operator operator,
            String activityName,
            Long activityId,
            CpTimeCycle timeCycle,
            Shop shop,
            List<Sku> skus,
            List<SkuCpItem> skuCpItems,
            List<SkuPrice> skuPrices
    ) {

        this.operator = operator;
        this.activityName = activityName;
        this.timeCycle = timeCycle;
        this.shop = shop;
        this.skuCpItems = skuCpItems;
        this.skus = skus;
        this.activityId = activityId;
        this.skuPrices = skuPrices;
    }

    public static GeneralCpOneShopCreateCommand create(
            Operator operator,
            Long activityId,
            CpTimeCycle timeCycle,
            Shop shop,
            List<Sku> skus,
            List<SkuCpItem> skuCpItems,
            List<SkuPrice> skuPrices
    ) {

        GeneralCpOneShopCreateCommand generalCpOneShopCreateCommand =
                new GeneralCpOneShopCreateCommand(operator, null, activityId, timeCycle, shop, skus, skuCpItems, skuPrices);

        return generalCpOneShopCreateCommand;
    }
}
