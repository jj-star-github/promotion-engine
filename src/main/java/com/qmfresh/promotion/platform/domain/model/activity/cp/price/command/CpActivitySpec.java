package com.qmfresh.promotion.platform.domain.model.activity.cp.price.command;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qmfresh.promotion.platform.domain.model.activity.cp.AuditStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.AreaCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import com.qmfresh.promotion.platform.domain.model.activity.cp.vip.VipCpActivity;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.ChangePriceActivityMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ActivityPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ChangePriceActivityPO;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */

@Data
@Slf4j
public class CpActivitySpec implements Serializable {

    private GeneralCpItem generalCpItem;
    private AreaCpItem areaCpItem;
    private ShopCpActivity shopCpActivity;
    private VipCpActivity vipCpActivity;


    public static CpActivitySpec spec(GeneralCpItem generalCpItem,
                                      AreaCpItem areaCpItem,
                                      ShopCpActivity shopCpActivity,
                                      VipCpActivity vipCpActivity) {
        CpActivitySpec cpActivitySpec = new CpActivitySpec();
        cpActivitySpec.setGeneralCpItem(generalCpItem);
        cpActivitySpec.setAreaCpItem(areaCpItem);
        cpActivitySpec.setShopCpActivity(shopCpActivity);
        cpActivitySpec.setVipCpActivity(vipCpActivity);
        return cpActivitySpec;
    }

    /**
     * 假设同一时间，只能创建一个同一类型的改价活动
     *
     * @param mapper 访问对象
     * @param shopId 门店id
     * @param skuId  商品id
     * @return 当前运行时改价活动规范
     */
    public static CpActivitySpec spec(ChangePriceActivityMapper mapper, Integer shopId, Integer skuId) {
        List<ChangePriceActivityPO> activityPOS = mapper.selectList(
                new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getStatus, CpStatus.RUNNING.getCode())
                        .eq(ChangePriceActivityPO::getAuditStatus, AuditStatus.AUDIT_SUCCESS.getCode())
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getShopId, shopId)
                        .eq(ChangePriceActivityPO::getSkuId, skuId)
        );
        Map<Integer, ChangePriceActivityPO> changePriceActivityPOMap = new HashMap<>();
        if (CollectionUtils.isNotEmpty(activityPOS)) {
            for (ChangePriceActivityPO activityPO : activityPOS) {
                ChangePriceActivityPO tmp = changePriceActivityPOMap.get(activityPO.getType());
                if (tmp == null) {
                    changePriceActivityPOMap.put(activityPO.getType(), activityPO);
                } else {
                    log.error("存在重复启动的活动：type={} tmpId ={} newId={} shopId={} skuId={}",
                            activityPO.getType(), tmp.getId(), activityPO.getId(), activityPO.getShopId(), activityPO.getSkuId());
                    //启动时间晚的去计算
                    if (tmp.getPlanActivityStartTime().getTime() >= activityPO.getPlanActivityStartTime().getTime()) {
                        changePriceActivityPOMap.put(tmp.getType(), tmp);
                    } else {
                        changePriceActivityPOMap.put(activityPO.getType(), activityPO);
                    }
                }
            }
        }

        GeneralCpItem generalCpItem = changePriceActivityPOMap.get(CpBizType.CP_PLATFORM.getCode()) == null ?
                null : ChangePriceActivityPO.generalCpItem(changePriceActivityPOMap.get(CpBizType.CP_PLATFORM.getCode()));
        ShopCpActivity shopCpActivity = changePriceActivityPOMap.get(CpBizType.CP_SHOP.getCode()) == null ?
                null : ChangePriceActivityPO.shopCpActivity(changePriceActivityPOMap.get(CpBizType.CP_SHOP.getCode()));
        AreaCpItem areaCpItem = changePriceActivityPOMap.get(CpBizType.CP_AREA.getCode()) == null ?
                null : ChangePriceActivityPO.areaCpItem(changePriceActivityPOMap.get(CpBizType.CP_AREA.getCode()));

        return spec(generalCpItem, areaCpItem, shopCpActivity, null);


    }
}
