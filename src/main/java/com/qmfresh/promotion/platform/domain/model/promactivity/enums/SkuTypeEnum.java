package com.qmfresh.promotion.platform.domain.model.promactivity.enums;


import lombok.Getter;

/**
 * 品类型
 */
@Getter
public enum SkuTypeEnum {

    /**
     * 全拼
     */
    ALL(1),
    /**
     * 一级类目
     */
    CLASS1(2),
    /**
     * 单品
     */
    SKU(4),
    /**
     * 二级类目
     */
    CLASS2(3),
    ;
    private Integer code;

    SkuTypeEnum(Integer code) {
        this.code = code;
    }
}
