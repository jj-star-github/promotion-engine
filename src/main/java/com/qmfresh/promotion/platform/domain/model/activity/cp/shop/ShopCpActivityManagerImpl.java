package com.qmfresh.promotion.platform.domain.model.activity.cp.shop;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.SkuManager;
import com.qmfresh.promotion.platform.domain.model.activity.SkuPrice;
import com.qmfresh.promotion.platform.domain.model.activity.cp.AuditStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.action.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.context.CpContext;
import com.qmfresh.promotion.platform.domain.model.activity.cp.context.ShopCpContext;
import com.qmfresh.promotion.platform.domain.model.activity.cp.event.CpEvent;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPrice;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPriceCompute;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.command.CpActivitySpec;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.command.PromotionPriceCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.command.ShopCpChangedQueryCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.command.ShopCpCloseCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.command.ShopCpQueryCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.command.ShopCpStartCommand;
import com.qmfresh.promotion.platform.domain.model.funds.FundsAccountManager;
import com.qmfresh.promotion.platform.domain.model.support.MqLog;
import com.qmfresh.promotion.platform.domain.service.DLock;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsLockVO;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.ActivityMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.ChangePriceActivityMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ActivityPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ChangePriceActivityPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.support.MqLogMapper;
import com.qmfresh.promotion.platform.infrastructure.message.RocketMQSender;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class ShopCpActivityManagerImpl implements ShopCpActivityManager {

    private static final String DLOCK_PRE_KEY = "PROMOTION:CP:";
    @Resource
    private ChangePriceActivityMapper changePriceActivityMapper;
    @Resource
    private ActivityMapper activityMapper;
    @Resource
    private DefaultMQProducer defaultMQProducer;
    @Resource
    private MqLogMapper mqLogMapper;
    @Resource
    private SkuManager skuManager;
    @Resource
    private FundsAccountManager fundsAccountManager;
    @Resource
    private DLock dLock;
    @Resource
    private PromotionPriceCompute promotionPriceCompute;

    public static String cpItemKey(Integer type, Integer shopId, Integer skuId) {
        if (shopId == null) {
            throw new BusinessException("门店id不能为空");
        }
        if (skuId == null) {
            throw new BusinessException("商品id不能为空");
        }
        if (type == null) {
            throw new BusinessException("改价类型不能为空");
        }

        return DLOCK_PRE_KEY + type + ":" + shopId + ":" + skuId;
    }

    /**
     * @param cpContext 改价上下文
     * @return ShopCpActivity
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ShopCpActivity create(CpContext cpContext) {
        ShopCpContext shopCpContext = (ShopCpContext) cpContext;
        Shop shop = shopCpContext.getShop();
        Operator operator = shopCpContext.getOperator();
        PlanCpAction cpAction = (PlanCpAction) shopCpContext.getCpAction();

        //创建活动
        ActivityPO activity = ActivityPO.create(shopCpContext);
        activityMapper.insert(activity);
        //创建改价对象
        ChangePriceActivityPO changePriceActivityPO = ChangePriceActivityPO.create(shopCpContext, activity);
        changePriceActivityMapper.insert(changePriceActivityPO);
        //门店改价对象
        ShopCpActivity shopCpActivity = ChangePriceActivityPO.shopCpActivity(changePriceActivityPO);
        log.info("创建门店改价：activityId= {} activityCpId ={} shopId ={} skuId ={} skuName={} rulePrice={}", activity.getId(),
                changePriceActivityPO.getId(), changePriceActivityPO.getShopId(), changePriceActivityPO.getSkuId(), changePriceActivityPO.getSkuName(), changePriceActivityPO.getRulePrice());
        ShopCpContext eventContext = shopCpContext.recrete(PlanEventCpAction.create(shopCpActivity));

        boolean tryLock = dLock.tryLock(cpItemKey(CpBizType.CP_SHOP.getCode(), changePriceActivityPO.getShopId(), changePriceActivityPO.getSkuId()), 5000L);
        if (!tryLock) {
            throw new BusinessException("有用户正在修改门店改价");
        }

        if (cpAction.getPlanMarketingFee().doubleValue() > 0) {
            log.warn("计划营销费用大于零：不锁营销费用");
        } else {
            //锁定营销费用
            boolean result = fundsAccountManager.lockAmount(FundsLockVO.lockShop(activity.getId(), shop, cpAction.getPlanMarketingFee()));

            if (!result) {
                log.warn("营销费用不足：shopId={} skuId={} planMarketingFee={}", shop.getShopId(), cpAction.getSku().getSkuId(), cpAction.getPlanMarketingFee());
                throw new BusinessException("营销费用不足，请申请营销费用");
            }
        }

        RocketMQSender.doSend(defaultMQProducer, CpEvent.CP_EVENT_TOPIC, CpEvent.createEvent(eventContext), new MqLog.PersistCallback(mqLogMapper));
        log.info("创建门店改价事件发送：activityId ={} activityCpId ={} shopId ={} skuId={}",
                activity.getId(), changePriceActivityPO.getId(), changePriceActivityPO.getShopId(), changePriceActivityPO.getSkuId());


        if (changePriceActivityPO.getPlanActivityStartTime().getTime() <= System.currentTimeMillis()) {
            log.info("检查当前活动开始时间是否小于当前时间：activityCpId={} startTime={},直接启动",
                    changePriceActivityPO.getId(),
                    changePriceActivityPO.getPlanActivityStartTime());
            //直接启动门店改价
            ShopCpContext startContext = shopCpContext.recrete(
                    StartCpAction.create(changePriceActivityPO.getActivityId()));
            startContext.start(startContext);
        }

        return shopCpActivity;
    }

    @Override
    public Page<ShopCpActivity> toStartShopCpActivity(Integer pageNum, Integer pageSize) {
        IPage<ChangePriceActivityPO> changePriceActivityPOIPage = changePriceActivityMapper.selectPage(
                new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(pageNum, pageNum),
                new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getType, CpBizType.CP_SHOP.getCode())
                        .eq(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode())
                        .eq(ChangePriceActivityPO::getAuditStatus, AuditStatus.AUDIT_SUCCESS.getCode())
                        .le(ChangePriceActivityPO::getPlanActivityStartTime, new Date())
        );
        return Page.convertIPage(pageNum, pageSize, changePriceActivityPOIPage, ChangePriceActivityPO::shopCpActivity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void start(ShopCpContext shopCpContext, ShopCpActivity toStartShopCpActivity) {
        startOneShopCp(changePriceActivityMapper.selectById(toStartShopCpActivity.getActivityCpId()),
                ShopCpStartCommand.create("创建启动"));

//        //关闭门店改价活动
//        Shop shop = toStartShopCpActivity.getShop();
//        Sku sku = toStartShopCpActivity.getSku();
//
//        log.info("为了活动{}开始查询当前门店是否存在运行中的门店改价：shopId={} skuId ={}", toStartShopCpActivity.getActivityCpId(),
//                shop.getShopId(), sku.getSkuId());
//        //应该放到ShopCpActivity方法中或者上下文中
//        List<ChangePriceActivityPO> changePriceActivityPOList = changePriceActivityMapper.selectList(new LambdaQueryWrapper<ChangePriceActivityPO>()
//                .eq(ChangePriceActivityPO::getType, CpBizType.CP_SHOP.getCode())
//                .eq(ChangePriceActivityPO::getAuditStatus, AuditStatus.AUDIT_SUCCESS.getCode())
//                .eq(ChangePriceActivityPO::getStatus, CpStatus.RUNNING.getCode())
//                .eq(ChangePriceActivityPO::getShopId, shop.getShopId())
//                .eq(ChangePriceActivityPO::getSkuId, sku.getSkuId())
//        );
//
//
//        //以下是门店改价的整套体系
//        SkuPrice skuPrice = skuManager.shopSkuPrice(shop.getShopId(), sku.getSkuId());
//        if (skuPrice == null) {
//            log.error("当前商品价格不存在 shopId = {} skuId={}", shop.getShopId(), sku.getSkuId());
//            throw new BusinessException("当前门店商品价格异常");
//        }
//        if (skuPrice.getCurrentPrice() == null) {
//            log.error("当前商品价格不存在 shopId = {} skuId={}", shop.getShopId(), sku.getSkuId());
//            throw new BusinessException("当前商品价格不存在");
//        }
//        BigDecimal rulePrice = toStartShopCpActivity.getRulePrice();
//
//
//        //关闭当前所有的门店改价活动
//        if (CollectionUtils.isNotEmpty(changePriceActivityPOList)) {
//            for (ChangePriceActivityPO changePriceActivityPO : changePriceActivityPOList) {
//                log.info("查询到{}个要关闭门店改价活动，开始关闭活动id={},shopId={} skuId={}", changePriceActivityPOList.size(),
//                        changePriceActivityPO.getId(), changePriceActivityPO.getShopId(), changePriceActivityPO.getSkuId());
//                ShopCpContext recrete = shopCpContext.recrete(
//                        CloseCpAction
//                                .createCommand(ChangePriceActivityPO.shopCpActivity(changePriceActivityPO),
//                                        ShopCpActivity.CLOSE_REMARK_START_NEW_SHOP_CP)
//                );
//                recrete.close(recrete);
//            }
//        }
//        //todo check the compute logic
//        PromotionPriceCommand priceCommand = PromotionPriceCommand
//                .builder()
//                .originalPrice(skuPrice.getOriginalPrice())
//                .shopCpActivity(toStartShopCpActivity)
//                .generalCpItem(get(shop.getShopId(), sku.getSkuId()))
//                .build();
//        PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
//        promotionPriceCompute.snapshot(promotionPrice, "门店改价启动");
//
//
//        Long activityCpId = toStartShopCpActivity.getActivityCpId();
//        log.info("activityCpId={} originalDiffMarketingFee ={}  currentPrice={} originPrice={} ", activityCpId, promotionPrice.getLastPromotionPrice(), skuPrice.getCurrentPrice(), skuPrice.getOriginalPrice());
//
//
//        BigDecimal marketingDiffFee = BigDecimal.ZERO;
//        if (promotionPrice.getSpec().getShopPromotion() != null) {
//            marketingDiffFee = promotionPrice.getSpec().getShopPromotion().getMarketingDiffFee();
//        }
//
//        //创建改价活动事件上下文
//        ShopCpContext eventContext = shopCpContext
//                .recrete(StartEventCpAction.createCommand(toStartShopCpActivity, skuPrice.getCurrentPrice(), marketingDiffFee));
//
//        ChangePriceActivityPO update = new ChangePriceActivityPO();
//        update.setId(activityCpId);
//        update.setStatus(CpStatus.RUNNING.getCode());
//        update.setOriginalPrice(skuPrice.getOriginalPrice());
//        update.setRealActivityStartTime(new Date());
//        update.setMarketingDiffPrice(marketingDiffFee);
//        update.setCurrentPrice(skuPrice.getCurrentPrice());
//        int i = this.changePriceActivityMapper.update(update, new LambdaQueryWrapper<ChangePriceActivityPO>()
//                .eq(ChangePriceActivityPO::getIsDeleted, 0)
//                .eq(ChangePriceActivityPO::getId, activityCpId)
//                .eq(ChangePriceActivityPO::getType, CpBizType.CP_SHOP.getCode())
//                .eq(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode())
//                .eq(ChangePriceActivityPO::getAuditStatus, AuditStatus.AUDIT_SUCCESS.getCode())
//        );
//
//
//        if (i == 0) {
//            log.warn("门店改价活动失败：{}", JSON.toJSONString(update));
//        } else {
//            log.info("门店改价成功：{}", activityCpId);
//            RocketMQSender.doSend(defaultMQProducer,
//                    CpEvent.CP_EVENT_TOPIC,
//                    CpEvent.createEvent(eventContext)
//                    , new MqLog.PersistCallback(mqLogMapper));
//        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void close(ShopCpContext shopCpContext) {
        CloseCpAction<ShopCpActivity> cpActivityCloseCpAction = (CloseCpAction<ShopCpActivity>) shopCpContext.getCpAction();
        ShopCpActivity toCloseShopCpActivity = cpActivityCloseCpAction.getCpActivity();

        if (toCloseShopCpActivity == null) {
            log.info("系统开始自动失效活动");
            shopCpActivityAutoClose(cpActivityCloseCpAction.getRemark());
            return;
        }

        ChangePriceActivityPO changePriceActivityPO = changePriceActivityMapper.selectById(toCloseShopCpActivity.getActivityCpId());
        String remark = cpActivityCloseCpAction.getRemark();
        ShopCpCloseCommand shopCpCloseCommand = ShopCpCloseCommand.createShop(shopCpContext.getOperator(), remark);
        closeOneShopCpActivity(changePriceActivityPO, shopCpCloseCommand);


    }

    @Override
    public void closeByJob() {
        shopCpActivityAutoClose("系统自动关闭");
    }

    private void closeOneShopCpActivity(ChangePriceActivityPO changePriceActivityPO, ShopCpCloseCommand shopCpCloseCommand) {
        Shop shop = ChangePriceActivityPO.getShop(changePriceActivityPO);

        if (ChangePriceActivityPO.hasClosed(changePriceActivityPO)) {
            log.warn("活动已经关闭：{}", JSON.toJSONString(changePriceActivityPO));
            return;
        }

        SkuPrice skuPrice = skuManager.shopSkuPrice(changePriceActivityPO.getShopId(), changePriceActivityPO.getSkuId());
        ChangePriceActivityPO update = new ChangePriceActivityPO();
        update.setId(changePriceActivityPO.getId());
        update.setStatus(CpStatus.CLOSED.getCode());
        update.setRemark(shopCpCloseCommand.getRemark());
        update.setGmtModified(new Date());
        int i = changePriceActivityMapper.update(update, new LambdaQueryWrapper<ChangePriceActivityPO>()
                .eq(ChangePriceActivityPO::getIsDeleted, 0)
                .eq(ChangePriceActivityPO::getId, changePriceActivityPO.getId())
                .eq(ChangePriceActivityPO::getType, CpBizType.CP_SHOP.getCode())
        );

        changePriceActivityPO.setStatus(CpStatus.CLOSED.getCode());
        changePriceActivityPO.setRemark(shopCpCloseCommand.getRemark());

        if (i == 0) {
            log.info("门店改价活动已关闭：{}", JSON.toJSONString(changePriceActivityPO));
        } else {
            if (skuPrice == null || skuPrice.getOriginalPrice() == null) {
                log.warn("商品信息为空：shopId={} skuId={}", changePriceActivityPO.getShopId(), changePriceActivityPO.getSkuId());
            } else {
                Integer shopId = changePriceActivityPO.getShopId();
                Integer skuId = changePriceActivityPO.getSkuId();
                String remark = shopCpCloseCommand.getRemark();
                CpActivitySpec spec = CpActivitySpec.spec(changePriceActivityMapper, shopId, skuId);
                if (spec.getShopCpActivity() != null && spec.getShopCpActivity().getActivityCpId().equals(changePriceActivityPO.getId())) {
                    log.warn("门店关闭的活动是其本身：shopId={} skuId={} cpId={} activityId={}",
                            shopId, skuId, changePriceActivityPO.getId(), changePriceActivityPO.getActivityId());
                    spec.setShopCpActivity(null);
                }
                //计算促销价
                PromotionPriceCommand priceCommand = PromotionPriceCommand
                        .create(shopId, skuId, skuPrice.getOriginalPrice(), spec);
                PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
                promotionPriceCompute.snapshot(promotionPrice, remark);
            }
            if (changePriceActivityPO.getPlanMarketingAmount().doubleValue() > 0) {
                log.info("门店改价计划营销费用为正，不释放");
            } else {
                //释放营销费用
                log.info("门店改价关闭，开始释放营销费用：activityId= {} activityCpId={}", changePriceActivityPO.getActivityId(), changePriceActivityPO.getId());
                fundsAccountManager.lockAmount(FundsLockVO.unlockShop(changePriceActivityPO.getActivityId(), shop, changePriceActivityPO.getPlanMarketingAmount()));
            }
            log.info("门店改价关闭成功：activityId = {} activityCpId={}", changePriceActivityPO.getActivityId(), changePriceActivityPO.getId());
            RocketMQSender.doSend(defaultMQProducer,
                    CpEvent.CP_EVENT_TOPIC,
                    CpEvent.create(changePriceActivityPO),
                    new MqLog.PersistCallback(mqLogMapper)
            );
        }
    }

    /**
     * 自动关闭
     */
    private void shopCpActivityAutoClose(String remark) {
        LambdaQueryWrapper<ChangePriceActivityPO> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ChangePriceActivityPO::getIsDeleted, 0);
        lambdaQueryWrapper.eq(ChangePriceActivityPO::getType, CpBizType.CP_SHOP.getCode());
        lambdaQueryWrapper.in(ChangePriceActivityPO::getStatus, CpStatus.RUNNING.getCode(), CpStatus.CREATE.getCode());
        lambdaQueryWrapper.le(ChangePriceActivityPO::getPlanActivityEndTime, new Date());

        ChangePriceActivityPO.visit(changePriceActivityMapper, lambdaQueryWrapper, ShopCpCloseCommand.createShop(remark, true),
                this::closeOneShopCpActivity);
    }


    /**
     * 分页查询门店改价
     *
     * @param shopCpContext 门店改价上下文
     * @return 分页门店改价活动
     */
    @Override
    public Page<ShopCpActivity> pageShopCpActivity(ShopCpContext shopCpContext) {
        Operator operator = shopCpContext.getOperator();
        QueryCpAction queryCpAction = (QueryCpAction) shopCpContext.getCpAction();
        Integer pageNum = queryCpAction.getPageNum();
        Integer pageSize = queryCpAction.getPageSize();
        Integer skuId = queryCpAction.getSkuId();
        String skuName = queryCpAction.getSkuName();

        //门店业务处理
        Shop shop = (Shop) operator.getRepresentSubject().getExtend();
        LambdaQueryWrapper<ChangePriceActivityPO> lambdaQueryWrapper = new LambdaQueryWrapper<ChangePriceActivityPO>()
                .eq(ChangePriceActivityPO::getIsDeleted, 0)
                .eq(ChangePriceActivityPO::getType, CpBizType.CP_SHOP.getCode())
                .eq(ChangePriceActivityPO::getShopId, shop.getShopId())
                .eq(skuId != null, ChangePriceActivityPO::getSkuId, skuId)
                .like(skuName != null, ChangePriceActivityPO::getName, skuName)
                .orderByDesc(ChangePriceActivityPO::getGmtModified);
        IPage<ChangePriceActivityPO> changePriceActivityPOPage = this.changePriceActivityMapper.selectPage(new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(pageNum, pageSize)
                , lambdaQueryWrapper
        );
        return Page.convertIPage(pageNum, pageSize, changePriceActivityPOPage, ChangePriceActivityPO::shopCpActivity);
    }

    /**
     * 分页查询门店改价信息
     * <p>
     * check索引name
     *
     * @param shopCpQueryCommand
     * @return
     */
    @Override
    public Page<ShopCpActivity> pageShopCpActivity(ShopCpQueryCommand shopCpQueryCommand) {

        IPage<ChangePriceActivityPO> changePriceActivityPOIPage = changePriceActivityMapper.selectPage(
                new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(shopCpQueryCommand.getPageNum(), shopCpQueryCommand.getPageSize()),
                new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .in(CollectionUtils.isNotEmpty(shopCpQueryCommand.getStatusList()), ChangePriceActivityPO::getStatus, shopCpQueryCommand.getStatusList())
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getType, CpBizType.CP_SHOP.getCode())
                        .like(StringUtils.isNotBlank(shopCpQueryCommand.getActivityNameKey()), ChangePriceActivityPO::getName, shopCpQueryCommand.getActivityNameKey())
                        .eq(shopCpQueryCommand.getSkuId() != null, ChangePriceActivityPO::getSkuId, shopCpQueryCommand.getSkuId())
                        .eq(shopCpQueryCommand.getShopId() != null, ChangePriceActivityPO::getShopId, shopCpQueryCommand.getShopId())
                        .eq(shopCpQueryCommand.getActivityId() != null, ChangePriceActivityPO::getActivityId, shopCpQueryCommand.getActivityId())
                        .orderByDesc(ChangePriceActivityPO::getId)
        );

        Page<ShopCpActivity> shopCpActivityPage = Page.convertIPage(shopCpQueryCommand.getPageNum(), shopCpQueryCommand.getPageSize(),
                changePriceActivityPOIPage, ChangePriceActivityPO::shopCpActivity);
        return shopCpActivityPage;
    }

    @Override
    public ShopCpActivity shopCpActivity(Long activityCpId) {
        ChangePriceActivityPO changePriceActivityPO = changePriceActivityMapper
                .selectById(activityCpId);
        if (changePriceActivityPO == null) {
            return null;
        }
        return ChangePriceActivityPO.shopCpActivity(changePriceActivityPO);
    }

    @Override
    public ShopCpActivity shopCpActivityByActivityId(Long activityId) {
        ChangePriceActivityPO changePriceActivityPO =
                changePriceActivityMapper.selectOne(
                        new LambdaQueryWrapper<ChangePriceActivityPO>()
                                .eq(ChangePriceActivityPO::getIsDeleted, 0)
                                .eq(ChangePriceActivityPO::getActivityId, activityId)
                                .eq(ChangePriceActivityPO::getType, CpBizType.CP_SHOP.getCode())
                );
        return ChangePriceActivityPO.shopCpActivity(changePriceActivityPO);
    }

    /**
     * 查询已经商品
     *
     * @param shopCpChangedQueryCommand 商品已将的商品
     * @return
     */
    @Override
    public Page<ShopCpActivity> pageShopCpActivityChanged(ShopCpChangedQueryCommand shopCpChangedQueryCommand) {

        IPage<ChangePriceActivityPO> changePriceActivityPOIPage = changePriceActivityMapper.selectPage(new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(shopCpChangedQueryCommand.getPageNum(),
                        shopCpChangedQueryCommand.getPageSize()),
                new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getShopId, shopCpChangedQueryCommand.getShopId())
                        .in(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode(), CpStatus.RUNNING.getCode())
                        .eq(ChangePriceActivityPO::getType, CpBizType.CP_SHOP.getCode())
                        .like(StringUtils.isNotBlank(shopCpChangedQueryCommand.getSkuKey()), ChangePriceActivityPO::getName, shopCpChangedQueryCommand.getSkuKey())
                        .orderByDesc(ChangePriceActivityPO::getGmtModified)

        );
        return Page.convertIPage(shopCpChangedQueryCommand.getPageNum(),
                shopCpChangedQueryCommand.getPageSize(),
                changePriceActivityPOIPage,
                ChangePriceActivityPO::shopCpActivity);
    }

    @Override
    public void startByJob() {
        LambdaQueryWrapper<ChangePriceActivityPO> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ChangePriceActivityPO::getIsDeleted, 0);
        lambdaQueryWrapper.eq(ChangePriceActivityPO::getType, CpBizType.CP_SHOP.getCode());
        lambdaQueryWrapper.in(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode());
        lambdaQueryWrapper.le(ChangePriceActivityPO::getPlanActivityStartTime, new Date());

        ChangePriceActivityPO.visit(changePriceActivityMapper, lambdaQueryWrapper, ShopCpStartCommand.create("门店活动自动启动"),
                this::startOneShopCp);

    }

    /**
     * 启动一个活动
     *
     * @param changePriceActivityPO
     * @param shopCpStartCommand
     */
    @Transactional(rollbackFor = Exception.class)
    public void startOneShopCp(ChangePriceActivityPO changePriceActivityPO, ShopCpStartCommand shopCpStartCommand) {

        Integer shopId = changePriceActivityPO.getShopId();
        Integer skuId = changePriceActivityPO.getSkuId();
        List<ChangePriceActivityPO> olds = changePriceActivityMapper.selectList(
                new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getType, CpBizType.CP_SHOP.getCode())
                        .eq(ChangePriceActivityPO::getStatus, CpStatus.RUNNING.getCode())
                        .eq(ChangePriceActivityPO::getShopId, shopId)
                        .eq(ChangePriceActivityPO::getSkuId, skuId)
        );
        if (CollectionUtils.isNotEmpty(olds)) {
            log.info("存在未关闭的活动，开始关闭活动：olds={}", JSON.toJSONString(olds.stream().map(ChangePriceActivityPO::getId).collect(Collectors.toList())));
            for (ChangePriceActivityPO changePriceActivity : olds) {
                closeOneShopCpActivity(changePriceActivity, ShopCpCloseCommand.createShop("启动门店改价，关闭老活动", false));
            }
        }

        SkuPrice skuPrice = skuManager.shopSkuPrice(changePriceActivityPO.getShopId(), changePriceActivityPO.getSkuId());
        if (skuPrice == null) {
            log.error("当前商品价格不存在 shopId = {} skuId={}", changePriceActivityPO.getShopId(), changePriceActivityPO.getSkuId());
            throw new BusinessException("当前门店商品价格异常");
        }

        //更新门店改价状态
        ChangePriceActivityPO update =
                new ChangePriceActivityPO();
        update.setStatus(CpStatus.RUNNING.getCode());
        update.setGmtModified(new Date());
        update.setLastOperatorName(changePriceActivityPO.getLastOperatorName());
        changePriceActivityMapper
                .update(update, new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getId, changePriceActivityPO.getId())
                );


        //计算促销价 to think too many place
        CpActivitySpec spec = CpActivitySpec.spec(changePriceActivityMapper, shopId, skuId);
        spec.setShopCpActivity(ChangePriceActivityPO.shopCpActivity(changePriceActivityPO));
        //计算促销价
        PromotionPriceCommand priceCommand = PromotionPriceCommand
                .create(shopId, skuId, skuPrice.getOriginalPrice(), spec);
        //发送促销价
        PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
        promotionPriceCompute.snapshot(promotionPrice, "门店启动改价");

        //发送开始消息
        log.info("门店改价启动成功：activityId = {} activityCpId={}", changePriceActivityPO.getActivityId(), changePriceActivityPO.getId());
        RocketMQSender.doSend(defaultMQProducer,
                CpEvent.CP_EVENT_TOPIC,
                CpEvent.create(changePriceActivityPO),
                new MqLog.PersistCallback(mqLogMapper)
        );


    }

    public GeneralCpItem get(Integer shopId, Integer skuId) {
        ChangePriceActivityPO changePriceActivityPO =
                changePriceActivityMapper.selectOne(
                        new LambdaQueryWrapper<ChangePriceActivityPO>()
                                .eq(ChangePriceActivityPO::getIsDeleted, 0)
                                .eq(ChangePriceActivityPO::getStatus, CpStatus.RUNNING.getCode())
                                .eq(ChangePriceActivityPO::getAuditStatus, AuditStatus.AUDIT_SUCCESS.getCode())
                                .eq(ChangePriceActivityPO::getShopId, shopId)
                                .eq(ChangePriceActivityPO::getSkuId, skuId)
                                .eq(ChangePriceActivityPO::getType, CpBizType.CP_PLATFORM.getCode())
                );
        if (changePriceActivityPO == null) {
            return null;
        }
        return ChangePriceActivityPO.generalCpItem(changePriceActivityPO);
    }


}
