package com.qmfresh.promotion.platform.domain.model.funds;

import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.FundsApplyDTO;
import com.qmfresh.promotion.bean.promotion.*;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsApply;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsApplyDayStatis;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.CheckFundsApplyBillParamDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.FundsApplyCountDTO;

import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.FundsApplyCountQueryDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.PageQueryFundsApplyCheckParamDTO;

import java.util.List;

/**
 * 营销费用申请管理
 */
public interface IFundsApplyManager {

    //生成营销费申请单
    Boolean  createFundsApplyBill(FundsApplyDTO fundsApply);

    //分页查询申请记录
    PageDTO pageQueryFundsApply(PageQueryFundsApplyParamDTO param);

    //运营后台审核经费申请列表
    PageDTO<FundsApply> pageQueryFundsApplyCheckList(PageQueryFundsApplyCheckParamDTO param);

    PageDTO<FundsApply> fundsAccountList(PageQueryFundsApplyCheckParamDTO param);

    FundsApply queryFundsApplyBill(CheckFundsApplyBillParamDTO param);

    //审核申请单
    Boolean upFundsApplyStatus(FundsApply param);

    //审核统计
    FundsApplyCountDTO queryCheckApplyCount(FundsApplyCountQueryDTO param);

    //是否有在审批中的申请单
    Boolean hasFundsApplayStatus(PageQueryFundsApplyParamDTO param);

    /**
     * 查询门店申请费用日统计
     * @param shopId    门店ID
     * @param shopType  门店类型
     * @param beginTime 开始时间
     * @param endTime   结束时间
     * @return
     */
    List<FundsApplyDayStatis> queryDayStatis(Integer shopId, Integer shopType, Integer beginTime, Integer endTime);
}
