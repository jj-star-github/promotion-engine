package com.qmfresh.promotion.platform.domain.valueobj.funds;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@ToString
public class FundsSettleVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 门店ID
     */
    private Integer shopId;
    /**
     * 门店类型
     */
    private Integer shopType;
    /**
     * 门店名
     */
    private String shopName;
    /**
     * 订单信息
     */
    private String orderCode;
    /**
     * 费用交易金额  正=加  负=减
     */
    private BigDecimal changeAmount;
    /**
     * 顶单实际支付金额  （一个订单产生多个门店更新时 只有门店更新时传值）
     */
    private BigDecimal realPay;
    /**
     * 营销销售额
     */
    private BigDecimal marketingPay;
    /**
     * 总营销费用
     */
    private BigDecimal fundsAmount;
    /**
     * 门店营销费用  只算订单收入
     */
    private BigDecimal shopFundsAmount;

    /**
     * 订单总使用营销费用
     */
    private BigDecimal useAmount;
    /**
     * 门店使用费用
     */
    private BigDecimal shopUseAmount;
    /**
     * 大区新增营销费用
     */
    private BigDecimal areaFundsAmount;
    /**
     * 大区使用营销费用
     */
    private BigDecimal areaUseAmount;
    /**
     * 订单明细
     */
    private List<FundsOrderTransactionVO> orderTransaction;

    /**
     * 订单创建日期 YYYY-MM—DD
     */
    private String createDay;

}
