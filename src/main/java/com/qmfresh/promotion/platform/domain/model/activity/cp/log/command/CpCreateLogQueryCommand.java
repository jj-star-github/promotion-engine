package com.qmfresh.promotion.platform.domain.model.activity.cp.log.command;

import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLogStatus;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class CpCreateLogQueryCommand {

    private Integer pageNum;
    private Integer pageSize;
    private List<CpCreateLogStatus> statusList;


    public static CpCreateLogQueryCommand create(Integer pageNum, Integer pageSize, List<CpCreateLogStatus> statusList) {

        if (pageNum == null) {
            throw new BusinessException("必须设置页码");
        }
        if (pageSize == null) {
            throw new BusinessException("必须设置分页大小");
        }

        CpCreateLogQueryCommand command = CpCreateLogQueryCommand
                .builder()
                .pageNum(pageNum)
                .pageSize(pageSize)
                .statusList(statusList)
                .build();
        return command;
    }
}
