package com.qmfresh.promotion.platform.domain.model.activity;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.context.CpContext;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class Activity implements Serializable {
    /**
     * 营销活动id
     */
    private Long activityId;
    /**
     * 业务类型
     */
    private CpBizType type;
    /**
     * 改价活动状态
     */
    private CpStatus cpStatus;
    /**
     * 营销活动名称
     */
    private String name;
    /**
     * 触发规则
     */
    private String triggerRule;
    /**
     * 限制规则
     */
    private String limitRule;
    /**
     * 结算规则
     */
    private String settleRule;


}
