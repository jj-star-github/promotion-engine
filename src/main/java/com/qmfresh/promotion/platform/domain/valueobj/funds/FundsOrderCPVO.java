package com.qmfresh.promotion.platform.domain.valueobj.funds;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ToString
public class FundsOrderCPVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private BigDecimal originPrice; //原价
    private Integer skuNum; //商品数量
}
