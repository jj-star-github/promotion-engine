package com.qmfresh.promotion.platform.domain.model.activity.cp.general.command;

import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class GeneralCpEditSkuCommand implements Serializable {

    /**
     * 商品id
     */
    private Operator operator;
    /**
     * 商品id
     */
    private Integer skuId;
    /**
     * 目标价格
     */
    private BigDecimal rulePrice;
    private Long activityId;

    public static GeneralCpEditSkuCommand create(Operator operator, Long activityId, Integer skuId, BigDecimal rulePrice) {
        if (skuId == null) {
            throw new BusinessException("skuId不能为空");
        }
        if (rulePrice == null || rulePrice.doubleValue() <= 0) {
            throw new BusinessException("促销价格不能小于等于0");
        }
        if (activityId == null) {
            throw new BusinessException("必须设置活动id");
        }
        GeneralCpEditSkuCommand generalCpEditSkuCommand = new GeneralCpEditSkuCommand();
        generalCpEditSkuCommand.setOperator(operator);
        generalCpEditSkuCommand.setSkuId(skuId);
        generalCpEditSkuCommand.setRulePrice(rulePrice);
        generalCpEditSkuCommand.setActivityId(activityId);
        return generalCpEditSkuCommand;
    }

}
