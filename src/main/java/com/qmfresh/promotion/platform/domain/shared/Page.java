package com.qmfresh.promotion.platform.domain.shared;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Getter;
import org.apache.commons.collections.CollectionUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public class Page<T> implements Serializable {

    /**
     * 页码
     */
    private Integer pageNum;
    /**
     * 页大小
     */
    private Integer pageSize;
    /**
     * 总页数
     */
    private Long totalCount;
    /**
     * 记录列表
     */
    private List<T> records;


    /**
     * 从mybatis-plus的ipage中使用
     *
     * @param page 分页对象
     * @param <T>  范型参数
     * @return 分页对象
     */
    public static <T> Page<T> from(Integer pageNum, Integer pageSize, IPage<T> page) {
        Page<T> pg = new Page<>();
        pg.pageNum = pageNum;
        pg.pageSize = pageSize;
        pg.records = page.getRecords();
        pg.totalCount = page.getTotal();
        return pg;
    }

    /**
     * 一个转换起
     *
     * @param page    分页对象
     * @param convert 转换函数
     * @param <S>     原泛型
     * @param <T>     目标泛型
     * @return 转换后的分页对象
     */
    public static <S, T> Page<T> convert(Page<S> page, Function<S, T> convert) {
        Page<T> pageT = new Page<>();
        pageT.pageNum = page.getPageNum();
        pageT.pageSize = page.getPageSize();
        pageT.totalCount = page.getTotalCount();
        if (CollectionUtils.isEmpty(page.getRecords())) {
            pageT.records = Collections.emptyList();
        } else {
            pageT.records = page.records.stream().map(convert).collect(Collectors.toList());
        }
        return pageT;
    }

    public static <S, T> Page<T> convertIPage(Integer pageNum, Integer pageSize, IPage<S> page, Function<S, T> convert) {
        return convert(from(pageNum, pageSize, page), convert);
    }


}
