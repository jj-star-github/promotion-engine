package com.qmfresh.promotion.platform.domain.model.activity.cp.area;

import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.Sku;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpItem;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class AreaCpItem implements Serializable {

    private Long activityId;
    private Long activityCpId;
    private String activityName;
    private CpBizType cpBizType;
    private Shop shop;
    private Sku sku;
    private BigDecimal originalPrice;
    private BigDecimal currentPrice;
    private BigDecimal rulePrice;
    private BigDecimal marketingDiffFee;
    private CpTimeCycle cpTimeCycle;
    private CpType cpType;
    private String lastOperatorName;


    public static   GeneralCpItem convert(AreaCpItem areaCpItem) {
        if (areaCpItem == null) {
            return null;
        }

        GeneralCpItem generalCpItem = GeneralCpItem.builder()
                .activityId(areaCpItem.getActivityId())
                .activityCpId(areaCpItem.getActivityCpId())
                .activityName(areaCpItem.getActivityName())
                .cpBizType(areaCpItem.getCpBizType())
                .shop(areaCpItem.getShop())
                .sku(areaCpItem.getSku())
                .originalPrice(areaCpItem.getOriginalPrice())
                .currentPrice(areaCpItem.getCurrentPrice())
                .rulePrice(areaCpItem.getRulePrice())
                .marketingDiffFee(areaCpItem.getMarketingDiffFee())
                .cpTimeCycle(areaCpItem.getCpTimeCycle())
                .cpType(areaCpItem.getCpType())
                .lastOperatorName(areaCpItem.getLastOperatorName())
                .build();
        return generalCpItem;
    }
}
