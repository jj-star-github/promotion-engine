package com.qmfresh.promotion.platform.domain.model.activity.cp.log;

import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Getter;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public enum CpCreateLogCompletedStatus {

    /**
     * 待执行
     */
    TO_RUNNING(1),
    /**
     * 已过期
     */
    EXPIRED(2),
    /**
     * 已完成
     */
    COMPLETED(3),
    /**
     * 已终止
     */
    TERMINATED(4);

    private Integer code;

    CpCreateLogCompletedStatus(Integer code) {
        this.code = code;
    }


    public CpCreateLogCompletedStatus from(Integer status) {

        for (CpCreateLogCompletedStatus i : CpCreateLogCompletedStatus.values()) {
            if (i.getCode().equals(status)) {
                return i;
            }
        }
        throw new BusinessException("不支持完成类型");
    }}
