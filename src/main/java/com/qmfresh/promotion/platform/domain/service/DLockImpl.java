package com.qmfresh.promotion.platform.domain.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class DLockImpl implements DLock {


    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Boolean tryLock(String key, Long timeout) {
        Boolean result = stringRedisTemplate.opsForValue().setIfAbsent(key, Thread.currentThread().getId() + "", timeout, TimeUnit.MILLISECONDS);
        return result == null ? false : result;
    }

    /**
     * 存在可能会错误删除key，维护好key生成
     *
     * @param key
     * @return
     */
    @Override
    public Boolean unlock(String key) {
        return stringRedisTemplate.delete(key);
    }


}
