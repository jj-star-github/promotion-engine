package com.qmfresh.promotion.platform.domain.model.funds;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsSettleVO;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsSettleInfo;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsShopDayAmountSum;

import java.util.Date;
import java.util.List;

/**
 * 费用账户
 */
public interface FundsSettleInfoManager extends IService<FundsSettleInfo> {

    /**
     * 新增锁定费用流水
     * @param vo     锁定费用信息
     * @param accountId   账户信息
     */
    void insertChange(FundsSettleVO vo, Long accountId);

    /**
     * 查询门店日统计
     * @param accountId
     * @param dayTime
     * @return
     */
    FundsShopDayAmountSum queryDayAmount(Long accountId,Integer shopId, Integer shopType, String dayTime);

    /**
     *
     * @param shopId
     * @param shopType
     * @param endTime
     * @return
     */
    List<FundsShopDayAmountSum> queryDatStatis(Integer shopId, Integer shopType, Date endTime);

    /**
     * 查询账户 订单收入明细
     * @param accountId
     * @param createDay
     * @param start
     * @param length
     * @return
     */
    List<FundsSettleInfo> queryDayNote(Long accountId, String createDay, Integer start, Integer length);

    /**
     * 订单收入明细 总数
     * @param accountId
     * @param createDay
     * @return
     */
    Integer queryDayCount(Long accountId, String createDay);

    /**
     * 查询统计信息
     * @param createDay
     * @return
     */
    FundsSettleInfo queryDaySum(String createDay);

    /**
     * 查询日指标数据
     * @param beginTime
     * @param endTime
     * @return
     */
    List<FundsSettleInfo> queryDayTarget(String beginTime, String endTime, Integer shopType, Integer shopId, Long account, Integer start, Integer length);

    /**
     * 查询指标总数
     * @param beginTime
     * @param endTime
     * @param shopType
     * @param shopId
     * @param account
     * @return
     */
    Integer queryDayTargetCount(String beginTime, String endTime, Integer shopType, Integer shopId, Long account);

    /**
     * 查询账户明细
     * @param createDay
     * @param start
     * @param length
     * @return
     */
    List<FundsSettleInfo> queryAccountStatis(String createDay, Integer start, Integer length);

    /**
     * 查询账户明细总数
     * @param createDay
     * @return
     */
    Integer queryAccountStatisCount(String createDay);

    /**
     * 根据门店列表统计
     * @param createDay
     * @param start
     * @param length
     * @return
     */
    List<FundsSettleInfo> queryAccountByShopList(String createDay, Integer start, Integer length, List<Integer> shopIdList, Integer shopType);

    /**
     * 根据门店列表 总数
     * @param createDay
     * @return
     */
    Integer queryCountByShopList(String createDay, List<Integer> shopIdList, Integer shopType);

    /**
     * 批量查询日指标数据
     * @param beginTime
     * @param endTime
     * @return
     */
    List<FundsSettleInfo> batchQueryDayTarget(String beginTime, String endTime, Integer shopType, Integer shopId, Long account, Integer start, Integer length);

    /**
     * 批量查询指标总数
     * @param beginTime
     * @param endTime
     * @param shopType
     * @param shopId
     * @param account
     * @return
     */
    Integer batchQueryDayTargetCount(String beginTime, String endTime, Integer shopType, Integer shopId, Long account);

}
