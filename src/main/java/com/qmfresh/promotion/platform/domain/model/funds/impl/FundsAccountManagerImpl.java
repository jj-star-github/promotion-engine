package com.qmfresh.promotion.platform.domain.model.funds.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.AreaConditionQuery;
import com.qmfresh.promotion.entity.BaseArea;
import com.qmfresh.promotion.entity.ShopDetail;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.ShopManager;
import com.qmfresh.promotion.platform.domain.model.funds.FundsChangeNoteManager;
import com.qmfresh.promotion.platform.domain.model.funds.FundsLockChangeManager;
import com.qmfresh.promotion.platform.domain.model.funds.FundsAccountManager;
import com.qmfresh.promotion.platform.domain.model.funds.FundsSettleInfoManager;
import com.qmfresh.promotion.platform.domain.model.funds.enums.FundsShopTypeEnum;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsLockVO;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsSettleVO;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsTransactionVO;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.FundsAccountMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsAccount;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsAccountSum;
import com.qmfresh.promotion.platform.interfaces.platform.facade.account.AccountCreateDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 */
@Service
@Slf4j
public class FundsAccountManagerImpl extends ServiceImpl<FundsAccountMapper, FundsAccount>
        implements FundsAccountManager {

    @Resource
    private FundsLockChangeManager fundsLockChangeManager;
    @Resource
    private FundsChangeNoteManager changeNoteManager;
    @Resource
    private FundsSettleInfoManager settleInfoManager;
    @Resource
    private IQmExternalApiService iQmExternalApiService;
    @Resource
    private ShopManager shopManager;

    @Override
    public FundsAccount byShopIdAndType(Integer shopId, Integer shopType) {
        FundsAccount account = baseMapper.byShopIdAndType(shopId, shopType);
        if (null == account) {
            account = new FundsAccount();
            account.setLockAmount(new BigDecimal("0"));
            account.setTotalAmount(new BigDecimal("0"));
        }
        return account;
    }

    @Override
    public List<FundsAccount> queryNotEnoughAccount() {
        return baseMapper.queryNotEnoughAccount();
    }

    @Override
    public FundsAccount byId(Long id) {
        return baseMapper.selectById(id);
    }

    private FundsAccount updateAccount(FundsAccount fundsAccount) {
        FundsAccount account = baseMapper.byShopIdAndType(fundsAccount.getShopId(), fundsAccount.getShopType());
        log.info("账户查询，dto={}", JSON.toJSONString(account));
        //账号为空新创建
        if (account == null) {
            account = new FundsAccount();
            account.setShopId(fundsAccount.getShopId());
            account.setShopName(fundsAccount.getShopName());
            account.setShopAccount(fundsAccount.getShopName());
            account.setStatus(1);
            account.setShopType(fundsAccount.getShopType());
            account.setTotalAmount(new BigDecimal(0));
            account.setLockAmount(new BigDecimal(0));
            account.setGmtCreate(new Date());
            account.setGmtModified(new Date());
            boolean insert = super.save(account);
            if (!insert) {
                log.error("账户新增失败 param={}", JSON.toJSONString(fundsAccount));
                throw new BusinessException("账户更新失败");
            }
        }
        //验证费用是否等于0
        if ((null == fundsAccount.getTotalAmount() || fundsAccount.getTotalAmount().compareTo(BigDecimal.ZERO) == 0)
                && (null == fundsAccount.getLockAmount() || fundsAccount.getLockAmount().compareTo(BigDecimal.ZERO) == 0)) {
            log.info("账户更新-费用不可为0，dto={}", JSON.toJSONString(fundsAccount));
            return account;
        }

        //验证账户状态
        if(account.getStatus() != 1){
            log.error("账户更新-账户状态异常，dto={}", JSON.toJSONString(fundsAccount));
            throw new BusinessException("账户更新失败-账户状态异常");
        }
        FundsAccount update = new FundsAccount();
        update.setId(account.getId());
        update.setTotalAmount(fundsAccount.getTotalAmount());
        update.setLockAmount(fundsAccount.getLockAmount());
        update.setShopId(fundsAccount.getShopId());
        update.setGmtModified(new Date());
        baseMapper.updateAccount(update);
        account = baseMapper.selectById(account.getId());
        //验证锁定费用是否大于剩余费用 如果大于抛出异常
        if (null != update.getLockAmount() && fundsAccount.getLockAmount().compareTo(BigDecimal.ZERO) < 0) {
            if (account.getLockAmount().abs().compareTo(account.getTotalAmount()) > 0) {
                log.info("锁定费用失败，shopId={} totalAmount={} lockAmount={}", fundsAccount.getShopId(), account.getTotalAmount(), account.getLockAmount());
                throw new BusinessException("账户费用不足");
            }
        }
        return account;
    }

    @Override
    public FundsAccountSum queryAccountSum(Integer shopType) {
        return baseMapper.getSumBy(shopType);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean lockAmount(FundsLockVO lockVo) {
        log.info("营销费用锁定，param={}", JSON.toJSONString(lockVo));
        //验证费用是否等于0
        if (lockVo.getChangeAmount().compareTo(new BigDecimal("0")) == 0) {
            log.info("锁定-费用不可为0，dto={}", JSON.toJSONString(lockVo));
            return true;
        }
        if (lockVo.getAccountType().compareTo(FundsShopTypeEnum.ZB.getCode()) == 0 && null == lockVo.getShopId()) {
            lockVo.setShopId(1);
        }
        //验证账户信息是否存在
        FundsAccount account = baseMapper.byShopIdAndType(lockVo.getShopId(), lockVo.getAccountType());
        if (null == account) {
            log.info("锁定-账号不存在 {}", JSON.toJSONString(lockVo));
            return false;
        }
        //账户更新
        FundsAccount fundsAccount = new FundsAccount();
        fundsAccount.setShopId(lockVo.getShopId());
        fundsAccount.setShopType(lockVo.getAccountType());
        fundsAccount.setShopName(lockVo.getShopName());
        fundsAccount.setLockAmount(lockVo.getChangeAmount());
        account = updateAccount(fundsAccount);
        //流水
        fundsLockChangeManager.insertChange(lockVo, account.getId());
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateAccount(FundsTransactionVO transactionVo) {
        log.info("营销费用账户更新，param={}", JSON.toJSONString(transactionVo));
        //验证费用是否不等于0
        if (transactionVo.getChangeAmount().compareTo(new BigDecimal("0")) == 0) {
            log.info("账户更新-费用不可为0，dto={}", JSON.toJSONString(transactionVo));
            return true;
        }
        //跟新账户信息
        FundsAccount fundsAccount = new FundsAccount();
        fundsAccount.setShopId(transactionVo.getShopId());
        fundsAccount.setShopType(transactionVo.getShopType());
        fundsAccount.setShopName(transactionVo.getShopName());
        fundsAccount.setTotalAmount(transactionVo.getChangeAmount());
        FundsAccount account = updateAccount(fundsAccount);
        //流水
        changeNoteManager.insertChange(transactionVo, account);
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateWeekAccount(FundsTransactionVO transactionVo) {
        log.info("营销费用账户更新，param={}", JSON.toJSONString(transactionVo));
        //跟新账户信息
        FundsAccount fundsAccount = new FundsAccount();
        fundsAccount.setShopId(transactionVo.getShopId());
        fundsAccount.setShopType(transactionVo.getShopType());
        fundsAccount.setShopName(transactionVo.getShopName());
        fundsAccount.setTotalAmount(transactionVo.getChangeAmount());
        FundsAccount account = updateAccount(fundsAccount);
        //流水
        changeNoteManager.insertChange(transactionVo, account);
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateAccountBySettle(FundsSettleVO settleVO) {
        log.info("营销费用账户更新-订单业务，param={}", JSON.toJSONString(settleVO));

        //更新账户
        FundsAccount fundsAccount = new FundsAccount();
        fundsAccount.setShopId(settleVO.getShopId());
        fundsAccount.setShopType(settleVO.getShopType());
        fundsAccount.setShopName(settleVO.getShopName());
        fundsAccount.setTotalAmount(settleVO.getChangeAmount());
        fundsAccount = updateAccount(fundsAccount);
        //新增流水
        changeNoteManager.insertChange(settleVO.getOrderTransaction(), fundsAccount, settleVO.getChangeAmount(), settleVO.getCreateDay());
        //订单信息同步
        settleInfoManager.insertChange(settleVO, fundsAccount.getId());
        return true;
    }

    @Override
    public List<FundsAccount> queryAccountList(List<Integer> shopTypes, String shopName, Integer shopId, String areaName, Integer start, Integer length) {
        return baseMapper.queryAccountList(shopTypes, shopName, shopId, areaName, start, length);
    }

    @Override
    public Integer queryAccountListCount(List<Integer> shopTypes, String shopName, Integer shopId, String areaName) {
        return baseMapper.queryAccountListCount(shopTypes, shopName, shopId, areaName);
    }

    @Override
    public Boolean createAccount(AccountCreateDTO accountCreateDTO){
        log.info("创建营销账户，param={}", JSON.toJSONString(accountCreateDTO));
        FundsAccount account = baseMapper.byShopIdAndType(accountCreateDTO.getQueryShopId(), accountCreateDTO.getShopType());
        if(account != null){
            log.warn("创建营销账户 - 该门店已有账户，shopId={}", accountCreateDTO.getQueryShopId());
            throw new BusinessException("账户创建失败，已有账户！");
        }else {
            account = new FundsAccount();
            account.setShopId(accountCreateDTO.getQueryShopId());
            account.setShopName(accountCreateDTO.getShopName());
            account.setShopType(accountCreateDTO.getShopType());
            if(accountCreateDTO.getShopType().equals(4)){
                Shop shop = shopManager.shop(accountCreateDTO.getQueryShopId());
                account.setShopAccount(shop.getShopName());
            }else if(accountCreateDTO.getShopType().equals(2)){
                account.setShopAccount(accountCreateDTO.getAccountName());
            }
            account.setStatus(accountCreateDTO.getStatus());
            account.setRemark(accountCreateDTO.getRemark());
            account.setMinPromPrice(accountCreateDTO.getMinPromPrice());
            account.setMaxPromPrice(accountCreateDTO.getMaxPromPrice());
            account.setOperatorId(accountCreateDTO.getOperatorId());
            account.setOperatorName(accountCreateDTO.getOperatorName());
            account.setTotalAmount(new BigDecimal(0));
            account.setLockAmount(new BigDecimal(0));
            account.setGmtCreate(new Date());
            account.setGmtModified(new Date());
            if(accountCreateDTO.getShopType().equals(4)){
                List shopList = new ArrayList();
                shopList.add(accountCreateDTO.getQueryShopId());
                AreaConditionQuery param = new AreaConditionQuery();
                param.setShopIds(shopList);
                //门店所属大区
                List<BaseArea> areaDetail = iQmExternalApiService.selectAreaByShopIds(param);
                if(CollectionUtils.isEmpty(areaDetail)){
                    log.warn("创建营销账户 - 该门店没有所属大区，shopId={}", accountCreateDTO.getQueryShopId());
                    throw new BusinessException("创建营销账户失败，该门店没有所属大区！");
                }
                account.setAreaId(areaDetail.get(0).getId());
            }else if(accountCreateDTO.getShopType().equals(1)){
                account.setAreaId(0);
            }else if(accountCreateDTO.getShopType().equals(2)){
                account.setAreaId(1);
            }
            boolean insert = super.save(account);
            if (!insert) {
                log.error("账户新增失败 param={}", JSON.toJSONString(accountCreateDTO));
                throw new BusinessException("账户更新失败");
            }
            return true;
        }
    }

    @Override
    public Boolean modifyAccount(AccountCreateDTO accountCreateDTO){
        log.info("编辑营销账户，param={}", JSON.toJSONString(accountCreateDTO));
        FundsAccount update = new FundsAccount();
        update.setId(accountCreateDTO.getId());
        update.setShopName(accountCreateDTO.getShopName());
        update.setMinPromPrice(accountCreateDTO.getMinPromPrice());
        update.setMaxPromPrice(accountCreateDTO.getMaxPromPrice());
        update.setRemark(accountCreateDTO.getRemark());
        update.setStatus(accountCreateDTO.getStatus());
        update.setOperatorId(accountCreateDTO.getOperatorId());
        update.setOperatorName(accountCreateDTO.getOperatorName());
        update.setGmtModified(new Date());
        if(accountCreateDTO.getShopType().equals(4)){
            List shopList = new ArrayList();
            shopList.add(accountCreateDTO.getQueryShopId());
            AreaConditionQuery param = new AreaConditionQuery();
            param.setShopIds(shopList);
            //门店所属大区
            List<BaseArea> areaDetail = iQmExternalApiService.selectAreaByShopIds(param);
            if(CollectionUtils.isEmpty(areaDetail)){
                log.warn("编辑营销账户 - 该门店没有所属大区，shopId={}", accountCreateDTO.getQueryShopId());
                throw new BusinessException("编辑营销账户失败，该门店没有所属大区！");
            }
            update.setAreaId(areaDetail.get(0).getId());
        }else if(accountCreateDTO.getShopType().equals(1)){
            update.setAreaId(0);
        }else if(accountCreateDTO.getShopType().equals(2)){
            update.setAreaId(1);
        }
        boolean up = super.updateById(update);
        if(!up){
            log.error("账户编辑失败 param={}", JSON.toJSONString(accountCreateDTO));
            throw new BusinessException("账户编辑失败");
        }
        return true;
    }
}
