package com.qmfresh.promotion.platform.domain.model.activity.cp;

import lombok.Getter;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public enum LifeCycle {

    /**
     * createCloseEvent
     */
    CREATE(1),
    /**
     * start
     */
    START(2),
    /**
     * finish
     */
    FINISH(3);

    private int code;

    LifeCycle(Integer code) {
        this.code = code;
    }
}
