package com.qmfresh.promotion.platform.domain.model.activity.cp.command;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Builder
@Getter
public class ShopQueryCommand implements Serializable {
    /**
     * 页码
     */
    private Integer pageNum;
    /**
     * 页大小
     */
    private Integer pageSize;
    /**
     * 活动id
     */
    private Long activityId;
    /**
     * 改价类型
     */
    private CpBizType cpBizType;

    public static ShopQueryCommand create(Integer pageNum,
                                          Integer pageSize,
                                          Long activityId,
                                          CpBizType cpBizType) {

        if (pageNum == null || pageSize == null) {
            throw new BusinessException("必须设置分页信息");
        }

        if (activityId == null) {
            throw new BusinessException("必须设置活动id");
        }

        ShopQueryCommand shopQueryCommand = ShopQueryCommand.builder()
                .pageNum(pageNum)
                .pageSize(pageSize)
                .activityId(activityId)
                .cpBizType(cpBizType)
                .build();

        return shopQueryCommand;

    }
}
