package com.qmfresh.promotion.platform.domain.model.activity.cp.general.command;

import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.SkuCpItem;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;

/**
 * 在命令中加规则
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public class GeneralCpCreateCommand implements Serializable {

    private Operator operator;
    private String activityName;
    private CpTimeCycle timeCycle;
    private List<Integer> shopIds;
    private List<SkuCpItem> skuCpItems;
    private Long activityId;


    public GeneralCpCreateCommand(Operator operator,
                                  String activityName,
                                  CpTimeCycle timeCycle,
                                  List<Integer> shopIds,
                                  List<SkuCpItem> items) {
        this.operator = operator;
        this.activityName = activityName;
        this.timeCycle = timeCycle;
        this.shopIds = shopIds;
        this.skuCpItems = items;
    }

    public GeneralCpCreateCommand(Operator operator,
                                  String activityName,
                                  CpTimeCycle timeCycle,
                                  List<Integer> shopIds,
                                  List<SkuCpItem> items,Long activityId) {
        this.operator = operator;
        this.activityName = activityName;
        this.timeCycle = timeCycle;
        this.shopIds = shopIds;
        this.skuCpItems = items;
        this.activityId = activityId;
    }

    public static void check(String request) {


    }
}
