package com.qmfresh.promotion.platform.domain.model.activity.cp.command;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.SkuCpItem;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CpCreateCommand implements Serializable {

    private Operator operator;
    private String activityName;
    private CpTimeCycle timeCycle;
    private List<Integer> shopIds;
    private List<SkuCpItem> skuCpItems;
    private Long marketingAccountId;
    private Long activityId;
    private CpBizType cpBizType;


    public static CpCreateCommand create(Operator operator,
                                         String activityName,
                                         CpTimeCycle cpTimeCycle,
                                         List<Integer> shopIds,
                                         List<SkuCpItem> skuCpItems,
                                         Long marketingAccountId,
                                         CpBizType cpBizType) {

        if (operator == null) {
            throw new BusinessException("必须设置操作者");
        }
        if (StringUtils.isBlank(activityName)) {
            throw new BusinessException("必须设置活动名称");
        }
        if (cpTimeCycle == null) {
            throw new BusinessException("必须设置改价周期");
        }
        if (CollectionUtils.isEmpty(shopIds)) {
            throw new BusinessException("必须设置门店列表");
        }
        if (CollectionUtils.isEmpty(skuCpItems)) {
            throw new BusinessException("必须设置改价列表");
        }
        if (cpBizType == null) {
            throw new BusinessException("必须设置改价业务类型");
        }

        if (CpBizType.CP_AREA.equals(cpBizType) && marketingAccountId == null) {
            throw new BusinessException("大区改机，必须设置大区账号");
        }

        return CpCreateCommand.builder()
                .operator(operator)
                .activityName(activityName)
                .timeCycle(cpTimeCycle)
                .shopIds(shopIds)
                .skuCpItems(skuCpItems)
                .marketingAccountId(marketingAccountId)
                .cpBizType(cpBizType)
                .build();
    }

    public static CpCreateCommand create(String json) {
        return JSON.parseObject(json, new TypeReference<CpCreateCommand>() {
        });
    }

}
