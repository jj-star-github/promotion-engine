package com.qmfresh.promotion.platform.domain.model.promactivity.enums;


import lombok.Getter;

/**
 * 目标用户类型
 */
@Getter
public enum UserTypeEnum {

    /**
     * 全部
     */
    ALL(1),
    /**
     * 会员
     */
    VIP(2),
    /**
     * 非会员
     */
    NO(3),
    ;
    private Integer code;

    UserTypeEnum(Integer code) {
        this.code = code;
    }
}
