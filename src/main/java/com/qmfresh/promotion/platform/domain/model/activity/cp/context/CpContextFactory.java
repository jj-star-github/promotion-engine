package com.qmfresh.promotion.platform.domain.model.activity.cp.context;

import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.action.CpAction;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface CpContextFactory {
    /**
     * 创建门店改价
     *
     * @return 改价上下文
     */
    CpContext createShopCp(Operator operator, CpAction cpAction);

    /**
     * 创建平台改价上文
     *
     * @param operator 操作者
     * @param cpAction 改价动作
     * @return 改价上下文
     */
    CpContext createPlatformCp(Operator operator, CpAction cpAction);

    /**
     * 创建会员改价上下文
     *
     * @param operator 操作者
     * @param cpAction 改价动作
     * @return 改价上下文
     */
    CpContext createVipCp(Operator operator, CpAction cpAction);
}
