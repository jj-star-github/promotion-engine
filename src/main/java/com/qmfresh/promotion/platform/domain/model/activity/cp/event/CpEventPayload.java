package com.qmfresh.promotion.platform.domain.model.activity.cp.event;

import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ChangePriceActivityPO;
import lombok.Getter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public class CpEventPayload implements Serializable {


    /**
     * 营销活动id
     */
    private Long activityId;
    /**
     * 改价活动id
     */
    private Long activityCpId;

    private Integer shopId;
    private Integer skuId;
    /**
     * 规则价格
     */
    private BigDecimal rulePrice;
    private BigDecimal currentPrice;
    private BigDecimal marketingDiffFee;
    private Integer type;
    private Integer cpType;
    private BigDecimal planMarketingAmount;
    private String remark;
    /**
     * 活动创建人
     */
    private String activityCreator;
    /**
     * @see com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus
     */
    private Integer status;


    /**
     * 创建改价事件内容
     *
     * @param activity 活动信息
     * @return cpEventPayload
     */
    public static CpEventPayload createStartEvent(ShopCpActivity activity, BigDecimal currentPrice, BigDecimal marketingDiffFee) {
        CpEventPayload cpEventPayload = baseCpEventPayload(activity);
        cpEventPayload.currentPrice = currentPrice;
        cpEventPayload.marketingDiffFee = marketingDiffFee;
        return cpEventPayload;
    }

    private static CpEventPayload baseCpEventPayload(ShopCpActivity activity) {
        CpEventPayload cpEventPayload = new CpEventPayload();
        cpEventPayload.activityId = activity.getActivityId();
        cpEventPayload.activityCpId = activity.getActivityCpId();
        cpEventPayload.shopId = activity.getShop().getShopId();
        cpEventPayload.skuId = activity.getSku().getSkuId();
        cpEventPayload.rulePrice = activity.getRulePrice();
        cpEventPayload.type = activity.getBizType().getCode();
        cpEventPayload.cpType = activity.getCpType().getCode();
        cpEventPayload.planMarketingAmount = activity.getPlanMarketingFee();
        cpEventPayload.activityCreator = activity.getCreatePersonName();
        return cpEventPayload;
    }

    public static CpEventPayload createCloseEvent(ShopCpActivity activity, String remark) {
        CpEventPayload cpEventPayload = baseCpEventPayload(activity);
        cpEventPayload.remark = remark;
        return cpEventPayload;
    }

    public static CpEventPayload createPlanEvent(ShopCpActivity activity) {
        CpEventPayload cpEventPayload = baseCpEventPayload(activity);
        return cpEventPayload;
    }

    /**
     * 改价活动对象
     *
     * @param changePriceActivityPO 改价活动对象
     * @return CpEventPayload
     */
    public static CpEventPayload create(ChangePriceActivityPO changePriceActivityPO) {
        CpEventPayload cpEventPayload = new CpEventPayload();
        cpEventPayload.skuId = changePriceActivityPO.getSkuId();
        cpEventPayload.shopId = changePriceActivityPO.getShopId();
        cpEventPayload.remark = changePriceActivityPO.getRemark();
        cpEventPayload.planMarketingAmount = changePriceActivityPO.getPlanMarketingAmount();
        cpEventPayload.cpType = changePriceActivityPO.getCpType();
        cpEventPayload.type = changePriceActivityPO.getType();

        cpEventPayload.activityId = changePriceActivityPO.getActivityId();
        cpEventPayload.activityCpId = changePriceActivityPO.getId();
        cpEventPayload.activityCreator = changePriceActivityPO.getCreatePersonName();

        cpEventPayload.currentPrice = changePriceActivityPO.getCurrentPrice();
        cpEventPayload.rulePrice = changePriceActivityPO.getRulePrice();
        cpEventPayload.marketingDiffFee = changePriceActivityPO.getMarketingDiffPrice();
        cpEventPayload.status = changePriceActivityPO.getStatus();
        return cpEventPayload;
    }
}
