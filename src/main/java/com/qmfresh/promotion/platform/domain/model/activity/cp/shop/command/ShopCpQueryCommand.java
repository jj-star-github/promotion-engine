package com.qmfresh.promotion.platform.domain.model.activity.cp.shop.command;

import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class ShopCpQueryCommand implements Serializable {


    private Integer pageNum;
    private Integer pageSize;

    private Integer skuId;
    private Integer shopId;
    private Integer type;
    private Date fromBeginTime;
    private Date toBeginTime;
    private List<Integer> statusList;
    private Long activityId;
    private String skuKey;
    private String activityNameKey;


    public static ShopCpQueryCommand createPlatformQuery(
            Integer pageNum,
            Integer pageSize,
            Integer skuId,
            Integer shopId,
            Integer type,
            List<Integer> statusList,
            Long activityId,
            String activityNameKey,
            Date fromBeginTime,
            Date fromEndTime
    ) {

        if (pageNum <= 0 || pageSize <= 0) {
            throw new BusinessException("必须设置分页数据");
        }
        ShopCpQueryCommand shopCpQueryCommand = ShopCpQueryCommand.builder()
                .pageNum(pageNum)
                .pageSize(pageSize)
                .skuId(skuId)
                .shopId(shopId)
                .type(type)
                .statusList(statusList)
                .activityId(activityId)
                .activityNameKey(activityNameKey)
                .fromBeginTime(fromBeginTime)
                .toBeginTime(fromEndTime)
                .build();

        return shopCpQueryCommand;
    }
}
