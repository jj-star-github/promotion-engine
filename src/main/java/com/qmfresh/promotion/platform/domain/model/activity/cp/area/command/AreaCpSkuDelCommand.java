package com.qmfresh.promotion.platform.domain.model.activity.cp.area.command;

import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import io.swagger.models.auth.In;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class AreaCpSkuDelCommand implements Serializable {

    /**
     * 改价明细id
     */
    private Integer skuId;
    private Long activityId;
    /**
     * 操作人
     */
    private Operator operator;

    private String remark;


    public static AreaCpSkuDelCommand create(Operator operator,
                                             Long activityId,
                                             Integer skuId,
                                             String remark) {
        if (operator == null) {
            throw new BusinessException("操作者不能为空");
        }
        if (activityId == null) {
            throw new BusinessException("活动id不能为空");
        }
        if (skuId == null) {
            throw new BusinessException("商品id不能为空");
        }

        AreaCpSkuDelCommand skuDelCommand =
                AreaCpSkuDelCommand.builder()
                        .operator(operator)
                        .activityId(activityId)
                        .skuId(skuId)
                        .remark(remark)
                        .build();
        return skuDelCommand;
    }
}
