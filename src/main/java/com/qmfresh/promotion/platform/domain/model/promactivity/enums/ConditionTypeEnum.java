package com.qmfresh.promotion.platform.domain.model.promactivity.enums;


import lombok.Getter;

/**
 * 奖品类型:1.实物,2.优惠券,3.番茄币
 */
@Getter
public enum ConditionTypeEnum {

    /**
     * 商品
     */
    SKU(1),
    /**
     * 优惠券
     */
    COUPON(2),
    /**
     * 番茄币
     */
    CLASS2(3),
    ;
    private Integer code;

    ConditionTypeEnum(Integer code) {
        this.code = code;
    }
}
