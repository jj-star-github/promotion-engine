package com.qmfresh.promotion.platform.domain.model.activity.cp.area.command;

import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Builder;
import lombok.Getter;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class AreaCpSkuQueryCommand {

    private Integer pageNum;
    private Integer pageSize;
    private Long activityId;
    private Integer skuId;
    private String class3NameKey;


    public static AreaCpSkuQueryCommand create(
            Integer pageNum,
            Integer pageSize,
            Long activityId,
            Integer skuId,
            String class3NameKey
    ) {

        if (pageNum == null || pageSize == null) {
            throw new BusinessException("必须设置分页信息");
        }
        if (activityId == null) {
            throw new BusinessException("必须设置活动");
        }
        AreaCpSkuQueryCommand skuQueryCommand =
                AreaCpSkuQueryCommand.builder()
                        .pageNum(pageNum)
                        .pageSize(pageSize)
                        .activityId(activityId)
                        .skuId(skuId)
                        .class3NameKey(class3NameKey)
                        .build();
        return skuQueryCommand;

    }
}
