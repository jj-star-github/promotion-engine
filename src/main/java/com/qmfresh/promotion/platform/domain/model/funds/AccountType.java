package com.qmfresh.promotion.platform.domain.model.funds;

import lombok.Getter;

/**
 * 账户类型 1：总部 2：大区 3：片区 4：门店
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public enum AccountType {

    /**
     * 总部
     */
    PLATFORM(1),
    /**
     * 大区
     */
    BIG_AREA(2),
    /**
     * 片区
     */
    AREA(3),
    /**
     * 门店
     */
    SHOP(4);
    private Integer code;

    AccountType(Integer code) {
        this.code = code;
    }
}
