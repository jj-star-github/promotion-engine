package com.qmfresh.promotion.platform.domain.model.activity.cp.event;

import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPrice;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@Slf4j
public class CpComputeEvent implements Serializable {

    public static final String CP_EVENT_COMPUTE_TOPIC = "marketing_cp_compute_event";
    /**
     * 事件发送时间
     */
    private Long timestamp;
    private String remark;

    /**
     * 事件内容
     */
    private Object payload;

    public static CpComputeEvent createEvent(PromotionPrice promotionPrice, String remark) {
        CpComputeEvent cpComputeEvent = new CpComputeEvent();
        cpComputeEvent.setTimestamp(System.currentTimeMillis());
        cpComputeEvent.setRemark(remark);
        cpComputeEvent.setPayload(promotionPrice);
        return cpComputeEvent;

    }
}
