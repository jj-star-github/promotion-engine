package com.qmfresh.promotion.platform.domain.shared;

import lombok.Getter;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public enum BizCode {

    //token失效
    TOKEN_EXPIRE(4011),
    //参数异常
    PARAM_INVALIDATE(4201),
    //验证码失效
    VERIFY_CODE_EXPIRE(4202),
    //session解析失败
    SESSION_PARSE_ERROR(4203),
    //服务异常
    SERVER_ERROR(5010),
    //db持久化异常
    DB_PERSIST_ERROR(5002),
    //缓存服务异常
    CACHE_ERROR(5003),
    //消息异常
    MQ_ERROR(5004),
    //网络异常
    NETWORK_ERROR(5005),
    //业务异常
    BIZ_ERROR(5100),
    //微信解密异常
    WECHAT_DECRYPT_ERROR(5101),
    //用户已注册
    USER_REGISTED_ERROR(5102),
    //消息发送失败
    MESSAGE_SEND_ERROR(5103),
    //微信接口请求失败
    WECHAT_INTERFACE_ERROR(5104),
    //正在创建用户
    USER_IS_CREATING(5105),
    //passport is creating
    PASSPORT_IS_CREATING(5106),
    //手机号码解密失败
    WECHAT_PHONE_DECREPT_ERROR(5107),
    //错误
    ERROR(9999);
    private Integer code;

    BizCode(Integer code) {
        this.code = code;
    }


}
