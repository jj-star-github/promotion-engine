package com.qmfresh.promotion.platform.domain.model.activity.cp.shop;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.Sku;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.context.CpContext;
import com.qmfresh.promotion.platform.domain.model.activity.cp.context.ShopCpContext;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@Slf4j
@Builder
public class ShopCpActivity implements Serializable {

    public static final String CLOSE_REMARK_START_NEW_SHOP_CP = "启动新门店改价";

    /**
     * 活动id
     */
    private Long activityId;
    /**
     * 改价活动id
     */
    private Long activityCpId;
    /**
     * 活动名称
     */
    private String name;

    private CpBizType bizType;
    private CpStatus cpStatus;
    /**
     * 门店信息
     */
    private Shop shop;
    /**
     * 商品信息
     */
    private Sku sku;

    private BigDecimal planMarketingFee;
    private BigDecimal rulePrice;
    private BigDecimal currentPrice;
    private BigDecimal originalPrice;
    /**
     * 改价类型
     */
    private CpType cpType;
    private BigDecimal marketingDiffFee;

    private Long createPersonId;
    private String createPersonName;
    private String lastOperatorPerson;

    private Date planActivityStartTime;
    private Date planActivityEndTime;
    private Date createTime;


    /**
     * 创建门店改价
     * 核心是门店创建的约束规则
     *
     * @param cpContext 改价上下文
     * @return 门店改价活动
     */
    public static ShopCpActivity create(CpContext cpContext) {
        ShopCpContext shopCpContext = (ShopCpContext) cpContext;
        ShopCpActivityManager shopCpActivityManager = shopCpContext.getShopCpActivityManager();
        ShopCpActivity shopCpActivity = shopCpActivityManager.create(cpContext);
        return shopCpActivity;
    }

    /**
     * 关闭门店改价活动
     *
     * @param shopCpContext 门店改价上下文
     */
    public void close(ShopCpContext shopCpContext) {
        ShopCpActivityManager shopCpActivityManager = shopCpContext.getShopCpActivityManager();
        shopCpActivityManager.close(shopCpContext);

    }

    /**
     * 启动门店改价
     *
     * @param shopCpContext
     */
    public void start(ShopCpContext shopCpContext) {
        log.info("开始启动门店改价活动：{}", JSON.toJSONString(this));
        ShopCpActivityManager shopCpActivityManager = shopCpContext.getShopCpActivityManager();
        shopCpActivityManager.start(shopCpContext, this);
    }
}
