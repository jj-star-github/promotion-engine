package com.qmfresh.promotion.platform.domain.model.activity.cp.general.command;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class GeneralCpQueryCommand implements Serializable {

    private Long activityId;
    private Integer pageNum;
    private Integer pageSize;
    private Integer skuId;
    private Integer shopId;
    private Date planBeginTimeStart;
    private Date planBeginTimeEnd;
    private Integer shopType;
    private String activityNameKey;
    private List<CpStatus> cpStatusList;
}
