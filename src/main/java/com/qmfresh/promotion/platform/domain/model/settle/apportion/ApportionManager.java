package com.qmfresh.promotion.platform.domain.model.settle.apportion;

import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopPromotionRule;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.dto.CashOrderDTO;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.dto.WeekClearDTO;
import com.qmfresh.promotion.platform.interfaces.shop.facade.funds.ShopWeekClearDTO;

import java.util.List;

/**
 * @Author zbr
 * @Date 9:35 2020/8/24
 */
public interface ApportionManager {
    /**
     * 订单分摊转换
     */
    void apportion(CashOrderDTO orderDTO);

    void baseApportion(CashOrderDTO orderDTO, ShopPromotionRule shopPromotionRule);

    void areaApportion(CashOrderDTO orderDTO, ShopPromotionRule shopPromotionRule);

    ShopPromotionRule queryRuleType(Integer shopLevel);

    void dayWmsReceive();

    void weekClear();

    void updateOrder(List<String> list);

    void shopWeekClear(WeekClearDTO weekClearDTO);
}
