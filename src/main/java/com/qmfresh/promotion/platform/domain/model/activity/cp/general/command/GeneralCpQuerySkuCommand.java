package com.qmfresh.promotion.platform.domain.model.activity.cp.general.command;

import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class GeneralCpQuerySkuCommand implements Serializable {

    private Long activityId;
    private Integer pageNum;
    private Integer pageSize;
    private String class3Name;
    private Integer skuId;

    /**
     * @param pageNum
     * @param pageSize
     * @param activityId
     * @param class3Name
     * @param skuId
     * @return
     */
    public static GeneralCpQuerySkuCommand create(Integer pageNum, Integer pageSize, Long activityId, String class3Name, Integer skuId) {
        if (pageNum == null || pageSize == null) {
            throw new BusinessException("必须设置分页信息");
        }
        if (activityId == null) {
            throw new BusinessException("必须设置活动id");
        }
        GeneralCpQuerySkuCommand generalCpQuerySkuCommand
                = new GeneralCpQuerySkuCommand();
        generalCpQuerySkuCommand.setActivityId(activityId);
        generalCpQuerySkuCommand.setPageNum(pageNum);
        generalCpQuerySkuCommand.setPageSize(pageSize);
        generalCpQuerySkuCommand.setClass3Name(class3Name);
        generalCpQuerySkuCommand.setSkuId(skuId);
        return generalCpQuerySkuCommand;
    }
}
