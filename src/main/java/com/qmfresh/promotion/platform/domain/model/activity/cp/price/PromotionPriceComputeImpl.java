package com.qmfresh.promotion.platform.domain.model.activity.cp.price;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.AreaCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.event.CpComputeEvent;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.command.PromotionPriceCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import com.qmfresh.promotion.platform.domain.model.support.MqLog;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.ChangePriceActivityMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ChangePriceActivityPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.support.MqLogMapper;
import com.qmfresh.promotion.platform.infrastructure.message.RocketMQSender;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class PromotionPriceComputeImpl implements PromotionPriceCompute {

    @Resource
    private DefaultMQProducer defaultMQProducer;
    @Resource
    private MqLogMapper mapper;
    @Resource
    private ChangePriceActivityMapper changePriceActivityMapper;


    /**
     * 计算当前促销价
     * <p>
     * 营销费用分摊计算：
     * <p>
     * 总部活动最低时，不分摊门店的营销费用
     * 门店活动最低时，可以分摊总部活动
     *
     * @return 返回促销价
     */
    public PromotionPrice compute(PromotionPriceCommand promotionPriceCommand) {

        ShopCpActivity shopCpActivity = promotionPriceCommand.getShopCpActivity();
        AreaCpItem areaCpItem = promotionPriceCommand.getAreaCpItem();
        GeneralCpItem generalCpItem = promotionPriceCommand.getGeneralCpItem();

        BigDecimal originalPrice = promotionPriceCommand.getOriginalPrice();
        Integer skuId = promotionPriceCommand.getSkuId();
        Integer shopId = promotionPriceCommand.getShopId();

        if (areaCpItem != null && generalCpItem == null) {
            generalCpItem = AreaCpItem.convert(areaCpItem);
        }

        if (areaCpItem != null) {
            if (areaCpItem.getRulePrice().doubleValue() < generalCpItem.getRulePrice().doubleValue()) {
                generalCpItem = AreaCpItem.convert(areaCpItem);
            }
        }

        if (shopCpActivity == null && generalCpItem == null) {
            return PromotionPrice.create(originalPrice, shopId, skuId, PromotionPriceSpec.createEmpty());
        }

        BigDecimal promotionPrice;
        BigDecimal marketingDiffFee;

        //存在门店改价
        if (shopCpActivity != null && generalCpItem == null) {
            BigDecimal rulePrice = shopCpActivity.getRulePrice();
            marketingDiffFee = rulePrice.subtract(originalPrice);
            promotionPrice = rulePrice;
            log.info("仅存在门店改价，促销价等于促销终价：shopId={} skuId={} rulePrice={} marketingDiffFee={}", shopId, skuId, rulePrice, marketingDiffFee);
            return getPromotionPrice(shopCpActivity, originalPrice, promotionPrice, marketingDiffFee, shopCpActivity.getShop().getShopId(), shopCpActivity.getSku().getSkuId());
        }

        //只存在总部改价
        if (shopCpActivity == null) {
            BigDecimal rulePrice = generalCpItem.getRulePrice();
            marketingDiffFee = rulePrice.subtract(originalPrice);
            promotionPrice = rulePrice;
            log.info("仅存在总部改机，促销价:param={}", JSON.toJSONString(promotionPriceCommand));
            return PromotionPrice.create(promotionPrice, generalCpItem.getShop().getShopId(), generalCpItem.getSku().getSkuId(), PromotionPriceSpec.create(
                    PromotionPriceRule.create(generalCpItem, originalPrice, marketingDiffFee),
                    null,
                    null,
                    null
            ));
        }
        //同时存在门店改价和总部
        BigDecimal generalRulePrice = generalCpItem.getRulePrice();
        BigDecimal shopRulePrice = shopCpActivity.getRulePrice();

        //原价大于总部活动(锚定原价或者总部的最小)
        if (originalPrice.doubleValue() >= generalRulePrice.doubleValue()) {
            if (generalRulePrice.doubleValue() <= shopRulePrice.doubleValue()) {
                marketingDiffFee = generalRulePrice.subtract(originalPrice);
                promotionPrice = generalRulePrice;
                log.info("总部门店同时存在，原价大于总部；总部最小:param={}", JSON.toJSONString(promotionPriceCommand));
                return PromotionPrice.create(promotionPrice, generalCpItem.getShop().getShopId(), generalCpItem.getSku().getSkuId(), PromotionPriceSpec.create(
                        PromotionPriceRule.create(generalCpItem, originalPrice, marketingDiffFee),
                        null,
                        null,
                        null
                ));

            } else {
                BigDecimal generalMarketingDiffFee = generalRulePrice.subtract(originalPrice);
                BigDecimal shopMarketingDiffFee = shopRulePrice.subtract(generalRulePrice);
                promotionPrice = shopRulePrice;
                log.info("总部门店同时存在，原价大于总部；门店最小:param={}", JSON.toJSONString(promotionPriceCommand));
                return create(shopCpActivity, generalCpItem, originalPrice, generalCpItem.getSku().getSkuId()
                        , generalCpItem.getShop().getShopId(), promotionPrice, shopMarketingDiffFee, generalMarketingDiffFee);
            }
        } else {
            //原价小于总部活动，门店促销只能原价，不能赚总部价；总部价不赚营销费用
            if (generalRulePrice.doubleValue() <= shopRulePrice.doubleValue()) {
                marketingDiffFee = generalRulePrice.subtract(originalPrice);
                promotionPrice = generalRulePrice;
                log.info("总部门店同时存在，原价小于总部；总部最小:param={}", JSON.toJSONString(promotionPriceCommand));

                return PromotionPrice.create(promotionPrice, shopId, skuId, PromotionPriceSpec.create(
                        PromotionPriceRule.create(generalCpItem, originalPrice, marketingDiffFee),
                        null,
                        null,
                        null
                ));
            } else {

                BigDecimal generalMarketingDiffFee = BigDecimal.ZERO;
                //门店只能赚原价
                BigDecimal shopMarketingDiffFee = shopRulePrice.subtract(originalPrice);
                promotionPrice = shopRulePrice;
                log.info("总部门店同时存在，原价小于总部；门店最小：param={}", JSON.toJSONString(promotionPriceCommand));
                return create(shopCpActivity, generalCpItem, originalPrice,
                        skuId, shopId, promotionPrice, shopMarketingDiffFee, generalMarketingDiffFee);
            }
        }
    }

    private PromotionPrice getPromotionPrice(ShopCpActivity shopCpActivity, BigDecimal originalPrice, BigDecimal promotionPrice, BigDecimal marketingDiffFee, Integer shopId2, Integer skuId2) {
        return PromotionPrice.create(promotionPrice, shopId2, skuId2, PromotionPriceSpec.create(
                null,
                null,
                PromotionPriceRule.create(shopCpActivity, originalPrice, marketingDiffFee),
                null
        ));
    }

    private static PromotionPrice create(ShopCpActivity shopCpActivity, GeneralCpItem generalCpItem,
                                         BigDecimal originalPrice, Integer skuId, Integer shopId,
                                         BigDecimal promotionPrice,
                                         BigDecimal shopMarketingDiffFee,
                                         BigDecimal generalMarketingDiffFee) {
        return PromotionPrice.create(
                promotionPrice,
                shopId, skuId,
                PromotionPriceSpec
                        .create(PromotionPriceRule.create(generalCpItem, originalPrice, generalMarketingDiffFee),
                                null,
                                PromotionPriceRule.create(shopCpActivity, originalPrice, shopMarketingDiffFee),
                                null
                        )
        );
    }

    @Override
    public PromotionPrice compute(BigDecimal originPrice, Integer shopId, Integer skuId, boolean snapshot, String remark) {
        if (shopId == null) {
            throw new BusinessException("必须设置门店id");
        }
        if (skuId == null) {
            throw new BusinessException("必须设置skuId");
        }


        PromotionPrice promotionPrice = compute(PromotionPriceCommand.createCurrent(originPrice, shopId, skuId,
                null, changePriceActivityMapper));
        if (snapshot) {
            snapshot(promotionPrice, remark);
        }

        return promotionPrice;
    }


    /**
     * 将当前的改价固化在当前改价活动中
     *
     * @param promotionPrice 改价信息
     */
    @Transactional(rollbackFor = Exception.class)
    public void snapshot(PromotionPrice promotionPrice, String remark) {
        PromotionPriceSpec spec = promotionPrice.getSpec();
        PromotionPriceRule shopPromotion = spec.getShopPromotion();
        PromotionPriceRule generalPromotion = spec.getGeneralPromotion();
        //开始持久化改价信息
        if (shopPromotion != null) {
            Long cpId = shopPromotion.getActivityCpId();
            BigDecimal marketingDiffFee = shopPromotion.getMarketingDiffFee();
            BigDecimal original = shopPromotion.getOriginalPrice();
            updateCpInfo(cpId, marketingDiffFee, original);
        }

        if (generalPromotion != null) {
            updateCpInfo(generalPromotion.getActivityCpId(), generalPromotion.getMarketingDiffFee(), generalPromotion.getOriginalPrice());

        }
        RocketMQSender.doSend(defaultMQProducer, CpComputeEvent.CP_EVENT_COMPUTE_TOPIC, CpComputeEvent.createEvent(promotionPrice, remark),
                new MqLog.PersistCallback(mapper)
        );

    }

    private void updateCpInfo(Long cpId, BigDecimal marketingDiffFee, BigDecimal original) {
        ChangePriceActivityPO update = new ChangePriceActivityPO();
        update.setMarketingDiffPrice(marketingDiffFee);
        update.setOriginalPrice(original);
        update.setGmtModified(new Date());
        changePriceActivityMapper.update(update, new LambdaQueryWrapper<ChangePriceActivityPO>()
                .eq(ChangePriceActivityPO::getId, cpId)
        );
    }

}
