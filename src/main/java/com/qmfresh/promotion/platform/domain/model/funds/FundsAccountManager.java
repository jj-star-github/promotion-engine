package com.qmfresh.promotion.platform.domain.model.funds;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsLockVO;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsSettleVO;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsTransactionVO;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsAccount;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsAccountSum;
import com.qmfresh.promotion.platform.interfaces.platform.facade.account.AccountCreateDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 费用账户
 */
public interface FundsAccountManager extends IService<FundsAccount> {

    /**
     * 查询账户信息
     *  账户不存在则返回：0   用于其他业务使用
     * @param shopId   门店ID
     * @param shopType 门店类型
     * @return
     */
    FundsAccount byShopIdAndType(Integer shopId, Integer shopType);

    /**
     * 查询所有欠费的账户
     * @return
     */
    List<FundsAccount> queryNotEnoughAccount();

    /**
     * 根据ID查询账户信息
     * @param id
     * @return
     */
    FundsAccount byId(Long id);

    /**
     * 根据门店类型统计 可用费用总额和锁定费用总额
     * @param shopType
     * @return
     */
    FundsAccountSum queryAccountSum(Integer shopType);

    /**
     * 费用锁定  调用该方法 需要捕获异常，抛异常时锁定失败
     * @param lockVO
     * @return
     */
    boolean lockAmount(FundsLockVO lockVO);

    /**
     * 普通费用交易 调用该方法 需要捕获异常，抛异常时锁定失败
     * @param transactionVo
     * @return
     */
    boolean updateAccount(FundsTransactionVO transactionVo);

    @Transactional(rollbackFor = Exception.class)
    boolean updateWeekAccount(FundsTransactionVO transactionVo);

    /**
     * 订单处理账户变更 （单独处理）
     * @param settleVO
     * @return
     */
    boolean updateAccountBySettle(FundsSettleVO settleVO);

    /**
     * 根据业务类型查询账户
     * @param shopTypes
     * @param shopName
     * @param shopId
     * @param areaName
     * @param start
     * @param length
     * @return
     */
    List<FundsAccount> queryAccountList(List<Integer> shopTypes, String shopName,Integer shopId,String areaName, Integer start, Integer length);

    /**
     * 根据业务类型查询账户数量
     * @param shopTypes
     * @param shopName
     * @param shopId
     * @param areaName
     * @return
     */
    Integer queryAccountListCount(List<Integer> shopTypes, String shopName,Integer shopId,String areaName);

    /**
     * 创建营销账户
     * @param accountCreateDTO
     * @return
     */
    Boolean createAccount(AccountCreateDTO accountCreateDTO);

    /**
     * 编辑营销账户
     * @param accountCreateDTO
     * @return
     */
    Boolean modifyAccount(AccountCreateDTO accountCreateDTO);
}
