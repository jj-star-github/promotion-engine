package com.qmfresh.promotion.platform.domain.model.promactivity.enums;


import lombok.Getter;

/**
 * 品类型
 */
@Getter
public enum StatusEnum {

    /**
     * 待开始
     */
    CREATE(1),
    /**
     * 进行中
     */
    RUNNING(3),
    /**
     * 已失效
     */
    CANCELED(4),
    /**
     * 已关闭
     */
    CLOSED(7),
    /**
     * 已结束(expired)
     */
    FINISHED(8);

    private Integer code;

    StatusEnum(Integer code) {
        this.code = code;
    }
}
