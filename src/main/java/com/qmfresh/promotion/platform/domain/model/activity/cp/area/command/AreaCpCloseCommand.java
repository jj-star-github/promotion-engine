package com.qmfresh.promotion.platform.domain.model.activity.cp.area.command;

import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class AreaCpCloseCommand implements Serializable {

    private Operator operator;
    private Long activityId;
    private String remark;

    public static AreaCpCloseCommand create(Operator operator, Long activityId, String remark) {

        AreaCpCloseCommand areaCpCloseCommand =
                AreaCpCloseCommand.builder()
                        .operator(operator)
                        .activityId(activityId)
                        .remark(remark)
                        .build();
        return areaCpCloseCommand;
    }
}
