package com.qmfresh.promotion.platform.domain.model.activity.cp.area;

import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class AreaCpActivity implements Serializable {

    /**
     * 总部改价id
     */
    private Long activityId;
    /**
     * 活动名称
     */
    private String activityName;
    /**
     * 活动区域id
     */
    private Integer areaId;
    private CpBizType cpBizType;
    private Integer shopCount;
    private Integer skuCount;
    private CpTimeCycle cpTimeCycle;
    private CpStatus cpStatus;
    private BigDecimal sumPlanMarketingFee;
    private BigDecimal usedPlanMarketingFee;
    private Date createTime;
    private String lastModifiedPerson;
}
