package com.qmfresh.promotion.platform.domain.model.activity.cp.general.command;

import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class GeneralCpCloseCommand implements Serializable {

    private Operator operator;

    private Long activityId;
    private String remark;

    /**
     * 人工关闭命令
     *
     * @param operator   操作者
     * @param activityId 活动id
     * @param remark     remark
     * @return
     */
    public static GeneralCpCloseCommand create(Operator operator, Long activityId, String remark) {
        GeneralCpCloseCommand generalCpCloseCommand = GeneralCpCloseCommand.builder()
                .operator(operator)
                .activityId(activityId)
                .remark(remark)
                .build();
        return generalCpCloseCommand;
    }

    /**
     * job关闭活动命令
     *
     * @param operator 操作者
     * @param remark   备注
     * @return 关闭命令
     */
    public static GeneralCpCloseCommand createForJob(Operator operator, String remark) {
        GeneralCpCloseCommand generalCpCloseCommand =
                GeneralCpCloseCommand.builder()
                        .operator(operator)
                        .remark(remark)
                        .build();
        return generalCpCloseCommand;
    }
}
