package com.qmfresh.promotion.platform.domain.model.funds.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsLockVO;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsLockChange;
import com.qmfresh.promotion.platform.domain.model.funds.FundsLockChangeManager;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.FundsLockChangeMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 */
@Service
@Slf4j
public class FundsLockChangeManagerImpl extends ServiceImpl<FundsLockChangeMapper, FundsLockChange>
        implements FundsLockChangeManager {

    @Override
    public void insertChange(FundsLockVO lockVo, Long accountId) {
        FundsLockChange change = new FundsLockChange();
        BeanUtils.copyProperties(lockVo, change);
        change.setAccountId(accountId);
        change.setBusinessId(Long.valueOf(lockVo.getBusinessId()));
        change.setShopType(lockVo.getAccountType());
        change.setChangeAmount(lockVo.getChangeAmount().compareTo(new BigDecimal("0")) > 0 ? lockVo.getChangeAmount() : lockVo.getChangeAmount().negate());
        change.setChangeType(lockVo.getChangeAmount().compareTo(new BigDecimal("0")) > 0 ? 1 : -1);
        change.setCreateDay(DateUtil.formatDate());
        change.setGmtCreate(new Date());
        change.setGmtModified(new Date());
        baseMapper.insert(change);
    }

    @Override
    public List<FundsLockChange> queryDaySumSupportShopType(String createDay) {
        return baseMapper.queryDaySumSupportShopType(createDay);
    }

    @Override
    public List<FundsLockChange> queryDaySumSupportAccount(Long accountId, List<String> createDayList) {
        return baseMapper.queryDaySumSupportAccount(accountId, createDayList);
    }

    @Override
    public FundsLockChange queryDaySumSupportAccount(Long accountId, String createDay) {
        List<String> createDayList = new ArrayList<>();
        createDayList.add(createDay);
        List<FundsLockChange> lockChanges = baseMapper.queryDaySumSupportAccount(accountId, createDayList);
        return CollectionUtils.isEmpty(lockChanges) ? null : lockChanges.get(0);
    }

    @Override
    public List<FundsLockChange> queryDaySumSupportAccount(List<Long> accountId, String beginTime, String endTime) {
        return baseMapper.queryDaySumSupportAccounts(accountId, beginTime, endTime);
    }
}
