package com.qmfresh.promotion.platform.domain.model.settle.apportion;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.ShopWeekClearMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopWeekClear;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopWeekSum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @Author zbr
 * @Date 19:03 2020/8/27
 */
@Service
@Slf4j
public class ShopWeekClearManagerImpl extends ServiceImpl<ShopWeekClearMapper,ShopWeekClear> implements ShopWeekClearManager{

    @Override
    public ShopWeekSum querySumpriceAmount(Integer shopId, String createDayStart, String createDayEnd) {
        return baseMapper.querySumpriceAmount(shopId,createDayStart,createDayEnd);
    }
}
