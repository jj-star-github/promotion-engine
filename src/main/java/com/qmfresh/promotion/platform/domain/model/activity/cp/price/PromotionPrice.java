package com.qmfresh.promotion.platform.domain.model.activity.cp.price;

import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

/**
 * 促销价技术
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class PromotionPrice {
    private Integer shopId;
    private Integer skuId;
    /**
     * 最终促销价
     */
    private BigDecimal lastPromotionPrice;
    /**
     * 最终促销价规范
     */
    private PromotionPriceSpec spec;
    /**
     * 促销价发生时间
     */
    private Long timestamp;

    private String activityCreator;

    public static PromotionPrice create(BigDecimal lastPromotionPrice,
                                        Integer shopId,
                                        Integer skuId,
                                        PromotionPriceSpec spec) {
        if (shopId == null) {
            throw new BusinessException("必须设置门店id");
        }
        if (skuId == null) {
            throw new BusinessException("必须设置skuId");
        }
        PromotionPrice promotionPrice = PromotionPrice.builder()
                .lastPromotionPrice(lastPromotionPrice)
                .shopId(shopId)
                .skuId(skuId)
                .spec(spec)
                .timestamp(System.currentTimeMillis())
                .build();
        return promotionPrice;

    }
}
