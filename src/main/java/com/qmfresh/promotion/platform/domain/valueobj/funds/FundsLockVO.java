package com.qmfresh.promotion.platform.domain.valueobj.funds;


import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.funds.AccountType;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 营销费用锁定流水信息
 */
@Data
@ToString
public class FundsLockVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 业务ID （活动ID）
     */
    private String businessId;
    /**
     * 门店传递门店ID  总部传入：1
     */
    private Integer shopId;
    /**
     * 账户类型 1：总部 2：大区 3：片区 4：门店
     */
    private Integer accountType;
    /**
     * 门店名称
     */
    private String shopName;
    /**
     * 锁定费用 正=加锁定费用  负=减锁定费用
     */
    private BigDecimal changeAmount;


    /**
     * 门店锁营销费用对象
     *
     * @param activityId   活动id
     * @param shop         门店id
     * @param changeAmount 改变的营销费用
     * @return 锁费用对象
     */
    public static FundsLockVO lockShop(Long activityId, Shop shop, BigDecimal changeAmount) {
        return create(activityId, shop, changeAmount);
    }

    /**
     * 门店释放营销费用的对象
     *
     * @param activityId   活动id
     * @param shop         门店id
     * @param changeAmount 改变的营销费用
     * @return 锁费用对象
     */
    public static FundsLockVO unlockShop(Long activityId, Shop shop, BigDecimal changeAmount) {
        return create(activityId, shop, changeAmount.negate());
    }

    public static FundsLockVO create(Long activityId, Shop shop, BigDecimal changeAmount) {
        FundsLockVO fundsLockVO = new FundsLockVO();
        fundsLockVO.setBusinessId(activityId + "");
        fundsLockVO.setShopId(shop.getShopId());
        fundsLockVO.setAccountType(AccountType.SHOP.getCode());
        fundsLockVO.setShopName(shop.getShopName());
        fundsLockVO.setChangeAmount(changeAmount);
        return fundsLockVO;
    }
}
