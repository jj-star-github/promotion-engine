package com.qmfresh.promotion.platform.domain.model.activity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.infrastructure.http.Https;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class SkuManagerImpl implements SkuManager {

    @Value("${url.service.cmfresh}")
    private String productServiceUrl;
    @Value("${url.service.cmfresh}")
    private String shopServiceUrl;
    @Value("${url.service.shopes}")
    private String shopEsServiceUrl;

    /**
     * 商品类目，商品价格，商品名称
     * <p>
     * {"success":true,"errorCode":0,"message":"","body":[{"id":10007,"class3Id":3282,"skuCode":"10007","skuName":"长白萝卜","status":1,"spuId":4525,"skuFormat":"斤","skuDesc":"","pics":"20180507144453_53006.jpg","packagePics":"","isWeight":1,"internationalCode":"","isShelfLife":0,"isFixedConversionRate":0,"physicalCount":0,"physicalUnit":0,"valuationUnit":0,"skuLevel":0,"spuCount":1000,"temperature":1,"expireDays":3,"length":0,"width":0,"height":0,"volume":0,"netWeight":0,"grossWeight":0,"tareWeight":0,"isCertificate":1,"isPrecious":0,"isFragile":0,"physicalMeasureUnit":24,"valuationMeasureUnit":24,"isBindPackage":0,"skuFrontName":"长白萝卜","brandId":0,"biId":42515,"bannerPic":"","contentPic":"","priceCount":1.00,"class1Id":55,"class2Id":266,"class1Name":"蔬菜","class2Name":"根茎","class3Name":"根茎","deliveryGroupId":1,"displayOrder":null,"firstPy":"cblb","unitFormat":"斤","skuId":null,"skuType":1,"ct":1525675495,"ut":1584446085}]}
     *
     * @param skuId skuId
     * @return 商品
     */
    @Override
    public Sku sku(Integer skuId) {
        String result = Https.post(productServiceUrl + "/shop/ex/sku/getSku", null, JSON.toJSONString(Arrays.asList(skuId)));
        List<Sku> skus = getSkuList(result);
        if (skus == null) return null;

        if (CollectionUtils.isEmpty(skus)) {
            return null;
        }
        return skus.get(0);
    }

    private List<Sku> getSkuList(String result) {
        JSONObject jsonObject = JSONObject.parseObject(result);
        Boolean success = jsonObject.getBoolean("success");
        if (success == null || !success) {
            log.warn("商品商品不存在：{}", jsonObject.getString("message"));
            return null;
        }
        JSONArray jsonArray = jsonObject.getJSONArray("body");
        List<Sku> skus = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jo = jsonArray.getJSONObject(i);
            skus.add(getSku(jo));
        }
        return skus;
    }

    private Sku getSku(JSONObject jo) {
        Sku sku = new Sku();
        Integer id = jo.getInteger("id");
        Integer class1Id = jo.getInteger("class1Id");
        String class1Name = jo.getString("class1Name");
        Integer class2Id = jo.getInteger("class2Id");
        String class2Name = jo.getString("class2Name");
        Integer class3Id = jo.getInteger("class3Id");
        String class3Name = jo.getString("class3Name");
        String skuFormat = jo.getString("skuFormat");
        String skuName = jo.getString("skuName");
        Integer skuType = jo.getInteger("skuType");
        String skuImage = jo.getString("pics");
        String unitFormat = jo.getString("unitFormat");
        sku.setSkuId(id);
        sku.setClass1Id(class1Id);
        sku.setClass1Name(class1Name);
        sku.setClass2Id(class2Id);
        sku.setClass2Name(class2Name);
        sku.setClass3Id(class3Id);
        sku.setClass3Name(class3Name);
        sku.setSkuFormat(skuFormat);
        sku.setSkuName(skuName);
        sku.setSkuType(skuType);
        sku.setSkuImage(skuImage);
        sku.setUnitFormat(unitFormat);
        return sku;
    }

    /**
     * 获取当前售价
     * <p>
     * {"success":true,"errorCode":0,"message":"","body":[{"id":407,"ssuId":408,"cityId":0,"areaId":0,"price":1290,"vipPrice":0.00,"skuId":10007,"userId":0,"isDeleted":0,"isOnline":0,"isThirdOnline":0,"isLock":0,"lockPrice":0.00,"priceApplyId":0,"isStore":0,"shopId":5,"isCommon":0,"formatId":6643,"updater":null,"updateDepartment":null,"advisePrice":1290,"lastPrice":null,"reasonType":null,"optTime":0,"channel":2,"productSsu":null,"sku":null,"pluCode":null,"salesUnit":null,"internationalCode":null,"userName":null,"realName":null,"shopName":null,"oldOptTime":null,"ct":1527067824,"ut":1596175734}]}
     *
     * @param shopId 门店id
     * @param skuId  商品id
     * @return 商品的当前售价
     */
    @Override
    public SkuPrice shopSkuPrice(Integer shopId, Integer skuId) {

        Map<String, Object> param = new HashMap<>();
        param.put("shopId", shopId);
        param.put("skuId", skuId);

        log.info("param:" + JSON.toJSONString(param));
        String result = Https.post(productServiceUrl + "/shop/ex/ssu/getProductSsuPriceListByCondition", null, JSON.toJSONString(param));
        log.info("param:" + JSON.toJSONString(result));
        JSONObject jsonObject = JSON.parseObject(result);
        boolean success = jsonObject.getBoolean("success");
        if (!success) {
            log.warn("门店商品当前售价为空：shopId={} skuId ={}", shopId, skuId);
            return null;
        }
        JSONArray jsonArray = jsonObject.getJSONArray("body");
        if(jsonArray.size() == 0){
            log.warn("门店商品当前售价为空：shopId={} skuId ={}", shopId, skuId);
            return null;
        }
        JSONObject jo = jsonArray.getJSONObject(0);
        return getSkuPrice(jo, shopId);
    }

    private SkuPrice getSkuPrice(JSONObject jo, Integer shopId) {

        Long price = jo.getLong("price");
        BigDecimal vipPrice = jo.getBigDecimal("vipPrice");
        Long originalPrice = jo.getLong("advisePrice");
        Integer skuId = jo.getInteger("skuId");
        if (price == null) {
            log.warn("门店商品当前售价没有设置：shopId={} skuId={}", shopId, skuId);
            return null;
        }

        log.info("获取当前门店价格：shopId= {} skuId = {} price ={}", shopId, skuId, price);
        try {
            BigDecimal originalPrice1 = BigDecimal.ZERO;
            if (originalPrice != null) {
                originalPrice1 = new BigDecimal((originalPrice / 1000.0) + "");
            }
            SkuPrice skuPrice = SkuPrice.create(shopId, skuId,
                    originalPrice1,
                    new BigDecimal((price / 1000.0) + ""),
                    vipPrice
            );
            log.info("当前门店商品的价格：{}", skuPrice);
            return skuPrice;
        } catch (Exception e) {
            return null;
        }

    }

    /**
     * 总
     *
     * @param shopId 门店id
     * @param skuIds 门店商品ids
     * @return
     */
    @Override
    public List<SkuPrice> shopSkuPrice(Integer shopId, List<Integer> skuIds) {
        String result = Https
                .post(productServiceUrl + "/shop/ex/ssu/getProductSsuPriceListByCondition", null, JSON.toJSONString(new SkuParam(shopId, skuIds)));
        JSONObject jsonObject = JSON.parseObject(result);
        Boolean success = jsonObject.getBoolean("success");
        if (success == null || !success) {
            log.warn("商品接口调用报错：shopId ={} skuIds ={}", shopId, JSON.toJSONString(skuIds));
            return new ArrayList<>();
        }
        JSONArray jsonArray = jsonObject.getJSONArray("body");
        List<SkuPrice> skus = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jo = jsonArray.getJSONObject(i);
            SkuPrice skuPrice = getSkuPrice(jo, shopId);
            if (skuPrice == null) {
                continue;
            }
            skus.add(skuPrice);
        }
        return skus;
    }

    /**
     * {"success":true,"errorCode":0,"message":"","body":[{"id":1023370,"ssuId":1025048,"cityId":0,"areaId":0,"price":0,"vipPrice":0.00,"skuId":10007,"userId":0,"isDeleted":0,"isOnline":0,"isThirdOnline":0,"isLock":0,"lockPrice":0.00,"priceApplyId":0,"isStore":0,"shopId":10,"isCommon":0,"formatId":6643,"updater":null,"updateDepartment":null,"advisePrice":0,"lastPrice":null,"reasonType":null,"optTime":0,"channel":2,"productSsu":null,"sku":null,"pluCode":null,"salesUnit":null,"internationalCode":null,"userName":null,"realName":null,"shopName":null,"oldOptTime":null,"ct":1595591865,"ut":1595591865},{"id":998780,"ssuId":997452,"cityId":0,"areaId":0,"price":3680,"vipPrice":0.00,"skuId":10010,"userId":-1,"isDeleted":0,"isOnline":1,"isThirdOnline":1,"isLock":0,"lockPrice":1.99,"priceApplyId":0,"isStore":0,"shopId":10,"isCommon":0,"formatId":6519,"updater":null,"updateDepartment":null,"advisePrice":3680,"lastPrice":null,"reasonType":null,"optTime":0,"channel":2,"productSsu":null,"sku":null,"pluCode":null,"salesUnit":null,"internationalCode":null,"userName":null,"realName":null,"shopName":null,"oldOptTime":null,"ct":1594039407,"ut":1594039407}]}
     *
     * @param skuIds
     * @return
     */
    @Override
    public List<Sku> sku(List<Integer> skuIds) {
        String result = Https.post(productServiceUrl + "/shop/ex/sku/getSku", null, JSON.toJSONString(skuIds));
        return getSkuList(result);
    }


    /**
     * {"success":true,"errorCode":0,"message":"","body":[{"id":"55451","priceId":55451,"shopId":24,"shopName":null,"skuId":10007,"skuName":"长白萝卜","skuNamePy":"ZHANGBAI ZHANG BAILUOBU BAI LUOBU LUO BU ","skuFormat":"斤","unitFormat":"斤","isWeight":1,"class1Id":55,"class2Id":266,"class1Name":"蔬菜","class2Name":"根茎","isOnline":1,"isThirdOnline":1,"isLock":1,"inventory":27.22,"yesterdaySales":0,"lastPurchasePrice":1.68,"lastReceivePrice":1.87,"originPrice":2.58,"sellPrice":2.58,"vipPrice":0.00,"actList":null,"status":1,"skuCount":null,"saleAmt":null,"cSaleNum":8.26,"pics":"20180507144453_53006.jpg","isVip":null},{"id":"56889","priceId":56889,"shopId":24,"shopName":null,"skuId":10010,"skuName":"西葫芦 ","skuNamePy":"XIHULU XI HULU HU LU ","skuFormat":"斤","unitFormat":"斤","isWeight":1,"class1Id":55,"class2Id":267,"class1Name":"蔬菜","class2Name":"花果","isOnline":1,"isThirdOnline":1,"isLock":0,"inventory":26.28,"yesterdaySales":0,"lastPurchasePrice":1.80,"lastReceivePrice":2.00,"originPrice":4.98,"sellPrice":4.98,"vipPrice":0.00,"actList":null,"status":1,"skuCount":null,"saleAmt":null,"cSaleNum":4.35,"pics":"20180507150808_59170.jpg","isVip":null}]}
     *
     * @param shopId
     * @param skuIds
     * @return
     */
    @Override
    public List<SkuExt> skuExt(Integer shopId, List<Integer> skuIds) {
        log.info("skuExt请求参数：shopId= {} skuIds={}", shopId, JSON.toJSONString(skuIds));
        String result = null;
        try {
            result = Https.post(productServiceUrl + "/shop/ex/sku/queryEsSkuInfoByCondition", null, JSON.toJSONString(new SkuParam(shopId, skuIds)));
            log.info("skuExt请求结果：result = {}", result);
            JSONObject jsonObject = JSON.parseObject(result);
            Boolean success = jsonObject.getBoolean("success");
            if (success == null || !success) {
                log.warn("商品接口失败，无法查询到商品基础信息：shopId={} skuIds={}", shopId, skuIds);
                return new ArrayList<>();
            }
            JSONArray body = jsonObject.getJSONArray("body");
            if (CollectionUtils.isEmpty(body)) {
                log.warn("无法查询到商品基础信息：shopId={} skuIds={}", shopId, skuIds);
                return new ArrayList<>();
            }
            List<SkuExt> skuExts = new ArrayList<>();
            for (int i = 0; i < body.size(); i++) {
                JSONObject jo = body.getJSONObject(i);
                SkuExt skuExt = SkuExt.create(shopId, jo);
                skuExts.add(skuExt);
            }
            return skuExts;
        } catch (Exception e) {
            log.warn("商品信息有问题：{} {}", productServiceUrl + "/shop/ex/sku/queryEsSkuInfoByCondition", (result == null) ? "" : result, e);
            throw new BusinessException("调用商品价格服务失败");
        }


    }

    @Data
    public static class SkuParam {
        private Integer shopId;
        private List<Integer> skuIds;

        public SkuParam(Integer shopId, List<Integer> skuIds) {
            Assert.notEmpty(skuIds, "商品列表不为空");
            Assert.notNull(shopId, "门店不能为空");
            this.shopId = shopId;
            this.skuIds = skuIds;
        }
    }

}
