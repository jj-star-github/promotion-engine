package com.qmfresh.promotion.platform.domain.model.activity.cp.action;

import lombok.Getter;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public class QueryCpAction implements CpAction {

    private Integer pageNum;
    private Integer pageSize;
    private Integer skuId;
    private String skuName;

    public static QueryCpAction create(Integer pageNum, Integer pageSize, Integer skuId, String skuName) {
        QueryCpAction queryCpAction = new QueryCpAction();
        queryCpAction.pageNum = pageNum;
        queryCpAction.pageSize = pageSize;
        queryCpAction.skuId = skuId;
        queryCpAction.skuName = skuName;
        return queryCpAction;
    }
}
