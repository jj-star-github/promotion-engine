package com.qmfresh.promotion.platform.domain.model.promactivity.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityTargetSkuManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.enums.SkuTypeEnum;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.PromActivityTargetSkuMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityTargetSkuPO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityCampOnSkuDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityTargetSkuDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 */
@Service
@Slf4j
public class PromActivityTargetSkuManagerImpl extends ServiceImpl<PromActivityTargetSkuMapper, PromActivityTargetSkuPO>
        implements PromActivityTargetSkuManager {

    @Override
    public List<PromActivityTargetSkuPO> queryByActivity(Long activityId) {
        return baseMapper.queryByActivity(activityId);
    }

    @Override
    public List<PromActivityTargetSkuPO> queryByActivityList(List<Long> activityIdList) {
        return baseMapper.queryByActivityList(activityIdList);
    }

    @Override
    public List<Integer> querySkuId(Long activityId) {
        return baseMapper.querySkuId(activityId);
    }

    @Override
    public void insertBatch(Long activityId, List<PromActivityTargetSkuDTO> skuDTOList) {
        List<PromActivityTargetSkuPO> skuPOList = new ArrayList<>();
        skuDTOList.forEach(
                skuDTO->{
                    PromActivityTargetSkuPO skuPO = new PromActivityTargetSkuPO(skuDTO);
                    skuPO.setActivityId(activityId);
                    skuPOList.add(skuPO);
                }
        );
        if (!skuPOList.isEmpty()) {
            super.saveBatch(skuPOList);
        }
    }

    @Override
    public void deletedBatch(Long activityId, List<Integer> skuIdList) {
        baseMapper.deletedBatch(activityId, skuIdList, new Date());
    }

    @Override
    public List<Long> queryActivityIdByClass2(List<Long> activityList, List<Integer> class2IdList) {
        return baseMapper.queryActivityIdByClass2(class2IdList, activityList);
    }

    @Override
    public List<Long> queryActivityIdBySkuId(List<Long> activityList, List<Integer> skuIdList, Integer skuType) {
        return baseMapper.queryActivityIdBySkuId(skuIdList, activityList, skuType);
    }

    @Override
    public List<PromActivityCampOnSkuDTO> filter(List<PromActivityCampOnSkuDTO> skuDTOList, PromActivityPO activityPO, String traceId) {
        List<PromActivityCampOnSkuDTO> exchangeSkuList = new ArrayList<PromActivityCampOnSkuDTO>();
        //是否支持全品
        if (SkuTypeEnum.ALL.getCode().equals(activityPO.getSkuType())) {
            return skuDTOList;
        }
        List<Integer> skuIdList = activityPO.getTargetSkuId();
        //获取活动目标品类
        if (CollectionUtils.isEmpty(skuIdList)) {
            log.info("SKU验证-当前活动没有对应的SKU信息 traceId={} activityId={}", traceId, activityPO.getId());
            return exchangeSkuList;
        }
        //判断活动目标品类型
        switch (activityPO.getSkuType()) {
            case 2: //class1
                skuDTOList.forEach(
                        skuDTO->{
                            if (skuIdList.contains(skuDTO.getClass1Id())){
                                exchangeSkuList.add(skuDTO);
                            }
                        });
                break;
            case 3: //class2
                skuDTOList.forEach(
                        skuDTO->{
                            if (skuIdList.contains(skuDTO.getClass2Id())){
                                exchangeSkuList.add(skuDTO);
                            }
                        });
                break;
            case 4: //SKU
                skuDTOList.forEach(
                        skuDTO->{
                            if (skuIdList.contains(skuDTO.getSkuId())){
                                exchangeSkuList.add(skuDTO);
                            }
                        });
                break;
            default:
                log.info("SKU验证-活动skuType信息有误 traceId={} activityId={}", traceId, activityPO.getId());
             break;
        }
        return exchangeSkuList;
    }

}
