package com.qmfresh.promotion.platform.domain.model.funds.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.platform.domain.model.funds.FundsChangeNoteManager;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsOrderTransactionVO;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsTransactionVO;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.*;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.FundsChangeNoteMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 */
@Service
@Slf4j
public class FundsChangeNoteManagerImpl extends ServiceImpl<FundsChangeNoteMapper, FundsChangeNote>
        implements FundsChangeNoteManager {

    @Override
    public void insertChange(FundsTransactionVO transactionVo, FundsAccount account) {
        FundsChangeNote change = new FundsChangeNote();
        BeanUtils.copyProperties(transactionVo, change);
        change.setAccountId(account.getId());
        change.setChangeAmount(transactionVo.getChangeAmount().compareTo(new BigDecimal("0")) > 0 ? transactionVo.getChangeAmount() : transactionVo.getChangeAmount().negate());
        change.setChangeType(transactionVo.getChangeAmount().compareTo(new BigDecimal("0")) > 0 ? 1 : -1);
        change.setChangeBefore(account.getTotalAmount().subtract(transactionVo.getChangeAmount()));
        change.setChangeAfter(account.getTotalAmount());
        change.setCreateDay(DateUtil.formatDate());
        change.setGmtCreate(new Date());
        change.setGmtModified(new Date());
        baseMapper.insert(change);
    }

    @Override
    public void insertChange(List<FundsOrderTransactionVO> orderTransaction, FundsAccount account, BigDecimal changeAmount, String createDay) {
        BigDecimal changeBefore = account.getTotalAmount().subtract(changeAmount);
        List<FundsChangeNote> changeNoteList = new ArrayList<>();
        orderTransaction.forEach(
                ot -> {
                    FundsChangeNote change = new FundsChangeNote();
                    BeanUtils.copyProperties(ot, change);
                    change.setAccountId(account.getId());
                    change.setShopId(account.getShopId());
                    change.setShopType(account.getShopType());
                    change.setChangeAmount(ot.getChangeAmount().compareTo(new BigDecimal("0")) > 0 ? ot.getChangeAmount() : ot.getChangeAmount().negate());
                    change.setChangeType(ot.getChangeAmount().compareTo(new BigDecimal("0")) > 0 ? 1 : -1);
                    change.setChangeBefore(changeBefore);
                    change.setActivityId(ot.getActivityId());
                    change.setChangeAfter(account.getTotalAmount());
                    if (StringUtils.isNotBlank(createDay)) {
                        change.setCreateDay(createDay);
                    } else {
                        change.setCreateDay(DateUtil.formatDate());
                    }
                    change.setGmtCreate(new Date());
                    change.setGmtModified(new Date());
                    changeNoteList.add(change);
                }
        );
        if (!changeNoteList.isEmpty()) {
            super.saveBatch(changeNoteList);
        }
    }

    @Override
    public List<FundsChangeNote> querySkuBuAccount(Long accountId, String createDay) {
        return baseMapper.querySkuByAccount(accountId, createDay);
    }

    @Override
    public List<FundsChangeNote> queryPayNote(Long accountId, String createDay, List<Integer> businessTypeList, List<String> businessId, Integer start, Integer length) {
        return baseMapper.queryPayNote(accountId, createDay, businessTypeList, businessId, start, length);
    }

    @Override
    public Integer queryPayNoteCount(Long accountId, String createDay, Integer businessType, String businessId) {
        return baseMapper.queryPayNoteCount(accountId, createDay, businessType, businessId);
    }

    @Override
    public List<FundsChangeNote> queryZbPayStatis(Long accountId, String createDay, Integer start, Integer length) {
        return baseMapper.queryZbPayStatis(accountId, createDay, start,length);
    }

    @Override
    public Integer queryZbPayStatisCount(Long accountId, String createDay) {
        return baseMapper.queryZbPayStatisCount(accountId, createDay);
    }

    @Override
    public List<FundsShopDayChangeStatis> shopDayNoteStatis(Long accountId, String beginTime, String endTime, Integer shopType, Integer start, Integer length) {
        return baseMapper.shopDayNoteStatis(accountId, beginTime, endTime, shopType, start, length);
    }

    @Override
    public Integer shopDayNoteStatisCount(Long accountId, String beginTime, String endTime, Integer shopType) {
        return baseMapper.shopDayNoteStatisCount(accountId, beginTime, endTime, shopType);
    }

    @Override
    public FundsChangeNoteGMVSum sumGMV(Long accountId, String beginTime, String endTime) {
        return baseMapper.sumGMV(accountId, beginTime, endTime);
    }

    @Override
    public List<Integer> queryTargetShopByAccountId(Long accountId, String createDay) {
        return baseMapper.queryTargetShopByAccountId(accountId, createDay);
    }

    @Override
    public List<FundsWeekClear> queryWeekClear(Long accountId, Date endTime) {
        return baseMapper.queryWeekClear(accountId, endTime);
    }

    @Override
    public FundsWeekClear queryDayWeekClear(Long accountId, String createDay) {
        return baseMapper.queryDayWeekClear(accountId, createDay);
    }

    @Override
    public FundsChangeNote queryByWeekClear(String businessCode) {
        return baseMapper.queryByWeekClear(businessCode);
    }

    @Override
    public List<FundsChangeNote> queryWeekClearChangeNote(Long accountId, String createDay,Integer start, Integer length) {
        return baseMapper.queryWeekClearChangeNote(accountId,createDay,start,length);
    }

    @Override
    public Integer countWeekClearChangeNote(Long accountId, String createDay) {
        return baseMapper.countWeekClearChangeNote(accountId,createDay);
    }
}