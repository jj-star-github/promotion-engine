package com.qmfresh.promotion.platform.domain.model.activity.cp.general.command;

import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import lombok.Builder;
import lombok.Getter;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.List;

/**
 * 思路基于所有门店的更改
 * <p>
 * 1.
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class GeneralCpShopEditCommand implements Serializable {

    private Operator operator;
    /**
     * 活动id
     */
    private Long activityId;

    /**
     * 门店id，活动修改后的所有门店
     */
    private List<Integer> shopIds;


    /**
     * 总部改价门店编辑命令
     *
     * @param operator   操作者
     * @param activityId 改价活动id
     * @param shopIds    门店列表
     * @return 总部改价门店编辑命令
     */
    public static GeneralCpShopEditCommand create(Operator operator, Long activityId, List<Integer> shopIds) {
        Assert.notNull(activityId, "活动id不能为空");
        Assert.notEmpty(shopIds, "门店列表不能为空");
        Assert.notNull(operator, "操作者不能为空");

        return GeneralCpShopEditCommand.builder()
                .operator(operator)
                .activityId(activityId)
                .shopIds(shopIds)
                .build();
    }


}
