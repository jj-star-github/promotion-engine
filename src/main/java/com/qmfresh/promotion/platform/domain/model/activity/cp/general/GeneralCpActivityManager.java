package com.qmfresh.promotion.platform.domain.model.activity.cp.general;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpItemResult;
import com.qmfresh.promotion.platform.domain.model.activity.cp.SkuCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.command.*;
import com.qmfresh.promotion.platform.domain.shared.Page;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface GeneralCpActivityManager {
    /**
     * 查询总部改价商品明细
     *
     * @param generalCpQueryCommand
     * @return
     */
    Page<GeneralCpItem> pageGeneralCpDetails(GeneralCpQueryCommand generalCpQueryCommand);

    Page<GeneralCpActivity> pageGeneralCpActivity(GeneralCpQueryCommand generalCpQueryCommand);

    /**
     * 关闭总部改价是一个比较重的业务操作
     *
     * @param generalCpCloseCommand 总部改价
     */
    void closeGeneralCpActivity(GeneralCpCloseCommand generalCpCloseCommand);

    /**
     * 启动门店改价
     */
    void startGeneralCpActivityByJob();

    void stopGeneralCpActivityByJob();

    /**
     * 编辑总部改价门店
     *
     * @param generalCpShopEditCommand 总部改价命令
     */
    void editShopOfGeneralCpActivity(GeneralCpShopEditCommand generalCpShopEditCommand);

    /**
     * 以下是商品的编辑
     */
    void editGeneralCpSku(GeneralCpEditSkuCommand generalCpEditSkuCommand);

    void delGeneralCpSku(GeneralCpDelSkuCommand generalCpDelSkuCommand);

    /**
     * 分页总部改价列表
     *
     * @param generalCpQuerySkuCommand 总部改价查询
     * @return
     */
    Page<SkuCpItem> pageGeneralCpSkus(GeneralCpQuerySkuCommand generalCpQuerySkuCommand);

    /***
     *
     * @param generalCpAutoCreateCommand
     * @return
     */
    CpItemResult autoCreateGeneralCp(GeneralCpAutoCreateCommand generalCpAutoCreateCommand);
}
