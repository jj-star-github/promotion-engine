package com.qmfresh.promotion.platform.domain.model.settle.apportion;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.gis.Shop;
import com.qmfresh.promotion.bean.sku.Sku;
import com.qmfresh.promotion.bean.sku.SkuQuery;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.entity.*;
import com.qmfresh.promotion.external.service.ICashExternalApiService;
import com.qmfresh.promotion.external.service.IInventoryService;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.platform.domain.model.activity.SkuManager;
import com.qmfresh.promotion.platform.domain.model.funds.FundsAccountManager;
import com.qmfresh.promotion.platform.domain.model.funds.FundsChangeNoteManager;
import com.qmfresh.promotion.platform.domain.model.funds.enums.FundsBusinessTypeEnum;
import com.qmfresh.promotion.platform.domain.model.funds.enums.FundsShopTypeEnum;
import com.qmfresh.promotion.platform.domain.model.settle.rule.ShopRuleManager;
import com.qmfresh.promotion.platform.domain.service.DLock;
import com.qmfresh.promotion.platform.domain.shared.BigDecimalUtil;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.valueobj.funds.*;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.FundsChangeNoteMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsAccount;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsChangeNote;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsChangeNoteGMVSum;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopPromotionRule;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopWeekClear;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopWeekSum;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.dto.CashOrderDTO;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.dto.CashOrderItemDTO;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.dto.WeekClearDTO;
import com.qmfresh.promotion.platform.interfaces.common.BizCode;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.record.dto.WmsReceiveDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto.QueryRuleInfoReqDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto.ShopIdDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto.ShopWeekDTO;
import com.qmfresh.promotion.platform.interfaces.shop.facade.funds.WeekClear;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author zbr
 * @Date 9:38 2020/8/24
 */
@Service
@Slf4j
public class ApportionManagerImpl implements ApportionManager {

    @Resource
    private IQmExternalApiService iQmExternalApiService;
    @Resource
    private ICashExternalApiService iCashExternalApiService;
    @Resource
    private ShopRuleManager shopRuleManager;
    @Resource
    private FundsAccountManager service;
    @Resource
    private IInventoryService inventoryService;
    @Resource
    private SkuManager skuManager;
    @Resource
    private ShopWeekClearManager shopWeekClearManager;
    @Resource
    private FundsAccountManager fundsAccountManager;
    @Resource
    private FundsChangeNoteManager fundsChangeNoteManager;
    @Resource
    private DLock dLock;

    @Override
    public void apportion(CashOrderDTO orderDTO) {
        log.info("start into apportion,request:" + JSON.toJSONString(orderDTO));

        Shop shop = new Shop();
        shop.setPromotionGrayType(1);
        List<Shop> shopList = iQmExternalApiService.getShopByCondition(shop);
        if (ListUtil.isNullOrEmpty(shopList)) {
            log.warn("未获取到门店列表");
            return;
        }

        List<Long> skuIdList = shopList.stream().map(Shop::getId).collect(Collectors.toList());
        if(!skuIdList.contains(orderDTO.getShopId().longValue())){
            log.warn("该门店不在白名单内，shopId={} 白名单门店id={}", orderDTO.getShopId(), JSON.toJSONString(skuIdList));
            return;
        }

        ShopDetail shopDetail = iQmExternalApiService.getShopById(orderDTO.getShopId());
        log.info("门店信息:" + JSON.toJSONString(shopDetail));
        //门店级别
        Integer shopLevel = shopDetail.getShopLevel();
        ShopPromotionRule shopPromotionRule = queryRuleType(shopLevel);
        log.info("门店营销规则:" + JSON.toJSONString(shopPromotionRule));

        //百
        BigDecimal hundred = new BigDecimal("100");
        //shopId
        Integer shopId = orderDTO.getShopId();
        //门店名称
        String shopName = shopDetail.getShopName();
        //营销费率
        BigDecimal promotionPercent = shopPromotionRule.getPromotionPercent();
        //营销费率门店占比
        BigDecimal shopPercent = shopPromotionRule.getShopPercent();
        //营销费率门店占比
        BigDecimal areaPercent = shopPromotionRule.getAreaPercent();
        //订单号
        String orderCode = orderDTO.getOrderCode();
        //实付金额
        BigDecimal goodsAmount = orderDTO.getGoodsAmount();
        //抹零
        BigDecimal freePrice = orderDTO.getFreePrice();
        //收银机整单改价打折  总价-实付价-抹零-优惠券价-番茄币
        BigDecimal discountPrice = orderDTO.getGoodsPrice().subtract(goodsAmount).subtract(freePrice).subtract(orderDTO.getCouponPrice()).subtract(orderDTO.getCoinToMoney());
        log.info("收银机改价打折:" + JSON.toJSONString(discountPrice));

        List<FundsOrderTransactionVO> voList = new ArrayList<>();
        BigDecimal shopSettle = new BigDecimal("0.00");
        BigDecimal areaSettle = new BigDecimal("0.00");
        BigDecimal fundsAmount = new BigDecimal("0.00");
        BigDecimal shopCutValueAmount = new BigDecimal("0.00");
        BigDecimal baseCutValueAmount = new BigDecimal("0.00");
        BigDecimal areaCutValueAmount = new BigDecimal("0.00");
        BigDecimal itemSum = new BigDecimal("0.00");
        Integer skuNum = 0;
        Boolean isAct = false;

        for (CashOrderItemDTO orderItem : orderDTO.getItems()) {
            BigDecimal shopValue = new BigDecimal("0.00");
            BigDecimal baseValue = new BigDecimal("0.00");
            BigDecimal areaValue = new BigDecimal("0.00");
            Long shopActId = null;
            if(null == orderItem.getAdvicePrice()){
                log.warn("该商品advicePrice为空，skuId={}，advicePrice={}", orderItem.getSkuId(), orderItem.getAdvicePrice() );
                return;
            }
            Long advicePrice = orderItem.getAdvicePrice();
            BigDecimal originalPrice =  new BigDecimal((advicePrice / 1000.0) + "");
            //查询sku所对应的一级类目
            com.qmfresh.promotion.platform.domain.model.activity.Sku skuClass=skuManager.sku(orderItem.getSkuId());
            Integer class1Id = skuClass.getClass1Id();
            //查sku详情
            SkuQuery skuQuery = new SkuQuery();
            skuQuery.setSkuId(Long.valueOf(orderItem.getSkuId()));
            List<Sku> sku = iQmExternalApiService.getBySkuCondition(skuQuery);
            log.info("sku信息:" + JSON.toJSONString(sku));

            //单品收入
            FundsOrderTransactionVO vo = new FundsOrderTransactionVO();
            //shopId
            vo.setTargetShop(shopId);
            //skuID
            vo.setBusinessId(String.valueOf(orderItem.getSkuId()));
            //orderCode
            vo.setBusinessCode(orderCode);
            vo.setBusinessType(FundsBusinessTypeEnum.SKU_INCOME.getCode());
            //is_sku_clean
            if(class1Id == 55 || class1Id==56){
                vo.setIsSkuClean(1);
            }else {
                vo.setIsSkuClean(0);
            }
            //单品GMV
            BigDecimal itemGMV = orderItem.getTotal().multiply(originalPrice);
            vo.setBusinessGmv(itemGMV);
            //GMV*promotionPercent  单品总营销费用收入
            BigDecimal itemPromotion = itemGMV.multiply(promotionPercent.divide(hundred));
            vo.setBusinessMarketingAmount(itemPromotion);
            //GMV*promotionPercent*shopPercent  单品门店总营销费用收入分摊
            BigDecimal itemShop = itemPromotion.multiply(shopPercent.divide(hundred));
            //大区单品收入分摊
            BigDecimal itemArea = itemPromotion.multiply(areaPercent.divide(hundred));
            vo.setChangeAmount(itemShop);
            log.info("GMV计算，itemGMV={} itemPromotion={} itemShop={}", itemGMV, itemPromotion, itemShop);
            //单品订单实付金额
            vo.setBusinessRealAmount(orderItem.getRealPrice());
            vo.setRemark(sku.get(0).getSkuName());
            voList.add(vo);

            //单品折扣
            if (BigDecimal.ZERO.compareTo(orderItem.getDiscount()) < 0) {
                FundsOrderTransactionVO discountVo = new FundsOrderTransactionVO();
                discountVo.setTargetShop(shopId);
                discountVo.setBusinessId(String.valueOf(orderItem.getSkuId()));
                discountVo.setBusinessCode(orderCode);
                discountVo.setBusinessType(FundsBusinessTypeEnum.ITEM_DISCOUNT.getCode());
                discountVo.setIsSkuClean(0);
                discountVo.setChangeAmount(orderItem.getDiscount().negate());
                discountVo.setBusinessRealAmount(orderItem.getSubTotal());
                FundsSkuVO dcSku = new FundsSkuVO();
                dcSku.setAmount(orderItem.getTotal());
                dcSku.setOriginPrice(originalPrice);
                dcSku.setPriceAfterDiscount(orderItem.getRealPrice().divide(orderItem.getTotal(), 6, BigDecimal.ROUND_DOWN));
                dcSku.setSkuId(Long.valueOf(orderItem.getSkuId()));
                dcSku.setSkuName(sku.get(0).getSkuName());
                dcSku.setSkuFormat(sku.get(0).getSkuFormat());
                dcSku.setIsWeight(sku.get(0).getIsWeight());
                discountVo.setRemark(JSON.toJSONString(dcSku));
                voList.add(discountVo);
                shopCutValueAmount = shopCutValueAmount.add(orderItem.getDiscount().negate());
            }

            List<ActDetail> actDetails = JSON.parseArray(orderItem.getActList(), ActDetail.class);
            log.info("改价轨迹:" + JSON.toJSONString(actDetails));
            if(CollectionUtils.isNotEmpty(actDetails)){
                isAct = true;
                for (ActDetail act : actDetails) {
                    if (act.getPromotionType() == 11) {
                        shopActId = act.getActId();
                        shopValue = act.getCutValue();
                    } else if (act.getPromotionType() == 12) {
                        baseValue = act.getCutValue();
                    } else if (act.getPromotionType() == 14){
                        areaValue = act.getCutValue();
                    }
                }
                BigDecimal shopCutValue = shopValue.multiply(orderItem.getTotal());
                BigDecimal baseCutValue = baseValue.multiply(orderItem.getTotal());
                BigDecimal areaCutValue = areaValue.multiply(orderItem.getTotal());

                if(shopCutValue.compareTo(BigDecimal.ZERO)!=0){
                    //单品支出
                    FundsOrderTransactionVO zcVO = new FundsOrderTransactionVO();
                    zcVO.setTargetShop(shopId);
                    //改价活动id
                    zcVO.setActivityId(shopActId);
                    //skuid
                    zcVO.setBusinessId(String.valueOf(orderItem.getSkuId()));
                    //ordercode
                    zcVO.setBusinessCode(orderCode);
                    zcVO.setBusinessType(FundsBusinessTypeEnum.SKU_MARKETING.getCode());
                    //营销费用 正=加   负=扣减
                    zcVO.setChangeAmount(shopCutValue);
                    zcVO.setBusinessRealAmount(orderItem.getRealPrice());
                    FundsSkuVO skuVO = new FundsSkuVO();
                    skuVO.setAmount(orderItem.getTotal());
                    skuVO.setOriginPrice(originalPrice);
                    skuVO.setPriceAfterDiscount(orderItem.getSellPrice());
                    skuVO.setSkuId(Long.valueOf(orderItem.getSkuId()));
                    skuVO.setSkuName(sku.get(0).getSkuName());
                    skuVO.setSkuFormat(sku.get(0).getSkuFormat());
                    skuVO.setRealAmount(orderItem.getRealPrice());
                    skuVO.setIsWeight(sku.get(0).getIsWeight());
                    zcVO.setRemark(JSON.toJSONString(skuVO));
                    voList.add(zcVO);

                    //累加门店单品支出
                    if(shopCutValue.compareTo(BigDecimal.ZERO) > 0){
                        shopSettle = shopSettle.add(shopCutValue);
                        fundsAmount = fundsAmount.add(shopCutValue);
                    }else {
                        shopCutValueAmount = shopCutValueAmount.add(shopCutValue);
                    }
                }
                //累加总部单品支出
                if(baseCutValue.compareTo(BigDecimal.ZERO) > 0){
                    fundsAmount = fundsAmount.add(baseCutValue);
                }else {
                    baseCutValueAmount = baseCutValueAmount.add(baseCutValue);
                }
                //累加大区单品支出
                if(areaCutValue.compareTo(BigDecimal.ZERO) > 0){
                    areaSettle = areaSettle.add(areaCutValue);
                    fundsAmount = fundsAmount.add(areaCutValue);
                }else {
                    areaCutValueAmount = areaCutValueAmount.add(areaCutValue);
                }
            }
            //单品 建议价*数量
            BigDecimal itemAmount = originalPrice.multiply(orderItem.getTotal());
            //累加单品建议价*数量
            itemSum = itemSum.add(itemAmount);
            //累加单品门店总营销费用收入分摊
            shopSettle = shopSettle.add(itemShop);
            //累加单品大区总营销费用收入分摊
            areaSettle = areaSettle.add(itemArea);
            //累加单品总营销费用收入
            fundsAmount = fundsAmount.add(itemPromotion);
            //计数
            skuNum = skuNum+1;

        }
        log.info("计数:" + JSON.toJSONString(skuNum));
        log.info("累加值，shopSettle={} fundsAmount={} shopCutValueAmount={} baseCutValueAmount={}", shopSettle, fundsAmount,shopCutValueAmount, baseCutValueAmount);

        //收银机改价打折--门店有
        if (discountPrice.compareTo(BigDecimal.ZERO) > 0) {
            FundsOrderTransactionVO cpVO = new FundsOrderTransactionVO();
            cpVO.setTargetShop(shopId);
            cpVO.setBusinessId(orderCode);
            cpVO.setBusinessCode(orderCode);
            cpVO.setBusinessType(FundsBusinessTypeEnum.ORDER_CP.getCode());
            cpVO.setChangeAmount(discountPrice.negate());
            log.info("收银机改价打折:" + JSON.toJSONString(discountPrice.negate()));
            FundsOrderCPVO fundsOrderCPVO = new FundsOrderCPVO();
            fundsOrderCPVO.setOriginPrice(orderDTO.getGoodsPrice());
            fundsOrderCPVO.setSkuNum(skuNum);
            cpVO.setRemark(JSON.toJSONString(fundsOrderCPVO));
            cpVO.setBusinessRealAmount(goodsAmount);
            voList.add(cpVO);
        }

        FundsSettleVO dto = new FundsSettleVO();
        dto.setShopId(shopId);
        dto.setShopType(FundsShopTypeEnum.MD.getCode());
        dto.setShopName(shopName);
        dto.setOrderCode(orderCode);
        //门店营销费用收入 单品累加
        dto.setShopFundsAmount(shopSettle);
        //大区营销费用收入 单品累加
        dto.setAreaFundsAmount(areaSettle);
        //总营销费用收入  单品累加
        dto.setFundsAmount(fundsAmount);
        //订单累计金额
        dto.setRealPay(itemSum);
        //订单营销销售额  参加改价的单品总额
        if (isAct){
            dto.setMarketingPay(itemSum);
        }
        BigDecimal shopUseAmount = shopCutValueAmount.add(discountPrice.negate());
        dto.setShopUseAmount(shopUseAmount.abs());
        //大区营销费用支出
        dto.setAreaUseAmount(areaCutValueAmount.abs());
        //门店 交易费用  加减之后对营销账户影响的费用值
        BigDecimal changeAmount = shopSettle.add(shopUseAmount);
        dto.setChangeAmount(changeAmount);
        //总营销费用支出  门店单品总支出 + 收银机改价 + 总部单品支出 + 抹零 + 大区总支出
        BigDecimal baseUseAmount = baseCutValueAmount.add(freePrice.negate());
        BigDecimal useAmount = baseUseAmount.add(shopUseAmount).add(areaCutValueAmount);
        dto.setUseAmount(useAmount.abs());
        dto.setOrderTransaction(voList);
        Long time = orderDTO.getCreateTime();
        Date day = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        dto.setCreateDay(sdf.format(day));
        service.updateAccountBySettle(dto);
        this.baseApportion(orderDTO,shopPromotionRule);
        this.areaApportion(orderDTO,shopPromotionRule);
    }

    @Override
    public void baseApportion(CashOrderDTO orderDTO,ShopPromotionRule shopPromotionRule) {
        log.info("start into baseApportion，orderDTO={}", orderDTO);

        //百
        BigDecimal hundred = new BigDecimal("100");
        //shopId
        Integer shopId = orderDTO.getShopId();
        //营销费率
        BigDecimal promotionPercent = shopPromotionRule.getPromotionPercent();
        //营销费率总部占比
        BigDecimal basePercent = shopPromotionRule.getBasePercent();

        //订单号
        String orderCode = orderDTO.getOrderCode();
        //实付金额
        BigDecimal goodsAmount = orderDTO.getGoodsAmount();
        //抹零
        BigDecimal freePrice = orderDTO.getFreePrice();
        List<FundsOrderTransactionVO> voList = new ArrayList<>();
        BigDecimal baseSettle = new BigDecimal("0.00");
        BigDecimal baseCutValueAmount = new BigDecimal("0.00");

        for (CashOrderItemDTO orderItem : orderDTO.getItems()) {
            BigDecimal baseValue = new BigDecimal("0.00");
            Long baseActId = null;
            Long advicePrice = orderItem.getAdvicePrice();
            BigDecimal originalPrice =  new BigDecimal((advicePrice / 1000.0) + "");
            SkuQuery skuQuery = new SkuQuery();
            skuQuery.setSkuId(Long.valueOf(orderItem.getSkuId()));
            //查sku详情
            List<Sku> sku = iQmExternalApiService.getBySkuCondition(skuQuery);

            //单品收入
            FundsOrderTransactionVO vo = new FundsOrderTransactionVO();
            //shopId
            vo.setTargetShop(shopId);
            //skuID
            vo.setBusinessId(String.valueOf(orderItem.getSkuId()));
            //orderCode
            vo.setBusinessCode(orderCode);
            vo.setBusinessType(FundsBusinessTypeEnum.SKU_INCOME.getCode());
            //单品GMV
            BigDecimal itemGMV = orderItem.getTotal().multiply(originalPrice);
            vo.setBusinessGmv(itemGMV);
            //GMV*promotionPercent  单品总营销费用收入
            BigDecimal itemPromotion = itemGMV.multiply(promotionPercent.divide(hundred));
            vo.setBusinessMarketingAmount(itemPromotion);
            //GMV*promotionPercent*shopPercent  单品门店总营销费用收入分摊
            BigDecimal itemBase = itemPromotion.multiply(basePercent.divide(hundred));
            vo.setChangeAmount(itemBase);
            log.info("GMV计算，itemGMV={} itemPromotion={} itemBase={}", itemGMV, itemPromotion, itemBase);
            //单品订单实付金额
            vo.setBusinessRealAmount(orderItem.getRealPrice());
            vo.setRemark(sku.get(0).getSkuName());
            voList.add(vo);

            List<ActDetail> actDetails = JSON.parseArray(orderItem.getActList(), ActDetail.class);
            log.info("改价轨迹:" + JSON.toJSONString(actDetails));

            if(CollectionUtils.isNotEmpty(actDetails)){
                for (ActDetail act : actDetails) {
                    if (act.getPromotionType() == 12) {
                        baseActId = act.getActId();
                        baseValue = act.getCutValue();
                    }
                }

                //营销费用 正=加   负=扣减  改价轨迹相加
                BigDecimal baseCutValue = baseValue.multiply(orderItem.getTotal());
                if(baseCutValue.compareTo(BigDecimal.ZERO)!=0){
                    //单品支出
                    FundsOrderTransactionVO zcVO = new FundsOrderTransactionVO();
                    zcVO.setTargetShop(shopId);
                    //改价活动id
                    zcVO.setActivityId(baseActId);
                    //skuid
                    zcVO.setBusinessId(String.valueOf(orderItem.getSkuId()));
                    //ordercode
                    zcVO.setBusinessCode(orderCode);
                    zcVO.setBusinessType(FundsBusinessTypeEnum.SKU_MARKETING.getCode());
                    zcVO.setChangeAmount(baseCutValue);
                    zcVO.setBusinessRealAmount(orderItem.getRealPrice());
                    FundsSkuVO skuVO = new FundsSkuVO();
                    skuVO.setAmount(orderItem.getTotal());
                    skuVO.setOriginPrice(originalPrice);
                    skuVO.setPriceAfterDiscount(orderItem.getSellPrice());
                    skuVO.setSkuId(Long.valueOf(orderItem.getSkuId()));
                    skuVO.setSkuName(sku.get(0).getSkuName());
                    skuVO.setSkuFormat(sku.get(0).getSkuFormat());
                    skuVO.setIsWeight(sku.get(0).getIsWeight());
                    skuVO.setRealAmount(orderItem.getRealPrice());
                    zcVO.setRemark(JSON.toJSONString(skuVO));
                    voList.add(zcVO);

                    //累加总部单品支出
                    if(baseCutValue.compareTo(BigDecimal.ZERO) > 0){
                        baseSettle = baseSettle.add(baseCutValue);
                    }else {
                        baseCutValueAmount = baseCutValueAmount.add(baseCutValue);
                    }
                }
            }
            //累加单品门店总营销费用收入分摊
            baseSettle = baseSettle.add(itemBase);
        }
        //订单抹零--总部有
        if (freePrice.compareTo(BigDecimal.ZERO) > 0) {
            FundsOrderTransactionVO mlVO = new FundsOrderTransactionVO();
            mlVO.setTargetShop(shopId);
            mlVO.setBusinessId(orderCode);
            mlVO.setBusinessCode(orderCode);
            mlVO.setBusinessType(FundsBusinessTypeEnum.ORDER_ML.getCode());
            mlVO.setChangeAmount(freePrice.negate());
            log.info("抹零:" + JSON.toJSONString(freePrice.negate()));
            mlVO.setRemark("抹零");
            mlVO.setBusinessRealAmount(goodsAmount);
            voList.add(mlVO);
        }

        FundsSettleVO dto = new FundsSettleVO();
        dto.setShopId(1);
        dto.setShopType(FundsShopTypeEnum.ZB.getCode());
        dto.setShopName("总部");
        dto.setOrderCode(orderCode);
        //总部营销费用支出   总部单品总支出 + 抹零
        BigDecimal baseUseAmount = baseCutValueAmount.add(freePrice.negate());
        //门店 交易费用  加减之后对营销账户影响的费用值
        BigDecimal changeAmount = baseSettle.add(baseUseAmount);
        dto.setChangeAmount(changeAmount);
        dto.setOrderTransaction(voList);
        service.updateAccountBySettle(dto);
    }

    @Override
    public void areaApportion(CashOrderDTO orderDTO,ShopPromotionRule shopPromotionRule) {
        log.info("start into areaApportion，orderDTO={}", orderDTO);
        //百
        BigDecimal hundred = new BigDecimal("100");
        //shopId
        Integer shopId = orderDTO.getShopId();
        //营销费率
        BigDecimal promotionPercent = shopPromotionRule.getPromotionPercent();
        //营销费率大区占比
        BigDecimal areaPercent = shopPromotionRule.getAreaPercent();
        //订单号
        String orderCode = orderDTO.getOrderCode();
        List<FundsOrderTransactionVO> voList = new ArrayList<>();
        BigDecimal areaSettle = new BigDecimal("0.00");
        BigDecimal areaCutValueAmount = new BigDecimal("0.00");

        for (CashOrderItemDTO orderItem : orderDTO.getItems()) {
            Long areaActId = null;
            BigDecimal areaValue = new BigDecimal("0.00");
            Long advicePrice = orderItem.getAdvicePrice();
            BigDecimal originalPrice =  new BigDecimal((advicePrice / 1000.0) + "");
            SkuQuery skuQuery = new SkuQuery();
            skuQuery.setSkuId(Long.valueOf(orderItem.getSkuId()));
            //查sku详情
            List<Sku> sku = iQmExternalApiService.getBySkuCondition(skuQuery);

            //单品收入
            FundsOrderTransactionVO vo = new FundsOrderTransactionVO();
            //shopId
            vo.setTargetShop(shopId);
            //skuID
            vo.setBusinessId(String.valueOf(orderItem.getSkuId()));
            //orderCode
            vo.setBusinessCode(orderCode);
            vo.setBusinessType(FundsBusinessTypeEnum.SKU_INCOME.getCode());
            //单品GMV
            BigDecimal itemGMV = orderItem.getTotal().multiply(originalPrice);
            vo.setBusinessGmv(itemGMV);
            //GMV*promotionPercent  单品总营销费用收入
            BigDecimal itemPromotion = itemGMV.multiply(promotionPercent.divide(hundred));
            vo.setBusinessMarketingAmount(itemPromotion);
            //GMV*promotionPercent*areaPercent  单品大区总营销费用收入分摊
            BigDecimal itemArea = itemPromotion.multiply(areaPercent.divide(hundred));
            vo.setChangeAmount(itemArea);
            log.info("GMV计算，itemGMV={} itemPromotion={} itemArea={}", itemGMV, itemPromotion, itemArea);
            //单品订单实付金额
            vo.setBusinessRealAmount(orderItem.getRealPrice());
            vo.setRemark(sku.get(0).getSkuName());
            voList.add(vo);

            List<ActDetail> actDetails = JSON.parseArray(orderItem.getActList(), ActDetail.class);
            log.info("改价轨迹:" + JSON.toJSONString(actDetails));

            if(CollectionUtils.isNotEmpty(actDetails)){
                for (ActDetail act : actDetails) {
                    if (act.getPromotionType() == 14) {
                        areaActId = act.getActId();
                        areaValue = act.getCutValue();
                    }
                }

                //营销费用 正=加   负=扣减  改价轨迹相加
                BigDecimal areaCutValue = areaValue.multiply(orderItem.getTotal());
                if(areaCutValue.compareTo(BigDecimal.ZERO)!=0){
                    //单品支出
                    FundsOrderTransactionVO zcVO = new FundsOrderTransactionVO();
                    zcVO.setTargetShop(shopId);
                    //改价活动id
                    zcVO.setActivityId(areaActId);
                    //skuid
                    zcVO.setBusinessId(String.valueOf(orderItem.getSkuId()));
                    //ordercode
                    zcVO.setBusinessCode(orderCode);
                    zcVO.setBusinessType(FundsBusinessTypeEnum.SKU_MARKETING.getCode());
                    zcVO.setChangeAmount(areaCutValue);
                    zcVO.setBusinessRealAmount(orderItem.getRealPrice());
                    FundsSkuVO skuVO = new FundsSkuVO();
                    skuVO.setAmount(orderItem.getTotal());
                    skuVO.setOriginPrice(originalPrice);
                    skuVO.setPriceAfterDiscount(orderItem.getSellPrice());
                    skuVO.setSkuId(Long.valueOf(orderItem.getSkuId()));
                    skuVO.setSkuName(sku.get(0).getSkuName());
                    skuVO.setSkuFormat(sku.get(0).getSkuFormat());
                    skuVO.setIsWeight(sku.get(0).getIsWeight());
                    skuVO.setRealAmount(orderItem.getRealPrice());
                    zcVO.setRemark(JSON.toJSONString(skuVO));
                    voList.add(zcVO);

                    //累加大区单品支出
                    if(areaCutValue.compareTo(BigDecimal.ZERO) > 0){
                        areaSettle = areaSettle.add(areaCutValue);
                    }else {
                        areaCutValueAmount = areaCutValueAmount.add(areaCutValue);
                    }
                }
            }
            //累加单品大区总营销费用收入分摊
            areaSettle = areaSettle.add(itemArea);
        }

        FundsSettleVO dto = new FundsSettleVO();
        dto.setShopType(FundsShopTypeEnum.DQ.getCode());
        List shopList = new ArrayList();
        shopList.add(shopId);
        AreaConditionQuery param = new AreaConditionQuery();
        param.setShopIds(shopList);
        List<BaseArea> areaDetail = iQmExternalApiService.selectAreaByShopIds(param);
        dto.setShopId(areaDetail.get(0).getId());
        dto.setShopName(areaDetail.get(0).getAreaName());
        dto.setOrderCode(orderCode);
        //大区 交易费用  加减之后对营销账户影响的费用值
        BigDecimal changeAmount = areaSettle.add(areaCutValueAmount);
        dto.setChangeAmount(changeAmount);
        dto.setOrderTransaction(voList);
        service.updateAccountBySettle(dto);
    }

    @Override
    public ShopPromotionRule queryRuleType(Integer shopLevel){
        QueryRuleInfoReqDTO shopPromotionRule = new QueryRuleInfoReqDTO();
        log.info("门店等级:" + JSON.toJSONString(shopLevel));
        if (shopLevel == null) {
            throw new BusinessException("门店等级为空:" + JSON.toJSONString(shopLevel));
        } else {
            switch (shopLevel){
                case 1:
                    shopPromotionRule.setShopGrade(1);
                    shopPromotionRule.setShopType(1);
                    break;
                case 2:
                    shopPromotionRule.setShopGrade(1);
                    shopPromotionRule.setShopType(2);
                    break;
                case 3:
                    shopPromotionRule.setShopGrade(1);
                    shopPromotionRule.setShopType(3);
                    break;
                case 4:
                    shopPromotionRule.setShopGrade(2);
                    shopPromotionRule.setShopType(1);
                    break;
                case 5:
                    shopPromotionRule.setShopGrade(2);
                    shopPromotionRule.setShopType(2);
                    break;
                case 6:
                    shopPromotionRule.setShopGrade(2);
                    shopPromotionRule.setShopType(3);
                    break;
                case 7:
                    shopPromotionRule.setShopGrade(3);
                    shopPromotionRule.setShopType(1);
                    break;
                case 8:
                    shopPromotionRule.setShopGrade(3);
                    shopPromotionRule.setShopType(2);
                    break;
                case 9:
                    shopPromotionRule.setShopGrade(3);
                    shopPromotionRule.setShopType(3);
                    break;
                default:
                    shopPromotionRule.setShopGrade(9);
                    shopPromotionRule.setShopType(9);
                    break;
            }
            return shopRuleManager.queryRuleType(shopPromotionRule);
        }
    }

    @Override
    public void dayWmsReceive(){
        WmsReceiveDTO wmsReceiveDTO = new WmsReceiveDTO();
        Shop shop = new Shop();
        shop.setPromotionGrayType(1);
        List<Shop> shopList = iQmExternalApiService.getShopByCondition(shop);
        log.info("白名单门店:" + JSON.toJSONString(shopList));
        if (ListUtil.isNullOrEmpty(shopList)) {
            log.warn("未获取到门店列表");
        }
        for(Shop shopDetail:shopList){
            //昨天0点的时间戳
            Calendar calendar = Calendar.getInstance();
            calendar.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)-1,0,0,0);
            Long yesterdayStart = calendar.getTime().getTime()/1000;

            //昨天23:59:59 秒的时间戳
            Calendar calendar1 = Calendar.getInstance();
            calendar1.set(calendar1.get(Calendar.YEAR),calendar1.get(Calendar.MONTH),calendar1.get(Calendar.DAY_OF_MONTH)-1,23,59,59);
            Long yesterdayEnd = calendar1.getTime().getTime()/1000;

            Long shopId = shopDetail.getId();
            wmsReceiveDTO.setWarehouseId(shopDetail.getWarehouseId());
            wmsReceiveDTO.setStatus(2);
            wmsReceiveDTO.setDeliveryBeginTime(yesterdayStart.intValue());
            wmsReceiveDTO.setDeliveryEndTime(yesterdayEnd.intValue());
            log.info("门店到货单请求参数:" + JSON.toJSONString(wmsReceiveDTO));
//            List<WmsReceiveDetail> wmsReceiveList = inventoryService.getwmsReceiveDetail(wmsReceiveDTO);
            List<ReceiveInfo> wmsReceiveList = inventoryService.getReceiveInfo(wmsReceiveDTO);
            log.info("门店到货单，shopId={} wmsReceiveList={}", shopId, JSON.toJSONString(wmsReceiveList));
            if (ListUtil.isNullOrEmpty(wmsReceiveList)) {
                continue;
            }
            for(ReceiveInfo receiveInfo: wmsReceiveList){
                log.info("单条到货单信息:" + JSON.toJSONString(receiveInfo));
                List<ShopWeekClear> shopWeekClearList = new ArrayList<>();
                for(WmsReceiveDetail detail:receiveInfo.getDetails()){
                    log.info("单品信息:" + JSON.toJSONString(detail));
                    Long skuId = detail.getSkuId();
//                    SkuPrice skuPrice = skuManager.shopSkuPrice(shopId.intValue(),skuId.intValue());
                    com.qmfresh.promotion.platform.domain.model.activity.Sku sku=skuManager.sku(skuId.intValue());
                    Integer class1Id = sku.getClass1Id();
                    log.info("商品一级类目:" + JSON.toJSONString(class1Id));

                    ShopWeekClear shopWeekClear = new ShopWeekClear();
                    shopWeekClear.setShopId(shopId);
                    shopWeekClear.setSkuId(skuId);
                    //商品原价*1.3
//                    BigDecimal originPrice = skuPrice.getOriginalPrice();
                    BigDecimal originPrice = detail.getUnitPrice().multiply(new BigDecimal("1.3"));
                    shopWeekClear.setOriginPrice(originPrice);
                    if(detail.getIsWeight()==0){
                        //到货量
                        BigDecimal receiveNum= detail.getReceiveNum();
                        shopWeekClear.setRealAmount(receiveNum);
                        BigDecimal priceAmount = originPrice.multiply(receiveNum);
                        shopWeekClear.setPriceAmount(priceAmount);
                    }else if(detail.getIsWeight()==1){
                        //到货量
                        BigDecimal receiveWeight= detail.getReceiveWeight();
                        shopWeekClear.setRealAmount(receiveWeight);
                        BigDecimal priceAmount = originPrice.multiply(receiveWeight);
                        shopWeekClear.setPriceAmount(priceAmount);
                    }
                    if(class1Id == 55 || class1Id==56){
                        shopWeekClear.setIsSkuClear(1);
                    }else {
                        shopWeekClear.setIsSkuClear(0);
                    }
                    shopWeekClear.setCreateDay(DateUtil.formatDate());
                    shopWeekClear.setGmtCreate(new Date());
                    shopWeekClear.setGmtModified(new Date());
                    shopWeekClear.setIsDeleted(0);
                    shopWeekClearList.add(shopWeekClear);
                }
                log.info("订单存储信息:" + JSON.toJSONString(shopWeekClearList));
                shopWeekClearManager.saveBatch(shopWeekClearList);
            }
        }
        log.info("end");
    }

    @Override
    public void weekClear() {
        Shop shop = new Shop();
        shop.setPromotionGrayType(1);
        List<Shop> shopList = iQmExternalApiService.getShopByCondition(shop);
        log.info("白名单门店:" + JSON.toJSONString(shopList));
        if (ListUtil.isNullOrEmpty(shopList)) {
            log.warn("未获取到门店列表");
        }
        for(Shop shopDetail:shopList){
            //门店id
            Long shopId = shopDetail.getId();
            FundsAccount fundsAccount = fundsAccountManager.byShopIdAndType(shopId.intValue(),4);
            log.info("门店营销账户信息:" + JSON.toJSONString(fundsAccount));
            //门店营销账户id
            Long accountId = fundsAccount.getId();
            log.info("门店营销账户id:" + JSON.toJSONString(accountId));
            if (accountId == null){
                log.warn("该门店没有营销账户，shopId={} accountId={}", shopId, accountId);
                continue;
            }
            ShopDetail shopInfo = iQmExternalApiService.getShopById(shopId.intValue());
            log.info("门店信息:" + JSON.toJSONString(shopInfo));
            //门店级别
            Integer shopLevel = shopInfo.getShopLevel();
            //门店名称
            String shopName = shopInfo.getShopName();
            ShopPromotionRule shopPromotionRule = queryRuleType(shopLevel);
            log.info("门店营销规则:" + JSON.toJSONString(shopPromotionRule));
            if(shopPromotionRule == null){
                log.warn("该门店营销规则等级有误，shopId={} shopLevel={}", shopId, shopLevel);
                continue;
            }
            //百
            BigDecimal hundred = new BigDecimal("100");
            //门店损耗率
            BigDecimal lossPercent = shopPromotionRule.getLossPercent();
            log.info("门店损耗率:" + JSON.toJSONString(lossPercent));
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
            String start = sdf.format(DateUtil.getDateBefore(7));
            String end = sdf.format(DateUtil.getDateBefore(1));
            BigDecimal gmv = new BigDecimal("0");
            BigDecimal weekValue = new BigDecimal("0");
            //GMV
            log.info("sumGMV请求参数，accountId={} start={} end={}", accountId, start,end);
            FundsChangeNoteGMVSum gmvSum = fundsChangeNoteManager.sumGMV(accountId,start,end);
            log.info("gmvSum:" + JSON.toJSONString(gmvSum));
            if(null == gmvSum) {
                log.warn("该门店GMV为null,重置为0" );
            }else {
                gmv = gmvSum.getBusinessGmv();
            }
            log.info("周gmv，shopId={} gmv={} ", shopId, gmv);
            //周实际进货值
            log.info("querySumpriceAmount请求参数，shopId={} start={} end={}", shopId, start,end);
            ShopWeekSum weekSum= shopWeekClearManager.querySumpriceAmount(shopId.intValue(),start,end);
            log.info("weekSum:" + JSON.toJSONString(weekSum));
            if(weekSum == null){
                log.warn("该门店weekValue为null,重置为0" );
            }else {
                weekValue = weekSum.getPriceAmount();
            }
            log.info("周实际进货值，shopId={} weekValue={} ", shopId, weekValue);
            //损耗值
            BigDecimal loss = weekValue.multiply(lossPercent.divide(hundred));
            log.info("损耗值，shopId={} loss={} ", shopId, loss);
            //周实际进货值 - 损耗值
            BigDecimal realWeekValue = weekValue.subtract(loss);
            //GMV - (周实际进货值 - 损耗值)
            BigDecimal weekClear = gmv.subtract(realWeekValue);
            log.info("周清最终计算值，shopId={} weekClear={} ", shopId, weekClear);
            if(weekClear.compareTo(BigDecimal.ZERO) > 0){
                FundsTransactionVO transactionVo = new FundsTransactionVO();
                transactionVo.setShopId(shopId.intValue());
                transactionVo.setShopName(shopName);
                transactionVo.setShopType(FundsShopTypeEnum.MD.getCode());
                transactionVo.setBusinessType(FundsBusinessTypeEnum.WEEK_INCOME.getCode());
                transactionVo.setChangeAmount(weekClear);
                transactionVo.setRemark("周清结算收入");
                service.updateAccount(transactionVo);
            }
        }
        log.info("end");
    }

    @Override
    public void updateOrder(List<String> list) {
        log.info("订单列表参数:" + JSON.toJSONString(list));
        List<CashOrderDTO> orderList= iCashExternalApiService.selectOrderMainItemByOrderCode(list);
        log.info("返回订单列表:" + JSON.toJSONString(orderList));
        for(CashOrderDTO cashOrderDTO : orderList){
            apportion(cashOrderDTO);
        }
    }

    @Override
    public void shopWeekClear(WeekClearDTO weekClearDTO) {
        boolean result = dLock.tryLock(weekClearDTO.getUniqueSign(), 5000L);
        if (!result) {
            log.warn("周清 - 消息重复发送，标识={}", weekClearDTO.getUniqueSign());
            return;
        }
        FundsChangeNote fundsChangeNote= fundsChangeNoteManager.queryByWeekClear(weekClearDTO.getUniqueSign());
        if(fundsChangeNote != null){
            log.warn("周清 - 消息重复处理，标识={}", weekClearDTO.getUniqueSign());
            return;
        }
        Long shopId  = weekClearDTO.getShopId();
        Shop shop = new Shop();
        shop.setPromotionGrayType(1);
        List<Shop> shops = iQmExternalApiService.getShopByCondition(shop);
        if (ListUtil.isNullOrEmpty(shops)) {
            log.warn("周清 - 未获取到门店列表");
            return;
        }
        List<Long> skuIdList = shops.stream().map(Shop::getId).collect(Collectors.toList());
        if(!skuIdList.contains(shopId)){
            log.warn("周清 - 该门店不在白名单内，shopId={} 白名单门店id={}", shopId, JSON.toJSONString(skuIdList));
            return;
        }
        //根据shopId查询比例和安全值
        ShopIdDTO shopIdDTO = new ShopIdDTO();
        shopIdDTO.setShopId(shopId.intValue());
        ShopWeekDTO shopWeekDTO= shopRuleManager.queryRuleByShopId(shopIdDTO);
        log.info("周清 - 门店比例：" + JSON.toJSONString(shopWeekDTO));
        if(shopWeekDTO == null){
            log.warn("周清 - 查询门店规则异常，shopId={}", shopId);
            return;
        }
        //根据shopId查询shopName
        ShopDetail shopDetail = iQmExternalApiService.getShopById(weekClearDTO.getShopId().intValue());
        if(shopDetail == null){
            log.warn("周清 - 查询门店详情异常，shopId={}", shopId);
            return;
        }
        String shopName = shopDetail.getShopName();
        //根据shopId查询accountid
        FundsAccount fundsAccount = fundsAccountManager.byShopIdAndType(shopId.intValue(),4);
        if(fundsAccount == null){
            log.warn("周清 - 该门店无营销账户，shopId={}", shopId);
            return;
        }
        //根据shopId查询areaId
        List shopList = new ArrayList();
        shopList.add(shopId);
        AreaConditionQuery param = new AreaConditionQuery();
        param.setShopIds(shopList);
        List<BaseArea> areaDetail = iQmExternalApiService.selectAreaByShopIds(param);
        if(ListUtil.isNullOrEmpty(areaDetail)){
            log.warn("周清 - 查询门店所属大区异常，shopId={}", shopId);
            return;
        }
        //唯一标识
        String businessCode = weekClearDTO.getUniqueSign();
        //门店的差值
        BigDecimal weekClearAmount = weekClearDTO.getReward();
        //最小安全值
        BigDecimal minPromPrice = fundsAccount.getMinPromPrice();
        //余额
        BigDecimal totalAmount = fundsAccount.getTotalAmount();
        //周清值*比例
        BigDecimal hundred = new BigDecimal("100");
        //门店去化奖励
        BigDecimal shopWeekClear = weekClearAmount.multiply(shopWeekDTO.getShopWeekClearPercent()).divide(hundred,6,BigDecimal.ROUND_HALF_UP);
        //大区去化奖励
        BigDecimal areaWeekclear = weekClearAmount.multiply(shopWeekDTO.getAreaWeekClearPercent()).divide(hundred,6,BigDecimal.ROUND_HALF_UP);
        log.info("周清 - 周清值，shopId={} weekClearAmount={} minPromPrice={} totalAmount={} shopWeekClear={} areaWeekclear={}", shopId,weekClearAmount,minPromPrice,totalAmount,shopWeekClear,areaWeekclear);
        //备注+起始日期
        FundsWeekClearVO fundsWeekClearVO = new FundsWeekClearVO();
        fundsWeekClearVO.setRemark("去化奖励");
        fundsWeekClearVO.setTimeStart(weekClearDTO.getVoidanceStartTime());
        fundsWeekClearVO.setTimeEnd(weekClearDTO.getVoidanceEndTime());
        fundsWeekClearVO.setReward(weekClearAmount);
        fundsWeekClearVO.setShopId(shopId);
        fundsWeekClearVO.setShopName(shopName);
        fundsWeekClearVO.setMinPromPrice(minPromPrice);
        fundsWeekClearVO.setShopWeekClearPercent(shopWeekDTO.getShopWeekClearPercent());
        fundsWeekClearVO.setAreaWeekClearPercent(shopWeekDTO.getAreaWeekClearPercent());
        fundsWeekClearVO.setChangeType(weekClearDTO.getChangeType());
        String remark = JSON.toJSONString(fundsWeekClearVO);
        //记录流水
        if(weekClearDTO.getChangeType().equals(1)){
            log.warn("周清 - 去化奖励正向，shopId={}", shopId);
            //门店
            updateAccount(shopId.intValue(),shopName,FundsShopTypeEnum.MD.getCode(),FundsBusinessTypeEnum.WEEK_INCOME.getCode(),shopWeekClear,remark,businessCode);
            //大区
            updateAccount(areaDetail.get(0).getId(),areaDetail.get(0).getAreaName(),FundsShopTypeEnum.DQ.getCode(),FundsBusinessTypeEnum.WEEK_INCOME.getCode(),areaWeekclear,remark,businessCode);
        }else if(weekClearDTO.getChangeType().equals(0)){
            //余额减周清值后的差值
            BigDecimal afterDiff = totalAmount.subtract(shopWeekClear);
            if(totalAmount.compareTo(minPromPrice) < 1){
                //余额小于等于安全值
                log.warn("周清 - 余额小于等于安全值，shopId={}", shopId);
                //门店0流水
                updateAccount(shopId.intValue(),shopName,FundsShopTypeEnum.MD.getCode(),FundsBusinessTypeEnum.WEEK_INCOME.getCode(),BigDecimal.ZERO,remark,businessCode);
                //大区代扣门店
                updateAccount(areaDetail.get(0).getId(),areaDetail.get(0).getAreaName(),FundsShopTypeEnum.DQ.getCode(),FundsBusinessTypeEnum.WEEK_INCOME.getCode(),shopWeekClear.negate(),remark,businessCode);
                //大区流水
                updateAccount(areaDetail.get(0).getId(),areaDetail.get(0).getAreaName(),FundsShopTypeEnum.DQ.getCode(),FundsBusinessTypeEnum.WEEK_INCOME.getCode(),areaWeekclear.negate(),remark,businessCode);
            }else {
                //余额-周清 >= 安全值
                if(afterDiff.compareTo(minPromPrice) > -1){
                    log.warn("周清 - 余额-周清 >= 安全值，shopId={}", shopId);
                    //门店流水
                    updateAccount(shopId.intValue(),shopName,FundsShopTypeEnum.MD.getCode(),FundsBusinessTypeEnum.WEEK_INCOME.getCode(),shopWeekClear.negate(),remark,businessCode);
                    //大区流水
                    updateAccount(areaDetail.get(0).getId(),areaDetail.get(0).getAreaName(),FundsShopTypeEnum.DQ.getCode(),FundsBusinessTypeEnum.WEEK_INCOME.getCode(),areaWeekclear.negate(),remark,businessCode);
                }else{
                    //余额-周清 < 安全值
                    //余额减安全值
                    log.warn("周清 - 余额-周清 < 安全值，shopId={}", shopId);
                    BigDecimal diff = totalAmount.subtract(minPromPrice).abs();
                    //大区扣减余额和安全值的差值
                    updateAccount(shopId.intValue(),shopName,FundsShopTypeEnum.MD.getCode(),FundsBusinessTypeEnum.WEEK_INCOME.getCode(),diff.negate(),remark,businessCode);
                    //根据差值更新大区账户
                    BigDecimal areaDiff = shopWeekClear.subtract(diff).abs();
                    //大区代扣
                    updateAccount(areaDetail.get(0).getId(),areaDetail.get(0).getAreaName(),FundsShopTypeEnum.DQ.getCode(),FundsBusinessTypeEnum.WEEK_INCOME.getCode(),areaDiff.negate(),remark,businessCode);
                    //大区流水
                    updateAccount(areaDetail.get(0).getId(),areaDetail.get(0).getAreaName(),FundsShopTypeEnum.DQ.getCode(),FundsBusinessTypeEnum.WEEK_INCOME.getCode(),areaWeekclear.negate(),remark,businessCode);
                }
            }
        }
    }

    public void updateAccount(Integer shopId,String shopName,Integer shopType,Integer businnessType, BigDecimal changeAmount,String remark,String businessCode){
        FundsTransactionVO transactionVo = new FundsTransactionVO();
        transactionVo.setShopId(shopId);
        transactionVo.setShopName(shopName);
        transactionVo.setShopType(shopType);
        transactionVo.setBusinessCode(businessCode);
        transactionVo.setBusinessType(businnessType);
        transactionVo.setChangeAmount(changeAmount);
        transactionVo.setRemark(remark);
        service.updateWeekAccount(transactionVo);
    }

}
