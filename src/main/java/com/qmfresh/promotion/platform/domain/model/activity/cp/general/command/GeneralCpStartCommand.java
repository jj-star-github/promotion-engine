package com.qmfresh.promotion.platform.domain.model.activity.cp.general.command;

import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class GeneralCpStartCommand implements Serializable {

    /**
     * 操作者
     */
    private Operator operator;
    /**
     * 创建时间
     */
    private Date currentTime;


    public static GeneralCpStartCommand createForJob(Operator operator) {
        GeneralCpStartCommand generalCpStartCommand
                = GeneralCpStartCommand.builder()
                .currentTime(new Date())
                .operator(operator)
                .build();
        return generalCpStartCommand;
    }


}
