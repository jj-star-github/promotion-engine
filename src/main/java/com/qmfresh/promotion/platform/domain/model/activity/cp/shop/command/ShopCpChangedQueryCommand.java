package com.qmfresh.promotion.platform.domain.model.activity.cp.shop.command;

import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class ShopCpChangedQueryCommand implements Serializable {

    /**
     * 门店id
     */
    private Integer shopId;
    /**
     *
     */
    private Integer pageNum;
    private Integer pageSize;
    private String skuKey;
    private String skuId;

    public static ShopCpChangedQueryCommand create(Integer pageNum,
                                                   Integer pageSize,
                                                   Integer shopId,
                                                   String skuKey
    ) {

        if (pageNum == null || pageSize == null) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("分页参数异常");
        }
        if (shopId == null) {
            throw new BusinessException("必须设置门店id");
        }
        ShopCpChangedQueryCommand shopCpChangedQueryCommand
                = ShopCpChangedQueryCommand.builder()
                .pageNum(pageNum)
                .pageSize(pageSize)
                .shopId(shopId)
                .skuKey(skuKey)
                .build();
        return shopCpChangedQueryCommand;
    }
}
