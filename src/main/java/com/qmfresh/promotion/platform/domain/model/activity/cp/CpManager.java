package com.qmfresh.promotion.platform.domain.model.activity.cp;

import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.Sku;
import com.qmfresh.promotion.platform.domain.model.activity.cp.command.CpCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.command.ShopQueryCommand;
import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ActivityPO;

import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface CpManager {


    Page<ShopCpResult> pageShopCp(ShopQueryCommand shopQueryCommand);


    /**
     * 创建改价活动
     *
     * @param cpCreateCommand 改价创建活动命令
     * @return 返回接口
     */
    CpItemResult create(CpCreateCommand cpCreateCommand);

    ActivityPO createOneShop(ActivityPO activityPO, Shop shop, List<Sku> skus,
                             CpCreateCommand cpCreateCommand, List<CpItem> result);
}
