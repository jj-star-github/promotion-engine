package com.qmfresh.promotion.platform.domain.model.settle.rule;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.ShopDetail;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.platform.domain.model.settle.apportion.ApportionManager;
import com.qmfresh.promotion.platform.domain.shared.BigDecimalUtil;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.FundsAccountMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsAccount;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.ShopRuleMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopPromotion;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopPromotionRule;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author zbr
 * @Date 15:42 2020/8/22
 */
@Service
@Slf4j
public class ShopRuleManagerImpl  extends ServiceImpl<ShopRuleMapper, ShopPromotionRule> implements ShopRuleManager {

    @Resource
    private ShopRuleManager shopRuleManager;
    @Resource
    private IQmExternalApiService iQmExternalApiService;
    @Resource
    private ApportionManager apportionManager;

    @Resource
    private FundsAccountMapper fundsAccountMapper;

    @Override
    public Boolean createShopRule(ShopRuleReqDTO param) {
        log.info("start into createShopRule,request:" + JSON.toJSONString(param));
        List<ShopRuleDTO> shopRuleDTOList = param.getShopRuleDTO();
        List<ShopPromotionRule> promotionRuleList = new ArrayList<>();
        for(int i = 0; i < shopRuleDTOList.size(); i++){
            ShopPromotionRule shopPromotionRule = new ShopPromotionRule();
            shopPromotionRule.setId(param.getShopRuleDTO().get(i).getId());
            shopPromotionRule.setShopGrade(param.getShopRuleDTO().get(i).getShopGrade());
            shopPromotionRule.setShopType(param.getShopRuleDTO().get(i).getShopType());
            shopPromotionRule.setWeekClearPercent(param.getShopRuleDTO().get(i).getWeekClearPercent());
            shopPromotionRule.setWeekClearShopPercent(param.getShopRuleDTO().get(i).getWeekClearShopPercent());
            shopPromotionRule.setWeekClearAreaPercent(param.getShopRuleDTO().get(i).getWeekClearAreaPercent());
            shopPromotionRule.setPromotionPercent(param.getShopRuleDTO().get(i).getPromotionPercent());
            shopPromotionRule.setBasePercent(param.getShopRuleDTO().get(i).getBasePercent());
            shopPromotionRule.setAreaPercent(param.getShopRuleDTO().get(i).getAreaPercent());
            shopPromotionRule.setShopPercent(param.getShopRuleDTO().get(i).getShopPercent());

            shopPromotionRule.setOperatorId(param.getOperatorId());
            shopPromotionRule.setOperatorName(param.getOperatorName());
            shopPromotionRule.setGmtCreate(new Date());
            shopPromotionRule.setGmtModified(new Date());
            shopPromotionRule.setIsDeleted(0);
            shopPromotionRule.setOperatorId(param.getOperatorId());
            shopPromotionRule.setOperatorName(param.getOperatorName());
            promotionRuleList.add(shopPromotionRule);
        }
        log.info("promotionRuleList:" + JSON.toJSONString(promotionRuleList));
        return shopRuleManager.saveOrUpdateBatch(promotionRuleList);
    }

    @Override
    public List<ShopPromotion> queryRuleInfoList(QueryRuleInfoReqDTO param) {
        log.info("start into queryRuleInfoList,request:" + JSON.toJSONString(param));
        return baseMapper.queryRuleInfoList(param);
    }

    @Override
    public ShopPromotionRule queryRuleDetailInfo(IdReqDto id) {
        log.info("start into queryRuleDetailInfo,request:" + JSON.toJSONString(id));
        return shopRuleManager.getById(id);
    }

    @Override
    public Boolean modifyShopRule(UpdateShopRuleReqDTO param) {
        log.info("start into modifyShopRule,request:" + JSON.toJSONString(param));
        ShopPromotionRule shopPromotionRule = new ShopPromotionRule();

        shopPromotionRule.setId(param.getId());
        shopPromotionRule.setShopGrade(param.getShopGrade());
        shopPromotionRule.setShopType(param.getShopType());
        shopPromotionRule.setLossPercent(param.getLossPercent());
        shopPromotionRule.setPromotionPercent(param.getPromotionPercent());
        shopPromotionRule.setBasePercent(param.getBasePercent());
        shopPromotionRule.setShopPercent(param.getShopPercent());
        shopPromotionRule.setAreaPercent(param.getAreaPercent());
        shopPromotionRule.setGmtModified(new Date());
        shopPromotionRule.setWeekClearPercent(param.getWeekClearPercent());
        shopPromotionRule.setWeekClearShopPercent(param.getWeekClearShopPercent());
        shopPromotionRule.setWeekClearAreaPercent(param.getWeekClearAreaPercent());
        return shopRuleManager.updateById(shopPromotionRule);
    }

    @Override
    public ShopPromotionRule queryRuleType(QueryRuleInfoReqDTO param) {
        log.info("门店规则等级:" + JSON.toJSONString(param));
        ShopPromotionRule shopPromotionRule = shopRuleManager.getOne(new LambdaQueryWrapper<ShopPromotionRule>()
                .eq(ShopPromotionRule::getShopGrade,param.getShopGrade())
                .eq(ShopPromotionRule::getShopType,param.getShopType())
                .eq(ShopPromotionRule::getIsDeleted,0)
        );
        log.info("门店规则:" + JSON.toJSONString(shopPromotionRule));
        return shopPromotionRule;
    }

    @Override
    public ShopWeekDTO queryRuleByShopId(ShopIdDTO shopId) {
        ShopWeekDTO shopWeekDTO = new ShopWeekDTO();
        ShopDetail shopInfo = iQmExternalApiService.getShopById(shopId.getShopId());
        log.info("门店信息:" + JSON.toJSONString(shopInfo));
        //门店级别
        Integer shopLevel = shopInfo.getShopLevel();
        ShopPromotionRule shopPromotionRule = apportionManager.queryRuleType(shopLevel);
        FundsAccount account = fundsAccountMapper.byShopIdAndType(shopId.getShopId(), 4);
        if(account == null){
            log.warn("该门店暂无营销账户 shopId={}", shopId);
            return null;
        }
        BigDecimal hundred = new BigDecimal("100");
        BigDecimal areaWeekClearPercent = shopPromotionRule.getWeekClearPercent().multiply(shopPromotionRule.getWeekClearAreaPercent()).divide(hundred);
        BigDecimal weekClearPercent = shopPromotionRule.getWeekClearPercent().multiply(shopPromotionRule.getWeekClearShopPercent()).divide(hundred);

        shopWeekDTO.setAreaWeekClearPercent(BigDecimalUtil.round(areaWeekClearPercent));
        shopWeekDTO.setShopWeekClearPercent(BigDecimalUtil.round(weekClearPercent));
        shopWeekDTO.setMinPromPrice(BigDecimalUtil.round(account.getMinPromPrice()));
        return shopWeekDTO;
    }
}
