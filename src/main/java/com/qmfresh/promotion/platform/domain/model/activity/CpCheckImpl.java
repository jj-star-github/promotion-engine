package com.qmfresh.promotion.platform.domain.model.activity;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.SkuCpItem;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.ChangePriceActivityMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ChangePriceActivityPO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class CpCheckImpl implements CpCheck {

    @Resource
    private ChangePriceActivityMapper changePriceActivityMapper;

    @Override
    public boolean cpCreateCheck(Shop shop, SkuCpItem skuCpItem, CpTimeCycle timeCycle, CpBizType cpBizType) {

        if (skuCpItem.getRulePrice() == null) {
            log.warn("促销价不能为空或零：shopId={} skuId={}", shop.getShopId(), skuCpItem.getSkuId());
            return false;
        }

        //检查同一时刻是否有相同的大区改价(开始时间在存在活动区间内)
        int result = changePriceActivityMapper
                .selectCount(
                        new LambdaQueryWrapper<ChangePriceActivityPO>()
                                .eq(ChangePriceActivityPO::getIsDeleted, 0)
                                .eq(ChangePriceActivityPO::getType, cpBizType.getCode())
                                .in(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode(), CpStatus.RUNNING.getCode())
                                .eq(ChangePriceActivityPO::getShopId, shop.getShopId())
                                .eq(ChangePriceActivityPO::getSkuId, skuCpItem.getSkuId())
                                .lt(ChangePriceActivityPO::getPlanActivityStartTime, timeCycle.getBeginTime())
                                .gt(ChangePriceActivityPO::getPlanActivityEndTime, timeCycle.getBeginTime())

                );
        //检查同一时刻是否有相关的大区时间（结束时间在活动区间）
        int result2 = changePriceActivityMapper.selectCount(
                new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getType, cpBizType.getCode())
                        .in(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode(), CpStatus.RUNNING.getCode())
                        .eq(ChangePriceActivityPO::getShopId, shop.getShopId())
                        .eq(ChangePriceActivityPO::getSkuId, skuCpItem.getSkuId())
                        .lt(ChangePriceActivityPO::getPlanActivityStartTime, timeCycle.getEndTime())
                        .gt(ChangePriceActivityPO::getPlanActivityEndTime, timeCycle.getEndTime())
        );

        int result3 = changePriceActivityMapper.selectCount(
                new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getType, cpBizType.getCode())
                        .in(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode(), CpStatus.RUNNING.getCode())
                        .eq(ChangePriceActivityPO::getShopId, shop.getShopId())
                        .eq(ChangePriceActivityPO::getSkuId, skuCpItem.getSkuId())
                        .ge(ChangePriceActivityPO::getPlanActivityStartTime, timeCycle.getBeginTime())
                        .le(ChangePriceActivityPO::getPlanActivityEndTime, timeCycle.getEndTime())
        );

        if ((result + result2 + result3) > 0) {
            log.info("shopId={} skuId={} 共存的result = {} result2={} result3={}", shop.getShopId(), skuCpItem.getSkuId(), result, result2, result3);
            return false;
        }

        log.info("检查通过重复活动：shopId={} skuId={}", shop.getShopId(), skuCpItem.getSkuId());
        return true;
    }

    /**
     * 判断活动在待开始状态
     *
     * @param activityId 活动id
     * @return 结果
     */
    @Override
    public boolean onReady(Long activityId) {
        int i = changePriceActivityMapper.selectCount(
                new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getActivityId, activityId)
                        .gt(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode())
        );
        return i <= 0;
    }

    @Override
    public int cpActivitySkuNum(Long activityId) {
        int i = changePriceActivityMapper.selectCount(
                new QueryWrapper<ChangePriceActivityPO>()
                        .eq("is_deleted", 0)
                        .eq("activity_id", activityId)
                        .select("distinct sku_id")
        );
        return i;
    }
}
