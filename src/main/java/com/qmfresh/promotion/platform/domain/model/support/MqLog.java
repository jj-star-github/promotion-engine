package com.qmfresh.promotion.platform.domain.model.support;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.TableName;
import com.qmfresh.promotion.platform.infrastructure.mapper.support.MqLogMapper;
import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.Message;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Date;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@TableName("t_mq_log")
@Slf4j
public class MqLog implements Serializable {
    private static int OK = 1;
    private static int FAILED = 2;

    private Long id;
    private Date gmtCreate;
    private Date gmtModified;
    private Integer isDeleted;
    private String topic;
    private String msgId;
    private String key;
    private String payload;
    private Integer status;
    private Date retriedTime;


    public static MqLog create(SendResult sendResult, Message message) {
        Integer status = SendStatus.SEND_OK.equals(sendResult.getSendStatus()) ? MqLogStatus.SUCCESS.getCode() :
                MqLogStatus.FAILED.getCode();
        String msgId = sendResult.getMsgId();
        MqLog mqLog = getMqLog(message, status, msgId);
        return mqLog;
    }

    private static MqLog getMqLog(Message message, Integer status, String msgId) {
        String topic = message.getTopic();
        MqLog mqLog = new MqLog();
        mqLog.setGmtCreate(new Date());
        mqLog.setGmtModified(new Date());
        mqLog.setIsDeleted(0);
        mqLog.setTopic(topic);
        mqLog.setMsgId(msgId);
        mqLog.setKey(message.getKeys());
        mqLog.setPayload(new String(message.getBody(), Charset.forName("utf-8")));
        mqLog.setStatus(status);
        return mqLog;
    }

    public static MqLog create(Exception e, Message message) {
        MqLog mqLog = getMqLog(message, MqLogStatus.FAILED.getCode(), "");
        return mqLog;

    }

    /**
     * @author xzw
     * @date ${date}  mailto 741342093@qq.com
     */
    public static interface MessageCallback {

        void exception(Exception e, Message message);

        void success(SendResult sendResult, Message message);
    }

    public static class Noop implements MessageCallback {

        @Override
        public void exception(Exception e, Message message) {
            log.warn("{}", JSON.toJSONString(message), e);
        }

        @Override
        public void success(SendResult sendResult, Message message) {
            log.info("{} {}", JSON.toJSONString(message), sendResult.getMsgId());
        }
    }

    public static class PersistCallback implements MessageCallback {

        private MqLogMapper mapper;

        public PersistCallback(MqLogMapper mqLogMapper) {
            this.mapper = mqLogMapper;
        }

        @Override
        public void exception(Exception e, Message message) {
            MqLog mqLog = MqLog.create(e, message);
            this.mapper.insert(mqLog);
        }

        @Override
        public void success(SendResult sendResult, Message message) {
            MqLog mqLog = MqLog.create(sendResult, message);
            this.mapper.insert(mqLog);
        }
    }

    @Getter
    public enum MqLogStatus {
        /**
         * 成功
         */
        SUCCESS(1),
        /**
         * 失败
         */
        FAILED(2),
        /**
         * 重试
         */
        RETRIED(3);
        int code;

        MqLogStatus(Integer code) {
            this.code = code;
        }
    }
}
