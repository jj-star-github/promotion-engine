package com.qmfresh.promotion.platform.domain.model.activity.cp.general.command;

import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpItem;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class GeneralCpAutoCreateCommand implements Serializable {

    /**
     * 改价时间
     */
    private CpTimeCycle cpTimeCycle;
    private String bizCode;
    private String bizName;
    private String remark;

    private String operatorName;
    private Integer operatorId;

    private List<CpItem> cpItemList;

    public static GeneralCpAutoCreateCommand create(String bizCode,
                                                    String bizName,
                                                    String remark,
                                                    Integer operatorId,
                                                    String operatorName,
                                                    List<CpItem> cpItemDTOS,
                                                    CpTimeCycle cpTimeCycle) {
        if (StringUtils.isBlank(bizCode)) {
            throw new BusinessException("业务Code不能为空");
        }
        if (CollectionUtils.isEmpty(cpItemDTOS)) {
            throw new BusinessException("改价列表不能为空");
        }
        if (cpTimeCycle == null) {
            throw new BusinessException("改价周期不能为空");
        }
        if (StringUtils.isBlank(operatorName)) {
            throw new BusinessException("操作名称不能为空");
        }
        if (operatorId == null) {
            throw new BusinessException("操作者id不能为空");
        }

        GeneralCpAutoCreateCommand generalCpAutoCreateCommand =
                GeneralCpAutoCreateCommand.builder()
                        .cpTimeCycle(cpTimeCycle)
                        .bizCode(bizCode)
                        .bizName(bizName)
                        .remark(remark)
                        .operatorId(operatorId)
                        .operatorName(operatorName)
                        .cpItemList(cpItemDTOS)
                        .build();

        return generalCpAutoCreateCommand;
    }
}
