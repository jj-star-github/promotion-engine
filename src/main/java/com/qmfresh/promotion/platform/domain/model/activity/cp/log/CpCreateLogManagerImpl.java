package com.qmfresh.promotion.platform.domain.model.activity.cp.log;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpItemResult;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpManager;
import com.qmfresh.promotion.platform.domain.model.activity.cp.command.CpCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.command.CpCreateLogCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.command.CpCreateLogQueryCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.command.CpCreateLogStartCommand;
import com.qmfresh.promotion.platform.domain.service.DLock;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.DaemonThreadFactory;
import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.CpCreateLogMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.CpCreateLogPO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class CpCreateLogManagerImpl implements CpCreateLogManager {
    @Resource
    private CpCreateLogMapper cpCreateLogMapper;
    @Resource
    private CpManager cpManager;
    @Resource
    private DLock dLock;

    private ExecutorService service = Executors.newFixedThreadPool(2,
            new DaemonThreadFactory("改价创建线程池"));


    @Override
    public CpCreateLog create(CpCreateLogCreateCommand createLogCreateCommand) {
        log.info("开始创建改价任务：param={}", JSON.toJSONString(createLogCreateCommand));
        CpCreateLog cpCreateLog = createCpCreateLog(createLogCreateCommand);
        if (cpCreateLog == null) {
            log.warn("改价创建业务不支持:bizType={} request={}", createLogCreateCommand.getCpBizType(), createLogCreateCommand.getRequest());
            throw new BusinessException("改价创建业务不支持");
        }

        CpCreateLogPO cpCreateLogPO = CpCreateLogPO.create(cpCreateLog);
        cpCreateLogMapper.insert(cpCreateLogPO);
        service.submit(() -> execute(cpCreateLogPO, CpCreateLogStartCommand.create()));
        return CpCreateLogPO.convert(cpCreateLogPO);
    }


    private CpCreateLog createCpCreateLog(CpCreateLogCreateCommand createLogCreateCommand) {
        CpCreateCommand cpCreateCommand = CpCreateCommand.create(createLogCreateCommand.getRequest());
        return CpCreateLog.create(cpCreateCommand, createLogCreateCommand);
    }

    /**
     * @param cpCreateLogQueryCommand 改价日志查询命令
     * @return
     */
    @Override
    public Page<CpCreateLog> pageCpCreateLog(CpCreateLogQueryCommand cpCreateLogQueryCommand) {
        Integer pageNum = cpCreateLogQueryCommand.getPageNum();
        Integer pageSize = cpCreateLogQueryCommand.getPageSize();
        LambdaQueryWrapper<CpCreateLogPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CpCreateLogPO::getIsDeleted, 0);
        if (CollectionUtils.isNotEmpty(cpCreateLogQueryCommand.getStatusList())) {
            queryWrapper.in(CpCreateLogPO::getStatus, cpCreateLogQueryCommand.getStatusList().stream()
                    .map(CpCreateLogStatus::getCode).collect(Collectors.toList()));
        }
        queryWrapper.orderByDesc(CpCreateLogPO::getId);
        IPage<CpCreateLogPO> iPage = cpCreateLogMapper.selectPage(new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(pageNum, pageSize), queryWrapper);
        return Page.convertIPage(pageNum, pageSize, iPage, CpCreateLogPO::convert);
    }

    @Override
    public CpCreateLog get(Long id) {
        CpCreateLogPO cpCreateLogPO = cpCreateLogMapper.selectById(id);
        if (cpCreateLogPO == null) {
            return null;
        }
        return CpCreateLogPO.convert(cpCreateLogPO);
    }

    @Override
    public void startCpCreateLogJob() {
        log.info("开始执行创建改价活动...");
        LambdaQueryWrapper<CpCreateLogPO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CpCreateLogPO::getStatus, CpCreateLogStatus.CREATING.getCode());
        queryWrapper.eq(CpCreateLogPO::getIsDeleted, 0);
        CpCreateLogStartCommand command = CpCreateLogStartCommand.builder()
                .startTimestamp(System.currentTimeMillis())
                .build();

        CpCreateLogPO.visit(
                cpCreateLogMapper,
                queryWrapper,
                command,
                this::execute);
        log.info("结束开始执行创建改价活动...");
    }

    /**
     * 这是一个长业务，不要可以不做事务嘛？
     * 做事务会有什么影响，应该在哪个程度做事务
     *
     * @param command     命令行
     * @param cpCreateLog 改价创建日志
     */
    @Override
    public void execute(CpCreateLogPO cpCreateLog, CpCreateLogStartCommand command) {
        log.info("开始执行活动创建：id ={} request= {}", cpCreateLog.getId(), cpCreateLog.getRequest());

        //做幂等
        boolean result = dLock.tryLock(DLock.generalCreateLogKey(cpCreateLog.getId()), 5 * 600L);
        if (!result) {
            throw new BusinessException("有改价创建日志存在");
        }

        //状态检查
        if (CpCreateLogStatus.COMPLETED.getCode().equals(cpCreateLog.getStatus())) {
            throw new BusinessException("已经创建完成");
        }
        String request = cpCreateLog.getRequest();
        CpCreateCommand cpCreateCommand = CpCreateCommand.create(request);
        String remark = "";
        CpItemResult cpItemResult = null;
        //创建失败会产生两个类似，尽管不营销活动的最终效果
        try {
            cpItemResult = cpManager.create(cpCreateCommand);
        } catch (Exception e) {
            log.warn("创建活动失败：{}", JSON.toJSONString(cpCreateCommand), e);
            if (e instanceof BusinessException) {
                remark = ((BusinessException) e).getErrorMsg();
            } else {
                remark = e.getMessage();
            }
        }
        Long updateId = cpCreateLog.getId();
        if (updateId == null) {
            throw new BusinessException("必须设置updateId");
        }

        //更新任务的状态
        CpCreateLogCompletedStatus cpCreateLogCompletedStatus;
        if (cpItemResult == null) {
            updateCpCreateLog(updateId, CpCreateLogCompletedStatus.TERMINATED, remark);
            return;
        }
        if (cpItemResult.getActivityId() == null) {
            cpCreateLogCompletedStatus = CpCreateLogCompletedStatus.TERMINATED;
            remark = remark + cpItemResult.getRemark() + ":" + JSON.toJSONString(cpItemResult.getCpItemList());
        } else {
            cpCreateLogCompletedStatus = CpCreateLogCompletedStatus.COMPLETED;
            remark = remark + cpItemResult.getRemark() + JSON.toJSONString(cpItemResult.getCpItemList());

        }
        updateCpCreateLog(updateId, cpCreateLogCompletedStatus, remark);
    }

    public void updateCpCreateLog(Long updateId,
                                   CpCreateLogCompletedStatus cpCreateLogCompletedStatus,
                                   String remark) {
        CpCreateLogPO update = new CpCreateLogPO();
        update.setStatus(CpCreateLogStatus.COMPLETED.getCode());
        update.setCompleteStatus(cpCreateLogCompletedStatus.getCode());
        update.setGmtModified(new Date());
        if (StringUtils.isNotBlank(remark)) {
            int length = remark.length();
            if (length >= 4000) {
                update.setRemark(remark.substring(0, 4000));
            } else {
                update.setRemark(remark);
            }
        }

        cpCreateLogMapper.update(update,
                new LambdaQueryWrapper<CpCreateLogPO>()
                        .eq(CpCreateLogPO::getId, updateId)
        );
    }


}
