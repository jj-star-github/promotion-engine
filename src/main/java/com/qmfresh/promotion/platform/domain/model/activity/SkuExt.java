package com.qmfresh.promotion.platform.domain.model.activity;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class SkuExt implements Serializable {

    private Integer shopId;

    private Integer skuId;
    /**
     * 销量
     */
    private BigDecimal soldAmount;
    /**
     * vip价格
     */
    private BigDecimal vipPrice;
    /**
     * 当前售价
     */
    private BigDecimal currentPrice;
    /**
     * 最终售价
     */
    private BigDecimal originPrice;
    /**
     * 采购价
     */
    private BigDecimal purchasePrice;
    /**
     * 门店成本价
     */
    private BigDecimal shopCostPrice;
    /**
     * 库存量
     */
    private BigDecimal inventoryAmount;

    public static SkuExt create(Integer shopId, JSONObject jo) {

        SkuExt skuExt = new SkuExt();
        skuExt.setShopId(shopId);
        skuExt.setSkuId(jo.getInteger("skuId"));

        skuExt.currentPrice = jo.getBigDecimal("sellPrice");
        skuExt.originPrice = jo.getBigDecimal("originPrice");
        skuExt.vipPrice = jo.getBigDecimal("vipPrice");
        skuExt.soldAmount = jo.getBigDecimal("cSaleNum");
        skuExt.skuId = jo.getInteger("skuId");
        skuExt.purchasePrice = jo.getBigDecimal("lastPurchasePrice");
        skuExt.shopCostPrice = jo.getBigDecimal("lastReceivePrice");
        skuExt.inventoryAmount = jo.getBigDecimal("inventory");
        return skuExt;
    }
}
