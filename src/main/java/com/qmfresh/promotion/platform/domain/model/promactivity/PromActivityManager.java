package com.qmfresh.promotion.platform.domain.model.promactivity;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.platform.domain.model.promactivity.vo.HitTargetConditionVo;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityPO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityBaseDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityCampOnSkuDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityCreateDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityQueryDTO;

import java.util.Date;
import java.util.List;

/**
 * 活动信息
 */
public interface PromActivityManager extends IService<PromActivityPO> {

    /**
     * 根据活动ID查询活动信息
     * @param activityId
     * @return
     */
    PromActivityPO queryByActivityId(Long activityId);

    /**
     *
     * @param queryDto
     * @return
     */
    Integer queryCountByParam(PromActivityQueryDTO queryDto);

    /**
     * 查询活动
     * @param queryDto
     * @return
     */
    List<PromActivityPO> queryByParam(PromActivityQueryDTO queryDto);

    /**
     * 活动失效
     * @param activityId
     * @param operatorName
     * @param operatorId
     */
    void noAvail(Long activityId, String operatorName, Integer operatorId);

    /**
     * 验证是否有相同活动
     * @param param
     * @return
     */
    boolean checkActivity(PromActivityCreateDTO param, String traceId, PromActivityPO po);

    /**
     * 创建活动
     * @param param
     */
    void createActivity(PromActivityCreateDTO param);

    /**
     * 跟新活动信息
     * @param param
     * @param traceId
     */
    void updateActivity(PromActivityCreateDTO param, PromActivityPO activity, String traceId);

    /**
     * 查询门店指定时间点有效的活动信息
     * @param shopId
     * @param time
     * @return
     */
    List<PromActivityPO> queryByShopId(Integer shopId, Date time, int userType);

    /**
     * 执行活动并返回奖品信息
     *
     * @param skuDTOList
     * @param activityPO
     * @return
     */
    HitTargetConditionVo execute(List<PromActivityCampOnSkuDTO> skuDTOList, PromActivityPO activityPO, String traceId);

    /**
     * 活动过期定时任务
     */
    void endJob();

    /**
     * 活动开始定时任务
     */
    void openJob();

}
