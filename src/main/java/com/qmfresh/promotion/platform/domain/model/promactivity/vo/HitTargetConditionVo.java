package com.qmfresh.promotion.platform.domain.model.promactivity.vo;

import com.qmfresh.promotion.bean.coupon.CouponInfo;
import com.qmfresh.promotion.dto.MeetInfo;
import com.qmfresh.promotion.dto.MzContext;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityConditionPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityPO;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 命中奖品信息
 */
@Data
public class HitTargetConditionVo {
    /**
     * 奖品信息
     */
    private PromActivityConditionPO conditionPO;
    /**
     * 奖品数量
     * 数量大于0才支持发奖品
     */
    private Integer num;

    /**
     * 命中条件
     */
    private BigDecimal meetAmount = BigDecimal.ZERO;

    /**
     * 下一级奖品信息
     */
    private List<MeetInfo> meetInfoList;

    public MzContext createMzContext(PromActivityPO activityPO) {
        MzContext mzContext = new MzContext();
        mzContext.setActivityId(activityPO.getId());
        mzContext.setActivityName(activityPO.getName());
        mzContext.setMeetMoney(this.getMeetAmount());
        mzContext.setDescription(activityPO.getRemark());
        mzContext.setMeetInfos(this.getMeetInfoList());
        mzContext.setMeet(false);
        if (this.getNum() > 0) {
            mzContext.setMeet(true);
            PromActivityConditionPO conditionPO = this.getConditionPO();
            mzContext.setPresentType(conditionPO.getAwardType());
            // 赠品类型:1.赠商品，2.赠券 3:番茄币
            switch (conditionPO.getAwardType()) {
                case 1:
                    break;
                case 2:
                    CouponInfo couponInfo = new CouponInfo();
                    couponInfo.setCouponId(conditionPO.getAwardId());
                    couponInfo.setCouponName(conditionPO.getAwardName());
                    couponInfo.setGivingNum(BigDecimal.valueOf(this.getNum()));
                    mzContext.setCouponInfo(couponInfo);
                    break;
                case 3:
                    break;
                default:
                    break;
            }
        }
        return mzContext;
    }
}
