package com.qmfresh.promotion.platform.domain.model.activity.cp;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class ShopCpResult implements Serializable {

    private Integer shopId;
    private Long activityId;
}
