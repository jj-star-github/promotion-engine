package com.qmfresh.promotion.platform.domain.model.activity;

import lombok.Data;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class AdminUser {

    private Integer userId;
    private String name;
    private String realName;
    private Integer roleId;
    private Integer shopId;
}
