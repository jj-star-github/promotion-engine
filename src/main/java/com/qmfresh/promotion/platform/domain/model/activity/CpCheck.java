package com.qmfresh.promotion.platform.domain.model.activity;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.SkuCpItem;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface CpCheck {


    /**
     * 检查改价活动创建
     *
     * @param shop      门店
     * @param skuCpItem 商品
     * @param timeCycle 周期
     * @param cpBizType
     * @return boolean只
     */
    boolean cpCreateCheck(Shop shop, SkuCpItem skuCpItem, CpTimeCycle timeCycle, CpBizType cpBizType);

    /**
     * 活动的状态检查
     *
     * @param activityId 活动id
     * @return boolean
     */
    boolean onReady(Long activityId);

    /**
     * 改价活动商品数
     *
     * @param activityId 活动id
     * @return 返回商品数量
     */
    int cpActivitySkuNum(Long activityId);


}
