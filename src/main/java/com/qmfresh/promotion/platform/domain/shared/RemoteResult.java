package com.qmfresh.promotion.platform.domain.shared;

import lombok.Data;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class RemoteResult<T> {

    private Boolean success;
    private Integer errCode;
    private String message;
    private T body;

}
