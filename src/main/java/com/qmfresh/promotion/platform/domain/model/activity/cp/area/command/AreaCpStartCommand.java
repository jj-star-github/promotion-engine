package com.qmfresh.promotion.platform.domain.model.activity.cp.area.command;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class AreaCpStartCommand implements Serializable {

    private Long activityId;
    private String remark;


    public static AreaCpStartCommand create(Long activityId, String remark) {
        AreaCpStartCommand areaCpStartCommand =
                AreaCpStartCommand.builder()
                        .activityId(activityId)
                        .remark(remark)
                        .build();
        return areaCpStartCommand;
    }

}
