package com.qmfresh.promotion.platform.domain.model.funds.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.platform.domain.model.funds.FundsSettleInfoManager;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsSettleVO;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.FundsSettleInfoMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsSettleInfo;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsShopDayAmountSum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 */
@Service
@Slf4j
public class FundsSettleInfoManagerImpl extends ServiceImpl<FundsSettleInfoMapper, FundsSettleInfo>
        implements FundsSettleInfoManager {

    @Override
    public void insertChange(FundsSettleVO vo, Long accountId) {
        //验证订单金额
        if (null == vo.getRealPay() || vo.getRealPay().compareTo(new BigDecimal("0")) < 1) {
            return;
        }
        FundsSettleInfo settleInfo = new FundsSettleInfo();
        BeanUtils.copyProperties(vo, settleInfo);
        settleInfo.setAccountId(accountId);
        if(StringUtils.isNotBlank(vo.getCreateDay())) {
            settleInfo.setCreateDay(vo.getCreateDay());
        } else {
            settleInfo.setCreateDay(DateUtil.formatDate());
        }
        settleInfo.setGmtCreate(new Date());
        settleInfo.setGmtModified(new Date());
        baseMapper.insert(settleInfo);
    }

    @Override
    public FundsShopDayAmountSum queryDayAmount(Long accountId,Integer shopId, Integer shopType, String dayTime) {
        return baseMapper.queryDayAmount(accountId, shopId, shopType,dayTime);
    }

    @Override
    public List<FundsShopDayAmountSum> queryDatStatis(Integer shopId, Integer shopType, Date endTime) {
        return baseMapper.queryDayStatis(shopId, shopType, endTime);
    }

    @Override
    public List<FundsSettleInfo> queryDayNote(Long accountId, String createDay, Integer start, Integer length) {
        return baseMapper.queryDayNote(accountId, createDay, start, length);
    }

    @Override
    public Integer queryDayCount(Long accountId, String createDay) {
        return baseMapper.queryDayCount(accountId, createDay);
    }

    @Override
    public FundsSettleInfo queryDaySum(String createDay) {
        return baseMapper.queryDaySum(createDay);
    }

    @Override
    public List<FundsSettleInfo> queryDayTarget(String beginTime, String endTime, Integer shopType, Integer shopId, Long account, Integer start, Integer length) {
        return baseMapper.queryDayTarget(beginTime, endTime, shopType, shopId, account, start, length);
    }

    @Override
    public Integer queryDayTargetCount(String beginTime, String endTime, Integer shopType, Integer shopId, Long account) {
        return baseMapper.queryDayTargetCount(beginTime, endTime, shopType, shopId, account);
    }

    @Override
    public List<FundsSettleInfo> queryAccountStatis(String createDay, Integer start, Integer length) {
        return baseMapper.queryAccountStatis(createDay, start, length);
    }

    @Override
    public Integer queryAccountStatisCount(String createDay) {
        return baseMapper.queryAccountStatisCount(createDay);
    }

    @Override
    public List<FundsSettleInfo> queryAccountByShopList(String createDay, Integer start, Integer length, List<Integer> shopIdList, Integer shopType) {
        return baseMapper.queryAccountByShopList(createDay, start, length, shopIdList, shopType);
    }

    @Override
    public Integer queryCountByShopList(String createDay, List<Integer> shopIdList, Integer shopType) {
        return baseMapper.queryCountByShopList(createDay, shopIdList, shopType);
    }

    @Override
    public List<FundsSettleInfo> batchQueryDayTarget(String beginTime, String endTime, Integer shopType, Integer shopId, Long account, Integer start, Integer length) {
        return baseMapper.batchQueryDayTarget(beginTime, endTime, shopType, shopId, account, start, length);
    }

    @Override
    public Integer batchQueryDayTargetCount(String beginTime, String endTime, Integer shopType, Integer shopId, Long account) {
        return baseMapper.batchQueryDayTargetCount(beginTime, endTime, shopType, shopId, account);
    }

}
