package com.qmfresh.promotion.platform.domain.model.activity.cp.action;

import lombok.Data;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class PlanEventCpAction<T> implements CpAction {
    /**
     * 活动
     */
    private T activity;


    /**
     * 创建计划改价事件
     *
     * @param activity 计划改价活动
     * @param <T>      范型
     * @return 计划（创建）改价事件
     */
    public static <T> PlanEventCpAction<T> create(T activity) {
        PlanEventCpAction<T> planEventCpAction = new PlanEventCpAction<>();
        planEventCpAction.setActivity(activity);
        return planEventCpAction;
    }

}
