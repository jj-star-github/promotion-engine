package com.qmfresh.promotion.platform.domain.model.activity.cp;

import lombok.Getter;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public enum AuditStatus {
    /**
     * 待审核
     */
    TO_AUDIT(1),
    /**
     * 已审核
     */
    AUDIT_SUCCESS(2),
    /**
     * 审核失败
     */
    AUDIT_FAILED(3);
    Integer code;

    AuditStatus(Integer code) {
        this.code = code;
    }
}
