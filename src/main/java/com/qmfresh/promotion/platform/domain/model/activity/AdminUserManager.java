package com.qmfresh.promotion.platform.domain.model.activity;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface AdminUserManager {

    /**
     * 获取权限中的用户id
     *
     * @param userId 用户id
     * @return 后台用户体系
     */
    AdminUser adminUser(Integer userId);
}
