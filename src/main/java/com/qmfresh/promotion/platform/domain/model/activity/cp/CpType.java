package com.qmfresh.promotion.platform.domain.model.activity.cp;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Slf4j
public enum CpType {

    /**
     * 差值
     */
    DIFF(1),
    /**
     * 折扣
     */
    DISCOUNT(2),
    /**
     * 目标值
     */
    TARGET(3);

    private Integer code;

    CpType(Integer code) {
        this.code = code;
    }


    public static CpType from(Integer i) {
        for (CpType cpType : CpType.values()) {
            if (cpType.getCode().equals(i)) {
                return cpType;
            }
        }
        log.warn("CpType异常：{}", i);
        return null;
    }
}
