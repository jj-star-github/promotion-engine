package com.qmfresh.promotion.platform.domain.valueobj.funds;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@ToString
public class FundsOrderTransactionVO extends FundsTransactionVO {

    private static final long serialVersionUID = 1L;

    /**
     * 目标商户ID 产生变更的门店ID
     */
    private Integer targetShop;

    /**
     * 活动ID  businessType= 3 时使用
     */
    private Long activityId;

    /**
     * SKU是否参与周清 1：是 0：否
     */
    private Integer isSkuClean;

    /**
     * 商品实际支付金额
     */
    private BigDecimal businessRealAmount;

    /**
     * 商品的促销金额
     */
    private BigDecimal businessMarketingAmount;


}
