package com.qmfresh.promotion.platform.domain.service;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.promotion.PromotionSsu;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.dto.PromotionSsuContext;
import com.qmfresh.promotion.dto.User;
import com.qmfresh.promotion.enums.ChannelTypeEnums;
import com.qmfresh.promotion.enums.PriceTypeEnums;
import com.qmfresh.promotion.platform.domain.service.vo.CouponPreferentialBean;
import com.qmfresh.promotion.platform.domain.shared.BigDecimalUtil;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromExecuteStrategyDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

/**
 */
@Component
@Slf4j
public class SkuPriceServiceImpl implements SkuPriceService {

    @Override
    public void fixOfflinePrice(PromExecuteStrategyDTO protocol, String traceId) {
        if (CollectionUtils.isEmpty(protocol.getPromotionSsuList())) {
            log.warn("购物车价格-商品列表为空 traceId={}", traceId);
            throw new BusinessException("商品列表不可为空");
        }
        //线下渠道处理商品价格
        if (protocol.getChannel().equals(ChannelTypeEnums.C_OFFLINE.getCode())) {
            //
            List<PromotionSsu> promotionSsuList = fixPrice(protocol.getUser(), protocol.getPromotionSsuList(), traceId);
            if (CollectionUtils.isEmpty(promotionSsuList)) {
                log.warn("购物车价格-商品价格存在异常 traceId={} ssu={}", traceId, JSON.toJSONString(protocol.getPromotionSsuList()));
                throw new BusinessException("商品价格存在异常");
            }
            protocol.setPromotionSsuList(promotionSsuList);
        }
        return;
    }

    /**
     * 算价 根据目标品计算
     * 1. 优惠券金额均摊
     * 2. 番茄币抵扣金额均摊
     * 3. 抹零金额均摊
     *
     * 每一项均摊的计算逻辑   目标品所占比例*总优惠金额
     *      验证 ： 已均摊金额是否大于或等于总优惠金额 （用来保证不可）
     *     计算占比：       ratio =（目标品的终价*重量）/SUM(目标品的终价*重量)    保留6位有效小数
     *     计算所占优惠金额：preferential = ratio*总优惠金额      四舍五入保留两位有效小数
     *     计算是否已超出：（均摊金额+已均摊金额）>总优惠金额？总优惠金额-已均摊金额 ： 均摊金额
     *  兜底
     *     计算处所有单项优惠金额的和是否小于总优惠金额，如果小于则随机分摊给一个品
     *
     * @param context
     * @param protocol
     * @param traceId
     */
    @Override
    public void realPrice(PromotionContext context, PromExecuteStrategyDTO protocol, CouponPreferentialBean couponPreferential, String traceId) {
        BigDecimal orderShouldPrice = BigDecimal.ZERO; //整单应付金额
        BigDecimal couponAmount = BigDecimal.ZERO;     //优惠券目标品应付金额
        //计算总订单应付金额  四舍五入精确到小数点后两位
        for (PromotionSsuContext ssuContext : context.getSsuContexts()) {
            BigDecimal realPrice =  BigDecimalUtil.mul(ssuContext.getAmount(), ssuContext.getPreferentialPrice());
            orderShouldPrice = orderShouldPrice.add(realPrice);
            //验证是否为优惠券目标品
            if (null != couponPreferential && couponPreferential.getUseCouponSkuId().contains(ssuContext.getSkuId())) {
                couponAmount = couponAmount.add(realPrice);
            }
        }

        //计算优惠券优惠总金额   折扣券要计算使用优惠最小原则
        BigDecimal couponPreferentialMoney = BigDecimal.ZERO;
        if(null != couponPreferential) {
            couponPreferentialMoney = couponPreferential.getCouponPreferentialMoney();
            if (CouponReduceTypeEnum.DISCOUNT.getCode() == couponPreferential.getPreferentialType()) {
                couponPreferentialMoney = BigDecimalUtil.mul(couponAmount, new BigDecimal(couponPreferential.getCouponDiscount()/100.0));
                couponPreferentialMoney = couponAmount.subtract(couponPreferentialMoney);
                couponPreferentialMoney = couponPreferentialMoney.compareTo(couponPreferential.getCouponPreferentialMoney()) <= 0 ? couponPreferentialMoney : couponPreferential.getCouponPreferentialMoney();
            }
        }

        BigDecimal orderCouponMoney = BigDecimal.ZERO;  //优惠券金额
        BigDecimal orderCoinToMoney = BigDecimal.ZERO;  //番茄币优惠金额
        BigDecimal orderFreeMoney = BigDecimal.ZERO;   //抹零优惠金额
        for(PromotionSsuContext ssuContext : context.getSsuContexts()) {
            BigDecimal realPrice = BigDecimalUtil.mul(ssuContext.getAmount(), ssuContext.getPreferentialPrice());
            ssuContext.setFinalMoney(realPrice);
            BigDecimal couponMoney = BigDecimal.ZERO;
            BigDecimal coinToMoney = BigDecimal.ZERO;
            BigDecimal freeMoney = BigDecimal.ZERO;
            BigDecimal ssuRealMoney = realPrice;
            //处理优惠券
            if(null != couponPreferential && couponPreferential.getUseCouponSkuId().contains(ssuContext.getSkuId())
                    && orderCouponMoney.compareTo(couponPreferentialMoney) < 0) {
                //计算占比
                BigDecimal couponRatio = realPrice.divide(couponAmount, 6, BigDecimal.ROUND_HALF_UP);
                couponMoney = couponPreferentialMoney.multiply(couponRatio).setScale(2,BigDecimal.ROUND_HALF_UP);
                //验证优惠金额是否超出
                couponMoney = orderCouponMoney.add(couponMoney).compareTo(couponPreferentialMoney) <= 0 ? couponMoney : couponPreferentialMoney.subtract(orderCouponMoney);
                //验证优惠是否大于单品所剩实付
                couponMoney = ssuRealMoney.compareTo(couponMoney) > 0 ? couponMoney : ssuRealMoney;
                ssuRealMoney = ssuRealMoney.subtract(couponMoney);
                orderCouponMoney = orderCouponMoney.add(couponMoney);
                ssuContext.setCouponMoney(couponMoney);
            }

            BigDecimal ratio = realPrice.divide(orderShouldPrice, 6, BigDecimal.ROUND_HALF_UP);
            //处理番茄币抵扣优惠
            if (protocol.getCoinToMoney().compareTo(BigDecimal.ZERO) > 0 &&
                    orderCoinToMoney.compareTo(protocol.getCoinToMoney()) < 0) {
                coinToMoney = BigDecimalUtil.mul(protocol.getCoinToMoney(), ratio);
                //验证优惠金额是否超出
                coinToMoney = orderCoinToMoney.add(coinToMoney).compareTo(protocol.getCoinToMoney()) <= 0 ? coinToMoney : protocol.getCoinToMoney().subtract(orderCoinToMoney);
                //验证优惠是否大于单品所剩实付
                coinToMoney = ssuRealMoney.compareTo(coinToMoney) > 0 ? coinToMoney : ssuRealMoney;
                ssuRealMoney = ssuRealMoney.subtract(coinToMoney);
                orderCoinToMoney = orderCoinToMoney.add(coinToMoney);
                ssuContext.setCoinToMoney(coinToMoney);
            }

            //处理抹零优惠
            if (protocol.getFreeMoney().compareTo(BigDecimal.ZERO) > 0 &&
                    orderFreeMoney.compareTo(protocol.getFreeMoney()) < 0) {
                freeMoney = BigDecimalUtil.mul(protocol.getFreeMoney(), ratio);
                freeMoney = orderFreeMoney.add(freeMoney).compareTo(protocol.getFreeMoney()) <= 0 ? freeMoney : protocol.getFreeMoney().subtract(orderFreeMoney);
                freeMoney = ssuRealMoney.compareTo(freeMoney) > 0 ? freeMoney : ssuRealMoney;
                orderFreeMoney = orderFreeMoney.add(freeMoney);
                ssuContext.setFreeMoney(freeMoney);
            }

            ssuContext.setRealMoney(realPrice.subtract(couponMoney).subtract(coinToMoney).subtract(freeMoney));
            //小计金额 原价*重量
            ssuContext.setOriginMoney(BigDecimalUtil.mul(ssuContext.getOriginPrice(), ssuContext.getAmount()));
            //单品优惠价格 （原价金额-终价金额）
            ssuContext.setPreferentialMoney(ssuContext.getOriginMoney().subtract(ssuContext.getFinalMoney()));
        }

        //误差处理
        if (couponPreferentialMoney.compareTo(orderCouponMoney) > 0
                || protocol.getCoinToMoney().compareTo(orderCoinToMoney) > 0 || protocol.getFreeMoney().compareTo(orderFreeMoney) > 0) {
            for (PromotionSsuContext ssuContext : context.getSsuContexts()) {

                BigDecimal skuRealMoney = ssuContext.getRealMoney();

                BigDecimal couponErrorMoney = couponPreferentialMoney.subtract(orderCouponMoney);
                if (couponErrorMoney.compareTo(BigDecimal.ZERO) > 0 && skuRealMoney.compareTo(BigDecimal.ZERO) > 0
                        && couponPreferential.getUseCouponSkuId().contains(ssuContext.getSkuId())) {// 优惠券误差
                    BigDecimal errorMoney = skuRealMoney.compareTo(couponErrorMoney) >= 0 ? couponErrorMoney : skuRealMoney;
                    orderCouponMoney = orderCouponMoney.add(errorMoney);
                    skuRealMoney = skuRealMoney.subtract(errorMoney);
                    ssuContext.setCouponMoney(ssuContext.getCouponMoney().add(errorMoney));
                }

                BigDecimal coinToErrorMoney = protocol.getCoinToMoney().subtract(orderCoinToMoney);
                if (coinToErrorMoney.compareTo(BigDecimal.ZERO) > 0 && skuRealMoney.compareTo(BigDecimal.ZERO) > 0) { //番茄币误差
                    BigDecimal errorMoney = skuRealMoney.compareTo(coinToErrorMoney) >= 0 ? coinToErrorMoney : skuRealMoney;
                    skuRealMoney = skuRealMoney.subtract(errorMoney);
                    orderCoinToMoney = orderCoinToMoney.add(errorMoney);
                    ssuContext.setCoinToMoney(ssuContext.getCoinToMoney().add(errorMoney));
                }

                BigDecimal freeErrorMoney = protocol.getFreeMoney().subtract(orderFreeMoney);
                if (freeErrorMoney.compareTo(BigDecimal.ZERO) > 0  && skuRealMoney.compareTo(BigDecimal.ZERO) > 0) { //抹零误差
                    BigDecimal errorMoney = skuRealMoney.compareTo(freeErrorMoney) >= 0 ? freeErrorMoney : skuRealMoney;
                    orderFreeMoney = orderFreeMoney.add(errorMoney);
                    skuRealMoney = skuRealMoney.subtract(errorMoney);
                    ssuContext.setFreeMoney(ssuContext.getFreeMoney().add(errorMoney));
                }
                ssuContext.setRealMoney(skuRealMoney);
            }
        }

        context.setOrderCouponMoney(orderCouponMoney);
        context.setOrderCoinToMoney(orderCoinToMoney);
        context.setOrderFreeMoney(orderFreeMoney);
    }

    /**
     * 线下购物车商品信息处理
     * 确定最小价格 合并原价和会员价商品
     * @param user
     * @param cartSsu
     * @param traceId
     * @return
     */
    private List<PromotionSsu> fixPrice(User user, List<PromotionSsu> cartSsu, String traceId) {
        List<PromotionSsu> mergedPromotionSsu = new ArrayList<>();
        //根据SKUID分组
        Map<Integer, List<PromotionSsu>> skuCartMap = cartSsu.stream().collect(Collectors.groupingBy(PromotionSsu::getSkuId));
        //确定价格并合并原价和会员价商品
        for (Map.Entry<Integer, List<PromotionSsu>> entry : skuCartMap.entrySet()) {
            mergedPromotionSsu.addAll(user.getUserId() == 0 ? this.noVipPrice(entry.getValue()) : this.vipPrice(entry.getValue()));
        }
        return mergedPromotionSsu;
    }

    /**
     * 非会员合并 原价数据
     * @param promotionSsus
     * @return
     */
    private List<PromotionSsu> noVipPrice(List<PromotionSsu> promotionSsus) {
        List<PromotionSsu> psList = new ArrayList<>();
        PromotionSsu normalPromotionSsu = new PromotionSsu(promotionSsus.get(0));
        for (PromotionSsu cs : promotionSsus) {
            //验证价格是否异常
            if (cs.getOriginPrice().compareTo(BigDecimal.ZERO) <= 0 && cs.getPriceAfterDiscount().compareTo(BigDecimal.ZERO) <= 0) {
                log.warn("商品价格异常 sku={}", JSON.toJSONString(cs));
                throw new BusinessException("商品价格异常");
            }
            //获取非0 的最小价格
            if(cs.getOriginPrice().compareTo(BigDecimal.ZERO) <= 0) {
                cs.setPriceAfterDiscount(cs.getPriceAfterDiscount());
            } else if (cs.getPriceAfterDiscount().compareTo(BigDecimal.ZERO) <= 0) {
                cs.setPriceAfterDiscount(cs.getOriginPrice());
            } else {
                //计算最小价格
                cs.setPriceAfterDiscount(cs.getOriginPrice().compareTo(cs.getPriceAfterDiscount()) <= 0 ? cs.getOriginPrice() : cs.getPriceAfterDiscount());
            }

            // 合并原价商品
            if (cs.getPriceAfterDiscount().compareTo(cs.getOriginPrice()) == 0) {
                normalPromotionSsu.setAmount(normalPromotionSsu.getAmount().add(cs.getAmount()));
                normalPromotionSsu.setPriceType(PriceTypeEnums.NORMAL.getCode());
                normalPromotionSsu.setPriceAfterDiscount(cs.getPriceAfterDiscount());
                continue;
            }
            cs.setPriceType(PriceTypeEnums.MODIFY.getCode());
            psList.add(cs);
        }
        if (normalPromotionSsu.getAmount().compareTo(BigDecimal.ZERO) > 0) {
            psList.add(normalPromotionSsu);
        }
        return psList;
    }

    /**
     * 合并原价和会员价
     * @param promotionSsus
     * @return
     */
    private List<PromotionSsu> vipPrice(List<PromotionSsu> promotionSsus) {
        List<PromotionSsu> psList = new ArrayList<>(promotionSsus.size());
        PromotionSsu vipPromotionSsu = new PromotionSsu(promotionSsus.get(0));
        PromotionSsu normalPromotionSsu = new PromotionSsu(promotionSsus.get(0));
        for (PromotionSsu cs : promotionSsus) {
            //验证价格是否异常
            if (cs.getOriginPrice().compareTo(BigDecimal.ZERO) <= 0 && cs.getPriceAfterDiscount().compareTo(BigDecimal.ZERO) <= 0
                    && cs.getVipPrice().compareTo(BigDecimal.ZERO) <= 0) {
                log.warn("商品价格异常 sku={}", JSON.toJSONString(cs));
                throw new BusinessException("商品价格异常");
            }
            //获取非0 的最小价格
            BigDecimal price = BigDecimal.ZERO;
            if (cs.getOriginPrice().compareTo(BigDecimal.ZERO) > 0) {
                price = cs.getOriginPrice();
            }
            if (cs.getVipPrice().compareTo(BigDecimal.ZERO) > 0) {
                price = price.compareTo(BigDecimal.ZERO) == 0 ? cs.getVipPrice() : price.compareTo(cs.getVipPrice()) <= 0 ? price : cs.getVipPrice();
            }
            if (cs.getPriceAfterDiscount().compareTo(BigDecimal.ZERO) > 0) {
                price = price.compareTo(BigDecimal.ZERO) == 0 ? cs.getPriceAfterDiscount() : price.compareTo(cs.getPriceAfterDiscount()) <= 0 ? price : cs.getPriceAfterDiscount();
            }

            //验证是否为原价商品
            if(price.compareTo(cs.getOriginPrice()) == 0) {
                normalPromotionSsu.setAmount(normalPromotionSsu.getAmount().add(cs.getAmount()));
                normalPromotionSsu.setPriceType(PriceTypeEnums.NORMAL.getCode());
                normalPromotionSsu.setPriceAfterDiscount(price);
                continue;
            }

            //验证是否为会员价
            if(price.compareTo(cs.getVipPrice()) == 0) {
                vipPromotionSsu.setAmount(vipPromotionSsu.getAmount().add(cs.getAmount()));
                vipPromotionSsu.setPriceType(PriceTypeEnums.VIP.getCode());
                vipPromotionSsu.setPriceAfterDiscount(price);
                continue;
            }
            cs.setPriceType(PriceTypeEnums.MODIFY.getCode());
            cs.setPriceAfterDiscount(price);
            psList.add(cs);
            continue;
        }
        if (vipPromotionSsu.getAmount().compareTo(BigDecimal.ZERO) > 0) {
            psList.add(vipPromotionSsu);
        }

        if (normalPromotionSsu.getAmount().compareTo(BigDecimal.ZERO) > 0) {
            psList.add(normalPromotionSsu);
        }
        return psList;
    }
}
