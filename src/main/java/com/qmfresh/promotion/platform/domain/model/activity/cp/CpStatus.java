package com.qmfresh.promotion.platform.domain.model.activity.cp;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Slf4j
public enum CpStatus {

    /**
     * 待进行
     */
    CREATE(1),
    /**
     * 改价中
     */
    CHANGING_PRICE(2),
    /**
     * 运行中
     */
    RUNNING(3),
    /**
     * 已取消
     */
    CANCELED(4),
    /**
     * 关闭中
     */
    CLOSING(5),
    /**
     * 结束中
     */
    FINISHING(6),
    /**
     * 已关闭
     */
    CLOSED(7),
    /**
     * 已结束(expired)
     */
    FINISHED(8);


    private int code;


    CpStatus(Integer code) {
        this.code = code;
    }

    public static CpStatus from(Integer status) {
        for (CpStatus cpStatus : CpStatus.values()) {
            if (cpStatus.getCode() == status) {
                return cpStatus;
            }
        }
        log.warn("status不在系统：{}", status);
        return null;
    }

    public static String statusDesc(CpStatus cpStatus) {
        if (CREATE.equals(cpStatus)) {
            return STATUS_DESC_CREATE;
        }
        if (RUNNING.equals(cpStatus)) {
            return STATUS_DESC_RUNNING;
        }

        if (CLOSED.equals(cpStatus)
                || CANCELED.equals(cpStatus)) {
            return STATUS_DESC_CLOSED;
        }
        //结束(FINISHED)
        return STATUS_DESC_EXPIRED;
    }

    private static final String STATUS_DESC_CREATE = "待生效";
    private static final String STATUS_DESC_RUNNING = "进行中";
    private static final String STATUS_DESC_EXPIRED = "已失效";
    private static final String STATUS_DESC_CLOSED = "已关闭";


    /**
     * 0=全部 1=待生效 2=进行中 3=已失效
     *
     * @param status 状态
     * @return 状态列表
     */
    public static List<Integer> status(Integer status) {
        List<Integer> statusList = new ArrayList<>();
        if (status == null || status == 0) {
        } else {
            if (status == 1) {
                statusList.add(CpStatus.CREATE.getCode());
            }
            if (status == 2) {
                statusList.add(CpStatus.RUNNING.getCode());
            }
            if (status == 3) {
                statusList.add(CpStatus.FINISHED.getCode());
                statusList.add(CpStatus.CLOSED.getCode());
            }
        }
        return statusList;
    }


}
