package com.qmfresh.promotion.platform.domain.model.activity;

import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface SkuManager {

    /**
     * 获取商品信息（带有价格，类目）
     *
     * @param skuId skuId
     * @return
     */
    Sku sku(Integer skuId);

    /**
     * 查询门店的产品
     *
     * @param shopId 门店id
     * @param skuId  商品id
     * @return 商品
     */
    SkuPrice shopSkuPrice(Integer shopId, Integer skuId);

    /**
     * 查询门店价格类比
     *
     * @param shopId 门店id
     * @param skuIds 门店商品ids
     * @return
     */
    List<SkuPrice> shopSkuPrice(Integer shopId, List<Integer> skuIds);

    /**
     * @param skuIds 商品列表
     * @return
     */
    List<Sku> sku(List<Integer> skuIds);

    /**
     * 商品库存量
     *
     * @param shopId
     * @param skuIds
     * @return
     */
    List<SkuExt> skuExt(Integer shopId, List<Integer> skuIds);
}
