package com.qmfresh.promotion.platform.domain.model.activity.cp.area.command;

import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.SkuCpItem;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class AreaCpCreateCommand implements Serializable {
    private Operator operator;
    private String activityName;
    private CpTimeCycle timeCycle;
    private List<Integer> shopIds;
    private List<SkuCpItem> skuCpItems;
    private Long marketingAccountId;
    private Long activityId;


    public static AreaCpCreateCommand create(Operator operator,
                                             String activityName,
                                             CpTimeCycle timeCycle,
                                             List<Integer> shopIds,
                                             List<SkuCpItem> skuCpItems) {
        return create(operator, activityName, timeCycle, shopIds, skuCpItems, null);
    }

    public static AreaCpCreateCommand create(Operator operator,
                                             String activityName,
                                             CpTimeCycle timeCycle,
                                             List<Integer> shopIds,
                                             List<SkuCpItem> skuCpItems,
                                             Long activityId
    ) {
        if (operator == null) {
            throw new BusinessException("必须设置操作人");
        }
        if (StringUtils.isBlank(activityName)) {
            throw new BusinessException("必须设置活动名称");
        }
        if (timeCycle == null) {
            throw new BusinessException("活动周期不能为空");
        }
        if (CollectionUtils.isEmpty(shopIds)) {
            throw new BusinessException("门店列表不能为空");
        }
        if (CollectionUtils.isEmpty(skuCpItems)) {
            throw new BusinessException("改价信息不能为空");
        }

        AreaCpCreateCommand areaCpQueryCommand = AreaCpCreateCommand.builder()
                .operator(operator)
                .activityName(activityName)
                .timeCycle(timeCycle)
                .shopIds(shopIds)
                .skuCpItems(skuCpItems)
                .activityId(activityId)
                .build();
        return areaCpQueryCommand;
    }


    public static void check(CpBizType cpBizType) {

    }
}
