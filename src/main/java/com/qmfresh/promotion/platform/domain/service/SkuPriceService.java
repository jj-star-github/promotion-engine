package com.qmfresh.promotion.platform.domain.service;

import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.platform.domain.service.vo.CouponPreferentialBean;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityCampOnDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromExecuteStrategyDTO;

import java.util.List;

/**
 * 商品算价服务
 */
public interface SkuPriceService {

    /**
     * 下单之前算价
     * @param protocol
     * @return
     */
    void fixOfflinePrice(PromExecuteStrategyDTO protocol, String traceId);

    /**
     * 计算实付
     * @param context
     * @param protocol
     * @param traceId
     */
    void realPrice (PromotionContext context, PromExecuteStrategyDTO protocol, CouponPreferentialBean couponPreferential, String traceId);

}
