package com.qmfresh.promotion.platform.domain.model.promactivity.enums;


import lombok.Getter;

/**
 * 门店类型
 */
@Getter
public enum ShopTypeEnum {

    /**
     * 总部
     */
    ZB(1),
    /**
     * 大区
     */
    DQ(2),
    /**
     * 片区
     */
    PQ(3),
    /**
     * 门店
     */
    MD(4),
    ;
    private Integer code;

    ShopTypeEnum(Integer code) {
        this.code = code;
    }
}
