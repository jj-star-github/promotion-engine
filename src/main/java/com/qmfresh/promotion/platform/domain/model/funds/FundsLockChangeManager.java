package com.qmfresh.promotion.platform.domain.model.funds;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsLockVO;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsLockChange;

import java.util.List;

/**
 * 费用账户
 */
public interface FundsLockChangeManager extends IService<FundsLockChange> {

    /**
     * 新增锁定费用流水
     * @param vo     锁定费用信息
     * @param accountId   账户信息
     */
    void insertChange(FundsLockVO vo, Long accountId);

    /**
     * 根据门店类型维度查询锁定费用的日统计
     * @param createDay
     * @return
     */
    List<FundsLockChange> queryDaySumSupportShopType(String createDay);

    /**
     * 查询日统计
     * @param accountId
     * @param createDayList
     * @return
     */
    List<FundsLockChange> queryDaySumSupportAccount(Long accountId, List<String> createDayList);

    /**
     *
     * @param accountId
     * @param createDay
     * @return
     */
    FundsLockChange queryDaySumSupportAccount(Long accountId, String createDay);

    /**
     * 账户日统计
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return
     */
    List<FundsLockChange> queryDaySumSupportAccount(List<Long> accountId, String beginTime, String endTime);

}
