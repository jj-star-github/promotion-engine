package com.qmfresh.promotion.platform.domain.valueobj.shop;

import lombok.Getter;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public class SkuVO implements Serializable {

    /**
     * 商品id
     */
    private Integer skuId;
    /**
     * 商品名称
     */
    private String skuName;
    /**
     * 商品格式
     */
    private String skuFormat;

    public static SkuVO create(Integer skuId, String skuName, String skuFormat) {
        SkuVO skuVO = new SkuVO();
        skuVO.skuId = skuId;
        skuVO.skuName = skuName;
        skuVO.skuFormat = skuFormat;
        return skuVO;
    }
}
