package com.qmfresh.promotion.platform.domain.model.settle.rule;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopPromotion;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopPromotionRule;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto.*;

import java.util.List;

/**
 * @Author zbr
 * @Date 15:43 2020/8/22
 */
public interface ShopRuleManager extends IService<ShopPromotionRule> {

    /**
     * 创建门店营销规则
     */
    Boolean createShopRule(ShopRuleReqDTO param);

    /**
     * 查询门店规则列表
     */
    List<ShopPromotion> queryRuleInfoList(QueryRuleInfoReqDTO param);

    /**
     * 根据id查询门店规则详情
     */
    ShopPromotionRule queryRuleDetailInfo(IdReqDto id);

    /**
     * 修改门店规则
     */
    Boolean modifyShopRule(UpdateShopRuleReqDTO param);

    /**
     * 根据等级查询门店规则详情
     */
    ShopPromotionRule queryRuleType(QueryRuleInfoReqDTO param);

    /**
     * 根据shopId查询门店规则
     */
    ShopWeekDTO queryRuleByShopId(ShopIdDTO shopId);
}
