package com.qmfresh.promotion.platform.domain.model.activity.cp.log;

import com.qmfresh.promotion.platform.domain.model.activity.cp.log.command.CpCreateLogCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.command.CpCreateLogQueryCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.command.CpCreateLogStartCommand;
import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.CpCreateLogPO;

/**
 * 改价操作日志
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface CpCreateLogManager {

    /**
     * 创建改价创建日志
     *
     * @param createLogCreateCommand 命令
     */
    CpCreateLog create(CpCreateLogCreateCommand createLogCreateCommand);


    /**
     * 分页查询创建改价日志，创建时间倒排
     *
     * @param cpCreateLogQueryCommand 改价日志查询命令
     * @return 改价创建日志
     */
    Page<CpCreateLog> pageCpCreateLog(CpCreateLogQueryCommand cpCreateLogQueryCommand);

    /**
     * 操作日志
     *
     * @param id 操作日志id
     * @return 改价创建日志
     */
    CpCreateLog get(Long id);

    /**
     * 定时启动改价创建日志job
     */
    void startCpCreateLogJob();

    /**
     * 执行单条创建日志
     *
     * @param cpCreateLog 改价创建日志
     */
    void execute(CpCreateLogPO cpCreateLog, CpCreateLogStartCommand command);

    void updateCpCreateLog(Long updateId,
                                  CpCreateLogCompletedStatus cpCreateLogCompletedStatus,
                                  String remark);

}
