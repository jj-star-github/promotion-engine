package com.qmfresh.promotion.platform.domain.model.funds;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsOrderTransactionVO;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsTransactionVO;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 账户更新流水
 */
public interface FundsChangeNoteManager extends IService<FundsChangeNote> {

    /**
     * 新增流水
     * @param vo
     * @param account
     * @return
     */
    void insertChange(FundsTransactionVO vo, FundsAccount account);

    /**
     * 订单操作-新增流水
     * @param orderTransaction
     * @param account
     * @return
     */
    void insertChange(List<FundsOrderTransactionVO> orderTransaction, FundsAccount account, BigDecimal changeAmount, String createDay);

    /**
     * 查询门店费用支出商品信息
     * @param accountId   账号ID
     * @param createDay   查询日期
     * @return
     */
    List<FundsChangeNote> querySkuBuAccount(Long accountId, String createDay);

    /**
     * 查询账户支出明细
     * @param accountId      账户ID
     * @param createDay     创建时间
     * @param businessTypeList  业务类型（不传查询 businessType in (3,5,8)）
     * @param businessId    业务ID主要再 businessType=3时 传入SKU-ID
     * @return
     */
    List<FundsChangeNote> queryPayNote(Long accountId, String createDay, List<Integer> businessTypeList, List<String> businessId, Integer start, Integer length);

    /**
     *
     * @param accountId
     * @param createDay
     * @param businessType
     * @param businessId
     * @return
     */
    Integer queryPayNoteCount(Long accountId, String createDay, Integer businessType, String businessId);

    /**
     * 总部支出流水 活动维度
     * @param accountId
     * @param createDay
     * @param start
     * @param length
     * @return
     */
    List<FundsChangeNote> queryZbPayStatis(Long accountId, String createDay, Integer start, Integer length);

    /**
     *  查询总部支出总数
     * @param accountId
     * @param createDay
     * @return
     */
    Integer queryZbPayStatisCount(Long accountId, String createDay);

    /**
     * 日流水统计
     * @param accountId
     * @param beginTime
     * @param endTime
     * @param start
     * @param length
     * @return
     */
    List<FundsShopDayChangeStatis> shopDayNoteStatis(Long accountId, String beginTime, String endTime, Integer shopType, Integer start, Integer length);

    /**
     * 日流水总数
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return
     */
    Integer shopDayNoteStatisCount(Long accountId, String beginTime, String endTime, Integer shopType);

    /**
     * 周GMV统计
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return
     */
    FundsChangeNoteGMVSum sumGMV(Long accountId, String beginTime, String endTime);

    /**
     * 根据账户查询多对应的目标账户ID
     * @param accountId
     * @param createDay
     * @return
     */
    List<Integer> queryTargetShopByAccountId(Long accountId, String createDay);

    /**
     * 查询门店去化奖励
     * @param accountId
     * @param endTime
     * @return
     */
    List<FundsWeekClear> queryWeekClear(Long accountId, Date endTime);

    /**
     * 查询门店日去化奖励
     * @param accountId
     * @param createDay
     * @return
     */
    FundsWeekClear queryDayWeekClear(Long accountId, String createDay);

    /**
     * 根据去化奖励标识查询
     * @param businessCode
     * @return
     */
    FundsChangeNote queryByWeekClear(String businessCode);

    /**
     * 后台查询门店日去化奖励
     * @param accountId
     * @param createDay
     * @return
     */
    List<FundsChangeNote> queryWeekClearChangeNote(Long accountId, String createDay,Integer start, Integer length);

    /**
     * 统计后台查询门店日去化奖励
     * @param accountId
     * @param createDay
     * @return
     */
    Integer countWeekClearChangeNote(Long accountId, String createDay);
}
