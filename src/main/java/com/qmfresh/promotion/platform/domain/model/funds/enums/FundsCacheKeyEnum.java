package com.qmfresh.promotion.platform.domain.model.funds.enums;


/**
 * 费用缓存key
 */
public enum FundsCacheKeyEnum {

    /**
     * 根据门店信息查询账户IDRedisTemplate
     */
    ACCOUNTID_SHOPID_TYPE,
    ;
    private final String value;

    FundsCacheKeyEnum() {
        this.value = "FUNDS:" + name();
    }

    public String key() {
        return value;
    }

    public String key(Object... params) {
        StringBuilder key = new StringBuilder(value);
        if (params != null && params.length > 0) {
            for (Object param : params) {
                key.append(':');
                key.append(String.valueOf(param));
            }
        }
        return key.toString();
    }

}
