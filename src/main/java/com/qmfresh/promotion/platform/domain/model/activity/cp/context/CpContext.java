package com.qmfresh.promotion.platform.domain.model.activity.cp.context;

import com.qmfresh.promotion.platform.domain.shared.Page;

import java.util.List;

/**
 * 上下文中去做聚合的事情
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface CpContext<T> {

    /**
     * 创建改价
     *
     * @param cpContext 改价上下文
     */
    T create(CpContext<T> cpContext);

    /**
     * 启动改价
     *
     * @param cpContext 启动改价
     */
    List<T> start(CpContext<T> cpContext);


    /**
     * 关闭改价
     *
     * @param cpContext
     */
    T close(CpContext<T> cpContext);


    Page<T> page(CpContext<T> cpContext);


}
