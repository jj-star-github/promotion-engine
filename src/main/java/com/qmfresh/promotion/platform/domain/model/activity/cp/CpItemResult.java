package com.qmfresh.promotion.platform.domain.model.activity.cp;

import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ActivityPO;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Builder
@Getter
public class CpItemResult implements Serializable {

    private Long activityId;
    private List<CpItem> cpItemList;
    private String remark;

    public static CpItemResult create(Long activityId, List<CpItem> cpItems, String remark) {
        CpItemResult cpItemResult = CpItemResult.builder()
                .activityId(activityId)
                .cpItemList(cpItems)
                .remark(remark)
                .build();
        return cpItemResult;
    }

    public static CpItemResult create(ActivityPO activityPO, CpBizType cpBizType, List<CpItem> items, String remark) {

        if (cpBizType == null) {
            throw new BusinessException("必须设置改价类型");
        }

        Long activityId = null;
        if (activityPO == null) {
            remark = remark + cpBizType.name() + "未创建改价活动";
        } else {
            activityId = activityPO.getId();
        }

        return CpItemResult.builder()
                .remark(remark)
                .activityId(activityId)
                .cpItemList(items)
                .build();
    }
}
