package com.qmfresh.promotion.platform.domain.model.activity.cp.context;

import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.action.CpAction;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpActivity;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpActivityManager;
import com.qmfresh.promotion.platform.domain.shared.Page;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@Slf4j
public class PlatformCpContext implements CpContext<GeneralCpActivity> {

    private Operator operator;
    private List<Integer> shopIds;
    private List<CpAction> cpActions;

    private GeneralCpActivityManager generalCpActivityManager;

    @Override
    public GeneralCpActivity create(CpContext<GeneralCpActivity> cpContext) {



        return null;

    }

    @Override
    public List<GeneralCpActivity> start(CpContext<GeneralCpActivity> cpContext) {

        return null;

    }

    @Override
    public GeneralCpActivity close(CpContext<GeneralCpActivity> cpContext) {
        return null;

    }

    @Override
    public Page<GeneralCpActivity> page(CpContext<GeneralCpActivity> cpContext) {
        return null;
    }
}
