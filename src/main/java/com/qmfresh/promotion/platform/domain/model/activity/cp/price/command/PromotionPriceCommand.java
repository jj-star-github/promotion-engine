package com.qmfresh.promotion.platform.domain.model.activity.cp.price.command;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qmfresh.promotion.platform.domain.model.activity.cp.AuditStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.AreaCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import com.qmfresh.promotion.platform.domain.model.activity.cp.vip.VipCpActivity;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.ChangePriceActivityMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ChangePriceActivityPO;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
@Slf4j
public class PromotionPriceCommand implements Serializable {

    /**
     * 原价
     */
    private BigDecimal originalPrice;
    /**
     * 当前门店活动
     */
    private ShopCpActivity shopCpActivity;
    /**
     * 当前总部活动
     */
    private GeneralCpItem generalCpItem;
    /**
     * 当前大区活动
     */
    private AreaCpItem areaCpItem;
    private VipCpActivity vipCpActivity;

    private Integer shopId;
    private Integer skuId;


    /**
     * 创建促销价格
     *
     * @param originalPrice 原价
     * @param shopId
     * @param skuId
     * @return 促销价计算命令
     */
    public static PromotionPriceCommand createWithoutActivity(BigDecimal originalPrice, Integer shopId, Integer skuId) {
        return PromotionPriceCommand.builder()
                .shopId(shopId)
                .skuId(skuId)
                .originalPrice(originalPrice)
                .build();
    }


    public static PromotionPriceCommand create(BigDecimal originalPrice,
                                               ShopCpActivity shopCpActivity,
                                               GeneralCpItem generalCpItem, Integer shopId, Integer skuId) {

        if (originalPrice == null || originalPrice.doubleValue() < 0) {
            throw new BusinessException("商品原价必须大于等于零");
        }

        PromotionPriceCommandBuilder commandBuilder = PromotionPriceCommand.builder()
                .originalPrice(originalPrice)
                .shopId(shopId)
                .skuId(skuId)
                .shopCpActivity(shopCpActivity)
                .generalCpItem(generalCpItem);
        if (shopCpActivity != null) {
            builder().skuId(shopCpActivity.getSku().getSkuId());
            builder().shopId(shopCpActivity.getShop().getShopId());
        }
        if (generalCpItem != null) {
            builder().skuId(generalCpItem.getSku().getSkuId());
            builder().shopId(generalCpItem.getShop().getShopId());
        }
        return commandBuilder.build();
    }


    /**
     * 创建当前促销命令
     *
     * @param originalPrice
     * @param shopId
     * @param skuId
     * @param includeId
     * @param changePriceActivityMapper
     * @return
     */
    public static PromotionPriceCommand createCurrent(BigDecimal originalPrice,
                                                      Integer shopId, Integer skuId,
                                                      Long includeId,
                                                      ChangePriceActivityMapper changePriceActivityMapper) {


        //改价活动列表
        List<ChangePriceActivityPO> changePriceActivityPOS = changePriceActivityMapper.selectList(new LambdaQueryWrapper<ChangePriceActivityPO>()
                .eq(ChangePriceActivityPO::getIsDeleted, 0)
                .eq(ChangePriceActivityPO::getShopId, shopId)
                .eq(ChangePriceActivityPO::getSkuId, skuId)
                .eq(ChangePriceActivityPO::getStatus, CpStatus.RUNNING.getCode())
                .eq(ChangePriceActivityPO::getAuditStatus, AuditStatus.AUDIT_SUCCESS.getCode())
        );

        ChangePriceActivityPO changePriceActivityPO = changePriceActivityMapper.selectById(includeId);

        ShopCpActivity shopCpActivity;
        GeneralCpItem generalCpItem;
        if (CollectionUtils.isNotEmpty(changePriceActivityPOS)) {
            Map<Integer, List<ChangePriceActivityPO>> map = changePriceActivityPOS.stream().collect(Collectors.groupingBy(ChangePriceActivityPO::getType, Collectors.toList()));
            shopCpActivity = CollectionUtils.isEmpty(map.get(CpBizType.CP_SHOP.getCode())) ? null : ChangePriceActivityPO.shopCpActivity(map.get(CpBizType.CP_SHOP.getCode()).get(0));
            generalCpItem = CollectionUtils.isEmpty(map.get(CpBizType.CP_PLATFORM.getCode())) ? null : ChangePriceActivityPO.generalCpItem(map.get(CpBizType.CP_PLATFORM.getCode()).get(0));

            if (changePriceActivityPO != null) {
                if (CpBizType.CP_PLATFORM.getCode().equals(changePriceActivityPO.getType())) {
                    generalCpItem = ChangePriceActivityPO.generalCpItem(changePriceActivityPO);
                }
                //替换当前进行的
                if (CpBizType.CP_SHOP.getCode().equals(changePriceActivityPO.getType())) {
                    shopCpActivity = ChangePriceActivityPO.shopCpActivity(changePriceActivityPO);
                }
            }

            return PromotionPriceCommand.create(originalPrice, shopCpActivity, generalCpItem, shopId, skuId);
        } else {
            if (changePriceActivityPO != null) {
                return PromotionPriceCommand.create(originalPrice, null, ChangePriceActivityPO.generalCpItem(changePriceActivityPO), shopId, skuId);
            }

            return PromotionPriceCommand.create(originalPrice, null, null, shopId, skuId);
        }
    }


    /**
     * 创建促销价
     *
     * @param shopId         门店id
     * @param skuId          商品id
     * @param originalPrice  原价
     * @param generalCpItem  总部改价明细
     * @param areaCpItem     大区改价明细
     * @param shopCpActivity 门店改价明细
     * @param vipCpActivity  会员价明细
     * @return 促销价
     */
    public static PromotionPriceCommand create(Integer shopId, Integer skuId, BigDecimal originalPrice,
                                               GeneralCpItem generalCpItem,
                                               AreaCpItem areaCpItem,
                                               ShopCpActivity shopCpActivity,
                                               VipCpActivity vipCpActivity
    ) {

        if (shopId == null) {
            throw new BusinessException("门店id不能为空");
        }
        if (skuId == null) {
            throw new BusinessException("商品Id不能为空");
        }
        if (originalPrice == null || originalPrice.doubleValue() <= 0) {
            log.warn("总部活动启动失败，原价为空：shopId={} sku={}", shopId, skuId);
            throw new BusinessException("商品原价不能为空");
        }

        PromotionPriceCommand priceCommand = PromotionPriceCommand
                .builder()
                .shopId(shopId)
                .skuId(skuId)
                .originalPrice(originalPrice)
                .generalCpItem(generalCpItem)
                .areaCpItem(areaCpItem)
                .shopCpActivity(shopCpActivity)
                .vipCpActivity(vipCpActivity)
                .build();
        return priceCommand;

    }

    public static PromotionPriceCommand create(Integer shopId, Integer skuId, BigDecimal originalPrice,
                                               CpActivitySpec spec) {
        if (spec == null) {
            throw new BusinessException("必须设置当前活动规范");
        }
        return create(shopId, skuId, originalPrice, spec.getGeneralCpItem(), spec.getAreaCpItem(), spec.getShopCpActivity(), spec.getVipCpActivity());
    }

}
