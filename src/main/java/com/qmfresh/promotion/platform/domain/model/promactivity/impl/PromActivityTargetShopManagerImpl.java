package com.qmfresh.promotion.platform.domain.model.promactivity.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityTargetShopManager;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.PromActivityTargetShopMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityTargetShopPO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityTargetShopDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 */
@Service
@Slf4j
public class PromActivityTargetShopManagerImpl extends ServiceImpl<PromActivityTargetShopMapper, PromActivityTargetShopPO>
        implements PromActivityTargetShopManager {

    @Override
    public List<PromActivityTargetShopPO> queryByActivity(Long activityId) {
        return baseMapper.queryByActivity(activityId);
    }

    @Override
    public List<Integer> queryShopId(Long activityId) {
        return baseMapper.queryShopId(activityId);
    }

    @Override
    public void insertBatch(Long activityId, List<PromActivityTargetShopDTO> shopDTOList) {
        List<PromActivityTargetShopPO> shopPOList = new ArrayList<>();
        shopDTOList.forEach(
                shopDTO->{
                    PromActivityTargetShopPO shopPO = new PromActivityTargetShopPO(shopDTO);
                    shopPO.setActivityId(activityId);
                    shopPOList.add(shopPO);
                }
        );
        if (!shopPOList.isEmpty()) {
            super.saveBatch(shopPOList);
        }
    }

    @Override
    public void deletedBatch(Long activityId, List<Integer> shopIdList) {
        baseMapper.deletedBatch(activityId, shopIdList, new Date());
    }

    @Override
    public List<Long> exchangeActivity(List<Long> activityList, Integer shopId, Integer shopType) {
        return baseMapper.exchangeActivity(activityList, shopId, shopType);
    }

    public List<Long> queryActivityId(List<Integer> shopIdList, Integer shopType, List<Long> activityList) {
        return baseMapper.queryActivityId(shopIdList, shopType, activityList);
    }
}
