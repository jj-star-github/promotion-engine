package com.qmfresh.promotion.platform.domain.model.activity;

import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface ShopManager {

    /**
     * 门店id
     *
     * @param shopId 门店id
     * @return 门店对象
     */
    Shop shop(Integer shopId);

    /**
     * 查询门店列表
     *
     * @param shopIds
     * @return
     */
    List<Shop> shops(List<Integer> shopIds);


}
