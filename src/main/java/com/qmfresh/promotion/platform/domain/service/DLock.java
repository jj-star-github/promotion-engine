package com.qmfresh.promotion.platform.domain.service;

import com.qmfresh.promotion.platform.domain.shared.BusinessException;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface DLock {

    String PROMOTION_GENERAL_CP_PRE_KEY = "promotion:cp:general:createCommand:";
    String PROMOTION_CP_CREATE_LOG_PRE_KEY = "promotion:cp:createlog:";

    /**
     * 超时时间的设置是个艺术
     *
     * @param key     redisKey
     * @param timeout 超时时间
     * @return boolean
     */
    Boolean tryLock(String key, Long timeout);

    Boolean unlock(String key);

    static String generalCpCreateKey(Long operatorId) {
        return PROMOTION_GENERAL_CP_PRE_KEY + operatorId;
    }

    static String generalCreateLogKey(Long createLogId) {
        if (createLogId == null) {
            throw new BusinessException("必须设置cp创建日志id");
        }

        return PROMOTION_CP_CREATE_LOG_PRE_KEY + createLogId;
    }
}
