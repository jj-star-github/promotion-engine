package com.qmfresh.promotion.platform.domain.model.promactivity;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityTargetSkuPO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityCampOnSkuDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityTargetSkuDTO;

import java.util.List;

/**
 * 目标商品信息
 */
public interface PromActivityTargetSkuManager extends IService<PromActivityTargetSkuPO> {

    /**
     * 根据活动查询
     * @param activityId
     * @return
     */
    List<PromActivityTargetSkuPO> queryByActivity(Long activityId);

    /**
     * 根据活动批量查询
     * @param activityIdList
     * @return
     */
    List<PromActivityTargetSkuPO> queryByActivityList(List<Long> activityIdList);

    /**
     * 查询活动目标SKUID
     * @param activityId
     * @return
     */
    List<Integer> querySkuId(Long activityId);

    /**
     * 批量插入目标商品信息
     * @param activityId
     * @param skuDTOList
     */
    void insertBatch(Long activityId, List<PromActivityTargetSkuDTO> skuDTOList);

    /**
     * 批量删除
     * @param activityId
     * @param skuIdList
     */
    void deletedBatch(Long activityId, List<Integer> skuIdList);

    /**
     * 过滤目标商品信息
     * @param skuDTOList
     * @param activityPO
     * @return
     */
    List<PromActivityCampOnSkuDTO> filter(List<PromActivityCampOnSkuDTO> skuDTOList, PromActivityPO activityPO, String traceId);

    /**
     * 根据 class2查询活动交集
     * @param activityList
     * @param class2IdList
     * @return
     */
    List<Long> queryActivityIdByClass2(List<Long> activityList, List<Integer> class2IdList);

    /**
     * 根据 SKUID查询活动交集
     * @param activityList
     * @param class2IdList
     * @return
     */
    List<Long> queryActivityIdBySkuId(List<Long> activityList, List<Integer> class2IdList, Integer skuType);

}
