package com.qmfresh.promotion.platform.domain.model.activity.cp.general.command;

import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class GeneralCpDelSkuCommand implements Serializable {

    /**
     * 改价明细id
     */
    private Integer skuId;
    private Long activityId;
    /**
     * 操作人
     */
    private Operator operator;

    private String remark;

    public static GeneralCpDelSkuCommand create(Operator operator, Long activityId, Integer skuId) {
        GeneralCpDelSkuCommand generalCpDelSkuCommand = new GeneralCpDelSkuCommand();
        if (skuId == null) {
            throw new BusinessException("skuId不能为空");
        }
        generalCpDelSkuCommand.setOperator(operator);
        generalCpDelSkuCommand.setSkuId(skuId);
        generalCpDelSkuCommand.setActivityId(activityId);
        return generalCpDelSkuCommand;
    }
}
