package com.qmfresh.promotion.platform.domain.model.activity.cp;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qmfresh.promotion.platform.domain.model.activity.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.command.CpCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.event.CpEvent;
import com.qmfresh.promotion.platform.domain.model.activity.cp.command.ShopQueryCommand;
import com.qmfresh.promotion.platform.domain.model.support.MqLog;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.ActivityMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.ChangePriceActivityMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ActivityPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ChangePriceActivityPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.support.MqLogMapper;
import com.qmfresh.promotion.platform.infrastructure.message.RocketMQSender;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 改价的通用方法
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class CpManagerImpl implements CpManager {

    @Resource
    private ChangePriceActivityMapper changePriceActivityMapper;
    @Resource
    private ShopManager shopManager;
    @Resource
    private SkuManager skuManager;
    @Resource
    private CpCheck cpCheck;
    @Resource
    private ActivityMapper activityMapper;
    @Resource
    private MqLogMapper mapper;
    @Resource
    private DefaultMQProducer defaultMQProducer;


    /**
     * 支持总部，大区，VIP，门店改价的信息查询
     *
     * @param shopQueryCommand 门店查询命令
     * @return 门店改价结果
     */
    @Override
    public Page<ShopCpResult> pageShopCp(ShopQueryCommand shopQueryCommand) {

        QueryWrapper<ChangePriceActivityPO> queryWrapper =
                new QueryWrapper<>();
        queryWrapper.eq("is_deleted", 0);
        queryWrapper.eq("type", shopQueryCommand.getCpBizType().getCode());
        queryWrapper.eq("activity_id", shopQueryCommand.getActivityId());
        queryWrapper.groupBy("shop_id");
        queryWrapper.select("distinct shop_id", "activity_id");

        IPage<ChangePriceActivityPO> result = changePriceActivityMapper.selectPage(
                new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(shopQueryCommand.getPageNum(), shopQueryCommand.getPageSize())
                , queryWrapper);
        return Page.convertIPage(shopQueryCommand.getPageNum(), shopQueryCommand.getPageSize(), result, ChangePriceActivityPO::convert);
    }

    /**
     * 创建改价的核心流程
     *
     * @param cpCreateCommand 改价创建活动命令
     * @return 创建结果
     */
    @Override
    public CpItemResult create(CpCreateCommand cpCreateCommand) {

        log.info("创建改价活动：param={}", JSON.toJSONString(cpCreateCommand));

        log.info("开始创建[{}]改价: shopCount={} skuCount={}", cpCreateCommand.getCpBizType().name(), cpCreateCommand.getShopIds().size(),
                cpCreateCommand.getSkuCpItems().size());

        //灰度店处理
        List<Shop> shops = shopManager.shops(cpCreateCommand.getShopIds());
        List<Shop> grayShops = Shop.grayShops(shops);
        if (CollectionUtils.isEmpty(grayShops)) {
            log.warn("不存在灰度店 shopIds={}", JSON.toJSONString(cpCreateCommand.getShopIds()));
            throw new BusinessException("选择门店没有灰度店");
        }

        //商品处理处理: 不检查商品是否在售卖中
        if (CollectionUtils.isEmpty(cpCreateCommand.getSkuCpItems())) {
            log.warn("改价商品列表为空：skus={}", JSON.toJSONString(cpCreateCommand.getSkuCpItems()));
            throw new BusinessException("改价商品为空");
        }

        //
        List<Sku> skus = skuManager.sku(cpCreateCommand
                .getSkuCpItems()
                .stream().map(SkuCpItem::getSkuId).collect(Collectors.toList())
        );
        if (CollectionUtils.isEmpty(skus)) {
            throw new BusinessException("查询商品后，改价商品列表为空");
        }

        ActivityPO activityPO = null;
        List<CpItem> result = new ArrayList<>();
        for (Shop shop : grayShops) {
            try {
                activityPO = createOneShop(activityPO, shop, skus, cpCreateCommand, result);
            } catch (Exception e) {
                log.warn("创建{} 改价失败：shopId={} skuId={}", cpCreateCommand.getCpBizType().name(), shop.getShopId(), JSON.toJSONString(skus));
            }
        }
        return CpItemResult.create(activityPO, cpCreateCommand.getCpBizType(), result, "");
    }

    /**
     * todo 关注事务
     *
     * @param activityPO      活动对象
     * @param shop            门店id
     * @param skus            商品列表
     * @param cpCreateCommand 商品改价信息列表
     * @return 改价活动
     */
    @Transactional(rollbackFor = Exception.class)
    public ActivityPO createOneShop(ActivityPO activityPO, Shop shop, List<Sku> skus,
                                    CpCreateCommand cpCreateCommand, List<CpItem> result) {
        //检查活动周期
        CpTimeCycle cpTimeCycle = cpCreateCommand.getTimeCycle();
        List<SkuCpItem> skuCpItems = cpCreateCommand.getSkuCpItems();

        Map<Integer, SkuCpItem> skuCpItemMap =
                skuCpItems.stream().collect(Collectors.toMap(SkuCpItem::getSkuId, o -> o));

        //形成单条创建明细
        for (Sku sku : skus) {
            SkuCpItem sci = skuCpItemMap.get(sku.getSkuId());

            boolean lt = cpCheck.cpCreateCheck(shop, sci, cpTimeCycle, cpCreateCommand.getCpBizType());
            if (!lt) {
                log.info("检查不通过，存在重复的活动：shopId={} skuId = {} cpTimeCycle={} activityId={}", shop.getShopId(), sku.getSkuId(), cpTimeCycle, activityPO.getId(), activityPO.getName());
                result.add(CpItem.create(shop, sku, sci, "存在重复改价活动"));
                continue;
            }
            //创建活动
            if (activityPO == null) {
                activityPO = ActivityPO.c(cpCreateCommand.getActivityName(), cpCreateCommand.getCpBizType());
                activityMapper.insert(activityPO);
            }
            //遍历创建明细
            ChangePriceActivityPO changePriceActivityPO = ChangePriceActivityPO.createCp(cpCreateCommand, activityPO, shop, sku, sci);
            changePriceActivityMapper.insert(changePriceActivityPO);
            //发送创建事件
            RocketMQSender.doSend(defaultMQProducer, CpEvent.CP_EVENT_TOPIC, CpEvent.create(changePriceActivityPO),
                    new MqLog.PersistCallback(mapper));
        }
        log.info("创建[{}]活动完成：shopId={}", cpCreateCommand.getCpBizType().name(), shop.getShopId());
        return activityPO;
    }
}