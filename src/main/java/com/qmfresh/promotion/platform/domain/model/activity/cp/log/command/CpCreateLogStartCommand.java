package com.qmfresh.promotion.platform.domain.model.activity.cp.log.command;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class CpCreateLogStartCommand implements Serializable {

    /**
     * 开始时间戳
     */
    private Long startTimestamp;

    public static CpCreateLogStartCommand create() {
        return CpCreateLogStartCommand
                .builder()
                .startTimestamp(System.currentTimeMillis())
                .build();
    }
}
