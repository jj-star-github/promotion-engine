package com.qmfresh.promotion.platform.domain.model.activity.cp.action;

import lombok.Data;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class CloseEventCpAction<T> implements CpAction {

    private T activity;
    private String remark;


    /**
     * 创建关闭活动事件
     *
     * @param activity 关闭的活动
     * @param remark   备注
     * @param <T>      泛型
     * @return 关闭动作
     */
    public static <T> CpAction create(T activity, String remark) {
        CloseEventCpAction<T> closeEventCpAction = new CloseEventCpAction<>();
        closeEventCpAction.setActivity(activity);
        closeEventCpAction.setRemark(remark);
        return closeEventCpAction;
    }
}
