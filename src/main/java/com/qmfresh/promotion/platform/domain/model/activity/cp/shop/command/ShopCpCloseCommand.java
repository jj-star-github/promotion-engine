package com.qmfresh.promotion.platform.domain.model.activity.cp.shop.command;

import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class ShopCpCloseCommand implements Serializable {

    private Operator operator;
    private String remark;
    private Boolean compute;

    public static ShopCpCloseCommand createShop(String remark, Boolean compute) {
        if (StringUtils.isBlank(remark)) {
            throw new BusinessException("门店自动改价关闭，必须设置remark");
        }
        ShopCpCloseCommand shopCpCloseCommand = ShopCpCloseCommand.builder()
                .operator(Operator.createSystemOperator())
                .remark(remark)
                .compute(compute)
                .build();
        return shopCpCloseCommand;
    }

    public static ShopCpCloseCommand createShop(Operator operator, String remark) {
        ShopCpCloseCommand shopCpCloseCommand = ShopCpCloseCommand.builder()
                .operator(operator)
                .remark(remark)
                .compute(true)
                .build();
        return shopCpCloseCommand;
    }
}
