package com.qmfresh.promotion.platform.domain.model.activity.cp.general.command;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class GeneralCpSkuChangeCommand implements Serializable {
    /**
     * 活动id
     */
    private Long activityId;
    /**
     * 活动明细id
     */
    private Long activityCpId;
    /**
     * 门店id
     */
    private Integer shopId;
    /**
     * 商品id
     */
    private Integer skuId;
    /**
     * 改价类型
     */
    private Integer cpType;
    /**
     * 规则价格
     */
    private BigDecimal rulePrice;
    /**
     * 计划营销费用
     */
    private BigDecimal planMarketingFee;
}
