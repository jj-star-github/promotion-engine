package com.qmfresh.promotion.platform.domain.model.activity.cp;

import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.Sku;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class CpItem implements Serializable {

    private Long activityCpId;
    private Integer shopId;
    private Integer skuId;
    private BigDecimal rulePrice;
    private BigDecimal planMarketingFee;
    private String remark;

    public static CpItem create(Shop shop, Sku sku, SkuCpItem skuCpItems, String remark) {
        CpItem cpItem = new CpItem();
        cpItem.setShopId(shop.getShopId());
        cpItem.setSkuId(sku.getSkuId());
        cpItem.setRulePrice(skuCpItems == null ? BigDecimal.ZERO : skuCpItems.getRulePrice());
        cpItem.setPlanMarketingFee(skuCpItems == null ? BigDecimal.ZERO : skuCpItems.getPlanMarketingFee());
        cpItem.setRemark(remark);
        return cpItem;
    }


    public static CpItem create(Shop shop, Sku sku, SkuCpItem skuCpItems) {
        return create(shop, sku, skuCpItems, "");
    }


}
