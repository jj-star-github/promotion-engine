package com.qmfresh.promotion.platform.domain.valueobj.funds;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;


@Data
@ToString
public class FundsTransactionVO implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 门店ID
     */
    private Integer shopId;
    /**
     * 门店类型
     * FundsShopTypeEnum
     */
    private Integer shopType;
    /**
     * 门店名称
     */
    private String shopName;
    /**
     * 业务ID， 费用申请记录ID， 周清记录ID    skuId
     */
    private String businessId;
    /**
     * 业务ID， 费用申请记录ID， 周清记录ID 订单号
     */
    private String businessCode;
    /**
     * 业务类型
     * FundsBusinessTypeEnum
     */
    private Integer businessType;
    /**
     * 营销费用 正=加   负=扣减
     */
    private BigDecimal changeAmount;
    /**
     * GMV
     */
    private BigDecimal businessGmv;
    /**
     * 描述  businessType=3 JSON封装数据：FundsSkuDto
     *      businessType=3 商品名
     *
     */
    private String remark;

}
