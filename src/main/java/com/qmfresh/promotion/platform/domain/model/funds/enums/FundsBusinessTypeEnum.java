package com.qmfresh.promotion.platform.domain.model.funds.enums;


import lombok.Getter;

/**
 * 费用变更业务类型
 *
 */
@Getter
public enum FundsBusinessTypeEnum {

    /**
     * 费用申请
     */
    FUNDS_APPLY(1, "费用申请"),
    /**
     * 单品费用盈利
     */
    SKU_INCOME(2, "单品费用盈利"),
    /**
     * 营销费用支持
     */
    SKU_MARKETING(3, "营销费用支出"),
    /**
     * 订单抹零
     */
    ORDER_ML(4, "订单抹零"),
    /**
     * 订单改价
     */
    ORDER_CP(5, "收银订单打折"),
    /**
     * 周清业务，有正负
     */
    WEEK_INCOME(6, "周清业务"),
    /**
     * 周清支出，废弃
     */
    WEEK_USE(7, "周清支出"),
    /**
     * 单品折扣
     */
    ITEM_DISCOUNT(8, "收银单品打折"),
    ;
    private Integer code;

    private String remark;

    FundsBusinessTypeEnum(Integer code, String remark) {
        this.code = code;
        this.remark = remark;
    }
}
