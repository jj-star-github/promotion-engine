package com.qmfresh.promotion.platform.domain.model.promactivity.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.dto.MeetInfo;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityConditionManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.enums.ActivityRuleEnum;
import com.qmfresh.promotion.platform.domain.model.promactivity.vo.HitTargetConditionVo;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.PromActivityConditionMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityConditionPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityTargetShopPO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityCampOnSkuDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityConditionDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.valves.rewrite.RewriteCond;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 */
@Service
@Slf4j
public class PromActivityConditionManagerImpl extends ServiceImpl<PromActivityConditionMapper, PromActivityConditionPO>
        implements PromActivityConditionManager {

    private static final Integer NO_LIMIT = 99999; //库存没有上限

    @Override
    public List<PromActivityConditionPO> queryByActivity(Long activityId) {
        return baseMapper.queryByActivity(activityId, null);
    }

    @Override
    public List<PromActivityConditionPO> queryByActivityList(List<Long> activityIdList) {
        return baseMapper.queryByActivityList(activityIdList, null);
    }

    @Override
    public void insertBatch(Long activityId, List<PromActivityConditionDTO> conditionDTOList) {
        List<PromActivityConditionPO> conditionPOList = new ArrayList<>();
        conditionDTOList.forEach(
                condition->{
                    PromActivityConditionPO conditionPO = new PromActivityConditionPO(condition);
                    conditionPO.setActivityId(activityId);
                    conditionPOList.add(conditionPO);
                }
        );
        if (!conditionPOList.isEmpty()) {
            super.saveBatch(conditionPOList);
        }
    }

    @Override
    public void updateCondition(PromActivityConditionDTO conditionDto) {
        PromActivityConditionPO conditionPO = new PromActivityConditionPO(conditionDto);
        conditionPO.setId(conditionDto.getId());
        super.updateById(conditionPO);
    }

    @Override
    public HitTargetConditionVo hitTarget(List<PromActivityCampOnSkuDTO> skuDTOList, PromActivityPO activityPO, String traceId) {
        log.info("获取奖品信息 traceId={}, skuDTOList={}, activityId={}", traceId, JSON.toJSONString(skuDTOList), activityPO.getId());
        List<PromActivityConditionPO> conditionPO = activityPO.getConditionPOList();
        //获取有库存的奖品信息
        if(CollectionUtils.isEmpty(conditionPO)) {
            log.info("获取奖品信息-当前活动没有满足条件的奖品信息 , traceId={} activityId={}", traceId, activityPO.getId());
            return null;
        }
        //计算总金额
        BigDecimal sumAmount = new BigDecimal(0);
        for (PromActivityCampOnSkuDTO skuDTO : skuDTOList) {sumAmount = sumAmount.add(skuDTO.getRealAmount());}
        log.info("获取奖品信息-命中 traceId={}, sumAmount={}, conditionPO={}", traceId, sumAmount, JSON.toJSONString(conditionPO));
        //执行每满
        if(ActivityRuleEnum.EVERY.getCode() == activityPO.getRuleType()){
            return every(conditionPO.get(0), sumAmount);
        }

        //执行阶梯 倒序验证获取第一个
        if (ActivityRuleEnum.LADDER.getCode() == activityPO.getRuleType()) {
            return ladder(conditionPO, sumAmount);
        }
        return null;
    }

    @Override
    public int updateStock(PromActivityConditionPO conditionPO, Integer num) {
        //无上限
        if (NO_LIMIT.compareTo(conditionPO.getStock()) == 0) {
            return num;
        }
        //无库存
        return 0;
    }

    /**
     * 执行每满
     * @param conditionPO
     * @param sumAmount
     * @return
     */
    private HitTargetConditionVo every(PromActivityConditionPO conditionPO, BigDecimal sumAmount) {
        HitTargetConditionVo hitTargetVo = new HitTargetConditionVo();
        List<MeetInfo> meetInfoList = new ArrayList<>();
        int num = 0;
        for (int i = 0; i < conditionPO.getMaxNum(); i++) {
            MeetInfo meetInfo = new MeetInfo();
            BigDecimal setMeetAmount = conditionPO.getConditionNum().multiply(BigDecimal.valueOf(i + 1));
            if (sumAmount.compareTo(setMeetAmount) < 0) {
                meetInfo.setLevel(i + 1);
                meetInfo.setMeet(false);
                meetInfo.setNextDiffMoney(setMeetAmount.subtract(sumAmount));
                meetInfo.setNextMeetMoney(setMeetAmount);
                meetInfoList.add(meetInfo);
                break;
            }
            meetInfo.setMeetMoney(setMeetAmount);
            meetInfo.setLevel(i + 1);
            meetInfo.setMeet(true);
            meetInfoList.add(meetInfo);
            num = num + 1;
        }
        hitTargetVo.setNum(num);
        hitTargetVo.setConditionPO(conditionPO);
        hitTargetVo.setMeetAmount(conditionPO.getConditionNum().multiply(BigDecimal.valueOf(num)));
        hitTargetVo.setMeetInfoList(meetInfoList);
        return hitTargetVo;
    }

    /**
     * 执行阶梯
     * @param conditionPOList
     * @param sumAmount
     * @return
     */
    private HitTargetConditionVo ladder(List<PromActivityConditionPO> conditionPOList, BigDecimal sumAmount) {
        //根据级别升序
        Collections.sort(conditionPOList, (o1, o2) -> o1.getConditionLevel() < o2.getConditionLevel() ? -1 : 1);
        HitTargetConditionVo hitTargetVo = new HitTargetConditionVo();
        List<MeetInfo> meetInfoList = new ArrayList<>();
        PromActivityConditionPO hitTarget = null;
        int num = 0;
        for(PromActivityConditionPO conditionPO : conditionPOList) {
            MeetInfo meetInfo = new MeetInfo();
            if (sumAmount.compareTo(conditionPO.getConditionNum()) < 0) {
                meetInfo.setNextMeetMoney(conditionPO.getConditionNum());
                meetInfo.setNextDiffMoney(conditionPO.getConditionNum().subtract(sumAmount));
                meetInfo.setLevel(conditionPO.getConditionLevel());
                meetInfo.setMeet(false);
                meetInfoList.add(meetInfo);
                break;
            }
            hitTarget = conditionPO;
            num = 1;
            meetInfo.setMeetMoney(conditionPO.getConditionNum());
            meetInfo.setLevel(conditionPO.getConditionLevel());
            meetInfo.setMeet(true);
            meetInfoList.add(meetInfo);
        }
        hitTargetVo.setNum(num);
        hitTargetVo.setConditionPO(hitTarget);
        hitTargetVo.setMeetInfoList(meetInfoList);
        if(num > 0) {
            hitTargetVo.setMeetAmount(hitTarget.getConditionNum());
        }
        return hitTargetVo;
    }
}
