package com.qmfresh.promotion.platform.domain.model.activity.cp.vip;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class VipCpActivityManagerImpl implements VipCpActivityManager {
}
