package com.qmfresh.promotion.platform.domain.model.activity.cp.action;

import lombok.Getter;

import java.util.Date;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public class StartCpAction implements CpAction {
    /**
     * 营销活动
     */
    private Long activityId;
    /**
     * 开始时间
     */
    private Date startTime;


    /**
     * 创建启动的活动
     *
     * @param activityId 营销活动
     * @return 启动对象
     */
    public static StartCpAction create(Long activityId) {
        StartCpAction startCpAction = new StartCpAction();
        startCpAction.startTime = new Date();
        if (activityId != null) {
            startCpAction.activityId = activityId;
        }
        return startCpAction;
    }

    /**
     * 创建启动动作
     *
     * @return 启动动作
     */
    public static StartCpAction create() {
        return create(null);
    }
}
