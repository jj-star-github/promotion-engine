package com.qmfresh.promotion.platform.domain.valueobj.shop;

import lombok.Getter;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public class ShopVO implements Serializable {

    /**
     * 门店id
     */
    private Integer shopId;
    /**
     * 门店名称
     */
    private String shopName;

    public static ShopVO create(Integer shopId, String shopName) {
        ShopVO shopVO = new ShopVO();
        shopVO.shopId = shopId;
        shopVO.shopName = shopName;
        return shopVO;
    }

}
