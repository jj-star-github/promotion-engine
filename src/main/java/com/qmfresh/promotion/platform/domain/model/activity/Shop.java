package com.qmfresh.promotion.platform.domain.model.activity;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class Shop implements Serializable {
    public static final Integer GREY = 1;

    private Integer shopId;
    private String shopName;
    /**
     * 门店类型：
     * PROPRIETARY_MODEL(1, "自营模式"),
     * PARTNER_MODEL(2, "合伙人模式"),
     * FREEZER(4, "冻库"),
     * Franchisee_MODEL(5, "加盟模式");
     */
    private Integer shopType;
    private String shopTypeDesc;
    private Integer cityId;
    private Integer areaId;
    /**
     * 营销灰度店标签 1=灰度
     */
    private Integer marketTag;
    private Integer level;

    public static Shop create(Integer shopId) {
        Shop shop = Shop.builder()
                .shopId(shopId)
                .build();
        return shop;
    }

    public String getShopTypeDesc() {
        if (shopType == null) {
            return "";
        }

        if (shopType.equals(1)) {
            return "自营";
        }
        if (shopType.equals(2)) {
            return "合伙人";
        }
        if (shopType.equals(4)) {
            return "冻库";
        }
        if (shopType.equals(5)) {
            return "加盟";
        }
        return "";

    }

    public static List<Shop> grayShops(List<Shop> shops) {
        List<Shop> inclueShopIds = new ArrayList<>();
        for (Shop shop : shops) {
            if (Shop.GREY.equals(shop.getMarketTag())) {
                inclueShopIds.add(shop);
            }
        }
        return inclueShopIds;
    }

    /**
     * 1-A 1-B 1-C
     * 2-A 2-B 2-C
     * 3-A 3-B 3-C
     *
     * @return
     */
    public String getShopLevel() {
        if (level == null) {
            return "";
        }

        if (1 == level) {
            return "1-A";
        }
        if (2 == level) {
            return "1-B";
        }
        if (3 == level) {
            return "1-C";
        }
        if (4 == level) {
            return "2-A";
        }
        if (5 == level) {
            return "2-B";
        }
        if (6 == level) {
            return "2-C";
        }
        if (7 == level) {
            return "3-A";
        }
        if (8 == level) {
            return "3-B";
        }
        if (9 == level) {
            return "3-C";
        }
        return "";
    }

}
