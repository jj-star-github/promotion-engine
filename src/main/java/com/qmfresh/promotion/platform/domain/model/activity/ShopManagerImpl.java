package com.qmfresh.promotion.platform.domain.model.activity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qmfresh.promotion.platform.infrastructure.http.Https;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class ShopManagerImpl implements ShopManager {

    @Value("${url.service.cmfresh}")
    private String shopServiceUrl;

    /**
     * {"success":true,"errorCode":0,"message":"","body":{"id":350,"shopType":5,"shopName":"南京秦淮源筑店","gisAreaId":10,"cityId":1,"shopDesc":null,"isDeleted":0,"areaId":0,"baseAreaId":null,"sellChannel":1,"status":1,"locationLan":"31.6690787","locationLgt":"119.02712459999998","startTime":null,"endTime":null,"isOrder":0,"startSendMoney":68.00,"address":"南京农卉农业科技有限公司","name":null,"phone":"15151898351","cityIds":null,"advancePayment":0,"notType":null,"notSellChannel":null,"isOpenCompany":0,"shopTypes":null,"warehouseId":363,"warehouseIds":null,"warehouseName":"南京秦淮源筑店","deliveryModel":3,"openDate":"2020-04-01","picsName":null,"area3Id":null,"changeTypeTime":null,"shopLevel":1,"promotionGrayType":0,"bank":null,"openingBank":null,"bankCard":null,"cardholder":null,"aliServiceCharge":null,"wxServiceCharge":null,"cashCardCharge":null,"isLeaShopUpdate":null,"ids":null,"shopFlag":true,"queryPermission":0,"donePermission":1,"queryFlag":"0","doneFlag":"1","nameIndex":null,"label":null,"isDelete":null,"province":null,"city":null,"level":null,"ct":1585710219,"ut":1591090577,"startSellTimeStamp":1598400000,"endSellTimeStamp":1598400000}}
     *
     * @param shopId 门店id
     * @return
     */
    @Override
    public Shop shop(Integer shopId) {
        String result = Https.post(shopServiceUrl + "/shop/ex/shop/getShopById", null, JSON.toJSONString(shopId));
        JSONObject jsonObject = JSONObject.parseObject(result);
        Boolean success = jsonObject.getBoolean("success");
        if (success == null || !success) {
            log.warn("请求门店接口失败：{}", shopId);
            return null;
        }

        JSONObject body = jsonObject.getJSONObject("body");
        Shop shop = parseShop(body);
        return shop;
    }

    private Shop parseShop(JSONObject jsonObject) {
        Integer id = jsonObject.getInteger("id");
        Integer shopType = jsonObject.getInteger("shopType");
        String shopName = jsonObject.getString("shopName");
        Integer cityId = jsonObject.getInteger("cityId");
        Integer areaId = jsonObject.getInteger("areaId");
        Integer marketTag = jsonObject.getInteger("promotionGrayType");
        Integer level = jsonObject.getInteger("shopLevel");

        return Shop.builder()
                .shopId(id)
                .shopType(shopType)
                .shopName(shopName)
                .cityId(cityId)
                .areaId(areaId)
                .marketTag(marketTag)
                .level(level)
                .build();
    }

    /**
     * {"success":true,"errorCode":0,"message":"","body":[{"id":350,"shopType":5,"shopName":"南京秦淮源筑店","gisAreaId":10,"cityId":1,"shopDesc":null,"isDeleted":0,"areaId":0,"baseAreaId":null,"sellChannel":1,"status":1,"locationLan":"31.6690787","locationLgt":"119.02712459999998","startTime":null,"endTime":null,"isOrder":0,"startSendMoney":68.00,"address":"南京农卉农业科技有限公司","name":null,"phone":"15151898351","cityIds":null,"advancePayment":0,"notType":null,"notSellChannel":null,"isOpenCompany":0,"shopTypes":null,"warehouseId":363,"warehouseIds":null,"warehouseName":"南京秦淮源筑店","deliveryModel":3,"openDate":"2020-04-01","picsName":null,"area3Id":null,"changeTypeTime":null,"shopLevel":1,"promotionGrayType":0,"bank":null,"openingBank":null,"bankCard":null,"cardholder":null,"aliServiceCharge":null,"wxServiceCharge":null,"cashCardCharge":null,"isLeaShopUpdate":null,"ids":null,"shopFlag":true,"queryPermission":0,"donePermission":1,"queryFlag":"0","doneFlag":"1","nameIndex":null,"label":null,"isDelete":null,"province":null,"city":null,"level":null,"ct":1585710219,"ut":1591090577,"startSellTimeStamp":1598659200,"endSellTimeStamp":1598659200},{"id":10,"shopType":1,"shopName":"测试店","gisAreaId":2,"cityId":1,"shopDesc":"","isDeleted":0,"areaId":0,"baseAreaId":null,"sellChannel":1,"status":1,"locationLan":"32.0490501","locationLgt":"118.81680380000003","startTime":"08:00","endTime":"23:00","isOrder":0,"startSendMoney":68.00,"address":"南京博物院","name":"测试店","phone":"888888888888","cityIds":null,"advancePayment":0,"notType":null,"notSellChannel":null,"isOpenCompany":0,"shopTypes":null,"warehouseId":10,"warehouseIds":null,"warehouseName":"测试店","deliveryModel":1,"openDate":null,"picsName":null,"area3Id":null,"changeTypeTime":1594197441,"shopLevel":1,"promotionGrayType":0,"bank":null,"openingBank":null,"bankCard":null,"cardholder":null,"aliServiceCharge":null,"wxServiceCharge":null,"cashCardCharge":null,"isLeaShopUpdate":null,"ids":null,"shopFlag":true,"queryPermission":0,"donePermission":1,"queryFlag":"0","doneFlag":"1","nameIndex":null,"label":null,"isDelete":null,"province":null,"city":null,"level":null,"ct":1530609236,"ut":1594197498,"startSellTimeStamp":1598659200,"endSellTimeStamp":1598713200}]}
     *
     * @param shopIds
     * @return
     */
    @Override
    public List<Shop> shops(List<Integer> shopIds) {
        String result = Https.post(shopServiceUrl + "/shop/ex/getByCondition", null, JSON.toJSONString(ShopParam.create(shopIds)));
        JSONObject jsonObject = JSONObject.parseObject(result);
        Boolean success = jsonObject.getBoolean("success");
        if (success == null || !success) {
            return new ArrayList<>();
        }

        JSONArray jsonArray = jsonObject.getJSONArray("body");
        List<Shop> shops = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jo = jsonArray.getJSONObject(i);
            shops.add(parseShop(jo));
        }
        return shops;
    }

    @Data
    public static class ShopParam {
        private List<Integer> ids;


        public static ShopParam create(List<Integer> shopIds) {
            ShopParam shopParam = new ShopParam();
            shopParam.setIds(shopIds);
            return shopParam;
        }
    }


}
