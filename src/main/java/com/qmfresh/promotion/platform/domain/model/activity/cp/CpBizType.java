package com.qmfresh.promotion.platform.domain.model.activity.cp;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Slf4j
public enum CpBizType {
    /**
     * 改价
     */
    CP(10, "改价"),
    /**
     * 门店改价
     */
    CP_SHOP(11, "门店改价"),
    /**
     * 总部改价
     */
    CP_PLATFORM(12, "总部改价"),
    /**
     * 会员改价
     */
    CP_VIP(13, "会员改价"),
    /**
     * 区域改价
     */
    CP_AREA(14, "区域改价");
    private Integer code;
    private String msg;

    CpBizType(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static CpBizType from(Integer i) {
        for (CpBizType cpBizType : CpBizType.values()) {
            if (cpBizType.code.equals(i)) {
                return cpBizType;
            }
        }
        log.warn("CpBizType不匹配：{}", i);
        return null;
    }
}
