package com.qmfresh.promotion.platform.domain.model.promactivity.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.bean.coupon.CouponQuery;
import com.qmfresh.promotion.bean.coupon.SendCouponReturnBean;
import com.qmfresh.promotion.bean.promotion.MzCampOn;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.dto.PromotionMzCampOnRsp;
import com.qmfresh.promotion.external.service.ICouponExternalApiService;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityConvertManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.enums.ConditionTypeEnum;
import com.qmfresh.promotion.platform.domain.model.promactivity.enums.ConvertSendStatusEnum;
import com.qmfresh.promotion.platform.domain.service.CouponService;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.PromActivityConvertMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityConvertPO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 */
@Service
@Slf4j
public class PromActivityConvertManagerImpl extends ServiceImpl<PromActivityConvertMapper, PromActivityConvertPO>
        implements PromActivityConvertManager {

    @Resource
    private ICouponExternalApiService couponExternalApiService;
    @Resource
    private CouponService couponService;


    @Override
    public List<PromActivityConvertPO> queryByCanSend(Integer userId, Integer shopId, String businessCode, Integer businessType) {
        return baseMapper.queryByCanSend(userId, shopId, businessCode, businessType);
    }

    @Override
    public List<PromActivityConvertPO> queryByBusiness(String businessCode, Integer businessType) {
        return baseMapper.queryByBusiness(businessCode, businessType);
    }

    @Override
    public void batchUpdateStatus(List<Long> idList, Integer status) {
        baseMapper.batchUpdateStatus(idList, status);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PromotionMzCampOnRsp couponCampOn(MzCampOn mzCampOn, String orderCode, Integer shopId, Integer userId, String traceId) {
        //获取优惠券码
        CouponQuery couponQuery = new CouponQuery();
        couponQuery.setActivityId(mzCampOn.getCouponId());
        couponQuery.setSourceShopId(shopId);
        couponQuery.setSendNum(mzCampOn.getGivingNum());
        PromotionMzCampOnRsp couponCodeBean = couponExternalApiService.queryCouponInfo(couponQuery);
        if (null == couponCodeBean) {
            log.error("优惠前接口调用异常， traceId={} orderCode={} param={}", traceId, orderCode, JSON.toJSONString(couponQuery));
            throw new BusinessException("优惠券信息调用异常");
        }
        PromActivityConvertPO convertPO = new PromActivityConvertPO();
        convertPO.setActivityId(mzCampOn.getActivityId());
        convertPO.setBusinessCode(orderCode);
        convertPO.setBusinessType(1);
        convertPO.setAwardId(mzCampOn.getCouponId());
        convertPO.setAwardType(ConditionTypeEnum.COUPON.getCode());
        convertPO.setShopId(shopId);
        convertPO.setUserId(userId);
        couponCodeBean.setActivityId(mzCampOn.getActivityId());
        convertPO.setRemark(JSON.toJSONString(couponCodeBean.getCouponCodeList()));
        convertPO.setNum(mzCampOn.getGivingNum());
        convertPO.setGmtCreate(new Date());
        convertPO.setGmtModified(new Date());
        convertPO.setStatus(ConvertSendStatusEnum.WAIT.getCode());
        baseMapper.insert(convertPO);
        return couponCodeBean;
    }

    @Override
    public void sendErrorConvert(String param, String traceId) {
        //获取24小时内优惠券发送失败的订单
        List<PromActivityConvertPO> convertPOList = null;
        if (StringUtils.isNotBlank(param)) {
            convertPOList = queryByBusiness(param, 1);
            convertPOList = convertPOList.stream().filter(item -> ConvertSendStatusEnum.ERROR.getCode() == item.getStatus()).collect(Collectors.toList());
        } else {
            Date endTime = new Date();
            Date startTime = DateUtil.getDateBefore(endTime, 1);
            convertPOList = baseMapper.querySendErrorConvert(startTime, endTime);
        }
        if (CollectionUtils.isEmpty(convertPOList)) {
            log.info("奖励发放失败处理-没有目标数据 traceId={} param={}", traceId, param);
            return;
        }
        log.info("奖励发放失败处理 traceId={} convertList={}", traceId, JSON.toJSONString(convertPOList));

        //批量处理
        this.sendAward(convertPOList, traceId);
    }

    @Override
    public List<SendCouponReturnBean> sendAward(List<PromActivityConvertPO> convertPOList, String traceId) {
        List<SendCouponReturnBean> returnBeans = new ArrayList<>();
        List successId = new ArrayList();
        List errorId = new ArrayList();
        //调用优惠券发放奖品
        for(PromActivityConvertPO convertPO : convertPOList) {
            // 赠品类型:1.赠商品，2.赠券 3:番茄币
            switch (convertPO.getAwardType()) {
                case 1:
                    break;
                case 2:
                    if (StringUtils.isBlank(convertPO.getRemark())) {
                        continue;
                    }
                    List<SendCouponReturnBean> beans = couponService.sendCoupon(convertPO, traceId);
                    if(CollectionUtils.isEmpty(beans)) {
                        errorId.add(convertPO.getId());
                        continue;
                    }
                    successId.add(convertPO.getId());
                    returnBeans.addAll(beans);
                    break;
                case 3:
                    break;
                default:
            }
        }
        if (CollectionUtils.isNotEmpty(errorId)) {
            baseMapper.batchUpdateStatus(errorId, ConvertSendStatusEnum.ERROR.getCode());
        }
        if (CollectionUtils.isNotEmpty(successId)) {
            baseMapper.batchUpdateStatus(successId, ConvertSendStatusEnum.SUCCESS.getCode());
        }
        return returnBeans;
    }
}
