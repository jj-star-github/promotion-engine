package com.qmfresh.promotion.platform.domain.model.activity.cp;

import com.qmfresh.promotion.platform.domain.model.activity.Subject;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.common.filter.impl.Op;
import org.springframework.util.Assert;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class Operator implements Serializable {
    /**
     * 代表主体，非个人
     */
    private Subject representSubject;
    /**
     * 操作者id
     */
    private Integer operatorId;
    /**
     * 操作者名称
     */
    private String operatorName;

    public static Operator createSystemOperator() {
        Operator operator = new Operator();
        operator.setOperatorId(0);
        operator.setOperatorName("系统");
        operator.setRepresentSubject(Subject.create(Subject.Platform.INTANCE));
        return operator;
    }

    public static Operator create(Integer operatorId, String operatorName, Subject subject) {
        if (operatorId == null) {
            throw new BusinessException("操作者id不能为空");
        }
        if (StringUtils.isBlank(operatorName)) {
            throw new BusinessException("操作者名称不能为空");
        }
        Operator operator = new Operator();
        operator.setOperatorId(operatorId);
        operator.setOperatorName(operatorName);
        operator.setRepresentSubject(subject);
        return operator;
    }

    public static Operator create(Integer operatorId, String operatorName) {
        return create(operatorId, operatorName, null);

    }

}
