package com.qmfresh.promotion.platform.domain.model.activity.cp.action;

import com.qmfresh.promotion.platform.domain.model.activity.Sku;
import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpType;
import lombok.Getter;

import java.math.BigDecimal;

/**
 * 改价计划动作
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public class PlanCpAction implements CpAction {
    /**
     * 商品信息
     */
    private Sku sku;
    /**
     * 计划营销费用
     */
    private BigDecimal planMarketingFee;
    /**
     * 目标金额
     */
    private BigDecimal rulePrice;
    /**
     * 改价类型
     */
    private CpType cpType;
    /**
     * 改价周期
     */
    private CpTimeCycle cpTimeCycle;

    /**
     * 创建门店改价
     *
     * @param sku              商品列表
     * @param planMarketingFee 计划营销费用
     * @param rulePrice        目标价
     * @param cpTimeCycle      时间周期
     * @return 计划改价动作
     */
    public static PlanCpAction createShopPlanCpAction(Sku sku,
                                                      BigDecimal planMarketingFee,
                                                      BigDecimal rulePrice,
                                                      CpTimeCycle cpTimeCycle) {
        //todo  to check
        PlanCpAction planCpAction = new PlanCpAction();
        planCpAction.sku = sku;
        planCpAction.planMarketingFee = planMarketingFee;
        planCpAction.rulePrice = rulePrice;
        planCpAction.cpTimeCycle = cpTimeCycle;
        planCpAction.cpType = CpType.DIFF;//to extend
        return planCpAction;
    }

}
