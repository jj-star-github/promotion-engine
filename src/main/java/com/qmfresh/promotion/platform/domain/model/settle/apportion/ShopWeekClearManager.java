package com.qmfresh.promotion.platform.domain.model.settle.apportion;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopWeekClear;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopWeekSum;

import java.math.BigDecimal;

public interface ShopWeekClearManager extends IService<ShopWeekClear> {
    ShopWeekSum querySumpriceAmount(Integer shopId, String createDayStart, String createDayEnd);
}
