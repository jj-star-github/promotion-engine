package com.qmfresh.promotion.platform.domain.model.activity.cp.price;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class PromotionPriceSpec implements Serializable {

    //最终促销价规范
    private PromotionPriceRule generalPromotion;
    private PromotionPriceRule areaPromotion;
    private PromotionPriceRule shopPromotion;
    private PromotionPriceRule vipPromotion;

    public static PromotionPriceSpec createEmpty() {
        return PromotionPriceSpec.create(null, null, null, null);
    }

    public static PromotionPriceSpec create(PromotionPriceRule general,
                                            PromotionPriceRule area,
                                            PromotionPriceRule shop,
                                            PromotionPriceRule vip
    ) {
        Map<Integer, PromotionPriceRule> map = new HashMap<>();
        if (general != null) {
            map.put(general.getType(), general);
        }
        if (area != null) {
            map.put(area.getType(), area);
        }
        if (vip != null) {
            map.put(vip.getType(), vip);
        }
        if (shop != null) {
            map.put(shop.getType(), shop);
        }

        PromotionPriceSpec priceSpec = PromotionPriceSpec.builder()
                .generalPromotion(map.get(CpBizType.CP_PLATFORM.getCode()))
                .areaPromotion(map.get(CpBizType.CP_AREA.getCode()))
                .shopPromotion(map.get(CpBizType.CP_SHOP.getCode()))
                .vipPromotion(map.get(CpBizType.CP_VIP.getCode()))
                .build();
        return priceSpec;
    }


}
