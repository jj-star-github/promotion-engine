package com.qmfresh.promotion.platform.domain.service;

import com.qmfresh.promotion.bean.coupon.CouponQuery;
import com.qmfresh.promotion.bean.coupon.SendCouponRequestBean;
import com.qmfresh.promotion.bean.coupon.SendCouponReturnBean;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.dto.PromotionMzCampOnRsp;
import com.qmfresh.promotion.platform.domain.service.vo.CouponPreferentialBean;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityConvertPO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromCouponInfoDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromExecuteStrategyDTO;

import java.util.List;

/**
 * 会员价服务
 */
public interface CouponService {

    /**
     * 验证优惠券
     * @param context
     * @param userId
     * @param traceId
     */
    CouponPreferentialBean checkCoupon(PromotionContext context, PromCouponInfoDTO couponInfo, Integer userId, String traceId);



    /**
     * 发放优惠券
     */
    List<SendCouponReturnBean> sendCoupon(PromActivityConvertPO convertPO, String traceId);

}
