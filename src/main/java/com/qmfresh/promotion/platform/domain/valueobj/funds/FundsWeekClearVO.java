package com.qmfresh.promotion.platform.domain.valueobj.funds;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ToString
public class FundsWeekClearVO implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long timeStart;
    private Long timeEnd;
    private String remark;
    private BigDecimal reward;
    private Long shopId;
    private String shopName;
    private Integer changeType;
    private BigDecimal shopWeekClearPercent;
    private BigDecimal areaWeekClearPercent;
    private BigDecimal minPromPrice;
}
