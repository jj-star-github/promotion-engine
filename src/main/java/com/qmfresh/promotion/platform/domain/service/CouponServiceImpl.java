package com.qmfresh.promotion.platform.domain.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.bean.coupon.CouponCheckParam;
import com.qmfresh.promotion.bean.coupon.SendCouponRequestBean;
import com.qmfresh.promotion.bean.coupon.SendCouponReturnBean;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.dto.PromotionMzCampOnRsp;
import com.qmfresh.promotion.external.service.ICouponExternalApiService;
import com.qmfresh.promotion.platform.domain.service.vo.CouponPreferentialBean;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityConvertPO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.CouponCheckResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.CouponGoodsInfo;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromCouponInfoDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 */
@Component
@Slf4j
public class CouponServiceImpl implements CouponService {

    @Resource
    private ICouponExternalApiService couponExternalApiService;

    @Override
    public CouponPreferentialBean checkCoupon(PromotionContext context, PromCouponInfoDTO couponInfo, Integer userId, String traceId) {
        CouponCheckParam param = new CouponCheckParam();
        param.setUserId(userId);
        param.setCouponCode(couponInfo.getCouponCode());
        param.setBizChannel(couponInfo.getBizChannel());
        List<CouponCheckParam.GoodsInfo> goodsInfoList = new ArrayList<>();
        context.getSsuContexts().forEach(ssuContext -> {
            CouponCheckParam.GoodsInfo goodInfo = new CouponCheckParam.GoodsInfo(ssuContext);
            goodsInfoList.add(goodInfo);
        });
        param.setGoodsList(goodsInfoList);

        CouponCheckResultDTO coupon = couponExternalApiService.checkCoupon(param);
        log.info("优惠券验证信息，traceId={} param={} coupon={}", traceId, JSON.toJSONString(param), JSON.toJSONString(coupon));
        if(null == coupon) {
            log.error("优惠券服务异常-traceId={} couponInfo={}", traceId, JSON.toJSONString(coupon));
            coupon = new CouponCheckResultDTO();
            coupon.setSuccessFlag(false);
            coupon.setReason("优惠券暂无法使用");
        }
        context.setCouponCheckInfo(coupon);
        CouponPreferentialBean couponPreferentialBean = null;
        if (coupon.getSuccessFlag()) {
            couponPreferentialBean = new CouponPreferentialBean();
            couponPreferentialBean.setCouponDiscount(coupon.getDiscount());
            couponPreferentialBean.setCouponPreferentialMoney(coupon.getPreferentialMoney());
            couponPreferentialBean.setPreferentialType(coupon.getPreferentialType());
            List skuIdList = coupon.getUsefulGoodsList().stream().map(CouponGoodsInfo::getSkuId).distinct().collect(Collectors.toList());;
            couponPreferentialBean.setUseCouponSkuId(skuIdList);
        }
        return couponPreferentialBean;
    }

    @Override
    public List<SendCouponReturnBean> sendCoupon(PromActivityConvertPO convertPO, String traceId) {
        List<String> couponCode = JSON.parseObject(convertPO.getRemark(), new TypeReference<List<String>>() {});
        SendCouponRequestBean requestBean = new SendCouponRequestBean();
        requestBean.setActivityId(convertPO.getAwardId().intValue());
        requestBean.setSendNum(couponCode.size());
        requestBean.setCouponCode(couponCode);
        requestBean.setUserId(convertPO.getUserId().longValue());
        requestBean.setSourceOrderNo(convertPO.getBusinessCode());
        requestBean.setSourceShopId(convertPO.getShopId());
        try {
            return couponExternalApiService.sendCoupon(requestBean);
        } catch (BusinessException e) {
            log.warn("优惠券发放失败，traceId={} errorMsg={}", traceId, e.getErrorMsg());
        }
        return null;
    }
}
