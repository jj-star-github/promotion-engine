package com.qmfresh.promotion.platform.domain.model.promactivity.enums;

/**
 *
 */
public enum ActivityCacheKey {
    ACTIVITY_BY_SHOPTYPE,
    CREATE_LOCK, //创建活动防止重复提交
    CAMP_ON_LOCK, //预占防止重复
    SEND_AWARD_LOCK, //领取奖励防止重复
    ;
    private final String value;

    ActivityCacheKey() {
        this.value = "PROMOTION:PROM:ACTIVITY" + name();
    }

    public String key() {
        return value;
    }

    public String key(Object... params) {
        StringBuilder key = new StringBuilder(value);
        if (params != null && params.length > 0) {
            for (Object param : params) {
                if (null != param) {
                    key.append(':');
                    key.append(String.valueOf(param));
                }
            }
        }
        return key.toString();
    }
}
