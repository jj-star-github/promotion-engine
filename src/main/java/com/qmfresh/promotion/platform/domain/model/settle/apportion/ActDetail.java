package com.qmfresh.promotion.platform.domain.model.settle.apportion;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@ToString
@Data
public class ActDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    private Integer priceId;
    /**
     * 活动Id
     */
    private Long actId;
    /**
     * 活动类型 11=门店改价 12=总部改价 13=会员价设置 14=区域改价
     */
    private Integer promotionType;
    /**
     * 活动值类型 1活动金额值、2打折值
     */
    private Integer cutType;
    /**
     * 活动值
     */
    private BigDecimal cutValue;
    private Integer status;
    private Integer createTime;
    private Integer updateTime;


}
