package com.qmfresh.promotion.platform.domain.model.funds.impl;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.FundsApplyDTO;
import com.qmfresh.promotion.bean.promotion.*;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsApply;
import com.qmfresh.promotion.platform.domain.model.funds.IFundsApplyManager;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.FundsApplyMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsApplyDayStatis;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.CheckFundsApplyBillParamDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.FundsApplyCountDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.FundsApplyCountQueryDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.PageQueryFundsApplyCheckParamDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.List;

@Slf4j
@Component
public class FundsApplyManagerImpl implements IFundsApplyManager {
    @Resource
    private FundsApplyMapper fundsApplyMapper;
    @Override
    public Boolean createFundsApplyBill(FundsApplyDTO fundsApplyParam) {
        FundsApply fundsApply =new FundsApply();
        BeanUtils.copyProperties(fundsApplyParam,fundsApply);
        int result = fundsApplyMapper.insertFundsApplyBill(fundsApply);
        return result > 0 ? true : false;
    }

    @Override
    public PageDTO<FundsApply> pageQueryFundsApply(PageQueryFundsApplyParamDTO param) {
        PageDTO<FundsApply> pageList = new PageDTO<>();
        //总页数
        int totalCount = fundsApplyMapper.countByCondition(param);
        List<FundsApply> fundsApplies = fundsApplyMapper.pageQueryFundsApply(param, param.getStart(), param.getEnd());
        if (fundsApplies == null || fundsApplies.isEmpty()) {
            return pageList;
        }
        pageList.setTotalCount(totalCount);
        pageList.setRecords(fundsApplies);
        return pageList;

    }

    @Override
    public PageDTO<FundsApply> pageQueryFundsApplyCheckList(PageQueryFundsApplyCheckParamDTO param) {
        PageDTO<FundsApply> pageList = new PageDTO<>();
        //总页数
        int totalCount = fundsApplyMapper.countByCheckCondition(param);
        log.info("pageQueryFundsApplyCheckList请求参数："+JSON.toJSONString(param));
        List<FundsApply> fundsApplies = fundsApplyMapper.pageQueryFundsApplyCheck(param, param.getStart(), param.getEnd());
        if (fundsApplies == null || fundsApplies.isEmpty()) {
            return pageList;
        }
        pageList.setTotalCount(totalCount);
        pageList.setRecords(fundsApplies);
        return pageList;
    }

    @Override
    public PageDTO<FundsApply> fundsAccountList(PageQueryFundsApplyCheckParamDTO param) {
        PageDTO<FundsApply> pageList = new PageDTO<>();
        //总页数
        int totalCount = fundsApplyMapper.countFundsAccountList(param);
        log.info("fundsAccountList请求参数："+JSON.toJSONString(param));
        List<FundsApply> fundsApplies = fundsApplyMapper.fundsAccountList(param, param.getStart(), param.getEnd());
        if (fundsApplies == null || fundsApplies.isEmpty()) {
            return pageList;
        }
        pageList.setTotalCount(totalCount);
        pageList.setRecords(fundsApplies);
        return pageList;
    }

    @Override
    public FundsApply queryFundsApplyBill(CheckFundsApplyBillParamDTO param) {
        FundsApply fundsApply = fundsApplyMapper.selectByfundsId(param.getId());
        return fundsApply;
    }

    @Override
    public Boolean upFundsApplyStatus(FundsApply param) {
        int fundsApply = fundsApplyMapper.upFundsApplyStatus(param);
        return fundsApply > 0  ? true :false;
    }

    @Override
    public FundsApplyCountDTO queryCheckApplyCount(FundsApplyCountQueryDTO param) {
        FundsApplyCountDTO fundsApplyCountDTO = fundsApplyMapper.queryCheckApplyCount(param);
        return fundsApplyCountDTO;
    }

    @Override
    public Boolean hasFundsApplayStatus(PageQueryFundsApplyParamDTO param) {
        List<FundsApply> fundsApplies = fundsApplyMapper.hasFundsApplayStatus(param);
        return  CollectionUtils.isEmpty(fundsApplies) ? false : true;
    }

    @Override
    public List<FundsApplyDayStatis> queryDayStatis(Integer shopId, Integer shopType, Integer beginTime, Integer endTime) {
        return fundsApplyMapper.queryDayStatis(shopId, shopType, beginTime, endTime);
    }
}
