package com.qmfresh.promotion.platform.domain.model.activity.cp.shop;

import com.qmfresh.promotion.platform.domain.model.activity.cp.context.CpContext;
import com.qmfresh.promotion.platform.domain.model.activity.cp.context.ShopCpContext;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.command.ShopCpChangedQueryCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.command.ShopCpQueryCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.command.ShopCpStartCommand;
import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ChangePriceActivityPO;

/**
 * as Repository
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface ShopCpActivityManager {


    ShopCpActivity create(CpContext cpContext);

    /**
     * 查询将要启动的改价活动
     *
     * @param pageNum  页码
     * @param pageSize 页大小
     * @return 分页改价对象
     */
    Page<ShopCpActivity> toStartShopCpActivity(Integer pageNum, Integer pageSize);

    /**
     * 启动一个门店改价活动
     *
     * @param shopCpActivity 门店改价活动
     */
    void start(ShopCpContext shopCpContext, ShopCpActivity shopCpActivity);

    /**
     * 关闭门店改价
     *
     * @param shopCpContext 门店改价上下文
     */
    void close(ShopCpContext shopCpContext);

    void closeByJob();

    /**
     * 分页查询门店改价
     *
     * @return 分页改价列表
     */
    Page<ShopCpActivity> pageShopCpActivity(ShopCpContext shopCpContext);

    Page<ShopCpActivity> pageShopCpActivity(ShopCpQueryCommand shopCpQueryCommand);


    ShopCpActivity shopCpActivity(Long activityCpId);

    ShopCpActivity shopCpActivityByActivityId(Long activityId);

    Page<ShopCpActivity> pageShopCpActivityChanged(ShopCpChangedQueryCommand shopCpChangedQueryCommand);

    void startByJob();

    void startOneShopCp(ChangePriceActivityPO changePriceActivityPO, ShopCpStartCommand shopCpStartCommand);

}
