package com.qmfresh.promotion.platform.domain.model.activity.cp.log.command;

import com.qmfresh.promotion.platform.domain.model.activity.Subject;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.command.AreaCpCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.command.GeneralCpCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLogType;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
@AllArgsConstructor
public class CpCreateLogCreateCommand {
    /**
     * 请求的转换必须过下
     *
     * @see com.qmfresh.promotion.platform.domain.model.activity.cp.command.CpCreateCommand
     */
    private String request;
    /**
     * 操作者
     */
    private Operator operator;

    private CpBizType cpBizType;

    private CpCreateLogType logType;

    public static CpCreateLogCreateCommand create(CpBizType cpBizType, String request, Integer operatorId, String operatorName) {
        return create(cpBizType, request, operatorId, operatorName,
                CpCreateLogType.MANUAL);
    }

    /**
     * 创建改价创建日志
     *
     * @param cpBizType
     * @param request      请求参数
     * @param operatorId   用户id
     * @param operatorName 用户名称
     * @return 创建命令
     */
    public static CpCreateLogCreateCommand create(CpBizType cpBizType, String request, Integer operatorId, String operatorName,
                                                  CpCreateLogType logType) {

        if (cpBizType == null) {
            throw new BusinessException("必须设置改价类型");
        }
        if (CpBizType.CP_SHOP.equals(cpBizType)) {
            throw new BusinessException("不支持门店改价类型");
        }

        if (operatorId == null) {
            throw new BusinessException("必须设置操作人id");
        }
        if (operatorName == null) {
            throw new BusinessException("必须设置操作人名称");
        }
        if (StringUtils.isBlank(request)) {
            throw new BusinessException("请求参数不能为空");
        }
        if (CpBizType.CP_PLATFORM.equals(cpBizType)) {
            GeneralCpCreateCommand.check(request);
        }
        if (CpBizType.CP_AREA.equals(cpBizType)) {
            AreaCpCreateCommand.check(cpBizType);
        }

        if (logType == null) {
            throw new BusinessException("必须设置类型");
        }

        CpCreateLogCreateCommand cpCreateLogCreateCommand = CpCreateLogCreateCommand.builder()
                .request(request)
                .operator(Operator.create(operatorId, operatorName, Subject.create(Subject.Platform.INTANCE)))
                .cpBizType(cpBizType)
                .logType(logType)
                .build();
        return cpCreateLogCreateCommand;
    }
}
