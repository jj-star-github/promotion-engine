package com.qmfresh.promotion.platform.domain.model.activity.cp.shop.command;

import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class ShopCpStartCommand implements Serializable {
    /**
     * 操作人
     */
    private Operator operator;
    /**
     * 门店id
     */
    private Integer shopId;
    /**
     * 商品id
     */
    private Integer skuId;
    /**
     * 备注
     */
    private String remark;

    private Long activityCpId;

    public static ShopCpStartCommand create(
            Operator operator,
            Integer shopId,
            Integer skuId,
            String remark,
            Long activityCpId
    ) {
        if (operator == null) {
            throw new BusinessException("操作者不能为空");
        }
        if (shopId == null) {
            throw new BusinessException("门店id不能为空");
        }
        if (skuId == null) {
            throw new BusinessException("门店id不能为空");
        }
        if (activityCpId == null) {
            throw new BusinessException("改价活动明细不能为空");
        }
        ShopCpStartCommand shopCpStartCommand = ShopCpStartCommand.builder()
                .operator(operator)
                .shopId(shopId)
                .skuId(skuId)
                .remark(remark)
                .activityCpId(activityCpId)
                .build();

        return shopCpStartCommand;
    }

    public static ShopCpStartCommand create(String remark) {
        ShopCpStartCommand shopCpStartCommand = ShopCpStartCommand.builder()
                .remark(remark)
                .build();
        return shopCpStartCommand;
    }
}
