package com.qmfresh.promotion.platform.domain.model.activity;

import lombok.Getter;

import java.io.Serializable;

/**
 * 当事人的设计可以是组织可以是人
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public class Subject<T> implements Serializable {

    private Integer subjectId;
    private String subjectName;
    private T extend;
    private Type type;

    public enum Type {
        SHOP,
        PLATFORM,
        AREA
    }

    public static <T> Subject<T> create(Integer subjectId, String subjectName, T object, Type type) {
        Subject<T> subject = new Subject<>();
        subject.subjectId = subjectId;
        subject.subjectName = subjectName;
        subject.extend = object;
        subject.type = type;
        return subject;
    }

    /**
     * 创建门店主体
     *
     * @param shop 门店id
     * @return 主体
     */
    public static Subject<Shop> create(Shop shop) {
        return create(shop.getShopId(), shop.getShopName(), shop, Type.SHOP);
    }

    public static Subject<Platform> create(Platform platform) {
        return create(null, null, platform, Type.PLATFORM);
    }

    public static Subject<Long> create(Long accountId) {
        return create(accountId.intValue(), "", accountId, Type.AREA);
    }

    public static Subject<Long> createAreaEmpty() {
        return create(0L);
    }


    public static class Platform {
        public static final Platform INTANCE = new Platform();

        private Platform() {
        }

        @Override
        public boolean equals(Object obj) {
            return Platform.class.getName().equals(obj.getClass().getName());
        }

        @Override
        public int hashCode() {
            return Platform.class.getName().hashCode();
        }
    }

}
