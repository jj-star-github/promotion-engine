package com.qmfresh.promotion.platform.domain.service;

import lombok.Getter;

import java.util.Objects;

/**
 * 优惠券折扣类型
 **/
@Getter
public enum CouponReduceTypeEnum {

    MONEY(1, "金额"),
    DISCOUNT(2, "折扣");

    public final Integer code;
    public final String name;

    CouponReduceTypeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public static CouponReduceTypeEnum get(Integer code) {
        for (CouponReduceTypeEnum value : values()) {
            if (Objects.equals(value.code, code)) {
                return value;
            }
        }
        return null;
    }
}
