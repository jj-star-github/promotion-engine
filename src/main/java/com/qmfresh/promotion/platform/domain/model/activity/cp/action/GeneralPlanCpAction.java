package com.qmfresh.promotion.platform.domain.model.activity.cp.action;

import lombok.Data;

import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class GeneralPlanCpAction implements CpAction {

    private List<Integer> shopIds;
//    private List<>
}
