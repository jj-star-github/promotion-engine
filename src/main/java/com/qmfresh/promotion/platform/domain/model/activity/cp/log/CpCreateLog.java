package com.qmfresh.promotion.platform.domain.model.activity.cp.log;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.command.CpCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.command.CpCreateLogCreateCommand;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class CpCreateLog implements Serializable {

    private Long id;
    private String request;
    private CpCreateLogStatus status;
    private CpCreateLogCompletedStatus completedStatus;
    private Operator operator;
    private Date gmtCreate;
    private Date gmtModified;
    private String name;
    private CpCreateLogType logType;
    private Long activityId;
    private String remark;

    public static CpCreateLog create(CpCreateCommand cpCreateCommand, CpCreateLogCreateCommand createLogCreateCommand) {
        String request = JSON.toJSONString(cpCreateCommand);
        Long activityId = cpCreateCommand.getActivityId();
        String activityName = cpCreateCommand.getActivityName();
        return create(request, activityName, activityId, createLogCreateCommand);
    }

    public static CpCreateLog create(String request, String activityName, Long activityId, CpCreateLogCreateCommand createLogCreateCommand) {
        return CpCreateLog.builder()
                .request(request)
                .status(CpCreateLogStatus.CREATING)
                .completedStatus(CpCreateLogCompletedStatus.TO_RUNNING)
                .operator(createLogCreateCommand.getOperator())
                .gmtCreate(new Date())
                .gmtModified(new Date())
                .name(activityName)
                .activityId(activityId)
                .logType(createLogCreateCommand.getLogType())
                .build();
    }
}


