package com.qmfresh.promotion.platform.domain.shared;

import lombok.Getter;
import lombok.ToString;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@ToString
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = -6003868869041167435L;

    private int errorCode;
    private String errorMsg;
    private Throwable e;

    public BusinessException(int errorCode) {
        super();
        this.errorCode = errorCode;
    }

    public BusinessException(String errorMsg) {
        super();
        this.errorMsg = errorMsg;
        this.errorCode = BizCode.ERROR.getCode();
    }

    public BusinessException(int errorCode, String errorMsg) {
        super();
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public BusinessException(int errorCode, Throwable e) {
        super(e);
        this.errorCode = errorCode;
        this.e = e;
    }

    public BusinessException(int errorCode, String errorMsg, Throwable e) {
        super(e);
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
        this.e = e;
    }

}
