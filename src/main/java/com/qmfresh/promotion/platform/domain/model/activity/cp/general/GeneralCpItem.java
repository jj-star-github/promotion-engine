package com.qmfresh.promotion.platform.domain.model.activity.cp.general;

import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.Sku;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpType;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class GeneralCpItem implements Serializable {

    private Long activityId;
    private Long activityCpId;
    private String activityName;
    private CpBizType cpBizType;
    private Shop shop;
    private Sku sku;
    private BigDecimal originalPrice;
    private BigDecimal currentPrice;
    private BigDecimal rulePrice;
    private BigDecimal marketingDiffFee;
    private CpTimeCycle cpTimeCycle;
    private CpType cpType;
    private String lastOperatorName;

}
