package com.qmfresh.promotion.platform.domain.model.activity.cp.vip;

import lombok.Getter;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public class VipCpActivity implements Serializable {

    private Long activityId;
}
