package com.qmfresh.promotion.platform.domain.model.activity.cp.log;

import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Getter;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public enum CpCreateLogStatus {
    /**
     * 待创建
     */
    CREATING(1),
    /**
     * 已完成
     */
    COMPLETED(2);

    private Integer code;

    CpCreateLogStatus(Integer code) {
        this.code = code;
    }

    public static CpCreateLogStatus from(Integer status) {

        for (CpCreateLogStatus i : CpCreateLogStatus.values()) {
            if (i.getCode().equals(status)) {
                return i;
            }
        }
        throw new BusinessException("完成状态不支持");
    }}
