package com.qmfresh.promotion.platform.domain.model.activity.cp.shop.command;

import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Builder
@Getter
public class ShopCpCreateCommand implements Serializable {
    /**
     * 操作人
     */
    private Operator operator;
    /**
     * 门店id
     */
    private Integer shopId;
    /**
     * skuId
     */
    private Integer skuId;
    /**
     * 计划营销费用
     */
    private BigDecimal planMarketingAmount;
    /**
     * 改价目标价
     */
    private BigDecimal rulePrice;
    /**
     * 改价时间段
     */
    private CpTimeCycle cpTimeCycle;
    /**
     * 备注
     */
    private String remark;

    public static ShopCpCreateCommand create(
            Operator operator,
            Integer shopId,
            Integer skuId,
            BigDecimal planMarketingAmount,
            BigDecimal rulePrice,
            CpTimeCycle cpTimeCycle,
            String remark
    ) {
        if (operator == null) {
            throw new BusinessException("操作人必须设置");
        }
        if (shopId == null) {
            throw new BusinessException("门店id不能为空");
        }
        if (skuId == null) {
            throw new BusinessException("商品id不能为空");
        }

        if (cpTimeCycle == null) {
            throw new BusinessException("必须设置改价周期");
        }

        ShopCpCreateCommand createCommand = ShopCpCreateCommand.builder()
                .operator(operator)
                .shopId(shopId)
                .skuId(skuId)
                .planMarketingAmount(planMarketingAmount)
                .rulePrice(rulePrice)
                .cpTimeCycle(cpTimeCycle)
                .remark(remark)
                .build();
        return createCommand;
    }

}
