package com.qmfresh.promotion.platform.domain.model.activity.cp.area;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qmfresh.promotion.platform.domain.model.activity.CpCheck;
import com.qmfresh.promotion.platform.domain.model.activity.SkuManager;
import com.qmfresh.promotion.platform.domain.model.activity.SkuPrice;
import com.qmfresh.promotion.platform.domain.model.activity.cp.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.command.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.event.CpEvent;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPrice;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPriceCompute;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.command.CpActivitySpec;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.command.PromotionPriceCommand;
import com.qmfresh.promotion.platform.domain.model.support.MqLog;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.ChangePriceActivityMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ChangePriceActivityPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.support.MqLogMapper;
import com.qmfresh.promotion.platform.infrastructure.message.RocketMQSender;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class AreaCpActivityManagerImpl implements AreaCpActivityManager {

    @Resource
    private ChangePriceActivityMapper changePriceActivityMapper;
    @Resource
    private SkuManager skuManager;
    @Resource
    private CpCheck cpCheck;
    @Resource
    private DefaultMQProducer defaultMQProducer;
    @Resource
    private MqLogMapper mapper;
    @Resource
    private PromotionPriceCompute promotionPriceCompute;


    @Override
    public void start(AreaCpStartCommand startCommand) {
        //启动大区改价
        LambdaQueryWrapper<ChangePriceActivityPO> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ChangePriceActivityPO::getIsDeleted, 0);
        lambdaQueryWrapper.eq(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode());
        lambdaQueryWrapper.eq(ChangePriceActivityPO::getAuditStatus, AuditStatus.AUDIT_SUCCESS.getCode());
        lambdaQueryWrapper.eq(ChangePriceActivityPO::getType, CpBizType.CP_AREA.getCode());
        lambdaQueryWrapper.le(ChangePriceActivityPO::getPlanActivityStartTime, new Date());
        lambdaQueryWrapper.ge(ChangePriceActivityPO::getPlanActivityEndTime, new Date());
        lambdaQueryWrapper.eq(startCommand.getActivityId() != null, ChangePriceActivityPO::getActivityId, startCommand.getActivityId());

        ChangePriceActivityPO.visit(changePriceActivityMapper,
                lambdaQueryWrapper,
                startCommand,
                this::doStart);
    }

    /**
     * 底层模型是当前只有一类活动
     *
     * @param changePriceActivityPO 促销改价
     * @param areaCpQueryCommand    大区查询命令
     */
    private void doStart(ChangePriceActivityPO changePriceActivityPO, AreaCpStartCommand areaCpQueryCommand) {
        try {

            Integer shopId = changePriceActivityPO.getShopId();
            Integer skuId = changePriceActivityPO.getSkuId();

            if (CpStatus.RUNNING.getCode() == changePriceActivityPO.getStatus()) {
                log.warn("活动已运行：shopId={} skuId={} activityCpId={}", shopId, skuId, changePriceActivityPO.getId());
                return;
            }
            SkuPrice skuPrice = skuManager.shopSkuPrice(shopId, skuId);
            if (skuPrice == null) {
                log.warn("商品未售卖该品：shopId={} skuId={}", shopId, skuId);
                //todo 新增remark
                return;
            }
            CpActivitySpec spec = CpActivitySpec.spec(changePriceActivityMapper, shopId, skuId);
            spec.setAreaCpItem(ChangePriceActivityPO.areaCpItem(changePriceActivityPO));
            //计算促销价
            PromotionPriceCommand priceCommand = PromotionPriceCommand
                    .create(shopId, skuId, skuPrice.getOriginalPrice(), spec);
            PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
            //发送促销价
            promotionPriceCompute.snapshot(promotionPrice, areaCpQueryCommand.getRemark());

            //发送大区变更状态
            ChangePriceActivityPO update = new ChangePriceActivityPO();
            update.setStatus(CpStatus.RUNNING.getCode());
            update.setGmtModified(new Date());
            update.setRemark(areaCpQueryCommand.getRemark());
            int result = changePriceActivityMapper.update(update,
                    new LambdaQueryWrapper<ChangePriceActivityPO>()
                            .eq(ChangePriceActivityPO::getId, changePriceActivityPO.getId())
                            .eq(ChangePriceActivityPO::getIsDeleted, 0)
                            .eq(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode())
            );
            log.info("已启动大区活动：shopId={} skuId={} activityCpId={}", shopId, skuId, changePriceActivityPO.getId());
            if (result > 0) {
                log.warn("改价状态为变更：activityCpId = {},shopId={} skuId={}", changePriceActivityPO.getId(),
                        changePriceActivityPO.getShopId(), changePriceActivityPO.getSkuId());
            } else {
                changePriceActivityPO.setStatus(CpStatus.RUNNING.getCode());
                RocketMQSender.doSend(defaultMQProducer, CpEvent.CP_EVENT_TOPIC,
                        CpEvent.create(changePriceActivityPO), new MqLog.PersistCallback(mapper));
            }
        } catch (Exception e) {
            log.warn("启动大区改价：shopId={},skuId={}", changePriceActivityPO.getShopId(), changePriceActivityPO.getSkuId(), e);
            throw new BusinessException("启动大区改价异常");
        }
    }


    @Override
    public void close(AreaCpCloseCommand cpCloseCommand) {
        //关闭大区改价
        LambdaQueryWrapper<ChangePriceActivityPO> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ChangePriceActivityPO::getIsDeleted, 0);
        lambdaQueryWrapper.in(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode(), CpStatus.RUNNING.getCode());
        lambdaQueryWrapper.eq(ChangePriceActivityPO::getAuditStatus, AuditStatus.AUDIT_SUCCESS.getCode());
        lambdaQueryWrapper.eq(ChangePriceActivityPO::getType, CpBizType.CP_AREA.getCode());
        if (cpCloseCommand.getActivityId() == null) {
            lambdaQueryWrapper.le(ChangePriceActivityPO::getPlanActivityEndTime, new Date());
        }
        lambdaQueryWrapper.eq(cpCloseCommand.getActivityId() != null, ChangePriceActivityPO::getActivityId, cpCloseCommand.getActivityId());

        ChangePriceActivityPO.visit(changePriceActivityMapper,
                lambdaQueryWrapper,
                cpCloseCommand,
                this::doClose);


    }

    private void doClose(ChangePriceActivityPO changePriceActivityPO, AreaCpCloseCommand cpCloseCommand) {
        Integer shopId = changePriceActivityPO.getShopId();
        Integer skuId = changePriceActivityPO.getSkuId();
        Integer status = changePriceActivityPO.getStatus();

        if (CpStatus.CLOSED.getCode() == changePriceActivityPO.getStatus()
                || CpStatus.FINISHED.getCode() == changePriceActivityPO.getStatus()
                || CpStatus.CANCELED.getCode() == changePriceActivityPO.getStatus()
        ) {
            log.warn("活动已结束：shopId={} skuId={} activityCpId={}", shopId, skuId, changePriceActivityPO.getId());
            return;
        }

        //发送大区变更状态
        ChangePriceActivityPO update = new ChangePriceActivityPO();
        if (cpCloseCommand.getActivityId() != null) {
            if (CpStatus.CREATE.getCode() == changePriceActivityPO.getStatus()) {
                update.setStatus(CpStatus.CANCELED.getCode());
            } else {
                update.setStatus(CpStatus.CLOSED.getCode());
            }
        } else {
            update.setStatus(CpStatus.FINISHED.getCode());
        }
        update.setGmtModified(new Date());
        update.setRemark(cpCloseCommand.getRemark());
        int result = changePriceActivityMapper.update(update,
                new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getId, changePriceActivityPO.getId())
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
        );
        log.info("已关闭大区活动：shopId={} skuId={} activityCpId={}", shopId, skuId, changePriceActivityPO.getId());
        if (result > 0) {
            log.warn("改价状态为变更：activityCpId = {},shopId={} skuId={}", changePriceActivityPO.getId(),
                    changePriceActivityPO.getShopId(), changePriceActivityPO.getSkuId());
        } else {
            if (cpCloseCommand.getActivityId() != null) {
                changePriceActivityPO.setStatus(CpStatus.CLOSED.getCode());
            } else {
                changePriceActivityPO.setStatus(CpStatus.FINISHED.getCode());
            }

            RocketMQSender.doSend(defaultMQProducer, CpEvent.CP_EVENT_TOPIC,
                    CpEvent.create(changePriceActivityPO), new MqLog.PersistCallback(mapper));
        }

        if (CpStatus.CREATE.getCode() == status) {
            log.info("待生效状态，关闭不同步改价信息：shopId={} skuId={}", shopId, skuId);
            return;
        }

        //促销价计算 //todo
        SkuPrice skuPrice = skuManager.shopSkuPrice(shopId, skuId);
        if (skuPrice == null) {
            log.warn("商品未售卖该品：shopId={} skuId={} activityCpId={}", shopId, skuId, changePriceActivityPO.getId());
            return;
        }
        String remark = cpCloseCommand.getRemark();
        CpActivitySpec spec = CpActivitySpec.spec(changePriceActivityMapper, shopId, skuId);

        if (spec.getAreaCpItem() != null && spec.getAreaCpItem().getActivityCpId().equals(changePriceActivityPO.getId())) {
            log.info("获取的大区活动是其本身：shopId={} skuId={} cpId={} activityId={}",
                    shopId, skuId, spec.getAreaCpItem().getActivityCpId(), spec.getAreaCpItem().getActivityId());
            spec.setAreaCpItem(null);
        }

        //计算促销价
        PromotionPriceCommand priceCommand = PromotionPriceCommand
                .create(shopId, skuId, skuPrice.getOriginalPrice(), spec);
        PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
        //发送促销价
        promotionPriceCompute.snapshot(promotionPrice, remark);


    }

    @Override
    public Page<AreaCpActivity> pageAreaCpList(AreaCpQueryCommand queryCommand) {

        //分页查询
        IPage<ChangePriceActivityPO> priceActivityPOIPage = changePriceActivityMapper
                .selectPage(new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(queryCommand.getPageNum(), queryCommand.getPageSize()),
                        new QueryWrapper<ChangePriceActivityPO>()
                                .eq("is_deleted", 0)
                                .eq("type", CpBizType.CP_AREA.getCode())
                                .eq(queryCommand.getActivityId() != null, "activity_id", queryCommand.getActivityId())
                                .like(StringUtils.isNotBlank(queryCommand.getActivityNameKey()), "name", queryCommand.getActivityNameKey())
                                .eq(queryCommand.getShopId() != null, "shop_id", queryCommand.getShopId())
                                .eq(queryCommand.getSkuId() != null, "sku_id", queryCommand.getSkuId())
                                .in(CollectionUtils.isNotEmpty(queryCommand.getStatusList()), "status", queryCommand.getStatusList())
                                .gt(queryCommand.getFromBeginCpActivityStartTime() != null, "plan_activity_start_time", queryCommand.getFromBeginCpActivityStartTime())
                                .le(queryCommand.getToBeginCpActivityStartTime() != null, "plan_activity_start_time", queryCommand.getToBeginCpActivityStartTime())
                                .groupBy("activity_id")
                                .orderByDesc("id")
                                .select(
                                        "activity_id",
                                        "name",
                                        "status",
                                        "type",
                                        "create_person_name",
                                        "plan_activity_start_time",
                                        "plan_activity_end_time",
                                        "cp_type",
                                        "count(distinct sku_id) as sku_count",
                                        "count(distinct shop_id) as shop_count",
                                        "sum(plan_marketing_amount) as sum_plan_marketing_fee",
                                        "gmt_create",
                                        "last_operator_name"
                                )

                );

        return Page.convertIPage(queryCommand.getPageNum(), queryCommand.getPageSize(),
                priceActivityPOIPage, ChangePriceActivityPO::areaCpActivity);
    }

    @Override
    public Page<SkuCpItem> pageAreaSkuList(AreaCpSkuQueryCommand queryCommand) {
        IPage<ChangePriceActivityPO> priceActivityPOIPage =
                changePriceActivityMapper.selectPage(
                        new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(queryCommand.getPageNum(), queryCommand.getPageSize()),
                        new QueryWrapper<ChangePriceActivityPO>()
                                .eq("is_deleted", 0)
                                .eq("type", CpBizType.CP_AREA.getCode())
                                .eq("activity_id", queryCommand.getActivityId())
                                .eq(queryCommand.getSkuId() != null, "sku_id", queryCommand.getSkuId())
                                .eq(StringUtils.isNotBlank(queryCommand.getClass3NameKey()), "class3_name", queryCommand.getClass3NameKey())
                                .orderByDesc("gmt_modified")
                                .groupBy("sku_id")
                                .select("sku_id",
                                        "rule_price",
                                        "class1_name",
                                        "class2_name",
                                        "class3_name",
                                        "sku_name",
                                        "activity_id")

                );
        return Page.convertIPage(queryCommand.getPageNum(), queryCommand.getPageSize(), priceActivityPOIPage,
                ChangePriceActivityPO::skuCpItem);
    }

    /**
     * todo  check the index
     * idx: is_deleted_activity_id_status_sku_id
     * idx: is_deleted_activity_id_status_shop_id
     *
     * @param skuEditCommand 商品编辑命令
     */
    @Override
    public void editSku(AreaCpSkuEditCommand skuEditCommand) {
        log.info("开始编码活动的商品:skuEditCommand={}", JSON.toJSONString(skuEditCommand));

        //check活动状态
        boolean ready = cpCheck.onReady(skuEditCommand.getActivityId());
        if (!ready) {
            log.warn("运行中活动，无法修改：activityId={}", skuEditCommand.getActivityId());
            throw new BusinessException("运行中活动，无法修改");
        }

        ChangePriceActivityPO update =
                new ChangePriceActivityPO();
        update.setLastOperatorName(skuEditCommand.getOperator().getOperatorName());
        update.setLastOperatorId(skuEditCommand.getOperator().getOperatorId().longValue());
        update.setGmtModified(new Date());
        update.setRulePrice(skuEditCommand.getRulePrice());
        int i = changePriceActivityMapper.update(update,
                new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getActivityId, skuEditCommand.getActivityId())
                        .eq(ChangePriceActivityPO::getSkuId, skuEditCommand.getSkuId())
                        .eq(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode())
        );
        log.info("修改活动成功：activityId ={} count={}", skuEditCommand.getActivityId(), i);
        //todo update the createCommand message？
    }

    /**
     * @param skuDelCommand 商品删除命令
     */
    @Override
    public void delSku(AreaCpSkuDelCommand skuDelCommand) {
        log.info("开始删除活动的商品：skuDelCommand={}", JSON.toJSONString(skuDelCommand));
        //check活动状态
        boolean ready = cpCheck.onReady(skuDelCommand.getActivityId());
        if (!ready) {
            log.warn("运行中活动，无法修改：activityId={}", skuDelCommand.getActivityId());
            throw new BusinessException("运行中活动，无法修改");
        }

        int skuNum = cpCheck.cpActivitySkuNum(skuDelCommand.getActivityId());
        if (skuNum <= 1) {
            log.warn("活动activityId ={} 商品数量 count= {}", skuDelCommand.getActivityId(), skuNum);
            throw new BusinessException("商品数量少于2个，请用失效功能");
        }

        ChangePriceActivityPO update =
                new ChangePriceActivityPO();
        update.setLastOperatorName(skuDelCommand.getOperator().getOperatorName());
        update.setLastOperatorId(skuDelCommand.getOperator().getOperatorId().longValue());
        update.setGmtModified(new Date());
        update.setRemark(skuDelCommand.getRemark());
        update.setIsDeleted(1);
        int i = changePriceActivityMapper.update(update,
                new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getActivityId, skuDelCommand.getActivityId())
                        .eq(ChangePriceActivityPO::getSkuId, skuDelCommand.getSkuId())
                        .eq(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode())
        );
        log.info("删除活动商品成功：activityId ={} count={}", skuDelCommand.getActivityId(), i);
    }

    @Override
    public void closeAreaCpActivityByJob() {
        AreaCpCloseCommand areaCpCloseCommand = AreaCpCloseCommand.create(Operator.createSystemOperator(), null, "系统自动关闭");
        close(areaCpCloseCommand);
    }

    @Override
    public void startAreaCpActivityByJob() {
        AreaCpStartCommand areaCpStartCommand = AreaCpStartCommand.create(null, "系统自动开始大区改价");
        start(areaCpStartCommand);
    }
}
