package com.qmfresh.promotion.platform.domain.model.activity.cp.log;

import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Getter;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public enum CpCreateLogType {
    /**
     * 手动
     */
    MANUAL(1),
    /**
     * excel
     */
    EXCEL(2),
    /**
     * 其他系统
     */
    OTHER_SYSTEM(3),
    /**
     * 其他
     */
    OTHERS(4);

    private Integer code;

    CpCreateLogType(Integer code) {
        this.code = code;
    }

    public static CpCreateLogType from(Integer type) {
        for (CpCreateLogType i : CpCreateLogType.values()) {
            if (i.getCode().equals(type)) {
                return i;
            }
        }
        throw new BusinessException("不支持日志类型");

    }}
