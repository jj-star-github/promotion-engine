package com.qmfresh.promotion.platform.domain.valueobj.funds;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ToString
public class FundsSkuVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long skuId; //SKU ID
    private String skuName; //SKU 名
    private BigDecimal originPrice; //原价
    private BigDecimal priceAfterDiscount; //折后价
    private BigDecimal amount; //重量
    private BigDecimal realAmount; //支付金额
    private String skuFormat;  //单位
    private Integer isWeight; //称重为1，否则为0
}
