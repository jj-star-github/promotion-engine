package com.qmfresh.promotion.platform.domain.model.activity.cp.action;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class CloseCpAction<T> implements CpAction {
    /**
     * 改价活动
     */
    private T cpActivity;
    /**
     * 备注
     */
    private String remark;


    /**
     * 改变改价活动
     *
     * @param cpActivity 改价活动
     * @param remark     备注
     * @param <T>        活动类型
     * @return 关闭动作
     */
    public static <T> CloseCpAction<T> create(T cpActivity, String remark) {
        CloseCpAction<T> closeCpAction = new CloseCpAction<>();
        closeCpAction.setCpActivity(cpActivity);
        closeCpAction.setRemark(remark);
        return closeCpAction;
    }

    public static CloseCpAction create(String remark) {
        CloseCpAction closeCpAction = new CloseCpAction();
        closeCpAction.setRemark(remark);
        return closeCpAction;
    }
}
