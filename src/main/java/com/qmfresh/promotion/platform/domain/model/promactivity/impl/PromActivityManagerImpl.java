package com.qmfresh.promotion.platform.domain.model.promactivity.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qmfresh.promotion.entity.*;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityConditionManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityTargetShopManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityTargetSkuManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.enums.*;
import com.qmfresh.promotion.platform.domain.model.promactivity.vo.HitTargetConditionVo;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.PromActivityMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityPO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 *
 */
@Service
@Slf4j
public class PromActivityManagerImpl extends ServiceImpl<PromActivityMapper, PromActivityPO>
        implements PromActivityManager {

    @Resource
    private PromActivityTargetSkuManager skuManager;
    @Resource
    private PromActivityTargetShopManager shopManager;
    @Resource
    private PromActivityConditionManager conditionManager;
    @Resource
    private IQmExternalApiService iQmExternalApiService;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public PromActivityPO queryByActivityId(Long activityId) {
        return baseMapper.queryById(activityId);
    }

    @Override
    public Integer queryCountByParam(PromActivityQueryDTO queryDto) {
        return baseMapper.queryCountByParam(queryDto);
    }

    @Override
    public List<PromActivityPO> queryByParam(PromActivityQueryDTO queryDto) {
        return baseMapper.queryByParam(queryDto, queryDto.getStart(), queryDto.getPageSize());
    }

    @Override
    public void noAvail(Long activityId, String operatorName, Integer operatorId) {
        PromActivityPO activity = baseMapper.queryById(activityId);
        if (null == activity) {
            return;
        }
        //清缓存
        cleanActivityCache(activity);
        baseMapper.noAvail(activityId, operatorName, operatorId, new Date());
    }

    @Override
    public boolean checkActivity(PromActivityCreateDTO param, String traceId, PromActivityPO po) {
        if (null == po) {
            po = new PromActivityPO(param);
        }
        List<PromActivityPO> activityList = baseMapper.queryActivityIdByParam(po);
        if (CollectionUtils.isEmpty(activityList)) {
            return true;
        }
        List<Long> activityIdList = activityList.stream().map(PromActivityPO::getId).collect(Collectors.toList());
        Map<Integer, List<PromActivityPO>> skuTypeMap = activityList.stream().collect(Collectors.groupingBy(PromActivityPO::getSkuType));
        //验证是否存在同品同店的活动
        List<Integer> skuIdList = new ArrayList<>();
        List<Integer> classIdList = new ArrayList<>();
        for(PromActivityTargetSkuDTO skuDto : param.getSkuList()) {
            skuIdList.add(skuDto.getSkuId());
            classIdList.add(skuDto.getClass2Id());
        }
        //全品验证
        //SKU单品验证
        if(SkuTypeEnum.SKU.getCode() == po.getSkuType()) {
            activityIdList = skuManager.queryActivityIdBySkuId(activityIdList, skuIdList, SkuTypeEnum.SKU.getCode());
            activityIdList = skuManager.queryActivityIdBySkuId(activityIdList, classIdList, SkuTypeEnum.CLASS2.getCode());
        }
        //根据CLASS2过滤活动
        if(SkuTypeEnum.CLASS2.getCode() == po.getSkuType()) {
            activityIdList = skuManager.queryActivityIdByClass2(activityIdList, classIdList);
        }
        //获取全品活动ID
        if(SkuTypeEnum.ALL.getCode() != po.getSkuType() && skuTypeMap.containsKey(SkuTypeEnum.ALL.getCode())) {
            List<PromActivityPO> promActivityPOList = skuTypeMap.get(SkuTypeEnum.ALL.getCode());
            activityIdList.addAll(promActivityPOList.stream().map(PromActivityPO::getId).collect(Collectors.toList()));
        }

        if (CollectionUtils.isEmpty(activityIdList)) {
            return true;
        }
        if (ShopTypeEnum.ZB.getCode() == param.getShopType()) {
            log.warn("活动验证未通过-全品和全门店活动已存在, traceId={} ", traceId);
            throw new BusinessException("已经存在活动不可选择全部门店");
        }
        //过滤门店信息
        List<Integer> shopIdList = param.getShopList().stream().map(PromActivityTargetShopDTO::getShopId).collect(Collectors.toList());
        activityIdList = shopManager.queryActivityId(shopIdList, param.getShopType(), activityIdList);
        if (CollectionUtils.isEmpty(activityIdList)) {
            return true;
        }
        throw new BusinessException("目标门店与已存在活动重复，活动ID:"+JSON.toJSONString(activityIdList));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createActivity(PromActivityCreateDTO param) {
        //创建活动信息
        PromActivityPO activityPO = new PromActivityPO(param);
        super.save(activityPO);

        if (param.getShopType() != ShopTypeEnum.ZB.getCode()) { //非全部门店-批量插入门店信息
            if(CollectionUtils.isEmpty(param.getShopList())) {
                throw new BusinessException("请选择对应的门店");
            }
            shopManager.insertBatch(activityPO.getId(), param.getShopList());
        }

        if (param.getSkuType() != SkuTypeEnum.ALL.getCode()) { //非全部商品-批量插入商品信息
            if(CollectionUtils.isEmpty(param.getSkuList())) {
                throw new BusinessException("请选择对应的商品");
            }
            skuManager.insertBatch(activityPO.getId(), param.getSkuList());
        }
        //批量插入奖品
        conditionManager.insertBatch(activityPO.getId(), param.getConditionList());
        //清缓存
        cleanActivityCache(activityPO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateActivity(PromActivityCreateDTO param, PromActivityPO activity, String traceId) {
        //跟新门店信息
        if(ShopTypeEnum.ZB.getCode() != param.getShopType() && CollectionUtils.isNotEmpty(param.getShopList())) {
            List<Integer> actShopIdList = shopManager.queryShopId(param.getId());
            List<PromActivityTargetShopDTO> addShopDTO = new ArrayList<>();
            param.getShopList().forEach(
                    shop->{
                        if (!actShopIdList.remove(shop.getShopId())) {
                            addShopDTO.add(shop);
                        }
                    });
            if (CollectionUtils.isNotEmpty(addShopDTO)) { //批量新增
                shopManager.insertBatch(param.getId(), addShopDTO);
            }
            if(CollectionUtils.isNotEmpty(actShopIdList)) { //批量删除
                shopManager.deletedBatch(param.getId(), actShopIdList);
            }
        }
        //更新商品信息
        if(SkuTypeEnum.ALL.getCode() != param.getSkuType() && CollectionUtils.isNotEmpty(param.getSkuList())) {
            List<Integer> actSkuIdList = skuManager.querySkuId(param.getId());
            List<PromActivityTargetSkuDTO> addSkuDTO = new ArrayList<>();
            param.getSkuList().forEach(
                    sku->{
                        if (!actSkuIdList.remove(sku.getSkuId())) {
                            addSkuDTO.add(sku);
                        }
                    });
            if (CollectionUtils.isNotEmpty(addSkuDTO)) { //批量新增
                skuManager.insertBatch(param.getId(), addSkuDTO);
            }
            if(CollectionUtils.isNotEmpty(actSkuIdList)) { //批量删除
                skuManager.deletedBatch(param.getId(), actSkuIdList);
            }
        }
        PromActivityPO activityPO = new PromActivityPO();
        activityPO.setId(param.getId());
        activityPO.setLastOperatorName(param.getOperatorName());
        activityPO.setLastOperatorId(param.getOperatorId());
        activityPO.setGmtModified(new Date());
        baseMapper.updateById(activityPO);
        //清缓存
        cleanActivityCache(activity);
    }

    @Override
    public List<PromActivityPO> queryByShopId(Integer shopId, Date time, int userType) {
        List<PromActivityPO> activityPOList = new ArrayList<>();
        //获取所有门店活动
        List<PromActivityPO> activityPOS = queryByShopType(time, null, ShopTypeEnum.ZB.getCode(), userType);
        if (CollectionUtils.isNotEmpty(activityPOS)) {
            activityPOList.addAll(activityPOS);
        }
        //获取门店活动
        activityPOS = queryByShopType(time, shopId, ShopTypeEnum.MD.getCode(), userType);
        if (CollectionUtils.isNotEmpty(activityPOS)) {
            activityPOList.addAll(activityPOS);
        }

        Integer dqId = null;
        Integer pqId = null;
        List shopList = new ArrayList();
        shopList.add(shopId);
        AreaConditionQuery param = new AreaConditionQuery();
        param.setShopIds(shopList);
        List<BaseArea> areaDetail = iQmExternalApiService.selectAreaByShopIds(param);
        if (CollectionUtils.isNotEmpty(areaDetail)) {
            dqId = areaDetail.get(0).getId();
            if (CollectionUtils.isNotEmpty(areaDetail.get(0).getChildren())) {
                pqId = areaDetail.get(0).getChildren().get(0).getId();
            }
        }
        //获取大区
        if (null != dqId) {
            activityPOS = queryByShopType(time, shopId, ShopTypeEnum.DQ.getCode(), userType);
            if (CollectionUtils.isNotEmpty(activityPOS)) {
                activityPOList.addAll(activityPOS);
            }
        }
        //获取片区
        if (null != pqId) {
            activityPOS = queryByShopType(time, shopId, ShopTypeEnum.PQ.getCode(), userType);
            if (CollectionUtils.isNotEmpty(activityPOS)) {
                activityPOList.addAll(activityPOS);
            }
        }
        return activityPOList;
    }

    @Override
    public HitTargetConditionVo execute(List<PromActivityCampOnSkuDTO> skuDTOList, PromActivityPO activityPO, String traceId) {
        //过滤品信息
        List<PromActivityCampOnSkuDTO> skuDTO = skuManager.filter(skuDTOList, activityPO, traceId);
        if(CollectionUtils.isEmpty(skuDTO)) {
            log.info("执行活动并返回奖品信息-没有命中商品信息 traceId={} skuList={} activitySkuType={}", traceId, JSON.toJSONString(skuDTOList), activityPO.getSkuType());
            return null;
        }
        //获取活动奖品
        return conditionManager.hitTarget(skuDTO, activityPO, traceId);
    }

    @Override
    public void endJob() {
        //查询进行中已过期的活动信息
        List<PromActivityPO> promActivityPOList = baseMapper.queryTimeOutActivity(new Date());
        if (CollectionUtils.isEmpty(promActivityPOList)) {
            return;
        }
        List<Long> activityIdList = promActivityPOList.stream().map(PromActivityPO::getId).collect(Collectors.toList());
        //批量更新
        baseMapper.updateStatus(activityIdList, StatusEnum.FINISHED.getCode());
        //循环清楚缓存
        promActivityPOList.forEach(activityPO -> {cleanActivityCache(activityPO);});
    }

    @Override
    public void openJob() {
        //查询
        List<PromActivityPO> promActivityPOList = baseMapper.queryNoOpenActivity(new Date());
        if (CollectionUtils.isEmpty(promActivityPOList)) {
            return;
        }
        List<Long> activityIdList = promActivityPOList.stream().map(PromActivityPO::getId).collect(Collectors.toList());
        //批量更新
        baseMapper.updateStatus(activityIdList, StatusEnum.RUNNING.getCode());
        //循环清楚缓存
        promActivityPOList.forEach(activityPO -> {cleanActivityCache(activityPO);});
    }

    public void cleanActivityCache(PromActivityPO activityPO) {
        switch (activityPO.getShopType()) {
            case 1:
                stringRedisTemplate.opsForValue().getOperations().delete(ActivityCacheKey.ACTIVITY_BY_SHOPTYPE.key(activityPO.getShopType(), null, UserTypeEnum.VIP.getCode()));
                stringRedisTemplate.opsForValue().getOperations().delete(ActivityCacheKey.ACTIVITY_BY_SHOPTYPE.key(activityPO.getShopType(), null, UserTypeEnum.NO.getCode()));
                break;
            default:
                List<Integer> shopIds = shopManager.queryShopId(activityPO.getId());
                for(Integer shopId : shopIds) {
                    stringRedisTemplate.opsForValue().getOperations().delete(ActivityCacheKey.ACTIVITY_BY_SHOPTYPE.key(activityPO.getShopType(), shopId, UserTypeEnum.VIP.getCode()));
                    stringRedisTemplate.opsForValue().getOperations().delete(ActivityCacheKey.ACTIVITY_BY_SHOPTYPE.key(activityPO.getShopType(), shopId, UserTypeEnum.NO.getCode()));
                }
                break;
        }
    }
    private List<PromActivityPO> queryByShopType(Date time, Integer shopId, Integer shopType, Integer userType) {
        //缓存获取
        List<PromActivityPO> activityPOList = new ArrayList<>();
        String key = ActivityCacheKey.ACTIVITY_BY_SHOPTYPE.key(shopType, shopId, userType);
        String activityStr = stringRedisTemplate.opsForValue().get(key);
        if (null != activityStr) {
            return JSON.parseObject(activityStr, new TypeReference<List<PromActivityPO>>() {});
        }
        //
        activityStr = "";
        List<Integer> userTypeList = Arrays.asList(UserTypeEnum.ALL.getCode(), userType);
        List<PromActivityPO> activityPOS = baseMapper.queryActivity(time, shopType, userTypeList);
        if (CollectionUtils.isNotEmpty(activityPOS)) {
            if (ShopTypeEnum.ZB.getCode().equals(shopType)){ //全部无需过滤shop信息
                activityPOList.addAll(activityPOS);
            } else {
                List<Long> activityIdList = activityPOS.stream().map(PromActivityPO::getId).collect(Collectors.toList());
                //根据目标门店信息过滤活动
                List<Long> activityExchange = shopManager.exchangeActivity(activityIdList, shopId, shopType);
                if (CollectionUtils.isNotEmpty(activityExchange)) {
                    activityPOS.forEach(activity->{
                        if (activityExchange.contains(activity.getId())) {
                            activityPOList.add(activity);
                        }
                    });
                }
            }
            activityStr = JSON.toJSONString(activityPOList);
        }
        stringRedisTemplate.opsForValue().set(key, activityStr, 60*60*1000, TimeUnit.MILLISECONDS);
        return activityPOList;
    }

}
