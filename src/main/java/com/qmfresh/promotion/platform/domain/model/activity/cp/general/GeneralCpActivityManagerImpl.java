package com.qmfresh.promotion.platform.domain.model.activity.cp.general;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qmfresh.promotion.platform.domain.model.activity.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.event.CpEvent;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.command.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPrice;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPriceCompute;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.command.CpActivitySpec;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.command.PromotionPriceCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import com.qmfresh.promotion.platform.domain.model.support.MqLog;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.ActivityMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.ChangePriceActivityMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ActivityPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ChangePriceActivityPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.support.MqLogMapper;
import com.qmfresh.promotion.platform.infrastructure.message.RocketMQSender;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class GeneralCpActivityManagerImpl implements GeneralCpActivityManager {

    @Resource
    private ShopManager shopManager;
    @Resource
    private SkuManager skuManager;
    @Resource
    private ActivityMapper activityMapper;
    @Resource
    private ChangePriceActivityMapper changePriceActivityMapper;
    @Resource
    private DefaultMQProducer defaultMQProducer;
    @Resource
    private MqLogMapper mqLogMapper;
    @Resource
    private PromotionPriceCompute promotionPriceCompute;

    /**
     * 分页查询改价详情
     *
     * @param generalCpQueryCommand 改价命令
     * @return 通用改价明细
     */
    @Override
    public Page<GeneralCpItem> pageGeneralCpDetails(GeneralCpQueryCommand generalCpQueryCommand) {
        log.info("查询参数：{}", JSON.toJSONString(generalCpQueryCommand));
        IPage<ChangePriceActivityPO> changePriceActivityPOIPage = changePriceActivityMapper.selectPage(
                new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(generalCpQueryCommand.getPageNum(), generalCpQueryCommand.getPageSize())
                , new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getActivityId, generalCpQueryCommand.getActivityId())
                        .eq(generalCpQueryCommand.getShopId() != null, ChangePriceActivityPO::getShopId, generalCpQueryCommand.getShopId())
                        .eq(generalCpQueryCommand.getSkuId() != null, ChangePriceActivityPO::getSkuId, generalCpQueryCommand.getSkuId())
                        .ge(generalCpQueryCommand.getPlanBeginTimeStart() != null, ChangePriceActivityPO::getPlanActivityStartTime, generalCpQueryCommand.getPlanBeginTimeStart())
                        .le(generalCpQueryCommand.getPlanBeginTimeEnd() != null, ChangePriceActivityPO::getPlanActivityStartTime, generalCpQueryCommand.getPlanBeginTimeEnd())
        );

        return Page.convertIPage(generalCpQueryCommand.getPageNum(), generalCpQueryCommand.getPageSize(), changePriceActivityPOIPage, ChangePriceActivityPO::generalCpItem);
    }

    @Override
    public Page<GeneralCpActivity> pageGeneralCpActivity(GeneralCpQueryCommand generalCpQueryCommand) {
        //分页查询
        IPage<ChangePriceActivityPO> changePriceActivityPOIPage = changePriceActivityMapper.selectPage(
                new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(generalCpQueryCommand.getPageNum(), generalCpQueryCommand.getPageSize()),
                new QueryWrapper<ChangePriceActivityPO>()
                        .eq("is_deleted", 0)
                        .eq("type", CpBizType.CP_PLATFORM.getCode())
                        .in(CollectionUtils.isNotEmpty(generalCpQueryCommand.getCpStatusList()), "status",
                                CollectionUtils.isNotEmpty(generalCpQueryCommand.getCpStatusList()) ?
                                        generalCpQueryCommand.getCpStatusList().stream().map(CpStatus::getCode).collect(Collectors.toList())
                                        : null)
                        .like(StringUtils.isNotBlank(generalCpQueryCommand.getActivityNameKey()), "name", generalCpQueryCommand.getActivityNameKey())
                        .eq(generalCpQueryCommand.getActivityId() != null, "activity_id", generalCpQueryCommand.getActivityId())
                        .eq(generalCpQueryCommand.getShopId() != null, "shop_id", generalCpQueryCommand.getShopId())
                        .ge(generalCpQueryCommand.getPlanBeginTimeStart() != null, "plan_activity_start_time", generalCpQueryCommand.getPlanBeginTimeStart())
                        .lt(generalCpQueryCommand.getPlanBeginTimeEnd() != null, "plan_activity_start_time", generalCpQueryCommand.getPlanBeginTimeEnd())
                        .groupBy("activity_id")
                        .orderByDesc("id")
                        .select("activity_id",
                                "name",
                                "status",
                                "type",
                                "create_person_name",
                                "plan_activity_start_time",
                                "plan_activity_end_time",
                                "cp_type",
                                "count(distinct sku_id) as sku_count",
                                "count(distinct shop_id) as shop_count",
                                "sum(plan_marketing_amount) as sum_plan_marketing_fee",
                                "gmt_create",
                                "last_operator_name"
                        )
        );
        Page<GeneralCpActivity> page = Page.convertIPage(generalCpQueryCommand.getPageNum(), generalCpQueryCommand.getPageSize(),
                changePriceActivityPOIPage, ChangePriceActivityPO::generalCpActivity);
        return page;
    }


    @Override
    public void closeGeneralCpActivity(GeneralCpCloseCommand generalCpCloseCommand) {

        //查询是否存在进行中的活动
        visit(new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getActivityId, generalCpCloseCommand.getActivityId())
                        .in(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode(), CpStatus.RUNNING.getCode())
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                ,
                generalCpCloseCommand, this::closeActivity
        );

    }

    @Override
    public void startGeneralCpActivityByJob() {
        //当前总部改价的命令
        Operator operator = Operator.createSystemOperator();
        GeneralCpStartCommand generalCpStartCommand = GeneralCpStartCommand.createForJob(operator);

        //遍历每条可以改价的信息
        visit(new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode())
                        .eq(ChangePriceActivityPO::getType, CpBizType.CP_PLATFORM.getCode())
                        .eq(ChangePriceActivityPO::getAuditStatus, AuditStatus.AUDIT_SUCCESS.getCode())
                        .le(ChangePriceActivityPO::getPlanActivityStartTime, generalCpStartCommand.getCurrentTime())
                        .gt(ChangePriceActivityPO::getPlanActivityEndTime, generalCpStartCommand.getCurrentTime())
                , generalCpStartCommand, this::startActivity);

    }

    /**
     * 关闭总部改价
     */
    @Override
    public void stopGeneralCpActivityByJob() {
        GeneralCpCloseCommand cpCloseCommand = GeneralCpCloseCommand.createForJob(Operator.createSystemOperator(), "系统关闭过期的总部改价");
        //遍历每条可以改价的信息
        visit(new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getType, CpBizType.CP_PLATFORM.getCode())
                        .in(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode(), CpStatus.RUNNING.getCode())
                        .le(ChangePriceActivityPO::getPlanActivityEndTime, new Date())
                , cpCloseCommand, this::closeActivity);

    }

    /**
     * 编辑门店 todo 1
     *
     * @param generalCpShopEditCommand 总部改价命令
     */
    @Override
    public void editShopOfGeneralCpActivity(GeneralCpShopEditCommand generalCpShopEditCommand) {

    }

    /**
     * @param generalCpEditSkuCommand
     */
    @Override
    public void editGeneralCpSku(GeneralCpEditSkuCommand generalCpEditSkuCommand) {
        Operator operator = generalCpEditSkuCommand.getOperator();
        Long activityId = generalCpEditSkuCommand.getActivityId();
        Integer skuId = generalCpEditSkuCommand.getSkuId();
        BigDecimal rulePrice = generalCpEditSkuCommand.getRulePrice();
        checkToEditGeneralCp(activityId, skuId);

        ChangePriceActivityPO update = new ChangePriceActivityPO();
        update.setRulePrice(rulePrice);
        update.setGmtModified(new Date());
        update.setLastOperatorName(operator.getOperatorName());

        //营销费用的计算

        int size = changePriceActivityMapper.update(update,
                new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getSkuId, skuId)
                        .eq(ChangePriceActivityPO::getActivityId, activityId)
        );
        log.info("有修改商品信息 activityId ={} skuId ={} size= {}", activityId, skuId, size);
    }

    /**
     * @param generalCpDelSkuCommand
     */
    @Override
    public void delGeneralCpSku(GeneralCpDelSkuCommand generalCpDelSkuCommand) {

        Operator operator = generalCpDelSkuCommand.getOperator();
        Long activityId = generalCpDelSkuCommand.getActivityId();
        Integer skuId = generalCpDelSkuCommand.getSkuId();
        checkToEditGeneralCp(activityId, skuId);
        Integer count = changePriceActivityMapper.selectCount(
                new QueryWrapper<ChangePriceActivityPO>()
                        .eq("is_deleted", 0)
                        .eq("activity_id", activityId)
                        .select("distinct sku_id")
        );
        if (count <= 1) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("商品信息只剩下一条，请删除");
        }
        ChangePriceActivityPO update = new ChangePriceActivityPO();
        update.setIsDeleted(1);
        update.setLastOperatorId(operator.getOperatorId().longValue());
        update.setLastOperatorName(operator.getOperatorName());
        int i = changePriceActivityMapper.update(update, new LambdaQueryWrapper<ChangePriceActivityPO>()
                .eq(ChangePriceActivityPO::getActivityId, activityId)
                .eq(ChangePriceActivityPO::getSkuId, skuId)
        );

        log.info("活动商品删除数量i={} activityId= {} skuId ={}", i, activityId, skuId);
    }

    private void checkToEditGeneralCp(Long activityId, Integer skuId) {

        IPage<ChangePriceActivityPO> priceActivityPOIPage = changePriceActivityMapper.selectPage(new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(1, 1),
                new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getActivityId, activityId)
                        .eq(ChangePriceActivityPO::getSkuId, skuId)
        );
        if (priceActivityPOIPage.getTotal() <= 0) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("商品不存在或者活动不存在");
        }

        if (CpStatus.CREATE.getCode() != priceActivityPOIPage.getRecords().get(0).getStatus()) {
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("非待改价活动，无法编辑商品");
        }
    }

    @Override
    public Page<SkuCpItem> pageGeneralCpSkus(GeneralCpQuerySkuCommand generalCpQuerySkuCommand) {

        IPage<ChangePriceActivityPO> changePriceActivityPOIPage = changePriceActivityMapper.selectPage(new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(generalCpQuerySkuCommand.getPageNum(), generalCpQuerySkuCommand.getPageSize())
                , new QueryWrapper<ChangePriceActivityPO>()
                        .eq("is_deleted", 0)
                        .eq("activity_id", generalCpQuerySkuCommand.getActivityId())
                        .eq(generalCpQuerySkuCommand.getSkuId() != null, "sku_id", generalCpQuerySkuCommand.getSkuId())
                        .eq(StringUtils.isNotBlank(generalCpQuerySkuCommand.getClass3Name()), "class3_name", generalCpQuerySkuCommand.getClass3Name())
                        .groupBy("sku_id")
                        .select("sku_id",
                                "rule_price",
                                "class1_name",
                                "class2_name",
                                "class3_name",
                                "sku_name",
                                "activity_id"
                        )

        );
        return Page.convertIPage(generalCpQuerySkuCommand.getPageNum(), generalCpQuerySkuCommand.getPageSize(), changePriceActivityPOIPage,
                ChangePriceActivityPO::skuCpItem);
    }

    @Override
    public CpItemResult autoCreateGeneralCp(GeneralCpAutoCreateCommand generalCpAutoCreateCommand) {
        String bizCode = generalCpAutoCreateCommand.getBizCode();
        String operatorName = generalCpAutoCreateCommand.getOperatorName();
        Integer operatorId = generalCpAutoCreateCommand.getOperatorId();
        List<CpItem> cpItems = generalCpAutoCreateCommand.getCpItemList();
        List<Integer> skuIds = cpItems.stream().map(CpItem::getSkuId).collect(Collectors.toList());
        List<Integer> shopIds = cpItems.stream().map(CpItem::getShopId).collect(Collectors.toList());
        CpTimeCycle cpTimeCycle = generalCpAutoCreateCommand.getCpTimeCycle();

        //check
        IPage<ChangePriceActivityPO> changePriceActivityPOs =
                changePriceActivityMapper.selectPage(new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(1, 2),
                        new LambdaQueryWrapper<ChangePriceActivityPO>()
                                .eq(ChangePriceActivityPO::getIsDeleted, 0)
                                .eq(ChangePriceActivityPO::getBizCode, bizCode)
                                .orderByDesc(ChangePriceActivityPO::getStatus)
                );

        //确认activityPO
        ActivityPO activityPO;
        if (CollectionUtils.isNotEmpty(changePriceActivityPOs.getRecords())) {
            Long activityId = changePriceActivityPOs.getRecords().get(0).getActivityId();
            activityPO = activityMapper.selectById(activityId);
        } else {
            //todo  think about how create beautiful
            activityPO = ActivityPO.c("总部改价策略" + generalCpAutoCreateCommand.getBizName(), CpBizType.CP_PLATFORM);
            activityMapper.insert(activityPO);
        }


        List<Shop> shops = shopManager.shops(shopIds);
        List<Shop> greyShops = Shop.grayShops(shops);
        if (CollectionUtils.isEmpty(greyShops)) {
            throw new BusinessException("所有品非灰度店");
        }
        Map<Integer, Shop> shopMap = greyShops.stream().collect(Collectors.toMap(Shop::getShopId, shop -> shop));
        List<Sku> skus = skuManager.sku(skuIds);
        Map<Integer, Sku> skuMap = skus.stream().collect(Collectors.toMap(Sku::getSkuId, sku -> sku));
        List<CpItem> faildList = new ArrayList<>();
        for (CpItem cpItem : cpItems) {
            //并发和重复问题（唯一性索引）
            try {
                List<ChangePriceActivityPO> changePriceActivityPOS = changePriceActivityMapper.selectList(
                        new LambdaQueryWrapper<ChangePriceActivityPO>()
                                .eq(ChangePriceActivityPO::getIsDeleted, 0)
                                .eq(ChangePriceActivityPO::getShopId, cpItem.getShopId())
                                .eq(ChangePriceActivityPO::getSkuId, cpItem.getSkuId())
                                .eq(ChangePriceActivityPO::getBizCode, bizCode)
                                .eq(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode())
                );
                if (CollectionUtils.isEmpty(changePriceActivityPOS)) {
                    Sku sku = skuMap.get(cpItem.getSkuId());
                    ChangePriceActivityPO changePriceActivityPO = ChangePriceActivityPO.createGeneralCp(
                            Operator.create(operatorId, operatorName, Subject.create(Subject.Platform.INTANCE)),
                            shopMap.get(cpItem.getShopId()),
                            skuMap.get(cpItem.getSkuId()),
                            null,
                            SkuCpItem.create(sku, cpItem, activityPO.getId()),
                            activityPO.getId(),
                            activityPO.getName(),
                            cpTimeCycle,
                            generalCpAutoCreateCommand.getBizCode()
                    );
                    changePriceActivityMapper.insert(changePriceActivityPO);
                } else {
                    ChangePriceActivityPO changePriceActivityPO = changePriceActivityPOS.get(0);
                    ChangePriceActivityPO update = new ChangePriceActivityPO();
                    update.setRulePrice(changePriceActivityPO.getRulePrice());
                    changePriceActivityMapper.update(update,
                            new LambdaQueryWrapper<ChangePriceActivityPO>()
                                    .eq(ChangePriceActivityPO::getId, changePriceActivityPO.getId())
                    );
                }
            } catch (Exception e) {
                log.warn("定价总部总部活动创建失败：bizCode= {} shopId= {} skuId={} rulePrice={}", bizCode, cpItem.getShopId(), cpItem.getSkuId(), cpItem.getRulePrice(), e);
                cpItem.setRemark("创建失败");
                faildList.add(cpItem);
            }
        }
        return CpItemResult.create(activityPO.getId(), faildList, activityPO.getName());
    }

    private void startActivity(ChangePriceActivityPO changePriceActivityPO, GeneralCpStartCommand generalCpStartCommand) {
        Integer shopId = changePriceActivityPO.getShopId();
        Operator operator = generalCpStartCommand.getOperator();
        Integer skuId = changePriceActivityPO.getSkuId();

        //商品价格
        SkuPrice skuPrice = null;
        try {
            skuPrice = skuManager.shopSkuPrice(shopId, skuId);
            if (skuPrice == null) {
                log.warn("启动时门店价格不存在：shopId={} skuId={} cpId={}", shopId, skuId, changePriceActivityPO.getId());
                return;
            }
        } catch (Exception e) {
            log.warn("商品价格异常：", e);
            return;
        }
        //修改改价活动状态
        ChangePriceActivityPO update = new ChangePriceActivityPO();
        update.setStatus(CpStatus.RUNNING.getCode());
        update.setRealActivityStartTime(new Date());

        int i = changePriceActivityMapper.update(update,
                new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getId, changePriceActivityPO.getId())
                        .eq(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode())
                        .eq(ChangePriceActivityPO::getAuditStatus, AuditStatus.AUDIT_SUCCESS.getCode())
        );


        if (i == 0) {
            log.warn("修改失败：cpId = {} activity = {}", changePriceActivityPO.getId(), changePriceActivityPO.getActivityId());
            return;
        }

        //营销费用计算
        CpActivitySpec spec = CpActivitySpec.spec(changePriceActivityMapper, shopId, skuId);
        spec.setGeneralCpItem(ChangePriceActivityPO.generalCpItem(changePriceActivityPO));
        //计算促销价
        PromotionPriceCommand priceCommand = PromotionPriceCommand
                .create(shopId, skuId, skuPrice.getOriginalPrice(), spec);
        PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
        promotionPriceCompute.snapshot(promotionPrice, "总部活动启动");

        //发送消息
        changePriceActivityPO.setOriginalPrice(skuPrice.getOriginalPrice());
        changePriceActivityPO.setCurrentPrice(skuPrice.getCurrentPrice());
        changePriceActivityPO.setStatus(CpStatus.RUNNING.getCode());
        changePriceActivityPO.setMarketingDiffPrice(promotionPrice.getSpec().getGeneralPromotion() != null ?
                promotionPrice.getSpec().getGeneralPromotion().getMarketingDiffFee() : BigDecimal.ZERO);
        RocketMQSender.doSend(defaultMQProducer, CpEvent.CP_EVENT_TOPIC, CpEvent.create(changePriceActivityPO), new MqLog.PersistCallback(mqLogMapper));
    }

    private void closeActivity(ChangePriceActivityPO changePriceActivityPO, GeneralCpCloseCommand generalCpCloseCommand) {
        ChangePriceActivityPO update = new ChangePriceActivityPO();
        update.setGmtModified(new Date());
        if (CpStatus.CREATE.getCode() != changePriceActivityPO.getStatus()
                && CpStatus.RUNNING.getCode() != changePriceActivityPO.getStatus()
        ) {
            log.warn("改价活动已经在终态，无法关闭：activityId = {} activityCpId={}", changePriceActivityPO.getActivityId(), changePriceActivityPO.getId());
            return;
        }

        if (generalCpCloseCommand.getActivityId() != null) {
            if (CpStatus.CREATE.getCode() == changePriceActivityPO.getStatus()) {
                update.setStatus(CpStatus.CANCELED.getCode());
            }
            if (CpStatus.RUNNING.getCode() == changePriceActivityPO.getStatus()) {
                update.setStatus(CpStatus.CLOSED.getCode());
            }
        } else {
            update.setStatus(CpStatus.FINISHED.getCode());
        }

        update.setRemark(generalCpCloseCommand.getRemark());
        update.setLastOperatorId(generalCpCloseCommand.getOperator().getOperatorId().longValue());
        update.setLastOperatorName(generalCpCloseCommand.getOperator().getOperatorName());
        int i = changePriceActivityMapper.update(update, new LambdaQueryWrapper<ChangePriceActivityPO>()
                .eq(ChangePriceActivityPO::getId, changePriceActivityPO.getId())
                .eq(ChangePriceActivityPO::getIsDeleted, 0)
        );
        changePriceActivityPO.setStatus(update.getStatus());
        changePriceActivityPO.setLastOperatorId(generalCpCloseCommand.getOperator().getOperatorId().longValue());
        changePriceActivityPO.setLastOperatorName(generalCpCloseCommand.getOperator().getOperatorName());

        SkuPrice skuPrice = skuManager.shopSkuPrice(changePriceActivityPO.getShopId(), changePriceActivityPO.getSkuId());
        if (skuPrice == null) {
            log.info("门店信息未获取,不同步改价信息：shopId={} skuId={}", changePriceActivityPO.getShopId(), changePriceActivityPO.getSkuId());
            return;
        } else {
            Integer shopId = changePriceActivityPO.getShopId();
            Integer skuId = changePriceActivityPO.getSkuId();
            CpActivitySpec spec = CpActivitySpec.spec(changePriceActivityMapper, shopId, skuId);
            if (spec.getGeneralCpItem() != null && spec.getGeneralCpItem().getActivityCpId().equals(changePriceActivityPO.getId())) {
                log.warn("大区活动关闭：获取活动是要关闭的活动：shopId={} skuId={} cpId={} activityId={}", shopId, skuId,
                        changePriceActivityPO.getId(),
                        changePriceActivityPO.getActivityId());
                spec.setGeneralCpItem(null);
            }

            //计算促销价
            PromotionPriceCommand priceCommand = PromotionPriceCommand
                    .create(shopId, skuId, skuPrice.getOriginalPrice(), spec);
            PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
            promotionPriceCompute.snapshot(promotionPrice, "总部活动关闭");
        }

        RocketMQSender.doSend(defaultMQProducer, CpEvent.CP_EVENT_TOPIC, CpEvent.create(changePriceActivityPO));
        log.info("关闭活动：activityId ={} activityCpId = {} i={}", changePriceActivityPO.getActivityId(), changePriceActivityPO.getId(), i);
    }

    /**
     * 存在同时改价的商品吗 todo 考虑门店和总部的规则
     *
     * @param shopId 门店id
     * @param skuId  商品id
     * @return boolean值
     */
    private Boolean existsCp(Long activityId, Integer shopId, Integer skuId, CpTimeCycle timeCycle) {
        List<ChangePriceActivityPO> changePriceActivityPOS = changePriceActivityMapper.selectList(new LambdaQueryWrapper<ChangePriceActivityPO>()
                .eq(ChangePriceActivityPO::getIsDeleted, 0)
                .eq(ChangePriceActivityPO::getShopId, shopId)
                .eq(ChangePriceActivityPO::getSkuId, skuId)
                .eq(ChangePriceActivityPO::getActivityId, activityId)
                .in(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode(), CpStatus.RUNNING.getCode())
                .le(ChangePriceActivityPO::getPlanActivityStartTime, timeCycle.getBeginTime())
                .ge(ChangePriceActivityPO::getPlanActivityEndTime, timeCycle.getBeginTime())
        );

        if (CollectionUtils.isNotEmpty(changePriceActivityPOS)) {
            log.warn("本活动商品已存在这个改价活动：shopId={} skuId={} activityId ={}", shopId, skuId, activityId);
            return true;

        }

        List<ChangePriceActivityPO> platformList = changePriceActivityMapper.selectList(new LambdaQueryWrapper<ChangePriceActivityPO>()
                .eq(ChangePriceActivityPO::getIsDeleted, 0)
                .eq(ChangePriceActivityPO::getType, CpBizType.CP_PLATFORM.getCode())
                .eq(ChangePriceActivityPO::getShopId, shopId)
                .eq(ChangePriceActivityPO::getSkuId, skuId)
                .in(ChangePriceActivityPO::getStatus, CpStatus.CREATE.getCode(), CpStatus.RUNNING.getCode())
                .le(ChangePriceActivityPO::getPlanActivityStartTime, timeCycle.getBeginTime())
                .ge(ChangePriceActivityPO::getPlanActivityEndTime, timeCycle.getBeginTime())
        );

        if (CollectionUtils.isNotEmpty(platformList)) {
            log.warn("本活动商品已存在平台改价活动： shopId={} skuId={} platformList={}", shopId, skuId, JSON.toJSONString(platformList));
            return true;
        }

//        List<ChangePriceActivityPO> shopList = changePriceActivityMapper.selectList(new LambdaQueryWrapper<ChangePriceActivityPO>()
//                .eq(ChangePriceActivityPO::getIsDeleted, 0)
//                .eq(ChangePriceActivityPO::getType, CpBizType.CP_SHOP.getCode())
//                .eq(ChangePriceActivityPO::getShopId, shopId)
//                .eq(ChangePriceActivityPO::getSkuId, skuId)
//                .le(ChangePriceActivityPO::getPlanActivityStartTime, timeCycle.getBeginTime())
//                .gt(ChangePriceActivityPO::getPlanActivityEndTime, timeCycle.getBeginTime())
//        );
//        if (CollectionUtils.isNotEmpty(shopList)) {
//            log.warn("本活动商品已存在门店改价活动：shopId={} skuId={} shopList={}", shopId, skuId, JSON.toJSONString(shopList));
//            return true;
//        }
        return false;
    }

    /**
     * 0. 锁定商品
     * 1. 锁定营销费用
     * 2. 保存数据
     * 3. 发送消息
     */
    private void createShopCpItem(Operator operator,
                                  Shop shop,
                                  Sku sku,
                                  SkuCpItem skuCpItems,
                                  SkuPrice skuPrice,
                                  String activityName,
                                  Long activityId,
                                  CpTimeCycle cpTimeCycle) {
        if (skuCpItems.getRulePrice() == null || skuCpItems.getRulePrice().doubleValue() <= 0) {
            log.warn("shopId={} skuId ={}必须设置营销费用", shop.getShopId(), sku.getSkuId()
            );
            return;
        }
        //商品正在被其他活动修改 todo
        //保存数据
        ChangePriceActivityPO changePriceActivityPO = ChangePriceActivityPO
                .createGeneralCp(operator, shop, sku, skuPrice, skuCpItems, activityId, activityName, cpTimeCycle);
        changePriceActivityMapper.insert(changePriceActivityPO);
        //消息发送失败
        CpEvent cpEvent = CpEvent.create(changePriceActivityPO);
        RocketMQSender.doSend(defaultMQProducer, CpEvent.CP_EVENT_TOPIC, cpEvent, new MqLog.PersistCallback(mqLogMapper));
    }


    private <S1> void visit(LambdaQueryWrapper<ChangePriceActivityPO> query, S1 s1, BiConsumer<ChangePriceActivityPO, S1> consumer) {
        IPage<ChangePriceActivityPO> changePriceActivityPOIPage =
                changePriceActivityMapper.selectPage(
                        new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(1, 100), query
                                .gt(ChangePriceActivityPO::getId, 1)
                                .orderByAsc(ChangePriceActivityPO::getId)
                );
        int count = 0;
        while (CollectionUtils.isNotEmpty(changePriceActivityPOIPage.getRecords()) && changePriceActivityPOIPage.getRecords().size() == 100) {
            try {
                for (ChangePriceActivityPO changePriceActivityPO : changePriceActivityPOIPage.getRecords()) {
                    doBiz(s1, consumer, changePriceActivityPO);
                    count = count + 1;
                }
                Thread.sleep(10);
            } catch (Exception e) {
                log.warn("每次停止10毫秒 ", e);

            }

            query.ge(ChangePriceActivityPO::getId, changePriceActivityPOIPage.getRecords().get((changePriceActivityPOIPage.getRecords().size() - 1)).getId());
            if (count > 100000) {
                throw new BusinessException("跳出这次循环，超过100000次");
            }
            changePriceActivityPOIPage = changePriceActivityMapper.selectPage(
                    new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(1, 100), query
                            .orderByAsc(ChangePriceActivityPO::getId)
            );
        }
        if (CollectionUtils.isNotEmpty(changePriceActivityPOIPage.getRecords())) {
            for (ChangePriceActivityPO changePriceActivityPO : changePriceActivityPOIPage.getRecords()) {
                doBiz(s1, consumer, changePriceActivityPO);
            }
        }
    }

    private <S1> void doBiz(S1 s1, BiConsumer<ChangePriceActivityPO, S1> consumer, ChangePriceActivityPO changePriceActivityPO) {
        try {
            consumer.accept(changePriceActivityPO, s1);
        } catch (Exception e) {
            if (e instanceof BusinessException) {
                log.warn("处理异常 {}", JSON.toJSONString(changePriceActivityPO), e);
            } else {
                log.error("处理异常 {}", JSON.toJSONString(changePriceActivityPO), e);
            }
        }
    }


    public ShopCpActivity get(Integer shopId, Integer skuId) {
        ChangePriceActivityPO changePriceActivityPO =
                changePriceActivityMapper.selectOne(new LambdaQueryWrapper<ChangePriceActivityPO>()
                        .eq(ChangePriceActivityPO::getIsDeleted, 0)
                        .eq(ChangePriceActivityPO::getShopId, shopId)
                        .eq(ChangePriceActivityPO::getSkuId, skuId)
                        .eq(ChangePriceActivityPO::getStatus, CpStatus.RUNNING.getCode())
                        .eq(ChangePriceActivityPO::getAuditStatus, AuditStatus.AUDIT_SUCCESS.getCode())
                        .eq(ChangePriceActivityPO::getType, CpBizType.CP_SHOP.getCode())
                );
        if (changePriceActivityPO == null) {
            return null;
        }
        return ChangePriceActivityPO.shopCpActivity(changePriceActivityPO);
    }


}
