package com.qmfresh.promotion.platform.domain.model.activity.cp;

import com.qmfresh.promotion.platform.domain.model.activity.Sku;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SkuCpItem implements Serializable {

    private String class1Name;
    private String class2Name;
    private String class3Name;
    private Integer skuId;
    private String skuName;
    private BigDecimal rulePrice;
    private BigDecimal planMarketingFee;
    private Long activityId;


    public static SkuCpItem create(Integer skuId, BigDecimal rulePrice, BigDecimal planMarketingFee) {
//        SkuCpItem skuCpItem = s
        SkuCpItem skuCpItem = SkuCpItem.builder()
                .skuId(skuId)
                .rulePrice(rulePrice)
                .planMarketingFee(planMarketingFee)
                .build();
        return skuCpItem;
    }

    public static SkuCpItem create(String class1Name, String class2Name, String class3Name, Integer skuId,
                                   String skuName,
                                   BigDecimal rulePrice, Long activityId) {
        return SkuCpItem.builder()
                .class1Name(class1Name)
                .class2Name(class2Name)
                .class3Name(class3Name)
                .skuId(skuId)
                .skuName(skuName)
                .rulePrice(rulePrice)
                .activityId(activityId)
                .build();
    }

    public static SkuCpItem create(Sku sku, CpItem cpItem, Long activityId) {
        return create(
                sku.getClass1Name(),
                sku.getClass2Name(),
                sku.getClass3Name(),
                sku.getSkuId(),
                sku.getSkuName(),
                cpItem.getRulePrice(),
                activityId);
    }

}
