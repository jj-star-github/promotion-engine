package com.qmfresh.promotion.platform.domain.model.activity.cp.area.command;

import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Builder;
import lombok.Getter;

import java.util.Date;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class AreaCpQueryCommand {

    /**
     * 页码
     */
    private Integer pageNum;
    /**
     * 页大小
     */
    private Integer pageSize;
    /**
     * 活动id
     */
    private Long activityId;
    /**
     * 活动名称
     */
    private String activityNameKey;
    /**
     * 门店id
     */
    private Integer shopId;
    /**
     * 商品id
     */
    private Integer skuId;
    /**
     * 状态列表
     */
    private List<Integer> statusList;
    /**
     * 起始活动开始时间
     */
    private Date fromBeginCpActivityStartTime;
    /**
     * 终止活动开始时间
     */
    private Date toBeginCpActivityStartTime;

    public static AreaCpQueryCommand create(Integer pageNum,
                                            Integer pageSize,
                                            Long activityId,
                                            String activityNameKey,
                                            Integer shopId,
                                            Integer skuId,
                                            List<Integer> statusList,
                                            Date fromBeginCpActivityStartTime,
                                            Date toBeginCpActivityStartTime) {
        if (pageNum == null) {
            throw new BusinessException("必须设置页码");
        }
        if (pageSize == null) {
            throw new BusinessException("必须设置页大小");
        }
        if ((fromBeginCpActivityStartTime == null && toBeginCpActivityStartTime != null) ||
                (fromBeginCpActivityStartTime != null && toBeginCpActivityStartTime == null)
        ) {
            throw new BusinessException("必须同时设置活动开始时间");
        }

        AreaCpQueryCommand areaCpQueryCommand = AreaCpQueryCommand.builder()
                .pageSize(pageSize)
                .pageNum(pageNum)
                .activityId(activityId)
                .activityNameKey(activityNameKey)
                .shopId(shopId)
                .skuId(skuId)
                .statusList(statusList)
                .fromBeginCpActivityStartTime(fromBeginCpActivityStartTime)
                .toBeginCpActivityStartTime(toBeginCpActivityStartTime)
                .build();

        return areaCpQueryCommand;
    }
}
