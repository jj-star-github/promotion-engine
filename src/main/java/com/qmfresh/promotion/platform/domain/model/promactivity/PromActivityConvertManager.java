package com.qmfresh.promotion.platform.domain.model.promactivity;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.bean.coupon.SendCouponReturnBean;
import com.qmfresh.promotion.bean.promotion.MzCampOn;
import com.qmfresh.promotion.dto.PromotionMzCampOnRsp;
import com.qmfresh.promotion.platform.domain.model.promactivity.vo.HitTargetConditionVo;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityConvertPO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityCampOnDTO;

import java.util.List;

/**
 * 活动奖品领取
 */
public interface PromActivityConvertManager extends IService<PromActivityConvertPO> {

    /**
     * 根据业务编码查询 未领取或领取失败的
     * @param userId
     * @param shopId
     * @param businessCode
     * @param businessType
     * @return
     */
    List<PromActivityConvertPO> queryByCanSend(Integer userId, Integer shopId, String businessCode, Integer businessType);

    /**
     * 根据业务编码查询
     * @param businessCode
     * @param businessType
     * @return
     */
    List<PromActivityConvertPO>  queryByBusiness(String businessCode, Integer businessType);

    /**
     * 批量更新状态
     * @param idList
     * @param status
     */
    void batchUpdateStatus(List<Long> idList, Integer status);

    /**
     * 奖品预占
     * @param mzCampOn
     * @param shopId
     * @param traceId
     */
    PromotionMzCampOnRsp couponCampOn(MzCampOn mzCampOn, String orderCode, Integer shopId, Integer userId, String traceId);

    /**
     * 发放奖励
     * @param convertPOList
     * @return
     */
    List<SendCouponReturnBean> sendAward(List<PromActivityConvertPO> convertPOList, String traceId);

    /**
     * 处理发送失败订单
     * @param traceId
     */
    void sendErrorConvert(String param, String traceId);

}
