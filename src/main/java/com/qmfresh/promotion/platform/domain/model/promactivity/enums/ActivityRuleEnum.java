package com.qmfresh.promotion.platform.domain.model.promactivity.enums;


import lombok.Getter;

/**
 * 活动规则类型
 */
@Getter
public enum ActivityRuleEnum {

    /**
     * 每满
     */
    EVERY(1),
    /**
     * 阶梯
     */
    LADDER(2);

    private Integer code;

    ActivityRuleEnum(Integer code) {
        this.code = code;
    }
}
