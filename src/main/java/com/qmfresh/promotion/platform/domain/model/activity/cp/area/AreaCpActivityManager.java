package com.qmfresh.promotion.platform.domain.model.activity.cp.area;

import com.qmfresh.promotion.platform.domain.model.activity.cp.SkuCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.command.*;
import com.qmfresh.promotion.platform.domain.shared.Page;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public interface AreaCpActivityManager {

    /**
     * 启动
     *
     * @param startCommand 开始命令
     */
    void start(AreaCpStartCommand startCommand);

    /**
     * 关闭
     *
     * @param cpCloseCommand 关闭命令
     */
    void close(AreaCpCloseCommand cpCloseCommand);

    /**
     * 分页大区列表查询
     *
     * @param queryCommand 查询命令
     * @return 分区活动
     */
    Page<AreaCpActivity> pageAreaCpList(AreaCpQueryCommand queryCommand);

    /**
     * 商品列表查询
     *
     * @param queryCommand 查询命令
     * @return 商品明细
     */
    Page<SkuCpItem> pageAreaSkuList(AreaCpSkuQueryCommand queryCommand);

    /**
     * 大区活动编辑商品
     *
     * @param skuEditCommand 商品编辑命令
     */
    void editSku(AreaCpSkuEditCommand skuEditCommand);

    /**
     * 大区活动删除
     *
     * @param skuDelCommand 商品删除命令
     */
    void delSku(AreaCpSkuDelCommand skuDelCommand);


    void closeAreaCpActivityByJob();

    void startAreaCpActivityByJob();

}
