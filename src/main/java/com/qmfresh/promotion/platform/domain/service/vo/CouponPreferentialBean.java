package com.qmfresh.promotion.platform.domain.service.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class CouponPreferentialBean implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    private Integer preferentialType;
    private BigDecimal couponPreferentialMoney;
    private List<Integer> useCouponSkuId;
    private Integer couponDiscount;

}

