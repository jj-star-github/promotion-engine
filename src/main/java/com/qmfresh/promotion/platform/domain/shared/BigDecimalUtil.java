package com.qmfresh.promotion.platform.domain.shared;

import java.math.BigDecimal;

/**
 * BigDecimal算法
 *
 */
public class BigDecimalUtil {

    /**
     * 提供精确的加法运算,精确小数点后两位 四舍五入
     *
     * @param v1 被加数
     * @param v2 加数
     * @return 两个参数的和
     */
    public static BigDecimal add(BigDecimal v1, BigDecimal v2) {
        v1 = null == v1 ? new BigDecimal(0) : v1;
        v2 = null == v2 ? new BigDecimal(0) : v2;
        return round(v1.add(v2), 2);
    }

    /**
     * 提供精确的减法运算,精确小数点后两位 四舍五入
     *
     * @param v1 被减数
     * @param v2 减数
     * @return 两个参数的差
     */
    public static BigDecimal sub(BigDecimal v1, BigDecimal v2) {
        v1 = null == v1 ? new BigDecimal(0) : v1;
        v2 = null == v2 ? new BigDecimal(0) : v2;
        return round(v1.subtract(v2), 2);
    }

    /**
     * 提供精确的乘法运算,精确小数点后两位 四舍五入
     *
     * @param v1 被减数
     * @param v2 减数
     * @return 两个参数的ji
     */
    public static BigDecimal mul(BigDecimal v1, BigDecimal v2) {
        v1 = null == v1 ? new BigDecimal(0) : v1;
        v2 = null == v2 ? new BigDecimal(0) : v2;
        return round(v1.multiply(v2), 2);
    }

    /**
     * 提供精确的乘法运算,精确小数点后两位 四舍五入
     *
     * @param v1 被减数
     * @param v2 减数
     * @return 两个参数的ji
     */
    public static BigDecimal mul2(BigDecimal v1, BigDecimal v2, int scale) {
        v1 = null == v1 ? new BigDecimal(0) : v1;
        v2 = null == v2 ? new BigDecimal(0) : v2;
        return round(v1.multiply(v2), scale);
    }

    /**
     * 提供精确的除法运算,精确小数点后两位 四舍五入
     *
     * @param v1 被减数
     * @param v2 减数
     * @return 两个参数的除
     */
    public static BigDecimal div(BigDecimal v1, BigDecimal v2) {
        v1 = null == v1 ? new BigDecimal(0) : v1;
        v2 = null == v2 ? new BigDecimal(0) : v2;
        return round(v1.divide(v2, 6, BigDecimal.ROUND_DOWN), 2);
    }

    /**
     * 提供精确的小数位四舍五入处理。
     *
     * @param v     需要四舍五入的数字
     * @return 四舍五入后的结果
     */
    public static BigDecimal round(BigDecimal v) {
        return round(v, 2);
    }

    /**
     * 提供精确的小数位四舍五入处理。
     *
     * @param v     需要四舍五入的数字
     * @param scale 小数点后保留几位
     * @return 四舍五入后的结果
     */
    private static BigDecimal round(BigDecimal v, int scale) {
        if (scale < 0) {
            throw new IllegalArgumentException("The scale must be a positive integer or zero");
        }
        BigDecimal one = new BigDecimal("1");
        return v.divide(one, scale, BigDecimal.ROUND_HALF_UP);
    }

}
