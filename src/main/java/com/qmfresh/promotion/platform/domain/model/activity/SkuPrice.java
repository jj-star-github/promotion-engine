package com.qmfresh.promotion.platform.domain.model.activity;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@ToString
@Slf4j
public class SkuPrice implements Serializable {
    /**
     * 门店id
     */
    private Integer shopId;
    /**
     * 商品id
     */
    private Integer skuId;
    /**
     * 原价（最终建议零售价）
     */
    private BigDecimal originalPrice;
    /**
     * 当前售价
     */
    private BigDecimal currentPrice;
    /**
     * 会员价
     */
    private BigDecimal vipPrice;

    private SkuPrice() {

    }

    public static SkuPrice create(Integer shopId, Integer skuId, BigDecimal originalPrice,
                                  BigDecimal currentPrice, BigDecimal vipPrice) {
        SkuPrice skuPrice = new SkuPrice();
        skuPrice.skuId = skuId;
        skuPrice.shopId = shopId;
        skuPrice.originalPrice = originalPrice;
        skuPrice.currentPrice = currentPrice;
        skuPrice.vipPrice = vipPrice;

        //check;skuPrice rule
        if (shopId < 0 || skuId < 0) {
            log.warn("门店id={}或者skuId={}有问题", shopId, skuId);
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("门店/商品有参数问题");
        }
        if (originalPrice.doubleValue() <= 0) {
            log.warn("原价必须大于等于零：shopId={} skuId={}", shopId, skuId);
            throw new com.qmfresh.promotion.platform.domain.shared.BusinessException("原价必须大于0");
        }

        return skuPrice;
    }
}
