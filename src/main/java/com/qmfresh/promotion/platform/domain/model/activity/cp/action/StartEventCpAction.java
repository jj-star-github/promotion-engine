package com.qmfresh.promotion.platform.domain.model.activity.cp.action;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class StartEventCpAction<T> implements CpAction {

    /**
     * 待启动的活动
     */
    private T toStartCpActivity;
    /**
     * 获取的最终售价
     */
    private BigDecimal currentPrice;
    /**
     * 差值
     */
    private BigDecimal marketingDiffPrice;


    public static <T> StartEventCpAction<T> create(T toStartCpActivity, BigDecimal currentPrice, BigDecimal marketingDiffPrice) {
        StartEventCpAction<T> startEventCpAction = new StartEventCpAction<>();
        startEventCpAction.setCurrentPrice(currentPrice);
        startEventCpAction.setMarketingDiffPrice(marketingDiffPrice);
        startEventCpAction.setToStartCpActivity(toStartCpActivity);
        return startEventCpAction;
    }
}
