package com.qmfresh.promotion.platform.domain.model.activity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class Sku implements Serializable {

    private Integer class1Id;
    private String class1Name;
    private Integer class2Id;
    private String class2Name;
    private Integer class3Id;
    private String class3Name;
    /**
     * skuId
     */
    private Integer skuId;
    private String skuName;
    private String skuFormat;
    private Integer skuType;
    private String skuImage;
    private String unitFormat;
    //1是计重，0是计件
    private Integer isWeight;
}
