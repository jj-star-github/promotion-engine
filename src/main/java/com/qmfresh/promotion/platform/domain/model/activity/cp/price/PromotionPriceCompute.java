package com.qmfresh.promotion.platform.domain.model.activity.cp.price;

import com.qmfresh.promotion.platform.domain.model.activity.cp.price.command.PromotionPriceCommand;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
public interface PromotionPriceCompute {

    /**
     * 计算促销价
     *
     * @param promotionPriceCommand
     * @return
     */
    PromotionPrice compute(PromotionPriceCommand promotionPriceCommand);

    PromotionPrice compute(BigDecimal originPrice, Integer shopId, Integer skuId, boolean snapshot, String remark);

    /**
     * 存储促销价
     *
     * @param promotionPrice
     * @param remark
     */
    void snapshot(PromotionPrice promotionPrice, String remark);
}
