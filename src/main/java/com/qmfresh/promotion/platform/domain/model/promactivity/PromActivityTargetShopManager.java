package com.qmfresh.promotion.platform.domain.model.promactivity;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityTargetShopPO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.PromActivityTargetShopDTO;

import java.util.List;

/**
 * 目标门店信息
 */
public interface PromActivityTargetShopManager extends IService<PromActivityTargetShopPO> {

    /**
     * 根据活动查询
     * @param activityId
     * @return
     */
    List<PromActivityTargetShopPO> queryByActivity(Long activityId);

    /**
     * 查询活动目标门店ID
     * @param activityId
     * @return
     */
    List<Integer> queryShopId(Long activityId);

    /**
     * 批量插入目标门店信息
     * @param activityId
     * @param shopDTOList
     */
    void insertBatch(Long activityId, List<PromActivityTargetShopDTO> shopDTOList);

    /**
     * 批量删除
     * @param activityId
     * @param shopIdList
     */
    void deletedBatch(Long activityId, List<Integer> shopIdList);

    /**
     * 根据门店过滤
     * @param activityList
     * @param shopId
     * @param shopType
     * @return
     */
    List<Long> exchangeActivity(List<Long> activityList, Integer shopId, Integer shopType);

    /**
     * 根据活动ID查询 交集活动
     * @param shopIdList
     * @param shopType
     * @param activityList
     * @return
     */
    List<Long> queryActivityId(List<Integer> shopIdList, Integer shopType, List<Long> activityList);

}
