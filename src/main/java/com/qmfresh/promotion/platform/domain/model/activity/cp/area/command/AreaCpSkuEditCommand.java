package com.qmfresh.promotion.platform.domain.model.activity.cp.area.command;

import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@Builder
public class AreaCpSkuEditCommand implements Serializable {

    /**
     * 商品id
     */
    private Operator operator;
    /**
     * 商品id
     */
    private Integer skuId;
    /**
     * 目标价格
     */
    private BigDecimal rulePrice;
    /**
     * 大区活动id
     */
    private Long activityId;


    public static AreaCpSkuEditCommand create(Operator operator,
                                              Long activityId,
                                              Integer skuId,
                                              BigDecimal rulePrice) {

        if (activityId == null) {
            throw new BusinessException("活动id不能为空");
        }
        if (skuId == null) {
            throw new BusinessException("商品id不能为空");
        }
        if (rulePrice == null || rulePrice.doubleValue() <= 0) {
            throw new BusinessException("促销价必须大于零");
        }

        AreaCpSkuEditCommand areaCpSkuEditCommand = AreaCpSkuEditCommand.builder()
                .operator(operator)
                .activityId(activityId)
                .skuId(skuId)
                .rulePrice(rulePrice)
                .build();
        return areaCpSkuEditCommand;
    }


}
