package com.qmfresh.promotion.platform.domain.model.activity;

import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 改价周期
 * <p>
 * <p>
 * 值对象
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ToString
@NoArgsConstructor
public class CpTimeCycle implements Serializable {

    private Date beginTime;
    private Date endTime;


    public CpTimeCycle(Date beginTime, Date endTime) {
        this.beginTime = beginTime;
        this.endTime = endTime;
    }

    public static CpTimeCycle create(Long beginTime, Long endTime) {
        if (beginTime == null) {
            throw new BusinessException("开始时间不能为空");
        }
        if (endTime == null) {
            throw new BusinessException("结束时间不能为空");
        }
        if (beginTime >= endTime) {
            throw new BusinessException("开始时间必须小于结束时间");
        }

        return new CpTimeCycle(new Date(beginTime), new Date(endTime));
    }


    public static CpTimeCycle create(Date beginTime, Date endTime) {
        if (beginTime == null) {
            throw new BusinessException("开始时间不能为空");
        }
        if (endTime == null) {
            throw new BusinessException("结束时间不能为空");
        }
        if (beginTime.getTime() >= endTime.getTime()) {
            throw new BusinessException("开始时间必须小于结束时间");
        }
        return new CpTimeCycle(beginTime, endTime);
    }


}
