package com.qmfresh.promotion.platform.domain.model.activity.cp.event;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.LifeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.cp.action.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.context.ShopCpContext;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ChangePriceActivityPO;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@Slf4j
public class CpEvent implements Serializable {

    public static final String CP_EVENT_TOPIC = "marketing_cp_event";


    /**
     * 事件类型（创建，启动，结束）
     *
     * @see com.qmfresh.promotion.platform.domain.model.activity.cp.LifeCycle
     */
    private Integer lifeCycle;
    /**
     * 业务类型（改价：门店改价，会员价，总部改价）
     *
     * @see com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType
     */
    private Integer bizType;
    /**
     * 事件发送时间
     */
    private Long timestamp;

    /**
     * 事件内容
     */
    private Object payload;

    /**
     * 创建门店改价
     *
     * @param shopCpContext 门店改价上下文
     * @return 改价事件
     */
    public static CpEvent createEvent(ShopCpContext shopCpContext) {
        CpAction cpAction = shopCpContext.getCpAction();
        CpEvent cpEvent = new CpEvent();
        if (cpAction instanceof StartEventCpAction) {
            StartEventCpAction<ShopCpActivity> shopCpActivityStartEventCpAction = (StartEventCpAction<ShopCpActivity>) cpAction;
            cpEvent.setLifeCycle(LifeCycle.START.getCode());
            cpEvent.setBizType(CpBizType.CP_SHOP.getCode());
            cpEvent.setTimestamp(new Date().getTime());
            cpEvent.setPayload(CpEventPayload.createStartEvent(shopCpActivityStartEventCpAction.getToStartCpActivity(),
                    shopCpActivityStartEventCpAction.getCurrentPrice(),
                    shopCpActivityStartEventCpAction.getMarketingDiffPrice()));
            log.info("创建门店改价开始事件：{}", JSON.toJSONString(cpEvent));
            return cpEvent;
        }

        if (cpAction instanceof PlanEventCpAction) {
            PlanEventCpAction<ShopCpActivity> planEventCpAction = (PlanEventCpAction<ShopCpActivity>) cpAction;
            cpEvent.setLifeCycle(LifeCycle.CREATE.getCode());
            cpEvent.setBizType(CpBizType.CP_SHOP.getCode());
            cpEvent.setTimestamp(new Date().getTime());
            cpEvent.setPayload(CpEventPayload.createPlanEvent(planEventCpAction.getActivity()));
            log.info("创建门店改价创建事件：{}", JSON.toJSONString(cpEvent));
            return cpEvent;
        }

        if (cpAction instanceof CloseEventCpAction) {
            CloseEventCpAction<ShopCpActivity> shopCpActivityCloseEventCpAction = (CloseEventCpAction<ShopCpActivity>) cpAction;
            cpEvent.setLifeCycle(LifeCycle.FINISH.getCode());
            cpEvent.setBizType(CpBizType.CP_SHOP.getCode());
            cpEvent.setTimestamp(new Date().getTime());
            cpEvent.setPayload(
                    CpEventPayload.createCloseEvent(shopCpActivityCloseEventCpAction.getActivity(), shopCpActivityCloseEventCpAction.getRemark())
            );
            log.info("创建关闭改价事件：{}", JSON.toJSONString(cpEvent));
            return cpEvent;

        }
        log.warn("创建门店事件失败：{}", JSON.toJSONString(cpAction));
        return cpEvent;
    }

    /**
     * 创建消息体
     *
     * @param changePriceActivityPO 改价活动
     * @return 改价事件
     */
    public static CpEvent create(ChangePriceActivityPO changePriceActivityPO) {
        CpEvent cpEvent = new CpEvent();
        cpEvent.setTimestamp(new Date().getTime());
        //设置业务
        cpEvent.setBizType(changePriceActivityPO.getType());

        //设置事件类型
        if (CpStatus.CREATE.getCode() == changePriceActivityPO.getStatus()) {
            cpEvent.setLifeCycle(LifeCycle.CREATE.getCode());
        }
        if (CpStatus.RUNNING.getCode() == changePriceActivityPO.getStatus()) {
            cpEvent.setLifeCycle(LifeCycle.START.getCode());
        }
        if ((CpStatus.FINISHED.getCode() == changePriceActivityPO.getStatus())
                || (CpStatus.CLOSED.getCode() == changePriceActivityPO.getStatus())
                || (CpStatus.CANCELED.getCode() == changePriceActivityPO.getStatus())
        ) {
            cpEvent.setLifeCycle(LifeCycle.FINISH.getCode());
        }

        //设置payload
        cpEvent.setPayload(CpEventPayload.create(changePriceActivityPO));
        return cpEvent;
    }


    /**
     * 关闭事件
     */
    public void close() {
        if (lifeCycle != null) {
            lifeCycle = null;
        }
        if (bizType != null) {
            bizType = null;
        }
        if (timestamp != null) {
            timestamp = null;
        }
        if (payload != null) {
            payload = null;
        }
    }
}
