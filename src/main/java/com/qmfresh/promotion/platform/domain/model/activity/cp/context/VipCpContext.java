package com.qmfresh.promotion.platform.domain.model.activity.cp.context;

import com.qmfresh.promotion.platform.domain.model.activity.cp.vip.VipCpActivity;
import com.qmfresh.promotion.platform.domain.model.activity.cp.vip.VipCpActivityManager;
import com.qmfresh.promotion.platform.domain.shared.Page;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@Slf4j
public class VipCpContext implements CpContext<VipCpActivity> {

    private VipCpActivityManager vipCpActivityManager;

    @Override
    public VipCpActivity create(CpContext<VipCpActivity> cpContext) {
        return null;
    }

    @Override
    public List<VipCpActivity> start(CpContext<VipCpActivity> cpContext) {
        return null;

    }

    @Override
    public VipCpActivity close(CpContext<VipCpActivity> cpContext) {
        return null;

    }

    @Override
    public Page<VipCpActivity> page(CpContext<VipCpActivity> cpContext) {
        return null;
    }
}
