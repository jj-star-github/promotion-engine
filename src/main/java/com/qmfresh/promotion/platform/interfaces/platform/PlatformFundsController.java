package com.qmfresh.promotion.platform.interfaces.platform;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.application.PlatformFundsService;
import com.qmfresh.promotion.platform.domain.shared.BizCode;
import com.qmfresh.promotion.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/platform/funds")
@Slf4j
@Api(tags = {"管理后台费用相关接口"})
public class PlatformFundsController {

    @Resource
    private PlatformFundsService platformFundsService;

    @ApiOperation(value = "查询账号信息", notes = "查询账号信息")
    @RequestMapping(value = "/queryAccount", method = RequestMethod.POST)
    public ServiceResultDTO<List<ShopFundsAdminAccountDTO>> queryAccount() {
        log.info("查询账号信息 请求");
        try {
            return ServiceResultDTO.success(platformFundsService.queryAccount());
        } catch (Exception e) {
            log.error("查询账号信息 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "查询账号费用信息", notes = "查询账号费用信息")
    @RequestMapping(value = "/queryAccountAmount", method = RequestMethod.POST)
    public ServiceResultDTO<ShopFundsAdminAccountDTO> queryAccount(@Valid @RequestBody ShopFundsAdminAccountAmountQueryDTO param) {
        log.info("查询账号费用信息 请求 param={}", JSON.toJSONString(param));
        try {
            return ServiceResultDTO.success(platformFundsService.queryAccount(param.getQueryShopId(), param.getShopType()));
        } catch (Exception e) {
            log.error("查询账号费用信息 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "查询总费用费用信息", notes = "查询总费用费用信息")
    @RequestMapping(value = "/queryASumAmount", method = RequestMethod.POST)
    public ServiceResultDTO<ShopFundsAdminTargetStatisDTO> querySumAccount() {
        log.info("查询总费用费用信息");
        try {
            return ServiceResultDTO.success(platformFundsService.querySumAccount());
        } catch (Exception e) {
            log.error("查询总费用费用信息 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "费用统计", notes = "费用统计")
    @RequestMapping(value = "/statis", method = RequestMethod.POST)
    public ServiceResultDTO<ShopFundsAdminTargetStatisDTO> statis() {
        log.info("费用统计 请求");
        try {
            return ServiceResultDTO.success(platformFundsService.statis());
        } catch (Exception e) {
            log.error("费用统计 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "门店费用指标统计", notes = "门店费用指标统计")
    @RequestMapping(value = "/target", method = RequestMethod.POST)
    public ServiceResultDTO<List<ShopFundsAdminTargetDTO>> target(@Valid @RequestBody ShopFundsAdminStatisQueryDTO param) {
        param.setShopId(null);
        log.info("门店费用指标统计 请求 param={}", JSON.toJSONString(param));
        try {
            return ServiceResultDTO.success(platformFundsService.target(param));
        } catch (Exception e) {
            log.error("门店费用指标统计 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "费用日统计", notes = "费用日统计")
    @RequestMapping(value = "/dayStatis", method = RequestMethod.POST)
    public ServiceResultDTO<ShopFundsAdminTargetStatisDTO> dayStatis() {
        log.info("费用日统计 请求");
        try {
            return ServiceResultDTO.success(platformFundsService.dayStatis());
        } catch (Exception e) {
            log.error("费用日统计 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "费用日统计明细", notes = "费用日统计明细")
    @RequestMapping(value = "/dayChange", method = RequestMethod.POST)
    public ServiceResultDTO<PageDTO<ShopFundsAdminDayDTO>> dayChange(@Valid @RequestBody ShopFundsAdminStatisQueryDTO param) {
        log.info("费用日统计明细 请求 param={}", JSON.toJSONString(param));
        try {
            return ServiceResultDTO.success(platformFundsService.dayChange(param));
        } catch (Exception e) {
            log.error("费用日统计明细 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "门店支付流水", notes = "门店支付流水")
    @RequestMapping(value = "/shopPayChangeNote", method = RequestMethod.POST)
    public ServiceResultDTO<PageDTO<ShopFundsPayChangeNoteDTO>> shopPayChangeNote(@Valid @RequestBody ShopFundsAdminChangeNoteQueryDTO param) {
        log.info("门店支付流水 请求 param={}", JSON.toJSONString(param));
        try {
            return ServiceResultDTO.success(platformFundsService.shopPayChangeNote(param));
        } catch (Exception e) {
            log.error("门店支付流水 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "门店收入流水", notes = "门店收入流水")
    @RequestMapping(value = "/shopNewChangeNote", method = RequestMethod.POST)
    public ServiceResultDTO<PageDTO<ShopFundsNewChangeNoteDTO>> shopNewChangeNote(@Valid @RequestBody ShopFundsAdminChangeNoteQueryDTO param) {
        log.info("门店支付流水 请求 param={}", JSON.toJSONString(param));
        try {
            return ServiceResultDTO.success(platformFundsService.shopNewChangeNote(param));
        } catch (Exception e) {
            log.error("费用日统计 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "总部支付流水", notes = "总部支付流水")
    @RequestMapping(value = "/zbPayChangeNote", method = RequestMethod.POST)
    public ServiceResultDTO<PageDTO<FundsZbPayChangeNoteDTO>> zbPayChangeNote(@Valid @RequestBody ShopFundsAdminChangeNoteQueryDTO param) {
        log.info("总部支付流水 请求 param={}", JSON.toJSONString(param));
        try {
            return ServiceResultDTO.success(platformFundsService.zbPayChangeNote(param));
        } catch (Exception e) {
            log.error("总部支付流水 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "总部收入流水", notes = "总部收入流水")
    @RequestMapping(value = "/zbNewChangeNote", method = RequestMethod.POST)
    public ServiceResultDTO<PageDTO<FundsZbNewChangeNoteDTO>> zbNewChangeNote(@Valid @RequestBody ShopFundsAdminChangeNoteQueryDTO param) {
        log.info("总部收入流水 请求 param={}", JSON.toJSONString(param));
        try {
            return ServiceResultDTO.success(platformFundsService.zbNewChangeNote(param));
        } catch (Exception e) {
            log.error("总部收入流水 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "大区收入流水", notes = "总部收入流水")
    @RequestMapping(value = "/dqNewChangeNote", method = RequestMethod.POST)
    public ServiceResultDTO<PageDTO<FundsZbNewChangeNoteDTO>> dqNewChangeNote(@Valid @RequestBody ShopFundsAdminChangeNoteQueryDTO param) {
        log.info("总部收入流水 请求 param={}", JSON.toJSONString(param));
        try {
            return ServiceResultDTO.success(platformFundsService.dqNewChangeNote(param));
        } catch (Exception e) {
            log.error("总部收入流水 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiModelProperty(value = "去化奖励流水",notes = "去化奖励流水")
    @RequestMapping(value = "/weekClearChangeNote",method = RequestMethod.POST)
    public ServiceResultDTO<PageDTO<WeekClearChangeNoteDTO>> weekClearChangeNote(@Valid @RequestBody WeekClearChangeNoteQueryDTO param){
        log.info("去化奖励流水 请求 param={}", JSON.toJSONString(param));
        try {
            return ServiceResultDTO.success(platformFundsService.weekClearChangeNote(param));
        } catch (Exception e) {
            log.error("去化奖励流水 接口异常 e={}", LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }
}
