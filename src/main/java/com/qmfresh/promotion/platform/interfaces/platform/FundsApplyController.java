package com.qmfresh.promotion.platform.interfaces.platform;


import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.activity.AdminUser;
import com.qmfresh.promotion.platform.domain.model.activity.AdminUserManager;
import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.ShopManager;
import com.qmfresh.promotion.platform.domain.model.funds.enums.FundsShopTypeEnum;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.FundsApplyDTO;
import com.qmfresh.promotion.bean.promotion.FundsApplyRecordDto;
import com.qmfresh.promotion.bean.promotion.PageQueryFundsApplyParamDTO;
import com.qmfresh.promotion.dto.FundsApplyParamDTO;
import com.qmfresh.promotion.platform.domain.model.funds.FundsAccountManager;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsAccount;
import com.qmfresh.promotion.platform.interfaces.common.BizCode;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.service.IFundsApplyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping(value = "/promotion/funds")
@Api(tags = {"APP端申请营销经费相关接口"})
@Slf4j
public class FundsApplyController {
    @Resource
    private IFundsApplyService fundsApplyService;

    @Resource
    private FundsAccountManager fundsAccountManager;

    @Resource
    private AdminUserManager adminUser;
    @Resource
    private ShopManager shopManager;
    /**
     * 申请营销经费
     *
     * @return
     */
    @PostMapping(value = "/apply")
    @ApiOperation("申请营销经费")
    public ServiceResultDTO<Boolean> createFundsApplyBill(@RequestBody FundsApplyParamDTO param, HttpServletRequest request) {
        //Assert.isNull(param,"入参不能为空！");
        String shopId = request.getHeader("shopId");
        log.info("门店编码,{}", shopId);
        param.setShopId(Long.valueOf(shopId));
        String userId = request.getHeader("userId");
        log.info("用户编码,{}", userId);
        param.setUserId(Long.valueOf(userId));
        AdminUser adminUser = this.adminUser.adminUser(Integer.valueOf(userId));
        param.setUserName(adminUser.getRealName());
        Shop shop = shopManager.shop(Integer.valueOf(shopId));
        param.setShopName(shop.getShopName());
        log.info("申请营销费用入口,{}", JSON.toJSONString(param));
        if (null == param.getShopId() || param.getShopId().toString().equals("")) {
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(), "门店编码必传！");
        }
        Assert.hasText(param.getApplyRemark(), "申请原因为空！param:" + JSON.toJSONString(param));
        FundsApplyDTO fundsApplyParam = new FundsApplyDTO();
        fundsApplyParam.setAccountId(param.getShopId());
        fundsApplyParam.setApplyAmount(param.getApplyAmount());
        fundsApplyParam.setApplyUserId(param.getUserId());
        fundsApplyParam.setApplyUserName(param.getUserName());
        fundsApplyParam.setApplyRemark(param.getApplyRemark());
        fundsApplyParam.setAccountName(param.getShopName());
        fundsApplyParam.setShopId(Integer.valueOf(shopId));
        //申请类型，1.用户即来源门店，2.系统
        fundsApplyParam.setApplyType(1);
        FundsAccount fundsAccount = fundsAccountManager.byShopIdAndType(fundsApplyParam.getAccountId().intValue(), FundsShopTypeEnum.MD.getCode());
        fundsApplyParam.setCurrentAmount(fundsAccount.getTotalAmount());
        fundsApplyParam.setShopId(param.getShopId().intValue());
        //'账户类型：1.总部，2.大区，3.片区，4.门店',
        fundsApplyParam.setShopType(4);
        log.info("创建营销申请单,{}", JSON.toJSONString(fundsApplyParam));
        try {
            Boolean fundsApplyBill = fundsApplyService.createFundsApplyBill(fundsApplyParam);
            log.info("创建营销申请单结果,{}", JSON.toJSONString(fundsApplyBill));
            if (fundsApplyBill) {
                return ServiceResultDTO.success(fundsApplyBill);
            } else {
                return ServiceResultDTO.fail(BizCode.FAILED.getCode(), "创建申请单失败，或有申请单在审批中！");
            }
        }catch (Exception e){
            log.info("创建申请单失败"+e.getMessage());
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(), "创建申请单失败，或有申请单在审批中！");
        }


    }

    /**
     * 分页查询营销费申请记录
     */
    @ApiOperation("分页查询营销费申请记录")
    @PostMapping(value = "/pageQueryFundsApplayRecord")
    public ServiceResultDTO<PageDTO<FundsApplyRecordDto>> queryFundsApplayRecord(@RequestBody PageQueryFundsApplyParamDTO param,HttpServletRequest request) {
        param.setShopId(Integer.valueOf(request.getHeader("shopId")));
        if (null == param.getShopId() || param.getShopId().toString().equals("")) {
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(), "门店编码必传！");
        }
        return ServiceResultDTO.success(fundsApplyService.pageQueryFundsApplyRecord(param));
    }

    /**
     * 查询是否有营销费申请在审批中
     */
    @PostMapping(value = "/hasFundsApplayRecord")
    @ApiOperation("查询是否有营销费申请在审批中")
    public ServiceResultDTO<Boolean> queryFundsApplayStatus(HttpServletRequest request) {
         PageQueryFundsApplyParamDTO param = new PageQueryFundsApplyParamDTO();
        param.setShopId(Integer.valueOf(request.getHeader("shopId")));
        if (null == param.getShopId() || param.getShopId().toString().equals("")) {
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(), "门店编码必传！");
        }
        return ServiceResultDTO.success(fundsApplyService.hasFundsApplayStatus(param));
    }
}
