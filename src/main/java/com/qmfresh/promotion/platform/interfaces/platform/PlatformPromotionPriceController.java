package com.qmfresh.promotion.platform.interfaces.platform;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.PromotionPriceFacadeService;
import com.qmfresh.promotion.platform.interfaces.platform.facade.price.PriceQueryDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.price.PromotionPriceDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Api(tags = "促销价计算服务")
@RestController
@RequestMapping("/platform/promotion/price")
@Slf4j
public class PlatformPromotionPriceController {


    @Resource
    private PromotionPriceFacadeService promotionPriceFacadeService;

    @PostMapping("/compute")
    @ApiOperation("计算促销价接口")
    public ServiceResultDTO<PromotionPriceDTO> compute(@RequestBody PriceQueryDTO priceQueryDTO) {
        log.info("计算促销价接口 param={}", JSON.toJSONString(priceQueryDTO));
        return ServiceResultDTO.success(promotionPriceFacadeService.compute(priceQueryDTO));
    }

    @PostMapping("/compute/try")
    @ApiOperation("测试计算促销价接口")
    public ServiceResultDTO<PromotionPriceDTO> computeTry(@RequestBody PriceQueryDTO priceQueryDTO) {
        return ServiceResultDTO.success(promotionPriceFacadeService.computeWithoutSnapshot(priceQueryDTO));
    }
}
