package com.qmfresh.promotion.platform.interfaces.platform.facade.price;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@ApiModel("改价查询模型")
@Data
public class PriceQueryDTO extends BaseDTO {

    @ApiModelProperty(value = "原价", required = true)
    private BigDecimal originalPrice;
    @ApiModelProperty("业务id")
    private Long bizId;
    @ApiModelProperty("备注")
    private String remark;
    @ApiModelProperty(value = "商品id", required = true)
    private Integer skuId;
    @ApiModelProperty(value = "当前门店id", required = true)
    private Integer currentShopId;
}
