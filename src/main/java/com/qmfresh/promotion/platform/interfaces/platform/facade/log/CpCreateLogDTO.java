package com.qmfresh.promotion.platform.interfaces.platform.facade.log;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.activity.cp.command.CpCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@ApiModel("改价创建日志")
@Data
public class CpCreateLogDTO implements Serializable {

    @ApiModelProperty("改价创建日志id")
    private Long cpCreateLogId;
    @ApiModelProperty("操作人")
    private String operatorName;
    @ApiModelProperty("门店列表")
    private String shopIds;
    @ApiModelProperty("门店数量")
    private Integer shopCount;
    @ApiModelProperty("改价商品信息")
    private String cpSku;
    @ApiModelProperty("商品数量")
    private Integer skuCount;
    @ApiModelProperty("改价状态")
    private String statusDesc;
    @ApiModelProperty("备注")
    private String remark;
    @ApiModelProperty("创建时间")
    private Long createTime;


    public static CpCreateLogDTO convert(CpCreateLog s) {
        CpCreateLogDTO cpCreateLogDTO = new CpCreateLogDTO();
        cpCreateLogDTO.setCpCreateLogId(s.getId());
        cpCreateLogDTO.setStatusDesc(s.getStatus().name());
        cpCreateLogDTO.setOperatorName(s.getOperator().getOperatorName());
        CpCreateCommand cpCreateCommand = CpCreateCommand.create(s.getRequest());
        cpCreateLogDTO.setShopIds(JSON.toJSONString(cpCreateCommand.getShopIds()));
        cpCreateLogDTO.setShopCount(cpCreateCommand.getShopIds().size());
        cpCreateLogDTO.setCpSku(JSON.toJSONString(cpCreateCommand.getSkuCpItems()));
        cpCreateLogDTO.setSkuCount(cpCreateCommand.getSkuCpItems().size());
        cpCreateLogDTO.setRemark(s.getRemark());
        cpCreateLogDTO.setCreateTime(s.getGmtCreate().getTime());
        return cpCreateLogDTO;
    }
}
