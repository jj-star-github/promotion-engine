package com.qmfresh.promotion.platform.interfaces.shop.facade.cp;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel("大区或总部改价信息")
public class ShopSkuPrice implements Serializable {
    @ApiModelProperty("改价类型-12：总部改价，14：大区改价")
    private Integer type;
    @ApiModelProperty("计划开始时间")
    private String planStartTime;
    @ApiModelProperty("计划结束时间")
    private String planEndTime;
    @ApiModelProperty("促销价")
    private BigDecimal promotionPrice;
    @ApiModelProperty("补贴或涨价-0补贴，1涨价")
    private Integer diffType;
    @ApiModelProperty("补贴或涨价金额")
    private BigDecimal diffPrice;
    @ApiModelProperty("是否改价-0否，1是")
    private Integer isDiff;

}
