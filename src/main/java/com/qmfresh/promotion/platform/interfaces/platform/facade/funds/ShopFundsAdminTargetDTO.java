package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "ShopFundsAdminTargetDTO", description = "费用统计指标数据")
public class ShopFundsAdminTargetDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("日期")
    private String createDay;
    @ApiModelProperty("销售额")
    private BigDecimal realPay;
    @ApiModelProperty("营销费用")
    private BigDecimal fundsAmount;

}
