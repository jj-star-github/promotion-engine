package com.qmfresh.promotion.platform.interfaces.platform.facade.settle.record.dto;

import lombok.Data;

import java.io.Serializable;

@Data

public class WmsReceiveDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    private Integer warehouseId;
    private Integer status;
    private Integer deliveryBeginTime;
    private Integer deliveryEndTime;
}
