package com.qmfresh.promotion.platform.interfaces.platform;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.service.DLock;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.interfaces.common.BizCode;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.CpCreateResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.CpSkuDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.GeneralCpFacadeService;
import com.qmfresh.promotion.platform.interfaces.platform.facade.ShopCpResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.general.*;
import com.qmfresh.promotion.platform.interfaces.platform.facade.shop.ShopCpQueryDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.shop.ShopListItemDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@RestController
@Api(tags = "管理后台改价相关接口")
@RequestMapping("/platform/cp")
@Slf4j
public class GeneralCpController {


    @Resource
    private DLock dLock;

    @Resource
    private GeneralCpFacadeService generalCpFacadeService;
    private static AtomicLong atomicLong = new AtomicLong(100);

    private static ExecutorService executorService = Executors
            .newFixedThreadPool(3, r -> new Thread(r, generalCpCreateKey("总部改价创建线程", atomicLong.getAndDecrement())));


    @ApiOperation("总部改价创建")
    @PostMapping("/general/create")
    public ServiceResultDTO<CpCreateResultDTO> createGeneralCpActivity(@RequestBody GeneralCpCreateDTO createDTO) {
        log.info("总部改建创建接口参数：{}", JSON.toJSONString(createDTO));
        boolean result = dLock.tryLock(DLock.generalCpCreateKey(createDTO.getOperatorId().longValue()), 15000L);
        if (!result) {
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(), "用户正在创建总部改价");
        }
        try {
            Future<CpCreateResultDTO> future = executorService.submit(() -> {
                CpCreateResultDTO generalCpActivity = generalCpFacadeService.createGeneralCpActivity(createDTO);
                return generalCpActivity;
            });
            future.get(3000, TimeUnit.SECONDS);
            if (future.isDone()) {
                return ServiceResultDTO.success(future.get());
            }
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(), "正在创建中，超过三秒等待了");
        } catch (Exception e) {
            String errMsg = "";
            if (e.getCause() instanceof BusinessException) {
                errMsg = ((BusinessException) e.getCause()).getErrorMsg();
            }
            log.warn("创建总部改价异常： {}", errMsg, e);
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(), errMsg);
        }
    }

    public static String generalCpCreateKey(String promotionGeneralCpPreKey, long operatorId) {
        return promotionGeneralCpPreKey + operatorId;
    }

    @ApiOperation("总部改价活动详情列表")
    @PostMapping("/general/info")
    public ServiceResultDTO<PageDTO<GeneralCpInfoDTO>> findGeneralCpDetails(@RequestBody GeneralCpQueryDTO generalCpQueryDTO) {
        PageDTO<GeneralCpInfoDTO> pageDTO = generalCpFacadeService.findGeneralCpDetails(generalCpQueryDTO);
        return ServiceResultDTO.success(pageDTO);
    }

    @ApiOperation("总部改价活动分页查询")
    @PostMapping("/general/page")
    public ServiceResultDTO<PageDTO<GeneralCpListItemDTO>> pageGeneralCpActivity(@RequestBody GeneralCpQueryDTO generalCpQueryDTO) {
        PageDTO<GeneralCpListItemDTO> pageGeneralCpActivity = generalCpFacadeService.pageGeneralCpActivity(generalCpQueryDTO);
        return ServiceResultDTO.success(pageGeneralCpActivity);
    }

    @ApiOperation("总部改价关闭")
    @PostMapping("/general/close")
    public ServiceResultDTO<Boolean> closeGeneralCpActivity(@RequestBody GeneralCpCloseDTO generalCpCloseDTO) {
        generalCpFacadeService.closeGeneralCpActivity(generalCpCloseDTO);
        return ServiceResultDTO.success(true);
    }

    @ApiOperation("总部活动创建：定价系统")
    @PostMapping("/general/auto/create")
    public ServiceResultDTO<CpCreateResultDTO> autoCreate(@RequestBody GeneralCpAutoCreateDTO autoCreateDTO) {
        CpCreateResultDTO cpCreateResultDTO = generalCpFacadeService.autoCreateGeneralCp(autoCreateDTO);
        return ServiceResultDTO.success(cpCreateResultDTO);
    }

    @ApiOperation("总部改价编辑门店")
    @PostMapping("/general/shop/edit")
    public ServiceResultDTO<Boolean> editShopOfGeneralCpActivity(@RequestBody GeneralCpEditShopDTO generalCpEditShopDTO) {
        generalCpFacadeService.editShopOfGeneralCpActivity(generalCpEditShopDTO);
        return ServiceResultDTO.success(true);
    }

    @ApiOperation("总部改价门店列表")
    @PostMapping("/general/shop/page")
    public ServiceResultDTO<PageDTO<ShopCpResultDTO>> pageShopOfGeneralCpActivity(@RequestBody GeneralCpQueryDTO queryDTO) {

        PageDTO<ShopCpResultDTO> pageDTO = generalCpFacadeService.pageShopOfGeneralCpActivity(queryDTO);
        return ServiceResultDTO.success(pageDTO);
    }

    @ApiOperation("总部改价编辑商品")
    @PostMapping("/general/sku/edit")
    public ServiceResultDTO<Boolean> editSkuOfGeneralCpActivity(@RequestBody GeneralCpEditSkuDTO skuDTO) {
        generalCpFacadeService.editSkuOfGeneralCpActivity(skuDTO);
        return ServiceResultDTO.success(true);
    }

    @ApiOperation("总部改价编辑商品列表")
    @PostMapping("/general/sku/page")
    public ServiceResultDTO<PageDTO<CpSkuDTO>> pageGeneralCpSkus(@RequestBody GeneralCpSkuQueryDTO generalCpSkuQueryDTO) {
        PageDTO<CpSkuDTO> pageDTO = generalCpFacadeService.pageGeneralCpSkus(generalCpSkuQueryDTO);
        return ServiceResultDTO.success(pageDTO);
    }


    @ApiOperation("门店改价列表")
    @PostMapping("/shop/page")
    public ServiceResultDTO<PageDTO<ShopListItemDTO>> pageShopCpActivity(@RequestBody ShopCpQueryDTO shopCpQueryDTO) {
        shopCpQueryDTO.setShopId(null);
        PageDTO<ShopListItemDTO> shopListItemDTOPageDTO = generalCpFacadeService.pageShopCpActivity(shopCpQueryDTO);
        return ServiceResultDTO.success(shopListItemDTOPageDTO);
    }

}
