package com.qmfresh.promotion.platform.interfaces.platform.facade.account;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "AccountCreateDTO", description = "创建账户信息")
public class AccountCreateDTO extends BaseDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("账户名称")
    private String accountName;
    @ApiModelProperty("账户类型")
    private Integer shopType;
    @ApiModelProperty("最小安全值")
    private BigDecimal minPromPrice;
    @ApiModelProperty("最大安全值")
    private BigDecimal maxPromPrice;
    @ApiModelProperty("门店id")
    private Integer queryShopId;
    @ApiModelProperty("门店名称")
    private String shopName;
    @ApiModelProperty("备注")
    private String remark;
    @ApiModelProperty("账户状态")
    private Integer status;
    @ApiModelProperty("账户id")
    private Long id;
}
