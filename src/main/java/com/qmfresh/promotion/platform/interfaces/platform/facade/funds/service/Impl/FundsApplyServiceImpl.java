package com.qmfresh.promotion.platform.interfaces.platform.facade.funds.service.Impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qmfresh.promotion.common.utils.TimeUtil;
import com.qmfresh.promotion.dto.FundsApplyCheckParamDTO;
import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.ShopManager;
import com.qmfresh.promotion.platform.domain.model.funds.enums.FundsBusinessTypeEnum;
import com.qmfresh.promotion.platform.domain.model.funds.enums.FundsShopTypeEnum;
import com.qmfresh.promotion.platform.domain.shared.BigDecimalUtil;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsAccount;
import com.qmfresh.promotion.platform.interfaces.common.BizCode;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.FundsApplyDTO;
import com.qmfresh.promotion.bean.promotion.*;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.platform.domain.model.funds.FundsAccountManager;
import com.qmfresh.promotion.platform.domain.model.funds.IFundsApplyManager;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsTransactionVO;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsApply;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.*;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.service.IFundsApplyService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class FundsApplyServiceImpl implements IFundsApplyService {
    private static final String preFix = "FUNDSAPPLY";
    private static final int AGREE = 4;
    private static final int REFUSE = 5;
    @Resource
    private IFundsApplyManager fundsApplyManager;
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    @Resource
    private FundsAccountManager fundsAccountManager;

    @Resource
    private ShopManager shopManager;
    @Override
    public Boolean createFundsApplyBill(FundsApplyDTO fundsApplyParam) {
        Boolean result;
        //redis 锁 控制用户10秒内不能重复点击
        if (redisTemplate.hasKey(preFix+fundsApplyParam.getAccountId().toString())) {
            throw new BusinessException("系统处理中，请勿重复提交申请");
        }else {
            redisTemplate.expire(preFix+fundsApplyParam.getAccountId().toString(), 10, TimeUnit.SECONDS);
        }
        //只能有一个审批中的流程校验
        PageQueryFundsApplyParamDTO hasfundsApply = new PageQueryFundsApplyParamDTO();
        hasfundsApply.setShopId(fundsApplyParam.getAccountId().intValue());
        hasfundsApply.setShopType(fundsApplyParam.getShopType());
        if(hasFundsApplayStatus(hasfundsApply)){
           return result = false;
        }
        //审核记录
        List<CheckRecordDto> recordList = new ArrayList<>();
        CheckRecordDto checkRecordDto = new CheckRecordDto();
        checkRecordDto.setOpTime(DateUtil.getCurrentTimeIntValue());
        checkRecordDto.setCheckStatus(0);
        recordList.add(checkRecordDto);
        fundsApplyParam.setApplyProgress(JSON.toJSONString(recordList));
        //审核类型，1.系统自动审核 2.独裁审批 3.流程审批
        fundsApplyParam.setExamineType(2);
        if(fundsApplyParam.getApplyType().equals(2) && fundsApplyParam.getShopType().equals(1)){
            fundsApplyParam.setAccountName("总部");
        }
         result = fundsApplyManager.createFundsApplyBill(fundsApplyParam);
        return result;
    }

    @Override
    public Boolean createApply(FundsApplyCheckParamDTO param) {
        FundsApplyDTO fundsApplyParam = new FundsApplyDTO();
        fundsApplyParam.setApplyAmount(param.getApplyAmount());
        fundsApplyParam.setApplyUserId(param.getUserId());
        fundsApplyParam.setApplyUserName(param.getUserName());
        fundsApplyParam.setApplyRemark(param.getApplyRemark());
        fundsApplyParam.setApplyType(2);
        if(param.getApplyType().equals(1)){
            param.setShopId(1);
            fundsApplyParam.setAccountId(param.getShopId().longValue());
            fundsApplyParam.setAccountName("总部");
            fundsApplyParam.setShopId(1);
            fundsApplyParam.setShopType(1);
            FundsAccount fundsAccount = fundsAccountManager.byShopIdAndType(1, FundsShopTypeEnum.ZB.getCode());
            fundsApplyParam.setCurrentAmount(fundsAccount.getTotalAmount());
        } else if(param.getApplyType().equals(2)){
            Integer shopId = param.getRealShopId();
            Shop shop = shopManager.shop(shopId.intValue());
            param.setShopName(shop.getShopName());
            fundsApplyParam.setAccountId(shopId.longValue());
            fundsApplyParam.setAccountName(param.getShopName());
            fundsApplyParam.setShopId(shopId);
            fundsApplyParam.setShopType(4);
            FundsAccount fundsAccount = fundsAccountManager.byShopIdAndType(shopId, FundsShopTypeEnum.MD.getCode());
            fundsApplyParam.setCurrentAmount(fundsAccount.getTotalAmount());
        } else if(param.getApplyType().equals(3)){
            fundsApplyParam.setAccountId(param.getAreaId().longValue());
            fundsApplyParam.setAccountName(param.getAreaName());
            fundsApplyParam.setShopId(param.getAreaId());
            fundsApplyParam.setShopType(2);
            FundsAccount fundsAccount = fundsAccountManager.byShopIdAndType(param.getAreaId(), FundsShopTypeEnum.DQ.getCode());
            fundsApplyParam.setCurrentAmount(fundsAccount.getTotalAmount());
        }
        log.info("创建营销申请单,{}", JSON.toJSONString(fundsApplyParam));
        Boolean fundsApplyBill = createFundsApplyBill(fundsApplyParam);
        if(fundsApplyBill){
            return fundsApplyBill;
        }else {
            return false;
        }
    }

    @Override
    public PageDTO<FundsApplyRecordDto> pageQueryFundsApplyRecord(PageQueryFundsApplyParamDTO param) {
        log.info("APP查询申请记录分页入参："+JSON.toJSONString(param));
        PageDTO<FundsApplyRecordDto> fundsApplyResult = new PageDTO<FundsApplyRecordDto>();
        PageDTO<FundsApply> activityPage = fundsApplyManager.pageQueryFundsApply(param);
        if (activityPage != null && !ListUtil.isNullOrEmpty(activityPage.getRecords())) {
            List<FundsApply> fundsApplys = activityPage.getRecords();
            fundsApplyResult.setTotalCount(activityPage.getTotalCount());
            List<FundsApplyRecordDto> fundsApplyRecordDtos = new ArrayList<>();
            for (FundsApply fundsApply : fundsApplys) {
                FundsApplyRecordDto fundsApplyRecordDto = new FundsApplyRecordDto();
                fundsApplyRecordDto.setTitle("营销费用申请");
                fundsApplyRecordDto.setApplyTime(fundsApply.getGmtCreate());
                fundsApplyRecordDto.setStatus((int)fundsApply.getStatus());
                fundsApplyRecordDto.setAmount(BigDecimalUtil.round(fundsApply.getApplyAmount()));
                String applyProgress = fundsApply.getApplyProgress();
                List<CheckRecordDto>  ckList = new ArrayList<>();
                ckList = JSONObject.parseArray(applyProgress,CheckRecordDto.class);
                fundsApplyRecordDto.setCheckRecords(ckList);
                FundsApplyBillDto fundsApplyBillDto = new FundsApplyBillDto();
                fundsApplyBillDto.setApplyTime(fundsApply.getGmtCreate());
                fundsApplyBillDto.setMoney(BigDecimalUtil.round(fundsApply.getApplyAmount()));
                fundsApplyBillDto.setReason(fundsApply.getApplyRemark());
                fundsApplyRecordDto.setFundsApplyBill(fundsApplyBillDto);
                fundsApplyRecordDtos.add(fundsApplyRecordDto);
            }
            fundsApplyResult.setRecords(fundsApplyRecordDtos);
        }
        log.info("APP查询申请记录分页出参："+JSON.toJSONString(fundsApplyResult));
        return fundsApplyResult;
    }

    @Override
    public PageDTO<FundsApplyCheckRecordDto> pageQueryFundsApplyCheckRecord(PageQueryFundsApplyCheckParamDTO param) {
        PageDTO<FundsApplyCheckRecordDto> fundsApplyResult = new PageDTO<>();
        param.setGmtCreateStart(TimeUtil.getSecsOfMorning(param.getGmtCreateStart()));
        param.setGmtCreateEnd(TimeUtil.getSecsOfNight(param.getGmtCreateStart()));
        log.info("查询申请流水："+ JSON.toJSONString(param));
        PageDTO<FundsApply> activityPage = fundsApplyManager.pageQueryFundsApplyCheckList(param);
        if ( !ListUtil.isNullOrEmpty(activityPage.getRecords())) {
            fundsApplyResult.setTotalCount(activityPage.getTotalCount());
            List<FundsApplyCheckRecordDto>  FundsApplyCheckRecordDtos =new ArrayList<>();
            for (FundsApply fundsApply: activityPage.getRecords()) {
                FundsApplyCheckRecordDto fundsApplyCheckRecordDto = new FundsApplyCheckRecordDto();
                fundsApplyCheckRecordDto.setApplyAmount(BigDecimalUtil.round(fundsApply.getApplyAmount()));
                // 门店名称或者总部区分,当前门店营销池金额
                fundsApplyCheckRecordDto.setApplyName(fundsApply.getAccountName());
                fundsApplyCheckRecordDto.setCurrentAmount(BigDecimalUtil.round(fundsApply.getCurrentAmount()));
                fundsApplyCheckRecordDto.setApplyType(fundsApply.getShopType());
                fundsApplyCheckRecordDto.setApplyUserName(fundsApply.getApplyUserName());
                fundsApplyCheckRecordDto.setApplyRemark(fundsApply.getApplyRemark());
                fundsApplyCheckRecordDto.setStatus(fundsApply.getStatus());
                fundsApplyCheckRecordDto.setId(fundsApply.getId());
                fundsApplyCheckRecordDto.setGmtCreate(fundsApply.getGmtCreate());
                fundsApplyCheckRecordDto.setExamineUserName(fundsApply.getExamineUserName());
                fundsApplyCheckRecordDto.setExamineTime(fundsApply.getExamineTime());
                fundsApplyCheckRecordDto.setExamineRemark(fundsApply.getExamineRemark());
                fundsApplyCheckRecordDto.setOriginatorName(fundsApply.getAccountName());
                FundsApplyCheckRecordDtos.add(fundsApplyCheckRecordDto);
            }
            fundsApplyResult.setRecords(FundsApplyCheckRecordDtos);
        }
        return fundsApplyResult;
    }

    @Override
    public PageDTO<FundsApplyCheckRecordDto> fundsAccountList(PageQueryFundsApplyCheckParamDTO param) {
        PageDTO<FundsApplyCheckRecordDto> fundsApplyResult = new PageDTO<>();
        log.info("查询申请流水："+ JSON.toJSONString(param));
        PageDTO<FundsApply> activityPage = fundsApplyManager.fundsAccountList(param);
        if ( !ListUtil.isNullOrEmpty(activityPage.getRecords())) {
            fundsApplyResult.setTotalCount(activityPage.getTotalCount());
            List<FundsApplyCheckRecordDto>  FundsApplyCheckRecordDtos =new ArrayList<>();
            for (FundsApply fundsApply: activityPage.getRecords()) {
                FundsApplyCheckRecordDto fundsApplyCheckRecordDto = new FundsApplyCheckRecordDto();
                fundsApplyCheckRecordDto.setApplyAmount(BigDecimalUtil.round(fundsApply.getApplyAmount()));
                // 门店名称或者总部区分,当前门店营销池金额
                fundsApplyCheckRecordDto.setApplyName(fundsApply.getAccountName());
                fundsApplyCheckRecordDto.setCurrentAmount(BigDecimalUtil.round(fundsApply.getCurrentAmount()));
                fundsApplyCheckRecordDto.setApplyType(fundsApply.getShopType());
                fundsApplyCheckRecordDto.setApplyUserName(fundsApply.getApplyUserName());
                fundsApplyCheckRecordDto.setApplyRemark(fundsApply.getApplyRemark());
                fundsApplyCheckRecordDto.setStatus(fundsApply.getStatus());
                fundsApplyCheckRecordDto.setId(fundsApply.getId());
                fundsApplyCheckRecordDto.setGmtCreate(fundsApply.getGmtCreate());
                fundsApplyCheckRecordDto.setExamineUserName(fundsApply.getExamineUserName());
                fundsApplyCheckRecordDto.setExamineTime(fundsApply.getExamineTime());
                fundsApplyCheckRecordDto.setExamineRemark(fundsApply.getExamineRemark());
                fundsApplyCheckRecordDto.setOriginatorName(fundsApply.getAccountName());
                FundsApplyCheckRecordDtos.add(fundsApplyCheckRecordDto);
            }
            fundsApplyResult.setRecords(FundsApplyCheckRecordDtos);
        }
        return fundsApplyResult;
    }

    @Override
    public Boolean checkFundsApplyBillStatus(CheckFundsApplyBillParamDTO param) {
        //查询审核单信息
        FundsApply fundsApply = fundsApplyManager.queryFundsApplyBill(param);
        if(null == fundsApply){
           throw  new BusinessException("申请单不存在，或者已经审核，请勿重复操作！");
        }else {
       //判断是同意还是拒绝
            if(param.getStatus() == AGREE){

                //调用放款接口，给门店分配营销额度
                FundsTransactionVO fundsAccount = new FundsTransactionVO();
                fundsAccount.setShopId(fundsApply.getAccountId().intValue());
                fundsAccount.setShopType(fundsApply.getShopType());
                fundsAccount.setShopName(fundsApply.getAccountName());
                fundsAccount.setBusinessId(fundsApply.getId()+"");
                fundsAccount.setBusinessCode(fundsApply.getId()+"");
                fundsAccount.setBusinessType(FundsBusinessTypeEnum.FUNDS_APPLY.getCode());
                fundsAccount.setRemark("费用申请");
                fundsAccount.setChangeAmount(fundsApply.getApplyAmount());
                log.info("分配营销额度入参："+JSON.toJSONString(fundsAccount));
                Boolean fundsResult = fundsAccountManager.updateAccount(fundsAccount);
                log.info("分配营销额度结果："+fundsResult);
                //更新审核状态
                if(fundsResult){
                    Boolean result = changeCheckStatus(param, fundsApply, AGREE);
                    return result;
                }else {
                    return false;
                }
            }else if(param.getStatus() == REFUSE){
                //更新审核状态
                Boolean resultRefuse = changeCheckStatus(param, fundsApply, REFUSE);
                return resultRefuse;
            }else {
                throw new BusinessException("审核状态异常！");
            }
        }

    }

    @Override
    public FundsApplyCountDto queryCheckApplyCount(FundsApplyCountParamDTO param) {
        FundsApplyCountDto fundsApplyCountDto = new FundsApplyCountDto();
        FundsApplyCountQueryDTO waitfundsApplyCountQueryDTO = new FundsApplyCountQueryDTO();
        waitfundsApplyCountQueryDTO.setStatus(0);
        FundsApplyCountDTO wfundsApplyCountDTO = fundsApplyManager.queryCheckApplyCount(waitfundsApplyCountQueryDTO);
        fundsApplyCountDto.setWaitCheckNum(wfundsApplyCountDTO.getNum());
        fundsApplyCountDto.setWaitCheckMoney(wfundsApplyCountDTO.getMoney());
        FundsApplyCountQueryDTO pwaitfundsApplyCountQueryDTO = new FundsApplyCountQueryDTO();
        pwaitfundsApplyCountQueryDTO.setStatus(4);
        FundsApplyCountDTO pfundsApplyCountDTO = fundsApplyManager.queryCheckApplyCount(pwaitfundsApplyCountQueryDTO);
        fundsApplyCountDto.setPassCheckMoney(pfundsApplyCountDTO.getMoney());
        fundsApplyCountDto.setPassCheckNum(pfundsApplyCountDTO.getNum());
        return fundsApplyCountDto;
    }

    @Override
    public Boolean hasFundsApplayStatus(PageQueryFundsApplyParamDTO param) {
        Boolean isHas = fundsApplyManager.hasFundsApplayStatus(param);
        return isHas;
    }

    /*
    更新审核状态
     */
    private Boolean changeCheckStatus(CheckFundsApplyBillParamDTO param, FundsApply fundsApply, int agree) {
        List<CheckRecordDto> ckList = new ArrayList<>();
        ckList = JSONObject.parseArray(fundsApply.getApplyProgress(), CheckRecordDto.class);
        CheckRecordDto checkRecordDto = new CheckRecordDto();
        checkRecordDto.setCheckStatus(agree);
        checkRecordDto.setOpTime(DateUtil.getCurrentTimeIntValue());
        ckList.add(checkRecordDto);
        FundsApply upFundsApply = new FundsApply();
        BeanUtils.copyProperties(fundsApply, upFundsApply);
        upFundsApply.setApplyProgress(JSON.toJSONString(ckList));
        upFundsApply.setStatus(agree);
        upFundsApply.setExamineUserId(param.getExamineUserId());
        upFundsApply.setExamineUserName(param.getExamineUserName());
        upFundsApply.setExamineTime(DateUtil.getCurrentTimeIntValue());
        if(!StringUtils.isEmpty(param.getExamineRemark())){
            upFundsApply.setExamineRemark(param.getExamineRemark());
        }
        Boolean result = fundsApplyManager.upFundsApplyStatus(upFundsApply);
        return  result;
    }

}
