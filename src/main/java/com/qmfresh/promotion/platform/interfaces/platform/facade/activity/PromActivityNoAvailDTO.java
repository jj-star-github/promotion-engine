package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "PromActivityBaseDTO", description = "查询活动信息")
public class PromActivityNoAvailDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("活动ID")
    private Long id;
    @ApiModelProperty("操作人id")
    private Integer operatorId;
    @ApiModelProperty("操作人名称")
    private String operatorName;

    public PromActivityNoAvailDTO() {}

}

