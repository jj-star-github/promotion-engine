package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 优惠券查询检查
 **/
@Data
@ApiModel(value = "CouponCheckResultDTO", description = "优惠券新消")
public class CouponCheckResultDTO {

    /**
     * 券id
     */
    private Long couponId;

    /**
     * 成功标记 0：成功，1：失败
     */
    private Boolean successFlag;

    /**
     * 失败原因
     */
    private String reason;

    /**
     * 优惠券展示的中间名称
     */
    private String couponName;

    /**
     * 使用说明
     */
    private String useInstruction;

    /**
     * 有效期
     */
    private String validityPeriod;

    /**
     * 优惠类型 1：固定金额 2：折扣
     */
    private Integer preferentialType;

    /**
     * 折扣
     */
    private Integer discount;

    /**
     * 具体优惠金额，如果preferentialType是2，代表最多优惠的金额;
     */
    private BigDecimal preferentialMoney;

    /**
     * 优惠券展示的最大名称
     */
    private String couponActivityName;

    /**
     * 今日、明日是否到期,0:没有过期,1:今日即将过期，2:明日即将过期
     */
    private Integer expiredFlag;

    /**
     * 优惠券绑定的订单号
     */
    private String sourceOrderNo;

    /**
     * 可用优惠券的商品列表
     */
    private List<CouponGoodsInfo> usefulGoodsList;

}
