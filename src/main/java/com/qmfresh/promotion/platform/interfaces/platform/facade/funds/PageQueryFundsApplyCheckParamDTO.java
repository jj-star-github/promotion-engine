package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel("分页查询营销费申请记录入参")
@Data
@ToString
public class PageQueryFundsApplyCheckParamDTO {
    /**
     * 申请人用户名
     */
    @ApiModelProperty("申请人用户名")
    private String applyUserName;
    /**
     * 申请类型，1.用户，2.系统
     */
    @ApiModelProperty("申请类型，1.用户，2.系统")
    private Integer applyType;
    /**
     * 审核状态:0.审核中，4.通过，5.驳回
     */
    @ApiModelProperty("审核状态:0.审核中，4.通过，5.驳回")
    private Integer status;
    /**
     * 创建时间开始
     */
    @ApiModelProperty("创建时间开始")
    private Integer gmtCreateStart;
    /**
     * 创建时间结束
     */
    @ApiModelProperty("创建时间结束")
    private Integer gmtCreateEnd;

    private  int start;

    private  int totalCount;

    private int end;
    @ApiModelProperty("分页大小")
    private  int pageSize;
    @ApiModelProperty("页码")
    private  int pageIndex;

    private Integer queryShopId;
    private Integer shopType;



    public int getTotalCount() {
        return totalCount;
    }

    public int getEnd() {
        return pageSize;
    }


    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getStart() {

        return (pageIndex-1)*pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public void setStart(int start) {
        this.start = start;
    }
    public PageQueryFundsApplyCheckParamDTO(){
        this.start  = 0;
        this.end = 20;
        this.pageIndex = 1;
        this.pageSize = 20;
    }

}
