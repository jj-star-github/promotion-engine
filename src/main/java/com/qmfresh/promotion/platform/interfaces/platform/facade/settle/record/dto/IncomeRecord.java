package com.qmfresh.promotion.platform.interfaces.platform.facade.settle.record.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "IncomeRecord", description = "收入记录")
public class IncomeRecord implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("订单编号")
    private String orderCode;
    @ApiModelProperty("订单金额")
    private BigDecimal realPay;
    @ApiModelProperty("保底毛利")
    private BigDecimal gross;
    @ApiModelProperty("总部营销费")
    private BigDecimal baseMarketing;
    @ApiModelProperty("门店营销费")
    private BigDecimal shopMarketing;
    @ApiModelProperty("创建时间")
    private Integer gmtCreate;
}
