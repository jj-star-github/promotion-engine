package com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "ShopRuleReqDTO", description = "创建门店营销费用规则")
public class ShopRuleReqDTO extends BaseDTO {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("shop_grade")
    private List<ShopRuleDTO> shopRuleDTO;
}
