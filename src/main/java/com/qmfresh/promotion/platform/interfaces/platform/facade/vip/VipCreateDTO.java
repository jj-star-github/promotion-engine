package com.qmfresh.promotion.platform.interfaces.platform.facade.vip;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import lombok.Data;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class VipCreateDTO extends BaseDTO {
}
