package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "PromActivitySendDTO", description = "领取奖励")
public class PromActivitySendDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("门店ID")
    private Integer sourceShopId;
    @ApiModelProperty("用户ID")
    private Integer userId;
    @ApiModelProperty("订单Code补单时会使用")
    private String sourceOrderNo;
}

