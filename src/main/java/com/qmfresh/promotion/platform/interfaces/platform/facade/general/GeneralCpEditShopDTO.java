package com.qmfresh.promotion.platform.interfaces.platform.facade.general;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@ApiModel("总部改价门店列表")
@Data
@ToString
public class GeneralCpEditShopDTO extends BaseDTO {
    @ApiModelProperty("总部改价活动id")
    private Long activityId;
    @ApiModelProperty("门店列表")
    private List<Integer> shopIds;
}
