package com.qmfresh.promotion.platform.interfaces.shop.facade.funds;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ApiModel(value = "ShopFundsChangeQueryDTO", description = "门店费用流水明细")
public class ShopFundsChangeQueryDTO extends BaseDTO {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("结算日期:yyyy-mm-dd")
    @NotNull(message = "查询时间不可为空")
    private String timeDay;
    @ApiModelProperty("业务类型 2:营销费收入，3.营销使用")
    @NotNull(message = "业务类型不可为空")
    private Integer businessType;
    @ApiModelProperty("营销使用关联 空：全部 -1: 其他")
    private List<String> useType;
    @ApiModelProperty("分页页码 默认第一页")
    private Integer pageIndex=1;
    @ApiModelProperty("分页页面大小 默认20条")
    private Integer pageSize=20;

    public Integer getStart() {
        return (this.pageIndex-1)*this.pageSize;
    }
}
