package com.qmfresh.promotion.platform.interfaces.platform.facade;

import com.qmfresh.promotion.platform.domain.model.activity.cp.ShopCpResult;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class ShopCpResultDTO implements Serializable {
    @ApiModelProperty("活动id")
    private Long activityId;
    @ApiModelProperty("门店id")
    private Integer shopId;

    public static ShopCpResultDTO convert(ShopCpResult s) {
        ShopCpResultDTO shopCpResultDTO = new ShopCpResultDTO();
        shopCpResultDTO.setActivityId(s.getActivityId());
        shopCpResultDTO.setShopId(s.getShopId());
        return shopCpResultDTO;
    }
}
