package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityTargetShopPO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "PromActivityTargetShopDTO", description = "目标门店信息")
public class PromActivityTargetShopDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("门店ID")
    private Integer shopId;
    @ApiModelProperty("门店名")
    private String shopName;
    @ApiModelProperty("门店标识,2.大区,3.片区,4.门店")
    private Integer shopType;

    public PromActivityTargetShopDTO() {}

}

