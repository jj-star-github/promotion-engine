package com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel(value = "QueryRuleInfoReqDTO", description = "查询门店营销费用规则")
public class QueryRuleInfoReqDTO implements Serializable{
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("shop_grade")
    private Integer shopGrade;
    @ApiModelProperty("shop_type")
    private Integer shopType;
    @ApiModelProperty("开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtModifiedStart;
    @ApiModelProperty("结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtModifiedEnd;
}
