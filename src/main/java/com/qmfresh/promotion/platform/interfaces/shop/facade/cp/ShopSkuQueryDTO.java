package com.qmfresh.promotion.platform.interfaces.shop.facade.cp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("门店sku")
public class ShopSkuQueryDTO implements Serializable {

    @ApiModelProperty("门店id")
    private Integer shopId;
    @ApiModelProperty("商品id")
    private Integer skuId;
}
