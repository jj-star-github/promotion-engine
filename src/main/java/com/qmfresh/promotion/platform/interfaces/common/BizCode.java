package com.qmfresh.promotion.platform.interfaces.common;

import lombok.Getter;

/**
 * 2000 ： http（2000）
 * 3002 ： http（302）
 * 4011 :  token失效，用户未登录
 * 4012 ： 用户无权限
 * 4004 : 页面不存在（404）
 * 4005 ： http方法异常（405）
 * 4000 ： http（400）
 * 5000 ： 服务端错误（500）
 * 5002 ： 服务端错误（502）
 * 9998 ： 远程接口错误
 * 9999 ： 系统接口错误
 * <p>
 * 5000后面定义业务代码
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
public enum BizCode {

    SUCCESS(0),


    FAILED(9999);

    private Integer code;


    BizCode(Integer code) {
        this.code = code;
    }

}
