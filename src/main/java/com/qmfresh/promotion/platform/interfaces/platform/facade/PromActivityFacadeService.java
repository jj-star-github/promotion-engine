package com.qmfresh.promotion.platform.interfaces.platform.facade;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.ShopManager;
import com.qmfresh.promotion.platform.domain.model.activity.Subject;
import com.qmfresh.promotion.platform.domain.model.activity.cp.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.command.ShopQueryCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpActivity;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpActivityManager;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.command.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivityManager;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.command.ShopCpQueryCommand;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.CpCreateResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.CpSkuDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.ShopCpResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.SkuCpDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.general.*;
import com.qmfresh.promotion.platform.interfaces.platform.facade.shop.ShopCpQueryDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.shop.ShopListItemDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 */
@Component
@Slf4j
public class PromActivityFacadeService {


}
