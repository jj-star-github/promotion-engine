package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
@ApiModel(value = "PromActivityConditionCouponDTO", description = "优惠券奖品信息")
public class PromActivityConditionCouponDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("优惠券使用规则")
    private String useInstruction;
    @ApiModelProperty("优惠券时间规则")
    private String useTimeRule;
    @ApiModelProperty("优惠券类型：1:满减,2:折扣,3:随机金额")
    private Integer couponType;
    @ApiModelProperty("优惠券名")
    private String couponName;
}

