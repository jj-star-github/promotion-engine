package com.qmfresh.promotion.platform.interfaces.platform.facade.general;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.CpItemDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@ApiModel("总部改价编辑商品")
@Data
public class GeneralCpEditSkuDTO extends BaseDTO {


    @ApiModelProperty("操作类型：1=编辑 2=删除")
    private Integer operatorType;

    @ApiModelProperty("总部改价id")
    private Long activityId;

    @ApiModelProperty("商品改价")
    private CpItemDTO skuCp;
}
