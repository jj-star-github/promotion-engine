package com.qmfresh.promotion.platform.interfaces.platform.facade;

import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLog;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLogManager;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.command.CpCreateLogQueryCommand;
import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.log.CpCreateLogDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.log.CpCreateLogQueryDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class CpCreateLogFacadeService {

    @Resource
    private CpCreateLogManager cpCreateLogManager;

    /**
     * 分页查询
     *
     * @param queryDTO
     * @return
     */
    public PageDTO<CpCreateLogDTO> page(CpCreateLogQueryDTO queryDTO) {
        CpCreateLogQueryCommand command = CpCreateLogQueryCommand
                .create(queryDTO.getPageNum(), queryDTO.getPageSize(), null);
        Page<CpCreateLog> cpCreateLogPage = cpCreateLogManager.pageCpCreateLog(command);
        return PageDTO.convert(queryDTO.getPageNum(), queryDTO.getPageSize(), cpCreateLogPage, CpCreateLogDTO::convert);
    }
}
