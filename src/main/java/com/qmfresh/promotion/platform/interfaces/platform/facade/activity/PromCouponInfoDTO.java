package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "PromCouponInfoDTO", description = "优惠券优惠信息")
public class PromCouponInfoDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("优惠券码")
    private String couponCode;
    @ApiModelProperty("渠道 1：收银 5：线上商城  10：外卖 15：门店订货")
    private Integer bizChannel;
}

