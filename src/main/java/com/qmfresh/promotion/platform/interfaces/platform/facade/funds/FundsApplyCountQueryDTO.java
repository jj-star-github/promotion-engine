package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

/**
 * 审核营销经费统计参数
 */
public class FundsApplyCountQueryDTO {
    //状态
    private  Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
