package com.qmfresh.promotion.platform.interfaces.platform;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.promotion.FundsApplyCheckRecordDto;
import com.qmfresh.promotion.bean.promotion.FundsApplyCountDto;
import com.qmfresh.promotion.dto.FundsApplyCheckParamDTO;
import com.qmfresh.promotion.platform.domain.model.activity.AdminUser;
import com.qmfresh.promotion.platform.domain.model.activity.AdminUserManager;
import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.ShopManager;
import com.qmfresh.promotion.platform.domain.model.funds.FundsAccountManager;
import com.qmfresh.promotion.platform.domain.model.funds.enums.FundsShopTypeEnum;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsAccount;
import com.qmfresh.promotion.platform.interfaces.common.BizCode;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.CheckFundsApplyBillParamDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.FundsApplyCountParamDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.FundsApplyDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.PageQueryFundsApplyCheckParamDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.service.IFundsApplyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 运营后台管理经费申请
 */
@RestController
@RequestMapping(value = "/platform/admin")
@Api(tags = {"运营后台管理经费申请相关接口"})
@Slf4j
public class FundsApplyCheckController extends PlatformBaseController{
    @Resource
    private IFundsApplyService fundsApplyService;

    @Resource
    private FundsAccountManager fundsAccountManager;

    @Resource
    private AdminUserManager adminUser;
    @Resource
    private ShopManager shopManager;
    /**
     * 分页查询门店营销费申请记录
     */
    @ApiOperation("分页查询门店营销费申请记录")
    @PostMapping(value = "/checkFundsList")
    public ServiceResultDTO<PageDTO<FundsApplyCheckRecordDto>> queryFundsApplayRecord(@RequestBody PageQueryFundsApplyCheckParamDTO param) {
        log.info("查询门店营销费申请记录,入参：" + JSON.toJSONString(param));
        return ServiceResultDTO.success(fundsApplyService.pageQueryFundsApplyCheckRecord(param));
    }

    /**
     * 分页查询营销费申请记录
     */
    @ApiOperation("分页查询营销费申请记录")
    @PostMapping(value = "/fundsAccountList")
    public ServiceResultDTO<PageDTO<FundsApplyCheckRecordDto>> fundsAccountList(@RequestBody PageQueryFundsApplyCheckParamDTO param) {
        return ServiceResultDTO.success(fundsApplyService.fundsAccountList(param));
    }

    /**
     * 运营审核申请单
     */
    @PostMapping(value = "/checkFundsStatus")
    @ApiOperation("运营审核申请的单子")
    public ServiceResultDTO<Boolean> CheckFundsApplayRecordStatus(@RequestBody CheckFundsApplyBillParamDTO param) {
        log.info("运营审核申请的单子,入参：" + JSON.toJSONString(param));
        if (null == param || param.getId() == null || param.getId().longValue() == 0 || param.getUserId() == null || StringUtils.isEmpty(param.getUserName())) {
            throw new BusinessException("参数为空！请检查");
        }
        param.setExamineUserId(param.getUserId());
        param.setExamineUserName(param.getUserName());
        try{
            return ServiceResultDTO.success(fundsApplyService.checkFundsApplyBillStatus(param));
        }catch (BusinessException be){
            log.warn("审核异常 ，id={},errorMsg={}",param.getId(), be.getErrorMsg());
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(),be.getErrorMsg());
        }catch (Exception e){
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(),"系统异常");
        }
    }

    /**
     * 审核统计
     */
    @PostMapping(value = "/checkFundsCount")
    @ApiOperation("审核统计")
    public ServiceResultDTO<FundsApplyCountDto> queryCheckApplyCount(@RequestBody FundsApplyCountParamDTO param) {
        return ServiceResultDTO.success(fundsApplyService.queryCheckApplyCount(param));
    }

    /**
     * 申请营销经费
     *
     * @return
     */
    @PostMapping(value = "/apply")
    @ApiOperation("申请营销经费")
    public ServiceResultDTO<Boolean> createFundsApplyBill(@RequestBody FundsApplyCheckParamDTO param, HttpServletRequest request) {
        Integer userId =  param.getOperatorId();
        log.info("总部用户编码,{}", userId);
        param.setUserId(Long.valueOf(userId));
        param.setUserName(param.getOperatorName());
        Assert.hasText(param.getApplyRemark(), "申请原因为空！param:" + JSON.toJSONString(param));
        Boolean fundsApplyBill = fundsApplyService.createApply(param);
        if (fundsApplyBill) {
            return ServiceResultDTO.success(fundsApplyBill);
        } else {
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(), "创建申请单失败，或有申请单在审批中！");
        }
    }
}
