package com.qmfresh.promotion.platform.interfaces.platform;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.coupon.SendCouponReturnBean;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.common.utils.TraceIdUtils;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.dto.PromotionMzCampOnReq;
import com.qmfresh.promotion.platform.application.PromActivityService;
import com.qmfresh.promotion.platform.domain.model.promactivity.enums.ActivityCacheKey;
import com.qmfresh.promotion.platform.domain.service.DLock;
import com.qmfresh.promotion.platform.domain.shared.BizCode;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.util.StringUtil;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/prom/activity")
@Slf4j
@Api(tags = {"活动接口"})
public class PromActivityController {

    @Resource
    private PromActivityService promActivityService;
    @Resource
    private DLock dLock;

    @ApiOperation(value = "活动命中", notes = "活动命中")
    @RequestMapping(value = "/executeStrategy", method = RequestMethod.POST)
    public ServiceResultDTO<PromotionContext> executeStrategy(@Valid @RequestBody PromExecuteStrategyDTO param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("活动命中 请求 traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            PromotionContext context = promActivityService.executeStrategy(param, traceId);
            log.info("活动命中 结果 traceId={} context={}", traceId, JSON.toJSONString(context));
            return ServiceResultDTO.success(context);
        } catch (BusinessException be) {
            log.warn("活动命中 业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), be.getErrorMsg());
        } catch (Exception e) {
            log.error("活动命中 接口异常 traceId={} e={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }


    @ApiOperation(value = "活动奖品预占", notes = "活动奖品预占")
    @RequestMapping(value = "/campOn", method = RequestMethod.POST)
    public ServiceResult mzPromotionCampOn(@RequestBody PromotionMzCampOnReq protocol) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("满赠促销预占，traceId = {} protocal = {}", traceId, JSON.toJSONString(protocol));
        if (StringUtil.isBlank(protocol.getOrderCode())) {
            return ServiceResultUtil.resultError("订单号");
        }
        if (ListUtil.isNullOrEmpty(protocol.getCampOnList())) {
            return ServiceResultUtil.resultError("预占列表不能为空");
        }
        try {
            return ServiceResultUtil.resultSuccess(promActivityService.campOn(protocol, traceId));
        } catch (BusinessException be) {
            log.warn("满赠促销预占-业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
            return ServiceResultUtil.resultError(com.qmfresh.promotion.platform.interfaces.common.BizCode.FAILED.getCode(), be.getErrorMsg());
        } catch (Exception e) {
            log.error("满赠促销预占-异常: traceId={}, ex={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
        }
        return ServiceResultUtil.resultError(com.qmfresh.promotion.platform.interfaces.common.BizCode.FAILED.getCode(), "系统异常");
    }

    @ApiOperation(value = "领取奖励", notes = "领取奖励")
    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public ServiceResultDTO<List<SendCouponReturnBean>> send(@Valid @RequestBody PromActivitySendDTO param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("领取奖励 请求 traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            //防止重复提交
            boolean result = dLock.tryLock(ActivityCacheKey.SEND_AWARD_LOCK.key(param.getSourceOrderNo()), 5000L);
            if (!result) {
                log.info("活动奖品预占 重复提交 traceId={} param={}", traceId, JSON.toJSONString(param));
                return ServiceResultDTO.fail(com.qmfresh.promotion.platform.interfaces.common.BizCode.FAILED.getCode(), "订单处理中");
            }
            return ServiceResultDTO.success(promActivityService.send(param, traceId));
        } catch (Exception e) {
            log.error("领取奖励 接口异常 traceId={} e={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

}
