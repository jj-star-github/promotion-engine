package com.qmfresh.promotion.platform.interfaces.platform.facade.shop;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@ApiModel("门店查询接口")
@Data
public class ShopCpQueryDTO extends BaseDTO {

    @ApiModelProperty("总部改价活动id")
    private Long activityId;
    @ApiModelProperty("总部改价活动商品查询key")
    private Integer skuId;
    @ApiModelProperty("页码")
    private Integer pageNum;
    @ApiModelProperty("页大小")
    private Integer pageSize;
    @ApiModelProperty("改价活动类型")
    private Integer type;
    @ApiModelProperty("门店key")
    private Integer currentShopId;
    @ApiModelProperty("门店类型")
    private Integer shopType;
    @ApiModelProperty("起始活动开始改价时间")
    private Long fromPlanBeginCpActivityTime;
    @ApiModelProperty("终止活动开始改价时间")
    private Long toPlanBeginCpActivityTime;
    @ApiModelProperty("0=全部 1=待生效 2=进行中 3=已失效 ")
    private Integer status;
    @ApiModelProperty("门店活动名称")
    private String activityNameKey;

}
