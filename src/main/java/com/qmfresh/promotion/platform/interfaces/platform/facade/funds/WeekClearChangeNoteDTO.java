package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "WeekClearChangeNoteDTO", description = "去化奖励流水")
public class WeekClearChangeNoteDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("门店名称")
    private String shopName;
    @ApiModelProperty("去化开始日期")
    private Long timeStart;
    @ApiModelProperty("去化结束日期")
    private Long timeEnd;
    @ApiModelProperty("周清比例")
    private BigDecimal percent;
    @ApiModelProperty("门店周清比例")
    private BigDecimal shopPercent;
    @ApiModelProperty("大区周清比例")
    private BigDecimal areaPercent;
    @ApiModelProperty("当期去化总额")
    private BigDecimal reward;
    @ApiModelProperty("去化前门店余额")
    private BigDecimal diffBefore;
    @ApiModelProperty("最低安全值")
    private BigDecimal minPromPrice;
    @ApiModelProperty("门店去化奖励")
    private BigDecimal shopWeekClear;
    @ApiModelProperty("去化后门店余额")
    private BigDecimal diffAfter;
    @ApiModelProperty("大区去化奖励")
    private BigDecimal areaWeekClear;
    @ApiModelProperty("去化奖励时间")
    private String gmtCreate;

}
