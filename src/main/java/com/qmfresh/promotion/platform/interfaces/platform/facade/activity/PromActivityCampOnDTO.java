package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import com.qmfresh.promotion.dto.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel(value = "PromActivityCampOnDTO", description = "预占请求")
public class PromActivityCampOnDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("门店ID")
    private Integer shopId;
    @ApiModelProperty("用户信息")
    private User user;
    @ApiModelProperty("渠道")
    private Integer channel;
    @ApiModelProperty("订单Code补单时会使用")
    private String orderCode;
    @ApiModelProperty("订单实付金额 使用抹零/整单打折/使用优惠券/番茄币之后的金额")
    private BigDecimal goodsAmount;
    @ApiModelProperty("订单商品信息")
    private List<PromActivityCampOnSkuDTO> skuDtoList;

}

