package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsAccount;
import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "ShopFundsAdminAccountDTO", description = "账户信息")
public class ShopFundsAdminAccountDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("账户ID")
    private Long accountId;
    @ApiModelProperty("门店ID")
    private Integer shopId;
    @ApiModelProperty("门店名")
    private String shopName;
    @ApiModelProperty("营销费用")
    private BigDecimal totalAmount;

    public ShopFundsAdminAccountDTO() {}

    public ShopFundsAdminAccountDTO(FundsAccount account) {
        this.setAccountId(account.getId());
        this.setShopId(account.getShopId());
        this.setShopName(account.getShopName());
    }
}

