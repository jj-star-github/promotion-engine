package com.qmfresh.promotion.platform.interfaces.platform.facade.general;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.SkuCpDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("总部改价创建模型定义")
public class GeneralCpCreateDTO extends BaseDTO {

    @ApiModelProperty("活动名称")
    private String activityName;
    @ApiModelProperty("计划活动开始时间")
    private Long beginPlanCpActivityTime;
    @ApiModelProperty("计划活动结束时间")
    private Long endPlanCpActivityTime;
    @ApiModelProperty("门店列表")
    private List<Integer> shopIds;
    @ApiModelProperty("商品列表")
    private List<SkuCpDTO> skuCps;



}
