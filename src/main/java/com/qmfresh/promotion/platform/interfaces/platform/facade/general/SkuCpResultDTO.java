package com.qmfresh.promotion.platform.interfaces.platform.facade.general;

import com.qmfresh.promotion.platform.interfaces.platform.facade.SkuCpDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("商品改价结果信息")
public class SkuCpResultDTO implements Serializable {

    @ApiModelProperty("门店id")
    Integer shopId;

    @ApiModelProperty("门店改价列表")
    List<SkuCpDTO> skuCpDTOs;
}
