package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.platform.domain.model.promactivity.enums.ConditionTypeEnum;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityConditionPO;
import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

@Data
@ApiModel(value = "PromActivityConditionDTO", description = "活动奖品信息")
public class PromActivityConditionDTO extends BaseDTO {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("id,奖品更新时使用")
    private Long id;
    @ApiModelProperty("奖品ID")
    private Long conditionId;
    @ApiModelProperty("奖品名称")
    private String conditionName;
    @ApiModelProperty("奖品类型:1.实物,2.优惠券,3.番茄币")
    private Integer conditionType;
    @ApiModelProperty("级别")
    private Integer level;
    @ApiModelProperty("库存, 99999:无上限")
    private Integer stock;
    @ApiModelProperty("最大发奖次数")
    private Integer maxNum;
    @ApiModelProperty("条件")
    private BigDecimal condition;
    @ApiModelProperty("优惠券信息")
    private PromActivityConditionCouponDTO conditionCouponDTO;

    @ApiModelProperty("活动ID")
    private Long activityId;


    public PromActivityConditionDTO() {}

    public PromActivityConditionDTO(PromActivityConditionPO po) {
        this.setId(po.getId());
        this.setConditionId(po.getAwardId());
        this.setConditionName(po.getAwardName());
        this.setConditionType(po.getAwardType());
        this.setCondition(po.getConditionNum());
        this.setLevel(po.getConditionLevel());
        this.setMaxNum(po.getMaxNum());
        this.setStock(po.getStock());
        if(ConditionTypeEnum.COUPON.getCode() == po.getAwardType() && StringUtils.isNotBlank(po.getRemark())) {
            this.setConditionCouponDTO(JSON.parseObject(po.getRemark(), new TypeReference<PromActivityConditionCouponDTO>() {
            }));
        }
    }

}

