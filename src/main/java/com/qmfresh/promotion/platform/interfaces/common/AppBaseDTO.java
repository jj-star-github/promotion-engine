package com.qmfresh.promotion.platform.interfaces.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("应用网关基础参数")
public class AppBaseDTO implements Serializable {

    @ApiModelProperty("操作人id")
    private Integer operatorId;
    @ApiModelProperty("操作人名称")
    private String operatorName;
}
