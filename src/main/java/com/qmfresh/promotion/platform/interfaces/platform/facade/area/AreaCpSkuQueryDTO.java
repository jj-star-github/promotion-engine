package com.qmfresh.promotion.platform.interfaces.platform.facade.area;

import com.qmfresh.promotion.platform.domain.model.activity.cp.area.command.AreaCpSkuQueryCommand;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@ApiModel("大区改价商品查询")
@Data
public class AreaCpSkuQueryDTO implements Serializable {

    @ApiModelProperty("页码")
    private Integer pageNum;
    @ApiModelProperty("页大小")
    private Integer pageSize;
    @ApiModelProperty("总部活动id")
    private Long activityId;
    @ApiModelProperty("商品id")
    private Integer skuId;
    @ApiModelProperty("三级分类名称")
    private String class3Name;

    public static AreaCpSkuQueryCommand createCommand(AreaCpSkuQueryDTO areaCpSkuQueryDTO) {
        AreaCpSkuQueryCommand skuQueryCommand = AreaCpSkuQueryCommand.create(
                areaCpSkuQueryDTO.getPageNum(),
                areaCpSkuQueryDTO.getPageSize(),
                areaCpSkuQueryDTO.getActivityId(),
                areaCpSkuQueryDTO.getSkuId(),
                areaCpSkuQueryDTO.getClass3Name()
        );

        return skuQueryCommand;
    }
}
