package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;


@Data
@ApiModel(value = "PromActivityQueryDTO", description = "查询活动信息")
public class PromActivityQueryDTO extends BaseDTO {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("活动ID")
    private String id;
    @ApiModelProperty("活动名称")
    private String name;
    @ApiModelProperty("活动类型:1.满赠")
    private Integer activityType;
    @ApiModelProperty("活动规则类型：{activityType=1,1.每满，2.阶梯}")
    private Integer ruleType;
    @ApiModelProperty("活动开始时间")
    private Date beginTime;
    @ApiModelProperty("活动结束时间")
    private Date endTime;
    @ApiModelProperty("活动状态:1.待进行,3.进行中,4.已取消,6=结束中,7=已关闭,8=已结束")
    private Integer status;
    @ApiModelProperty("渠道:1.全渠道,2.线上,3.线下")
    private Integer channel;

    @ApiModelProperty("页码 默认：1")
    private int pageNum = 1;
    @ApiModelProperty("分页大小 默认：20")
    private int pageSize = 20;

    public Integer getStart() {
        return (this.pageNum-1)*this.pageSize;
    }

    public PromActivityQueryDTO() {}

}

