package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import com.qmfresh.promotion.dto.PromotionProtocol;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel(value = "PromExecuteStrategyDTO", description = "命中请求接口")
public class PromExecuteStrategyDTO extends PromotionProtocol {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("番茄币抵扣金额")
    private BigDecimal coinToMoney = BigDecimal.ZERO;
    @ApiModelProperty("抹零金额")
    private BigDecimal freeMoney = BigDecimal.ZERO;
    @ApiModelProperty("可用优惠券信息")
    private PromCouponInfoDTO couponInfoDTO;

}

