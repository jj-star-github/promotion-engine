package com.qmfresh.promotion.platform.interfaces.platform.facade;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpItemResult;
import com.qmfresh.promotion.platform.interfaces.platform.facade.general.SkuCpResultDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.collections.CollectionUtils;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("改价创建结果")
public class CpCreateResultDTO implements Serializable {

    @ApiModelProperty("总部活动id")
    private Long activityId;

    @ApiModelProperty("总部改价失败列表,做多3000条，为了防止系统异常")
    private List<SkuCpResultDTO> failedList;

    /**
     * 推总部改价
     *
     * @param list
     * @param skuCps
     * @return
     */
    public static CpCreateResultDTO create(List<CpItem> list, List<SkuCpDTO> skuCps) {
        Map<Integer, List<CpItem>> cpItemMap = list.stream().collect(Collectors.groupingBy(CpItem::getShopId));
        Map<Integer, SkuCpDTO> map = skuCps.stream().collect(Collectors.toMap(SkuCpDTO::getSkuId, o -> o));

        List<SkuCpResultDTO> skuCpResultDTOS = new ArrayList<>();

        for (Integer shopId : cpItemMap.keySet()) {
            SkuCpResultDTO skuCpResultDTO = new SkuCpResultDTO();
            skuCpResultDTO.setShopId(shopId);
            List<SkuCpDTO> skuCpDTOS = new ArrayList<>();
            for (CpItem item : cpItemMap.get(shopId)) {
                skuCpDTOS.add(map.get(item.getSkuId()));
            }
            skuCpResultDTO.setSkuCpDTOs(skuCpDTOS);
            skuCpResultDTOS.add(skuCpResultDTO);
        }

        CpCreateResultDTO cpCreateResultDTO = new CpCreateResultDTO();
        cpCreateResultDTO.failedList = skuCpResultDTOS;
        return cpCreateResultDTO;
    }

    public static CpCreateResultDTO result(CpItemResult cpItemResult) {
        CpCreateResultDTO cpCreateResultDTO = new CpCreateResultDTO();
        cpCreateResultDTO.setActivityId(cpItemResult.getActivityId());

        List<CpItem> cpItems = cpItemResult.getCpItemList();
        if (CollectionUtils.isEmpty(cpItems)) {
            cpCreateResultDTO.setFailedList(new ArrayList<>());
        } else {
            List<SkuCpResultDTO> skuCpResultDTOS = new ArrayList<>();
            for (CpItem cpItem : cpItems) {
                SkuCpDTO skuCpDTO = new SkuCpDTO();
                skuCpDTO.setRulePrice(cpItem.getRulePrice());
                skuCpDTO.setSkuId(cpItem.getSkuId());
                skuCpDTO.setRemark(cpItem.getRemark());
                skuCpDTO.setPlanMarketingFee(cpItem.getPlanMarketingFee());
                SkuCpResultDTO skuCpResultDTO = new SkuCpResultDTO();
                skuCpResultDTO.setSkuCpDTOs(Collections.singletonList(skuCpDTO));
                skuCpResultDTO.setShopId(cpItem.getShopId());
                skuCpResultDTOS.add(skuCpResultDTO);
            }
            cpCreateResultDTO.setFailedList(skuCpResultDTOS);
        }
        return cpCreateResultDTO;
    }
}
