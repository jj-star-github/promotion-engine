package com.qmfresh.promotion.platform.interfaces.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * http://wiki.qmgyl.net/pages/viewpage.action?pageId=15204503
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel(value = "BaseDTO", description = "请求基础信息")
public class BaseDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("门店ID")
    private Integer shopId;
    @ApiModelProperty("渠道")
    private Integer channel;
    @ApiModelProperty("版本号")
    private Integer version;
    @ApiModelProperty("IP地址")
    private String ip;
    @ApiModelProperty("操作人id")
    private Integer operatorId;
    @ApiModelProperty("操作人名称")
    private String operatorName;


}
