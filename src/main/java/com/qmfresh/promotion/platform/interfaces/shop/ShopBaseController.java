package com.qmfresh.promotion.platform.interfaces.shop;


import com.qmfresh.promotion.platform.interfaces.MVCExceptionHandle;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * 获取请求基础信息
 */
public abstract class ShopBaseController extends MVCExceptionHandle {


    /**
     * 获取request
     *
     * @return
     */
    public HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    /**
     * 获取门店Id
     *
     * @return
     */
    protected Integer getShopId() {
        String shopId = getRequest().getHeader("shop_id");
        if (StringUtils.isBlank(shopId)) {
            shopId = getRequest().getParameter("shop_id");
        }
        if (StringUtils.isBlank(shopId)) {
            throw new ShopOauthException();
        }
        return Integer.valueOf(shopId);
    }

    public static class ShopOauthException extends RuntimeException {
        private static final long serialVersionUID = 5067141585734438228L;
    }

}
