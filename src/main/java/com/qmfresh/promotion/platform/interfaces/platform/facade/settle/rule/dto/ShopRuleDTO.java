package com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto;

import com.qmfresh.promotion.platform.interfaces.common.AppBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "ShopRuleDTO", description = "创建门店营销费用规则")
public class ShopRuleDTO extends AppBaseDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("id")
    private Long id;
    @ApiModelProperty("门店等级(123)")
    private Integer shopGrade;
    @ApiModelProperty("门店类别(1.A,2.B,3.C)")
    private Integer shopType;
    @ApiModelProperty("损耗率")
    private BigDecimal lossPercent;
    @ApiModelProperty("营销费率")
    private BigDecimal promotionPercent;
    @ApiModelProperty("总部占比")
    private BigDecimal basePercent;
    @ApiModelProperty("门店占比")
    private BigDecimal shopPercent;
    @ApiModelProperty("大区占比")
    private BigDecimal areaPercent;

    @ApiModelProperty("周清比例")
    private BigDecimal weekClearPercent;
    @ApiModelProperty("门店周清比例")
    private BigDecimal weekClearShopPercent;
    @ApiModelProperty("大区周清比例")
    private BigDecimal weekClearAreaPercent;

}
