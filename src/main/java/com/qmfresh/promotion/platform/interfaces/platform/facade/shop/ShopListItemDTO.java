package com.qmfresh.promotion.platform.interfaces.platform.facade.shop;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("门店改价商品列表")
public class ShopListItemDTO implements Serializable {

    @ApiModelProperty("门店改价活动id")
    private Long activityId;
    @ApiModelProperty("门店活动名称")
    private String activityName;
    @ApiModelProperty("门店名称")
    private String shopName;
    @ApiModelProperty("门店level")
    private String shopLevel;
    @ApiModelProperty("门店类型")
    private String shopTypeDesc;
    @ApiModelProperty("门店改价类型")
    private String typeDesc;
    @ApiModelProperty("商品id")
    private Integer skuId;
    @ApiModelProperty("商品名称")
    private String skuName;
    @ApiModelProperty("目标价")
    private BigDecimal rulePrice;
    @ApiModelProperty("改价差额")
    private BigDecimal marketingDiffFee;
    @ApiModelProperty("活动计划开始时间")
    private Long planBeginCpActivityTime;
    @ApiModelProperty("活动计划结束时间")
    private Long planEndCpActivityTime;
    @ApiModelProperty("状态")
    private String statusDesc;
    @ApiModelProperty("计划营销费用")
    private BigDecimal planMarketingFee;
    @ApiModelProperty("实际营销费用")
    private BigDecimal realMarketingFee;
    @ApiModelProperty("创建时间")
    private Long createTime;
    @ApiModelProperty("最后操作者")
    private String lastModifiedPerson;

    public static ShopListItemDTO convert(ShopCpActivity shopCpActivity) {
        ShopListItemDTO shopListItemDTO = new ShopListItemDTO();
        shopListItemDTO.activityId = shopCpActivity.getActivityId();
        shopListItemDTO.activityName = shopCpActivity.getName();
        shopListItemDTO.shopName = shopCpActivity.getShop().getShopName();
        shopListItemDTO.shopTypeDesc = shopCpActivity.getShop().getShopTypeDesc();
        shopListItemDTO.skuId = shopCpActivity.getSku().getSkuId();
        shopListItemDTO.skuName = shopCpActivity.getSku().getSkuName();
        shopListItemDTO.rulePrice = shopCpActivity.getRulePrice();
        shopListItemDTO.marketingDiffFee = shopCpActivity.getMarketingDiffFee();
        shopListItemDTO.planBeginCpActivityTime = shopCpActivity.getPlanActivityStartTime().getTime();
        shopListItemDTO.planEndCpActivityTime = shopCpActivity.getPlanActivityEndTime().getTime();
        shopListItemDTO.planMarketingFee = shopCpActivity.getPlanMarketingFee();
        shopListItemDTO.createTime = shopCpActivity.getCreateTime().getTime();
        shopListItemDTO.lastModifiedPerson = shopCpActivity.getLastOperatorPerson();
        shopListItemDTO.typeDesc = shopCpActivity.getBizType().getMsg();
        shopListItemDTO.statusDesc = CpStatus.statusDesc(shopCpActivity.getCpStatus());
        shopListItemDTO.lastModifiedPerson = shopCpActivity.getLastOperatorPerson();

        return shopListItemDTO;
    }
}
