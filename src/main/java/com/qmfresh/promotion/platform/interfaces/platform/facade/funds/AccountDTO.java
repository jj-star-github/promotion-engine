package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel(value = "AccountDTO", description = "账户信息")
public class AccountDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("账户ID")
    private Long id;
    @ApiModelProperty("账户名称")
    private String shopName;
    @ApiModelProperty("账户类型")
    private Integer shopType;
    @ApiModelProperty("账户绑定")
    private String shopAccount;
    @ApiModelProperty("可用营销费")
    private BigDecimal totalAmount;
    @ApiModelProperty("最小安全值")
    private BigDecimal minPromPrice;
    @ApiModelProperty("最大安全值")
    private BigDecimal maxPromPrice;
    @ApiModelProperty("账户状态")
    private Integer status;
    @ApiModelProperty("备注")
    private String remark;
    @ApiModelProperty("创建时间")
    private Date gmtCreate;
    @ApiModelProperty("最后编辑者")
    private String operatorName;
    @ApiModelProperty("营销费比例")
    private BigDecimal amountPercent;
    @ApiModelProperty("上级账户")
    private String parentAccount;
    @ApiModelProperty("shopId")
    private Integer shopId;
}
