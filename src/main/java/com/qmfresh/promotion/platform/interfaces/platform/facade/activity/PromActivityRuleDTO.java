package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel(value = "PromActivityRuleDTO", description = "活动满赠规则")
public class PromActivityRuleDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("活动满足条件")
    private BigDecimal meetMoney;

}

