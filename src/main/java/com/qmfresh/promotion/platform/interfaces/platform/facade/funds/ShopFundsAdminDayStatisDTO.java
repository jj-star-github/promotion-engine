package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "ShopFundsAdminDayStatisDTO", description = "费用日统计")
public class ShopFundsAdminDayStatisDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("今日总部使用营销费用")
    private Integer accountType;
    @ApiModelProperty("今日门店使用营销费用")
    private BigDecimal todayShopUseAmount;
    @ApiModelProperty("总部今日锁定")
    private BigDecimal todayLock;
    @ApiModelProperty("总部今日锁定")
    private BigDecimal shopTodayLock;
    @ApiModelProperty("昨日总部增加费用")
    private BigDecimal useAmount;
    @ApiModelProperty("昨日门店增加")
    private BigDecimal newAmount;
    @ApiModelProperty("总部费用")
    private BigDecimal amount;
    @ApiModelProperty("门店费用")
    private BigDecimal shopAmount;

}
