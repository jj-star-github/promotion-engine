package com.qmfresh.promotion.platform.interfaces.platform.facade.area;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpItem;
import com.qmfresh.promotion.platform.interfaces.platform.facade.general.SkuCpResultDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@ApiModel("大区改价结果")
public class AreaCpCreateResultDTO implements Serializable {

    @ApiModelProperty("大区改价失败列表")
    private List<SkuCpResultDTO> failedList;

    public static AreaCpCreateResultDTO create(List<CpItem> cpItems) {
        return null;
    }
}
