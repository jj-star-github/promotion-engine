package com.qmfresh.promotion.platform.interfaces.shop.facade.funds;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel(value = "ShopWeekClearDetailDTO", description = "门店去化奖励详情")
public class ShopWeekClearDetailDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("去化奖励")
    private BigDecimal weekAmount;
    @ApiModelProperty("开始日期")
    private String dayStart;
    @ApiModelProperty("结束日期")
    private String dayEnd;
    @ApiModelProperty("计算公式")
    private String formula;
    @ApiModelProperty("备注")
    private String remark;

}
