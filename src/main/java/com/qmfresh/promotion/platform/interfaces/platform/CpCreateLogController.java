package com.qmfresh.promotion.platform.interfaces.platform;

import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.CpCreateLogFacadeService;
import com.qmfresh.promotion.platform.interfaces.platform.facade.log.CpCreateLogDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.log.CpCreateLogQueryDTO;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Api(tags = "创建改价日志接口")
@RestController
@RequestMapping("/platform/cp")
public class CpCreateLogController {

    @Resource
    private CpCreateLogFacadeService cpCreateLogFacadeService;

    @RequestMapping("/log/page")
    public ServiceResultDTO<PageDTO<CpCreateLogDTO>> page(@RequestBody CpCreateLogQueryDTO queryDTO) {
        return ServiceResultDTO.success(cpCreateLogFacadeService.page(queryDTO));
    }
}
