package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import com.qmfresh.promotion.dto.PromotionSsuContext;
import com.qmfresh.promotion.platform.domain.shared.BigDecimalUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "PromActivityCampOnSkuDTO", description = "预占商品请求")
public class PromActivityCampOnSkuDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("skuId")
    private Integer skuId;
    @ApiModelProperty("一级分类")
    private Integer class1Id;
    @ApiModelProperty("二级分类")
    private Integer class2Id;
    @ApiModelProperty("数量（重量）")
    private BigDecimal amount;
    @ApiModelProperty("实付价格")
    private BigDecimal realAmount;

    public PromActivityCampOnSkuDTO() {}

    public PromActivityCampOnSkuDTO(PromotionSsuContext ssuContext) {
        this.setSkuId(ssuContext.getSkuId());
        this.setClass1Id(ssuContext.getClass1Id());
        this.setClass2Id(ssuContext.getClass2Id());
        this.setAmount(ssuContext.getAmount());
        this.setRealAmount(ssuContext.getRealMoney());
    }

}

