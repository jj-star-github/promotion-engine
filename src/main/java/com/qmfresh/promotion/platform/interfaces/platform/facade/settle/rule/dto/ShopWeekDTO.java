package com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "ShopWeekDTO", description = "查询门店营销费用规则")
public class ShopWeekDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("大区周清比例")
    private BigDecimal areaWeekClearPercent;
    @ApiModelProperty("门店周清比例")
    private BigDecimal shopWeekClearPercent;
    @ApiModelProperty("最小安全值")
    private BigDecimal minPromPrice;
}
