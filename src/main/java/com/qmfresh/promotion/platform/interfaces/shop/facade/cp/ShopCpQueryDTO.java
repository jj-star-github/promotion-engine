package com.qmfresh.promotion.platform.interfaces.shop.facade.cp;

import com.qmfresh.promotion.platform.interfaces.common.AppBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("门店改价查询对象")
public class ShopCpQueryDTO extends AppBaseDTO {

    @ApiModelProperty("页码")
    private Integer pageNum;
    @ApiModelProperty("页大小")
    private Integer pageSize;
    @ApiModelProperty("门店id")
    private Integer shopId;
    @ApiModelProperty("skuId")
    private Integer skuId;
    @ApiModelProperty("skuName")
    private String skuName;
    @ApiModelProperty("状态:0=全部 1=进行中 2=已过期 3=待生效")
    private Integer status;
    @ApiModelProperty("商品key")
    private String skuKey;

}
