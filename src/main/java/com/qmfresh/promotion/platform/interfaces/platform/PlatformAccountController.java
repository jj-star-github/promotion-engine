package com.qmfresh.promotion.platform.interfaces.platform;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.application.PlatformAccountService;
import com.qmfresh.promotion.platform.application.PlatformFundsService;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.promotion.platform.interfaces.common.BizCode;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.account.AccountCreateDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.AccountDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.AccountQueryDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/platform/account")
@Slf4j
@Api(tags = {"管理后台营销账户"})
public class PlatformAccountController {
    @Resource
    private PlatformAccountService platformAccountService;
    @Resource
    private PlatformFundsService platformFundsService;

    @ApiOperation(value = "创建账号信息", notes = "创建账号信息")
    @RequestMapping(value = "/createAccount", method = RequestMethod.POST)
    public ServiceResultDTO<Boolean> createAccount(@Valid @RequestBody AccountCreateDTO accountCreateDTO){
        log.info("into method createAccount,入参:" + JSON.toJSONString(accountCreateDTO));
        try{
            return ServiceResultDTO.success(platformAccountService.createAccount(accountCreateDTO));
        }catch (BusinessException be){
            log.warn("创建账号异常 ，shopId={}, errorMsg={}", accountCreateDTO.getShopId(),be.getErrorMsg());
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(),be.getErrorMsg());
        }catch (Exception e){
            log.error("创建账号异常：shopId={}, ex={}",accountCreateDTO.getShopId(),LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(),"系统异常");
        }
    }

    @ApiOperation(value = "编辑账号信息", notes = "编辑账号信息")
    @RequestMapping(value = "updataAccount", method = RequestMethod.POST)
    public ServiceResultDTO<Boolean> updataAccount(@Valid @RequestBody AccountCreateDTO accountCreateDTO){
        log.info("into method updataAccount,入参：" + JSON.toJSONString(accountCreateDTO));
        try{
            return ServiceResultDTO.success(platformAccountService.updataAccount(accountCreateDTO));
        } catch (BusinessException be){
            log.warn("编辑账号异常 ，shopId={}, errorMsg={}", accountCreateDTO.getShopId(),be.getErrorMsg());
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(),be.getErrorMsg());
        }catch (Exception e){
            log.error("编辑账号异常：shopId={}, ex={}", accountCreateDTO.getShopId(),LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(),"系统异常");
        }
    }

    @ApiOperation(value = "查询账户信息",notes = "查询账户信息")
    @RequestMapping(value = "/queryAccountList", method = RequestMethod.POST)
    public ServiceResultDTO<PageDTO<AccountDTO>> queryAccountList(@Valid @RequestBody AccountQueryDTO param){
        log.info("查询账户信息 请求 param={}",JSON.toJSONString(param));
        try{
            return ServiceResultDTO.success(platformFundsService.queryAccountList(param));
        }catch (Exception e){
            log.error("查询账户信息 接口异常 e={}",LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(com.qmfresh.promotion.platform.domain.shared.BizCode.ERROR.getCode(),"");
        }
    }
}
