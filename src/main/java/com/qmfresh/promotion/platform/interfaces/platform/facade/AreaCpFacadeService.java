package com.qmfresh.promotion.platform.interfaces.platform.facade;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.cp.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.AreaCpActivity;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.AreaCpActivityManager;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.command.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.command.CpCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.command.ShopQueryCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLog;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLogManager;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLogType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.command.CpCreateLogCreateCommand;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.area.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class AreaCpFacadeService {

    @Resource
    private AreaCpActivityManager areaCpActivityManager;
    @Resource
    private CpManager cpManager;
    @Resource
    private CpCreateLogManager cpCreateLogManager;

    /**
     * 创建大区活动
     *
     * @param areaCpCreateDTO 大区活动的创建
     * @return 大区活动创建的结果
     */
    public CpCreateResultDTO create(AreaCpCreateDTO areaCpCreateDTO) {
        if (areaCpCreateDTO.getMarketingAccountId() == null) {
            throw new BusinessException("大区活动必须设置大区账号");
        }

        Operator operator = Operator.create(areaCpCreateDTO.getOperatorId(), areaCpCreateDTO.getOperatorName());

        CpCreateCommand cpCreateCommand
                = CpCreateCommand.create(operator,
                areaCpCreateDTO.getActivityName(),
                CpTimeCycle.create(areaCpCreateDTO.getBeginPlanCpActivityTime(), areaCpCreateDTO.getEndPlanCpActivityTime()),
                areaCpCreateDTO.getShopIds(),
                SkuCpDTO.create(areaCpCreateDTO.getSkuCps()),
                areaCpCreateDTO.getMarketingAccountId(),
                CpBizType.CP_AREA
        );


        CpCreateLogCreateCommand createLogCreateCommand =
                CpCreateLogCreateCommand.create(
                        CpBizType.CP_AREA,
                        JSON.toJSONString(cpCreateCommand),
                        areaCpCreateDTO.getOperatorId(),
                        areaCpCreateDTO.getOperatorName(),
                        CpCreateLogType.MANUAL);
        CpCreateLog cpCreateLog = cpCreateLogManager.create(createLogCreateCommand);
        log.info("创建了创建改价日志：{}",cpCreateLog.getId());
        return new CpCreateResultDTO();
    }

    /**
     * 分页查询大区活动详情
     *
     * @param areaCpQueryDTO
     * @return
     */
    public PageDTO<AreaCpListItemDTO> pageAreaCps(AreaCpQueryDTO areaCpQueryDTO) {
        AreaCpQueryCommand areaCpQueryCommand = AreaCpQueryDTO.createCommand(areaCpQueryDTO);
        Page<AreaCpActivity> areaCpActivityPage = areaCpActivityManager.pageAreaCpList(areaCpQueryCommand);
        return PageDTO.convert(areaCpQueryDTO.getPageNum(), areaCpQueryDTO.getPageSize(), areaCpActivityPage,
                AreaCpListItemDTO::convert);
    }

    public PageDTO<CpSkuDTO> pageAreaCpSkus(AreaCpSkuQueryDTO areaCpSkuQueryDTO) {
        AreaCpSkuQueryCommand skuQueryCommand = AreaCpSkuQueryDTO.createCommand(areaCpSkuQueryDTO);
        Page<SkuCpItem> skuCpItemPage = areaCpActivityManager.pageAreaSkuList(skuQueryCommand);

        return PageDTO.convert(areaCpSkuQueryDTO.getPageNum(),
                areaCpSkuQueryDTO.getPageSize(),
                skuCpItemPage,
                CpSkuDTO::convert);
    }

    /**
     * 编辑大区活动商品或者删除
     *
     * @param skuDTO 商品明细
     */
    public void editSkuOfAreaCp(AreaCpEditSkuDTO skuDTO) {

        if (skuDTO.getOperatorType() == null) {
            throw new BusinessException("必须设置操作类型");
        }
        if (skuDTO.getActivityId() == null) {
            throw new BusinessException("必须指定活动id");
        }

        if (skuDTO.getOperatorType() == 1) {
            AreaCpSkuEditCommand areaCpSkuEditCommand = AreaCpEditSkuDTO.createEditCommand(skuDTO);
            areaCpActivityManager.editSku(areaCpSkuEditCommand);
            return;
        }

        if (skuDTO.getOperatorType() == 2) {
            AreaCpSkuDelCommand areaCpSkuDelCommand = AreaCpEditSkuDTO.createDelCommand(skuDTO);
            areaCpActivityManager.delSku(areaCpSkuDelCommand);
            return;
        }
        throw new BusinessException("操作类型不支持");

    }


    public void closeAreaCpActivity(AreaCpCloseDTO areaCpCloseDTO) {
        AreaCpCloseCommand closeCommand = AreaCpCloseDTO.createCommand(areaCpCloseDTO);
        areaCpActivityManager.close(closeCommand);
    }

    public PageDTO<ShopCpResultDTO> pageShopOfAreaCpActivity(AreaCpQueryDTO areaCpQueryDTO) {
        ShopQueryCommand shopQueryCommand = ShopQueryCommand.create(areaCpQueryDTO.getPageNum(),
                areaCpQueryDTO.getPageSize(), areaCpQueryDTO.getActivityId(), CpBizType.CP_AREA);
        Page<ShopCpResult> page = cpManager.pageShopCp(shopQueryCommand);
        return PageDTO.convert(areaCpQueryDTO.getPageNum(), areaCpQueryDTO.getPageSize(), page, ShopCpResultDTO::convert);
    }
}
