package com.qmfresh.promotion.platform.interfaces.common;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel(value = "ServiceResultDTO", description = "基础响应")
public class ServiceResultDTO<T> implements Serializable {

    private static final long serialVersionUID = 7812556364757470645L;

    private Boolean success = true;
    private Integer errorCode;
    private String message;
    private T body;

    private ServiceResultDTO() {
    }

    /**
     * 创建成功
     *
     * @param body
     * @param <T>
     * @return
     */
    public static <T> ServiceResultDTO<T> success(T body) {
        return create(true, BizCode.SUCCESS.getCode(), BizCode.SUCCESS.name(), body);
    }

    /**
     * 创建失败
     *
     * @param errorCode 错误码
     * @param message   消息内容
     * @param <T>       泛型
     * @return ServiceResultDTO
     */
    public static <T> ServiceResultDTO<T> fail(Integer errorCode, String message) {
        return create(false, errorCode, message, null);
    }

    /**
     * 创建
     *
     * @param success   是否成功
     * @param errorCode 错误码
     * @param message   错误消息
     * @param body      内容
     * @param <T>       类型
     * @return ServiceResult的对象
     */
    private static <T> ServiceResultDTO<T> create(Boolean success, Integer errorCode, String message, T body) {
        ServiceResultDTO<T> serviceResultDTO = new ServiceResultDTO<>();
        serviceResultDTO.setSuccess(success);
        serviceResultDTO.setErrorCode(errorCode);
        serviceResultDTO.setMessage(message);
        serviceResultDTO.setBody(body);
        return serviceResultDTO;
    }


}
