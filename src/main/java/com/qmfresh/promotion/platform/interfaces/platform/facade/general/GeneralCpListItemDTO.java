package com.qmfresh.promotion.platform.interfaces.platform.facade.general;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpActivity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@ApiModel("总部改价列表明细对象")
@Data
@ToString
public class GeneralCpListItemDTO implements Serializable {
    @ApiModelProperty("改价活动id")
    private Long activityId;
    @ApiModelProperty("改价活动名称")
    private String activityName;
    @ApiModelProperty("活动类型")
    private Integer type;
    @ApiModelProperty("改价类型描述")
    private String typeDesc;
    @ApiModelProperty("门店数")
    private Integer shopCount;
    @ApiModelProperty("商品数")
    private Integer skuCount;
    @ApiModelProperty("计划活动开始时间")
    private Long planBeginCpActivityTime;
    @ApiModelProperty("计划活动结束时间")
    private Long planEndCpActivityTime;
    @ApiModelProperty("活动状态描述")
    private String statusDesc;
    @ApiModelProperty("计划（锁定）营销费用")
    private BigDecimal planMarketingFee;
    @ApiModelProperty("实际使用营销费用")
    private BigDecimal realMarketingFee;
    @ApiModelProperty("创建时间")
    private Long createTime;
    @ApiModelProperty("最后编辑人")
    private String lastModifyPersonName;


    public static GeneralCpListItemDTO convert(GeneralCpActivity generalCpActivity) {
        GeneralCpListItemDTO generalCpListItemDTO = new GeneralCpListItemDTO();
        generalCpListItemDTO.activityId = generalCpActivity.getActivityId();
        generalCpListItemDTO.activityName = generalCpActivity.getActivityName();
        generalCpListItemDTO.type = generalCpActivity.getCpBizType().getCode();
        generalCpListItemDTO.typeDesc = generalCpActivity.getCpBizType().getMsg();
        generalCpListItemDTO.shopCount = generalCpActivity.getShopCount();
        generalCpListItemDTO.skuCount = generalCpActivity.getSkuCount();
        generalCpListItemDTO.planBeginCpActivityTime = generalCpActivity.getCpTimeCycle().getBeginTime().getTime();
        generalCpListItemDTO.planEndCpActivityTime = generalCpActivity.getCpTimeCycle().getEndTime().getTime();
        generalCpListItemDTO.planMarketingFee = generalCpActivity.getSumPlanMarketingFee();
        generalCpListItemDTO.realMarketingFee = BigDecimal.ZERO;
        generalCpListItemDTO.createTime = generalCpActivity.getCreateTime().getTime();
        generalCpListItemDTO.lastModifyPersonName = generalCpActivity.getLastModifiedPerson();
        generalCpListItemDTO.statusDesc = CpStatus.statusDesc(generalCpActivity.getCpStatus());
        return generalCpListItemDTO;
    }


}
