package com.qmfresh.promotion.platform.interfaces.shop.facade.funds;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "ShopFundsUseTypeDTO", description = "门店费用流水明细")
public class ShopFundsUseTypeDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("使用类型")
    private Long useType;
    @ApiModelProperty("名称")
    private String name;
}
