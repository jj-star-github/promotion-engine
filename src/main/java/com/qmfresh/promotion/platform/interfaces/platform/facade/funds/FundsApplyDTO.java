package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * t_funds_apply
 * @author 
 */
@Data
public class FundsApplyDTO implements Serializable {
    private BigDecimal currentAmount;
    private Long id;

    /**
     * 账户ID
     */
    private Long accountId;
    private String  accountName;
    /**
     * 申请人用户ID
     */
    private Long applyUserId;

    /**
     * 申请人用户名
     */
    private String applyUserName;

    /**
     * 申请额度
     */
    private BigDecimal applyAmount;

    /**
     * 申请描述
     */
    private String applyRemark;

    /**
     * 申请类型，1.用户，2.系统
     */
    private Integer applyType;

    /**
     * 审核人用户ID
     */
    private Long examineUserId;

    /**
     * 审核人用户名
     */
    private String examineUserName;

    /**
     * 审核描述
     */
    private String examineRemark;

    /**
     * 审核状态:0.审核中，1.通过，2.驳回
     */
    private Byte status;

    /**
     * 创建时间
     */
    private Integer gmtCreate;

    /**
     * 更改时间
     */
    private Integer gmtModified;

    /**
     * 删除标记
     */
    private Integer isDeleted;

    /**
     * 申请进程
     */
    private String applyProgress;

    /**
     * 审核类型，1.系统自动审核 2.独裁审批 3.流程审批
     */
    private Integer examineType;

    private Integer shopId;
    private Integer shopType;
    private static final long serialVersionUID = 1L;

}