package com.qmfresh.promotion.platform.interfaces.shop.facade.funds;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "ShopFundsAccountDTO", description = "门店费用账户信息")
public class ShopFundsAccountDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("可用营销费用")
    private BigDecimal totalAmount = new BigDecimal("0");
    @ApiModelProperty("锁定营销费用")
    private BigDecimal lockAmount = new BigDecimal("0");
    @ApiModelProperty("今日使用营销费用")
    private BigDecimal useAmount = new BigDecimal("0");
    @ApiModelProperty("今日新增营销费用")
    private BigDecimal newAmount = new BigDecimal("0");
    @ApiModelProperty("今日销售额")
    private BigDecimal reelPay = new BigDecimal("0");
    @ApiModelProperty("今日营销销售额")
    private BigDecimal marketingPay = new BigDecimal("0");

}
