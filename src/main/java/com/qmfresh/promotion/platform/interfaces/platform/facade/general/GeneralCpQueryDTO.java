package com.qmfresh.promotion.platform.interfaces.platform.facade.general;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.command.GeneralCpQueryCommand;
import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@ApiModel("总部改价查询接口")
@Data
@ToString
public class GeneralCpQueryDTO extends BaseDTO {
    @ApiModelProperty(value = "总部活动活动id")
    private Long activityId;
    @ApiModelProperty(value = "综合活动名称")
    private String activityNameKey;
    @ApiModelProperty("页码")
    private Integer pageNum;
    @ApiModelProperty("页大小")
    private Integer pageSize;
    /**
     * @see com.qmfresh.promotion.platform.domain.model.activity.cp.CpBizType
     */
    @ApiModelProperty("改价活动类型")
    private Integer cpBizType;
    @ApiModelProperty("门店id")
    private Integer queryShopId;
    @ApiModelProperty("门店类型")
    private Integer shopType;
    @ApiModelProperty("商品id")
    private Integer skuId;
    @ApiModelProperty("起始活动开始改价时间")
    private Long fromPlanBeginCpActivityTime;
    @ApiModelProperty("终止活动开始改价时间")
    private Long toPlanBeginCpActivityTime;
    @ApiModelProperty("状态:0=全部 1=待生效 3=已过期 2=进行中")
    private Integer status;

    public static GeneralCpQueryCommand create(GeneralCpQueryDTO generalCpQueryDTO) {

        List<CpStatus> cpStatusList = new ArrayList<>();
        if (generalCpQueryDTO.getStatus() == null) {

        } else {
            if (generalCpQueryDTO.getStatus() == 0) {
            }
            if (generalCpQueryDTO.getStatus() == 1) {
                cpStatusList.add(CpStatus.CREATE);
            }
            if (generalCpQueryDTO.getStatus() == 2) {
                cpStatusList.add(CpStatus.RUNNING);
            }
            if (generalCpQueryDTO.getStatus() == 3) {
                cpStatusList.add(CpStatus.CLOSED);
                cpStatusList.add(CpStatus.FINISHED);
            }
        }

        GeneralCpQueryCommand generalCpQueryCommand = GeneralCpQueryCommand.builder()
                .pageNum(generalCpQueryDTO.getPageNum())
                .pageSize(generalCpQueryDTO.getPageSize())
                .activityId(generalCpQueryDTO.getActivityId())
                .activityNameKey(generalCpQueryDTO.getActivityNameKey())
                .planBeginTimeEnd(generalCpQueryDTO.getToPlanBeginCpActivityTime() == null ? null :
                        new Date(generalCpQueryDTO.getToPlanBeginCpActivityTime()))
                .planBeginTimeStart(generalCpQueryDTO.getFromPlanBeginCpActivityTime() == null ? null :
                        new Date(generalCpQueryDTO.getFromPlanBeginCpActivityTime()))
                .skuId(generalCpQueryDTO.getSkuId())
                .shopId(generalCpQueryDTO.getQueryShopId())
                .cpStatusList(cpStatusList)
                .build();
        return generalCpQueryCommand;
    }
}
