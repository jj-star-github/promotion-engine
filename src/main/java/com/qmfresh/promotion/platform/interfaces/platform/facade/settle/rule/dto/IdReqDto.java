package com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "IdReqDto", description = "根据id查询")
public class IdReqDto implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("id")
    private Long id;
}
