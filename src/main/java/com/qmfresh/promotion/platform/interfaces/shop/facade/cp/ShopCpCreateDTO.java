package com.qmfresh.promotion.platform.interfaces.shop.facade.cp;

import com.qmfresh.promotion.platform.interfaces.common.AppBaseDTO;
import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 门店改价活动对象
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("门店改价创建对象")
public class ShopCpCreateDTO extends AppBaseDTO {
    @ApiModelProperty("商品id")
    private Integer skuId;
    @ApiModelProperty("商品目标价")
    private BigDecimal rulePrice;
    @ApiModelProperty("计划活动开始时间,毫秒")
    private Long planStartActivityTime;
    @ApiModelProperty("计划活动结束时间,毫秒")
    private Long planEndActivityTime;
    @ApiModelProperty("计划营销费用")
    private BigDecimal planMarketingFee;
    @ApiModelProperty("门店id")
    private Integer shopId;

}
