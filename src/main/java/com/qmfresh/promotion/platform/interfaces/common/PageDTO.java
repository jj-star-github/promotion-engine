package com.qmfresh.promotion.platform.interfaces.common;

import com.qmfresh.promotion.platform.domain.shared.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("分页对象")
public class PageDTO<T> {

    @ApiModelProperty("页码")
    private Integer pageNum;
    @ApiModelProperty("页大小")
    private Integer pageSize;
    @ApiModelProperty("记录列表")
    private List<T> records;
    @ApiModelProperty("总数")
    private Integer totalCount;


    /**
     * 转换分页对象
     *
     * @param pageNum  页码
     * @param pageSize 页大小
     * @param page     原分页对象
     * @param convert  转换器
     * @param <S>      sourceType
     * @param <T>      targetType
     * @return PageDTO对象
     */
    public static <S, T> PageDTO<T> convert(Integer pageNum, Integer pageSize, Page<S> page, Function<S, T> convert) {
        PageDTO<T> pageDTO = new PageDTO<>();
        pageDTO.pageNum = pageNum;
        pageDTO.pageSize = pageSize;
        pageDTO.totalCount = page.getTotalCount().intValue();

        if (CollectionUtils.isEmpty(page.getRecords())) {
            pageDTO.setRecords(new ArrayList<>());
        } else {
            pageDTO.setRecords(page.getRecords().stream().map(convert).collect(Collectors.toList()));
        }
        return pageDTO;
    }
}
