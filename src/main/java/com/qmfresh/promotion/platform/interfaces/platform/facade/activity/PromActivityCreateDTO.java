package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "PromActivityCreateDTO", description = "查询活动信息")
public class PromActivityCreateDTO extends BaseDTO {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("活动ID")
    private Long  id;
    @ApiModelProperty("活动名称")
    private String name;
    @ApiModelProperty("活动类型:1.满赠")
    private Integer activityType;
    @ApiModelProperty("活动规则类型：{activityType=1,1.每满，2.阶梯}")
    private Integer ruleType;
    @ApiModelProperty("活动开始时间")
    private Long beginTime;
    @ApiModelProperty("活动结束时间")
    private Long endTime;
    @ApiModelProperty("目标人群:1.全部,2.会员,3.非会员")
    private Integer userType;
    @ApiModelProperty("渠道:1.全渠道,2.线上,3.线下")
    private Integer activityChannel;

    @ApiModelProperty("目标门店类型,1:全部,2.大区,3.片区,4.门店")
    private Integer shopType;
    @ApiModelProperty("目标门店, shopType=1 不传")
    private List<PromActivityTargetShopDTO> shopList;
    @ApiModelProperty("目标商品类型,1:全品,2:Class1,3:class2,4:sku")
    private Integer skuType;
    @ApiModelProperty("目标商品,skuType=1 不传")
    private List<PromActivityTargetSkuDTO> skuList;
    @ApiModelProperty("奖品信息")
    private List<PromActivityConditionDTO> conditionList;

    @ApiModelProperty("活动描述")
    private String remark;

    public PromActivityCreateDTO() {}

}

