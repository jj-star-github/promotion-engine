package com.qmfresh.promotion.platform.interfaces.shop.facade.cp;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * todo 单位
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Getter
@ApiModel("门店改价")
public class ShopCpInfoDTO implements Serializable {

    private static final String STATUS_CREATE = "待生效";
    private static final String STATUS_RUNNING = "进行中";
    private static final String STATUS_CLOSED = "已关闭";


    @ApiModelProperty("改价活动id")
    private Long activityId;
    @ApiModelProperty("改价明细id")
    private Long activityCpId;
    @ApiModelProperty("门店id")
    private Integer shopId;
    @ApiModelProperty("商品id")
    private Integer skuId;
    @ApiModelProperty("商品名称")
    private String skuName;
    @ApiModelProperty("商品图像")
    private String skuImgUrl;
    @ApiModelProperty("商品格式")
    private String skuFormat;
    @ApiModelProperty("改价活动生效时间起始")
    private Long startActivityTime;
    @ApiModelProperty("改价活动生效时间结束")
    private Long endActivityTime;
    @ApiModelProperty("创建改价活动时间")
    private Long createCpActivityTime;
    @ApiModelProperty("原价（最终建议零售价）")
    private BigDecimal originPrice;
    @ApiModelProperty("改价后的价格（目标价）")
    private BigDecimal rulePrice;
    @ApiModelProperty("营销费用差值")
    private BigDecimal marketingDiffPrice;
    @ApiModelProperty("状态描述")
    private String statusDesc;
    @ApiModelProperty("商品单位")
    private String unitFormat;

    /**
     * 创建门店改价信息
     *
     * @param shopCpActivity 门店改价活动
     * @return 门店改价信息
     */
    public static ShopCpInfoDTO create(ShopCpActivity shopCpActivity) {
        ShopCpInfoDTO shopCpInfoDTO = new ShopCpInfoDTO();
        shopCpInfoDTO.activityCpId = shopCpActivity.getActivityCpId();
        shopCpInfoDTO.activityId = shopCpActivity.getActivityId();
        shopCpInfoDTO.shopId = shopCpActivity.getShop().getShopId();
        shopCpInfoDTO.skuId = shopCpActivity.getSku().getSkuId();
        shopCpInfoDTO.skuName = shopCpActivity.getSku().getSkuName();
        shopCpInfoDTO.skuImgUrl = shopCpActivity.getSku().getSkuImage();
        shopCpInfoDTO.skuFormat = shopCpActivity.getSku().getSkuFormat();
        shopCpInfoDTO.startActivityTime = shopCpActivity.getPlanActivityStartTime().getTime();
        shopCpInfoDTO.endActivityTime = shopCpActivity.getPlanActivityEndTime().getTime();
        shopCpInfoDTO.createCpActivityTime = shopCpActivity.getCreateTime().getTime();
        shopCpInfoDTO.rulePrice = shopCpActivity.getRulePrice() != null ? shopCpActivity.getRulePrice().setScale(2, RoundingMode.HALF_UP) : BigDecimal.ZERO;
        shopCpInfoDTO.marketingDiffPrice = shopCpActivity.getMarketingDiffFee() != null ? shopCpActivity.getMarketingDiffFee().setScale(2, RoundingMode.HALF_UP) : BigDecimal.ZERO;


        shopCpInfoDTO.statusDesc = statusDesc(shopCpActivity.getCpStatus());
        shopCpInfoDTO.originPrice = (shopCpActivity.getOriginalPrice() != null) ? shopCpActivity.getOriginalPrice().setScale(2, RoundingMode.HALF_UP) : BigDecimal.ZERO;
        shopCpInfoDTO.unitFormat = shopCpActivity.getSku().getUnitFormat();
        return shopCpInfoDTO;
    }

    private static String statusDesc(CpStatus cpStatus) {
        if (CpStatus.CREATE.equals(cpStatus)) {
            return STATUS_CREATE;
        }
        if (CpStatus.RUNNING.equals(cpStatus)) {
            return STATUS_RUNNING;
        }
        return STATUS_CLOSED;
    }
}
