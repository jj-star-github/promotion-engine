package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import java.math.BigDecimal;

/**
 *运营经费统计
 */
public class FundsApplyCountDTO {
    //数量
    private Integer num;
    /**
     * 金额
     */
    private BigDecimal money;

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
}
