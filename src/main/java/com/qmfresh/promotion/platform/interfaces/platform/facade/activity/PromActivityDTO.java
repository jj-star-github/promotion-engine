package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.PromActivityPO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel(value = "PromActivityDTO", description = "活动信息")
public class PromActivityDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("活动ID")
    private Long id;
    @ApiModelProperty("活动名称")
    private String name;
    @ApiModelProperty("活动类型:1.满赠")
    private Integer activityType;
    @ApiModelProperty("活动规则类型：{activityType=1,1.每满，2.阶梯}")
    private Integer ruleType;
    @ApiModelProperty("活动开始时间")
    private Date beginTime;
    @ApiModelProperty("活动结束时间")
    private Date endTime;
    @ApiModelProperty("活动状态:1.待进行,3.进行中,4.已取消,6=结束中,7=已关闭,8=已结束")
    private Integer status;
    @ApiModelProperty("目标人群:1.全部,2.会员,3.非会员")
    private Integer userType;
    @ApiModelProperty("渠道:1.全渠道,2.线上,3.线下")
    private Integer channel;
    @ApiModelProperty("目标门店,1:全部,2.大区,3.片区,4.门店")
    private Integer shopType;
    @ApiModelProperty("目标品,1:全品,2:Class1,3:class2,4:sku")
    private Integer skuType;
    @ApiModelProperty("最近操作人名称")
    private String lastOperatorName;
    @ApiModelProperty("最后修改时间")
    private Date gmtModified;


    public PromActivityDTO() {}

    public PromActivityDTO(PromActivityPO po) {
        this.setId(po.getId());
        this.setActivityType(po.getActivityType());
        this.setRuleType(po.getRuleType());
        this.setName(po.getName());
        this.setBeginTime(po.getBeginTime());
        this.setEndTime(po.getEndTime());
        this.setChannel(po.getChannel());
        this.setStatus(po.getStatus());
        this.setUserType(po.getUserType());
        this.setShopType(po.getShopType());
        this.setSkuType(po.getSkuType());
        this.setLastOperatorName(po.getLastOperatorName());
        this.setGmtModified(po.getGmtModified());
    }

}

