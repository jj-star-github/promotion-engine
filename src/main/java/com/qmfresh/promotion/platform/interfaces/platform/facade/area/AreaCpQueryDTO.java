package com.qmfresh.promotion.platform.interfaces.platform.facade.area;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.command.AreaCpQueryCommand;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("大区查询")
public class AreaCpQueryDTO extends BaseDTO {

    @ApiModelProperty("页码")
    private Integer pageNum;
    @ApiModelProperty("页大小")
    private Integer pageSize;
    @ApiModelProperty("活动id")
    private Long activityId;
    @ApiModelProperty("活动名称关键词")
    private String activityNameKey;
    @ApiModelProperty("门店id")
    private Integer currentShopId;
    @ApiModelProperty("商品id")
    private Integer skuId;
    @ApiModelProperty("0=全部 1=待生效 2=进行中 3=已失效")
    private Integer status;
    @ApiModelProperty("从起始时间：毫秒")
    private Long fromCpActivityStartTime;
    @ApiModelProperty("到起始时间：毫秒")
    private Long toCpActivityStartTime;

    public static AreaCpQueryCommand createCommand(AreaCpQueryDTO areaCpQueryDTO) {

        //查询状态的转化
        List<Integer> statusList = new ArrayList<>();
        if (areaCpQueryDTO.getStatus() != null) {
            if (1 == areaCpQueryDTO.getStatus()) {
                statusList.add(CpStatus.CREATE.getCode());
            }
            if (2 == areaCpQueryDTO.getStatus()) {
                statusList.add(CpStatus.RUNNING.getCode());
            }
            if (3 == areaCpQueryDTO.getStatus()) {
                statusList.add(CpStatus.CLOSED.getCode());
                statusList.add(CpStatus.FINISHED.getCode());
            }
        }
        Date fromDate = null;
        if (areaCpQueryDTO.getFromCpActivityStartTime() != null) {
            fromDate = new Date(areaCpQueryDTO.getFromCpActivityStartTime());
        }
        Date toDate = null;
        if (areaCpQueryDTO.getToCpActivityStartTime() != null) {
            toDate = new Date(areaCpQueryDTO.getToCpActivityStartTime());
        }


        AreaCpQueryCommand areaCpQueryCommand = AreaCpQueryCommand.create(
                areaCpQueryDTO.getPageNum(),
                areaCpQueryDTO.getPageSize(),
                areaCpQueryDTO.getActivityId(),
                areaCpQueryDTO.getActivityNameKey(),
                areaCpQueryDTO.getCurrentShopId(),
                areaCpQueryDTO.getSkuId(),
                statusList,
                fromDate,
                toDate
        );
        return areaCpQueryCommand;
    }
}
