package com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.platform.domain.shared.BizCode;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopPromotion;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopPromotionRule;
import com.qmfresh.promotion.platform.domain.model.settle.rule.ShopRuleManager;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/***
 * @Author zbr
 * @Description 门店营销费用规则
 * @Date 14:43 2020/8/17
 */
@RestController
@RequestMapping(value = "/shopRule")
@Slf4j
@Api(description = "门店营销费用规则")
public class ShopRuleController {

    @Resource
    private ShopRuleManager shopRuleManager;

    @ApiOperation(value = "创建门店规则", notes = "创建门店规则")
    @RequestMapping(value = "/createShopRule", method = RequestMethod.POST)
    public ServiceResult createShopRule(@RequestBody ShopRuleReqDTO param){
        log.info("into method createShopRule,入参:" + JSON.toJSONString(param));
        return ServiceResultUtil.resultSuccess(shopRuleManager.createShopRule(param));
    }

    @ApiOperation(value = "查询门店规则列表", notes = "查询门店规则列表")
    @RequestMapping(value = "/queryRuleInfoList", method = RequestMethod.POST)
    public ServiceResult<List<ShopPromotion>> queryRuleInfoList(@RequestBody QueryRuleInfoReqDTO param){
        log.info("into method queryRuleInfoList,入参:" + JSON.toJSONString(param));
        return ServiceResultUtil.resultSuccess(shopRuleManager.queryRuleInfoList(param));
    }

    @ApiOperation(value = "查询门店规则明细", notes = "查询门店规则明细")
    @RequestMapping(value = "/queryRuleDetailInfo", method = RequestMethod.POST)
    public ServiceResult<ShopPromotionRule> queryRuleDetailInfo(@RequestBody IdReqDto id){
        log.info("into method queryRuleDetailInfo,入参:" + JSON.toJSONString(id));
        return ServiceResultUtil.resultSuccess(shopRuleManager.queryRuleDetailInfo(id));
    }

    @ApiOperation(value = "修改门店规则", notes = "修改门店规则")
    @RequestMapping(value = "/modifyShopRule", method = RequestMethod.POST)
    public ServiceResult modifyShopRule(@RequestBody UpdateShopRuleReqDTO param){
        log.info("into method modifyShopRule,入参:" + JSON.toJSONString(param));
        return ServiceResultUtil.resultSuccess(shopRuleManager.modifyShopRule(param));
    }

    @ApiOperation(value = "根据shopId查询门店规则", notes = "根据shopId查询门店规则")
    @RequestMapping(value = "/queryRuleByShopId", method = RequestMethod.POST)
    public ServiceResultDTO<ShopWeekDTO> queryRuleByShopId(@RequestBody ShopIdDTO shopId){
        log.info("into method queryRuleByShopId,入参:" + JSON.toJSONString(shopId));
        try {
            return ServiceResultDTO.success(shopRuleManager.queryRuleByShopId(shopId));
        }catch (BusinessException be){
            log.error("查询门店规则 接口异常 shopId={} e={}", shopId, LogExceptionStackTrace.errorStackTrace(be));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(),be.getErrorMsg());
        }catch (Exception e) {
            log.error("查询门店规则 接口异常 shopId={} e={}", shopId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }
}
