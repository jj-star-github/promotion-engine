package com.qmfresh.promotion.platform.interfaces.shop.facade.cp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel("门店改价信息")
public class ShopPriceQueryDTO implements Serializable {
    @ApiModelProperty("门店id")
    private Integer shopId;
    @ApiModelProperty("商品id")
    private Integer skuId;
    @ApiModelProperty("促销价")
    private BigDecimal promotionPrice;
    @ApiModelProperty("开始时间")
    private Long startTime;
    @ApiModelProperty("结束时间")
    private Long endTime;
}
