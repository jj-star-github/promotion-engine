package com.qmfresh.promotion.platform.interfaces.platform.facade.area;

import com.qmfresh.promotion.platform.domain.model.activity.Subject;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.command.AreaCpCloseCommand;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@ApiModel("大区改价关闭对象")
@Data
@Slf4j
public class AreaCpCloseDTO extends BaseDTO {


    private Long activityId;
    private String remark;


    public static AreaCpCloseCommand createCommand(AreaCpCloseDTO areaCpCloseDTO) {

        if (areaCpCloseDTO.getActivityId() == null) {
            throw new BusinessException("必须设置活动id");
        }

        Operator operator = Operator.create(areaCpCloseDTO.getOperatorId(),
                areaCpCloseDTO.getOperatorName(), Subject.createAreaEmpty());


        AreaCpCloseCommand areaCpCloseCommand =
                AreaCpCloseCommand.create(operator, areaCpCloseDTO.getActivityId(), areaCpCloseDTO.getRemark());
        return areaCpCloseCommand;
    }

}
