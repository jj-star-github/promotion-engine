package com.qmfresh.promotion.platform.interfaces.common.util;


import io.netty.util.concurrent.DefaultThreadFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
public class ExecutorsUtil {

    public static ExecutorService generalCpExecutorPool = Executors
            .newFixedThreadPool(2,
                    new DefaultThreadFactory(""));

    public static ExecutorService areaCpExecutorPool =
            Executors.newFixedThreadPool(2,
                   new DefaultThreadFactory("") );
}
