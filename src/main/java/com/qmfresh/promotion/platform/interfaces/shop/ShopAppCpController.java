package com.qmfresh.promotion.platform.interfaces.shop;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.sku.ShopSkuQuery;
import com.qmfresh.promotion.platform.domain.model.activity.AdminUserManager;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.promotion.platform.interfaces.common.BizCode;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.general.GeneralCpListItemDTO;
import com.qmfresh.promotion.platform.interfaces.shop.facade.ShopCpFacadeService;
import com.qmfresh.promotion.platform.interfaces.shop.facade.cp.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Api(tags = {"门店改价接口"})
@RestController
@RequestMapping("/shop/cp")
@Slf4j
public class ShopAppCpController {

    @Resource
    private ShopCpFacadeService shopCpFacadeService;
    @Resource
    private AdminUserManager adminUserManager;


    @ApiOperation("门店改价创建")
    @PostMapping("/shop/create")
    public ServiceResultDTO<ShopCpInfoDTO> createShopCpActivity(
            @RequestHeader("shopId") Integer shopId,
            @RequestHeader("userId") Integer userId,
            @RequestBody ShopCpCreateDTO shopCpCreateDTO) {
        Assert.notNull(shopCpCreateDTO.getPlanMarketingFee(), "请先确认输入金额");
        try {
            shopCpCreateDTO.setShopId(shopId);
            shopCpCreateDTO.setOperatorId(userId);
            shopCpCreateDTO.setOperatorName(adminUserManager.adminUser(userId).getRealName());
            //todo try catch
            ShopCpInfoDTO shopCpInfoDTO = shopCpFacadeService.createShopCpActivity(shopCpCreateDTO);
            return ServiceResultDTO.success(shopCpInfoDTO);

        } catch (Exception e) {
            log.warn("门店改价创建", e);
            if (e instanceof BusinessException) {
                return ServiceResultDTO.fail(BizCode.FAILED.getCode(), ((BusinessException) e).getErrorMsg());
            }
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(), "系统异常");

        }
    }

    @ApiOperation("门店改价分页查询")
    @PostMapping("/shop/page")
    public ServiceResultDTO<PageDTO<ShopCpInfoDTO>> pageShopCpActivity(
            @RequestHeader("shopId") Integer shopId,
            @RequestHeader("userId") Integer userId,
            @RequestBody ShopCpQueryDTO shopCpQueryDTO) {
        //todo try catch
        try {
            shopCpQueryDTO.setOperatorId(userId);
            shopCpQueryDTO.setShopId(shopId);
            shopCpQueryDTO.setOperatorName(adminUserManager.adminUser(userId).getRealName());
            shopCpQueryDTO.setSkuName(shopCpQueryDTO.getSkuKey());
            PageDTO<ShopCpInfoDTO> shopCpInfoDTOPageDTO = shopCpFacadeService.pageShopCpActivity(shopCpQueryDTO);
            return ServiceResultDTO.success(shopCpInfoDTOPageDTO);
        } catch (Exception e) {
            log.warn("分页查询异常:",e);
            if (e instanceof BusinessException) {
                return ServiceResultDTO.fail(BizCode.FAILED.getCode(), ((BusinessException) e).getErrorMsg());
            }
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(), "系统异常");

        }

    }

    @ApiOperation("门店已改价分页查询")
    @PostMapping("/shop/changed/page")
    public ServiceResultDTO<PageDTO<ShopCpItemChangedDTO>> pageShopCpActivityChanged(
            @RequestHeader("shopId") Integer shopId,
            @RequestHeader("userId") Integer userId,
            @RequestBody ShopCpQueryDTO shopCpQueryDTO) {
        //todo try catch
        shopCpQueryDTO.setOperatorId(userId);
        shopCpQueryDTO.setShopId(shopId);
        shopCpQueryDTO.setOperatorName(adminUserManager.adminUser(userId).getRealName());
        PageDTO<ShopCpItemChangedDTO> shopCpInfoDTOPageDTO = shopCpFacadeService.pageShopCpActivityChanged(shopCpQueryDTO);
        return ServiceResultDTO.success(shopCpInfoDTOPageDTO);
    }

    @ApiOperation("门店改价关闭")
    @PostMapping("/shop/close")
    public ServiceResultDTO<Boolean> closeShopCpActivity(
            @RequestHeader("shopId") Integer shopId,
            @RequestHeader("userId") Integer userId,
            @RequestBody ShopCpCloseDTO shopCpCloseDTO) {
        try {
            shopCpCloseDTO.setShopId(shopId);
            shopCpCloseDTO.setOperatorId(userId);
            shopCpCloseDTO.setOperatorName(adminUserManager.adminUser(userId).getRealName());
            shopCpFacadeService.closeShopCpActivity(shopCpCloseDTO);
            return ServiceResultDTO.success(true);

        } catch (Exception e) {
            log.warn("门店改价关闭 ",e);
            if (e instanceof BusinessException) {
                return ServiceResultDTO.fail(BizCode.FAILED.getCode(), ((BusinessException) e).getErrorMsg());
            }
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(), "系统异常");

        }
    }

    @ApiOperation("当前门店的总部改价")
    @PostMapping("/general/list")
    public ServiceResultDTO<PageDTO<GeneralCpListItemDTO>> listShopGeneralCpActivities(
            @RequestHeader("shopId") Integer shopId,
            @RequestHeader("userId") Integer userId,
            @RequestBody ShopCpQueryDTO shopCpQueryDTO) {
        shopCpQueryDTO.setShopId(shopId);
        shopCpQueryDTO.setOperatorId(userId);
        shopCpQueryDTO.setOperatorName(adminUserManager.adminUser(userId).getRealName());
        PageDTO<GeneralCpListItemDTO> pageDTO = shopCpFacadeService.listShopGeneralCpActivities(shopCpQueryDTO);
        return ServiceResultDTO.success(pageDTO);
    }

    @ApiOperation("查询当前门店sku的总部或大区促销价")
    @PostMapping("/shop/sku/price")
    public ServiceResultDTO<ShopSkuPrice> shopSkuPrice(@RequestBody ShopSkuQueryDTO shopSkuQueryDTO){
        log.info("into method shopSkuPrice,入参:" + JSON.toJSONString(shopSkuQueryDTO));
        try{
            return ServiceResultDTO.success(shopCpFacadeService.shopSkuPrice(shopSkuQueryDTO));
        }catch (BusinessException be){
            log.warn("查询异常 ，shopId={}, skuId={}, errorMsg={}", shopSkuQueryDTO.getShopId(),shopSkuQueryDTO.getSkuId(), be.getErrorMsg());
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(),be.getErrorMsg());
        }catch (Exception e){
            log.error("查询总部或大区促销价异常：shopId={}, skuId={}, ex={}",shopSkuQueryDTO.getShopId(),shopSkuQueryDTO.getSkuId(),LogExceptionStackTrace.errorStackTrace(e));
        }
        return ServiceResultDTO.fail(BizCode.FAILED.getCode(),"系统异常");
    }

    @ApiOperation("根据当前门店sku的价格计算返回差价和营销费")
    @PostMapping("/shop/sku/diff")
    public ServiceResultDTO<ShopDiffPrice> shopDiffPrice(@RequestBody ShopPriceQueryDTO shopPriceQueryDTO){
        log.info("into method shopDiffPrice,入参:" + JSON.toJSONString(shopPriceQueryDTO));
        try{
            return ServiceResultDTO.success(shopCpFacadeService.shopDiffPrice(shopPriceQueryDTO));
        }catch (BusinessException be){
            log.warn("计算异常 ，shopId={}, skuId={}, errorMsg={}", shopPriceQueryDTO.getShopId(),shopPriceQueryDTO.getSkuId(), be.getErrorMsg());
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(),be.getErrorMsg());
        }catch (Exception e){
            log.error("价格计算异常：shopId={}, skuId={}, ex={}",shopPriceQueryDTO.getShopId(),shopPriceQueryDTO.getSkuId(),LogExceptionStackTrace.errorStackTrace(e));
        }
        return ServiceResultDTO.fail(BizCode.FAILED.getCode(),"系统异常");
    }
}
