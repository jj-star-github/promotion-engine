package com.qmfresh.promotion.platform.interfaces.shop.facade.funds;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data

public class WeekClear implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    private BigDecimal weekAmount;
    private Long timeStart;
    private Long timeEnd;
    private BigDecimal shopWeekClearPercent;
    private BigDecimal areaWeekClearPercent;
    private BigDecimal minPromPrice;
}
