package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel(value = "ShopFundsNewChangeNoteDTO", description = "费用收入流水")
public class ShopFundsNewChangeNoteDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("订单号")
    private String orderCode;
    @ApiModelProperty("订单金额")
    private BigDecimal realPay;
    @ApiModelProperty("营销收入")
    private BigDecimal fundsAmount;
    @ApiModelProperty("总部营销收入")
    private BigDecimal zbFundsAmount;
    @ApiModelProperty("门店营销收入")
    private BigDecimal shopFundsAmount;
    @ApiModelProperty("大区营销收入")
    private BigDecimal areaFundsAmount;
    @ApiModelProperty("时间")
    private Date gmtCreate;

}
