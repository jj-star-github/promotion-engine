package com.qmfresh.promotion.platform.interfaces.platform;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.service.DLock;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.interfaces.common.BizCode;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.AreaCpFacadeService;
import com.qmfresh.promotion.platform.interfaces.platform.facade.CpCreateResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.CpSkuDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.ShopCpResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.area.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static com.qmfresh.promotion.platform.interfaces.platform.GeneralCpController.generalCpCreateKey;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Api(tags = "大区活动改价接口")
@RestController
@RequestMapping("/platform/cp")
@Slf4j
public class AreaCpController {

    @Resource
    private AreaCpFacadeService areaCpFacadeService;

    private static final AtomicLong atomicLong = new AtomicLong();
    @Resource
    private DLock dLock;

    private static ExecutorService executorService = Executors
            .newFixedThreadPool(5, r -> new Thread(r, generalCpCreateKey("大区改价创建线程", atomicLong.getAndDecrement())));


    @ApiOperation("大区活动创建")
    @PostMapping("/area/cp/create")
    public ServiceResultDTO<CpCreateResultDTO> areaCreate(@RequestBody AreaCpCreateDTO areaCpCreateDTO) {

        log.info("大区改建创建接口参数：{}", JSON.toJSONString(areaCpCreateDTO));
        boolean result = dLock.tryLock(DLock.generalCpCreateKey(areaCpCreateDTO.getOperatorId().longValue()), 15000L);
        if (!result) {
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(), "用户正在创建大区改价");
        }
        try {
            Future<CpCreateResultDTO> future = executorService.submit(() -> {
                CpCreateResultDTO generalCpActivity = areaCpFacadeService.create(areaCpCreateDTO);
                return generalCpActivity;
            });
            future.get(3000, TimeUnit.SECONDS);
            if (future.isDone()) {
                return ServiceResultDTO.success(future.get());
            }
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(), "正在创建中，超过三秒等待了");
        } catch (Exception e) {
            String errMsg = "";
            if (e.getCause() instanceof BusinessException) {
                errMsg = ((BusinessException) e.getCause()).getErrorMsg();
            }
            log.warn("创建总部改价异常： {}", errMsg, e);
            return ServiceResultDTO.fail(BizCode.FAILED.getCode(), errMsg);
        }
    }

    @ApiOperation("大区改价活动分页查询")
    @PostMapping("/area/cp/page")
    public ServiceResultDTO<PageDTO<AreaCpListItemDTO>> pageGeneralCpActivity(@RequestBody AreaCpQueryDTO areaCpQueryDTO) {
        PageDTO<AreaCpListItemDTO> pageGeneralCpActivity = areaCpFacadeService.pageAreaCps(areaCpQueryDTO);
        return ServiceResultDTO.success(pageGeneralCpActivity);
    }

    @ApiOperation("大区改价编辑商品列表")
    @PostMapping("/area/cp/sku/page")
    public ServiceResultDTO<PageDTO<CpSkuDTO>> pageGeneralCpSkus(@RequestBody AreaCpSkuQueryDTO areaCpSkuQueryDTO) {
        PageDTO<CpSkuDTO> pageDTO = areaCpFacadeService.pageAreaCpSkus(areaCpSkuQueryDTO);
        return ServiceResultDTO.success(pageDTO);
    }

    @ApiOperation("大区改价编辑商品")
    @PostMapping("/area/cp/sku/edit")
    public ServiceResultDTO<Boolean> editSkuOfGeneralCpActivity(@RequestBody AreaCpEditSkuDTO skuDTO) {
        areaCpFacadeService.editSkuOfAreaCp(skuDTO);
        return ServiceResultDTO.success(true);
    }

    @ApiOperation("大区分页查询门店")
    @PostMapping("/area/cp/shop/page")
    public ServiceResultDTO<PageDTO<ShopCpResultDTO>> pageShopOfAreaCpActivity(@RequestBody AreaCpQueryDTO areaCpQueryDTO) {
        PageDTO<ShopCpResultDTO> page = areaCpFacadeService.pageShopOfAreaCpActivity(areaCpQueryDTO);
        return ServiceResultDTO.success(page);
    }


    @ApiOperation("关闭大区改价")
    @PostMapping("/area/cp/close")
    public ServiceResultDTO<Boolean> closeGeneralCpActivity(@RequestBody AreaCpCloseDTO areaCpCloseDTO) {
        log.info("关闭大区改价：param={}", JSON.toJSONString(areaCpCloseDTO));
        areaCpFacadeService.closeAreaCpActivity(areaCpCloseDTO);
        return ServiceResultDTO.success(true);
    }


}
