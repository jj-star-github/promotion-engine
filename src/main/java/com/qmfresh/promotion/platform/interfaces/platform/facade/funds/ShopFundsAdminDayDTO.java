package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "ShopFundsAdminDayDTO", description = "费用日统计明细")
public class ShopFundsAdminDayDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("账户类型 1.总部，2.大区，3.片区，4.门店")
    private Integer accountType;
    @ApiModelProperty("账户ID 明细接口中使用")
    private Long accountId;
    @ApiModelProperty("门店")
    private String shopName;
    @ApiModelProperty("日期")
    private String createDay;
    @ApiModelProperty("当日初始费用")
    private BigDecimal initialAmount= new BigDecimal("0");
    @ApiModelProperty("当日锁定")
    private BigDecimal lockAmount=new BigDecimal("0");
    @ApiModelProperty("当日使用")
    private BigDecimal useAmount;
    @ApiModelProperty("当日增加")
    private BigDecimal newAmount;
    @ApiModelProperty("总费用")
    private BigDecimal amount;
    @ApiModelProperty("门店id")
    private Integer shopId;

}
