package com.qmfresh.promotion.platform.interfaces.platform;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.common.utils.TraceIdUtils;
import com.qmfresh.promotion.platform.application.PlatformPromActivityService;
import com.qmfresh.promotion.platform.domain.model.promactivity.enums.ActivityCacheKey;
import com.qmfresh.promotion.platform.domain.service.DLock;
import com.qmfresh.promotion.platform.domain.shared.BizCode;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/platform/prom/activity")
@Slf4j
@Api(tags = {"管理后台活动接口"})
public class PlatformPromActivityController {

    @Resource
    private PlatformPromActivityService platformPromActivityService;
    @Resource
    private DLock dLock;

    @ApiOperation(value = "活动查询", notes = "活动查询")
    @RequestMapping(value = "/query", method = RequestMethod.POST)
    public ServiceResultDTO<PageDTO<PromActivityDTO>> query(@Valid @RequestBody PromActivityQueryDTO param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("活动查询 请求 traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            return ServiceResultDTO.success(platformPromActivityService.query(param));
        } catch (Exception e) {
            log.error("费用日统计 接口异常 traceId={} e={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "创建活动", notes = "创建活动")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ServiceResultDTO<Boolean> create(@Valid @RequestBody PromActivityCreateDTO param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("创建活动-请求 traceId = {}, param={}", traceId, JSON.toJSONString(param));
        try {
            //防止重复提交
            boolean result = dLock.tryLock(ActivityCacheKey.CREATE_LOCK.key(param.getOperatorId().longValue()), 15000L);
            if (!result) {
                return ServiceResultDTO.fail(com.qmfresh.promotion.platform.interfaces.common.BizCode.FAILED.getCode(), "请勿重复提交");
            }
            //参数验证
            if (StringUtils.isBlank(param.getName()) || null == param.getActivityType()
                    || null == param.getBeginTime() || null == param.getEndTime() || CollectionUtils.isEmpty(param.getConditionList())) {
                return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "请求参数有误");
            }
            platformPromActivityService.create(param, traceId);
            return ServiceResultDTO.success(true);
        } catch (BusinessException be) {
            log.warn("创建活动-业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
            return ServiceResultDTO.fail(BizCode.NETWORK_ERROR.getCode(), be.getErrorMsg());
        } catch (Exception e) {
            log.error("创建活动-接口异常 traceId={} e={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "系统异常");
        }
    }

    @ApiOperation(value = "活动修改", notes = "活动修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ServiceResultDTO<Boolean> update(@Valid @RequestBody PromActivityCreateDTO param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("活动修改-请求 traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            //参数验证
            return ServiceResultDTO.success(platformPromActivityService.update(param, traceId));
        } catch (BusinessException be) {
            log.warn("活动修改-业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
            return ServiceResultDTO.fail(BizCode.NETWORK_ERROR.getCode(), be.getErrorMsg());
        } catch (Exception e) {
            log.error("活动修改-接口异常 traceId={} e={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "活动失效", notes = "活动失效")
    @RequestMapping(value = "/noAvail", method = RequestMethod.POST)
    public ServiceResultDTO<Boolean> noAvail(@Valid @RequestBody PromActivityBaseDTO param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("活动失效 请求 traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            return ServiceResultDTO.success(platformPromActivityService.noAvail(param));
        } catch (Exception e) {
            log.error("活动失效 接口异常 traceId={} e={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "查询门店信息", notes = "查询门店信息")
    @RequestMapping(value = "/queryTargetShop", method = RequestMethod.POST)
    public ServiceResultDTO<List<Integer>> queryTargetShop(@Valid @RequestBody PromActivityBaseDTO param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("查询门店信息 请求 traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            if (null == param.getId()) {
                return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "请选择活动");
            }
            return ServiceResultDTO.success(platformPromActivityService.queryTargetShopId(param.getId()));
        } catch (Exception e) {
            log.error("查询门店信息 接口异常 traceId={} e={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "查询商品信息", notes = "查询商品信息")
    @RequestMapping(value = "/queryTargetSku", method = RequestMethod.POST)
    public ServiceResultDTO<List<Integer>> queryTargetSku(@Valid @RequestBody PromActivityBaseDTO param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("查询门店信息 请求 traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            if (null == param.getId()) {
                return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "请选择活动");
            }
            return ServiceResultDTO.success(platformPromActivityService.queryTargetSkuId(param.getId()));
        } catch (Exception e) {
            log.error("查询门店信息 接口异常 traceId={} e={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "查询奖品信息", notes = "查询奖品信息")
    @RequestMapping(value = "/queryCondition", method = RequestMethod.POST)
    public ServiceResultDTO<List<PromActivityConditionDTO>> queryCondition(@Valid @RequestBody PromActivityBaseDTO param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("查询奖品信息 请求 traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            if (null == param.getId()) {
                return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "请选择活动");
            }
            return ServiceResultDTO.success(platformPromActivityService.queryCondition(param.getId()));
        } catch (Exception e) {
            log.error("查询奖品信息 接口异常 traceId={} e={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "更新奖品信息", notes = "更新奖品信息")
    @RequestMapping(value = "/updateCondition", method = RequestMethod.POST)
    public ServiceResultDTO<Boolean> queryCondition(@Valid @RequestBody PromActivityConditionDTO param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("更新奖品信息 请求 traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            if (null == param.getId() || null == param.getActivityId()) {
                return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "请选择");
            }
            return ServiceResultDTO.success(platformPromActivityService.updateCondition(param, traceId));
        } catch (Exception e) {
            log.error("更新奖品信息 接口异常 traceId={} e={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

}
