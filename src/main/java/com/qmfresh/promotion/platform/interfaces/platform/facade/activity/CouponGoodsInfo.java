package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import java.math.BigDecimal;

/**
 *  优惠券返回可用
 **/
public class CouponGoodsInfo {

    /**
     * 商品id
     */
    private Integer skuId;

    /**
     * 商品单价
     */
    private BigDecimal price;

    /**
     * 商品数量
     */
    private BigDecimal amount;

    /**
     * 券所属一级分类
     */
    private Integer class1Id;

    /**
     * 券所属二级分类
     */
    private Integer class2Id;

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getClass1Id() {
        return class1Id;
    }

    public void setClass1Id(Integer class1Id) {
        this.class1Id = class1Id;
    }

    public Integer getClass2Id() {
        return class2Id;
    }

    public void setClass2Id(Integer class2Id) {
        this.class2Id = class2Id;
    }
}
