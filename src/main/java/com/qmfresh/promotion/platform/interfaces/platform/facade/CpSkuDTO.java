package com.qmfresh.promotion.platform.interfaces.platform.facade;

import com.qmfresh.promotion.platform.domain.model.activity.cp.SkuCpItem;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("总部改价商品")
public class CpSkuDTO implements Serializable {
    @ApiModelProperty("一级类目名称")
    private String class1Name;
    @ApiModelProperty("二级类目名称")
    private String class2Name;
    @ApiModelProperty("三级类目名称")
    private String class3Name;
    @ApiModelProperty("商品id")
    private Integer skuId;
    @ApiModelProperty("营销价格")
    private BigDecimal rulePrice;
    @ApiModelProperty("商品名称")
    private String skuName;
    @ApiModelProperty("营销活动id")
    private Long activityId;


    public static CpSkuDTO convert(SkuCpItem skuCpItem) {
        CpSkuDTO cpSkuDTO = new CpSkuDTO();

        cpSkuDTO.setClass1Name(skuCpItem.getClass1Name());
        cpSkuDTO.setClass2Name(skuCpItem.getClass2Name());
        cpSkuDTO.setClass3Name(skuCpItem.getClass3Name());
        cpSkuDTO.setSkuId(skuCpItem.getSkuId());
        cpSkuDTO.setSkuName(skuCpItem.getSkuName());
        cpSkuDTO.setRulePrice(skuCpItem.getRulePrice());
        cpSkuDTO.setActivityId(skuCpItem.getActivityId());

        return cpSkuDTO;
    }
}
