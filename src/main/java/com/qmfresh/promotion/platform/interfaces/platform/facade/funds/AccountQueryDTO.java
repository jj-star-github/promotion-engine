package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "AccountQueryDTO", description = "账户查询")
public class AccountQueryDTO extends BaseDTO {

    @ApiModelProperty("账户类型")
    private Integer shopType;
    @ApiModelProperty("账户名称")
    private String shopName;
    @ApiModelProperty("门店id")
    private Integer queryShopId;
    @ApiModelProperty("大区名称")
    private String areaName;
    @ApiModelProperty("页码 默认：1")
    private int pageNum = 1;
    @ApiModelProperty("分页大小 默认：20")
    private int pageSize = 20;

    public Integer getStart() {
        return (this.pageNum-1)*this.pageSize;
    }
}
