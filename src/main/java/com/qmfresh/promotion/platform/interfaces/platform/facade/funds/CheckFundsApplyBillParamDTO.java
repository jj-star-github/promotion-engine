package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 审核营销经费申请单
 */
@Data
@ApiModel("审核营销经费的申请单")
public class CheckFundsApplyBillParamDTO {
    //申请单ID
    @ApiModelProperty("申请单ID")
    private  Long id;
    /**
     * 审核人用户ID
     */
    private Long examineUserId;

    /**
     * 审核人用户名
     */
    private String examineUserName;
    /**
     * 审核人用户ID
     */
    @ApiModelProperty("审核人用户ID")
    private Long userId;

    /**
     * 审核人用户名
     */
    @ApiModelProperty("审核人用户名")
    private String userName;
    /**
     * 审核描述
     */
    @ApiModelProperty("审核描述")
    private String examineRemark;

    /**
     * 审核状态:0.审核中，4.通过，5.驳回
     */
    @ApiModelProperty("审核状态:0.审核中，4.通过，5.驳回")
    private int status;


}
