package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel(value = "ShopFundsPayChangeNoteDTO", description = "费用支出流水")
public class ShopFundsPayChangeNoteDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("订单号")
    private String orderCode;
    @ApiModelProperty("业务类型，3.营销费用支出，5.订单改价")
    private Integer businessType;
    @ApiModelProperty("skuId")
    private Long skuId;
    @ApiModelProperty("商品名称")
    private String skuName;
    @ApiModelProperty("总部营销收入")
    private BigDecimal changeAmount;
    @ApiModelProperty("操作人")
    private String createUserName;
    @ApiModelProperty("时间")
    private Date gmtCreate;
}
