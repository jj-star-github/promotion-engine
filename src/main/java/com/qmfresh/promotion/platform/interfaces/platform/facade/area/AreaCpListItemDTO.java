package com.qmfresh.promotion.platform.interfaces.platform.facade.area;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.AreaCpActivity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@ApiModel("大区改价列表")
@Data
public class AreaCpListItemDTO implements Serializable {

    @ApiModelProperty("改价活动id")
    private Long activityId;
    @ApiModelProperty("改价活动名称")
    private String activityName;
    @ApiModelProperty("活动类型")
    private Integer type;
    @ApiModelProperty("改价类型描述")
    private String typeDesc;
    @ApiModelProperty("账号大区")
    private String accountAreaName;
    @ApiModelProperty("门店数")
    private Integer shopCount;
    @ApiModelProperty("商品数")
    private Integer skuCount;
    @ApiModelProperty("计划活动开始时间")
    private Long planBeginCpActivityTime;
    @ApiModelProperty("计划活动结束时间")
    private Long planEndCpActivityTime;
    @ApiModelProperty("活动状态描述")
    private String statusDesc;
    @ApiModelProperty("计划（锁定）营销费用")
    private BigDecimal planMarketingFee;
    @ApiModelProperty("实际使用营销费用")
    private BigDecimal realMarketingFee;
    @ApiModelProperty("创建时间")
    private Long createTime;
    @ApiModelProperty("最后编辑人")
    private String lastModifyPersonName;

    public static AreaCpListItemDTO convert(AreaCpActivity s) {
        AreaCpListItemDTO areaCpListItemDTO = new AreaCpListItemDTO();
        areaCpListItemDTO.setActivityId(s.getActivityId());
        areaCpListItemDTO.setActivityName(s.getActivityName());
        areaCpListItemDTO.setType(s.getCpBizType().getCode());
        areaCpListItemDTO.setTypeDesc(s.getCpBizType().getMsg());
        areaCpListItemDTO.setShopCount(s.getShopCount());
        areaCpListItemDTO.setSkuCount(s.getSkuCount());
        areaCpListItemDTO.setPlanBeginCpActivityTime(s.getCpTimeCycle().getBeginTime().getTime());
        areaCpListItemDTO.setPlanEndCpActivityTime(s.getCpTimeCycle().getEndTime().getTime());
        areaCpListItemDTO.setStatusDesc(CpStatus.statusDesc(s.getCpStatus()));
        areaCpListItemDTO.setCreateTime(s.getCreateTime().getTime());
        areaCpListItemDTO.setLastModifyPersonName(s.getLastModifiedPerson());
        return areaCpListItemDTO;
    }
}
