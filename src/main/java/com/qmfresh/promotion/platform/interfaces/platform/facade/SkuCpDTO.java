package com.qmfresh.promotion.platform.interfaces.platform.facade;

import com.qmfresh.promotion.platform.domain.model.activity.cp.SkuCpItem;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.collections.CollectionUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("商品改价对象")
public class SkuCpDTO implements Serializable {

    @ApiModelProperty("商品id")
    private Integer skuId;
    @ApiModelProperty("目标营销价格")
    private BigDecimal rulePrice;
    @ApiModelProperty("计划营销费用")
    private BigDecimal planMarketingFee;
    @ApiModelProperty("备注")
    private String remark;

    public static List<SkuCpItem> create(List<SkuCpDTO> skuCpDTOS) {
        if (CollectionUtils.isEmpty(skuCpDTOS)) {
            return new ArrayList<>();
        }
        return skuCpDTOS.stream().map(SkuCpDTO::cpItem).collect(Collectors.toList());
    }

    public static SkuCpItem cpItem(SkuCpDTO skuCpDTO) {
        return SkuCpItem.create(skuCpDTO.getSkuId(), skuCpDTO.getRulePrice(), skuCpDTO.getPlanMarketingFee());
    }

    public static SkuCpDTO create(Integer skuId, BigDecimal rulePrice, BigDecimal planMarketingFee) {
        SkuCpDTO skuCpDTO = new SkuCpDTO();
        skuCpDTO.setSkuId(skuId);
        skuCpDTO.setRulePrice(rulePrice);
        skuCpDTO.setPlanMarketingFee(planMarketingFee);
        return skuCpDTO;
    }
}
