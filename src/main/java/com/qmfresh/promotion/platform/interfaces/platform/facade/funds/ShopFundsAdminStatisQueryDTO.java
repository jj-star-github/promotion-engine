package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "ShopFundsAdminStatisQueryDTO", description = "管理后台查询")
public class ShopFundsAdminStatisQueryDTO extends BaseDTO {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("开始时间:yyyy-mm-dd")
    @NotNull(message = "beginTime不可为空")
    private String beginTime;
    @ApiModelProperty("结束时间:yyyy-mm-dd")
    @NotNull(message = "endTime不可为空")
    private String endTime;
    @ApiModelProperty("账号类型 1:总部 2：大区 4：门店")
    private Integer accountType;
    @ApiModelProperty("账户ID")
    private Long accountId;
    @ApiModelProperty("页码 默认：1")
    private int pageNum = 1;
    @ApiModelProperty("分页大小 默认：20")
    private int pageSize = 20;
    @ApiModelProperty("门店ID")
    private Integer queryShopId;

    public Integer getStart() {
        return (this.pageNum-1)*this.pageSize;
    }
}