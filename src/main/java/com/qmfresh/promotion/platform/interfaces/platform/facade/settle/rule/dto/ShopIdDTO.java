package com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "ShopIdDTO", description = "根据shopId查询")
public class ShopIdDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("shopId")
    private Integer shopId;
}
