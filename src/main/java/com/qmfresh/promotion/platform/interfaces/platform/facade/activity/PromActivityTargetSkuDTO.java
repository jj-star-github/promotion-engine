package com.qmfresh.promotion.platform.interfaces.platform.facade.activity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "PromActivityTargetShopDTO", description = "目标商品信息")
public class PromActivityTargetSkuDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("商品ID")
    private Integer skuId;
    @ApiModelProperty("商品名")
    private String skuName;
    @ApiModelProperty("商品类型 2:Class1,3:class2,4:sku")
    private Integer skuType;
    @ApiModelProperty("Class1 skuType=1/3/4 传入")
    private Integer class1Id;
    @ApiModelProperty("class2 skuType=3/4 传入")
    private Integer class2Id;

    public PromActivityTargetSkuDTO() {
    }

}

