package com.qmfresh.promotion.platform.interfaces.platform.facade;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.Shop;
import com.qmfresh.promotion.platform.domain.model.activity.ShopManager;
import com.qmfresh.promotion.platform.domain.model.activity.Subject;
import com.qmfresh.promotion.platform.domain.model.activity.cp.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.command.CpCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.command.ShopQueryCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpActivity;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpActivityManager;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.command.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLog;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLogManager;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.CpCreateLogType;
import com.qmfresh.promotion.platform.domain.model.activity.cp.log.command.CpCreateLogCreateCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivityManager;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.command.ShopCpQueryCommand;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.general.*;
import com.qmfresh.promotion.platform.interfaces.platform.facade.shop.ShopCpQueryDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.shop.ShopListItemDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class GeneralCpFacadeService {

    @Resource
    private GeneralCpActivityManager generalCpActivityManager;
    @Resource
    private ShopCpActivityManager shopCpActivityManager;
    @Resource
    private ShopManager shopManager;
    @Resource
    private CpManager cpManager;
    @Resource
    private CpCreateLogManager cpCreateLogManager;

    /**
     * 创建总部改价活动
     *
     * @param createDTO 改价活动对象
     */
    public CpCreateResultDTO createGeneralCpActivity(GeneralCpCreateDTO createDTO) {
        Operator operator = Operator.create(createDTO.getOperatorId(), createDTO.getOperatorName(), Subject.create(Subject.Platform.INTANCE));

        if (createDTO.getBeginPlanCpActivityTime() == null ||
                createDTO.getEndPlanCpActivityTime() == null) {
            throw new BusinessException("必须设置开始时间和结束时间");
        }
        //todo
        Long now = System.currentTimeMillis();
        if (createDTO.getBeginPlanCpActivityTime() < now) {
            createDTO.setBeginPlanCpActivityTime(now);
        }

        CpTimeCycle cpTimeCycle = CpTimeCycle.create(createDTO.getBeginPlanCpActivityTime(), createDTO.getEndPlanCpActivityTime());


        CpCreateCommand cpCreateCommand
                = CpCreateCommand.create(operator,
                createDTO.getActivityName(),
                cpTimeCycle,
                createDTO.getShopIds(),
                SkuCpDTO.create(createDTO.getSkuCps()),
                null,
                CpBizType.CP_PLATFORM
        );


        CpCreateLogCreateCommand createLogCreateCommand =
                CpCreateLogCreateCommand.create(
                        CpBizType.CP_PLATFORM,
                        JSON.toJSONString(cpCreateCommand),
                        createDTO.getOperatorId(),
                        createDTO.getOperatorName(),
                        CpCreateLogType.MANUAL);
        CpCreateLog cpCreateLog = cpCreateLogManager.create(createLogCreateCommand);
        log.info("创建总部改价记录：i={}", cpCreateLog.getId());
        return new CpCreateResultDTO();
    }

    /**
     * 查询总部改价详情列表
     *
     * @param generalCpQueryDTO 总部改价
     * @return
     */
    public PageDTO<GeneralCpInfoDTO> findGeneralCpDetails(GeneralCpQueryDTO generalCpQueryDTO) {
        GeneralCpQueryCommand generalCpQueryCommand = GeneralCpQueryDTO.create(generalCpQueryDTO);
        if (generalCpQueryCommand.getActivityId() == null) {
            throw new BusinessException("必须设置活动id");
        }

        Page<GeneralCpItem> pageGeneralCpDetails = generalCpActivityManager.pageGeneralCpDetails(generalCpQueryCommand);
        return PageDTO.convert(pageGeneralCpDetails.getPageNum(), pageGeneralCpDetails.getPageSize(), pageGeneralCpDetails, GeneralCpInfoDTO::convert);

    }

    /**
     * 查询总部改价列表
     *
     * @param generalCpQueryDTO
     * @return
     */
    public PageDTO<GeneralCpListItemDTO> pageGeneralCpActivity(GeneralCpQueryDTO generalCpQueryDTO) {
        GeneralCpQueryCommand generalCpQueryCommand = GeneralCpQueryDTO.create(generalCpQueryDTO);
        Page<GeneralCpActivity> page = generalCpActivityManager.pageGeneralCpActivity(generalCpQueryCommand);
        return PageDTO.convert(generalCpQueryDTO.getPageNum(), generalCpQueryDTO.getPageSize(),
                page, GeneralCpListItemDTO::convert);
    }

    /**
     * 关闭总部改价活动
     *
     * @param generalCpCloseDTO 总部改价
     */
    public void closeGeneralCpActivity(GeneralCpCloseDTO generalCpCloseDTO) {
        Operator operator = Operator.create(generalCpCloseDTO.getOperatorId(), generalCpCloseDTO.getOperatorName(),
                Subject.create(Subject.Platform.INTANCE));
        generalCpActivityManager.closeGeneralCpActivity(GeneralCpCloseCommand.create(operator, generalCpCloseDTO.getActivityId(), generalCpCloseDTO.getRemark()));
    }

    /**
     * 编辑总部改价门店信息
     *
     * @param generalCpEditShopDTO 编辑门店改价总部信息
     */
    public void editShopOfGeneralCpActivity(GeneralCpEditShopDTO generalCpEditShopDTO) {
        Operator operator = Operator.create(generalCpEditShopDTO.getOperatorId(), generalCpEditShopDTO.getOperatorName(),
                Subject.create(Subject.Platform.INTANCE));

        GeneralCpShopEditCommand generalCpShopEditCommand = GeneralCpShopEditCommand.create(
                operator,
                generalCpEditShopDTO.getActivityId(),
                generalCpEditShopDTO.getShopIds()
        );

        generalCpActivityManager.editShopOfGeneralCpActivity(generalCpShopEditCommand);

    }

    /**
     * 门店改价活动列表
     *
     * @param shopCpQueryDTO 门店改价查询活动
     * @return 门店改价列表
     */
    public PageDTO<ShopListItemDTO> pageShopCpActivity(ShopCpQueryDTO shopCpQueryDTO) {

        Integer status = shopCpQueryDTO.getStatus();
        List<Integer> statusList = CpStatus.status(status);

        ShopCpQueryCommand shopCpQueryCommand = ShopCpQueryCommand
                .createPlatformQuery(shopCpQueryDTO.getPageNum(),
                        shopCpQueryDTO.getPageSize(),
                        shopCpQueryDTO.getSkuId(),
                        shopCpQueryDTO.getCurrentShopId(),
                        shopCpQueryDTO.getType(),
                        statusList,
                        shopCpQueryDTO.getActivityId(),
                        shopCpQueryDTO.getActivityNameKey(),
                        shopCpQueryDTO.getFromPlanBeginCpActivityTime() == null ? null : new Date(shopCpQueryDTO.getFromPlanBeginCpActivityTime()),
                        shopCpQueryDTO.getToPlanBeginCpActivityTime() == null ? null : new Date(shopCpQueryDTO.getToPlanBeginCpActivityTime())
                );

        Page<ShopCpActivity> shopCpActivityPage = shopCpActivityManager.pageShopCpActivity(shopCpQueryCommand);

        PageDTO<ShopListItemDTO> pageDTO = PageDTO.convert(shopCpQueryDTO.getPageNum(), shopCpQueryDTO.getPageSize(), shopCpActivityPage, ShopListItemDTO::convert);
        if (CollectionUtils.isNotEmpty(shopCpActivityPage.getRecords())) {
            List<Integer> shopIds = new ArrayList<>();
            List<ShopCpActivity> shopCpActivities = shopCpActivityPage.getRecords();
            for (ShopCpActivity shopCpActivity : shopCpActivities) {
                shopIds.add(shopCpActivity.getShop().getShopId());
            }
            List<Shop> shops = shopManager.shops(shopIds);
            Map<String, Shop> shopMap = shops.stream().collect(Collectors.toMap(Shop::getShopName, shop -> shop));
            List<ShopListItemDTO> tmp = new ArrayList<>();
            for (ShopListItemDTO s : pageDTO.getRecords()) {
                Shop shop = shopMap.get(s.getShopName());
                s.setShopTypeDesc(shop.getShopTypeDesc());
                s.setShopLevel(shop.getShopLevel());
                tmp.add(s);
            }
            pageDTO.setRecords(tmp);
        }

        return pageDTO;
    }

    /**
     * 只能修改进行中改价活动
     *
     * @param skuDTO 改价商品dto
     */
    public void editSkuOfGeneralCpActivity(GeneralCpEditSkuDTO skuDTO) {
        if (skuDTO.getOperatorType() == null) {
            throw new BusinessException("必须设置操作类型");
        }
        //编辑总部改价
        if (skuDTO.getOperatorType().equals(1)) {

            GeneralCpEditSkuCommand generalCpEditSkuCommand =
                    GeneralCpEditSkuCommand.create(
                            Operator.create(skuDTO.getOperatorId(), skuDTO.getOperatorName(),
                                    Subject.create(Subject.Platform.INTANCE)),
                            skuDTO.getActivityId(),
                            skuDTO.getSkuCp().getSkuId(),
                            skuDTO.getSkuCp().getRulePrice()
                    );

            generalCpActivityManager.editGeneralCpSku(generalCpEditSkuCommand);
            return;
        }

        //删除总部改价
        if (skuDTO.getOperatorType().equals(2)) {

            GeneralCpDelSkuCommand generalCpDelSkuCommand =
                    GeneralCpDelSkuCommand.create(
                            Operator.create(skuDTO.getOperatorId(), skuDTO.getOperatorName(), Subject.create(Subject.Platform.INTANCE)),
                            skuDTO.getActivityId(),
                            skuDTO.getSkuCp().getSkuId()
                    );
            generalCpActivityManager.delGeneralCpSku(generalCpDelSkuCommand);
            return;

        }
        throw new BusinessException("操作类型不支持");
    }

    public PageDTO<CpSkuDTO> pageGeneralCpSkus(GeneralCpSkuQueryDTO generalCpSkuQueryDTO) {

        Page<SkuCpItem> pageGeneralCpSkus = generalCpActivityManager.pageGeneralCpSkus(GeneralCpQuerySkuCommand.create(
                generalCpSkuQueryDTO.getPageNum(),
                generalCpSkuQueryDTO.getPageSize(),
                generalCpSkuQueryDTO.getActivityId(),
                generalCpSkuQueryDTO.getClass3Name(),
                generalCpSkuQueryDTO.getSkuId()));
        return PageDTO.convert(generalCpSkuQueryDTO.getPageNum(), generalCpSkuQueryDTO.getPageSize(),
                pageGeneralCpSkus, CpSkuDTO::convert);
    }

    public CpCreateResultDTO autoCreateGeneralCp(GeneralCpAutoCreateDTO autoCreateDTO) {

        GeneralCpAutoCreateCommand generalCpAutoCreateCommand =
                GeneralCpAutoCreateCommand.create(autoCreateDTO.getBizCode(),
                        autoCreateDTO.getBizName(),
                        autoCreateDTO.getRemark(), autoCreateDTO.getOperatorId(),
                        autoCreateDTO.getOperatorName(),
                        GeneralCpAutoCreateDTO.convert(autoCreateDTO.getCpItemList()), CpTimeCycle.create(autoCreateDTO.getPlanCpActivityStartTime(), autoCreateDTO.getPlanCpActivityEndTime()));
        CpItemResult cpItemResult = generalCpActivityManager.autoCreateGeneralCp(generalCpAutoCreateCommand);

        return CpCreateResultDTO.result(cpItemResult);
    }

    /**
     * 门店分页
     *
     * @param queryDTO 总部改价
     * @return 分页结果信息
     */
    public PageDTO<ShopCpResultDTO> pageShopOfGeneralCpActivity(GeneralCpQueryDTO queryDTO) {
        log.info("pageShopOfGeneralCpActivity请求参数：{}", JSON.toJSONString(queryDTO));
        ShopQueryCommand shopQueryCommand = ShopQueryCommand.create(queryDTO.getPageNum(),
                queryDTO.getPageSize(),
                queryDTO.getActivityId(), CpBizType.CP_PLATFORM);
        Page<ShopCpResult> shopCpResultPage = cpManager.pageShopCp(shopQueryCommand);
        return PageDTO.convert(queryDTO.getPageNum(), queryDTO.getPageSize(), shopCpResultPage, ShopCpResultDTO::convert);
    }
}
