package com.qmfresh.promotion.platform.interfaces.platform.facade.area;

import com.qmfresh.promotion.platform.domain.model.activity.Subject;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.command.AreaCpSkuDelCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.command.AreaCpSkuEditCommand;
import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.CpItemDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@ApiModel("大区改价模型")
@Data
public class AreaCpEditSkuDTO extends BaseDTO {

    @ApiModelProperty("操作类型：1=编辑 2=删除")
    private Integer operatorType;

    @ApiModelProperty("总部改价id")
    private Long activityId;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("商品改价")
    private CpItemDTO skuCp;

    public static AreaCpSkuEditCommand createEditCommand(AreaCpEditSkuDTO skuDTO) {
        Operator operator = Operator.create(skuDTO.getOperatorId(), skuDTO.getOperatorName(), Subject.createAreaEmpty());
        Long activityId = skuDTO.getActivityId();
        AreaCpSkuEditCommand areaCpSkuEditCommand =
                AreaCpSkuEditCommand.create(
                        operator,
                        activityId,
                        skuDTO.getSkuCp().getSkuId(),
                        skuDTO.getSkuCp().getRulePrice());


        return areaCpSkuEditCommand;
    }

    public static AreaCpSkuDelCommand createDelCommand(AreaCpEditSkuDTO skuDTO) {
        Operator operator = Operator.create(skuDTO.getOperatorId(), skuDTO.getOperatorName(), Subject.createAreaEmpty());
        AreaCpSkuDelCommand cpSkuDelCommand = AreaCpSkuDelCommand.create(
                operator,
                skuDTO.getActivityId(),
                skuDTO.getSkuCp().getSkuId(),
                skuDTO.getRemark()
        );


        return cpSkuDelCommand;
    }
}
