package com.qmfresh.promotion.platform.interfaces.shop.facade.funds;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.platform.domain.shared.BigDecimalUtil;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsOrderCPVO;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsSkuVO;
import com.qmfresh.promotion.platform.domain.model.funds.enums.FundsBusinessTypeEnum;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsChangeNote;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.FundsSettleInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.eclipse.jetty.util.StringUtil;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "ShopFundsChangeDTO", description = "门店费用流水")
public class ShopFundsChangeDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("业务类型使用费用流水使用：3: 改价支出 5：其他-收银改价 ")
    private Integer businessType;
    @ApiModelProperty("营销费用")
    private BigDecimal changeAmount;
    @ApiModelProperty("支付金额")
    private BigDecimal totalPrice;
    @ApiModelProperty("订单号")
    private String orderNum;
    @ApiModelProperty("创建时间")
    private String createTime;
    @ApiModelProperty("原价")
    private BigDecimal originPrice;
    @ApiModelProperty("促销价")
    private BigDecimal priceAfterDiscount;
    @ApiModelProperty("重量")
    private BigDecimal amount;
    @ApiModelProperty("商品单位")
    private String skuFormat;
    @ApiModelProperty("商品id")
    private Long skuId;
    @ApiModelProperty("商品名称")
    private String skuName;
    @ApiModelProperty("商品数量")
    private Integer skuNum;
    @ApiModelProperty("标题")
    private String title;

    public ShopFundsChangeDTO () {}
    public ShopFundsChangeDTO(FundsSettleInfo settleInfo) {
        this.setOrderNum(settleInfo.getOrderCode());
        this.setTotalPrice(BigDecimalUtil.round(settleInfo.getRealPay()));
        this.setChangeAmount(BigDecimalUtil.round(settleInfo.getShopFundsAmount()));
        this.setCreateTime(DateUtil.dateToStr(settleInfo.getGmtCreate(), "yyyy-MM-dd HH:mm:ss"));
    }

    public ShopFundsChangeDTO(FundsChangeNote changeNote) {
        if (StringUtil.isNotBlank(changeNote.getRemark())) {
            this.setOrderNum(changeNote.getBusinessCode());
            this.setChangeAmount(BigDecimalUtil.round(changeNote.getChangeAmount()));
            this.setCreateTime(DateUtil.dateToStr(changeNote.getGmtCreate(), "yyyy-MM-dd HH:mm:ss"));
            this.setBusinessType(changeNote.getBusinessType());
            if (FundsBusinessTypeEnum.SKU_MARKETING.getCode() == changeNote.getBusinessType()) {
                FundsSkuVO skuVo = JSON.parseObject(changeNote.getRemark(), new TypeReference<FundsSkuVO>() {
                });
                this.setSkuId(skuVo.getSkuId());
                this.setSkuName(skuVo.getSkuName());
                this.setSkuFormat(skuVo.getSkuFormat());
                if(null == skuVo.getIsWeight() || 1 == skuVo.getIsWeight().intValue()) {
                    this.setOriginPrice(BigDecimalUtil.div(skuVo.getOriginPrice(), new BigDecimal("2")));
                    this.setPriceAfterDiscount(BigDecimalUtil.div(skuVo.getPriceAfterDiscount(), new BigDecimal("2")));
                    this.setAmount(BigDecimalUtil.mul(skuVo.getAmount(), new BigDecimal("2")));
                } else {
                    this.setOriginPrice(skuVo.getOriginPrice());
                    this.setPriceAfterDiscount(skuVo.getPriceAfterDiscount());
                    this.setAmount(skuVo.getAmount());
                }
                this.setTotalPrice(skuVo.getRealAmount());
            } else if(FundsBusinessTypeEnum.ORDER_CP.getCode() == changeNote.getBusinessType()){
                FundsOrderCPVO orderCPVo = JSON.parseObject(changeNote.getRemark(), new TypeReference<FundsOrderCPVO>() {
                });
                this.setTotalPrice(BigDecimalUtil.round(changeNote.getBusinessRealAmount()));
                this.setOriginPrice(orderCPVo.getOriginPrice());//原件
                this.setSkuNum(orderCPVo.getSkuNum()); //订单商品数量
                this.setTitle(FundsBusinessTypeEnum.ORDER_CP.getRemark());
            } else if(FundsBusinessTypeEnum.ITEM_DISCOUNT.getCode() == changeNote.getBusinessType()) {
                FundsSkuVO skuVo = JSON.parseObject(changeNote.getRemark(), new TypeReference<FundsSkuVO>() {
                });
                this.setSkuId(skuVo.getSkuId());
                this.setSkuName(skuVo.getSkuName());
                if(null == skuVo.getIsWeight() || 1 == skuVo.getIsWeight().intValue()) {
                    this.setOriginPrice(BigDecimalUtil.div(skuVo.getOriginPrice(), new BigDecimal("2")));
                    this.setPriceAfterDiscount(BigDecimalUtil.div(skuVo.getPriceAfterDiscount(), new BigDecimal("2")));
                    this.setAmount(BigDecimalUtil.mul(skuVo.getAmount(), new BigDecimal("2")));
                } else {
                    this.setOriginPrice(skuVo.getOriginPrice());
                    this.setPriceAfterDiscount(skuVo.getPriceAfterDiscount());
                    this.setAmount(skuVo.getAmount());
                }
                this.setSkuFormat(skuVo.getSkuFormat());
                this.setTitle(FundsBusinessTypeEnum.ITEM_DISCOUNT.getRemark());
            } else {
                this.setSkuName(changeNote.getRemark());
            }
        }
    }

}
