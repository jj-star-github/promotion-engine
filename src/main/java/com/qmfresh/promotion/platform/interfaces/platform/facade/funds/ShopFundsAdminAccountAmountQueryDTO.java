package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "ShopFundsAdminAccountAmountQueryDTO", description = "账号费用查询")
public class ShopFundsAdminAccountAmountQueryDTO extends BaseDTO {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("门店ID，总部：1")
    @NotNull(message = "queryShopId不可空")
    private Integer queryShopId;
    @ApiModelProperty("门店类型，1：总部  4：门店")
    @NotNull(message = "shopType不可为空")
    private Integer shopType;
}