package com.qmfresh.promotion.platform.interfaces.platform.facade.general;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("总部改价商品查询")
public class GeneralCpSkuQueryDTO extends BaseDTO {

    @ApiModelProperty("页码")
    private Integer pageNum;
    @ApiModelProperty("页大小")
    private Integer pageSize;
    @ApiModelProperty("总部活动id")
    private Long activityId;
    @ApiModelProperty("商品id")
    private Integer skuId;
    @ApiModelProperty("三级分类名称")
    private String class3Name;
}
