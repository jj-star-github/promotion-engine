package com.qmfresh.promotion.platform.interfaces.shop.facade.funds;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "ShopFundsDayQueryDTO", description = "门店费用统计")
public class ShopFundsDayQueryDTO extends BaseDTO {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("结算日期:yyyy-mm-dd, 空：默认近30天")
    private String timeDay;
    @ApiModelProperty("是否显示申请费用 0：不显示 1：显示默认")
    private Integer isShowApply=1;
}
