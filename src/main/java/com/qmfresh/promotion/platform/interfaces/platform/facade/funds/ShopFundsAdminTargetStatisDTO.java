package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "ShopFundsAdminTargetStatisDTO", description = "费用统计信息")
public class ShopFundsAdminTargetStatisDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("今日总部使用营销费用")
    private BigDecimal todayUseAmount = new BigDecimal("0");
    @ApiModelProperty("今日门店使用营销费用")
    private BigDecimal todayShopUseAmount = new BigDecimal("0");
    @ApiModelProperty("今天门店使用营销费用")
    private BigDecimal todayAreaUseAmount = BigDecimal.ZERO;
    @ApiModelProperty("总部今日锁定")
    private BigDecimal todayLock = new BigDecimal("0");
    @ApiModelProperty("门店今日锁定")
    private BigDecimal shopTodayLock = new BigDecimal("0");

    @ApiModelProperty("昨日总部增加费用")
    private BigDecimal yesterdayNewAmount = new BigDecimal("0");
    @ApiModelProperty("昨日门店增加")
    private BigDecimal yesterdayShopNewAmount = new BigDecimal("0");
    @ApiModelProperty("昨日大区增加")
    private BigDecimal yesterdayAreaNewAmount = BigDecimal.ZERO;

    @ApiModelProperty("总部费用")
    private BigDecimal amount = new BigDecimal("0");
    @ApiModelProperty("总部锁定费用")
    private BigDecimal lockAmount = new BigDecimal("0");
    @ApiModelProperty("门店费用")
    private BigDecimal shopAmount = new BigDecimal("0");
    @ApiModelProperty("门店锁定费用")
    private BigDecimal shopLockAmount = new BigDecimal("0");
    @ApiModelProperty("大区营销费用")
    private BigDecimal areaAmount = new BigDecimal("0");
    @ApiModelProperty("大区锁定费用")
    private BigDecimal areaLockAmount = new BigDecimal("0");

    @ApiModelProperty("今日销售额")
    private BigDecimal todayRealPay = new BigDecimal("0");
    @ApiModelProperty("今日营销销售额")
    private BigDecimal todayMarketingPay = new BigDecimal("0");
    @ApiModelProperty("今日总营销")
    private BigDecimal todayFundAmount = new BigDecimal("0");
    @ApiModelProperty("今日门店营销费")
    private BigDecimal todayShopFundAmount = new BigDecimal("0");
    @ApiModelProperty("今日门店营销费")
    private BigDecimal todayZBFundAmount = new BigDecimal("0");
    @ApiModelProperty("今日大区营销")
    private BigDecimal todayAreaFundAmount = new BigDecimal("0");

}
