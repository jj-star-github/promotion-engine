package com.qmfresh.promotion.platform.interfaces.shop.facade.cp;

import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("已改价的商品")
public class ShopCpItemChangedDTO implements Serializable {

    @ApiModelProperty("活动id")
    private Long activityId;
    @ApiModelProperty("改价活动id")
    private Long activityCpId;
    @ApiModelProperty("改价活动名称")
    private String activityName;
    @ApiModelProperty("当前售价")
    private BigDecimal currentPrice;
    @ApiModelProperty("会员价")
    private BigDecimal vipPrice;
    @ApiModelProperty("采购价")
    private BigDecimal purchasePrice;
    @ApiModelProperty("已销售重量")
    private BigDecimal soldAmount;
    @ApiModelProperty("库存量")
    private BigDecimal storageAmount;
    @ApiModelProperty("活动生效时间")
    private Long planStartActivityTime;
    @ApiModelProperty("活动结束时间")
    private Long planEndActivityTime;
    @ApiModelProperty("计划营销费用")
    private BigDecimal platMarketingAmount;
    @ApiModelProperty("商品id")
    private Integer skuId;
    @ApiModelProperty("商品名称")
    private String skuName;
    @ApiModelProperty("商品格式")
    private String skuFormat;
    @ApiModelProperty("改价状态:0=可以 1=不可以")
    private Integer changeable;
    @ApiModelProperty("商品单位")
    private String unitFormat;
    @ApiModelProperty("商品图片")
    private String skuImage;
    @ApiModelProperty("商品原价")
    private BigDecimal originalPrice;

    public static ShopCpItemChangedDTO convert(ShopCpActivity shopCpActivity) {

        ShopCpItemChangedDTO shopCpItemChangedDTO = new ShopCpItemChangedDTO();
        shopCpItemChangedDTO.setActivityId(shopCpActivity.getActivityId());
        shopCpItemChangedDTO.setActivityCpId(shopCpActivity.getActivityCpId());
        shopCpItemChangedDTO.setActivityName(shopCpActivity.getName());
        shopCpItemChangedDTO.setPlanEndActivityTime(shopCpActivity.getPlanActivityEndTime().getTime());
        shopCpItemChangedDTO.setPlanStartActivityTime(shopCpActivity.getPlanActivityStartTime().getTime());
        shopCpItemChangedDTO.setSkuId(shopCpActivity.getSku().getSkuId());
        shopCpItemChangedDTO.setSkuFormat(shopCpActivity.getSku().getSkuFormat());
        shopCpItemChangedDTO.setSkuName(shopCpActivity.getSku().getSkuName());
        shopCpItemChangedDTO.setPlatMarketingAmount(shopCpActivity.getPlanMarketingFee() != null ? shopCpActivity.getPlanMarketingFee() : BigDecimal.ZERO);
        shopCpItemChangedDTO.setUnitFormat(shopCpActivity.getSku().getUnitFormat());
        shopCpItemChangedDTO.setSkuImage(shopCpActivity.getSku().getSkuImage());
        shopCpItemChangedDTO.setOriginalPrice(shopCpActivity.getOriginalPrice());

        return shopCpItemChangedDTO;
    }

}
