package com.qmfresh.promotion.platform.interfaces;

import com.qmfresh.promotion.common.utils.TraceIdUtils;
import com.qmfresh.promotion.platform.domain.shared.BizCode;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.shop.ShopBaseController;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * mvc异常工具
 */
@Slf4j
@ControllerAdvice
public class MVCExceptionHandle {

    /**
     * 捕捉异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = {Exception.class})
    public Object handleException(HttpServletResponse response, Exception e) {
        String traceId = TraceIdUtils.getTraceId();

        if (e instanceof BusinessException) {
            log.warn("业务异常 {}", ExceptionUtils.getStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), ((BusinessException) e).getErrorMsg());
        }

        if (e instanceof NullPointerException) {
            log.error("异常 {}", e);
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "业务异常");
        }

        if (e instanceof IllegalArgumentException) {
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), e.getMessage());
        }

        if (e instanceof MethodArgumentNotValidException) {
            MethodArgumentNotValidException me = (MethodArgumentNotValidException) e;
            StringBuilder sb = new StringBuilder();
            List<ObjectError> errors = me.getBindingResult().getAllErrors();
            if (errors.size() > 1) {
                sb.append("共" + errors.size() + "个错误；");
            }
            for (ObjectError error : errors) {
                sb.append(error.getDefaultMessage()).append("，");
            }
            sb.delete(sb.length() - 1, sb.length());
            log.warn("参数非法 param={} ex={} traceId={}", sb.toString(), ExceptionUtils.getStackTrace(e), traceId);
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        } else if (e instanceof ShopBaseController.ShopOauthException) {
            log.warn("ShopId获取失败: ex={}, traceId={}", ExceptionUtils.getStackTrace(e), traceId);
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        } else {
            log.error("ex={}, traceId={}", ExceptionUtils.getStackTrace(e), traceId);
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "业务异常");
        }
    }
}
