package com.qmfresh.promotion.platform.interfaces.platform.facade.general;

import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpItem;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("总部改价信息")
public class GeneralCpInfoDTO implements Serializable {

    @ApiModelProperty("改价活动id")
    private Long activityId;
    @ApiModelProperty("改价活动明细id")
    private Long activityCpId;
    @ApiModelProperty("商品id")
    private Integer skuId;
    @ApiModelProperty("商品名称")
    private String skuName;
    @ApiModelProperty("一级分类名称")
    private String class1Name;
    @ApiModelProperty("二级分类名称")
    private String class2Name;
    @ApiModelProperty("三级分类名称")
    private String class3Name;
    @ApiModelProperty("促销价格")
    private BigDecimal rulePrice;
    @ApiModelProperty("门店id")
    private Integer shopId;
    @ApiModelProperty("门店名称")
    private String shopName;


    /**
     * 转换对象
     *
     * @param generalCpItem 改价详情
     * @return 返回结果值
     */
    public static GeneralCpInfoDTO convert(GeneralCpItem generalCpItem) {
        GeneralCpInfoDTO cpInfoDTO = new GeneralCpInfoDTO();
        cpInfoDTO.activityId = generalCpItem.getActivityId();
        cpInfoDTO.activityCpId = generalCpItem.getActivityCpId();
        cpInfoDTO.skuId = generalCpItem.getSku().getSkuId();
        cpInfoDTO.skuName = generalCpItem.getSku().getSkuName();
        cpInfoDTO.class1Name = generalCpItem.getSku().getClass1Name();
        cpInfoDTO.class2Name = generalCpItem.getSku().getClass2Name();
        cpInfoDTO.class3Name = generalCpItem.getSku().getClass3Name();
        cpInfoDTO.rulePrice = generalCpItem.getRulePrice();
        cpInfoDTO.shopId = generalCpItem.getShop().getShopId();
        cpInfoDTO.shopName = generalCpItem.getShop().getShopName();
        return cpInfoDTO;
    }

}
