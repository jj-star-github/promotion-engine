package com.qmfresh.promotion.platform.interfaces.shop.facade;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.sellout.SkuStock;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.common.utils.TimeUtil;
import com.qmfresh.promotion.dto.SkuStockInShopQuery;
import com.qmfresh.promotion.entity.ShopDetail;
import com.qmfresh.promotion.entity.ShopSku;
import com.qmfresh.promotion.entity.ShopSkuSellOrStock;
import com.qmfresh.promotion.external.service.IBiExternalApiService;
import com.qmfresh.promotion.external.service.IInventoryService;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.platform.domain.model.activity.*;
import com.qmfresh.promotion.platform.domain.model.activity.cp.CpStatus;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.action.CloseCpAction;
import com.qmfresh.promotion.platform.domain.model.activity.cp.action.PlanCpAction;
import com.qmfresh.promotion.platform.domain.model.activity.cp.action.QueryCpAction;
import com.qmfresh.promotion.platform.domain.model.activity.cp.context.CpContext;
import com.qmfresh.promotion.platform.domain.model.activity.cp.context.CpContextFactory;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpActivity;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.GeneralCpActivityManager;
import com.qmfresh.promotion.platform.domain.model.activity.cp.general.command.GeneralCpQueryCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPrice;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPriceCompute;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.command.CpActivitySpec;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.command.PromotionPriceCommand;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivity;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.ShopCpActivityManager;
import com.qmfresh.promotion.platform.domain.model.activity.cp.shop.command.ShopCpChangedQueryCommand;
import com.qmfresh.promotion.platform.domain.model.settle.apportion.ApportionManager;
import com.qmfresh.promotion.platform.domain.shared.BigDecimalUtil;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.ChangePriceActivityMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ChangePriceActivityPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopPromotionRule;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.general.GeneralCpListItemDTO;
import com.qmfresh.promotion.platform.interfaces.shop.facade.cp.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 做一些对象的拆解和组合,形成一些领域的上下文
 *
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class ShopCpFacadeService {

    @Resource
    private SkuManager skuManager;
    @Resource
    private ShopManager shopManager;
    @Resource
    private CpContextFactory cpContextFactory;
    @Resource
    private GeneralCpActivityManager generalCpActivityManager;
    @Resource
    private ShopCpActivityManager shopCpActivityManager;
    @Resource
    private ChangePriceActivityMapper changePriceActivityMapper;
    @Resource
    private PromotionPriceCompute promotionPriceCompute;
    @Resource
    private ChangePriceActivityMapper changePriceActivityManage;
    @Resource
    private IQmExternalApiService iQmExternalApiService;
    @Resource
    private ApportionManager apportionManager;
    @Resource
    private IBiExternalApiService iBiExternalApiService;
    @Autowired
    private IInventoryService inventoryService;

    /**
     * 创建门店活动
     *
     * @param shopCpCreateDTO 门店创建活动
     * @return 门店改价信息
     */
    public ShopCpInfoDTO createShopCpActivity(ShopCpCreateDTO shopCpCreateDTO) {
        log.info("创建门店改价请求参数：{}", JSON.toJSONString(shopCpCreateDTO));
        //准备数据
        Shop shop = shopManager.shop(shopCpCreateDTO.getShopId());
        Operator operator = Operator
                .create(shopCpCreateDTO.getOperatorId(),
                        shopCpCreateDTO.getOperatorName(), Subject.create(shop));
        Sku sku = skuManager.sku(shopCpCreateDTO.getSkuId());

        //计算门店改价的活动
        Long start = shopCpCreateDTO.getPlanStartActivityTime();
        Long end = shopCpCreateDTO.getPlanEndActivityTime();

        DateTime now = DateTime.now();
        Integer startDate = new DateTime(start).getDayOfYear();
        Integer nowDate = now.getDayOfYear();
        if (nowDate.equals(startDate)) {
            shopCpCreateDTO.setPlanStartActivityTime(now.getMillis());
        } else {
            shopCpCreateDTO.setPlanStartActivityTime(start);
        }

        DateTime endDate = new DateTime(end).withTimeAtStartOfDay().plusDays(1).minusSeconds(1);
        shopCpCreateDTO.setPlanEndActivityTime(endDate.getMillis());

        //创建上下文
        CpContext context = cpContextFactory.createShopCp(operator,
                PlanCpAction.createShopPlanCpAction(sku,
                        shopCpCreateDTO.getPlanMarketingFee(),
                        shopCpCreateDTO.getRulePrice(),
                        CpTimeCycle.create(
                                shopCpCreateDTO.getPlanStartActivityTime(),
                                shopCpCreateDTO.getPlanEndActivityTime()
                        )));

        //执行创建活动
        ShopCpActivity shopCpActivity = (ShopCpActivity) context.create(context);

        //转换改价信息
        return ShopCpInfoDTO.create(shopCpActivity);
    }

    /**
     * 分页查询门店改价活动
     *
     * @param shopCpQueryDTO 门店改价查询对象
     * @return 门店改价分页对象
     */
    public PageDTO<ShopCpInfoDTO> pageShopCpActivity(ShopCpQueryDTO shopCpQueryDTO) {
        Shop shop = shopManager.shop(shopCpQueryDTO.getShopId());
        Operator operator = Operator.create(shopCpQueryDTO.getOperatorId(),
                shopCpQueryDTO.getOperatorName(), Subject.create(shop)
        );
        CpContext cpContext = cpContextFactory.createShopCp(operator,
                QueryCpAction.create(shopCpQueryDTO.getPageNum(), shopCpQueryDTO.getPageSize(),
                        shopCpQueryDTO.getSkuId(), shopCpQueryDTO.getSkuName()));
        Page<ShopCpActivity> page = cpContext.page(cpContext);
        return PageDTO.convert(shopCpQueryDTO.getPageNum(), shopCpQueryDTO.getPageSize(), page, ShopCpInfoDTO::create);
    }

    /**
     * 关闭门店改价活动
     *
     * @param shopCpCloseDTO 门店改价关闭对象
     */
    public void closeShopCpActivity(ShopCpCloseDTO shopCpCloseDTO) {
        Shop shop = shopManager.shop(shopCpCloseDTO.getShopId());
        Operator operator = Operator.create(shopCpCloseDTO.getOperatorId(),
                shopCpCloseDTO.getOperatorName(), Subject.create(shop));

        ShopCpActivity shopCpActivity = ShopCpActivity.builder()
                .build();
        shopCpActivity.setActivityCpId(shopCpCloseDTO.getActivityCpId());
        CpContext cpContext = cpContextFactory.createShopCp(operator,
                CloseCpAction.create(shopCpActivity, "门店店长关闭活动：" + shop.getShopName() + operator.getOperatorName())
        );

        cpContext.close(cpContext);
    }


    public PageDTO<GeneralCpListItemDTO> listShopGeneralCpActivities(ShopCpQueryDTO shopCpQueryDTO) {
        // @ApiModelProperty("状态:0=全部 1=进行中 2=已过期 3=待生效,4=待生效，进行中")

        List<CpStatus> cpStatuses = new ArrayList<>();
        if (shopCpQueryDTO.getStatus() != null && shopCpQueryDTO.getStatus() == 1) {
            cpStatuses.add(CpStatus.RUNNING);
        }
        if (shopCpQueryDTO.getStatus() != null && shopCpQueryDTO.getStatus() == 3) {
            cpStatuses.add(CpStatus.CREATE);
        }
        if (shopCpQueryDTO.getStatus() != null && shopCpQueryDTO.getStatus() == 2) {
            cpStatuses.add(CpStatus.CANCELED);
            cpStatuses.add(CpStatus.CLOSED);
            cpStatuses.add(CpStatus.FINISHED);
        }
        if (shopCpQueryDTO.getStatus() != null && shopCpQueryDTO.getStatus() == 4) {
            cpStatuses.add(CpStatus.CREATE);
            cpStatuses.add(CpStatus.RUNNING);
        }

        Integer shopId = shopCpQueryDTO.getShopId();
        Page<GeneralCpActivity> page = generalCpActivityManager.pageGeneralCpActivity(GeneralCpQueryCommand.builder()
                .pageNum(shopCpQueryDTO.getPageNum())
                .pageSize(shopCpQueryDTO.getPageSize())
                .shopId(shopId)
                .cpStatusList(cpStatuses)
                .build()
        );

        return PageDTO.convert(shopCpQueryDTO.getPageNum(), shopCpQueryDTO.getPageSize(), page, GeneralCpListItemDTO::convert);
    }

    /**
     * 查询门店已改价的商品
     *
     * @param shopCpQueryDTO 查询对象
     * @return 分页返回
     */
    public PageDTO<ShopCpItemChangedDTO> pageShopCpActivityChanged(ShopCpQueryDTO shopCpQueryDTO) {


        String skuKey = shopCpQueryDTO.getSkuKey();


        ShopCpChangedQueryCommand shopCpChangedQueryCommand = ShopCpChangedQueryCommand.create(
                shopCpQueryDTO.getPageNum(),
                shopCpQueryDTO.getPageSize(),
                shopCpQueryDTO.getShopId(),
                shopCpQueryDTO.getSkuKey()
        );

        Page<ShopCpActivity> page = shopCpActivityManager.pageShopCpActivityChanged(shopCpChangedQueryCommand);


        PageDTO<ShopCpItemChangedDTO> shopCpItemChangedDTOPageDTO =
                PageDTO.convert(shopCpQueryDTO.getPageNum(), shopCpQueryDTO.getPageSize(),
                        page, ShopCpItemChangedDTO::convert);

        if (CollectionUtils.isEmpty(shopCpItemChangedDTOPageDTO.getRecords())) {
            return shopCpItemChangedDTOPageDTO;
        }

        Integer shopId = shopCpQueryDTO.getShopId();
        List<Integer> skuIds = shopCpItemChangedDTOPageDTO.getRecords().stream().map(ShopCpItemChangedDTO::getSkuId).collect(Collectors.toList());

        List<SkuExt> skuExts = skuManager.skuExt(shopId, skuIds);
        Map<Integer, SkuExt> skuExtMap = skuExts.stream().collect(Collectors.toMap(SkuExt::getSkuId, o -> o));
        List<ShopCpItemChangedDTO> temp = new ArrayList<>();
        List<ShopCpItemChangedDTO> shopCpItemChangedDTOS = shopCpItemChangedDTOPageDTO.getRecords();
        for (ShopCpItemChangedDTO shopCpItemChangedDTO : shopCpItemChangedDTOS) {
            SkuExt skuExt = skuExtMap.get(shopCpItemChangedDTO.getSkuId());
            shopCpItemChangedDTO.setCurrentPrice(skuExt == null || skuExt.getCurrentPrice() == null ? BigDecimal.ZERO : skuExt.getCurrentPrice());
            shopCpItemChangedDTO.setStorageAmount(skuExt == null || skuExt.getInventoryAmount() == null ? BigDecimal.ZERO : skuExt.getInventoryAmount());
            shopCpItemChangedDTO.setPurchasePrice(skuExt == null || skuExt.getPurchasePrice() == null ? BigDecimal.ZERO : skuExt.getPurchasePrice());
            shopCpItemChangedDTO.setSoldAmount(skuExt == null || skuExt.getSoldAmount() == null ? BigDecimal.ZERO : skuExt.getSoldAmount());
            shopCpItemChangedDTO.setOriginalPrice(skuExt == null || skuExt.getOriginPrice() == null ? BigDecimal.ZERO : skuExt.getOriginPrice());
            temp.add(shopCpItemChangedDTO);
        }

        shopCpItemChangedDTOPageDTO.setRecords(temp);
        return shopCpItemChangedDTOPageDTO;
    }

    public ShopSkuPrice shopSkuPrice(ShopSkuQueryDTO shopSkuQueryDTO){
        Integer shopId = shopSkuQueryDTO.getShopId();
        Integer skuId = shopSkuQueryDTO.getSkuId();
        SkuPrice skuPrice = skuManager.shopSkuPrice(shopId, skuId);
        if (skuPrice == null) {
            log.warn("未售卖该品：shopId={} skuId={}", shopId, skuId);
            throw new BusinessException("该商品未售卖");
        }
        CpActivitySpec spec = CpActivitySpec.spec(changePriceActivityMapper, shopId, skuId);
        spec.setShopCpActivity(null);
        //计算促销价
        PromotionPriceCommand priceCommand = PromotionPriceCommand
                .create(shopId, skuId, skuPrice.getOriginalPrice(), spec);
        PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
        log.info("promotionPrice:" + JSON.toJSONString(promotionPrice));

        ShopSkuPrice shopSkuPrice = new ShopSkuPrice();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if(promotionPrice.getSpec().getGeneralPromotion() != null && promotionPrice.getSpec().getAreaPromotion() == null){
            shopSkuPrice.setType(promotionPrice.getSpec().getGeneralPromotion().getType());
            shopSkuPrice.setPromotionPrice(BigDecimalUtil.round(promotionPrice.getSpec().getGeneralPromotion().getRulePrice()));
            BigDecimal diffPrice = promotionPrice.getSpec().getGeneralPromotion().getMarketingDiffFee();
            if(diffPrice.compareTo(BigDecimal.ZERO) > -1){
                shopSkuPrice.setDiffType(1);
                shopSkuPrice.setDiffPrice(BigDecimalUtil.round(diffPrice.abs()));
            }else {
                shopSkuPrice.setDiffType(0);
                shopSkuPrice.setDiffPrice(BigDecimalUtil.round(diffPrice.abs()));
            }
            Long activityCpId = promotionPrice.getSpec().getGeneralPromotion().getActivityCpId();
            ChangePriceActivityPO changePriceActivityPO = changePriceActivityManage.selectById(activityCpId);
            Date startTime = changePriceActivityPO.getPlanActivityStartTime();
            Date endTime = changePriceActivityPO.getPlanActivityEndTime();
            shopSkuPrice.setPlanStartTime(sdf.format(startTime));
            shopSkuPrice.setPlanEndTime(sdf.format(endTime));
            shopSkuPrice.setIsDiff(1);
        }else if(promotionPrice.getSpec().getAreaPromotion() != null && promotionPrice.getSpec().getGeneralPromotion() ==null){
            shopSkuPrice.setType(promotionPrice.getSpec().getAreaPromotion().getType());
            shopSkuPrice.setPromotionPrice(BigDecimalUtil.round(promotionPrice.getSpec().getAreaPromotion().getRulePrice()));
            BigDecimal diffPrice = promotionPrice.getSpec().getAreaPromotion().getMarketingDiffFee();
            if(diffPrice.compareTo(BigDecimal.ZERO) > -1){
                shopSkuPrice.setDiffType(1);
                shopSkuPrice.setDiffPrice(BigDecimalUtil.round(diffPrice.abs()));
            }else {
                shopSkuPrice.setDiffType(0);
                shopSkuPrice.setDiffPrice(BigDecimalUtil.round(diffPrice.abs()));
            }
            Long activityCpId = promotionPrice.getSpec().getAreaPromotion().getActivityCpId();
            ChangePriceActivityPO changePriceActivityPO = changePriceActivityManage.selectById(activityCpId);
            Date startTime = changePriceActivityPO.getPlanActivityStartTime();
            Date endTime = changePriceActivityPO.getPlanActivityEndTime();
            shopSkuPrice.setPlanStartTime(sdf.format(startTime));
            shopSkuPrice.setPlanEndTime(sdf.format(endTime));
            shopSkuPrice.setIsDiff(1);
        }else {
            shopSkuPrice.setIsDiff(0);
        }
        log.info("shopSkuPrice:" + JSON.toJSONString(shopSkuPrice));
        return shopSkuPrice;
    }

    public ShopDiffPrice shopDiffPrice(ShopPriceQueryDTO shopPriceQueryDTO) throws ParseException{
        ShopDiffPrice shopDiffPrice = new ShopDiffPrice();
        Integer shopId = shopPriceQueryDTO.getShopId();
        Integer skuId = shopPriceQueryDTO.getSkuId();
        BigDecimal promPrice = shopPriceQueryDTO.getPromotionPrice();
        SkuPrice skuPrice = skuManager.shopSkuPrice(shopId, skuId);
        //差值
        BigDecimal diffPrice = promPrice.subtract(skuPrice.getOriginalPrice());
        shopDiffPrice.setDiffPrice(BigDecimalUtil.round(diffPrice));

        if (skuPrice == null) {
            log.warn("未售卖该品：shopId={} skuId={}", shopId, skuId);
            throw new BusinessException("该商品未售卖");
        }

        CpActivitySpec spec = CpActivitySpec.spec(changePriceActivityMapper, shopId, skuId);
        spec.setShopCpActivity(null);
        //计算促销价
        PromotionPriceCommand priceCommand = PromotionPriceCommand
                .create(shopId, skuId, skuPrice.getOriginalPrice(), spec);
        PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);
        log.info("promotionPrice:" + JSON.toJSONString(promotionPrice));
        //开始时间00:00:00
        Integer startTime = TimeUtil.getSecsOfMorning(shopPriceQueryDTO.getStartTime().intValue());
        //开始时间23:59:59
        Integer endTime = TimeUtil.getSecsOfNight(shopPriceQueryDTO.getEndTime().intValue());
        String format = "yyyy-MM-dd HH:mm:ss";
        Date shopStartTime = DateUtil.getDateByTimeStamp(startTime,format);
        Date shopEndTime = DateUtil.getDateByTimeStamp(endTime,format);
        log.info("时间：shopStartTime={} shopEndTime={}", shopStartTime, shopEndTime);
        BigDecimal rulePrice = null;
        Date generalStartTime = null;
        Date generalEndTime = null;
        if(promotionPrice.getSpec().getGeneralPromotion() != null && promotionPrice.getSpec().getAreaPromotion() == null){
            rulePrice = promotionPrice.getSpec().getGeneralPromotion().getRulePrice();
            Long activityCpId = promotionPrice.getSpec().getGeneralPromotion().getActivityCpId();
            ChangePriceActivityPO changePriceActivityPO = changePriceActivityManage.selectById(activityCpId);
            generalStartTime = changePriceActivityPO.getPlanActivityStartTime();
            generalEndTime = changePriceActivityPO.getPlanActivityEndTime();
        }else if(promotionPrice.getSpec().getAreaPromotion() != null && promotionPrice.getSpec().getGeneralPromotion() ==null){
            rulePrice = promotionPrice.getSpec().getAreaPromotion().getRulePrice();
            Long activityCpId = promotionPrice.getSpec().getAreaPromotion().getActivityCpId();
            ChangePriceActivityPO changePriceActivityPO = changePriceActivityManage.selectById(activityCpId);
            generalStartTime = changePriceActivityPO.getPlanActivityStartTime();
            generalEndTime = changePriceActivityPO.getPlanActivityEndTime();
        }
        //generalStartTime
        if(generalStartTime != null){
            Integer genStartTime = TimeUtil.getSecsOfMorning(generalStartTime);
            Date gsTime = DateUtil.getDateByTimeStamp(genStartTime,format);
            log.info("存在活动时间：generalStartTime={} generalEndTime={}", gsTime, generalEndTime);
            if(rulePrice != null && promPrice.compareTo(rulePrice) == 1 && gsTime != null && generalEndTime != null){
                if(DateUtil.isInTheTime(shopStartTime,gsTime,generalEndTime) || DateUtil.isInTheTime(shopEndTime,gsTime,generalEndTime)){
                    shopDiffPrice.setDiffTips("高于总部/大区价格 设置不生效");
                }
                if(DateUtil.isInTheTime(gsTime,shopStartTime,shopEndTime) || DateUtil.isInTheTime(generalEndTime,shopStartTime,shopEndTime)){
                    shopDiffPrice.setDiffTips("高于总部/大区价格 设置不生效");
                }
            }
        }

        ShopDetail shopDetail = iQmExternalApiService.getShopById(shopId);
        log.info("门店信息:" + JSON.toJSONString(shopDetail));
        //门店级别
        Integer shopLevel = shopDetail.getShopLevel();
        ShopPromotionRule shopPromotionRule = apportionManager.queryRuleType(shopLevel);
        BigDecimal hundred = new BigDecimal("100");
        BigDecimal shopProPrice = skuPrice.getOriginalPrice().multiply(shopPromotionRule.getPromotionPercent().divide(hundred)).multiply(shopPromotionRule.getShopPercent().divide(hundred));
        log.info("门店营销规则:" + JSON.toJSONString(shopPromotionRule));
        log.info("每斤/件会得到的营销经费:" + JSON.toJSONString(shopProPrice));
        shopDiffPrice.setProPrice(BigDecimalUtil.round(diffPrice.add(shopProPrice)));

        //查询近七天平均销量
        ShopSku shopSku = new ShopSku();
        shopSku.setShopId(shopId);
        shopSku.setSkuId(skuId);
        ShopSkuSellOrStock sellOrStock = iBiExternalApiService.getShopSkuSellOrStock(shopSku);
        log.info("查询近七天平均销量:" + JSON.toJSONString(sellOrStock));
        BigDecimal saleNum = new BigDecimal("0.00");
        saleNum = sellOrStock.getSaleNum();
        //通过shopid查WarehouseId
        ShopDetail warehouse = iQmExternalApiService.getShopById(shopId);
        log.info("通过shopid查WarehouseId:" + JSON.toJSONString(warehouse));
        Integer warehouseId = warehouse.getWarehouseId();
        if (warehouseId == null) {
            log.warn("通过shopid查WarehouseId错误：shopId={} skuId={}", shopId, skuId);
            throw new BusinessException("通过shopid查WarehouseId错误");
        }
        //查询sku是否标品
        List skuList = new ArrayList();
        skuList.add(skuId);
        List<com.qmfresh.promotion.bean.sku.Sku> sku = iQmExternalApiService.getBySkuIds(skuList);
        log.info("查询sku是否标品:" + JSON.toJSONString(sku));
        if(sku == null){
            log.warn("sku无效：shopId={} skuId={}", shopId, skuId);
            throw new BusinessException("sku无效");
        }
        if(saleNum.compareTo(BigDecimal.ZERO)==0){
            //查询sku实时库存
            SkuStockInShopQuery query = new SkuStockInShopQuery();
            query.setWarehouseId(warehouseId.longValue());
            query.setSkuIds(skuList);
            List<SkuStock> stocks = inventoryService.querySkuStockInShop(query);
            log.info("查询sku实时库存:" + JSON.toJSONString(stocks));
            if(ListUtil.isNullOrEmpty(stocks)){
                log.warn("查询实时库存为空：shopId={} skuId={}", shopId, skuId);
                saleNum = new BigDecimal("0.00");
            }else {
                if(sku.get(0).getIsWeight().equals(0)){
                    saleNum = stocks.get(0).getAmount();
                }else if(sku.get(0).getIsWeight().equals(1)){
                    saleNum = stocks.get(0).getAmountFormat();
                }
                if(saleNum.compareTo(BigDecimal.ZERO) == -1){
                    saleNum = new BigDecimal("0.00");
                }
            }
        }

        //活动天数
        long day = 1000 * 24 * 60 * 60;
        long dayNum = DateUtil.getDatePoor(shopEndTime,shopStartTime)/day +1;
        log.info("活动天数:" + JSON.toJSONString(dayNum));
        shopDiffPrice.setUseProPrice(BigDecimalUtil.round(saleNum.multiply(diffPrice).multiply(new BigDecimal(dayNum))));
        log.info("返回参数:" + JSON.toJSONString(shopDiffPrice));
        return shopDiffPrice;
    }
}
