package com.qmfresh.promotion.platform.interfaces.platform.facade;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpItem;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("改价明细")
public class CpItemDTO implements Serializable {

    @ApiModelProperty("门店id")
    private Integer shopId;
    @ApiModelProperty("商品id")
    private Integer skuId;
    @ApiModelProperty("商品价格")
    private BigDecimal rulePrice;


    public static CpItemDTO create(Integer skuId, BigDecimal rulePrice) {
        return create(null, skuId, rulePrice);
    }

    public static CpItemDTO create(Integer shopId, Integer skuId, BigDecimal rulePrice) {
        CpItemDTO cpItemDTO = new CpItemDTO();
        cpItemDTO.setShopId(shopId);
        cpItemDTO.setSkuId(skuId);
        cpItemDTO.setRulePrice(rulePrice);
        return cpItemDTO;
    }

    public static CpItem create(CpItemDTO cpItemDTO) {
        CpItem cpItem = new CpItem();
        cpItem.setShopId(cpItemDTO.getShopId());
        cpItem.setSkuId(cpItemDTO.getSkuId());
        cpItem.setRulePrice(cpItemDTO.getRulePrice());
        return cpItem;
    }
}
