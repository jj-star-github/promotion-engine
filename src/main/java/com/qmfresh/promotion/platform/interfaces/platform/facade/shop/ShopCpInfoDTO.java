package com.qmfresh.promotion.platform.interfaces.platform.facade.shop;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@ApiModel("门店改价列表")
public class ShopCpInfoDTO implements Serializable {
}
