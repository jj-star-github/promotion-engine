package com.qmfresh.promotion.platform.interfaces.platform.facade.settle.record.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "IncomeRecord", description = "收入记录")
public class UseRecord implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("类型")
    private String type;
    @ApiModelProperty("活动名称")
    private String activityName;
    @ApiModelProperty("锁定营销费")
    private BigDecimal lockMarketing;
    @ApiModelProperty("使用营销费")
    private BigDecimal useMarketing;
    @ApiModelProperty("总营销费")
    private BigDecimal amountMarketing;
    @ApiModelProperty("操作者")
    private String operator;
    @ApiModelProperty("创建时间")
    private Integer gmtCreate;
}
