package com.qmfresh.promotion.platform.interfaces.platform.facade.settle.record.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "MarketingRecordReqDTO", description = "查询营销费用记录")
public class MarketingRecordReqDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("查询时间")
    private Integer gmtModified;
    @ApiModelProperty("门店id")
    private Integer shopId;
}
