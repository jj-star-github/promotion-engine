package com.qmfresh.promotion.platform.interfaces.platform.facade.price;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("促销价生成说明规范")
public class PromotionPriceSpecDTO implements Serializable {

    @ApiModelProperty("总部改价情况")
    private PromotionPriceRuleDTO general;
    @ApiModelProperty("门店改价情况")
    private PromotionPriceRuleDTO shop;
    @ApiModelProperty("会员价情况")
    private PromotionPriceRuleDTO vip;
    @ApiModelProperty("大区改价情况")
    private PromotionPriceRuleDTO area;
}
