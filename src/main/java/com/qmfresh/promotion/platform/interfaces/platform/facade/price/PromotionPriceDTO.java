package com.qmfresh.promotion.platform.interfaces.platform.facade.price;

import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPrice;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("最终促销价")
public class PromotionPriceDTO implements Serializable {
    @ApiModelProperty("最终促销价格")
    private BigDecimal lastPromotionPrice;
    @ApiModelProperty("计算时间戳")
    private Long createTimestamp;
    @ApiModelProperty("促销价生成说明")
    private PromotionPriceSpecDTO spec;
    @ApiModelProperty("门店id")
    private Integer shopId;
    @ApiModelProperty("skuId")
    private Integer skuId;

    /**
     * @param promotionPrice 促销价
     * @return 促销价
     */
    public static PromotionPriceDTO create(PromotionPrice promotionPrice, BigDecimal originalPrice) {
        PromotionPriceDTO promotionPriceDTO = new PromotionPriceDTO();
        promotionPriceDTO.setLastPromotionPrice(promotionPrice.getLastPromotionPrice());
        promotionPriceDTO.setCreateTimestamp(System.currentTimeMillis());
        promotionPriceDTO.setShopId(promotionPrice.getShopId());
        promotionPriceDTO.setSkuId(promotionPrice.getSkuId());

        PromotionPriceSpecDTO promotionPriceSpecDTO = new PromotionPriceSpecDTO();
        promotionPriceSpecDTO.setArea(null);
        if (promotionPrice.getSpec().getGeneralPromotion() != null) {
            promotionPriceSpecDTO.setGeneral(PromotionPriceRuleDTO.create(promotionPrice.getSpec().getGeneralPromotion(), originalPrice));
        }
        if (promotionPrice.getSpec().getShopPromotion() != null) {
            promotionPriceSpecDTO.setShop(PromotionPriceRuleDTO.create(promotionPrice.getSpec().getShopPromotion(), originalPrice));
        }
        if (promotionPrice.getSpec().getAreaPromotion() != null) {
            promotionPriceSpecDTO.setArea(PromotionPriceRuleDTO.create(promotionPrice.getSpec().getAreaPromotion(), originalPrice));
        }
        promotionPriceSpecDTO.setVip(null);
        promotionPriceDTO.setSpec(promotionPriceSpecDTO);
        return promotionPriceDTO;
    }
}
