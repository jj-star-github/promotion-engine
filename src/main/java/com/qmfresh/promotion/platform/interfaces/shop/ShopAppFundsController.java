package com.qmfresh.promotion.platform.interfaces.shop;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.application.ShopAppFundsService;
import com.qmfresh.promotion.platform.domain.shared.BizCode;
import com.qmfresh.promotion.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.shop.facade.funds.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/shop/funds")
@Slf4j
@Api(tags = "门店费用接口")
public class ShopAppFundsController extends ShopBaseController{

    @Resource
    ShopAppFundsService shopAppFundsService;

    @ApiOperation(value = "门店账户信息", notes = "门店账户信息")
    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public ServiceResultDTO<ShopFundsAccountDTO> account() {
        Integer shopId = getShopId();
        log.info("门店账户信息查询 请求：shopId={}", shopId);
        try {
            return ServiceResultDTO.success(shopAppFundsService.queryAccount(shopId));
        } catch (Exception e) {
            log.error("门店账户信息查询 接口异常 shopId={} e={}", shopId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "门店费用日统计", notes = "门店费用日统计")
    @RequestMapping(value = "/dayStatis", method = RequestMethod.POST)
    public ServiceResultDTO<List<ShopFundsDayStatisDTO>> dayStatis(@Valid @RequestBody ShopFundsDayQueryDTO param) {
        Integer shopId = getShopId();
        log.info("门店费用日统计 请求：shopId={} param={}", shopId, JSON.toJSONString(param));
        try {
            return ServiceResultDTO.success(shopAppFundsService.queryFundsDayStatis(shopId, param.getTimeDay(), param.getIsShowApply()));
        } catch (Exception e) {
            log.error("门店费用日统计 接口异常 shopId={} e={}", shopId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "门店费用流水", notes = "门店费用流水")
    @RequestMapping(value = "/businessChange", method = RequestMethod.POST)
    public ServiceResultDTO<List<ShopFundsChangeDTO>> businessChange(@Valid @RequestBody ShopFundsChangeQueryDTO param) {
        Integer shopId = getShopId();
        log.info("门店费用流水 请求：shopId={} param={}",shopId, JSON.toJSONString(param));
        try {
            param.setShopId(shopId);
            return ServiceResultDTO.success(shopAppFundsService.businessChange(param));
        } catch (Exception e) {
            log.error("门店费用流水 接口异常 shopId={} e={}", shopId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "费用使用类型", notes = "费用使用类型")
    @RequestMapping(value = "/queryUseType", method = RequestMethod.POST)
    public ServiceResultDTO<List<ShopFundsUseTypeDTO>> queryUseType(@Valid @RequestBody ShopFundsDayQueryDTO param) {
        Integer shopId = getShopId();
        log.info("费用使用类型 请求：shopId={} param={}", shopId, JSON.toJSONString(param));
        try {
            return ServiceResultDTO.success(shopAppFundsService.queryUseType(shopId, param.getTimeDay()));
        } catch (Exception e) {
            log.error("费用使用类型 接口异常 shopId={} e={}", shopId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }

    @ApiOperation(value = "查询门店去化奖励详情",notes = "查询门店去化奖励详情")
    @RequestMapping(value = "/queryWeekClear",method = RequestMethod.POST)
    public ServiceResultDTO<ShopWeekClearDetailDTO> queryWeekClear(@Valid @RequestBody ShopWeekClearDTO param){
        Integer shopId = getShopId();
        log.info("查询门店去化奖励详情 请求：shopId={} param={}",shopId, JSON.toJSONString(param));
        try {
            param.setShopId(shopId);
            return ServiceResultDTO.success(shopAppFundsService.queryWeekClear(param));
        } catch (Exception e) {
            log.error("查询门店去化奖励详情 接口异常 shopId={} e={}", shopId, LogExceptionStackTrace.errorStackTrace(e));
            return ServiceResultDTO.fail(BizCode.ERROR.getCode(), "");
        }
    }
}
