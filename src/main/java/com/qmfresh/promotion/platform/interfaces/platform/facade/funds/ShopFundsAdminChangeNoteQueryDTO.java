package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "ShopFundsAdminChangeNoteQueryDTO", description = "流水查询")
public class ShopFundsAdminChangeNoteQueryDTO extends BaseDTO {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("时间:yyyy-mm-dd")
    @NotNull(message = "createDay不可为空")
    private String createDay;
    @ApiModelProperty("账号ID")
    @NotNull(message = "accountId不可为空")
    private Long accountId;
    @ApiModelProperty("页码 默认：1")
    private int pageNum = 1;
    @ApiModelProperty("分页大小 默认：20")
    private int pageSize = 20;

    public Integer getStart() {
        return (this.pageNum-1)*this.pageSize;
    }
}