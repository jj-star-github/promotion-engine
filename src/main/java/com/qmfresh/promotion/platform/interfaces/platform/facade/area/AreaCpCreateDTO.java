package com.qmfresh.promotion.platform.interfaces.platform.facade.area;

import com.qmfresh.promotion.platform.domain.model.activity.CpTimeCycle;
import com.qmfresh.promotion.platform.domain.model.activity.Subject;
import com.qmfresh.promotion.platform.domain.model.activity.cp.Operator;
import com.qmfresh.promotion.platform.domain.model.activity.cp.area.command.AreaCpCreateCommand;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.SkuCpDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@ApiModel("创建对象")
@Data
public class AreaCpCreateDTO extends BaseDTO {
    @ApiModelProperty(value = "活动名称", required = true)
    private String activityName;
    @ApiModelProperty(value = "计划活动开始时间", required = true)
    private Long beginPlanCpActivityTime;
    @ApiModelProperty(value = "计划活动结束时间", required = true)
    private Long endPlanCpActivityTime;
    @ApiModelProperty(value = "门店列表", required = true)
    private List<Integer> shopIds;
    @ApiModelProperty(value = "商品列表", required = true)
    private List<SkuCpDTO> skuCps;
    @ApiModelProperty(value = "营销账号id", required = true)
    private Long marketingAccountId;

    public static AreaCpCreateCommand areaCreateCommand(AreaCpCreateDTO areaCpCreateDTO) {

        if (areaCpCreateDTO.getMarketingAccountId() == null) {
            throw new BusinessException("必须指定营销账号");
        }

        if (CollectionUtils.isEmpty(areaCpCreateDTO.getSkuCps())) {
            throw new BusinessException("改价商品列表不能为空");
        }

        Operator operator = Operator.create(areaCpCreateDTO.getOperatorId(),
                areaCpCreateDTO.getOperatorName(),
                Subject.create(areaCpCreateDTO.getMarketingAccountId())
        );
        AreaCpCreateCommand areaCpCreateCommand =
                AreaCpCreateCommand.create(operator,
                        areaCpCreateDTO.getActivityName(),
                        CpTimeCycle.create(areaCpCreateDTO.getBeginPlanCpActivityTime(), areaCpCreateDTO.getEndPlanCpActivityTime()),
                        areaCpCreateDTO.getShopIds(),
                        areaCpCreateDTO.getSkuCps().stream().map(SkuCpDTO::cpItem).collect(Collectors.toList())
                );

        return areaCpCreateCommand;
    }
}
