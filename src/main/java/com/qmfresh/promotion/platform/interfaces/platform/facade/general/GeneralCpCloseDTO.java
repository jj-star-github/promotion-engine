package com.qmfresh.promotion.platform.interfaces.platform.facade.general;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("总部改价关闭模型")
public class GeneralCpCloseDTO extends BaseDTO {
    @ApiModelProperty("改价活动id")
    private Long activityId;
    @ApiModelProperty("备注")
    private String remark;
}
