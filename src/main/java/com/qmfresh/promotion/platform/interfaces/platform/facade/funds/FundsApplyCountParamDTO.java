package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 审核营销经费统计
 */
@Data
@ApiModel("审核营销经费申请单 ")
public class FundsApplyCountParamDTO {
    //时间（占位）
    @ApiModelProperty("时间")
    private  Integer date;


}
