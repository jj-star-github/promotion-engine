package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "FundsZbPayChangeNoteDTO", description = "总部支出流水")
public class FundsZbPayChangeNoteDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("活动类型")
    private Integer activityType;
    @ApiModelProperty("活动名")
    private String activityName;
    @ApiModelProperty("活动ID -1：收银机抹零")
    private Long activityId;
    @ApiModelProperty("操作人")
    private String createName;
    @ApiModelProperty("时间")
    private String createDay;
    @ApiModelProperty("使用营销费用")
    private BigDecimal fundsAmount;
}
