package com.qmfresh.promotion.platform.interfaces.platform.facade.price;

import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPriceRule;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class PromotionPriceRuleDTO implements Serializable {

    @ApiModelProperty("活动id")
    private Long activityId;
    @ApiModelProperty("门店id")
    private Integer shopId;
    @ApiModelProperty("商品Id")
    private Integer skuId;
    @ApiModelProperty("促销价")
    private BigDecimal rulePrice;
    @ApiModelProperty("促销开始时间")
    private Long beginPromotionActivityTime;
    @ApiModelProperty("活动类型：11=门店改价 12=总部改价 13=会员 14=大区")
    private Integer type;
    @ApiModelProperty("促销差值")
    private BigDecimal marketingDiffFee;
    @ApiModelProperty("原价")
    private BigDecimal originalPrice;
    @ApiModelProperty("改价类型1=差值 2=折扣 3=目标值")
    private Integer cpType;

    public static PromotionPriceRuleDTO create(PromotionPriceRule generalPromotion, BigDecimal originalPrice) {
        PromotionPriceRuleDTO promotionPriceRuleDTO = new PromotionPriceRuleDTO();
        promotionPriceRuleDTO.setActivityId(generalPromotion.getActivityId());
        promotionPriceRuleDTO.setShopId(generalPromotion.getShopId());
        promotionPriceRuleDTO.setSkuId(generalPromotion.getSkuId());
        promotionPriceRuleDTO.setRulePrice(generalPromotion.getRulePrice());
        promotionPriceRuleDTO.setBeginPromotionActivityTime(generalPromotion.getPlanStartCpActivityTime());
        promotionPriceRuleDTO.setType(generalPromotion.getCpBizType().getCode());
        promotionPriceRuleDTO.setMarketingDiffFee(generalPromotion.getMarketingDiffFee());
        promotionPriceRuleDTO.setOriginalPrice(originalPrice);
        promotionPriceRuleDTO.setCpType(generalPromotion.getCpType());

        return promotionPriceRuleDTO;
    }
}
