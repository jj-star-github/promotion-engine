package com.qmfresh.promotion.platform.interfaces.platform.facade.funds.service;


import com.qmfresh.promotion.dto.FundsApplyCheckParamDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.FundsApplyDTO;
import com.qmfresh.promotion.bean.promotion.FundsApplyCheckRecordDto;
import com.qmfresh.promotion.bean.promotion.FundsApplyCountDto;
import com.qmfresh.promotion.bean.promotion.FundsApplyRecordDto;
import com.qmfresh.promotion.bean.promotion.PageQueryFundsApplyParamDTO;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.CheckFundsApplyBillParamDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.FundsApplyCountParamDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.PageQueryFundsApplyCheckParamDTO;

/**
 * 营销经费申请服务
 */
public interface IFundsApplyService {

    /**
     * 经费申请
     *     * @param fundsApplyParam
     */
    Boolean createFundsApplyBill (FundsApplyDTO fundsApplyParam);

    /**
     * @Author zbr
     * @Description 申请营销经费
     * @Date 9:43 2020/9/12
     */
    Boolean createApply(FundsApplyCheckParamDTO param);

    /**
     * 分页查询经费申请记录
     *     * @param param
     */
    PageDTO<FundsApplyRecordDto> pageQueryFundsApplyRecord(PageQueryFundsApplyParamDTO param);

    /**
     * 分页查询经费申请记录
     *     * @param param
     */
    PageDTO<FundsApplyCheckRecordDto> pageQueryFundsApplyCheckRecord(PageQueryFundsApplyCheckParamDTO param);

    PageDTO<FundsApplyCheckRecordDto> fundsAccountList(PageQueryFundsApplyCheckParamDTO param);

    Boolean checkFundsApplyBillStatus(CheckFundsApplyBillParamDTO param);

    FundsApplyCountDto queryCheckApplyCount(FundsApplyCountParamDTO param);

    /**
     * APP端查看是否有正在审批中的经费申请单
     * @param param
     * @return
     */
    Boolean hasFundsApplayStatus(PageQueryFundsApplyParamDTO param);

}
