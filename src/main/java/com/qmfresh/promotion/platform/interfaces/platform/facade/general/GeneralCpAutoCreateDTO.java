package com.qmfresh.promotion.platform.interfaces.platform.facade.general;

import com.qmfresh.promotion.platform.domain.model.activity.cp.CpItem;
import com.qmfresh.promotion.platform.domain.model.activity.cp.SkuCpItem;
import com.qmfresh.promotion.platform.interfaces.platform.facade.CpItemDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.SkuCpDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.collections.CollectionUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
public class GeneralCpAutoCreateDTO implements Serializable {

    @ApiModelProperty("业务编码")
    private String bizCode;

    @ApiModelProperty("业务名称")
    private String bizName;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("操作人")
    private String operatorName;
    @ApiModelProperty("操作人id")
    private Integer operatorId;

    @ApiModelProperty("改价列表")
    private List<CpItemDTO> cpItemList;

    @ApiModelProperty("计划开始时间")
    private Long planCpActivityStartTime;
    @ApiModelProperty("计划结束时间")
    private Long planCpActivityEndTime;


    public static List<CpItem> convert(List<CpItemDTO> cpItemDTOS) {
        if (CollectionUtils.isEmpty(cpItemDTOS)) {
            return new ArrayList<>();
        }
        return cpItemDTOS.stream().map(CpItemDTO::create).collect(Collectors.toList());
    }

}
