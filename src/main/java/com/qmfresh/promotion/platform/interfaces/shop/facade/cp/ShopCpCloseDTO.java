package com.qmfresh.promotion.platform.interfaces.shop.facade.cp;

import com.qmfresh.promotion.platform.interfaces.common.AppBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Data
@ApiModel("门店改价关闭对象")
public class ShopCpCloseDTO extends AppBaseDTO {
    @ApiModelProperty("改价活动明细id")
    private Long activityCpId;

    @ApiModelProperty("门店id")
    private Integer shopId;
}
