package com.qmfresh.promotion.platform.interfaces.shop.facade.cp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel("门店调价后影响的信息")
public class ShopDiffPrice implements Serializable {
    @ApiModelProperty("调价幅度")
    private BigDecimal diffPrice;
    @ApiModelProperty("营销费")
    private BigDecimal proPrice;
    @ApiModelProperty("该品一天使用经费")
    private BigDecimal useProPrice;
    @ApiModelProperty("改价是否高于原价 返回字符串")
    private String diffTips;
}
