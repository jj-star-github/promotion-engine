package com.qmfresh.promotion.platform.interfaces.shop.facade.funds;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "ShopFundsDayStatisDTO", description = "门店费用统计")
public class ShopFundsDayStatisDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("结算日期")
    private String timeDay;
    @ApiModelProperty("使用营销费用")
    private BigDecimal useAmount;
    @ApiModelProperty("新增营销费用")
    private BigDecimal newAmount;
    @ApiModelProperty("申请费用")
    private BigDecimal applyAmount = new BigDecimal("0");
    @ApiModelProperty("销售去化奖励")
    private WeekClear weekClear;
}
