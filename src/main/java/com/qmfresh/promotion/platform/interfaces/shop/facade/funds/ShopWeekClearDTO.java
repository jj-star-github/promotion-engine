package com.qmfresh.promotion.platform.interfaces.shop.facade.funds;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value = "ShopWeekClearDTO", description = "查询门店去化奖励详情")
public class ShopWeekClearDTO extends BaseDTO {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("结算日期:yyyy-mm-dd")
    @NotNull(message = "查询时间不可为空")
    private String timeDay;
}
