package com.qmfresh.promotion.platform.interfaces.platform.facade;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPrice;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.PromotionPriceCompute;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.command.CpActivitySpec;
import com.qmfresh.promotion.platform.domain.model.activity.cp.price.command.PromotionPriceCommand;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.ChangePriceActivityMapper;
import com.qmfresh.promotion.platform.interfaces.platform.facade.price.PriceQueryDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.price.PromotionPriceDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@Component
@Slf4j
public class PromotionPriceFacadeService {

    @Resource
    private PromotionPriceCompute promotionPriceCompute;
    @Resource
    private ChangePriceActivityMapper changePriceActivityMapper;

    public PromotionPriceDTO compute(PriceQueryDTO queryDTO) {
        log.info("原价变动计算当前促销价 param={}", JSON.toJSONString(queryDTO));

        Integer shopId = queryDTO.getCurrentShopId();
        Integer skuId = queryDTO.getSkuId();
        CpActivitySpec spec = CpActivitySpec.spec(changePriceActivityMapper, shopId, skuId);
        //计算促销价
        PromotionPriceCommand priceCommand = PromotionPriceCommand
                .create(shopId, skuId, queryDTO.getOriginalPrice(), spec);
        PromotionPrice promotionPrice = promotionPriceCompute.compute(priceCommand);

        return PromotionPriceDTO.create(promotionPrice, queryDTO.getOriginalPrice());
    }

    public PromotionPriceDTO computeWithoutSnapshot(PriceQueryDTO queryDTO) {
        log.info("原价变动计算当前促销价 param={}", JSON.toJSONString(queryDTO));
        PromotionPrice promotionPrice = promotionPriceCompute.compute(
                queryDTO.getOriginalPrice(), queryDTO.getCurrentShopId(), queryDTO.getSkuId(), false,
                "修改原价"
        );
        return PromotionPriceDTO.create(promotionPrice, queryDTO.getOriginalPrice());
    }
}
