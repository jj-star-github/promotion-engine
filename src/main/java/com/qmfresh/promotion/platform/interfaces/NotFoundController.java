package com.qmfresh.promotion.platform.interfaces;

import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author xzw
 * @date ${date}  mailto 741342093@qq.com
 */
@RestController
public class NotFoundController implements ErrorController {
    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping(value = {"/error"})
    public ServiceResultDTO<String> error(HttpServletRequest request) {
        return ServiceResultDTO.fail(4004,"页面不存在");
    }

}
