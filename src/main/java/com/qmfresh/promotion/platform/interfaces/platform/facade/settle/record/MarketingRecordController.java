package com.qmfresh.promotion.platform.interfaces.platform.facade.settle.record;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.platform.interfaces.common.ServiceResultDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.record.dto.IncomeRecord;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.record.dto.MarketingRecordReqDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.record.dto.UseRecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/***
 * @Author zbr
 * @Description 营销费用记录
 * @Date 19:21 2020/8/18
 */
@RestController
@RequestMapping(value = "/marketingRecord")
@Slf4j
@Api(description = "门店营销费用记录")
public class MarketingRecordController {
    @ApiOperation(value = "查询收入记录", notes = "查询收入记录")
    @RequestMapping(value = "/queryIncomeRecord", method = RequestMethod.POST)
    public ServiceResultDTO<ListWithPage<IncomeRecord>> queryIncomeRecord(@RequestBody MarketingRecordReqDTO param) {
        log.info("into method queryIncomeRecord,入参:" + JSON.toJSONString(param));
        return ServiceResultDTO.success(null);
    }

    @ApiOperation(value = "查询使用记录", notes = "查询使用记录")
    @RequestMapping(value = "/queryUseRecord", method = RequestMethod.POST)
    public ServiceResultDTO<ListWithPage<UseRecord>> queryUseRecord(@RequestBody MarketingRecordReqDTO param) {
        log.info("into method queryUseRecord,入参:" + JSON.toJSONString(param));
        return ServiceResultDTO.success(null);
    }
}
