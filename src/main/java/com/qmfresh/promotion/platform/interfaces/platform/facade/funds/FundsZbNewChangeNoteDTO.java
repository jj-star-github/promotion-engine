package com.qmfresh.promotion.platform.interfaces.platform.facade.funds;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "FundsZbNewChangeNoteDTO", description = "总部收入流水")
public class FundsZbNewChangeNoteDTO implements Serializable {
    private static final long serialVersionUID = 7812556364757470645L;
    @ApiModelProperty("账号")
    private Long accountId;
    @ApiModelProperty("门店ID")
    private Integer shopId;
    @ApiModelProperty("门店名")
    private String shopName;
    @ApiModelProperty("总营销费用")
    private BigDecimal fundsAmount;
    @ApiModelProperty("总部营销费用")
    private BigDecimal zbFundsAmount;
    @ApiModelProperty("门店营销费用")
    private BigDecimal shopFundsAmount;
    @ApiModelProperty("大区营销费用")
    private BigDecimal areaFundsAmount;
    @ApiModelProperty("时间")
    private String createDay;
}
