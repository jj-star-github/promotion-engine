package com.qmfresh.promotion.platform.application;

import com.qmfresh.promotion.platform.interfaces.platform.facade.account.AccountCreateDTO;

/**
 * 后台营销账户
 */
public interface PlatformAccountService {
    /**
     * 创建账户信息
     * @return
     */
    Boolean createAccount(AccountCreateDTO accountCreateDTO);
    /**
     * 修改账户信息
     * @return
     */
    Boolean updataAccount(AccountCreateDTO accountCreateDTO);
}
