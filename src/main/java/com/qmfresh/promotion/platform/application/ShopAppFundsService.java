package com.qmfresh.promotion.platform.application;

import com.qmfresh.promotion.platform.interfaces.shop.facade.funds.*;

import java.text.ParseException;
import java.util.List;

/**
 * 门店营销费用
 */
public interface ShopAppFundsService {

    /**
     * 查询账户今日数据
     * @param shopId 门店Id
     * @return
     */
    ShopFundsAccountDTO queryAccount(Integer shopId);

    /**
     * 门店日统计
     * @param shopId   门店ID
     * @param timeDay  日期 YYYY-MM-dd
     * @return
     */
    List<ShopFundsDayStatisDTO> queryFundsDayStatis(Integer shopId, String timeDay, Integer isShowApply);

    /**
     * 查询营销流水
     * @param dto
     * @return
     */
    List<ShopFundsChangeDTO> businessChange(ShopFundsChangeQueryDTO dto);

    /**
     * 查询费用使用类型
     * @param shopId
     * @param timeDay
     * @return
     */
    List<ShopFundsUseTypeDTO> queryUseType(Integer shopId, String timeDay);

    /**
     * 查询门店去化奖励详情
     * @param param
     * @return
     */
    ShopWeekClearDetailDTO queryWeekClear(ShopWeekClearDTO param);

}
