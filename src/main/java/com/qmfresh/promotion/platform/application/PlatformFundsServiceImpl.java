package com.qmfresh.promotion.platform.application;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.entity.AreaConditionQuery;
import com.qmfresh.promotion.entity.BaseArea;
import com.qmfresh.promotion.entity.ShopDetail;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.platform.domain.model.funds.*;
import com.qmfresh.promotion.platform.domain.model.funds.enums.FundsBusinessTypeEnum;
import com.qmfresh.promotion.platform.domain.model.funds.enums.FundsShopTypeEnum;
import com.qmfresh.promotion.platform.domain.model.settle.apportion.ApportionManager;
import com.qmfresh.promotion.platform.domain.shared.BigDecimalUtil;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsSkuVO;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsWeekClearVO;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.ChangePriceActivityMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.ChangePriceActivityPO;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.FundsAccountMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.*;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.ShopPromotionRule;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 */
@Component
@Slf4j
public class PlatformFundsServiceImpl implements PlatformFundsService {

    @Resource
    private FundsAccountManager fundsAccountManager;
    @Resource
    private FundsSettleInfoManager fundsSettleInfoManager;
    @Resource
    private FundsChangeNoteManager fundsChangeNoteManager;
    @Resource
    private FundsLockChangeManager fundsLockChangeManager;
    @Resource
    private ChangePriceActivityMapper changePriceActivityManage;
    @Resource
    private IQmExternalApiService iQmExternalApiService;
    @Resource
    private ApportionManager apportionManager;
    @Resource
    private FundsAccountMapper fundsAccountMapper;

    @Override
    public List<ShopFundsAdminAccountDTO> queryAccount() {
        List<ShopFundsAdminAccountDTO> accountDTOS = new ArrayList<>();
        LambdaQueryWrapper<FundsAccount> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(FundsAccount::getShopType, 4);
        wrapper.eq(FundsAccount::getIsDeleted, 0);
        List<FundsAccount> fundsAccounts = fundsAccountManager.list(wrapper);
        if (CollectionUtils.isEmpty(fundsAccounts)) {
            return accountDTOS;
        }
        fundsAccounts.forEach(
                fundsAccount -> {
                    ShopFundsAdminAccountDTO accountDTO = new ShopFundsAdminAccountDTO(fundsAccount);
                    accountDTOS.add(accountDTO);
                }
        );
        return accountDTOS;
    }

    @Override
    public ShopFundsAdminAccountDTO queryAccount(Integer shopId, Integer accountType) {
        FundsAccount account = fundsAccountManager.byShopIdAndType(shopId, accountType);
        ShopFundsAdminAccountDTO accountDTO = new ShopFundsAdminAccountDTO();
        accountDTO.setTotalAmount(BigDecimalUtil.round(account.getTotalAmount()));
        return accountDTO;
    }

    @Override
    public ShopFundsAdminTargetStatisDTO statis() {
        ShopFundsAdminTargetStatisDTO dto = new ShopFundsAdminTargetStatisDTO();
        //查询今日使用营销费用
        FundsSettleInfo dayUseSum = fundsSettleInfoManager.queryDaySum(DateUtil.formatDate());
        if (null != dayUseSum) {
            dto.setTodayShopUseAmount(BigDecimalUtil.round(dayUseSum.getShopUseAmount()));
            dto.setTodayAreaUseAmount(BigDecimalUtil.round(dayUseSum.getAreaUseAmount()));
            dto.setTodayUseAmount(BigDecimalUtil.sub(dayUseSum.getUseAmount().subtract(dayUseSum.getAreaUseAmount()), dayUseSum.getShopUseAmount()));
        }
        //今日锁定营销费用
        List<FundsLockChange> lockDaySum = fundsLockChangeManager.queryDaySumSupportShopType(DateUtil.formatDate());
        if (CollectionUtils.isNotEmpty(lockDaySum)) {
            lockDaySum.forEach(
                    lockDay->{
                        if (FundsShopTypeEnum.ZB.getCode() == lockDay.getShopType()) {
                            dto.setTodayLock(BigDecimalUtil.round(lockDay.getChangeAmount()));
                        }else if (FundsShopTypeEnum.ZB.getCode() == lockDay.getShopType()) {
                            dto.setShopTodayLock(BigDecimalUtil.round(lockDay.getChangeAmount()));
                        }
                    }
            );
        }

        //查询昨日新增营销费用
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        FundsSettleInfo yesterdayNewSum = fundsSettleInfoManager.queryDaySum(sdf.format(DateUtil.getDateBefore(1)));
        if (null != yesterdayNewSum) {
            dto.setYesterdayShopNewAmount(BigDecimalUtil.round(yesterdayNewSum.getShopFundsAmount()));
            dto.setYesterdayAreaNewAmount(BigDecimalUtil.round(yesterdayNewSum.getAreaFundsAmount()));
            dto.setYesterdayNewAmount(BigDecimalUtil.sub(yesterdayNewSum.getFundsAmount().subtract(yesterdayNewSum.getAreaFundsAmount()), yesterdayNewSum.getShopFundsAmount()));
        }

        //查询总部账户信息
        FundsAccountSum zbAccountSum = fundsAccountManager.queryAccountSum(FundsShopTypeEnum.ZB.getCode());
        if (null != zbAccountSum) {
            dto.setAmount(BigDecimalUtil.round(zbAccountSum.getSumAmount()));
            dto.setLockAmount(BigDecimalUtil.round(zbAccountSum.getSumLockAmount().negate()));
        }
        //查询门店账户信息（所有门店）
        FundsAccountSum mdAccountSum = fundsAccountManager.queryAccountSum(FundsShopTypeEnum.MD.getCode());
        if (null != mdAccountSum) {
            dto.setShopAmount(BigDecimalUtil.round(mdAccountSum.getSumAmount()));
            dto.setShopLockAmount(BigDecimalUtil.round(mdAccountSum.getSumLockAmount().negate()));
        }
        //查询门店账户信息（所有大区）
        FundsAccountSum dqAccountSum = fundsAccountManager.queryAccountSum(FundsShopTypeEnum.DQ.getCode());
        if (null != dqAccountSum) {
            dto.setAreaAmount(BigDecimalUtil.round(dqAccountSum.getSumAmount()));
            dto.setAreaLockAmount(BigDecimalUtil.round(dqAccountSum.getSumLockAmount().negate()));
        }
        return dto;
    }

    @Override
    public ShopFundsAdminTargetStatisDTO querySumAccount() {
        ShopFundsAdminTargetStatisDTO dto = new ShopFundsAdminTargetStatisDTO();
        //查询总部账户信息
        FundsAccountSum zbAccountSum = fundsAccountManager.queryAccountSum(FundsShopTypeEnum.ZB.getCode());
        if (null != zbAccountSum) {
            dto.setAmount(BigDecimalUtil.round(zbAccountSum.getSumAmount()));
            dto.setLockAmount(BigDecimalUtil.round(zbAccountSum.getSumLockAmount().negate()));
        }
        //查询门店账户信息（所有门店）
        FundsAccountSum mdAccountSum = fundsAccountManager.queryAccountSum(FundsShopTypeEnum.MD.getCode());
        if (null != mdAccountSum) {
            dto.setShopAmount(BigDecimalUtil.round(mdAccountSum.getSumAmount()));
            dto.setShopLockAmount(BigDecimalUtil.round(mdAccountSum.getSumLockAmount().negate()));
        }
        //查询门店账户信息（所有大区）
        FundsAccountSum dqAccountSum = fundsAccountManager.queryAccountSum(FundsShopTypeEnum.DQ.getCode());
        if (null != dqAccountSum) {
            dto.setAreaAmount(BigDecimalUtil.round(dqAccountSum.getSumAmount()));
            dto.setAreaLockAmount(BigDecimalUtil.round(dqAccountSum.getSumLockAmount().negate()));
        }
        return dto;
    }

    @Override
    public List<ShopFundsAdminTargetDTO> target(ShopFundsAdminStatisQueryDTO queryDTO) {
        List<ShopFundsAdminTargetDTO> targetDTOS = new ArrayList<>();
        List<FundsSettleInfo> settleInfoList = fundsSettleInfoManager.queryDayTarget(queryDTO.getBeginTime(), queryDTO.getEndTime(), queryDTO.getAccountType(), queryDTO.getQueryShopId(), null ,null, null);
        if (CollectionUtils.isEmpty(settleInfoList)) {
            return targetDTOS;
        }
        settleInfoList.forEach(
                settleInfo -> {
                    ShopFundsAdminTargetDTO dto = new ShopFundsAdminTargetDTO();
                    BeanUtils.copyProperties(settleInfo, dto);
                    targetDTOS.add(dto);
                }
        );
        Collections.sort(targetDTOS, (o1, o2) ->  DateUtil.parseDate(o1.getCreateDay(), DateUtil.YMD).before(DateUtil.parseDate(o2.getCreateDay(), DateUtil.YMD)) ? -1 : 1);
        return targetDTOS;
    }

    @Override
    public ShopFundsAdminTargetStatisDTO dayStatis() {
        ShopFundsAdminTargetStatisDTO dto = new ShopFundsAdminTargetStatisDTO();
        //查询今日使用营销费用
        FundsSettleInfo daySum = fundsSettleInfoManager.queryDaySum(DateUtil.formatDate());
        if (null != daySum) {
            dto.setTodayRealPay(BigDecimalUtil.round(daySum.getRealPay()));
            dto.setTodayMarketingPay(BigDecimalUtil.round(daySum.getMarketingPay()));
            dto.setTodayFundAmount(BigDecimalUtil.round(daySum.getFundsAmount())); //总和加-总和使用
            dto.setTodayShopFundAmount(BigDecimalUtil.round(daySum.getShopFundsAmount())); //门店加-门店减
            dto.setTodayAreaFundAmount(BigDecimalUtil.round(daySum.getAreaFundsAmount())); //大区今日新增营销费
            dto.setTodayZBFundAmount(BigDecimalUtil.sub(daySum.getFundsAmount().subtract(daySum.getAreaFundsAmount()), daySum.getShopFundsAmount())); //总部营销费用
            dto.setTodayShopUseAmount(BigDecimalUtil.round(daySum.getShopUseAmount())); //门店使用费用
            dto.setTodayAreaUseAmount(BigDecimalUtil.round(daySum.getAreaUseAmount())); //大区使用
            dto.setTodayUseAmount(BigDecimalUtil.sub(daySum.getUseAmount().subtract(daySum.getAreaUseAmount()), daySum.getShopUseAmount())); //总部使用费用
        }

        //查询总部账户信息
        FundsAccountSum zbAccountSum = fundsAccountManager.queryAccountSum(FundsShopTypeEnum.ZB.getCode());
        if (null != zbAccountSum) {
            dto.setAmount(BigDecimalUtil.round(zbAccountSum.getSumAmount()));
            dto.setLockAmount(BigDecimalUtil.round(zbAccountSum.getSumLockAmount().negate()));
        }
        //查询门店账户信息（所有门店）
        FundsAccountSum mdAccountSum = fundsAccountManager.queryAccountSum(FundsShopTypeEnum.MD.getCode());
        if (null != mdAccountSum) {
            dto.setShopAmount(BigDecimalUtil.round(mdAccountSum.getSumAmount()));
            dto.setShopLockAmount(BigDecimalUtil.round(mdAccountSum.getSumLockAmount()).negate());
        }

        //查询门店账户信息（所有门店）
        FundsAccountSum dqAccountSum = fundsAccountManager.queryAccountSum(FundsShopTypeEnum.DQ.getCode());
        if (null != dqAccountSum) {
            dto.setAreaAmount(BigDecimalUtil.round(dqAccountSum.getSumAmount()));
            dto.setAreaLockAmount(BigDecimalUtil.round(dqAccountSum.getSumLockAmount()).negate());
        }
        return dto;
    }

    @Override
    public PageDTO<ShopFundsPayChangeNoteDTO> shopPayChangeNote(ShopFundsAdminChangeNoteQueryDTO param) {
        PageDTO<ShopFundsPayChangeNoteDTO> pageDTO = new PageDTO<>();
        Integer totalCount = fundsChangeNoteManager.queryPayNoteCount(param.getAccountId(), param.getCreateDay(), null, null);
        if ( null == totalCount || 0 >= totalCount) {
            return pageDTO;
        }
        List<ShopFundsPayChangeNoteDTO> payChangeNoteDTOS = new ArrayList<>();
        List<FundsChangeNote> changeNoteList = fundsChangeNoteManager.queryPayNote(param.getAccountId(), param.getCreateDay(), null, null, param.getStart(), param.getPageSize());
        if (CollectionUtils.isNotEmpty(changeNoteList)) {
            //获取所有活动Id
            List<Long> activityIds = changeNoteList.stream().map(FundsChangeNote::getActivityId).collect(Collectors.toList());
            List<ChangePriceActivityPO> activityPOS = changePriceActivityManage.selectList(new LambdaQueryWrapper<ChangePriceActivityPO>()
                    .in(ChangePriceActivityPO::getActivityId, activityIds));
            Map<Long, ChangePriceActivityPO> activityPOMap = CollectionUtils.isEmpty(activityPOS) ? new HashMap<>() : activityPOS.stream().collect(Collectors.groupingBy(ChangePriceActivityPO::getActivityId,Collectors.collectingAndThen(Collectors.toList(),value->value.get(0))));
            changeNoteList.forEach(
                    changeNote -> {
                        ShopFundsPayChangeNoteDTO changeNoteDTO = new ShopFundsPayChangeNoteDTO();
                        changeNoteDTO.setChangeAmount(BigDecimalUtil.round(changeNote.getChangeAmount()));
                        changeNoteDTO.setGmtCreate(changeNote.getGmtCreate());
                        changeNoteDTO.setOrderCode(changeNote.getBusinessCode());
                        changeNoteDTO.setBusinessType(changeNote.getBusinessType());
                        if (FundsBusinessTypeEnum.SKU_MARKETING.getCode() == changeNote.getBusinessType()) {
                            FundsSkuVO skuVo = JSON.parseObject(changeNote.getRemark(), new TypeReference<FundsSkuVO>() {
                            });
                            changeNoteDTO.setSkuId(skuVo.getSkuId());
                            changeNoteDTO.setSkuName(skuVo.getSkuName());
                            changeNoteDTO.setCreateUserName(activityPOMap.containsKey(changeNote.getActivityId()) ? activityPOMap.get(changeNote.getActivityId()).getCreatePersonName() : "系统");
                        } else {
                            changeNoteDTO.setSkuName(changeNote.getRemark());
                        }
                        payChangeNoteDTOS.add(changeNoteDTO);
                    }
            );
        }
        pageDTO.setTotalCount(totalCount); //总行数
        pageDTO.setRecords(payChangeNoteDTOS); //数据信息
        return pageDTO;
    }

    @Override
    public PageDTO<ShopFundsNewChangeNoteDTO> shopNewChangeNote(ShopFundsAdminChangeNoteQueryDTO param) {
        PageDTO<ShopFundsNewChangeNoteDTO> pageDTO = new PageDTO<>();
        Integer totalCount = fundsSettleInfoManager.queryDayCount(param.getAccountId(), param.getCreateDay());
        if (null == totalCount || 0 >= totalCount) {
            return pageDTO;
        }
        pageDTO.setTotalCount(totalCount); //总行数
        List<ShopFundsNewChangeNoteDTO> changeNoteDTOS = new ArrayList<>();
        List<FundsSettleInfo> settleInfoList = fundsSettleInfoManager.queryDayNote(param.getAccountId(), param.getCreateDay(), param.getStart(), param.getPageSize());
        if (CollectionUtils.isNotEmpty(settleInfoList)) {
            settleInfoList.forEach(
                    settleInfo -> {
                        ShopFundsNewChangeNoteDTO changeNoteDTO = new ShopFundsNewChangeNoteDTO();
                        changeNoteDTO.setGmtCreate(settleInfo.getGmtCreate());
                        changeNoteDTO.setOrderCode(settleInfo.getOrderCode());
                        changeNoteDTO.setRealPay(BigDecimalUtil.round(settleInfo.getRealPay()));
                        changeNoteDTO.setFundsAmount(BigDecimalUtil.round(settleInfo.getFundsAmount())); //总营销收入
                        changeNoteDTO.setShopFundsAmount(BigDecimalUtil.round(settleInfo.getShopFundsAmount())); //门店的营销收入
                        changeNoteDTO.setAreaFundsAmount(BigDecimalUtil.round(settleInfo.getAreaFundsAmount()));
                        changeNoteDTO.setZbFundsAmount(BigDecimalUtil.sub(settleInfo.getFundsAmount().subtract(settleInfo.getAreaFundsAmount()), changeNoteDTO.getShopFundsAmount())); //总部的营销收入
                        changeNoteDTOS.add(changeNoteDTO);
                    }
            );
        }
        pageDTO.setRecords(changeNoteDTOS); //数据信息
        return pageDTO;
    }

    @Override
    public PageDTO<FundsZbPayChangeNoteDTO> zbPayChangeNote(ShopFundsAdminChangeNoteQueryDTO param) {
        PageDTO<FundsZbPayChangeNoteDTO>  pageDTO = new PageDTO<>();
        Integer totalCount = fundsChangeNoteManager.queryZbPayStatisCount(param.getAccountId(), param.getCreateDay());
        if (null == totalCount || 0 >= totalCount) {
            return pageDTO;
        }
        pageDTO.setTotalCount(totalCount);
        List<FundsZbPayChangeNoteDTO> changeNoteDTOS = new ArrayList<>();
        List<FundsChangeNote> changeNoteList = fundsChangeNoteManager.queryZbPayStatis(param.getAccountId(), param.getCreateDay(), param.getStart(), param.getPageSize());
        if (CollectionUtils.isNotEmpty(changeNoteList)) {
            //查询活动信息
            List<Long> activityIds = changeNoteList.stream().map(FundsChangeNote::getActivityId).collect(Collectors.toList());
            List<ChangePriceActivityPO> activityPOS = changePriceActivityManage.selectList(new LambdaQueryWrapper<ChangePriceActivityPO>()
                    .in(ChangePriceActivityPO::getActivityId, activityIds));
            Map<Long, ChangePriceActivityPO> activityPOMap = CollectionUtils.isEmpty(activityPOS) ? new HashMap<>() : activityPOS.stream().collect(Collectors.groupingBy(ChangePriceActivityPO::getActivityId,Collectors.collectingAndThen(Collectors.toList(),value->value.get(0))));
            changeNoteList.forEach(
                    changeNote -> {
                        FundsZbPayChangeNoteDTO changeNoteDTO = new FundsZbPayChangeNoteDTO();
                        changeNoteDTO.setActivityId(changeNote.getActivityId());
                        changeNoteDTO.setFundsAmount(BigDecimalUtil.round(changeNote.getChangeAmount()));
                        changeNoteDTO.setCreateDay(param.getCreateDay());
                        changeNoteDTO.setActivityType(activityPOMap.containsKey(changeNote.getActivityId()) ? activityPOMap.get(changeNote.getActivityId()).getType() : -1);
                        changeNoteDTO.setActivityName(activityPOMap.containsKey(changeNote.getActivityId()) ? activityPOMap.get(changeNote.getActivityId()).getName() : "");
                        changeNoteDTOS.add(changeNoteDTO);
                    }
            );
        }
        pageDTO.setRecords(changeNoteDTOS);
        return pageDTO;
    }

    @Override
    public PageDTO<FundsZbNewChangeNoteDTO> zbNewChangeNote(ShopFundsAdminChangeNoteQueryDTO param) {
        PageDTO<FundsZbNewChangeNoteDTO>  pageDTO = new PageDTO<>();
        Integer totalCount = fundsSettleInfoManager.queryAccountStatisCount(param.getCreateDay());
        if (null == totalCount || 0 >= totalCount) {
            return pageDTO;
        }
        pageDTO.setTotalCount(totalCount);
        List<FundsZbNewChangeNoteDTO> changeNoteDTOS = new ArrayList<>();
        List<FundsSettleInfo> settleInfoList = fundsSettleInfoManager.queryAccountStatis(param.getCreateDay(), param.getStart(), param.getPageSize());
        if (CollectionUtils.isNotEmpty(settleInfoList)) {
            settleInfoList.forEach(
                    settleInfo -> {
                        FundsZbNewChangeNoteDTO changeNoteDTO = new FundsZbNewChangeNoteDTO();
                        FundsAccount account = fundsAccountManager.byId(settleInfo.getAccountId());
                        if (null != account) {
                            changeNoteDTO.setAccountId(account.getId());
                            changeNoteDTO.setShopId(account.getShopId());
                            changeNoteDTO.setShopName(account.getShopName());
                            changeNoteDTO.setFundsAmount(BigDecimalUtil.round(settleInfo.getFundsAmount()));
                            changeNoteDTO.setShopFundsAmount(BigDecimalUtil.round(settleInfo.getShopFundsAmount()));
                            changeNoteDTO.setAreaFundsAmount(BigDecimalUtil.round(settleInfo.getAreaFundsAmount()));
                            changeNoteDTO.setZbFundsAmount(BigDecimalUtil.sub(changeNoteDTO.getFundsAmount().subtract(settleInfo.getAreaFundsAmount()), changeNoteDTO.getShopFundsAmount()));
                            changeNoteDTO.setCreateDay(param.getCreateDay());
                            changeNoteDTOS.add(changeNoteDTO);
                        }
                    }
            );
        }
        pageDTO.setRecords(changeNoteDTOS);
        return pageDTO;
    }

    @Override
    public PageDTO<ShopFundsAdminDayDTO> dayChange(ShopFundsAdminStatisQueryDTO queryDTO) {

        PageDTO<ShopFundsAdminDayDTO> pageDTO = new PageDTO<>();
        FundsAccount account = null;
        Long accountId = null;
        if (null != queryDTO.getQueryShopId()) {
            account = fundsAccountManager.byShopIdAndType(queryDTO.getQueryShopId(), FundsShopTypeEnum.MD.getCode());
            if (null == account.getId()) {
                return pageDTO;
            }
            accountId = account.getId();
        }

        //查询总数 batchQueryDayTarget
        Integer totalCount = fundsSettleInfoManager.batchQueryDayTargetCount(queryDTO.getBeginTime(), queryDTO.getEndTime(), null, null, accountId);
        if (null == totalCount || 0 >= totalCount) {
            return pageDTO;
        }
        pageDTO.setTotalCount(totalCount);
        List<ShopFundsAdminDayDTO> adminDayDTOS = new ArrayList<>();
        //查询明细
        List<FundsSettleInfo> settleInfoList = fundsSettleInfoManager.batchQueryDayTarget(queryDTO.getBeginTime(), queryDTO.getEndTime(), null, null, accountId, queryDTO.getStart(), queryDTO.getPageSize());
        if (CollectionUtils.isEmpty(settleInfoList)) {
            return pageDTO;
        }
        //查询账户信息
        Map<Long, FundsAccount> changeMap = new HashMap<>();
        List<Long>  accountIdS = new ArrayList<>();
        if (null == account) {
            accountIdS = settleInfoList.stream().map(FundsSettleInfo::getAccountId).collect(Collectors.toList());
            //查询账号信息
            List<FundsAccount> accountList = fundsAccountManager.listByIds(accountIdS);
            changeMap = CollectionUtils.isEmpty(accountList) ? new HashMap<>() : accountList.stream().collect(Collectors.groupingBy(FundsAccount::getId,Collectors.collectingAndThen(Collectors.toList(),value->value.get(0))));
        } else {
            changeMap.put(account.getId(), account);
            accountIdS.add(account.getId());
        }
        //查询锁定营销费用
        List<FundsLockChange> lockChangeList = fundsLockChangeManager.queryDaySumSupportAccount(accountIdS, queryDTO.getBeginTime(), queryDTO.getEndTime());
        Map<Long, List<FundsLockChange>> lockChangeMap = CollectionUtils.isEmpty(lockChangeList) ? new HashMap<>() : lockChangeList.stream().collect(Collectors.groupingBy(FundsLockChange::getAccountId));

        for(FundsSettleInfo settleInfo : settleInfoList) {
            ShopFundsAdminDayDTO dayDTO = new ShopFundsAdminDayDTO();
            Long eachAccountId = settleInfo.getAccountId();
            dayDTO.setShopName(changeMap.containsKey(eachAccountId) ? changeMap.get(eachAccountId).getShopName() : "");
            dayDTO.setAccountId(eachAccountId);
            dayDTO.setShopId(settleInfo.getShopId());
            dayDTO.setAccountType(changeMap.containsKey(eachAccountId) ? changeMap.get(eachAccountId).getShopType() : 0);
            dayDTO.setCreateDay(settleInfo.getCreateDay());
            dayDTO.setNewAmount(BigDecimalUtil.round(settleInfo.getShopFundsAmount()));
            dayDTO.setUseAmount(BigDecimalUtil.round(settleInfo.getShopUseAmount()));
            dayDTO.setAmount(BigDecimalUtil.sub(settleInfo.getShopFundsAmount(), (settleInfo.getShopUseAmount())));
            //日锁定
            //处理锁定数据
            if (lockChangeMap.containsKey(eachAccountId)) {
                List<FundsLockChange> lockList = lockChangeMap.get(eachAccountId);
                lockList.forEach(lock->{
                    if(lock.getCreateDay().equals(dayDTO.getCreateDay())) {
                        dayDTO.setLockAmount(lock.getChangeAmount());
                    }
                });
            }
            adminDayDTOS.add(dayDTO);
        }
        pageDTO.setRecords(adminDayDTOS);
        return pageDTO;
    }

    @Override
    public PageDTO<FundsZbPayChangeNoteDTO> dqPayChangeNote(ShopFundsAdminChangeNoteQueryDTO param) {
        return null;
    }

    @Override
    public PageDTO<FundsZbNewChangeNoteDTO> dqNewChangeNote(ShopFundsAdminChangeNoteQueryDTO param) {
        PageDTO<FundsZbNewChangeNoteDTO>  pageDTO = new PageDTO<>();
        //根据查询大区所对应的目标门店信息
        List<Integer> shopIdList = fundsChangeNoteManager.queryTargetShopByAccountId(param.getAccountId(), param.getCreateDay());
        if(CollectionUtils.isEmpty(shopIdList)) {
            return pageDTO;
        }
        Integer totalCount = fundsSettleInfoManager.queryCountByShopList(param.getCreateDay(), shopIdList, FundsShopTypeEnum.MD.getCode());
        if (null == totalCount || 0 >= totalCount) {
            return pageDTO;
        }
        pageDTO.setTotalCount(totalCount);
        List<FundsZbNewChangeNoteDTO> changeNoteDTOS = new ArrayList<>();
        List<FundsSettleInfo> settleInfoList = fundsSettleInfoManager.queryAccountByShopList(param.getCreateDay(), param.getStart(), param.getPageSize(), shopIdList, FundsShopTypeEnum.MD.getCode());
        if (CollectionUtils.isNotEmpty(settleInfoList)) {
            settleInfoList.forEach(
                    settleInfo -> {
                        FundsZbNewChangeNoteDTO changeNoteDTO = new FundsZbNewChangeNoteDTO();
                        FundsAccount account = fundsAccountManager.byId(settleInfo.getAccountId());
                        if (null != account) {
                            changeNoteDTO.setAccountId(account.getId());
                            changeNoteDTO.setShopId(account.getShopId());
                            changeNoteDTO.setShopName(account.getShopName());
                            changeNoteDTO.setFundsAmount(BigDecimalUtil.round(settleInfo.getFundsAmount()));
                            changeNoteDTO.setShopFundsAmount(BigDecimalUtil.round(settleInfo.getShopFundsAmount()));
                            changeNoteDTO.setAreaFundsAmount(BigDecimalUtil.round(settleInfo.getAreaFundsAmount()));
                            changeNoteDTO.setZbFundsAmount(BigDecimalUtil.sub(changeNoteDTO.getFundsAmount().subtract(settleInfo.getAreaFundsAmount()), changeNoteDTO.getShopFundsAmount()));
                            changeNoteDTO.setCreateDay(param.getCreateDay());
                            changeNoteDTOS.add(changeNoteDTO);
                        }
                    }
            );
        }
        pageDTO.setRecords(changeNoteDTOS);
        return pageDTO;
    }

    @Override
    public PageDTO<AccountDTO> queryAccountList(AccountQueryDTO param) {
        PageDTO<AccountDTO>  pageDTO = new PageDTO<>();
        List<FundsAccount> fundsAccountList = new ArrayList<>();
        List<AccountDTO> accountDTOList = new ArrayList<>();
        Integer totalCount = null;
        if(param.getShopType() != null){
            if(param.getShopType().equals(1)){
                List<Integer> shopTypes = new ArrayList<>();
                shopTypes.add(1);
                shopTypes.add(2);
                fundsAccountList = fundsAccountManager.queryAccountList(shopTypes,param.getShopName(),param.getQueryShopId(),param.getAreaName(),param.getStart(),param.getPageSize());
                totalCount = fundsAccountManager.queryAccountListCount(shopTypes,param.getShopName(),param.getQueryShopId(),param.getAreaName());
            }else if(param.getShopType().equals(4)){
                List<Integer> shopTypes = new ArrayList<>();
                shopTypes.add(4);
                totalCount = fundsAccountManager.queryAccountListCount(shopTypes,param.getShopName(),param.getQueryShopId(),param.getAreaName());
                fundsAccountList = fundsAccountManager.queryAccountList(shopTypes,param.getShopName(),param.getQueryShopId(),param.getAreaName(),param.getStart(),param.getPageSize());
            }
        }else {
            totalCount = fundsAccountManager.queryAccountListCount(null,param.getShopName(),param.getQueryShopId(),param.getAreaName());
            fundsAccountList = fundsAccountManager.queryAccountList(null,param.getShopName(),param.getQueryShopId(),param.getAreaName(),param.getStart(),param.getPageSize());
        }
        if (null == totalCount || 0 >= totalCount) {
            return pageDTO;
        }
        pageDTO.setTotalCount(totalCount);
        if (CollectionUtils.isNotEmpty(fundsAccountList)) {
            fundsAccountList.forEach(
                    fundsAccount -> {
                        AccountDTO accountDTO = new AccountDTO();
                        BeanUtils.copyProperties(fundsAccount, accountDTO);
                        BigDecimal amountPercent = fundsAccount.getTotalAmount().divide(fundsAccount.getMaxPromPrice(),2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100"));
                        accountDTO.setAmountPercent(amountPercent);
                        if(fundsAccount.getShopType().equals(2)){
                            accountDTO.setParentAccount("总部");
                        }else if(fundsAccount.getShopType().equals(4)){
                            List shopList = new ArrayList();
                            shopList.add(fundsAccount.getShopId());
                            AreaConditionQuery areaQuery = new AreaConditionQuery();
                            areaQuery.setShopIds(shopList);
                            //门店所属大区
                            List<BaseArea> areaDetail = iQmExternalApiService.selectAreaByShopIds(areaQuery);
                            if(CollectionUtils.isEmpty(areaDetail)){
                                log.warn("该门店没有所属大区，shopId={}", fundsAccount.getShopId());
                                accountDTO.setParentAccount("无");
                            }else {
                                accountDTO.setParentAccount(areaDetail.get(0).getAreaName() + "账户");
                            }
                        }else{
                            accountDTO.setParentAccount("无");
                        }
                        accountDTOList.add(accountDTO);
                    }
            );
        }
        pageDTO.setRecords(accountDTOList);
        return pageDTO;
    }

    @Override
    public PageDTO<WeekClearChangeNoteDTO> weekClearChangeNote(WeekClearChangeNoteQueryDTO param) {
        PageDTO<WeekClearChangeNoteDTO>  pageDTO = new PageDTO<>();
        String sdf = "yyyy-MM-dd HH:mm:ss";
        //查询去化奖励流水
        List<FundsChangeNote> fundsChangeNoteList= fundsChangeNoteManager.queryWeekClearChangeNote(param.getAccountId(),param.getCreateDay(),param.getStart(),param.getPageSize());
        log.info("查询去化奖励流水，fundsChangeNoteList={}", JSON.toJSONString(fundsChangeNoteList));
        Integer totalCount = fundsChangeNoteManager.countWeekClearChangeNote(param.getAccountId(),param.getCreateDay());
        if (null == totalCount || 0 >= totalCount) {
            return pageDTO;
        }
        pageDTO.setTotalCount(totalCount);
        if(param.getAccountType().equals(4)){
            ShopDetail shopInfo = iQmExternalApiService.getShopById(param.getQueryShopId());
            log.info("门店信息:" + JSON.toJSONString(shopInfo));
            //门店级别
            Integer shopLevel = shopInfo.getShopLevel();
            //门店规则
            ShopPromotionRule shopPromotionRule = apportionManager.queryRuleType(shopLevel);
            //营销账户信息
            FundsAccount account = fundsAccountMapper.byShopIdAndType(param.getQueryShopId(), 4);
            List<WeekClearChangeNoteDTO> changeNoteDTOS = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(fundsChangeNoteList)) {
                fundsChangeNoteList.forEach(
                        fundsChangeNote -> {
                            FundsWeekClearVO fundsWeekClearVO = JSON.parseObject(fundsChangeNote.getRemark(), new TypeReference<FundsWeekClearVO>(){});
                            WeekClearChangeNoteDTO changeNote = new WeekClearChangeNoteDTO();
                            changeNote.setShopName(fundsWeekClearVO.getShopName());
                            changeNote.setTimeStart(fundsWeekClearVO.getTimeStart());
                            changeNote.setTimeEnd(fundsWeekClearVO.getTimeEnd());
                            changeNote.setPercent(shopPromotionRule.getWeekClearPercent());
                            changeNote.setShopPercent(shopPromotionRule.getWeekClearShopPercent());
                            changeNote.setAreaPercent(shopPromotionRule.getWeekClearAreaPercent());
                            changeNote.setReward(BigDecimalUtil.round(fundsWeekClearVO.getReward()));
                            changeNote.setDiffBefore(BigDecimalUtil.round(fundsChangeNote.getChangeBefore()));
                            changeNote.setDiffAfter(BigDecimalUtil.round(fundsChangeNote.getChangeAfter()));
                            changeNote.setMinPromPrice(account.getMinPromPrice());
                            changeNote.setShopWeekClear(fundsChangeNote.getChangeType().equals(1) ? BigDecimalUtil.round(fundsChangeNote.getChangeAmount()) : BigDecimalUtil.round(fundsChangeNote.getChangeAmount().negate()));
                            BigDecimal areaWeelClear = fundsWeekClearVO.getReward().multiply(shopPromotionRule.getWeekClearPercent()).divide(new BigDecimal("100")).subtract(fundsChangeNote.getChangeAmount());
                            changeNote.setAreaWeekClear(fundsWeekClearVO.getChangeType().equals(1) ? BigDecimalUtil.round(areaWeelClear) : BigDecimalUtil.round(areaWeelClear.negate()));
                            changeNote.setGmtCreate(DateUtil.dateToStr(fundsChangeNote.getGmtCreate(),sdf));
                            changeNoteDTOS.add(changeNote);
                        }
                );
            }
            pageDTO.setRecords(changeNoteDTOS);
        }else if(param.getAccountType().equals(2)){
            List<WeekClearChangeNoteDTO> areaChangeNoteDTOS = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(fundsChangeNoteList)) {
                fundsChangeNoteList.forEach(
                        fundsChangeNote -> {
                            FundsWeekClearVO fundsWeekClearVO = JSON.parseObject(fundsChangeNote.getRemark(), new TypeReference<FundsWeekClearVO>(){});
                            ShopDetail shopInfo = iQmExternalApiService.getShopById(fundsWeekClearVO.getShopId().intValue());
                            //门店级别
                            Integer shopLevel = shopInfo.getShopLevel();
                            //门店规则
                            ShopPromotionRule shopPromotionRule = apportionManager.queryRuleType(shopLevel);
                            //营销账户信息
                            FundsAccount account = fundsAccountMapper.byShopIdAndType(fundsWeekClearVO.getShopId().intValue(), 4);
                            //查询去化奖励流水
                            List<FundsChangeNote> shopFundsChangeNote= fundsChangeNoteManager.queryWeekClearChangeNote(account.getId(),param.getCreateDay(),0,20);
                            WeekClearChangeNoteDTO changeNote = new WeekClearChangeNoteDTO();
                            changeNote.setShopName(fundsWeekClearVO.getShopName());
                            changeNote.setTimeStart(fundsWeekClearVO.getTimeStart());
                            changeNote.setTimeEnd(fundsWeekClearVO.getTimeEnd());
                            changeNote.setPercent(shopPromotionRule.getPromotionPercent());
                            changeNote.setShopPercent(shopPromotionRule.getShopPercent());
                            changeNote.setAreaPercent(shopPromotionRule.getAreaPercent());
                            changeNote.setReward(BigDecimalUtil.round(fundsWeekClearVO.getReward()));
                            changeNote.setDiffBefore(BigDecimalUtil.round(shopFundsChangeNote.get(0).getChangeBefore()));
                            changeNote.setDiffAfter(BigDecimalUtil.round(shopFundsChangeNote.get(0).getChangeAfter()));
                            changeNote.setMinPromPrice(account.getMinPromPrice());
                            changeNote.setShopWeekClear(shopFundsChangeNote.get(0).getChangeType().equals(1) ? BigDecimalUtil.round(shopFundsChangeNote.get(0).getChangeAmount()) : BigDecimalUtil.round(shopFundsChangeNote.get(0).getChangeAmount().negate()));
                            changeNote.setAreaWeekClear(fundsWeekClearVO.getChangeType().equals(1) ? BigDecimalUtil.round(fundsWeekClearVO.getReward().subtract(shopFundsChangeNote.get(0).getChangeAmount())) : BigDecimalUtil.round(fundsWeekClearVO.getReward().subtract(shopFundsChangeNote.get(0).getChangeAmount().negate())));
                            changeNote.setGmtCreate(DateUtil.dateToStr(shopFundsChangeNote.get(0).getGmtCreate(),sdf));
                            areaChangeNoteDTOS.add(changeNote);
                        }
                );
            }
            pageDTO.setRecords(areaChangeNoteDTOS);
        }
        return pageDTO;
    }
}
