package com.qmfresh.promotion.platform.application;

import com.qmfresh.promotion.bean.coupon.SendCouponReturnBean;
import com.qmfresh.promotion.dto.PromotionContext;
import com.qmfresh.promotion.dto.PromotionMzCampOnReq;
import com.qmfresh.promotion.dto.PromotionMzCampOnRsp;
import com.qmfresh.promotion.dto.PromotionProtocol;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.*;

import java.util.List;

/**
 * 促销活动
 */
public interface PromActivityService {

    /**
     * 订单执行
     * @param protocol
     * @param traceId
     * @return
     */
    PromotionContext executeStrategy(PromExecuteStrategyDTO protocol, String traceId);

    /**
     * 活动奖品预占
     * @param campOnDTO
     * @param traceId
     */
    List<PromotionMzCampOnRsp> campOn(PromotionMzCampOnReq campOnDTO, String traceId);

    /**
     * 领取奖励
     * @param param
     * @param traceId
     */
    List<SendCouponReturnBean> send(PromActivitySendDTO param, String traceId);

}
