package com.qmfresh.promotion.platform.application;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.platform.domain.model.funds.FundsAccountManager;
import com.qmfresh.promotion.platform.domain.model.funds.FundsChangeNoteManager;
import com.qmfresh.promotion.platform.domain.model.funds.FundsSettleInfoManager;
import com.qmfresh.promotion.platform.domain.model.funds.IFundsApplyManager;
import com.qmfresh.promotion.platform.domain.model.settle.rule.ShopRuleManager;
import com.qmfresh.promotion.platform.domain.shared.BigDecimalUtil;
import com.qmfresh.promotion.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsSkuVO;
import com.qmfresh.promotion.platform.domain.model.funds.enums.FundsBusinessTypeEnum;
import com.qmfresh.promotion.platform.domain.model.funds.enums.FundsShopTypeEnum;
import com.qmfresh.promotion.platform.domain.valueobj.funds.FundsWeekClearVO;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.FundsAccountMapper;
import com.qmfresh.promotion.platform.infrastructure.mapper.funds.entity.*;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto.ShopIdDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.rule.dto.ShopWeekDTO;
import com.qmfresh.promotion.platform.interfaces.shop.facade.funds.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 */
@Component
@Slf4j
public class ShopAppFundsServiceImpl implements ShopAppFundsService {

    @Resource
    private FundsAccountManager fundsAccountManager;
    @Resource
    private FundsSettleInfoManager fundsSettleInfoManager;
    @Resource
    private FundsChangeNoteManager fundsChangeNoteManager;
    @Resource
    private IFundsApplyManager fundsApplyManager;
    @Resource
    private FundsAccountMapper fundsAccountMapper;
    @Resource
    private ShopRuleManager shopRuleManager;

    @Override
    public ShopFundsAccountDTO queryAccount(Integer shopId) {
        ShopFundsAccountDTO accountDto = new ShopFundsAccountDTO();
        //查询门店账户信息
        FundsAccount account = fundsAccountManager.byShopIdAndType(shopId, FundsShopTypeEnum.MD.getCode());
        if (null == account.getId()) {
            log.info("门店账户信息查询 - 账户信息不存在，shopId = {}", shopId);
            return accountDto;
        }
        accountDto.setTotalAmount(BigDecimalUtil.round(account.getTotalAmount()));
        accountDto.setLockAmount(BigDecimalUtil.round(account.getLockAmount().abs()));

        //查询门店当日统计
        FundsShopDayAmountSum shopDay = fundsSettleInfoManager.queryDayAmount(account.getId(), null, null, DateUtil.formatDate());
        if (null != shopDay) {
            accountDto.setUseAmount(BigDecimalUtil.round(shopDay.getSumUseAmount()));
            accountDto.setNewAmount(BigDecimalUtil.round(shopDay.getSumFundsAmount()));
            accountDto.setReelPay(BigDecimalUtil.round(shopDay.getSumRealPay()));
            accountDto.setMarketingPay(BigDecimalUtil.round(shopDay.getSumMarketingPay()));
        }
        return accountDto;
    }

    @Override
    public List<ShopFundsDayStatisDTO> queryFundsDayStatis(Integer shopId, String timeDay, Integer isShowApply) {
        List<ShopFundsDayStatisDTO>  datStatis = new ArrayList<>();
        //根据shopId查询accountId
        FundsAccount account = fundsAccountMapper.byShopIdAndType(shopId, 4);
        if(account == null){
            log.info("门店费用日统计 - 暂无营销账户 shopId={}", shopId);
            return datStatis;
        }
        Long accountId = account.getId();
        //查询具体日期
        if (StringUtils.isNotEmpty(timeDay)) {
            FundsShopDayAmountSum amountSum = fundsSettleInfoManager.queryDayAmount(null, shopId, FundsShopTypeEnum.MD.getCode(), timeDay);
            if (null == amountSum) {
                log.info("门店费用日统计 - 暂无数据 shopId={}, timeDay={}", shopId, timeDay);
                return datStatis;
            }
            ShopFundsDayStatisDTO dayStatisDTO = new ShopFundsDayStatisDTO();
            dayStatisDTO.setTimeDay(timeDay);
            dayStatisDTO.setUseAmount(BigDecimalUtil.round(amountSum.getSumUseAmount()));
            dayStatisDTO.setNewAmount(BigDecimalUtil.round(amountSum.getSumFundsAmount()));
            //查询日申请
            if (0 == isShowApply) {
                SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                List<FundsApplyDayStatis> applyDayStatis = null;
                try {
                    applyDayStatis = fundsApplyManager.queryDayStatis(shopId, FundsShopTypeEnum.MD.getCode(), DateUtil.getStartTimeStamp(sdf.parse(timeDay)), DateUtil.getEndTimeStamp(sdf.parse(timeDay)));
                } catch (ParseException e) {
                    if (null == amountSum) {
                        log.error("门店费用日统计 - 请求参数日期格式错误 sshopId={}, timeDay={} shopId, timeDay e={}", shopId, timeDay, LogExceptionStackTrace.errorStackTrace(e));
                        return datStatis;
                    }
                }
                if(CollectionUtils.isNotEmpty(applyDayStatis)) {
                    dayStatisDTO.setApplyAmount(BigDecimalUtil.round(applyDayStatis.get(0).getSumApplayAmount()));
                }
            }
            //查询去化奖励
            FundsWeekClear fundsWeekClear = fundsChangeNoteManager.queryDayWeekClear(accountId,timeDay);
            if(fundsWeekClear == null){
                log.warn("门店日去化奖励 - 暂无数据 shopId={}, timeDay={}", shopId, timeDay);
            }else {
                dayStatisDTO.setWeekClear(useWeekClear(fundsWeekClear));
            }
            datStatis.add(dayStatisDTO);
            return datStatis;
        }

        //默认查询近30天
        List<FundsShopDayAmountSum> dayAmountSums = fundsSettleInfoManager.queryDatStatis(shopId, FundsShopTypeEnum.MD.getCode(), DateUtils.addDays(new Date(), -30));
        if (CollectionUtils.isEmpty(dayAmountSums)) {
            log.info("门店费用日统计 - 暂无数据 shopId={}", shopId);
            return datStatis;
        }
        //查询日申请
        Map<String, BigDecimal> applyDayMap = new HashMap<>();
        if (0 == isShowApply) {
            Integer now = DateUtil.getCurrentTimeIntValue();
            Integer before = DateUtil.getStartTimeStamp(DateUtil.getDateBefore(30));
            List<FundsApplyDayStatis> applyDayStatis = fundsApplyManager.queryDayStatis(shopId, FundsShopTypeEnum.MD.getCode(), before, now);
            if (CollectionUtils.isNotEmpty(applyDayStatis)) {
                applyDayStatis.forEach(
                        applyDay->{
                            applyDayMap.put(applyDay.getCreateDay(), applyDay.getSumApplayAmount());
                        }
                );
            }
        }
        //查询去化奖励
        Map<String,WeekClear> weekClearMap = new HashMap<>();
        List<FundsWeekClear> fundsWeekClearList = fundsChangeNoteManager.queryWeekClear(accountId,DateUtils.addDays(new Date(), -30));
        if (CollectionUtils.isNotEmpty(fundsWeekClearList)) {
            fundsWeekClearList.forEach(
                    fundsWeekClear -> {
                        weekClearMap.put(fundsWeekClear.getCreateDay(),useWeekClear(fundsWeekClear));
                    }
            );
        }
        dayAmountSums.forEach(
            dayAmountSum -> {
                ShopFundsDayStatisDTO dayStatisDTO = new ShopFundsDayStatisDTO();
                dayStatisDTO.setTimeDay(dayAmountSum.getCreateDay());
                dayStatisDTO.setUseAmount(BigDecimalUtil.round(dayAmountSum.getSumUseAmount()));
                dayStatisDTO.setNewAmount(BigDecimalUtil.round(dayAmountSum.getSumFundsAmount()));
                //查询日申请
                if (!applyDayMap.isEmpty() && applyDayMap.containsKey(dayAmountSum.getCreateDay())) {
                    dayStatisDTO.setApplyAmount(BigDecimalUtil.round(applyDayMap.get(dayAmountSum.getCreateDay())));
                }
                //查询去化奖励
                if(!weekClearMap.isEmpty() && weekClearMap.containsKey(dayAmountSum.getCreateDay())){
                    dayStatisDTO.setWeekClear(weekClearMap.get(dayAmountSum.getCreateDay()));
                    weekClearMap.remove(dayAmountSum.getCreateDay());
                }
                datStatis.add(dayStatisDTO);
            }
        );
        log.info("门店费用日统计 - 与收入日期不匹配去化奖励 weekClearMap={}", weekClearMap);
        for (Map.Entry<String, WeekClear> entry : weekClearMap.entrySet()) {
            ShopFundsDayStatisDTO weekStatisDTO = new ShopFundsDayStatisDTO();
            weekStatisDTO.setNewAmount(BigDecimal.ZERO);
            weekStatisDTO.setUseAmount(BigDecimal.ZERO);
            weekStatisDTO.setTimeDay(entry.getKey());
            weekStatisDTO.setWeekClear(entry.getValue());
            datStatis.add(weekStatisDTO);
        }
        Collections.sort(datStatis, (o1, o2) ->  DateUtil.parseDate(o1.getTimeDay(), DateUtil.YMD).before(DateUtil.parseDate(o2.getTimeDay(), DateUtil.YMD)) ? 1 : -1);
        return datStatis;
    }

    @Override
    public List<ShopFundsChangeDTO> businessChange(ShopFundsChangeQueryDTO dto) {

        //查询账户信息
        FundsAccount account = fundsAccountManager.byShopIdAndType(dto.getShopId(), FundsShopTypeEnum.MD.getCode());
        if (null == account || null == account.getId()) {
            log.info("门店账户流水-账户信息不存在 shopId={}", dto.getShopId());
            return null;
        }
        List<ShopFundsChangeDTO> changeDTOS = new ArrayList<>();
        //费用收入 从表settle查询
        if (FundsBusinessTypeEnum.SKU_INCOME.getCode() == dto.getBusinessType()) {
            List<FundsSettleInfo> settleInfoList = fundsSettleInfoManager.queryDayNote(account.getId(), dto.getTimeDay(), dto.getStart(), dto.getPageSize());
            if (CollectionUtils.isEmpty(settleInfoList)) {
                log.info("门店账户流水 - 暂无数据 dto={}", JSON.toJSONString(dto));
                return changeDTOS;
            }
            settleInfoList.forEach(
                    settleInfo -> {
                        ShopFundsChangeDTO changeDTO = new ShopFundsChangeDTO(settleInfo);
                        changeDTOS.add(changeDTO);
                    }
            );
        } else if (FundsBusinessTypeEnum.SKU_MARKETING.getCode() == dto.getBusinessType()) { //支出
            List<FundsChangeNote> changeNoteList = new ArrayList<>();
            if(CollectionUtils.isEmpty(dto.getUseType())) { //查询全部
                changeNoteList = fundsChangeNoteManager.queryPayNote(account.getId(), dto.getTimeDay(), null, null, dto.getStart(), dto.getPageSize());
            }else {
                List<String> userType = dto.getUseType();
                List<FundsChangeNote> skuChangeNoteList = null;
                if(userType.contains("-1")) { //其他
                    skuChangeNoteList = fundsChangeNoteManager.queryPayNote(account.getId(), dto.getTimeDay(), Arrays.asList(FundsBusinessTypeEnum.ORDER_CP.getCode(), FundsBusinessTypeEnum.ITEM_DISCOUNT.getCode()), null, dto.getStart(), dto.getPageSize());
                }
                if (CollectionUtils.isNotEmpty(skuChangeNoteList)) {
                    changeNoteList.addAll(skuChangeNoteList);
                }
                //根据SKU查询
                userType.remove("-1");
                if (CollectionUtils.isNotEmpty(userType)){
                    skuChangeNoteList = fundsChangeNoteManager.queryPayNote(account.getId(), dto.getTimeDay(), Arrays.asList(FundsBusinessTypeEnum.SKU_MARKETING.getCode()), userType, dto.getStart(), dto.getPageSize());
                    if (CollectionUtils.isNotEmpty(skuChangeNoteList)) {
                        changeNoteList.addAll(skuChangeNoteList);
                    }
                }
            }
            if (CollectionUtils.isEmpty(changeNoteList)) {
                log.info("门店账户流水 - 暂无数据 dto={}", JSON.toJSONString(dto));
                return changeDTOS;
            }
            changeNoteList.forEach(
                    note -> {
                        ShopFundsChangeDTO changeDTO = new ShopFundsChangeDTO(note);
                        changeDTOS.add(changeDTO);
                    }
            );
        }
        return changeDTOS;
    }

    @Override
    public List<ShopFundsUseTypeDTO> queryUseType(Integer shopId, String timeDay) {
        List<ShopFundsUseTypeDTO> useTypeList = new ArrayList<>();
        //查询账户信息
        FundsAccount account = fundsAccountManager.byShopIdAndType(shopId, FundsShopTypeEnum.MD.getCode());
        if (null == account || null == account.getId()) {
            log.info("营销费用使用类型-账户信息不存在 shopId={} timeDay={}", shopId, timeDay);
            return useTypeList;
        }

        //查询类型
        List<FundsChangeNote> changeNoteList = fundsChangeNoteManager.querySkuBuAccount(account.getId(), timeDay);
        if (CollectionUtils.isNotEmpty(changeNoteList)) {
            changeNoteList.forEach(
                    changeNote -> {
                        if(StringUtils.isNotEmpty(changeNote.getRemark())) {
                            FundsSkuVO skuVo = JSON.parseObject(changeNote.getRemark(), new TypeReference<FundsSkuVO>() {
                            });
                            ShopFundsUseTypeDTO  dto = new ShopFundsUseTypeDTO();
                            dto.setUseType(skuVo.getSkuId());
                            dto.setName(skuVo.getSkuName());
                            useTypeList.add(dto);
                        }
                    }
            );
        }
        return useTypeList;
    }

    public WeekClear useWeekClear(FundsWeekClear fundsWeekClear){
        WeekClear weekClear = new WeekClear();
        BigDecimal weekClearAmount = new BigDecimal("0.00");
        if(StringUtils.isNotEmpty(fundsWeekClear.getRemark())) {
            FundsWeekClearVO fundsWeekClearVO = JSON.parseObject(fundsWeekClear.getRemark(), new TypeReference<FundsWeekClearVO>(){});
            weekClear.setTimeStart(fundsWeekClearVO.getTimeStart());
            weekClear.setTimeEnd(fundsWeekClearVO.getTimeEnd());
            weekClear.setMinPromPrice(fundsWeekClearVO.getMinPromPrice());
            weekClear.setShopWeekClearPercent(fundsWeekClearVO.getShopWeekClearPercent());
            weekClear.setAreaWeekClearPercent(fundsWeekClearVO.getAreaWeekClearPercent());
        }
        if(fundsWeekClear.getChangeType().equals(-1)){
            weekClearAmount = fundsWeekClear.getWeekClear().negate();
        }else {
            weekClearAmount = fundsWeekClear.getWeekClear();
        }
        weekClear.setWeekAmount(BigDecimalUtil.round(weekClearAmount));
        return weekClear;
    }

    @Override
    public ShopWeekClearDetailDTO queryWeekClear(ShopWeekClearDTO param){
        ShopWeekClearDetailDTO shopWeekClearDetail = new ShopWeekClearDetailDTO();
        FundsAccount account = fundsAccountMapper.byShopIdAndType(param.getShopId(), 4);
        if(account == null){
            log.info("查询门店日去化奖励详情 - 暂无营销账户 shopId={}", param.getShopId());
            return shopWeekClearDetail;
        }
        Long accountId = account.getId();
        //查询去化奖励
        FundsWeekClear fundsWeekClear = fundsChangeNoteManager.queryDayWeekClear(accountId,param.getTimeDay());
        if(fundsWeekClear == null){
            log.info("查询门店日去化奖励详情 - 暂无数据 shopId={}, timeDay={}", param.getShopId(), param.getTimeDay());
            return shopWeekClearDetail;
        }
        WeekClear weekClear = useWeekClear(fundsWeekClear);
        //查询门店去化奖励比例和安全值
        ShopIdDTO shopIdDTO = new ShopIdDTO();
        shopIdDTO.setShopId(param.getShopId());
        ShopWeekDTO shopWeekDTO = shopRuleManager.queryRuleByShopId(shopIdDTO);
        if(shopWeekDTO == null){
            log.info("查询门店日去化奖励详情 - 暂无门店去化奖励比例和安全值 shopId={}, timeDay={}", param.getShopId(), param.getTimeDay());
            return shopWeekClearDetail;
        }
        String formula = "去化奖励=(出货量-目标量)*";
        String remark = "最低扣至营销经费池不小于";
        String format = "yyyy-MM-dd";
        shopWeekClearDetail.setWeekAmount(BigDecimalUtil.round(weekClear.getWeekAmount()));
        shopWeekClearDetail.setDayStart(DateUtil.getDateStringByTimeStamp(weekClear.getTimeStart().intValue(),format));
        shopWeekClearDetail.setDayEnd(DateUtil.getDateStringByTimeStamp(weekClear.getTimeEnd().intValue(),format));
        shopWeekClearDetail.setFormula(formula + BigDecimalUtil.round(weekClear.getShopWeekClearPercent()) + "%");
        shopWeekClearDetail.setRemark(remark + BigDecimalUtil.round(weekClear.getMinPromPrice()));
        return shopWeekClearDetail;
    }
}
