package com.qmfresh.promotion.platform.application;

import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.*;

import java.util.List;

/**
 * 促销活动
 */
public interface PlatformPromActivityService {

    /**
     * 查询活动信息
     * @param queryDto
     * @return
     */
    PageDTO<PromActivityDTO> query(PromActivityQueryDTO queryDto);

    /**
     * 活动无效
     * @param param
     */
    boolean noAvail(PromActivityBaseDTO param);

    /**
     * 创建活动信息
     * @param createDTO
     * @return
     */
    void create(PromActivityCreateDTO createDTO, String traceId);

    /**
     * 修改活动信息
     * @param createDTO
     * @return
     */
    boolean update(PromActivityCreateDTO createDTO, String traceId);

    /**
     * 查询活动目标门店ID
     * @param activityId
     * @return
     */
    List<Integer> queryTargetShopId(Long activityId);

    /**
     * 查询活动关联的SKUId
     * @param activityId
     * @return
     */
    List<Integer> queryTargetSkuId(Long activityId);

    /**
     * 查询活动关联的奖品信息
     * @param activityId
     * @return
     */
    List<PromActivityConditionDTO> queryCondition(Long activityId);

    /**
     * 跟新活动奖品
     * @param conditionDTO
     * @param traceId
     */
    boolean updateCondition(PromActivityConditionDTO conditionDTO, String traceId);
}
