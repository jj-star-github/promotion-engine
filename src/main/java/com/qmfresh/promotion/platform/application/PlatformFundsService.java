package com.qmfresh.promotion.platform.application;

import com.qmfresh.promotion.platform.domain.shared.Page;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.funds.*;
import com.qmfresh.promotion.platform.interfaces.shop.facade.funds.*;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

/**
 * 门店营销费用
 */
public interface PlatformFundsService {

    /**
     * 查询账户信息
     * @return
     */
    List<ShopFundsAdminAccountDTO> queryAccount();

    /**
     * 查询账户信息
      * @param shopId
     * @param accountType
     * @return
     */
    ShopFundsAdminAccountDTO queryAccount(Integer shopId, Integer accountType);

    /**
     * 查询账户信息
     * @return
     */
    ShopFundsAdminTargetStatisDTO statis();

    /**
     * 查询总费用
     * @return
     */
    ShopFundsAdminTargetStatisDTO querySumAccount();

    /**
     * 查询日指标
     * @return
     */
    List<ShopFundsAdminTargetDTO> target(ShopFundsAdminStatisQueryDTO queryDTO);

    /**
     * 查询日 账户信息
     * @return
     */
    ShopFundsAdminTargetStatisDTO dayStatis();

    /**
     * 查询账户支出流水
     * @param param
     * @return
     */
    PageDTO<ShopFundsPayChangeNoteDTO> shopPayChangeNote(ShopFundsAdminChangeNoteQueryDTO param);

    /**
     * 查询账户收入流水
     * @param param
     * @return
     */
    PageDTO<ShopFundsNewChangeNoteDTO> shopNewChangeNote(ShopFundsAdminChangeNoteQueryDTO param);

    /**
     * 总部支出流水
     * @param param
     * @return
     */
    PageDTO<FundsZbPayChangeNoteDTO> zbPayChangeNote(ShopFundsAdminChangeNoteQueryDTO param);

    /**
     * 总部收入流水
     * @param param
     * @return
     */
    PageDTO<FundsZbNewChangeNoteDTO> zbNewChangeNote(ShopFundsAdminChangeNoteQueryDTO param);

    /**
     * 门店
     * @param queryDTO
     * @return
     */
    PageDTO<ShopFundsAdminDayDTO> dayChange(ShopFundsAdminStatisQueryDTO queryDTO);

    /**
     * 大区支出流水
     * @param param
     * @return
     */
    PageDTO<FundsZbPayChangeNoteDTO> dqPayChangeNote(ShopFundsAdminChangeNoteQueryDTO param);

    /**
     * 大区收入流水
     * @param param
     * @return
     */
    PageDTO<FundsZbNewChangeNoteDTO> dqNewChangeNote(ShopFundsAdminChangeNoteQueryDTO param);

    /**
     * 查询账户信息
     * @param param
     * @return
     */
    PageDTO<AccountDTO> queryAccountList(AccountQueryDTO param);

    /**
     * 去化奖励流水
     * @param param
     * @return
     */
    PageDTO<WeekClearChangeNoteDTO> weekClearChangeNote(WeekClearChangeNoteQueryDTO param);
}
