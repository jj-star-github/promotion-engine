package com.qmfresh.promotion.platform.application;

import com.qmfresh.promotion.platform.domain.model.funds.FundsAccountManager;
import com.qmfresh.promotion.platform.interfaces.platform.facade.account.AccountCreateDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@Slf4j
public class PlatformAccountServiceImpl implements PlatformAccountService {

    @Resource
    private FundsAccountManager fundsAccountManager;

    @Override
    public Boolean createAccount(AccountCreateDTO accountCreateDTO) {
        return fundsAccountManager.createAccount(accountCreateDTO);
    }

    @Override
    public Boolean updataAccount(AccountCreateDTO accountCreateDTO) {
        return fundsAccountManager.modifyAccount(accountCreateDTO);
    }
}
