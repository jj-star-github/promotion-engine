package com.qmfresh.promotion.platform.application;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qmfresh.promotion.bean.coupon.SendCouponReturnBean;
import com.qmfresh.promotion.bean.promotion.CartContext;
import com.qmfresh.promotion.bean.promotion.MzCampOn;
import com.qmfresh.promotion.bean.promotion.QueryActivityParam;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.dto.*;
import com.qmfresh.promotion.entity.*;
import com.qmfresh.promotion.enums.ActivityOnlineTypeEnums;
import com.qmfresh.promotion.enums.ActivityStatusTypeEnums;
import com.qmfresh.promotion.enums.ChannelTypeEnums;
import com.qmfresh.promotion.enums.PromotionTypeEnums;
import com.qmfresh.promotion.manager.IPromotionRuleManager;
import com.qmfresh.promotion.mapper.AreaScopeShopMapper;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityConditionManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityConvertManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityTargetSkuManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.enums.UserTypeEnum;
import com.qmfresh.promotion.platform.domain.model.promactivity.vo.HitTargetConditionVo;
import com.qmfresh.promotion.platform.domain.service.CouponService;
import com.qmfresh.promotion.platform.domain.service.SkuPriceService;
import com.qmfresh.promotion.platform.domain.service.vo.CouponPreferentialBean;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.*;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.*;
import com.qmfresh.promotion.service.IPromotionActivityService;
import com.qmfresh.promotion.strategy.facade.PromotionExecuteFacade;
import com.qmfresh.promotion.strategy.rule.VipStrategy;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 */
@Component
@Slf4j
public class PromActivityServiceImpl implements PromActivityService {

    @Resource
    private SkuPriceService skuPriceService;
    @Resource
    private IPromotionActivityService promotionActivityService;
    @Resource
    private IPromotionRuleManager promotionRuleManager;
    @Resource
    private VipStrategy vipStrategy;
    @Resource
    private PromActivityManager promActivityManager;
    @Resource
    private PromActivityConvertManager convertManager;
    @Resource
    private AreaScopeShopMapper areaScopeShopMapper;
    @Resource
    private PromotionExecuteFacade promotionExecuteFacade;
    @Resource
    private CouponService couponService;
    @Resource
    private PromActivityTargetSkuManager promActivityTargetSkuManager;
    @Resource
    private PromActivityConditionManager promActivityConditionManager;

    @Override
    public PromotionContext executeStrategy(PromExecuteStrategyDTO protocol, String traceId) {
        PromotionContext context = null;
        if (protocol.getChannel().equals(ChannelTypeEnums.C_OFFLINE.getCode())) {
            skuPriceService.fixOfflinePrice(protocol, traceId);
            context = promotionExecuteFacade.initPromotionContext(protocol);
        } else {
            context = promotionExecuteFacade.initCmallPromotionContext(protocol);
        }
        //会员商品处理
        executeVipCp(context, protocol, traceId);

        //验证优惠券
        CouponPreferentialBean couponPreferential = null;
        if(null != protocol.getCouponInfoDTO() && StringUtils.isNotBlank(protocol.getCouponInfoDTO().getCouponCode())) {
            Integer userId = null != protocol.getUser() ? protocol.getUser().getUserId() : 0;
            couponPreferential = couponService.checkCoupon(context, protocol.getCouponInfoDTO(), userId, traceId);
        }
        //算价
        skuPriceService.realPrice(context, protocol, couponPreferential, traceId);

        //活动执行
        executePromActivity(context, protocol, traceId);
        return context;
    }

    @Override
    public List<PromotionMzCampOnRsp> campOn(PromotionMzCampOnReq protocol, String traceId) {
        List<PromotionMzCampOnRsp> campOnRspList = new ArrayList<>();
        //验证订单是否已存在
        List<PromActivityConvertPO> convertPOList = convertManager.queryByBusiness(protocol.getOrderCode(), 1);
        if (CollectionUtils.isNotEmpty(convertPOList)) {
            log.info("商品预占-订单重复操作 traceId={} shopId={}", traceId, protocol.getOrderCode());
            return campOnRspList;
        }

        //获取奖品信息
        List<MzCampOn> mzCampOnList = protocol.getCampOnList();
        Integer userId = null != protocol.getUserId() ? protocol.getUserId() : 0;
        mzCampOnList.forEach(mzCampOn -> {
            if (null == mzCampOn.getActivityId() || null == mzCampOn.getCouponId() || null == mzCampOn.getGivingNum()) {
                throw new BusinessException("预占信息有误");
            }
            //验证活动是否存在
            PromActivityPO activity = promActivityManager.queryByActivityId(mzCampOn.getActivityId());
            if (null == activity) {
                log.info("商品预占-订单重复操作 traceId={} shopId={}", traceId, protocol.getOrderCode());
                throw new BusinessException("活动不存在");
            }
            PromotionMzCampOnRsp couponCodeBean = convertManager.couponCampOn(mzCampOn, protocol.getOrderCode(), protocol.getShopId(), userId, traceId);
            campOnRspList.add(couponCodeBean);
        });
        return campOnRspList;
    }

    @Override
    public List<SendCouponReturnBean> send(PromActivitySendDTO param, String traceId) {
        //查询订单奖品数据是否存在
        List<PromActivityConvertPO> convertPOList = convertManager.queryByCanSend(param.getUserId(), param.getSourceShopId(), param.getSourceOrderNo(), 1);
        if (CollectionUtils.isEmpty(convertPOList)) {
            log.info("商品领取-订单已处理 traceId={} shopId={}", traceId, JSON.toJSONString(param));
            return null;
        }
        return convertManager.sendAward(convertPOList, traceId);
    }


    /**
     * 执行会员价活动 (TODO:VIP改价活动改了之后可以替换, 没有会员活动但有会员价这种异常需要处理)
     * 主要根据会员商品的数量对商品进行拆分
     */
    private void executeVipCp(PromotionContext context, PromotionProtocol protocol, String traceId) {
        //验证用户是否是会员
        if(!isVip(protocol.getUser(), protocol.getChannel())) {
            log.info("活动命中- 非会员用户 traceId={}", traceId);
            return;
        }
        //查询会员改价活动
        QueryActivityParam queryActivityParam = new QueryActivityParam();
        queryActivityParam.setEffectBeginTime(DateUtil.getCurrentTimeIntValue());
        queryActivityParam.setEffectEndTime(DateUtil.getCurrentTimeIntValue());
        queryActivityParam.setIsOnline(ActivityOnlineTypeEnums.ONLINE.getCode());
        queryActivityParam.setStatus(ActivityStatusTypeEnums.STARTED.getCode());
        queryActivityParam.setChannel(protocol.getChannel());
        queryActivityParam.setModuleIds(Arrays.asList(PromotionTypeEnums.VIP.getCode()));
        List<PromotionActivity> vipActivity = promotionActivityService.queryByCondition(queryActivityParam);
        if(CollectionUtils.isEmpty(vipActivity)) { //活动不存在
            log.info("活动命中-当前门店没有会员改价活动, traceId={} queryActivityParam={}", traceId, JSON.toJSONString(queryActivityParam));
            return;
        }
        List<AreaScopeShop> areaScopeShops = areaScopeShopMapper.selectList(new LambdaQueryWrapper<AreaScopeShop>()
                .eq(AreaScopeShop::getShopId, protocol.getArea().getShopId())
                .in(AreaScopeShop::getActivityId, vipActivity.stream().map(PromotionActivity::getId).collect(Collectors.toList()))
                .eq(AreaScopeShop::getIsDeleted, 0)
        );
        if (!ListUtil.isNullOrEmpty(areaScopeShops)) {
            List<Long> activityIds = areaScopeShops.stream().map(AreaScopeShop::getActivityId).distinct().collect(Collectors.toList());
            vipActivity = vipActivity.stream().filter(item -> activityIds.contains(item.getId())).collect(Collectors.toList());
        }
        if(CollectionUtils.isEmpty(vipActivity)) { //活动不存在
            log.info("活动命中-当前门店没有会员改价活动, traceId={} queryActivityParam={}", traceId, JSON.toJSONString(queryActivityParam));
            return;
        }
        List<Long> activityIds = vipActivity.stream().map(PromotionActivity::getId).collect(Collectors.toList());
        List<PromotionRule> vipCpRule = promotionRuleManager.list(new QueryWrapper<PromotionRule>()
                .eq("is_online", 1)
                .eq("is_deleted", 0)
                .in("activity_id", activityIds));
        if (CollectionUtils.isEmpty(vipCpRule)) {
            log.info("活动命中-当前门店没有有效的会员改价活动, traceId={} activityIds={}", traceId, JSON.toJSONString(activityIds));
            return;
        }
        CartContext cartContext = new CartContext();
        cartContext.setShopId(protocol.getArea().getShopId());
        cartContext.setChannel(protocol.getChannel());
        cartContext.setUserId(protocol.getUser().getUserId().longValue());
        cartContext.setPromotionSsus(protocol.getPromotionSsuList());
        cartContext.setUserLevel(protocol.getUser().getLevel());
        vipStrategy.execute(vipCpRule, cartContext, context);
    }

    /**
     * 验证用户是否为会员
     * @param user
     * @param channel
     * @return
     */
    protected boolean isVip(User user, Integer channel) {
        if (channel.equals(ChannelTypeEnums.C_ONLINE.getCode())) {
            return user.getUserId() > 0 && user.getLevel() > 0;
        } else {
            return user.getUserId() > 0;
        }
    }

    /**
     * 执行活动命中
     * @param context
     * @param protocol
     * @param traceId
     */
    private void executePromActivity(PromotionContext context, PromotionProtocol protocol, String traceId){
        //获取有效的活动信息
        int userType = isVip(protocol.getUser(), protocol.getChannel()) ? UserTypeEnum.VIP.getCode() : UserTypeEnum.NO.getCode();
        List<PromActivityPO> promActivityPOList = promActivityManager.queryByShopId(protocol.getArea().getShopId(), new Date(), userType);
        if (CollectionUtils.isEmpty(promActivityPOList)){
            log.info("命中活动-当前门店暂无有效活动, traceId={} shopId={}", traceId, protocol.getArea().getShopId());
            return;
        }
        //封装商品信息
        List<PromActivityCampOnSkuDTO> skuDTOList = new ArrayList<>();
        for(PromotionSsuContext ssuContext : context.getSsuContexts()) {
            PromActivityCampOnSkuDTO skuDTO = new PromActivityCampOnSkuDTO(ssuContext);
            skuDTOList.add(skuDTO);
        }

        List<Long> activityIdList = promActivityPOList.stream().map(PromActivityPO::getId).collect(Collectors.toList());
        //查询活动目标品信息
        List<PromActivityTargetSkuPO> targetSkuPOList = promActivityTargetSkuManager.queryByActivityList(activityIdList);
        Map<Long, List<PromActivityTargetSkuPO>> targetSkuMap = targetSkuPOList.stream().collect(Collectors.groupingBy(PromActivityTargetSkuPO::getActivityId));

        //查询活动奖品信息
        List<PromActivityConditionPO> conditionPOList = promActivityConditionManager.queryByActivityList(activityIdList);
        if (CollectionUtils.isEmpty(conditionPOList)) {
            context.setMzContexts(Collections.emptyList());
            return;
        }
        Map<Long, List<PromActivityConditionPO>> conditionMap = conditionPOList.stream().collect(Collectors.groupingBy(PromActivityConditionPO::getActivityId));
        //执行活动
        List<MzContext> mzContexts = new ArrayList<>();
        for(PromActivityPO activityPO : promActivityPOList) {
            activityPO.setConditionPOList(conditionMap.get(activityPO.getId()));
            if(targetSkuMap.containsKey(activityPO.getId())) {
                activityPO.setTargetSkuId(targetSkuMap.get(activityPO.getId()));
            }
            HitTargetConditionVo conditionVo = promActivityManager.execute(skuDTOList, activityPO, traceId);
            if (null == conditionVo) {
                continue;
            }
            //封装活动信息
            MzContext mzContext = conditionVo.createMzContext(activityPO);
            mzContexts.add(mzContext);
        }
        context.setMzContexts(mzContexts);
    }

}
