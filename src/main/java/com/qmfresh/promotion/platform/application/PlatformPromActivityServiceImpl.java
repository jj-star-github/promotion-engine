package com.qmfresh.promotion.platform.application;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityConditionManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityTargetShopManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.PromActivityTargetSkuManager;
import com.qmfresh.promotion.platform.domain.model.promactivity.enums.StatusEnum;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.infrastructure.mapper.activity.entity.*;
import com.qmfresh.promotion.platform.interfaces.common.PageDTO;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 */
@Component
@Slf4j
public class PlatformPromActivityServiceImpl implements PlatformPromActivityService {

    @Resource
    private PromActivityManager activityManager;
    @Resource
    private PromActivityTargetShopManager targetShopManager;
    @Resource
    private PromActivityTargetSkuManager targetSkuManager;
    @Resource
    private PromActivityConditionManager conditionManager;

    @Override
    public PageDTO<PromActivityDTO> query(PromActivityQueryDTO queryDto) {
        PageDTO<PromActivityDTO> pageDTO = new PageDTO<>();
        Integer totalCount = activityManager.queryCountByParam(queryDto);
        if (null == totalCount || 0 >= totalCount) {
            return pageDTO;
        }
        pageDTO.setTotalCount(totalCount); //总行数
        List<PromActivityPO> activityPOS = activityManager.queryByParam(queryDto);
        List<PromActivityDTO> activityDTOS = new ArrayList<>();
        activityPOS.forEach(
                activityPO -> {
                    PromActivityDTO activityDTO = new PromActivityDTO(activityPO);
                    activityDTOS.add(activityDTO);
                }
        );
        pageDTO.setRecords(activityDTOS); //数据信息
        return pageDTO;
    }

    @Override
    public boolean noAvail(PromActivityBaseDTO param) {
        activityManager.noAvail(param.getId(), param.getOperatorName(), param.getOperatorId());
        return true;
    }

    @Override
    public void create(PromActivityCreateDTO createDTO, String traceId) {
        //验证活动时间
        Long now = System.currentTimeMillis();
        if(createDTO.getEndTime() < now || createDTO.getEndTime() < createDTO.getBeginTime()) {
            log.warn("创建活动失败-活动时间有误 traceId={} param={}", traceId, JSON.toJSONString(createDTO));
            throw new BusinessException("活动时间有误");
        }
        //活动验证
        if(!activityManager.checkActivity(createDTO, traceId, null)){
            log.warn("创建活动失败-活动已存在 traceId={} param={}", traceId, JSON.toJSONString(createDTO));
            throw new BusinessException("活动已存在");
        }
        //创建活动
        activityManager.createActivity(createDTO);
    }

    @Override
    public boolean update(PromActivityCreateDTO createDTO, String traceId) {
        //查询活动
        PromActivityPO promActivityPO = activityManager.queryByActivityId(createDTO.getId());
        if (null == promActivityPO || StatusEnum.CREATE.getCode() != promActivityPO.checkStatus()) {
            log.warn("活动更新失败-活动不存在或状态有误 traceId={} activityId={}", traceId, createDTO.getId());
            throw new BusinessException("活动不存在或状态有误");
        }
        //验证活动基础信息
        if(null != createDTO.getShopType() && createDTO.getShopType() != promActivityPO.getShopType()) {
            log.warn("活动更新失败-门店类型不可修改 traceId={} activityId={}", traceId, createDTO.getId());
            throw new BusinessException("门店类型不可修改");
        }
        if(null != createDTO.getSkuType() && createDTO.getSkuType() != promActivityPO.getSkuType()) {
            log.warn("活动更新失败-商品类型不可修改 traceId={} activityId={}", traceId, createDTO.getId());
            throw new BusinessException("商品类型不可修改");
        }
        //活动验证
        createDTO.setShopType(promActivityPO.getShopType());
        createDTO.setSkuType(promActivityPO.getShopType());
        if(!activityManager.checkActivity(createDTO, traceId, promActivityPO)){
            log.warn("活动更新失败-活动商品或门店重复 traceId={} activityId={}", traceId, createDTO.getId());
            throw new BusinessException("活动奖品或门店重复");
        }
        //修改
        activityManager.updateActivity(createDTO, promActivityPO, traceId);
        return true;
    }

    @Override
    public List<Integer> queryTargetShopId(Long activityId) {
        return targetShopManager.queryShopId(activityId);
    }

    @Override
    public List<Integer> queryTargetSkuId(Long activityId) {
        return targetSkuManager.querySkuId(activityId);
    }

    @Override
    public List<PromActivityConditionDTO> queryCondition(Long activityId) {
        List<PromActivityConditionDTO> conditionDTOS = new ArrayList<>();
        List<PromActivityConditionPO> conditionPOS = conditionManager.queryByActivity(activityId);
        if (CollectionUtils.isEmpty(conditionPOS)) {
            return conditionDTOS;
        }
        conditionPOS.forEach(
                conditionPO ->{
                    PromActivityConditionDTO targetSkuDTO = new PromActivityConditionDTO(conditionPO);
                    conditionDTOS.add(targetSkuDTO);
                }
        );
        return conditionDTOS;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateCondition(PromActivityConditionDTO conditionDTO, String traceId) {
        //查询活动
        PromActivityPO promActivityPO = activityManager.queryByActivityId(conditionDTO.getActivityId());
        if (null == promActivityPO || StatusEnum.CREATE.getCode() != promActivityPO.checkStatus()) {
            log.warn("活动更新失败-活动不存在或状态有误 traceId={} activityId={}", traceId, conditionDTO.getActivityId());
            throw new BusinessException("活动不存在或状态有误");
        }
        conditionManager.updateCondition(conditionDTO);
        PromActivityPO updateActivity = new PromActivityPO();
        updateActivity.setId(conditionDTO.getActivityId());
        updateActivity.setLastOperatorId(conditionDTO.getOperatorId());
        updateActivity.setLastOperatorName(conditionDTO.getOperatorName());
        updateActivity.setGmtModified(new Date());
        activityManager.updateById(updateActivity);
        return true;
    }
}
