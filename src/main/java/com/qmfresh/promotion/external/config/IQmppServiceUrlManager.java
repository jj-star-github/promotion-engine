/*
 * Copyright (C) 2018-2021 Qm All rights reserved
 *      ┌─┐       ┌─┐ + +
 *   ┌──┘ ┴───────┘ ┴──┐++
 *   │       ───       │++ + + +
 *   ███████───███████ │+
 *   │       ─┴─       │
 *   └───┐         ┌───┘
 *       │         │   + +
 *       │         └──────────────┐
 *       │                        ├─┐
 *       │                        ┌─┘
 *       └─┐  ┐  ┌───────┬──┐  ┌──┘  + + + +
 *         │ ─┤ ─┤       │ ─┤ ─┤
 *         └──┴──┘       └──┴──┘  + + + +
 *                神兽保佑
 *               代码无BUG!
 */
package com.qmfresh.promotion.external.config;

import com.qmfresh.promotion.common.utils.UtilMethods;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author lxc
 * @Date 2020/3/13 0013 17:57
 */
@Component
public class IQmppServiceUrlManager {

    private String qmppCallBackUrl;

    @Value("${qmpp.service.url}")
    public void setCallBackUrl(String url) {
        qmppCallBackUrl = url;
    }

    public String getNoticeModifyPriceUrl() {
        return UtilMethods.concatUrl(qmppCallBackUrl, "automaticDiscountNotice/modifyPrice");
    }
}
