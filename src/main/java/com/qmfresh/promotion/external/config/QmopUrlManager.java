package com.qmfresh.promotion.external.config;

import com.qmfresh.promotion.common.utils.UtilMethods;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @ClassName QmopUrlManager
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/6 14:22
 */
@Component
public class QmopUrlManager {

    private String callBackUrl;

    @Value("${cds.service.url}")
    public void setCallBackUrl(String url) {
        callBackUrl = url;
    }

    public String getAuditedShopUrl() {
        return UtilMethods.concatUrl(callBackUrl, "shopAuthority/queryShopPriceAuthorityDetail");
    }
}
