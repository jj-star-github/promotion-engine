package com.qmfresh.promotion.external.config;

import com.qmfresh.promotion.common.utils.UtilMethods;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 库存URL管理
 */
@Component
public class InventoryUrlManager {

    private String url;
    @Value("${inventory.service.url}")
    public void setUrl(String url) {
        this.url = url;
    }

    public String getQuerySkuStockUrl(){
        return UtilMethods.concatUrl(url,"wms/search/querySkuStockList");
    }

    public String getwmsReceiveDetail(){
        return UtilMethods.concatUrl(url,"wms/receive/queryDetail");
    }

    public String getReceiveInfo(){
        return UtilMethods.concatUrl(url,"wms/receive/getReceiveInfo");
    }
}
