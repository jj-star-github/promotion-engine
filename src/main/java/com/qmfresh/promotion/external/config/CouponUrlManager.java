package com.qmfresh.promotion.external.config;

import com.qmfresh.promotion.common.utils.UtilMethods;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CouponUrlManager {
    private String couponCallBackUrl;

    @Value("${coupon.service.url}")
    public void setCallBackUrl(String url) {
        couponCallBackUrl = url;
    }

    public String getCouponByConditionUrl() {
        return UtilMethods.concatUrl(couponCallBackUrl, "coupon/getByCondition");
    }

    public String queryCouponInfo() {
        return UtilMethods.concatUrl(couponCallBackUrl, "apiCoupon/queryCouponInfo");
    }

    public String sendCoupon() {
        return UtilMethods.concatUrl(couponCallBackUrl, "apiCoupon/cloudPosSendCoupon");
    }

    public String checkCoupon() {
        return UtilMethods.concatUrl(couponCallBackUrl, "apiCoupon/checkCoupon");
    }

}
