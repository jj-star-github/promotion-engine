package com.qmfresh.promotion.external.config;

import com.qmfresh.promotion.common.utils.UtilMethods;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class QmServiceUrlManager {
    private String qmCallBackUrl;

    @Value("${product.service.url}")
    private String productServiceUrl;

    public String getQmCallBackUrl() {
		return qmCallBackUrl;
	}

	public void setQmCallBackUrl(String qmCallBackUrl) {
		this.qmCallBackUrl = qmCallBackUrl;
	}


	@Value("${qm.service.url}")
    public void setCallBackUrl(String url) {
        qmCallBackUrl = url;
    }

    public String getShopByConditionUrl() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/getByCondition");
    }

    public String batchCreateOrderUrl() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/procurement/batchCreateOrderC");
    }

    public String getShopBatchIdsUrl() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/shop/selectBatchIds");
    }

    public String querySkuUrl() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/sku/getBySkuCondition");
    }

    public String getBySkuIdsUrl() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/sku/getBySkuIds");
    }

    public String getSkuUrl(){
        return UtilMethods.concatUrl(qmCallBackUrl,"shop/ex/sku/getSku");
    }

    public String createOrderUrl() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/procurement/batchCreateOrderC");
    }

    public String checkBatchCreateOrderCUrl() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/procurement/checkBatchCreateOrderC");
    }

    public String getGisCityUrl() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/gisCity/list");
    }

    public String batchModifyPriceUrl() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/ssu/batchmodifyPrice");
    }

    public String deleteOrderItemUrl() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/procurement/item/delete");
    }

    public String updateOrderItemUrl() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/procurement/item/update");
    }

    public String getCSellMinPriceBySkuIds() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/ssu/getCSellMinPriceBySkuIds");
    }

    public String getShopBindSkuList() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/sku/getShopBindSkuList");
    }

    public String filterEnableBindSku() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/sku/filterEnableBindSku");
    }

    public String getShopByCityIds() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/shop/getByCityIds");
    }

    public String getShopSkuCSellPrice() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/ssu/getShopSkuCSellPrice");
    }

    public String getProductSsuById() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/ssu/getProductSsuById");
    }

    public String listSsuByIds() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/ssu/listSsuByIds");
    }

    public String getSsuFormatByCondition() {
        return UtilMethods.concatUrl(productServiceUrl, "shop/ex/ssu/getSsuFormatByCondition");
    }

    public String listSku() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/sku/getSsuFormatByCondition");
    }

    public String getShopProductSsuPriceListUrl() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/ssu/getShopProductPriceList2");
    }

    public String resetVipPriceUrl() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/ssu/resetVipPrice");
    }

    public String getQueryShopSkuSalesVolumeUrl(){
        return UtilMethods.concatUrl(qmCallBackUrl, "/shop/orderC/realTimeSalesByCondition");
    }
    public String getQueryShopSkuPriceUrl(){
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/ssu/getShopProductPriceListBatch");
    }
    public String getUpdateShopSkuPriceUrl(){
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/ssu/updateShopProductPriceListBatch");
    }

    public String getProductSsuPriceListByCondition(){
        return UtilMethods.concatUrl(qmCallBackUrl, "/shop/ex/ssu/getProductSsuPriceListByCondition");
    }

    public String getShopByIdUrl(){
        return UtilMethods.concatUrl(qmCallBackUrl, "/shop/ex/shop/getShopById");
    }

    public String getClass2List() {
        return UtilMethods.concatUrl(qmCallBackUrl, "shop/ex/sku/getClass2List");
    }

    public String selectAreaByShopIds(){
        return UtilMethods.concatUrl(qmCallBackUrl, "areaShop/selectAreaByShopIds");
    }
}
