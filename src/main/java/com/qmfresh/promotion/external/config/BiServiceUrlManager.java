package com.qmfresh.promotion.external.config;

import com.qmfresh.promotion.common.utils.UtilMethods;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BiServiceUrlManager {
    private String biCallBackUrl;

    @Value("${bi.service.url}")
    public void setCallBackUrl(String url) {
        biCallBackUrl = url;
    }

    public String getShopSkuSellOrStock(){
        return UtilMethods.concatUrl(biCallBackUrl, "shop/data/getShopSkuSellOrStock");
    }
}
