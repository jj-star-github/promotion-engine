package com.qmfresh.promotion.external.config;

import com.qmfresh.promotion.common.utils.UtilMethods;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CmallUrlManager {
    private String cmallCallBackUrl;

    @Value("${cmall.service.url}")
    public void setCallBackUrl(String url) {
        cmallCallBackUrl = url;
    }

    public String queryCmallPriceListUrl() {
        return UtilMethods.concatUrl(cmallCallBackUrl, "ssu/getSsuOriginPrice");
    }

    public String querySsuListUrl() {
        return UtilMethods.concatUrl(cmallCallBackUrl, "ssu/getSsuList");
    }

    public String querySsuListByParamUrl() {
        return UtilMethods.concatUrl(cmallCallBackUrl, "ssu/getSsuListByParam");
    }
}
