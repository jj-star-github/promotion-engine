package com.qmfresh.promotion.external.config;

import com.qmfresh.promotion.common.utils.UtilMethods;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CashServiceUrlManager {

    private String domain;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Value("${cash.service.url}")
    public void setCallBackUrl(String url) {
        domain = url;
    }

    public String getQueryShopSkuSalesVolumeUrl(){
        return UtilMethods.concatUrl(domain, "/shop/orderC/realTimeSalesByCondition");
    }

    public String selectOrderMainItemByOrderCode(){
        return UtilMethods.concatUrl(domain, "cash/orderC/selectOrderMainItemByOrderCode");
    }
}
