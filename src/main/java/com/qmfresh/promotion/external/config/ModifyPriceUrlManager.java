package com.qmfresh.promotion.external.config;

import com.qmfresh.promotion.common.utils.UtilMethods;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @ClassName ModifyPriceUrlManager
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/6 17:53
 */
@Component
public class ModifyPriceUrlManager {

    private String callBackUrl;

    @Value("${qmpp.service.url}")
    public void setCallBack(String callBack) {
        this.callBackUrl = callBack;
    }

    public String getModifyPromotionPriceUrl(){
        return UtilMethods.concatUrl(callBackUrl, "promotionNotice/modifyPrice");
    }

    public String getModifyVipPriceUrl(){
        return UtilMethods.concatUrl(callBackUrl, "memberNotice/modifyPrice");
    }

    public String getQueryPriceListUrl(){
        return UtilMethods.concatUrl(callBackUrl, "memberNotice/queryPriceList");
    }
}
