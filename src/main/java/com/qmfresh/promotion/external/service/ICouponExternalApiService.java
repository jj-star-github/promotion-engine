package com.qmfresh.promotion.external.service;

import com.qmfresh.promotion.bean.coupon.*;
import com.qmfresh.promotion.dto.PromotionMzCampOnRsp;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.CouponCheckResultDTO;

import java.util.List;

/**
 * Created by wyh on 2018/6/6.
 *
 * @author wyh
 */
public interface ICouponExternalApiService {

    /**
     * 条件查询门店信息
     *
     * @param condition
     * @return
     */
    List<CouponInfo> getCouponByCondition(CouponQuery condition);

    /**
     * 查询优惠券信息
     * @param condition
     * @return
     */
    PromotionMzCampOnRsp queryCouponInfo(CouponQuery condition);

    /**
     * 优惠券发放
     * @param requestBean
     * @return
     */
    List<SendCouponReturnBean> sendCoupon(SendCouponRequestBean requestBean);

    /**
     * 验证优惠券
     * @param requestBean
     * @return
     */
    CouponCheckResultDTO checkCoupon(CouponCheckParam requestBean);

}
