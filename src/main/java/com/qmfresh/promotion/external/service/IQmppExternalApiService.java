/*
 * Copyright (C) 2018-2021 Qm All rights reserved
 *      ┌─┐       ┌─┐ + +
 *   ┌──┘ ┴───────┘ ┴──┐++
 *   │       ───       │++ + + +
 *   ███████───███████ │+
 *   │       ─┴─       │
 *   └───┐         ┌───┘
 *       │         │   + +
 *       │         └──────────────┐
 *       │                        ├─┐
 *       │                        ┌─┘
 *       └─┐  ┐  ┌───────┬──┐  ┌──┘  + + + +
 *         │ ─┤ ─┤       │ ─┤ ─┤
 *         └──┴──┘       └──┴──┘  + + + +
 *                神兽保佑
 *               代码无BUG!
 */
package com.qmfresh.promotion.external.service;

import com.qmfresh.promotion.bean.sellout.DiscountModifyPriceDto;
import com.qmfresh.promotion.bean.sellout.SkuPrice;

import java.util.List;

/**
 * @author lxc
 * @Date 2020/3/13 0013 17:50
 */
public interface IQmppExternalApiService {

    /**
     * 通知中台修改某个门店，旗下的SKU的价格
     *
     * @param reqDto
     * @return
     */
    Boolean noticeCenterUpdateSkuPrice(DiscountModifyPriceDto reqDto);
}
