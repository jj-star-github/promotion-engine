package com.qmfresh.promotion.external.service;

import com.qmfresh.promotion.dto.ShopAuthorityResult;
import com.qmfresh.promotion.dto.qmpp.AuthenParam;

import java.util.List;

/**
 * @ClassName IQmopExternalApiService
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/6 14:28
 */
public interface IQmopExternalApiService {

    List<ShopAuthorityResult> findAuthorityByShopIdList(List<AuthenParam> authenParams);
}
