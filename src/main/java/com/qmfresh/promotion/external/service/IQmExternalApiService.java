package com.qmfresh.promotion.external.service;

import com.qmfresh.promotion.bean.gis.GisCity;
import com.qmfresh.promotion.bean.gis.Shop;
import com.qmfresh.promotion.bean.price.ProductSsuPriceResult;
import com.qmfresh.promotion.bean.price.ResetVipPriceParam;
import com.qmfresh.promotion.bean.price.ShopSsuQuery;
import com.qmfresh.promotion.bean.sellout.SkuPrice;
import com.qmfresh.promotion.bean.sku.*;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.dto.qmpp.Class2Result;
import com.qmfresh.promotion.dto.qmpp.QmppSkuDto;
import com.qmfresh.promotion.entity.AreaConditionQuery;
import com.qmfresh.promotion.entity.BaseArea;
import com.qmfresh.promotion.entity.ShopDetail;

import java.util.List;

/**
 * Created by wyh on 2018/6/6.
 *
 * @author wyh
 */
public interface IQmExternalApiService {

    /**
     * 条件查询门店信息
     *
     * @param condition
     * @return
     */
    List<Shop> getShopByCondition(Shop condition);

    /**
     * 根据门店ids查看门店信息
     *
     * @param shopIds
     * @return
     */
    List<Shop> selectShopByIds(List<Integer> shopIds);

    /**
     * 查询商品
     *
     * @param skuQuery
     * @return
     */
    List<Sku> getBySkuCondition(SkuQuery skuQuery);

    List<Sku> getBySkuIds(List<Integer> skuIds);

    /**
     * 获取商品详细信息
     * @param skuIds
     * @return
     */
    List<Sku> getSku(List<Integer> skuIds);

    /**
     * 获取城市信息
     *
     * @param condition
     * @return
     */
    List<GisCity> getGisCity(GisCity condition);

    /**
     * 根据城市ID集合获取门店
     *
     * @param cityIds 城市ID集合
     * @return
     */
    List<Shop> getShopByCityIds(List<Integer> cityIds);

    ProductSsu getProductSsuById(Integer ssuId);

    List<ProductSsu> listSsuByIds(List<Integer> ssuIds);

    /**
     * 条件查询ssu_format
     *
     * @param query
     * @return
     */
    List<ProductSsuFormatDto> getSsuFormatByCondition(QueryProductSsuFormat query);

    /**
     * 查询ssu
     *
     * @param query
     * @return
     */
    ListWithPage<ProductSsuPriceResult> getShopProductSsuPriceList(ShopSsuQuery query);

    /**
     * 重置会员价
     *
     * @param param
     * @return
     */
    Boolean resetVipPrice(ResetVipPriceParam param);

    /**
     * 查询某个门店，旗下的SKU 的实时销量
     */
 //   List<SkuSale> querySkuSalesVolumeInShop(SkuSale query);

    /**
     * 查询某个门店，旗下的SKU的实时价格
     */
    List<SkuPrice> querySkuPriceInShop(List<SkuPrice> query);

    /**
     * 修改某个门店，旗下的SKU的价格
     */
    Boolean updateSkuPriceInShop(List<SkuPrice> query);

    /**
     * 获取门店信息
     * @param shopId
     * @return
     */
    ShopDetail getShopById(Integer shopId);

    /**
     * 获取商品价格
     * @param param
     * @return
     */
    List<QmppSkuDto> getProductSsuPriceListByCondition(QueryProductSsuPrice param);

    /**
     * 获取二级分类
     *
     * @return
     */
    List<Class2Result> getClass2s();

    /**
     * 获取门店所对应大区
     * @param param
     * @return
     */
    List<BaseArea> selectAreaByShopIds(AreaConditionQuery param);
}

