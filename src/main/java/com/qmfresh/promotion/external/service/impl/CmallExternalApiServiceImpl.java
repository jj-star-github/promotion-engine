package com.qmfresh.promotion.external.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.bean.price.CMallPriceInfo;
import com.qmfresh.promotion.bean.price.CMallPriceQuery;
import com.qmfresh.promotion.bean.sku.Ssu;
import com.qmfresh.promotion.bean.sku.SsuQuery;
import com.qmfresh.promotion.bean.sku.SsuQueryForOnline;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.common.utils.RequestUtil;
import com.qmfresh.promotion.external.config.CmallUrlManager;
import com.qmfresh.promotion.external.service.ICmallExternalApiService;
import java.util.List;
import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/8/12.
 *
 * @author wyh
 */
@Service
@Slf4j
public class CmallExternalApiServiceImpl implements ICmallExternalApiService {

    @Resource
    private CmallUrlManager cmallUrlManager;

    @Override
    public List<CMallPriceInfo> queryCmallPriceList(CMallPriceQuery param) {
        ServiceResult<List<CMallPriceInfo>> srResult;
        try {
            srResult = RequestUtil.postJson(
                cmallUrlManager.queryCmallPriceListUrl(),
                JSON.toJSONString(param),
                new TypeReference<ServiceResult<List<CMallPriceInfo>>>() {
                }
            );
        } catch (Exception e) {
            String message = "url:" + cmallUrlManager.queryCmallPriceListUrl() + ",params:" + JSON.toJSONString(param);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public List<Ssu> querySsu(SsuQuery param) {
        ServiceResult<List<Ssu>> srResult;
        try {
            srResult = RequestUtil.postJson(
                cmallUrlManager.querySsuListUrl(),
                JSON.toJSONString(param),
                new TypeReference<ServiceResult<List<Ssu>>>() {
                }
            );
        } catch (Exception e) {
            String message = "url:" + cmallUrlManager.queryCmallPriceListUrl() + ",params:" + JSON.toJSONString(param);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public List<Ssu> getSsuListByParam(SsuQueryForOnline query) {
        ServiceResult<List<Ssu>> srResult;
        try {
            srResult = RequestUtil.postJson(
                    cmallUrlManager.querySsuListByParamUrl(),
                    JSON.toJSONString(query),
                    new TypeReference<ServiceResult<List<Ssu>>>() {
                    }
            );
        } catch (Exception e) {
            String message = "url:" + cmallUrlManager.querySsuListByParamUrl() + ",params:" + JSON.toJSONString(query);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }
}
