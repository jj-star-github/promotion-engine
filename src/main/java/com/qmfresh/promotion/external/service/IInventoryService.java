package com.qmfresh.promotion.external.service;

import com.qmfresh.promotion.dto.SkuStockInShopQuery;
import com.qmfresh.promotion.bean.sellout.SkuStock;
import com.qmfresh.promotion.entity.ReceiveInfo;
import com.qmfresh.promotion.entity.WmsReceiveDetail;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.record.dto.WmsReceiveDTO;

import java.util.List;

/**
 * 门店库存服务
 */
public interface IInventoryService {
    /**
     * 查询某一个门店一系列SKU 的实时库存
     */
    List<SkuStock> querySkuStockInShop(SkuStockInShopQuery query);

    List<WmsReceiveDetail> getwmsReceiveDetail(WmsReceiveDTO wmsReceiveDTO);

    List<ReceiveInfo> getReceiveInfo(WmsReceiveDTO wmsReceiveDTO);
}
