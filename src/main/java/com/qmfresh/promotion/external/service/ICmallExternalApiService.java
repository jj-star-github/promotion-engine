package com.qmfresh.promotion.external.service;

import com.qmfresh.promotion.bean.price.CMallPriceInfo;
import com.qmfresh.promotion.bean.price.CMallPriceQuery;
import com.qmfresh.promotion.bean.sku.Ssu;
import com.qmfresh.promotion.bean.sku.SsuQuery;
import com.qmfresh.promotion.bean.sku.SsuQueryForOnline;

import java.util.List;

/**
 * Created by wyh on 2018/6/6.
 *
 * @author wyh
 */
public interface ICmallExternalApiService {

    /**
     * 查询C端线上ssu价格
     *
     * @param query
     * @return
     */
    List<CMallPriceInfo> queryCmallPriceList(CMallPriceQuery query);

    /**
     * 查看ssu列表
     *
     * @param query
     * @return
     */
    List<Ssu> querySsu(SsuQuery query);
    
    List<Ssu> getSsuListByParam(SsuQueryForOnline query);
}
