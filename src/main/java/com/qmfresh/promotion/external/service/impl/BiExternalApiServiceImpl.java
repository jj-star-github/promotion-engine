package com.qmfresh.promotion.external.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.common.utils.RequestUtil;
import com.qmfresh.promotion.entity.ShopSku;
import com.qmfresh.promotion.entity.ShopSkuSellOrStock;
import com.qmfresh.promotion.external.config.BiServiceUrlManager;
import com.qmfresh.promotion.external.service.IBiExternalApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class BiExternalApiServiceImpl implements IBiExternalApiService {
    @Resource
    private BiServiceUrlManager biServiceUrlManager;


    @Override
    public ShopSkuSellOrStock getShopSkuSellOrStock(ShopSku param){
        ServiceResult<ShopSkuSellOrStock> res;
        try{
            res = RequestUtil.postJson(
                    biServiceUrlManager.getShopSkuSellOrStock(),
                    JSON.toJSONString(param),
                    new TypeReference<ServiceResult<ShopSkuSellOrStock>>() {
                    }
            );
        }catch (Exception e){
            String message="url:" + biServiceUrlManager.getShopSkuSellOrStock() + ",param:" + JSON.toJSONString(param);
            log.error(message);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(res);
    }
}
