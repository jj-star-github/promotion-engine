package com.qmfresh.promotion.external.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.common.utils.RequestUtil;
import com.qmfresh.promotion.dto.SkuStockInShopQuery;
import com.qmfresh.promotion.bean.sellout.SkuStock;
import com.qmfresh.promotion.entity.ReceiveInfo;
import com.qmfresh.promotion.entity.WmsReceiveDetail;
import com.qmfresh.promotion.external.config.InventoryUrlManager;
import com.qmfresh.promotion.external.service.IInventoryService;
import com.qmfresh.promotion.platform.interfaces.platform.facade.settle.record.dto.WmsReceiveDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author xzw
 */
@Component
@Slf4j
public class InventoryServiceImpl implements IInventoryService {
    @Resource
    private InventoryUrlManager inventoryUrlManager;
    @Override
    public List<SkuStock> querySkuStockInShop(SkuStockInShopQuery param) {
        ServiceResult<List<SkuStock>> res;
        try{
            res = RequestUtil.postJson(
                    inventoryUrlManager.getQuerySkuStockUrl(),
                    JSON.toJSONString(param),
                    new TypeReference<ServiceResult<List<SkuStock>>>() {
                    }
            );
        }catch (Exception e){
            String message = "url:" + inventoryUrlManager.getQuerySkuStockUrl() + ",params:" + JSON.toJSONString(param);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(res);
    }

    @Override
    public List<WmsReceiveDetail> getwmsReceiveDetail(WmsReceiveDTO wmsReceiveDTO) {
        ServiceResult<List<WmsReceiveDetail>> res;
        try{
            res = RequestUtil.postJson(
                    inventoryUrlManager.getwmsReceiveDetail(),
                    JSON.toJSONString(wmsReceiveDTO),
                    new TypeReference<ServiceResult<List<WmsReceiveDetail>>>() {
                    }
            );
        }catch (Exception e){
            String message = "url:" + inventoryUrlManager.getwmsReceiveDetail() + ",params:" + JSON.toJSONString(wmsReceiveDTO);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(res);
    }

    @Override
    public List<ReceiveInfo> getReceiveInfo(WmsReceiveDTO wmsReceiveDTO) {
        ServiceResult<List<ReceiveInfo>> res;
        try{
            res = RequestUtil.postJson(
                    inventoryUrlManager.getReceiveInfo(),
                    JSON.toJSONString(wmsReceiveDTO),
                    new TypeReference<ServiceResult<List<ReceiveInfo>>>() {
                    }
            );
        }catch (Exception e){
            String message = "url:" + inventoryUrlManager.getReceiveInfo() + ",params:" + JSON.toJSONString(wmsReceiveDTO);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(res);
    }
}
