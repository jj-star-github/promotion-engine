package com.qmfresh.promotion.external.service;

import com.qmfresh.promotion.dto.ModifyPriceParam;
import com.qmfresh.promotion.dto.SendPromotionPriceParam;
import com.qmfresh.promotion.dto.SendVipPriceParam;
import com.qmfresh.promotion.dto.qmpp.QmppSkuDto;

import java.util.List;

/**
 * @ClassName IModifyPriceExternalApiService
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/6 17:48
 */
public interface IModifyPriceExternalApiService {

    Boolean modifyPromotionPrice(ModifyPriceParam<SendPromotionPriceParam> param);

    Boolean modifyVipPrice(ModifyPriceParam<SendVipPriceParam> param);

    List<QmppSkuDto> queryVipSkuList(ModifyPriceParam<SendVipPriceParam> param);
}
