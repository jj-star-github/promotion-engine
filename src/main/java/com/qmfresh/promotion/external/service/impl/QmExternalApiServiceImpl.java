package com.qmfresh.promotion.external.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.bean.gis.GisCity;
import com.qmfresh.promotion.bean.gis.Shop;
import com.qmfresh.promotion.bean.price.ProductSsuPriceResult;
import com.qmfresh.promotion.bean.price.ResetVipPriceParam;
import com.qmfresh.promotion.bean.price.ShopSsuQuery;
import com.qmfresh.promotion.bean.sellout.SkuPrice;
import com.qmfresh.promotion.bean.sku.*;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.common.utils.RequestUtil;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.dto.qmpp.Class2Result;
import com.qmfresh.promotion.dto.qmpp.QmppSkuDto;
import com.qmfresh.promotion.entity.AreaConditionQuery;
import com.qmfresh.promotion.entity.BaseArea;
import com.qmfresh.promotion.entity.ShopDetail;
import com.qmfresh.promotion.external.config.QmServiceUrlManager;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wyh on 2018/6/6.
 *
 * @author wyh
 */
@Service
@Slf4j
public class QmExternalApiServiceImpl implements IQmExternalApiService {


    @Resource
    private QmServiceUrlManager qmServiceUrlManager;


    @Override
    public List<Shop> getShopByCondition(Shop condition) {
        ServiceResult<List<Shop>> srResult;
        try {
            srResult = RequestUtil.postJson(
                qmServiceUrlManager.getShopByConditionUrl(),
                JSON.toJSONString(condition),
                new TypeReference<ServiceResult<List<Shop>>>() {
                }
            );
        } catch (Exception e) {
            String message = "url:" + qmServiceUrlManager.getShopByConditionUrl() + ",params:" + JSON.toJSONString(condition);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public List<Shop> selectShopByIds(List<Integer> shopIds) {
        ServiceResult<List<Shop>> srResult;
        try {
            srResult = RequestUtil.postJson(
                qmServiceUrlManager.getShopBatchIdsUrl(),
                JSON.toJSONString(shopIds),
                new TypeReference<ServiceResult<List<Shop>>>() {
                }
            );
        } catch (Exception e) {
            String message = "url:" + qmServiceUrlManager.getShopBatchIdsUrl() + ",params:" + JSON.toJSONString(shopIds);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public List<Sku> getBySkuCondition(SkuQuery condition) {
        ServiceResult<List<Sku>> srResult;
        try {
            srResult = RequestUtil.postJson(
                qmServiceUrlManager.querySkuUrl(),
                JSON.toJSONString(condition),
                new TypeReference<ServiceResult<List<Sku>>>() {
                }
            );
        } catch (Exception e) {
            String message = "url:" + qmServiceUrlManager.querySkuUrl() + ",params:" + JSON.toJSONString(condition);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public List<Sku> getBySkuIds(List<Integer> skuIds) {
        ServiceResult<List<Sku>> srResult;
        try {
            srResult = RequestUtil.postJson(
                qmServiceUrlManager.getBySkuIdsUrl(),
                JSON.toJSONString(skuIds),
                new TypeReference<ServiceResult<List<Sku>>>() {
                }
            );
        } catch (Exception e) {
            String message = "url:" + qmServiceUrlManager.getBySkuIdsUrl() + ",params:" + JSON.toJSONString(skuIds);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public List<Sku> getSku(List<Integer> skuIds) {
        ServiceResult<List<Sku>> srResult;
        try {
            srResult = RequestUtil.postJson(
                    qmServiceUrlManager.getSkuUrl(),
                    JSON.toJSONString(skuIds),
                    new TypeReference<ServiceResult<List<Sku>>>() {
                    }
            );
        } catch (Exception e) {
            String message = "url:" + qmServiceUrlManager.getSkuUrl() + ",params:" + JSON.toJSONString(skuIds);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public List<GisCity> getGisCity(GisCity condition) {
        ServiceResult<List<GisCity>> srResult;
        try {
            srResult = RequestUtil.postJson(
                qmServiceUrlManager.getGisCityUrl(),
                JSON.toJSONString(condition),
                new TypeReference<ServiceResult<List<GisCity>>>() {
                }
            );
        } catch (Exception e) {
            String message = "url:" + qmServiceUrlManager.getGisCityUrl() + ",params:" + JSON.toJSONString(condition);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public List<Shop> getShopByCityIds(List<Integer> cityIds) {
        ServiceResult<List<Shop>> srResult;
        try {
            srResult = RequestUtil.postJson(
                qmServiceUrlManager.getShopByCityIds(),
                JSON.toJSONString(cityIds),
                new TypeReference<ServiceResult<List<Shop>>>() {
                }
            );
        } catch (Exception e) {
            String message = "url:" + qmServiceUrlManager.getShopByCityIds() + ",params:" + JSON.toJSONString(cityIds);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public ProductSsu getProductSsuById(Integer ssuId) {
        ServiceResult<ProductSsu> srResult;
        try {
            srResult = RequestUtil.postJson(
                qmServiceUrlManager.getProductSsuById(),
                String.valueOf(ssuId),
                new TypeReference<ServiceResult<ProductSsu>>() {
                }
            );
        } catch (Exception e) {
            String message = "url:" + qmServiceUrlManager.getProductSsuById() + ",params:" + String.valueOf(ssuId);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public List<ProductSsu> listSsuByIds(List<Integer> ssuIds) {
        ServiceResult<List<ProductSsu>> srResult;
        try {
            srResult = RequestUtil.postJson(
                qmServiceUrlManager.listSsuByIds(),
                JSON.toJSONString(ssuIds),
                new TypeReference<ServiceResult<List<ProductSsu>>>() {
                }
            );
        } catch (Exception e) {
            String message = "url:" + qmServiceUrlManager.listSsuByIds() + ",params:" + JSON.toJSONString(ssuIds);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public List<ProductSsuFormatDto> getSsuFormatByCondition(QueryProductSsuFormat query) {
        ServiceResult<List<ProductSsuFormatDto>> srResult;
        try {
            srResult = RequestUtil.postJson(
                qmServiceUrlManager.getSsuFormatByCondition(),
                JSON.toJSONString(query),
                new TypeReference<ServiceResult<List<ProductSsuFormatDto>>>() {
                }
            );
        } catch (Exception e) {
            String message = "url:" + qmServiceUrlManager.getSsuFormatByCondition() + ",params:" + JSON.toJSONString(query);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public ListWithPage<ProductSsuPriceResult> getShopProductSsuPriceList(ShopSsuQuery query) {
        ServiceResult<ListWithPage<ProductSsuPriceResult>> srResult;
        try {
            srResult = RequestUtil.postJson(
                qmServiceUrlManager.getShopProductSsuPriceListUrl(),
                JSON.toJSONString(query),
                new TypeReference<ServiceResult<ListWithPage<ProductSsuPriceResult>>>() {
                }
            );
        } catch (Exception e) {
            String message = "url:" + qmServiceUrlManager.getShopProductSsuPriceListUrl() + ",params:" + JSON.toJSONString(query);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public Boolean resetVipPrice(ResetVipPriceParam param) {
        ServiceResult<Boolean> srResult;
        try {
            srResult = RequestUtil.postJson(
                qmServiceUrlManager.resetVipPriceUrl(),
                JSON.toJSONString(param),
                new TypeReference<ServiceResult<Boolean>>() {
                }
            );
        } catch (Exception e) {
            String message = "url:" + qmServiceUrlManager.resetVipPriceUrl() + ",params:" + JSON.toJSONString(param);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }



  /*  @Override
    public List<SkuSale> querySkuSalesVolumeInShop(SkuSale param) {
        ServiceResult<List<SkuSale>> res;
        try{
            res = RequestUtil.postJson(
                    qmServiceUrlManager.getQueryShopSkuSalesVolumeUrl(),
                    JSON.toJSONString(param),
                    new TypeReference<ServiceResult<List<SkuSale>>>() {
                    }
            );
        }catch (Exception e){
            String message="url:" + qmServiceUrlManager.getQueryShopSkuSalesVolumeUrl()+",param:"+JSON.toJSONString(param);
            logger.error(message);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(res);
    }*/

    @Override
    public List<SkuPrice> querySkuPriceInShop(List<SkuPrice> param) {
        ServiceResult<List<SkuPrice>> res;
        try{
            res = RequestUtil.postJson(
                    qmServiceUrlManager.getQueryShopSkuPriceUrl(),
                    JSON.toJSONString(param),
                    new TypeReference<ServiceResult<List<SkuPrice>>>() {
                    }
            );
        }catch (Exception e){
            String message="url:" + qmServiceUrlManager.getQueryShopSkuPriceUrl() + ",params:" + JSON.toJSONString(param);
            log.error(message);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(res);
    }

    @Override
    public Boolean updateSkuPriceInShop(List<SkuPrice> param) {
        ServiceResult<Boolean> res;
        try{
            res = RequestUtil.postJson(
                    qmServiceUrlManager.getUpdateShopSkuPriceUrl(),
                    JSON.toJSONString(param),
                    new TypeReference<ServiceResult<Boolean>>() {
                    }
            );
        }catch (Exception e){
            String message="url:" + qmServiceUrlManager.getUpdateShopSkuPriceUrl() + ",params:" + JSON.toJSONString(param);
            log.error(message);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(res);
    }

    @Override
    public ShopDetail getShopById(Integer shopId) {
        ServiceResult<ShopDetail> res;
        try{
            res = RequestUtil.postJson(
                    qmServiceUrlManager.getShopByIdUrl(),
                    JSON.toJSONString(shopId),
                    new TypeReference<ServiceResult<ShopDetail>>() {
                    }
            );
        }catch (Exception e){
            String message="url:" + qmServiceUrlManager.getShopByIdUrl() + ",shopId:" + JSON.toJSONString(shopId);
            log.error(message);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(res);
    }

    @Override
    public List<QmppSkuDto> getProductSsuPriceListByCondition(QueryProductSsuPrice param) {
        ServiceResult<List<QmppSkuDto>> srResult;
        try {
            srResult = RequestUtil.postJson(
                    qmServiceUrlManager.getProductSsuPriceListByCondition(),
                    JSON.toJSONString(param),
                    new TypeReference<ServiceResult<List<QmppSkuDto>>>() {
                    }
            );
        } catch (Exception e) {
            String message = "url:" + qmServiceUrlManager.getProductSsuPriceListByCondition() + ",params:" + JSON.toJSONString(param);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public List<Class2Result> getClass2s() {
        ServiceResult<List<Class2Result>> srResult;
        try {
            srResult = RequestUtil.postJson(
                    qmServiceUrlManager.getClass2List(),
                    null,
                    new TypeReference<ServiceResult<List<Class2Result>>>() {
                    }
            );
        } catch (Exception e) {
            String message = "url:" + qmServiceUrlManager.getClass2List();
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public List<BaseArea> selectAreaByShopIds(AreaConditionQuery param) {
        ServiceResult<List<BaseArea>> srResult;
        try {
            srResult = RequestUtil.postJson(
                    qmServiceUrlManager.selectAreaByShopIds(),
                    JSON.toJSONString(param),
                    new TypeReference<ServiceResult<List<BaseArea>>>() {
                    }
            );
        } catch (Exception e) {
            String message = "url:" + qmServiceUrlManager.selectAreaByShopIds() + ",params:" + JSON.toJSONString(param);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

}
