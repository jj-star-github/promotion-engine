package com.qmfresh.promotion.external.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.common.utils.RequestUtil;
import com.qmfresh.promotion.dto.ShopAuthorityResult;
import com.qmfresh.promotion.dto.qmpp.AuthenParam;
import com.qmfresh.promotion.external.config.QmopUrlManager;
import com.qmfresh.promotion.external.service.IQmopExternalApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName QmopExternalApiServiceImpl
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/6 14:29
 */
@Service
@Slf4j
public class QmopExternalApiServiceImpl implements IQmopExternalApiService {

    @Resource
    private QmopUrlManager qmopUrlManager;

    @Override
    public List<ShopAuthorityResult> findAuthorityByShopIdList(List<AuthenParam> authenParams) {
        ServiceResult<List<ShopAuthorityResult>> srResult;
        try {
            srResult = RequestUtil.postJson(
                    qmopUrlManager.getAuditedShopUrl(),
                    JSON.toJSONString(authenParams),
                    new TypeReference<ServiceResult<List<ShopAuthorityResult>>>() {
                    }
            );
        } catch (Exception e) {
            String message = "url:" + qmopUrlManager.getAuditedShopUrl() + ",params:" + JSON.toJSONString(authenParams);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }
}
