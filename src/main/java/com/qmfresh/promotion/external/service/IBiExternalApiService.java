package com.qmfresh.promotion.external.service;

import com.qmfresh.promotion.entity.ShopSku;
import com.qmfresh.promotion.entity.ShopSkuSellOrStock;

public interface IBiExternalApiService {
    /**
     * 查询近七天平均销量
     *
     * @param param
     * @return
     */
    ShopSkuSellOrStock getShopSkuSellOrStock(ShopSku param);
}
