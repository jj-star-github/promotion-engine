/*
 * Copyright (C) 2018-2021 Qm All rights reserved
 *      ┌─┐       ┌─┐ + +
 *   ┌──┘ ┴───────┘ ┴──┐++
 *   │       ───       │++ + + +
 *   ███████───███████ │+
 *   │       ─┴─       │
 *   └───┐         ┌───┘
 *       │         │   + +
 *       │         └──────────────┐
 *       │                        ├─┐
 *       │                        ┌─┘
 *       └─┐  ┐  ┌───────┬──┐  ┌──┘  + + + +
 *         │ ─┤ ─┤       │ ─┤ ─┤
 *         └──┴──┘       └──┴──┘  + + + +
 *                神兽保佑
 *               代码无BUG!
 */
package com.qmfresh.promotion.external.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.bean.sellout.DiscountModifyPriceDto;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.common.utils.RequestUtil;
import com.qmfresh.promotion.external.config.IQmppServiceUrlManager;
import com.qmfresh.promotion.external.service.IQmppExternalApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author lxc
 * @Date 2020/3/13 0013 17:51
 */
@Service
@Slf4j
public class QmppExternalApiServiceImpl implements IQmppExternalApiService {


    @Resource
    private IQmppServiceUrlManager qmppServiceUrlManager;

    @Override
    public Boolean noticeCenterUpdateSkuPrice(DiscountModifyPriceDto reqDto) {
        ServiceResult<Boolean> res;
        try {
            res = RequestUtil.postJson(
                    qmppServiceUrlManager.getNoticeModifyPriceUrl(),
                    JSON.toJSONString(reqDto),
                    new TypeReference<ServiceResult<Boolean>>() {
                    }
            );
        } catch (Exception e) {
            String message = "url:" + qmppServiceUrlManager.getNoticeModifyPriceUrl() + ",params:" + JSON.toJSONString(reqDto);
            log.error(message);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(res);
    }
}
