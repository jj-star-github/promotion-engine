package com.qmfresh.promotion.external.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.bean.coupon.*;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.common.utils.RequestUtil;
import com.qmfresh.promotion.dto.PromotionMzCampOnRsp;
import com.qmfresh.promotion.external.config.CouponUrlManager;
import com.qmfresh.promotion.external.service.ICouponExternalApiService;
import com.qmfresh.promotion.platform.interfaces.platform.facade.activity.CouponCheckResultDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wyh on 2018/6/6.
 *
 * @author wyh
 */
@Service
@Slf4j
public class CouponExternalApiServiceImpl implements ICouponExternalApiService {

    @Resource
    private CouponUrlManager couponUrlManager;

    @Override
    public List<CouponInfo> getCouponByCondition(CouponQuery condition) {
        ServiceResult<List<CouponInfo>> srResult;
        try {
            srResult = RequestUtil.postJson(
                couponUrlManager.getCouponByConditionUrl(),
                JSON.toJSONString(condition),
                new TypeReference<ServiceResult<List<CouponInfo>>>() {
                }
            );
        } catch (Exception e) {
            String message = "url:" + couponUrlManager.getCouponByConditionUrl() + ",params:" + JSON.toJSONString(condition);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public PromotionMzCampOnRsp queryCouponInfo(CouponQuery condition) {
        ServiceResult<PromotionMzCampOnRsp> srResult;
        try {
            srResult = RequestUtil.postJson(
                    couponUrlManager.queryCouponInfo(),
                    JSON.toJSONString(condition),
                    new TypeReference<ServiceResult<PromotionMzCampOnRsp>>() {
                    }
            );
        } catch (Exception e) {
            String message = "url:" + couponUrlManager.queryCouponInfo() + ",params:" + JSON.toJSONString(condition);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public List<SendCouponReturnBean> sendCoupon(SendCouponRequestBean requestBean) {
        ServiceResult<List<SendCouponReturnBean>> srResult;
        try {
            srResult = RequestUtil.postJson(
                    couponUrlManager.sendCoupon(),
                    JSON.toJSONString(requestBean),
                    new TypeReference<ServiceResult<List<SendCouponReturnBean>>>() {
                    }
            );
        } catch (Exception e) {
            String message = "url:" + couponUrlManager.sendCoupon() + ",params:" + JSON.toJSONString(requestBean);
            log.error(message, e);
            return null;
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public CouponCheckResultDTO checkCoupon(CouponCheckParam requestBean) {
        ServiceResult<CouponCheckResultDTO> srResult;
        try {
            srResult = RequestUtil.postJson(
                    couponUrlManager.checkCoupon(),
                    JSON.toJSONString(requestBean),
                    new TypeReference<ServiceResult<CouponCheckResultDTO>>() {
                    }
            );
        } catch (Exception e) {
            String message = "url:" + couponUrlManager.checkCoupon() + ",params:" + JSON.toJSONString(requestBean);
            log.error(message, e);
            return null;
        }
        return ServiceResultUtil.check(srResult);
    }

}
