package com.qmfresh.promotion.external.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.bean.sellout.SkuSale;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.common.utils.RequestUtil;
import com.qmfresh.promotion.external.config.CashServiceUrlManager;
import com.qmfresh.promotion.external.service.ICashExternalApiService;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.UpdateOrderList;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.dto.CashOrderDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
@Component
@Slf4j
public class CashExternalApiServiceImpl implements ICashExternalApiService {

    @Resource
    CashServiceUrlManager cashServiceUrlManager;


    @Override
    public List<SkuSale> querySkuSalesVolumeInShop(SkuSale query) {
        ServiceResult<List<SkuSale>> res;
        try{
            res = RequestUtil.postJson(
                    cashServiceUrlManager.getQueryShopSkuSalesVolumeUrl(),
                    JSON.toJSONString(query),
                    new TypeReference<ServiceResult<List<SkuSale>>>() {
                    }
            );
        }catch (Exception e){
            String message="url:" + cashServiceUrlManager.getQueryShopSkuSalesVolumeUrl()+",param:"+JSON.toJSONString(query);
            log.error(message);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(res);
    }

    @Override
    public List<CashOrderDTO> selectOrderMainItemByOrderCode(List<String> list) {
        ServiceResult<List<CashOrderDTO>> res;
        try{
            res = RequestUtil.postJson(
                    cashServiceUrlManager.selectOrderMainItemByOrderCode(),
                    JSON.toJSONString(list),
                    new TypeReference<ServiceResult<List<CashOrderDTO>>>() {
                    }
            );
        }catch (Exception e){
            String message="url:" + cashServiceUrlManager.selectOrderMainItemByOrderCode()+",param:"+JSON.toJSONString(list);
            log.error(message);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(res);
    }
}
