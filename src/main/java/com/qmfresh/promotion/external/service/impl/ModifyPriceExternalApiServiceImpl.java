package com.qmfresh.promotion.external.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.common.utils.RequestUtil;
import com.qmfresh.promotion.dto.ModifyPriceParam;
import com.qmfresh.promotion.dto.SendPromotionPriceParam;
import com.qmfresh.promotion.dto.SendVipPriceParam;
import com.qmfresh.promotion.dto.qmpp.QmppSkuDto;
import com.qmfresh.promotion.external.config.ModifyPriceUrlManager;
import com.qmfresh.promotion.external.service.IModifyPriceExternalApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName ModifyPriceExternalApiServiceImpl
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/6 17:49
 */
@Service
@Slf4j
public class ModifyPriceExternalApiServiceImpl implements IModifyPriceExternalApiService {


    @Resource
    private ModifyPriceUrlManager modifyPriceUrlManager;

    @Override
    public Boolean modifyPromotionPrice(ModifyPriceParam<SendPromotionPriceParam> param) {
        ServiceResult<Boolean> srResult;
        try {
            srResult = RequestUtil.postJson(
                    modifyPriceUrlManager.getModifyPromotionPriceUrl(),
                    JSON.toJSONString(param),
                    new TypeReference<ServiceResult<Boolean>>() {
                    }
            );
        } catch (Exception e) {
            String message = "url:" + modifyPriceUrlManager.getModifyPromotionPriceUrl() + ",params:" + JSON.toJSONString(param);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public Boolean modifyVipPrice(ModifyPriceParam<SendVipPriceParam> param) {
        ServiceResult<Boolean> srResult;
        try {
            srResult = RequestUtil.postJson(
                    modifyPriceUrlManager.getModifyVipPriceUrl(),
                    JSON.toJSONString(param),
                    new TypeReference<ServiceResult<Boolean>>() {
                    }
            );
        } catch (Exception e) {
            String message = "url:" + modifyPriceUrlManager.getModifyVipPriceUrl() + ",params:" + JSON.toJSONString(param);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }

    @Override
    public List<QmppSkuDto> queryVipSkuList(ModifyPriceParam<SendVipPriceParam> param) {
        ServiceResult<List<QmppSkuDto>> srResult;
        try {
            srResult = RequestUtil.postJson(
                    modifyPriceUrlManager.getQueryPriceListUrl(),
                    JSON.toJSONString(param),
                    new TypeReference<ServiceResult<List<QmppSkuDto>>>() {
                    }
            );
        } catch (Exception e) {
            String message = "url:" + modifyPriceUrlManager.getQueryPriceListUrl() + ",params:" + JSON.toJSONString(param);
            log.error(message, e);
            throw new RuntimeException(e);
        }
        return ServiceResultUtil.check(srResult);
    }
}
