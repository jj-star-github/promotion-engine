package com.qmfresh.promotion.external.service;

import com.qmfresh.promotion.bean.sellout.SkuSale;
import com.qmfresh.promotion.platform.infrastructure.mapper.settle.rule.entity.UpdateOrderList;
import com.qmfresh.promotion.platform.infrastructure.message.consumer.dto.CashOrderDTO;

import java.util.List;

/**
 * 收银端 服务
 */
public interface ICashExternalApiService {
    /**
     * 查询某个门店，旗下的SKU 的实时销量
     */
    List<SkuSale> querySkuSalesVolumeInShop(SkuSale query);

    List<CashOrderDTO> selectOrderMainItemByOrderCode(List<String> list);
}
