package com.qmfresh.promotion.enums;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public enum SsuTagTypeEnums {
    //排除标识：0.包含、1.排除

    CONTAIN(0, "包含"),
    EXCLUSIVE(1, "排除");

    private int code;
    private String detail;

    SsuTagTypeEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (SsuTagTypeEnums typeEnums : SsuTagTypeEnums.values()) {
            if (typeEnums.getCode() == code) {
                return typeEnums.getDetail();
            }
        }
        return null;
    }
}
