package com.qmfresh.promotion.enums;

public enum ProductScopeTypeEnums {
    //商品作用范围:1.一级分类,2.二级分类,3.三级分类,4.ssu,10.全场

    CLASS1(1, "一级分类"),
    CLASS2(2, "二级分类"),
    CLASS3(3, "三级分类"),
    SSU(4, "SSU"),
    ALL(10, "全场");

    private int code;
    private String detail;

    private ProductScopeTypeEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (ProductScopeTypeEnums typeEnums : ProductScopeTypeEnums.values()) {
            if (typeEnums.getCode() == code) {
                return typeEnums.getDetail();
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public static ProductScopeTypeEnums getEnum(int code) {
        for (ProductScopeTypeEnums enums : ProductScopeTypeEnums.values()) {
            if (enums.getCode() == code) {
                return enums;
            }
        }
        return null;
    }
}
