package com.qmfresh.promotion.enums;

public enum ChannelTypeEnums {
    //0所有平台,1.B端,2.C端线上,3.C端线下

    ALL(0, "所有平台"),
    B_ONLINE(1, "B端线上"),
    C_ONLINE(2, "C端线上"),
    C_OFFLINE(3, "C端线下");

    private int code;
    private String detail;

    private ChannelTypeEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (ChannelTypeEnums typeEnums : ChannelTypeEnums.values()) {
            if (typeEnums.getCode() == code) {
                return typeEnums.getDetail();
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
