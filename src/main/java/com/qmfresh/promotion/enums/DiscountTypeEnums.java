package com.qmfresh.promotion.enums;

public enum DiscountTypeEnums {
    //1.按单价减少一定金额，2.按折扣，3.按固定金额
    UNITPRICE(1, "按单价减少一定金额"),
    DISCOUNT(2, "按折扣"),
    FIXED(3, "按固定金额");

    private int code;
    private String detail;

    DiscountTypeEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (DiscountTypeEnums typeEnums : DiscountTypeEnums.values()) {
            if (typeEnums.getCode() == code) {
                return typeEnums.getDetail();
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public static DiscountTypeEnums getEnum(int code) {
        for (DiscountTypeEnums enums : DiscountTypeEnums.values()) {
            if (enums.getCode() == code) {
                return enums;
            }
        }
        return null;
    }
}
