package com.qmfresh.promotion.enums;

import lombok.Getter;
import lombok.ToString;

/**
 * @author xzw
 */
@Getter
@ToString
public enum PresentStatusEnums {
    //状态:0.失效，1.新建，2.生效(已绑定活动)
    INVALID(0, "失效"),
    NEW(1, "新建"),
    VALID(2, "生效(已绑定活动)");

    private int code;
    private String detail;

    PresentStatusEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (PresentStatusEnums typeEnums : PresentStatusEnums.values()) {
            if (typeEnums.getCode() == code) {
                return typeEnums.getDetail();
            }
        }
        return null;
    }

    public static PresentStatusEnums getEnum(int code) {
        for (PresentStatusEnums enums : PresentStatusEnums.values()) {
            if (enums.getCode() == code) {
                return enums;
            }
        }
        return null;
    }
}

