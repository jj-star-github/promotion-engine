package com.qmfresh.promotion.enums;

public enum PromotionTypeEnums {
    // 促销类型

    MZ(1, "满赠"),
    BZ(2, "买赠"),
    DP(3, "单品促销"),
    VIP(4, "会员促销"),
    MJ(5, "满减"),
    CP(6,"改价");
//    CQ(5, "出清");

    private int code;
    private String detail;

    private PromotionTypeEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (PromotionTypeEnums typeEnums : PromotionTypeEnums.values()) {
            if (typeEnums.getCode() == code) {
                return typeEnums.getDetail();
            }
        }
        return null;
    }

    public static PromotionTypeEnums getEnum(int code) {
        for (PromotionTypeEnums enums : PromotionTypeEnums.values()) {
            if (enums.getCode() == code) {
                return enums;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
