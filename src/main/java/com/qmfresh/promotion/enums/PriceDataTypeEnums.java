package com.qmfresh.promotion.enums;

public enum PriceDataTypeEnums {
	 FIXED(1, "按固定金额"),
	 DISCOUNT(2, "按折扣");    
	private int code;
    private String detail;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	private PriceDataTypeEnums(int code, String detail) {
		this.code = code;
		this.detail = detail;
	}
    
}
