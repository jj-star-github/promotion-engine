package com.qmfresh.promotion.enums;

/**
 * @ClassName PromotionCreatorTypeEnums
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/4 17:28
 */
public enum PromotionCreatorTypeEnums {

    HEAD_OFFICE(1,"运营"),
    SHOP(2,"门店");

    PromotionCreatorTypeEnums(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private int code;

    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }}
