package com.qmfresh.promotion.enums;

public enum AviatorRuleReturnEnums {
    //规则返回类型（1.布尔类型,2.数值型,3.自定义,4.AviatorObject）

    RT_BOOLEAN(1, "布尔类型"),
    RT_DECIMAL(2, "数值型"),
    RT_OBJECT(3, "自定义"),
    RT_AVIATOR_OBJECT(4, "AviatorObject");

    private int code;
    private String detail;

    private AviatorRuleReturnEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (AviatorRuleReturnEnums aviatorRuleEnums : AviatorRuleReturnEnums.values()) {
            if (aviatorRuleEnums.getCode() == code) {
                return aviatorRuleEnums.getDetail();
            }
        }
        return null;
    }

    public static AviatorRuleReturnEnums getEnum(int code) {
        for (AviatorRuleReturnEnums enums : AviatorRuleReturnEnums.values()) {
            if (enums.getCode() == code) {
                return enums;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
