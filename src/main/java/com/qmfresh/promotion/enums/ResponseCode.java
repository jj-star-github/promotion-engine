package com.qmfresh.promotion.enums;

public enum ResponseCode {
    /**
     * 参数无效
     */
    PARAMETER_INVALID(10001, "参数无效"),

    /**
     * 系统错误
     */
    SYSTEM_ERROR(500, "系统错误"),

    /**
     * 网络请求出错
     */
    NET_ERROR(502, "网络请求出错"),

    /**
     * API请求数据出错
     */
    API_ERROR(503, "API请求数据出错"),

    /**
     * DB ERROR
     */
    DB_ERROR(504, "DB ERROR"),

    /**
     * 操作成功
     */
    SUCCESS(0, "操作成功");

    private int code;
    private String detail;

    private ResponseCode(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
