package com.qmfresh.promotion.enums;

public enum PresentTypeEnums {
    //赠品类型:1.赠商品，2.赠券

    PRODUCT(1, "赠商品"),
    COUPON(2, "赠券");

    private int code;
    private String detail;

    private PresentTypeEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (PresentTypeEnums typeEnums : PresentTypeEnums.values()) {
            if (typeEnums.getCode() == code) {
                return typeEnums.getDetail();
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
