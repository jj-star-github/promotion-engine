package com.qmfresh.promotion.enums;

public enum AreaScopeTypeEnums {
    //区域范围：1.全国，2.城市，3.门店

    ALL(1, "全国"),
    CITY(2, "城市"),
    SHOP(3, "门店");

    private int code;
    private String detail;

    private AreaScopeTypeEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (AreaScopeTypeEnums typeEnums : AreaScopeTypeEnums.values()) {
            if (typeEnums.getCode() == code) {
                return typeEnums.getDetail();
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public static AreaScopeTypeEnums getEnum(int code) {
        for (AreaScopeTypeEnums enums : AreaScopeTypeEnums.values()) {
            if (enums.getCode() == code) {
                return enums;
            }
        }
        return null;
    }
}
