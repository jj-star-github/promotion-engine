package com.qmfresh.promotion.enums;

/**
 * 出清促销 配置状态
 */
public enum PromotionClearingConfigStatus {

    EFFECTIVE(0, "有效"),

    INVALID(1, "失效");

    private Integer code;

    private String detail;

    PromotionClearingConfigStatus(Integer code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
