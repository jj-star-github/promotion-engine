package com.qmfresh.promotion.enums;

import lombok.Getter;
import lombok.ToString;

/**
 * @author xzw
 */
@Getter
@ToString
public enum ActivityStatusTypeEnums {
    //生效状态:0.未进行,1.进行中，10已结束，11.已暂停
    NOT_START(0, "未进行"),
    STARTED(1, "进行中"),
    FINISH(10, "已结束"),
    STOP(11, "已暂停"),
    OUT_OF_DATE(12, "已过期"),
    UNAUDITED(101, "未审批"),
    APPROVED(102, "已同意"),
    REJECTED(103, "已拒绝"),
    DISABLE(104, "已失效");
    private int code;
    private String detail;

    ActivityStatusTypeEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (ActivityStatusTypeEnums typeEnums : ActivityStatusTypeEnums.values()) {
            if (typeEnums.getCode() == code) {
                return typeEnums.getDetail();
            }
        }
        return null;
    }

    public static ActivityStatusTypeEnums getEnum(int code) {
        for (ActivityStatusTypeEnums enums : ActivityStatusTypeEnums.values()) {
            if (enums.getCode() == code) {
                return enums;
            }
        }
        return null;
    }

}
