package com.qmfresh.promotion.enums;

public enum ActivityModifTypeEnums {
	INVALID(2,"失效"),
	EFFECT(1,"生效");	
	private int code;
    private String detail;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	private ActivityModifTypeEnums(int code, String detail) {
		this.code = code;
		this.detail = detail;
	}
    
}
