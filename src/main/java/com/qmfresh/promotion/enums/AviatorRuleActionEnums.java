package com.qmfresh.promotion.enums;

public enum AviatorRuleActionEnums {
    //规则执行动作（1：必须满足 2：满足跳过后面的验证 3：不满足跳过，只要后面有规则满足）

    MUST_SATISFY(1, "必须满足"),
    SATISFY_SKIP_LATER_CHECK(2, "满足跳过后面的验证"),
    UNSATISFY_LATER_HAVE_CHECK_SUCCESS(3, "不满足跳过，只要后面有规则满足");

    private int code;
    private String detail;

    private AviatorRuleActionEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (AviatorRuleActionEnums aviatorRuleEnums : AviatorRuleActionEnums.values()) {
            if (aviatorRuleEnums.getCode() == code) {
                return aviatorRuleEnums.getDetail();
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
