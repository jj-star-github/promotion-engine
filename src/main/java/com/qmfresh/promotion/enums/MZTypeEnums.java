package com.qmfresh.promotion.enums;

public enum MZTypeEnums {
    //满赠类型（1：阶梯满赠 2：每满赠）

    MUST_SATISFY(1, "阶梯满赠"),
    SATISFY_SKIP_LATER_CHECK(2, "每满赠");

    private int code;
    private String detail;

    private MZTypeEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (MZTypeEnums typeEnums : MZTypeEnums.values()) {
            if (typeEnums.getCode() == code) {
                return typeEnums.getDetail();
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
