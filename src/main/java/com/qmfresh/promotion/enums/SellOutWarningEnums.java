package com.qmfresh.promotion.enums;

/**
 * @program: promotion-engine
 * @description: 出清报警状态枚举值
 * @author: xbb
 * @create: 2019-12-09 09:44
 **/
public enum SellOutWarningEnums {

    NOT_SEND(0,"未发送至门店"),

    ALREADY_SEND(1,"已发送至门店"),

    ERROR_WARNING(2,"错误预警，无需发送给门店");

    private int code;

    private String desc;

    SellOutWarningEnums(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
