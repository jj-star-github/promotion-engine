package com.qmfresh.promotion.enums;

public enum CrowdTypeEnums {
    //用户类型（1.全部，2.非会员，10.普通会员，11.黄金会员，3.青番茄，4.红番茄,5.金番茄）

    ALL(1, "全部"),
    NOT_VIP(2, "非会员"),
    NORMAL_VIP(10, "普通会员"),
    GOLD_VIP(11, "黄金会员"),
    GREEN_TOMATO(3,"青番茄"),
    RED_TOMATO(4,"红番茄"),
    GOLD_TOMATO(5,"金番茄");

    private int code;
    private String detail;

    private CrowdTypeEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (CrowdTypeEnums typeEnums : CrowdTypeEnums.values()) {
            if (typeEnums.getCode() == code) {
                return typeEnums.getDetail();
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
