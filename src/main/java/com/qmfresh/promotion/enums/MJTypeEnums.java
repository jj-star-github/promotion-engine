package com.qmfresh.promotion.enums;

public enum MJTypeEnums {
    //满减类型（1：阶梯满减 2：每满减）

    MUST_SATISFY(1, "阶梯满减"),
    SATISFY_SKIP_LATER_CHECK(2, "每满减");

    private int code;
    private String detail;

    private MJTypeEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (MJTypeEnums typeEnums : MJTypeEnums.values()) {
            if (typeEnums.getCode() == code) {
                return typeEnums.getDetail();
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
