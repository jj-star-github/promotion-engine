package com.qmfresh.promotion.enums;

import lombok.Getter;
import lombok.ToString;

/**
 * @author xzw
 */
@Getter
@ToString
public enum ActivityOnlineTypeEnums {
    //是否上线:0.下线，1.上线

    OFFLINE(0, "下线"),
    ONLINE(1, "上线");

    private int code;
    private String detail;

    ActivityOnlineTypeEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (ActivityOnlineTypeEnums typeEnums : ActivityOnlineTypeEnums.values()) {
            if (typeEnums.getCode() == code) {
                return typeEnums.getDetail();
            }
        }
        return null;
    }
}
