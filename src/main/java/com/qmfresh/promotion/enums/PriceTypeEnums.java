package com.qmfresh.promotion.enums;

public enum PriceTypeEnums {
    //商品价格类型：1.普通商品，2.会员商品，3.手工改价商品，4.促销商品

    NORMAL(1, "普通商品"),
    VIP(2, "会员商品"),
    MODIFY(3, "手工改价商品"),
    DISCOUNT(4, "促销商品");

    private int code;
    private String detail;

    private PriceTypeEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (PriceTypeEnums typeEnums : PriceTypeEnums.values()) {
            if (typeEnums.getCode() == code) {
                return typeEnums.getDetail();
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
