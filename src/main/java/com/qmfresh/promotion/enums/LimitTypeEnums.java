package com.qmfresh.promotion.enums;

public enum LimitTypeEnums {
    //1.单店单日限购，2.单店单日单人限购
    SINGLE_DAY_PERSON(1, "单日单人限购"),
    SINGLE_DAY_SHOP_PERSON(2, "单店单日单人限购");

    private int code;
    private String detail;

    private LimitTypeEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public static String getName(int code) {
        for (LimitTypeEnums typeEnums : LimitTypeEnums.values()) {
            if (typeEnums.getCode() == code) {
                return typeEnums.getDetail();
            }
        }
        return null;
    }

    public static LimitTypeEnums getEnum(int code) {
        for (LimitTypeEnums enums : LimitTypeEnums.values()) {
            if (enums.getCode() == code) {
                return enums;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
