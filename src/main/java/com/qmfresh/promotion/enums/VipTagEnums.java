package com.qmfresh.promotion.enums;

/**
 * @ClassName VipTagEnums
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/5 9:15
 */
public enum VipTagEnums {

    IS_VIP(1,"会员商品"),

    NOT_VIP(0,"非会员商品");

    private int code;

    private String desc;

    VipTagEnums(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
