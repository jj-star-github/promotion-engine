package com.qmfresh.promotion.enums;

/**
 * @version v1.0
 * @ProjectName: promotion-engine
 * @ClassName: CreatorFromValueTranslateEnums
 * @Description: TODO
 * @Author: xbb
 * @Date: 2020/3/6 20:52
 */
public enum CreatorFromValueTranslateEnums {

    /**
     * 促销改价，总部运营对应值
     */
    HEAD_OFFICE_TO_MID_PLATFORM(1,2,"总部运营"),

    /**
     * 促销改价，门店对应值
     */
    SHOP_TO_MID_PLATFORM(2,3,"门店");

    /**
     * 本系统对应的值
     */
    private int codeOne;

    /**
     * 对接系统对应的值
     */
    private int codeTwo;

    private String desc;

    CreatorFromValueTranslateEnums(int codeOne, int codeTwo, String desc) {
        this.codeOne = codeOne;
        this.codeTwo = codeTwo;
        this.desc = desc;
    }

    public int getCodeOne() {
        return codeOne;
    }

    public void setCodeOne(int codeOne) {
        this.codeOne = codeOne;
    }

    public int getCodeTwo() {
        return codeTwo;
    }

    public void setCodeTwo(int codeTwo) {
        this.codeTwo = codeTwo;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static Integer getCode2ByCode1(Integer codeOne){
        for(CreatorFromValueTranslateEnums flagValueTranslateEnum : CreatorFromValueTranslateEnums.values()){
            if(codeOne.equals(flagValueTranslateEnum.getCodeOne())){
                return flagValueTranslateEnum.getCodeTwo();
            }
        }
        return null;
    }

    public static Integer getCode1ByCode2(Integer codeTwo){
        for(CreatorFromValueTranslateEnums cfvt : CreatorFromValueTranslateEnums.values()){
            if(codeTwo.equals(cfvt.getCodeTwo())){
                return cfvt.getCodeOne();
            }
        }
        return null;
    }
}
