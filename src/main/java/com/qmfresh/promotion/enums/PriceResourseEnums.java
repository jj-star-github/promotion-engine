package com.qmfresh.promotion.enums;

public enum PriceResourseEnums {
	
	VIP(4,"会员价"),
	CP(2,"活动价"),
	CQ(3,"出清价");
	private Integer code;
	private String detail;
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	private PriceResourseEnums(Integer code, String detail) {
		this.code = code;
		this.detail = detail;
	}
	
}
