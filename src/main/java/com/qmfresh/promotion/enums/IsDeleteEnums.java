package com.qmfresh.promotion.enums;

public enum IsDeleteEnums {

    NO(0, "未删除"),
    YES(1, "已删除");

    private int code;
    private String detail;

    IsDeleteEnums(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
