package com.qmfresh.promotion.enums;

/**
 * @ClassName StatusValueTranslateEnums
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/7 10:10
 */
public enum StatusValueTranslateEnums {

    NOT_AUTHORIZED_TO_MIDPLATFORM(101,0,"未审批"),

    APPROVED_TO_MIDPLATFORM(102,1,"已同意"),

    REJECTED_TO_MIDPLATFORM(103,2,"已拒绝"),

    OUT_OF_DATE_TO_MIDPLATFORM(12,3,"已过期"),

    DISABLE(104, 6, "已失效");

    /**
     * 本系统状态值
     */
    private Integer codeOne;

    /**
     * 对接系统状态值
     */
    private Integer codeTwo;

    private String desc;

    StatusValueTranslateEnums(Integer codeOne, Integer codeTwo, String desc) {
        this.codeOne = codeOne;
        this.codeTwo = codeTwo;
        this.desc = desc;
    }

    public Integer getCodeOne() {
        return codeOne;
    }

    public void setCodeOne(Integer codeOne) {
        this.codeOne = codeOne;
    }

    public Integer getCodeTwo() {
        return codeTwo;
    }

    public void setCodeTwo(Integer codeTwo) {
        this.codeTwo = codeTwo;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static Integer getCode1ByCode2(Integer codeTwo){
        for(StatusValueTranslateEnums svte : StatusValueTranslateEnums.values()){
            if(svte.getCodeTwo().equals(codeTwo)){
                return svte.getCodeOne();
            }
        }
        return null;
    }

    public static Integer getCode2ByCode1(Integer codeOne){
        for(StatusValueTranslateEnums svte : StatusValueTranslateEnums.values()){
            if(svte.getCodeOne().equals(codeOne)){
                return svte.getCodeTwo();
            }
        }
        return null;
    }
}
