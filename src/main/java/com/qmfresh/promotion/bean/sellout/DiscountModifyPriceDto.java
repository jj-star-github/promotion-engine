/*
 * Copyright (C) 2019-2020 Qm All rights reserved
 *      ┌─┐       ┌─┐ + +
 *   ┌──┘ ┴───────┘ ┴──┐++
 *   │       ───       │++ + + +
 *   ███████───███████ │+
 *   │       ─┴─       │
 *   └───┐         ┌───┘
 *       │         │   + +
 *       │         └──────────────┐
 *       │                        ├─┐
 *       │                        ┌─┘
 *       └─┐  ┐  ┌───────┬──┐  ┌──┘  + + + +
 *         │ ─┤ ─┤       │ ─┤ ─┤
 *         └──┴──┘       └──┴──┘  + + + +
 *                神兽保佑
 *               代码无BUG!
 */
package com.qmfresh.promotion.bean.sellout;

import javax.validation.Valid;
import java.util.List;

/**
 * @author lxc
 * @Date 2020/3/4 0004 9:25
 */
public class DiscountModifyPriceDto {
    
    @Valid
    private List<AutomaticDiscountReq> reqList;

    public List<AutomaticDiscountReq> getReqList() {
        return reqList;
    }

    public void setReqList(List<AutomaticDiscountReq> reqList) {
        this.reqList = reqList;
    }
}
