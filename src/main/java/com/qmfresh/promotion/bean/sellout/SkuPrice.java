package com.qmfresh.promotion.bean.sellout;

import java.math.BigDecimal;

public class SkuPrice {
    //门店ID
    private Integer shopId;

    //SKU ID
    private Integer skuId;

    //实时价格
    private BigDecimal price;


    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

}
