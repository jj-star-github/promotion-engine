package com.qmfresh.promotion.bean.sellout;

import java.math.BigDecimal;

/**
 * 门店实时库存信息
 */
public class SkuStock {
    private Long id;
    /**
     * 原材料id
     */
    private Long skuId;
    /**
     * 仓库id
     */
    private Long warehouseId;
    /**
     * 总库存量（物理单位）--数量
     */
    private BigDecimal amount;
    /**
     * 总库存量（计价单位）--重量
     */
    private BigDecimal amountFormat;

    private Integer isWeight;

    public SkuStock() {
        this.amount = BigDecimal.ZERO;
        this.amountFormat = BigDecimal.ZERO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmountFormat() {
        return amountFormat;
    }

    public void setAmountFormat(BigDecimal amountFormat) {
        this.amountFormat = amountFormat;
    }

    public Integer getIsWeight() {
        return isWeight;
    }

    public void setIsWeight(Integer isWeight) {
        this.isWeight = isWeight;
    }
}
