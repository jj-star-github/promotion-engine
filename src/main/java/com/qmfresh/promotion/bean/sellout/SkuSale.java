package com.qmfresh.promotion.bean.sellout;

import java.math.BigDecimal;
import java.util.List;

/**
 * 销量反馈实体类
 */
public class SkuSale {
    /**
     * 门店ID
     */
    private Integer shopId;

    /**
     * SKU ID
     */
    private Integer skuId;

    /**
     * 销量
     */
    private BigDecimal total;


    /**
     * SKU ID集合
     */
    private List<Integer> skuIds;

    public SkuSale() {
        this.total = BigDecimal.ZERO;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public List<Integer> getSkuIds() {
        return skuIds;
    }

    public void setSkuIds(List<Integer> skuIds) {
        this.skuIds = skuIds;
    }
}
