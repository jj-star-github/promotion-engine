package com.qmfresh.promotion.bean.sellout;

import java.math.BigDecimal;

/**
 * 推送至收银端消息体
 */
public class SkuMessage {

    /**
     * 门店ID
     */
    private Integer shopId;

    /**
     * 门店名称
     */
    private String shopName;

    /*
     * SKU ID
     */
    private Integer skuId;

    /**
     * SKU
     */
    private String skuName;

    /**
     * 本次预警触发时间 不包含年月日，只包含时分
     */
    private String time;

    /**
     * 预警价格
     */
    private BigDecimal price;

    /**
     * 预警折扣
     */
    private BigDecimal warningDiscount;

    /**
     * 预警级别
     */
    private Integer warningLevel;

    /**
     * 预警方式 0、广播播报 1、电视播报
     */
    private Integer warningWay;

    /**
     * 预警详情
     */
    private String warningDetail;

    /**
     * 播报开始时间
     */
    private Integer start;

    /**
     * 播报结束时间
     */
    private Integer end;

    /**
     * 促销通用话术
     */
    private String word;


    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getWarningDiscount() {
        return warningDiscount;
    }

    public void setWarningDiscount(BigDecimal warningDiscount) {
        this.warningDiscount = warningDiscount;
    }

    public Integer getWarningLevel() {
        return warningLevel;
    }

    public void setWarningLevel(Integer warningLevel) {
        this.warningLevel = warningLevel;
    }

    public Integer getWarningWay() {
        return warningWay;
    }

    public void setWarningWay(Integer warningWay) {
        this.warningWay = warningWay;
    }

    public String getWarningDetail() {
        return warningDetail;
    }

    public void setWarningDetail(String warningDetail) {
        this.warningDetail = warningDetail;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getEnd() {
        return end;
    }

    public void setEnd(Integer end) {
        this.end = end;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
