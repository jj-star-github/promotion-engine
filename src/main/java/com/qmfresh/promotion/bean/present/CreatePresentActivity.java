package com.qmfresh.promotion.bean.present;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by wyh on 2019/6/11.
 *
 * @author wyh
 */
public class CreatePresentActivity implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    /**
     * 赠品活动类型：1.促销活动，2.积分兑换活动
     */
    @NotNull(message = "赠品活动类型不能为空")
    private Integer activityType;

    /**
     * 赠品活动名称
     */
    @NotNull(message = "赠品活动名称不能为空")
    private String name;
    @NotNull(message = "赠品不能为空")
    private Integer ssuFormatId;

    /**
     * 限量
     */
    @NotNull(message = "赠品限量不能为空")
    private BigDecimal limitNum;

    /**
     * 活动开始时间
     */
    @NotNull(message = "赠品活动开始时间不能为空")
    private Integer effectBeginTime;
    /**
     * 活动结束时间
     */
    @NotNull(message = "赠品活动结束时间不能为空")
    private Integer effectEndTime;

    /**
     * 创建人id
     */
    @NotNull(message = "创建人id不能为空")
    private Long createUserId;

    /**
     * 创建人名称
     */
    @NotNull(message = "创建人名称不能为空")
    private String createUserName;

    private Integer id;

    /**
     * 渠道，线上还是线下1:线上,其他线下
     */
    private Integer channelType;
    /**
     * ssu名称
     */
    private String ssuName;

    private Integer skuId;

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public Integer getChannelType() {
        return channelType;
    }

    public void setChannelType(Integer channelType) {
        this.channelType = channelType;
    }

    public String getSsuName() {
        return ssuName;
    }

    public void setSsuName(String ssuName) {
        this.ssuName = ssuName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getActivityType() {
        return activityType;
    }

    public void setActivityType(Integer activityType) {
        this.activityType = activityType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSsuFormatId() {
        return ssuFormatId;
    }

    public void setSsuFormatId(Integer ssuFormatId) {
        this.ssuFormatId = ssuFormatId;
    }

    public BigDecimal getLimitNum() {
        return limitNum;
    }

    public void setLimitNum(BigDecimal limitNum) {
        this.limitNum = limitNum;
    }

    public Integer getEffectBeginTime() {
        return effectBeginTime;
    }

    public void setEffectBeginTime(Integer effectBeginTime) {
        this.effectBeginTime = effectBeginTime;
    }

    public Integer getEffectEndTime() {
        return effectEndTime;
    }

    public void setEffectEndTime(Integer effectEndTime) {
        this.effectEndTime = effectEndTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }
}
