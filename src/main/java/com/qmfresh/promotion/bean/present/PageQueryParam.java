package com.qmfresh.promotion.bean.present;

import com.qmfresh.promotion.dto.base.PageQuery;

/**
 * Created by wyh on 2019/6/11.
 *
 * @author wyh
 */
public class PageQueryParam extends PageQuery {

    /**
     * 赠品活动类型：1.促销活动，2.积分兑换活动
     */
    private Integer activityType;

    /**
     * 赠品活动名称
     */
    private String presentName;

    /**
     * ssu名称
     */
    private String ssuName;

    /**
     * 促销活动名称
     */
    private Long activityId;

    /**
     * ssu_format_id
     */
    private Integer ssuFormatId;

    /**
     * sku
     */
    private Integer skuId;

    private Integer status;

    private Integer beginTime;

    private Integer endTime;
    
    private Integer nowTime;

    public Integer getNowTime() {
        return nowTime;
    }

    public void setNowTime(Integer nowTime) {
        this.nowTime = nowTime;
    }

    public Integer getActivityType() {
        return activityType;
    }

    public void setActivityType(Integer activityType) {
        this.activityType = activityType;
    }

    public String getPresentName() {
        return presentName;
    }

    public void setPresentName(String presentName) {
        this.presentName = presentName;
    }

    public String getSsuName() {
        return ssuName;
    }

    public void setSsuName(String ssuName) {
        this.ssuName = ssuName;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Integer getSsuFormatId() {
        return ssuFormatId;
    }

    public void setSsuFormatId(Integer ssuFormatId) {
        this.ssuFormatId = ssuFormatId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Integer beginTime) {
        this.beginTime = beginTime;
    }

    public Integer getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }
}
