package com.qmfresh.promotion.bean.present;

import com.qmfresh.promotion.entity.PresentActivity;
import com.qmfresh.promotion.entity.PresentSsu;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wyh on 2019/6/11.
 *
 * @author wyh
 */
public class PresentActivityDto implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    private PresentActivity presentActivity;

    private List<PresentSsu> presentSsus;

    public PresentActivity getPresentActivity() {
        return presentActivity;
    }

    public void setPresentActivity(PresentActivity presentActivity) {
        this.presentActivity = presentActivity;
    }

    public List<PresentSsu> getPresentSsus() {
        return presentSsus;
    }

    public void setPresentSsus(List<PresentSsu> presentSsus) {
        this.presentSsus = presentSsus;
    }
}
