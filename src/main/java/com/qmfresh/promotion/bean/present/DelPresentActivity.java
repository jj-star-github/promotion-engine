/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/18
 * Description:DelPresentActivity.java
 */
package com.qmfresh.promotion.bean.present;

import java.io.Serializable;

/**
 * @author lxc
 */
public class DelPresentActivity implements Serializable {
    
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
