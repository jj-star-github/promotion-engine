/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/7/17
 * Description:VipBindExchangeActivity.java
 */
package com.qmfresh.promotion.bean.present;

import java.io.Serializable;
import java.util.List;

/**
 * @author lxc
 */
public class VipBindExchangeActivity implements Serializable {

    private Long activityId;
    
    private List<Long> presentIds;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public List<Long> getPresentIds() {
        return presentIds;
    }

    public void setPresentIds(List<Long> presentIds) {
        this.presentIds = presentIds;
    }
}
