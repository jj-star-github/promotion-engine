package com.qmfresh.promotion.bean.present;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by wyh on 2019/6/11.
 *
 * @author wyh
 */
public class UpdatePresentActivity implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    private Long presentId;

    /**
     * 赠品活动名称
     */
    private String name;
    private Integer ssuFormatId;

    /**
     * 限量
     */
    private BigDecimal limitNum;

    /**
     * 活动开始时间
     */
    private Integer effectBeginTime;
    /**
     * 活动结束时间
     */
    private Integer effectEndTime;

    /**
     * 创建人id
     */
    private Long updateUserId;

    /**
     * 创建人名称
     */
    private String updateUserName;

    /**
     * 变更状态：-1.删除
     */
    private Integer status;

    public Long getPresentId() {
        return presentId;
    }

    public void setPresentId(Long presentId) {
        this.presentId = presentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSsuFormatId() {
        return ssuFormatId;
    }

    public void setSsuFormatId(Integer ssuFormatId) {
        this.ssuFormatId = ssuFormatId;
    }

    public BigDecimal getLimitNum() {
        return limitNum;
    }

    public void setLimitNum(BigDecimal limitNum) {
        this.limitNum = limitNum;
    }

    public Integer getEffectBeginTime() {
        return effectBeginTime;
    }

    public void setEffectBeginTime(Integer effectBeginTime) {
        this.effectBeginTime = effectBeginTime;
    }

    public Integer getEffectEndTime() {
        return effectEndTime;
    }

    public void setEffectEndTime(Integer effectEndTime) {
        this.effectEndTime = effectEndTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    public String getUpdateUserName() {
        return updateUserName;
    }

    public void setUpdateUserName(String updateUserName) {
        this.updateUserName = updateUserName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
