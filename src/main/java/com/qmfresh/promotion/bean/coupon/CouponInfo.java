package com.qmfresh.promotion.bean.coupon;

import java.math.BigDecimal;

/**
 * Created by wyh on 2019/8/1.
 *
 * @author wyh
 */
public class CouponInfo {

    /**
     * 优惠券活动id
     */
    private Long couponId;

    /**
     * 优惠券名称
     */
    private String couponName;

    /**
     * 赠送量
     */
    private BigDecimal givingNum;

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public BigDecimal getGivingNum() {
        return givingNum;
    }

    public void setGivingNum(BigDecimal givingNum) {
        this.givingNum = givingNum;
    }

}
