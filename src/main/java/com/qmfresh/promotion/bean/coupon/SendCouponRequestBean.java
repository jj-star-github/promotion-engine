package com.qmfresh.promotion.bean.coupon;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 发放优惠券请求
 */
@Data
public class SendCouponRequestBean implements Serializable {
    
    private static final long serialVersionUID = 3541248946779625659L;
    /**
     * 会员号，非必填
     */
    private Long userId;
    /**
     * 请求发放数量
     */
    private Integer sendNum;
    /**
     * 券活动id
     */
    private Integer activityId;
    /**
     * 来源订单id
     */
    private String sourceOrderNo;
    /**
     * 发券的门店id
     */
    private Integer sourceShopId;
    /**
     * 限定渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;
    /**
     * 实付
     */
    private BigDecimal realAmount;

    /**
     * 优惠券码
     */
    private List<String> couponCode;

}
