package com.qmfresh.promotion.bean.coupon;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 优惠券发放返回数据
 */
@Data
@ApiModel(value = "SendCouponReturnBean", description = "领取奖励返回")
public class SendCouponReturnBean implements Serializable {
    
    private static final long serialVersionUID = -2634608309253218048L;
    
    /**
     * 券Id
     */
    private Integer couponId;
    /**
     * 券名称
     */
    private String couponName;
    /**
     * 券码
     */
    private String couponCode;
    /**
     * 券使用说明
     */
    private String useInstruction;
    /**
     * 有效期
     */
    private String validityPeriod;
    /**
     * 优惠券绑定的订单号
     */
    private String sourceOrderNo;
    /**
     * 券抵扣金额
     */
    private BigDecimal couponPrice;
    /**
     * 发券的门店id
     */
    private Integer sourceShopId;
    /**
     * 用券的门店id
     */
    private Integer useShopId;
    /**
     * 优惠金额
     */
    private BigDecimal subMoney;
    /**
     * 折扣
     */
    private Integer couponDiscount;
    /**
     * 优惠券类型(1:满减,2:折扣,3:随机金额)
     */
    private Integer couponType;
    /**
     * 券适用商品类型(1:全部可用2:指定可用3:指定不可用)
     */
    private Integer applicableGoodsType;

    /**
     * -1：已过期，1: 明日过期，2：后日过期,3：两天后过期
     */
    private Integer expiredFlag;

    /**
     * 优惠券使用时间
     */
    private Integer useTime;

    /**
     * 券类型(1:满减,2:折扣,3:随机金额)
     */
    private Integer couponCondition;

    /**
     * 发券时间
     */
    private Integer createTime;

    /**
     * 限定渠道(0:全渠道,1:线下，2:线上)
     */
    private Integer limitChannel;

    /**
     * 券活动名称
     */
    private String couponActivityName;

}
