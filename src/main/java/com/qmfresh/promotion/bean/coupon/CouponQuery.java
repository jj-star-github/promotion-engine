package com.qmfresh.promotion.bean.coupon;

import lombok.Data;

/**
 * Created by wyh on 2019/8/4.
 *
 * @author wyh
 */
@Data
public class CouponQuery {

    private Long couponId;

    private Integer channel;

    private Integer status;

    private Long activityId;

    /**
     * 发券的门店id
     */
    private Integer sourceShopId;

    private Integer sendNum;

}
