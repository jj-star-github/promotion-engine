package com.qmfresh.promotion.bean.coupon;

import com.qmfresh.promotion.dto.PromotionSsuContext;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 优惠券查询检查
 *
 **/
@Data
public class CouponCheckParam {

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 来源系统：拼团，线上，线下......(保留字段)
     */
    private Integer sourceSystem;

    /**
     * 商品信息数组
     */
    private List<GoodsInfo> goodsList;

    /**
     * 门店id
     */
    private Integer shopId;

    /**
     * 优惠券码
     */
    private String couponCode;

    /**
     * 渠道编码
     */
    private Integer bizChannel;


    @Data
    public static class GoodsInfo {

        /**
         * SKUID
         */
        private Integer skuId;

        /**
         * 价格
         */
        private BigDecimal price;

        /**
         * 下单量
         */
        private BigDecimal amount;

        private Integer class1Id;

        private Integer class2Id;

        public GoodsInfo () {}

        public GoodsInfo (PromotionSsuContext ssuContext) {
            this.setAmount(ssuContext.getAmount());
            this.setSkuId(ssuContext.getSkuId());
            this.setClass1Id(ssuContext.getClass1Id());
            this.setClass2Id(ssuContext.getClass2Id());
            this.setPrice(ssuContext.getPreferentialPrice());
        }

    }
}
