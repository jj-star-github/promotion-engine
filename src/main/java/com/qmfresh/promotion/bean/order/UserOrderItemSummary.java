package com.qmfresh.promotion.bean.order;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by wyh on 2019/6/3.
 *
 * @author wyh
 */
public class UserOrderItemSummary implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 门店id
     */
    private Integer shopId;

    /**
     * 售卖商品id
     */
    private Integer ssuId;

    /**
     * 销售总价
     */
    private BigDecimal price;

    /**
     * 退货总价
     */
    private BigDecimal returnPrice;

    /**
     * 总价
     */
    private BigDecimal totalPrice;

    /**
     * 购买数量
     */
    private BigDecimal amount;

    /**
     * 退货量
     */
    private BigDecimal returnAmount;

    /**
     * 实际数量
     */
    private BigDecimal totalAmount;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSsuId() {
        return ssuId;
    }

    public void setSsuId(Integer ssuId) {
        this.ssuId = ssuId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(BigDecimal returnPrice) {
        this.returnPrice = returnPrice;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getReturnAmount() {
        return returnAmount;
    }

    public void setReturnAmount(BigDecimal returnAmount) {
        this.returnAmount = returnAmount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public UserOrderItemSummary() {
        this.price = BigDecimal.ZERO;
        this.returnPrice = BigDecimal.ZERO;
        this.totalPrice = BigDecimal.ZERO;
        this.amount = BigDecimal.ZERO;
        this.returnAmount = BigDecimal.ZERO;
        this.totalAmount = BigDecimal.ZERO;
    }
}
