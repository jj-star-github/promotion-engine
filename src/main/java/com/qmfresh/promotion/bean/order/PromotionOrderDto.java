package com.qmfresh.promotion.bean.order;

import com.qmfresh.promotion.entity.PromotionOrderItem;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by wyh on 2019/6/14.
 *
 * @author wyh
 */
@Data
public class PromotionOrderDto implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    private String orderId;
    /**
     * 渠道
     */
    private Integer channel;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 门店Id
     */
    private Integer shopId;
    /**
     * 创建日期
     */
    private String createDate;
    /**
     * 业务发生时间
     */
    private Integer occurrenceTime;
    /**
     * 订单类型:1.正向，2.逆向
     */
    private Integer orderType;
    /**
     * 订单状态:0.无效，1.有效
     */
    private Integer status;
    /**
     * 原金额
     */
    private BigDecimal originTotalPrice;
    /**
     * 优惠金额（抹零、使用优惠券等优惠金额）
     */
    private BigDecimal preferentialPrice;
    /**
     * 总金额（实际支付）
     */
    private BigDecimal totalPrice;

    private List<PromotionOrderItem> items;
}
