package com.qmfresh.promotion.bean.order;

import java.io.Serializable;

/**
 * Created by wyh on 2019/6/3.
 *
 * @author wyh
 */
public class QueryOrderParam implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    private Long userId;

    private Integer shopId;

    private Integer skuId;

    private Integer beginTime;

    private Integer endTime;

    /**
     * 0.全部，1.普通会员，2.黄金会员
     */
    private Integer userType;

    private Integer channel;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public Integer getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Integer beginTime) {
        this.beginTime = beginTime;
    }

    public Integer getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }
}
