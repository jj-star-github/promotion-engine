package com.qmfresh.promotion.bean.mark;

import java.math.BigDecimal;

/**
 * Created by wyh on 2019/7/29.
 *
 * @author wyh
 */
public class VipMark {

    /**
     * 促销活动id
     */
    private Long activityId;

    /**
     * 会员价
     */
    private BigDecimal vipPrice;

    /**
     * 限购量
     */
    private BigDecimal limitNum;

    /**
     * 生效状态:0.未进行,1.进行中，10已结束，11.已暂停
     */
    private Integer status;

    /**
     * 会员活动描述
     */
    private String description;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public BigDecimal getVipPrice() {
        return vipPrice;
    }

    public void setVipPrice(BigDecimal vipPrice) {
        this.vipPrice = vipPrice;
    }

    public BigDecimal getLimitNum() {
        return limitNum;
    }

    public void setLimitNum(BigDecimal limitNum) {
        this.limitNum = limitNum;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
