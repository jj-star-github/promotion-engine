package com.qmfresh.promotion.bean.mark;

/**
 * Created by wyh on 2019/7/29.
 *
 * @author wyh
 */
public class SsuformatMark {

    private Integer shopId;

    private Integer ssuFormatId;

    private Integer updateTime;

    private VipMark vipMark;

    private MzMark mzMark;

    private MjMark mjMark;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSsuFormatId() {
        return ssuFormatId;
    }

    public void setSsuFormatId(Integer ssuFormatId) {
        this.ssuFormatId = ssuFormatId;
    }

    public Integer getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Integer updateTime) {
        this.updateTime = updateTime;
    }

    public VipMark getVipMark() {
        return vipMark;
    }

    public void setVipMark(VipMark vipMark) {
        this.vipMark = vipMark;
    }

    public MzMark getMzMark() {
        return mzMark;
    }

    public void setMzMark(MzMark mzMark) {
        this.mzMark = mzMark;
    }

    public MjMark getMjMark() {
        return mjMark;
    }

    public void setMjMark(MjMark mjMark) {
        this.mjMark = mjMark;
    }
}
