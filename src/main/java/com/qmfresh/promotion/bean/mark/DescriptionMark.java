package com.qmfresh.promotion.bean.mark;

/**
 * Created by wyh on 2019/7/29.
 *
 * @author wyh
 */
public class DescriptionMark {

    private Integer seq;

    private String description;

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
