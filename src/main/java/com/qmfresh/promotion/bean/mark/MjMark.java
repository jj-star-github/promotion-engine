package com.qmfresh.promotion.bean.mark;

import java.util.List;

/**
 * Created by wyh on 2019/7/29.
 *
 * @author wyh
 */
public class MjMark {
    /**
     * 促销活动id
     */
    private Long activityId;

    /**
     * 活动开始时间
     */
    private Integer startTime;

    /**
     * 活动结束时间
     */
    private Integer endTime;

    /**
     * 生效状态:0.未进行,1.进行中，10已结束，11.已暂停
     */
    private Integer status;

    /**
     * 满赠活动描述
     */
    private List<DescriptionMark> descriptions;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Integer getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<DescriptionMark> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<DescriptionMark> descriptions) {
        this.descriptions = descriptions;
    }
}
