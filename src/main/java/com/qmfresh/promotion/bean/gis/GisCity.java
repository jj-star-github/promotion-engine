package com.qmfresh.promotion.bean.gis;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @since 2018-05-02
 */
public class GisCity implements Serializable {
    private static final long serialVersionUID = 2974233873713137077L;

    private Integer id;
    private String name;
    private String shortName;
    private Integer status;
    private Integer cT;
    private Integer uT;
    /**
     * 开始售卖时间
     */
    private String startTime;
    /**
     * 截止时间
     */
    private String endTime;
    /**
     * 是否允许下单
     */
    private Integer isOrder;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCT() {
        return cT;
    }

    public void setCT(Integer cT) {
        this.cT = cT;
    }

    public Integer getUT() {
        return uT;
    }

    public void setUT(Integer uT) {
        this.uT = uT;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getIsOrder() {
        return isOrder;
    }

    public void setIsOrder(Integer isOrder) {
        this.isOrder = isOrder;
    }

}
