package com.qmfresh.promotion.bean.gis;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 店铺基本信息
 * </p>
 *
 * @author xiaowei
 * @since 2018-05-02
 */
public class Shop implements Serializable {
	private static final long serialVersionUID = 7812556364757470645L;

	private Long id;
	private Integer shopType;
	private String shopName;
	private Integer cityId;
	private String shopDesc;
	private Integer uT;
	private Integer cT;
	private Integer isDeleted;
	private Integer areaId;
    /**
     * 售卖渠道
     */
	private Integer sellChannel;
	private Integer status;
	private String locationLan;
	private String locationLgt;
    /**
     * 开始售卖时间
     */
	private String startTime;
    /**
     * 截单时间
     */
	private String endTime;
    /**
     * 是否开放下单
     */
	private Integer isOrder;

	private BigDecimal startSendMoney;

	private String address;
	private String name;
	private String phone;
	private Integer advancePayment;

	private Integer warehouseId;

	private String warehouseName;

	private Integer promotionGrayType;

	public Integer getPromotionGrayType() {
		return promotionGrayType;
	}

	public void setPromotionGrayType(Integer promotionGrayType) {
		this.promotionGrayType = promotionGrayType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getShopType() {
		return shopType;
	}

	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public String getShopDesc() {
		return shopDesc;
	}

	public void setShopDesc(String shopDesc) {
		this.shopDesc = shopDesc;
	}

	public Integer getUT() {
		return uT;
	}

	public void setUT(Integer uT) {
		this.uT = uT;
	}

	public Integer getCT() {
		return cT;
	}

	public void setCT(Integer cT) {
		this.cT = cT;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public Integer getSellChannel() {
		return sellChannel;
	}

	public void setSellChannel(Integer sellChannel) {
		this.sellChannel = sellChannel;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getLocationLan() {
		return locationLan;
	}

	public void setLocationLan(String locationLan) {
		this.locationLan = locationLan;
	}

	public String getLocationLgt() {
		return locationLgt;
	}

	public void setLocationLgt(String locationLgt) {
		this.locationLgt = locationLgt;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getIsOrder() {
		return isOrder;
	}

	public void setIsOrder(Integer isOrder) {
		this.isOrder = isOrder;
	}

	public BigDecimal getStartSendMoney() {
		return startSendMoney;
	}

	public void setStartSendMoney(BigDecimal startSendMoney) {
		this.startSendMoney = startSendMoney;
	}

	public Integer getAdvancePayment() {
		return advancePayment;
	}

	public void setAdvancePayment(Integer advancePayment) {
		this.advancePayment = advancePayment;
	}

	public Integer getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Integer warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}
}
