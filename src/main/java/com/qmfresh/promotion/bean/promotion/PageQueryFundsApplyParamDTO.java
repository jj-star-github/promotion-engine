package com.qmfresh.promotion.bean.promotion;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@ApiModel("分页查询营销费申请记录入参")
@ToString
public class PageQueryFundsApplyParamDTO extends BaseDTO {
    @ApiModelProperty("分页大小")
    private  int start;

    private  int totalCount;

    private int end;
    @ApiModelProperty("分页大小")
    private  int pageSize;
    @ApiModelProperty("页码")
    private  int pageIndex;

    private Integer shopType;

    public Integer getShopType() {
        return shopType;
    }

    public void setShopType(Integer shopType) {
        this.shopType = shopType;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public int getEnd() {
        return pageSize;
    }


    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getStart() {

        return (pageIndex-1)*pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public void setStart(int start) {
        this.start = start;
    }
    public PageQueryFundsApplyParamDTO(){
        this.start  = 0;
        this.end = 20;
        this.pageIndex = 1;
        this.pageSize = 20;
    }

}
