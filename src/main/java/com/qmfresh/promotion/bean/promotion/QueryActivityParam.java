package com.qmfresh.promotion.bean.promotion;

import com.qmfresh.promotion.entity.PromotionActivityTime;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wyh on 2019/5/24.
 *
 * @author wyh
 */
public class QueryActivityParam implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;
    /**
     * 促销模板id
     */
    private Integer moduleId;

    private List<Integer> moduleIds;

    /**
     * 活动开始时间
     */
    private Integer effectBeginTime;
    /**
     * 活动结束时间
     */
    private Integer effectEndTime;

    private List<Integer> weekDays;

    private List<PromotionActivityTime> activityTimes;

    /**
     * 是否上线:0.下线，1.上线
     */
    private Integer isOnline;

    /**
     * 是否上线:0.失效，1.生效
     */
    private Integer status;

    private List<Integer> statusList;

    private Integer shopId;

    private List<Integer> shopIds;

    private Integer channel;

    /**
     * 活动创建来源 1：运营，2：门店
     */
    private Integer creatorFrom;

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public List<Integer> getModuleIds() {
        return moduleIds;
    }

    public void setModuleIds(List<Integer> moduleIds) {
        this.moduleIds = moduleIds;
    }

    public Integer getEffectBeginTime() {
        return effectBeginTime;
    }

    public void setEffectBeginTime(Integer effectBeginTime) {
        this.effectBeginTime = effectBeginTime;
    }

    public Integer getEffectEndTime() {
        return effectEndTime;
    }

    public void setEffectEndTime(Integer effectEndTime) {
        this.effectEndTime = effectEndTime;
    }

    public List<Integer> getWeekDays() {
        return weekDays;
    }

    public void setWeekDays(List<Integer> weekDays) {
        this.weekDays = weekDays;
    }

    public List<PromotionActivityTime> getActivityTimes() {
        return activityTimes;
    }

    public void setActivityTimes(List<PromotionActivityTime> activityTimes) {
        this.activityTimes = activityTimes;
    }

    public Integer getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(Integer isOnline) {
        this.isOnline = isOnline;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Integer> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Integer> statusList) {
        this.statusList = statusList;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public List<Integer> getShopIds() {
        return shopIds;
    }

    public void setShopIds(List<Integer> shopIds) {
        this.shopIds = shopIds;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public Integer getCreatorFrom() {
        return creatorFrom;
    }

    public void setCreatorFrom(Integer creatorFrom) {
        this.creatorFrom = creatorFrom;
    }
}
