package com.qmfresh.promotion.bean.promotion;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class MzCampOn implements Serializable {
    private static final long serialVersionUID = 4857929662017099474L;

    //活动ID
    private Long activityId;
    //优惠券ID
    private Long couponId;
    //赠送数量
    private Integer givingNum;

}
