package com.qmfresh.promotion.bean.promotion;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 *运营经费申请审核记录
 */
@ApiModel("运营经费申请审核记录列表明细对象")
@Data
@ToString
public class FundsApplyCheckRecordDto {
    //申请单编号
    @ApiModelProperty("申请单编号")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;
    /**
     * 申请人用户名
     */
    @ApiModelProperty("申请人用户名")
    private String applyUserName;

    /**
     * 申请额度
     */
    @ApiModelProperty("申请额度")
    private BigDecimal applyAmount;

    /**
     * 申请描述
     */
    @ApiModelProperty("申请描述")
    private String applyRemark;

    /**
     * 申请类型，1.门店，2.总部
     */
    @ApiModelProperty("申请类型，1.门店，2.总部")
    private Integer applyType;
    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private  String applyName;

    /**
     * 审核状态:0.审核中，4.通过，5.驳回
     */
    @ApiModelProperty(" 审核状态:0.审核中，4.通过，5.驳回")
    private int status;

    /**
     * 提交时间
     */
    @ApiModelProperty( "提交时间")
    private Integer gmtCreate;


    /**
     * 当前金额
     */
    @ApiModelProperty("当前金额")
    private BigDecimal currentAmount;
    /**
     * 审核人用户名
     */
    @ApiModelProperty("审核人用户名")
    private String examineUserName;

    /**
     * 审核时间
     */
    @ApiModelProperty("审核时间")
    private Integer examineTime;

    /**
     * 审核描述
     */
    @ApiModelProperty("审核描述")
    private String examineRemark;


    @ApiModelProperty("发起者名称")
    private String originatorName;
}
