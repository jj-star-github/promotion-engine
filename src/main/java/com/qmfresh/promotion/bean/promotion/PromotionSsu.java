package com.qmfresh.promotion.bean.promotion;

import java.io.Serializable;
import java.math.BigDecimal;

public class PromotionSsu implements Serializable {
    private static final long serialVersionUID = 4857929662017099474L;

    private Integer labelId;

    private Integer skuId;
    /**
     * 门店售卖ssu
     */
    private Integer ssuId;
    private Integer ssuFormatId;
    private BigDecimal amount;

    /**
     * 原价
     */
    private BigDecimal originPrice;

    /**
     * 会员价
     */
    private BigDecimal vipPrice;

    /**
     * 折后价
     */
    private BigDecimal priceAfterDiscount;
    /**
     * 售卖二级分类
     */
    private Integer class2Id;

    private Integer class1Id;

    /**
     * 商品价格类型：1.普通商品，2.会员商品，3.手工改价商品，4.促销商品
     */
    private Integer priceType;

    public PromotionSsu () {}

    public PromotionSsu (PromotionSsu copySsu) {
        this.setAmount(BigDecimal.ZERO);
        this.setPriceAfterDiscount(copySsu.getPriceAfterDiscount());
        this.setPriceType(copySsu.getPriceType());
        this.setVipPrice(copySsu.getVipPrice());
        this.setOriginPrice(copySsu.getOriginPrice());
        this.setClass2Id(copySsu.getClass2Id());
        this.setClass1Id(copySsu.getClass1Id());
        this.setSkuId(copySsu.getSkuId());
        this.setSsuFormatId(copySsu.getSsuFormatId());
        this.setLabelId(copySsu.getLabelId());
        this.setSsuId(copySsu.getSsuId());
    }

    public Integer getLabelId() {
        return labelId;
    }

    public void setLabelId(Integer labelId) {
        this.labelId = labelId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public Integer getSsuId() {
        return ssuId;
    }

    public void setSsuId(Integer ssuId) {
        this.ssuId = ssuId;
    }

    public Integer getSsuFormatId() {
        return ssuFormatId;
    }

    public void setSsuFormatId(Integer ssuFormatId) {
        this.ssuFormatId = ssuFormatId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(BigDecimal originPrice) {
        this.originPrice = originPrice;
    }

    public BigDecimal getVipPrice() {
        return vipPrice;
    }

    public void setVipPrice(BigDecimal vipPrice) {
        this.vipPrice = vipPrice;
    }

    public BigDecimal getPriceAfterDiscount() {
        return priceAfterDiscount;
    }

    public void setPriceAfterDiscount(BigDecimal priceAfterDiscount) {
        this.priceAfterDiscount = priceAfterDiscount;
    }

    public Integer getClass2Id() {
        return class2Id;
    }

    public void setClass2Id(Integer class2Id) {
        this.class2Id = class2Id;
    }

    public Integer getClass1Id() {
        return class1Id;
    }

    public void setClass1Id(Integer class1Id) {
        this.class1Id = class1Id;
    }

    public Integer getPriceType() {
        return priceType;
    }

    public void setPriceType(Integer priceType) {
        this.priceType = priceType;
    }
}
