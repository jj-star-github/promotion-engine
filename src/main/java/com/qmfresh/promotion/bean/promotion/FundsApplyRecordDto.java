package com.qmfresh.promotion.bean.promotion;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

/**
 *运营经费申请记录
 */
@ApiModel("运营经费申请记录")
@Data
@ToString
public class FundsApplyRecordDto {
    //标题
    @ApiModelProperty("标题")
    private  String  title;
    //申请时间
    @ApiModelProperty("申请时间")
    private Integer applyTime;
    //状态statu 审核状态:0.审核中，4.通过，5.驳回
    @ApiModelProperty("状态statu 审核状态:0.审核中，4.通过，5.驳回")
    private Integer status;
    //金额
    @ApiModelProperty("金额")
    private BigDecimal amount ;
    //审核记录
    @ApiModelProperty("审核记录")
    private  List<CheckRecordDto> checkRecords;
    //申请单详情
    @ApiModelProperty("申请单详情")
    private  FundsApplyBillDto fundsApplyBill;


}
