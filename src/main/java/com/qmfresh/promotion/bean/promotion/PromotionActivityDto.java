package com.qmfresh.promotion.bean.promotion;

import com.qmfresh.promotion.entity.ActivitySsu;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionActivityTime;

import java.util.List;

/**
 * Created by wyh on 2019/6/15.
 *
 * @author wyh
 */
public class PromotionActivityDto {

    private PromotionActivity promotionActivity;

    private List<PromotionActivityTime> promotionActivityTimes;

    private String description;

    private List<ActivitySsu> activitySsuList;

    private Integer shopId;

    private String shopName;

    public PromotionActivity getPromotionActivity() {
        return promotionActivity;
    }

    public void setPromotionActivity(PromotionActivity promotionActivity) {
        this.promotionActivity = promotionActivity;
    }

    public List<PromotionActivityTime> getPromotionActivityTimes() {
        return promotionActivityTimes;
    }

    public void setPromotionActivityTimes(
        List<PromotionActivityTime> promotionActivityTimes) {
        this.promotionActivityTimes = promotionActivityTimes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ActivitySsu> getActivitySsuList() {
        return activitySsuList;
    }

    public void setActivitySsuList(List<ActivitySsu> activitySsuList) {
        this.activitySsuList = activitySsuList;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
