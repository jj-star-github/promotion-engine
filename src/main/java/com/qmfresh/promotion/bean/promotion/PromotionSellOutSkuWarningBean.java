package com.qmfresh.promotion.bean.promotion;

import java.io.Serializable;
import java.util.List;

/**
 * 预警主信息表
 */
public class PromotionSellOutSkuWarningBean implements Serializable{
    /**
     * 主键
     */
    private Integer id;

    /**
     * 城市ID
     */
    private Integer cityId;

    /**
     * 城市名称
     */
    private String cityName;

    /**
     * 虚拟仓ID
     */
    private Integer warehouseId;

    /**
     * 门店ID
     */
    private Integer shopId;

    /**
     * 门店名称
     */
    private String shopName;
    /**
     * 门店类型
     */
    private Integer shopType;
    /**
     * SKU ID
     */
    private Integer skuId;
    /**
     * SKU 名称
     */
    private String skuName;
    /**
     * 是否计重
     */
    private Integer isWeight;
    /**
     * 上次预警时间
     */
    private Integer lastWarningTime;

    /**
     * 操作人ID
     */
    private Integer createdId;
    /**
     * 操作人名称
     */
    private String createdName;
    /**
     * 状态
     */
    private Integer status;

    /**
     * 未触发预警的原因
     */
    private String msg;

    /**
     * 创建时间
     */
    private Integer cT;
    /**
     * 更新时间
     */
    private Integer uT;
    /**
     * 是否删除
     */
    private Integer isDeleted;

    /**
     * 预警的规则明细集合
     */
    private List<PromotionSellOutWarningRuleBean> warnings;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getShopType() {
        return shopType;
    }

    public void setShopType(Integer shopType) {
        this.shopType = shopType;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public Integer getIsWeight() {
        return isWeight;
    }

    public void setIsWeight(Integer isWeight) {
        this.isWeight = isWeight;
    }

    public Integer getLastWarningTime() {
        return lastWarningTime;
    }

    public void setLastWarningTime(Integer lastWarningTime) {
        this.lastWarningTime = lastWarningTime;
    }

    public Integer getCreatedId() {
        return createdId;
    }

    public void setCreatedId(Integer createdId) {
        this.createdId = createdId;
    }

    public String getCreatedName() {
        return createdName;
    }

    public void setCreatedName(String createdName) {
        this.createdName = createdName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getcT() {
        return cT;
    }

    public void setcT(Integer cT) {
        this.cT = cT;
    }

    public Integer getuT() {
        return uT;
    }

    public void setuT(Integer uT) {
        this.uT = uT;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public List<PromotionSellOutWarningRuleBean> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<PromotionSellOutWarningRuleBean> warnings) {
        this.warnings = warnings;
    }

}
