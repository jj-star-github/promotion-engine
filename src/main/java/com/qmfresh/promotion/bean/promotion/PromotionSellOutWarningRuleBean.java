package com.qmfresh.promotion.bean.promotion;


import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 出清预警规则
 */
public class PromotionSellOutWarningRuleBean implements Serializable{
    /**
     * 序号
     */
    private Integer id;

    /**
     * 预警触发时间即截止时间、展示使用
     */
    private Integer date;
    /**
     * 预警触发时间即截止时间、不包含年月日，只包含时分
     */
    private String time;

    /**
     * 去化率 /(排面量/实时库存)*100%  阈值
     */
    private BigDecimal removalRateThreshold;

    /**
     * 预警前价格
     */
    private BigDecimal warningPrice;

    /**
     * 预警折扣
     */
    private BigDecimal warningDiscount;

    /**
     * 预警级别
     */
    private Integer warningLevel;

    /**
     * 预警方式 0、广播播报 1、电视播报
     */
    private Integer warningWay;

    /**
     * 预警详情
     */
    private String warningDetail;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BigDecimal getRemovalRateThreshold() {
        return removalRateThreshold;
    }

    public void setRemovalRateThreshold(BigDecimal removalRateThreshold) {
        this.removalRateThreshold = removalRateThreshold;
    }

    public BigDecimal getWarningPrice() {
        return warningPrice;
    }

    public void setWarningPrice(BigDecimal warningPrice) {
        this.warningPrice = warningPrice;
    }

    public BigDecimal getWarningDiscount() {
        return warningDiscount;
    }

    public void setWarningDiscount(BigDecimal warningDiscount) {
        this.warningDiscount = warningDiscount;
    }

    public Integer getWarningLevel() {
        return warningLevel;
    }

    public void setWarningLevel(Integer warningLevel) {
        this.warningLevel = warningLevel;
    }

    public Integer getWarningWay() {
        return warningWay;
    }

    public void setWarningWay(Integer warningWay) {
        this.warningWay = warningWay;
    }

    public String getWarningDetail() {
        return warningDetail;
    }

    public void setWarningDetail(String warningDetail) {
        this.warningDetail = warningDetail;
    }
}
