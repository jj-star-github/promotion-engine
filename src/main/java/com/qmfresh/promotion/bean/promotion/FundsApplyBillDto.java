package com.qmfresh.promotion.bean.promotion;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 *申请单信息
 */
@ApiModel("申请单信息")
@Data
@ToString
public class FundsApplyBillDto {
    //金额
    @ApiModelProperty("金额")
    private BigDecimal money;
    //原因
    @ApiModelProperty("原因")
    private String  reason;
    //申请时间
    @ApiModelProperty("申请时间")
    private  Integer applyTime;


}
