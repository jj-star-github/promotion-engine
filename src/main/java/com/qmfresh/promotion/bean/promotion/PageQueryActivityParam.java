package com.qmfresh.promotion.bean.promotion;

import com.qmfresh.promotion.dto.base.PageQuery;
import lombok.Data;

import java.util.List;

/**
 * Created by wyh on 2019/5/24.
 *
 * @author wyh
 */
@Data
public class PageQueryActivityParam extends PageQuery {

    /**
     * 促销模板id
     */
    private Integer moduleId;

    private List<Long> moduleIds;

    /**
     * 促销名称(模糊匹配)
     */
    private String activityName;

    private Integer status;

    private Integer queryShopId;

    private Integer creatorFrom;

    private Integer startTime;

    private Integer endTime;

    private Integer id;

    private Integer skuId;

}
