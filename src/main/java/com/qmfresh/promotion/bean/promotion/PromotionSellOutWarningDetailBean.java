package com.qmfresh.promotion.bean.promotion;

import java.math.BigDecimal;

/**
 * @program: promotion-engine
 * @description: 预警详情
 * @author: xbb
 * @create: 2019-11-23 14:03
 **/
public class PromotionSellOutWarningDetailBean {

    private Long id;
    /**
     * 城市ID
     */
    private Integer cityId;
    /**
     * 城市名称
     */
    private String cityName;
    /**
     * 门店ID
     */
    private Integer shopId;
    /**
     * 门店名称
     */
    private String shopName;
    /**
     * SKU ID
     */
    private Integer skuId;
    /**
     * SKU 名称
     */
    private String skuName;
    /**
     * 去化率阈值
     */
    private BigDecimal removalRateThreshold;
    /**
     * 商品原价
     */
    private BigDecimal skuInitialPrice;
    /**
     * 预警后价格
     */
    private BigDecimal warningPrice;
    /**
     * 出清预警主表 主键ID
     */
    private Long sellOutWarningId;
    /**
     * 预警 具体时间
     */
    private String warningTime;
    /**
     * 预警 日期
     */
    private Integer warningDate;
    /**
     * 预警 级别
     */
    private Integer warningLevel;
    /**
     * 预警 方式 0、广播播报 1、电视播报
     */
    private Integer warningWay;
    /**
     * 预警 内容
     */
    private String warningDetail;
    /**
     * 创建时间
     */
    private Integer cT;
    /**
     * 更新时间
     */
    private Integer uT;
    /**
     * 是否删除
     */
    private Integer isDeleted;

    private Integer warningStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public BigDecimal getRemovalRateThreshold() {
        return removalRateThreshold;
    }

    public void setRemovalRateThreshold(BigDecimal removalRateThreshold) {
        this.removalRateThreshold = removalRateThreshold;
    }

    public BigDecimal getSkuInitialPrice() {
        return skuInitialPrice;
    }

    public void setSkuInitialPrice(BigDecimal skuInitialPrice) {
        this.skuInitialPrice = skuInitialPrice;
    }

    public BigDecimal getWarningPrice() {
        return warningPrice;
    }

    public void setWarningPrice(BigDecimal warningPrice) {
        this.warningPrice = warningPrice;
    }

    public Long getSellOutWarningId() {
        return sellOutWarningId;
    }

    public void setSellOutWarningId(Long sellOutWarningId) {
        this.sellOutWarningId = sellOutWarningId;
    }

    public String getWarningTime() {
        return warningTime;
    }

    public void setWarningTime(String warningTime) {
        this.warningTime = warningTime;
    }

    public Integer getWarningDate() {
        return warningDate;
    }

    public void setWarningDate(Integer warningDate) {
        this.warningDate = warningDate;
    }

    public Integer getWarningLevel() {
        return warningLevel;
    }

    public void setWarningLevel(Integer warningLevel) {
        this.warningLevel = warningLevel;
    }

    public Integer getWarningWay() {
        return warningWay;
    }

    public void setWarningWay(Integer warningWay) {
        this.warningWay = warningWay;
    }

    public String getWarningDetail() {
        return warningDetail;
    }

    public void setWarningDetail(String warningDetail) {
        this.warningDetail = warningDetail;
    }

    public Integer getcT() {
        return cT;
    }

    public void setcT(Integer cT) {
        this.cT = cT;
    }

    public Integer getuT() {
        return uT;
    }

    public void setuT(Integer uT) {
        this.uT = uT;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getWarningStatus() {
        return warningStatus;
    }

    public void setWarningStatus(Integer warningStatus) {
        this.warningStatus = warningStatus;
    }
}
