package com.qmfresh.promotion.bean.promotion;

import com.qmfresh.promotion.platform.interfaces.common.BaseDTO;

import java.util.List;

/**
 * Created by wyh on 2019/6/15.
 *
 * @author wyh
 */
public class PromotionActivityBean extends BaseDTO {

    private Long promotionActivityId;

    /**
     * 促销活动名称
     */
    private String activityName;

    /**
     * 促销类型
     */
    private Integer promotionType;

    /**
     * 生效时间
     */
    private String effectTime;

    /**
     * 规则描述
     */
    private String ruleDescription;

    /**
     * 渠道
     */
    private Integer channel;

    private Integer status;

    /**
     * 活动创建来源 1：总部运营 2：门店
     */
    private Integer creatorFrom;

    /**
     * 创建时间
     */
    private Integer cT;

    /**
     * 更新时间
     */
    private Integer uT;

    /**
     * 创建人
     */
    private String createUserName;

    private List<Integer> skuIds;

    private List<String> skuNames;

    private Integer shopId;

    private String shopName;

    public Long getPromotionActivityId() {
        return promotionActivityId;
    }

    public void setPromotionActivityId(Long promotionActivityId) {
        this.promotionActivityId = promotionActivityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Integer getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(Integer promotionType) {
        this.promotionType = promotionType;
    }

    public String getEffectTime() {
        return effectTime;
    }

    public void setEffectTime(String effectTime) {
        this.effectTime = effectTime;
    }

    public String getRuleDescription() {
        return ruleDescription;
    }

    public void setRuleDescription(String ruleDescription) {
        this.ruleDescription = ruleDescription;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCreatorFrom() {
        return creatorFrom;
    }

    public void setCreatorFrom(Integer creatorFrom) {
        this.creatorFrom = creatorFrom;
    }

    public Integer getcT() {
        return cT;
    }

    public void setcT(Integer cT) {
        this.cT = cT;
    }

    public Integer getuT() {
        return uT;
    }

    public void setuT(Integer uT) {
        this.uT = uT;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public List<Integer> getSkuIds() {
        return skuIds;
    }

    public void setSkuIds(List<Integer> skuIds) {
        this.skuIds = skuIds;
    }

    public List<String> getSkuNames() {
        return skuNames;
    }

    public void setSkuNames(List<String> skuNames) {
        this.skuNames = skuNames;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
