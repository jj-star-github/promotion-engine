package com.qmfresh.promotion.bean.promotion;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 *运营经费申请记录
 */
@ApiModel("运营经费申请记录")
@Data
@ToString
public class CheckRecordDto {
    //审核状态:0.审核中，4.通过，5.驳回
    @ApiModelProperty("审核状态:0.审核中，4.通过，5.驳回")
    private Integer checkStatus;
    //操作时间
    @ApiModelProperty("操作时间")
    private Integer opTime;


}
