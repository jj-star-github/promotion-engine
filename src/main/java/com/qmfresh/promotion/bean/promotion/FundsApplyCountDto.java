package com.qmfresh.promotion.bean.promotion;

import java.math.BigDecimal;

/**
 *运营经费统计
 */
public class FundsApplyCountDto {
    //待审核数量
    private Integer waitCheckNum;
    /**
     * 审核通过数量
     */
    private Integer passCheckNum;

    /**
     * 待审核金额
     */
    private BigDecimal waitCheckMoney;

    /**
     * 审核通过金额
     */
    private BigDecimal passCheckMoney;

    public Integer getWaitCheckNum() {
        return waitCheckNum;
    }

    public void setWaitCheckNum(Integer waitCheckNum) {
        this.waitCheckNum = waitCheckNum;
    }

    public Integer getPassCheckNum() {
        return passCheckNum;
    }

    public void setPassCheckNum(Integer passCheckNum) {
        this.passCheckNum = passCheckNum;
    }

    public BigDecimal getWaitCheckMoney() {
        return waitCheckMoney;
    }

    public void setWaitCheckMoney(BigDecimal waitCheckMoney) {
        this.waitCheckMoney = waitCheckMoney;
    }

    public BigDecimal getPassCheckMoney() {
        return passCheckMoney;
    }

    public void setPassCheckMoney(BigDecimal passCheckMoney) {
        this.passCheckMoney = passCheckMoney;
    }
}
