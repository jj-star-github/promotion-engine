package com.qmfresh.promotion.bean.promotion;

import java.io.Serializable;
import java.math.BigDecimal;

public class PromotionSellOutShopWarningBean implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 城市ID
     */
    private Integer cityId;

    /**
     * 城市名称
     */
    private String cityName;

    /**
     * 门店ID
     */
    private Integer shopId;

    /**
     * 门店名称
     */
    private String shopName;
    /**
     * 预警级别方式 0、数量 1、比例
     */
    private Integer warningType;
    /**
     * 预警级别数量
     */
    private Integer warningNum;
    /**
     * 预警级别比例
     */
    private BigDecimal warningRate;

    /**
     * 操作人ID
     */
    private Integer createdId;
    /**
     * 操作人名称
     */
    private String createdName;
    /**
     * 创建时间
     */
    private Integer cT;
    /**
     * 更新时间
     */
    private Integer uT;
    /**
     * 是否删除
     */
    private Integer isDeleted;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getWarningType() {
        return warningType;
    }

    public void setWarningType(Integer warningType) {
        this.warningType = warningType;
    }

    public Integer getWarningNum() {
        return warningNum;
    }

    public void setWarningNum(Integer warningNum) {
        this.warningNum = warningNum;
    }

    public BigDecimal getWarningRate() {
        return warningRate;
    }

    public void setWarningRate(BigDecimal warningRate) {
        this.warningRate = warningRate;
    }

    public Integer getCreatedId() {
        return createdId;
    }

    public void setCreatedId(Integer createdId) {
        this.createdId = createdId;
    }

    public String getCreatedName() {
        return createdName;
    }

    public void setCreatedName(String createdName) {
        this.createdName = createdName;
    }

    public Integer getcT() {
        return cT;
    }

    public void setcT(Integer cT) {
        this.cT = cT;
    }

    public Integer getuT() {
        return uT;
    }

    public void setuT(Integer uT) {
        this.uT = uT;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }
}
