package com.qmfresh.promotion.bean.promotion;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by wyh on 2019/5/31.
 *
 * @author wyh
 */
@Slf4j
public class CartContext {

    /**
     * 门店id
     */
    private Integer shopId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户级别
     */
    private Integer userLevel;

    /**
     * 渠道
     */
    private Integer channel;

    private BigDecimal realAmount;

    /**
     * 购买商品
     */
    private List<PromotionSsu> promotionSsus;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Integer userLevel) {
        this.userLevel = userLevel;
    }

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public List<PromotionSsu> getPromotionSsus() {
        return promotionSsus;
    }

    public void setPromotionSsus(List<PromotionSsu> promotionSsus) {
        this.promotionSsus = promotionSsus;
    }

    public BigDecimal getRealAmount() {
        return realAmount;
    }

    public void setRealAmount(BigDecimal realAmount) {
        this.realAmount = realAmount;
    }

}
