package com.qmfresh.promotion.bean.promotion;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wyh on 2019/5/24.
 *
 * @author wyh
 */
public class QueryPromotionModuleParam implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    private Long moduleId;

    private List<Long> moduleIds;

    private String name;
    /**
     * 优先级
     */
    private Integer priority;

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public List<Long> getModuleIds() {
        return moduleIds;
    }

    public void setModuleIds(List<Long> moduleIds) {
        this.moduleIds = moduleIds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
