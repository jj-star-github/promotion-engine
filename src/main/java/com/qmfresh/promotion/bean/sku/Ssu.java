package com.qmfresh.promotion.bean.sku;

import java.io.Serializable;

/**
 * Created by wyh on 2019/9/6.
 *
 * @author wyh
 */
public class Ssu implements Serializable {
    private static final long serialVersionUID = 2974233873713137077L;

    private Integer id;
    /**
     * sku的id
     */
    private Integer skuId;
    /**
     * ssu名称
     */
    private String ssuName;
    /**
     * ssu基础规格
     */
    private String ssuFormat;
    /**
     * ssu售显示规格
     */
    private String ssuSaleFormat;
    /**
     * ssu售卖规格/库存单位
     */
    private Integer ssuFp;
    /**
     * ssu描述
     */
    private String ssuDesc;
    /**
     * 售卖计价单位
     */
    private Integer ssuUnit;
    /**
     * 创建时间
     */
    private Integer cT;
    /**
     * 最后修改时间
     */
    private Integer uT;
    /**
     * 0失效1有效
     */
    private Integer status;
    /**
     * 是否删除
     */
    private Integer isDeleted;
    /**
     * 最小单位售卖规格
     */
    private String minSsuFormat;
    /**
     * 计重还是计件
     */
    private Integer isWeight;
    private Integer ssuFormatId;
    private Integer saleClass1Id;
    private Integer saleClass2Id;
    private String ssuUnitName;
    private Integer unitSkuNum;
    private Integer class1Id;
    private Integer class2Id;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getSsuName() {
        return ssuName;
    }

    public void setSsuName(String ssuName) {
        this.ssuName = ssuName;
    }

    public String getSsuFormat() {
        return ssuFormat;
    }

    public void setSsuFormat(String ssuFormat) {
        this.ssuFormat = ssuFormat;
    }

    public String getSsuSaleFormat() {
        return ssuSaleFormat;
    }

    public void setSsuSaleFormat(String ssuSaleFormat) {
        this.ssuSaleFormat = ssuSaleFormat;
    }

    public Integer getSsuFp() {
        return ssuFp;
    }

    public void setSsuFp(Integer ssuFp) {
        this.ssuFp = ssuFp;
    }

    public String getSsuDesc() {
        return ssuDesc;
    }

    public void setSsuDesc(String ssuDesc) {
        this.ssuDesc = ssuDesc;
    }

    public Integer getSsuUnit() {
        return ssuUnit;
    }

    public void setSsuUnit(Integer ssuUnit) {
        this.ssuUnit = ssuUnit;
    }

    public Integer getCT() {
        return cT;
    }

    public void setCT(Integer cT) {
        this.cT = cT;
    }

    public Integer getUT() {
        return uT;
    }

    public void setUT(Integer uT) {
        this.uT = uT;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getMinSsuFormat() {
        return minSsuFormat;
    }

    public void setMinSsuFormat(String minSsuFormat) {
        this.minSsuFormat = minSsuFormat;
    }

    public Integer getIsWeight() {
        return isWeight;
    }

    public void setIsWeight(Integer isWeight) {
        this.isWeight = isWeight;
    }

    public Integer getSsuFormatId() {
        return ssuFormatId;
    }

    public void setSsuFormatId(Integer ssuFormatId) {
        this.ssuFormatId = ssuFormatId;
    }

    public Integer getSaleClass1Id() {
        return saleClass1Id;
    }

    public void setSaleClass1Id(Integer saleClass1Id) {
        this.saleClass1Id = saleClass1Id;
    }

    public Integer getSaleClass2Id() {
        return saleClass2Id;
    }

    public void setSaleClass2Id(Integer saleClass2Id) {
        this.saleClass2Id = saleClass2Id;
    }

    public String getSsuUnitName() {
        return ssuUnitName;
    }

    public void setSsuUnitName(String ssuUnitName) {
        this.ssuUnitName = ssuUnitName;
    }

    public Integer getUnitSkuNum() {
        return unitSkuNum;
    }

    public void setUnitSkuNum(Integer unitSkuNum) {
        this.unitSkuNum = unitSkuNum;
    }

    public Integer getClass1Id() {
        return class1Id;
    }

    public void setClass1Id(Integer class1Id) {
        this.class1Id = class1Id;
    }

    public Integer getClass2Id() {
        return class2Id;
    }

    public void setClass2Id(Integer class2Id) {
        this.class2Id = class2Id;
    }

}

