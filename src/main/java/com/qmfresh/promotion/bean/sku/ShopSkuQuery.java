package com.qmfresh.promotion.bean.sku;

import com.qmfresh.promotion.dto.base.PageQuery;
import java.util.List;

public class ShopSkuQuery extends PageQuery {
    private static final long serialVersionUID = -2116394275881396827L;
    private Integer shopId;
    private String skuName;
    private Integer class3Id;
    private Integer class1Id;
    private Integer class2Id;
    private Integer channel;
    private String skuId;
    private List<Integer> class2Ids;

    public List<Integer> getClass2Ids() {
        return class2Ids;
    }

    public void setClass2Ids(List<Integer> class2Ids) {
        this.class2Ids = class2Ids;
    }

    public Integer getChannel() {
		return channel;
	}

	public void setChannel(Integer channel) {
		this.channel = channel;
	}

	public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public Integer getClass3Id() {
        return class3Id;
    }

    public void setClass3Id(Integer class3Id) {
        this.class3Id = class3Id;
    }

    public Integer getClass1Id() {
        return class1Id;
    }

    public void setClass1Id(Integer class1Id) {
        this.class1Id = class1Id;
    }

    public Integer getClass2Id() {
        return class2Id;
    }

    public void setClass2Id(Integer class2Id) {
        this.class2Id = class2Id;
    }
}
