package com.qmfresh.promotion.bean.sku;

import java.io.Serializable;

/**
 * <p>
 * 售卖产品ssu
 * </p>
 *
 * @author sky
 * @since 2018-09-10
 */
public class ProductSsuFormatDto implements Serializable {
	private static final long serialVersionUID = 2974233873713137077L;

	private Integer ssuFormatId;
    /**
     * sku的id
     */
	private Integer skuId;
    /**
     * ssu名称
     */
	private String ssuName;
    /**
     * ssu售卖规格
     */
	private String ssuFormat;
    /**
     * ssu售卖规格/库存单位
     */
	private Integer ssuFp;
    /**
     * ssu描述
     */
	private String ssuDesc;
    /**
     * 规格分类：1大包/2中包/3小包/4批发
     */
	private Integer formatType;
    /**
     * 售卖计价单位
     */
	private Integer ssuUnit;
    /**
     * 创建时间
     */
	private Integer cT;
    /**
     * 最后修改时间
     */
	private Integer uT;
    /**
     * 状态：1 待审批 2生效 3审批不通过 4预失效 5失效
     */
	private Integer status;
    /**
     * 是否删除
     */
	private Integer isDeleted;
	private String ssuCode;
	private String ssuBrand;
	private Integer sellChannel;
	private Integer platform;
	private Integer class1Id;
	private Integer class2Id;

	private Sku sku;

	public Integer getSsuFormatId() {
		return ssuFormatId;
	}

	public void setSsuFormatId(Integer ssuFormatId) {
		this.ssuFormatId = ssuFormatId;
	}

	public Integer getSkuId() {
		return skuId;
	}

	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}

	public String getSsuName() {
		return ssuName;
	}

	public void setSsuName(String ssuName) {
		this.ssuName = ssuName;
	}

	public String getSsuFormat() {
		return ssuFormat;
	}

	public void setSsuFormat(String ssuFormat) {
		this.ssuFormat = ssuFormat;
	}

	public Integer getSsuFp() {
		return ssuFp;
	}

	public void setSsuFp(Integer ssuFp) {
		this.ssuFp = ssuFp;
	}

	public String getSsuDesc() {
		return ssuDesc;
	}

	public void setSsuDesc(String ssuDesc) {
		this.ssuDesc = ssuDesc;
	}

	public Integer getFormatType() {
		return formatType;
	}

	public void setFormatType(Integer formatType) {
		this.formatType = formatType;
	}

	public Integer getSsuUnit() {
		return ssuUnit;
	}

	public void setSsuUnit(Integer ssuUnit) {
		this.ssuUnit = ssuUnit;
	}

	public Integer getCT() {
		return cT;
	}

	public void setCT(Integer cT) {
		this.cT = cT;
	}

	public Integer getUT() {
		return uT;
	}

	public void setUT(Integer uT) {
		this.uT = uT;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getSsuCode() {
		return ssuCode;
	}

	public void setSsuCode(String ssuCode) {
		this.ssuCode = ssuCode;
	}

	public String getSsuBrand() {
		return ssuBrand;
	}

	public void setSsuBrand(String ssuBrand) {
		this.ssuBrand = ssuBrand;
	}

	public Integer getSellChannel() {
		return sellChannel;
	}

	public void setSellChannel(Integer sellChannel) {
		this.sellChannel = sellChannel;
	}

	public Integer getPlatform() {
		return platform;
	}

	public void setPlatform(Integer platform) {
		this.platform = platform;
	}

	public Integer getClass1Id() {
		return class1Id;
	}

	public void setClass1Id(Integer class1Id) {
		this.class1Id = class1Id;
	}

	public Integer getClass2Id() {
		return class2Id;
	}

	public void setClass2Id(Integer class2Id) {
		this.class2Id = class2Id;
	}

	public Sku getSku() {
		return sku;
	}

	public void setSku(Sku sku) {
		this.sku = sku;
	}
}
