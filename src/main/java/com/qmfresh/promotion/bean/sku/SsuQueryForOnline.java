/*
 * Copyright (C) 2017-2018 Qy All rights reserved
 * Author: lxc
 * Date: 2019/9/16
 * Description:SsuQueryForOnline.java
 */
package com.qmfresh.promotion.bean.sku;

import java.io.Serializable;

/**
 * @author lxc
 */
public class SsuQueryForOnline implements Serializable {

    private static final long serialVersionUID = -8453258220724054653L;

    private Integer skuId;
    
    private String ssuName;

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getSsuName() {
        return ssuName;
    }

    public void setSsuName(String ssuName) {
        this.ssuName = ssuName;
    }
}
