package com.qmfresh.promotion.bean.sku;

import java.io.Serializable;

public class SkuQuery implements Serializable {
    private static final long serialVersionUID = 2974233873713137077L;

    private String skuName;
    private Long skuId;
    private String internationalCode;

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getInternationalCode() {
        return internationalCode;
    }

    public void setInternationalCode(String internationalCode) {
        this.internationalCode = internationalCode;
    }

}
