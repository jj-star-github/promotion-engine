package com.qmfresh.promotion.bean.sku;

import lombok.Data;

import java.util.List;

/**
 * 从门店获取商品价格
 */
@Data
public class QueryProductSsuPrice {

    /**
     * 门店ID
     */
    private Integer shopId;
    /**
     * skuId
     */
    private Integer skuId;

    /**
     * 多个SkuId
     */
    private List<Integer> skuIds;

}
