package com.qmfresh.promotion.bean.sku;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 产品档案sku
 * </p>
 *
 * @author xiaowei
 * @since 2018-05-02
 */
public class Sku implements Serializable {
    private static final long serialVersionUID = 2974233873713137077L;

    private Integer id;
    /**
     * 分类三id
     */
    private Integer class3Id;
    /**
     * 产品编码
     */
    private String skuCode;
    /**
     * 产品名称
     */
    private String skuName;
    /**
     * 状态 1 生效 0失效
     */
    private Integer status;
    /**
     * spu的id
     */
    private Integer spuId;
    /**
     * 规格
     */
    private String skuFormat;
    /**
     * 描述
     */
    private String skuDesc;
    /**
     * 产品图片
     */
    private String pics;
    /**
     * 外包装标识图片
     */
    private String packagePics;
    /**
     * 称重为1，否则为0
     */
    private Integer isWeight;
    /**
     * 产品国际码
     */
    private String internationalCode;
    /**
     * 是否保质期管理
     */
    private Integer isShelfLife;
    /**
     * 是否固定换算率
     */
    private Integer isFixedConversionRate;
    /**
     * 库存单位/计价单位 *1000
     */
    private Integer physicalCount;
    /**
     * 库存单位
     */
    private Integer physicalUnit;
    /**
     * 计价单位
     */
    private Integer valuationUnit;
    /**
     * 产品等级
     */
    private Integer skuLevel;
    /**
     * 包含最小单位数量*1000
     */
    private Integer spuCount;
    /**
     * 创建时间
     */
    private Integer cT;
    /**
     * 最后更新时间
     */
    private Integer uT;
    /**
     * 建议保存温层:1常温、2冷藏、3冷冻
     */
    private Integer temperature;
    /**
     * 建议保质期天数
     */
    private Integer expireDays;
    /**
     * 商品本身的尺寸(长) *1000 cm
     */
    private Integer length;
    /**
     * 商品本身的尺寸(宽) *1000 cm
     */
    private Integer width;
    /**
     * 商品本身的尺寸(高) *1000 cm
     */
    private Integer height;
    /**
     * 商品本身的尺寸(体积) *1000  m3
     */
    private Integer volume;
    /**
     * 商品本身的重量(净重) *1000  斤
     */
    private Integer netWeight;
    /**
     * 商品本身的重量(毛重) *1000  斤
     */
    private Integer grossWeight;
    /**
     * 商品本身的重量(皮重) *1000  斤
     */
    private Integer tareWeight;
    /**
     * 是否需要证书 1是,0否
     */
    private Integer isCertificate;
    /**
     * 是否贵品1是,0否
     */
    private Integer isPrecious;
    /**
     * 是否易碎1是,0否
     */
    private Integer isFragile;
    /**
     * 库存单位选择的计量方式id sku_units档案id
     */
    private Integer physicalMeasureUnit;
    /**
     * 计价单位选择的计量方式id sku_units的id
     */
    private Integer valuationMeasureUnit;
    /**
     * 是否需要绑定包装物
     */
    private Integer isBindPackage;
    private String skuFrontName;
    private Integer brandId;
    private Integer biId;
    private String bannerPic;
    private String contentPic;
    /**
     * 计价单位数量
     */
    private BigDecimal priceCount;
    private Integer class1Id;
    private Integer class2Id;
    private String class1Name;
    private String class2Name;
    private String class3Name;
    private Integer deliveryGroupId;

    private String firstPy;

    public String getFirstPy() {
        return firstPy;
    }

    public void setFirstPy(String firstPy) {
        this.firstPy = firstPy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClass3Id() {
        return class3Id;
    }

    public void setClass3Id(Integer class3Id) {
        this.class3Id = class3Id;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSpuId() {
        return spuId;
    }

    public void setSpuId(Integer spuId) {
        this.spuId = spuId;
    }

    public String getSkuFormat() {
        return skuFormat;
    }

    public void setSkuFormat(String skuFormat) {
        this.skuFormat = skuFormat;
    }

    public String getSkuDesc() {
        return skuDesc;
    }

    public void setSkuDesc(String skuDesc) {
        this.skuDesc = skuDesc;
    }

    public String getPics() {
        return pics;
    }

    public void setPics(String pics) {
        this.pics = pics;
    }

    public String getPackagePics() {
        return packagePics;
    }

    public void setPackagePics(String packagePics) {
        this.packagePics = packagePics;
    }

    public Integer getIsWeight() {
        return isWeight;
    }

    public void setIsWeight(Integer isWeight) {
        this.isWeight = isWeight;
    }

    public String getInternationalCode() {
        return internationalCode;
    }

    public void setInternationalCode(String internationalCode) {
        this.internationalCode = internationalCode;
    }

    public Integer getIsShelfLife() {
        return isShelfLife;
    }

    public void setIsShelfLife(Integer isShelfLife) {
        this.isShelfLife = isShelfLife;
    }

    public Integer getIsFixedConversionRate() {
        return isFixedConversionRate;
    }

    public void setIsFixedConversionRate(Integer isFixedConversionRate) {
        this.isFixedConversionRate = isFixedConversionRate;
    }

    public Integer getPhysicalCount() {
        return physicalCount;
    }

    public void setPhysicalCount(Integer physicalCount) {
        this.physicalCount = physicalCount;
    }

    public Integer getPhysicalUnit() {
        return physicalUnit;
    }

    public void setPhysicalUnit(Integer physicalUnit) {
        this.physicalUnit = physicalUnit;
    }

    public Integer getValuationUnit() {
        return valuationUnit;
    }

    public void setValuationUnit(Integer valuationUnit) {
        this.valuationUnit = valuationUnit;
    }

    public Integer getSkuLevel() {
        return skuLevel;
    }

    public void setSkuLevel(Integer skuLevel) {
        this.skuLevel = skuLevel;
    }

    public Integer getSpuCount() {
        return spuCount;
    }

    public void setSpuCount(Integer spuCount) {
        this.spuCount = spuCount;
    }

    public Integer getCT() {
        return cT;
    }

    public void setCT(Integer cT) {
        this.cT = cT;
    }

    public Integer getUT() {
        return uT;
    }

    public void setUT(Integer uT) {
        this.uT = uT;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public Integer getExpireDays() {
        return expireDays;
    }

    public void setExpireDays(Integer expireDays) {
        this.expireDays = expireDays;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Integer getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(Integer netWeight) {
        this.netWeight = netWeight;
    }

    public Integer getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(Integer grossWeight) {
        this.grossWeight = grossWeight;
    }

    public Integer getTareWeight() {
        return tareWeight;
    }

    public void setTareWeight(Integer tareWeight) {
        this.tareWeight = tareWeight;
    }

    public Integer getIsCertificate() {
        return isCertificate;
    }

    public void setIsCertificate(Integer isCertificate) {
        this.isCertificate = isCertificate;
    }

    public Integer getIsPrecious() {
        return isPrecious;
    }

    public void setIsPrecious(Integer isPrecious) {
        this.isPrecious = isPrecious;
    }

    public Integer getIsFragile() {
        return isFragile;
    }

    public void setIsFragile(Integer isFragile) {
        this.isFragile = isFragile;
    }

    public Integer getPhysicalMeasureUnit() {
        return physicalMeasureUnit;
    }

    public void setPhysicalMeasureUnit(Integer physicalMeasureUnit) {
        this.physicalMeasureUnit = physicalMeasureUnit;
    }

    public Integer getValuationMeasureUnit() {
        return valuationMeasureUnit;
    }

    public void setValuationMeasureUnit(Integer valuationMeasureUnit) {
        this.valuationMeasureUnit = valuationMeasureUnit;
    }

    public Integer getIsBindPackage() {
        return isBindPackage;
    }

    public void setIsBindPackage(Integer isBindPackage) {
        this.isBindPackage = isBindPackage;
    }

    public String getSkuFrontName() {
        return skuFrontName;
    }

    public void setSkuFrontName(String skuFrontName) {
        this.skuFrontName = skuFrontName;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getBiId() {
        return biId;
    }

    public void setBiId(Integer biId) {
        this.biId = biId;
    }

    public String getBannerPic() {
        return bannerPic;
    }

    public void setBannerPic(String bannerPic) {
        this.bannerPic = bannerPic;
    }

    public String getContentPic() {
        return contentPic;
    }

    public void setContentPic(String contentPic) {
        this.contentPic = contentPic;
    }

    public BigDecimal getPriceCount() {
        return priceCount;
    }

    public void setPriceCount(BigDecimal priceCount) {
        this.priceCount = priceCount;
    }

    public Integer getClass1Id() {
        return class1Id;
    }

    public void setClass1Id(Integer class1Id) {
        this.class1Id = class1Id;
    }

    public Integer getClass2Id() {
        return class2Id;
    }

    public void setClass2Id(Integer class2Id) {
        this.class2Id = class2Id;
    }

    public String getClass1Name() {
        return class1Name;
    }

    public void setClass1Name(String class1Name) {
        this.class1Name = class1Name;
    }

    public String getClass2Name() {
        return class2Name;
    }

    public void setClass2Name(String class2Name) {
        this.class2Name = class2Name;
    }

    public String getClass3Name() {
        return class3Name;
    }

    public void setClass3Name(String class3Name) {
        this.class3Name = class3Name;
    }

    public Integer getDeliveryGroupId() {
        return deliveryGroupId;
    }

    public void setDeliveryGroupId(Integer deliveryGroupId) {
        this.deliveryGroupId = deliveryGroupId;
    }

}
