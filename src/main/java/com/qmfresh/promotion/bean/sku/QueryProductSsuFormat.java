package com.qmfresh.promotion.bean.sku;

import java.util.List;

/**
 * Created by wyh on 2019/6/10.
 *
 * @author wyh
 */
public class QueryProductSsuFormat {

    private List<Integer> ssuFormatIds;

    private Integer skuId;

    private Integer sellChannel;

    private Integer platform;

    private Integer ssuFp;

    private List<Integer> skuIds;

    private List<Integer> class2Ids;

    private List<Integer> exSsuFormatIds;

    private Boolean needSku = false;

    public List<Integer> getSsuFormatIds() {
        return ssuFormatIds;
    }

    public void setSsuFormatIds(List<Integer> ssuFormatIds) {
        this.ssuFormatIds = ssuFormatIds;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public Integer getSellChannel() {
        return sellChannel;
    }

    public void setSellChannel(Integer sellChannel) {
        this.sellChannel = sellChannel;
    }

    public Integer getPlatform() {
        return platform;
    }

    public void setPlatform(Integer platform) {
        this.platform = platform;
    }

    public Integer getSsuFp() {
        return ssuFp;
    }

    public void setSsuFp(Integer ssuFp) {
        this.ssuFp = ssuFp;
    }

    public List<Integer> getSkuIds() {
        return skuIds;
    }

    public void setSkuIds(List<Integer> skuIds) {
        this.skuIds = skuIds;
    }

    public List<Integer> getClass2Ids() {
        return class2Ids;
    }

    public void setClass2Ids(List<Integer> class2Ids) {
        this.class2Ids = class2Ids;
    }

    public List<Integer> getExSsuFormatIds() {
        return exSsuFormatIds;
    }

    public void setExSsuFormatIds(List<Integer> exSsuFormatIds) {
        this.exSsuFormatIds = exSsuFormatIds;
    }

    public Boolean getNeedSku() {
        return needSku;
    }

    public void setNeedSku(Boolean needSku) {
        this.needSku = needSku;
    }
}
