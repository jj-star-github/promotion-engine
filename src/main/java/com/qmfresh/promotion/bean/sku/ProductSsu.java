package com.qmfresh.promotion.bean.sku;

import java.io.Serializable;

/**
 * <p>
 * 售卖产品ssu
 * </p>
 *
 * @author xiaowei
 * @since 2018-05-02
 */
public class ProductSsu implements Serializable {
    private static final long serialVersionUID = 2974233873713137077L;

    private Integer id;
    private Integer shopId;
    /**
     * sku的id
     */
    private Integer skuId;
    /**
     * ssu编码
     */
    private String ssuCode;
    /**
     * ssu名称
     */
    private String ssuName;
    /**
     * ssu售卖规格
     */
    private String ssuFormat;
    /**
     * ssu售卖规格/库存单位
     */
    private Integer ssuFp;
    /**
     * ssu描述
     */
    private String ssuDesc;
    /**
     * 规格分类：1大包/2中包/3小包/4批发
     */
    private Integer formatType;
    /**
     * 售卖品牌
     */
    private String ssuBrand;
    /**
     * 售卖计价单位
     */
    private Integer ssuUnit;
    /**
     * 创建时间
     */
    private Integer cT;
    /**
     * 最后修改时间
     */
    private Integer uT;
    /**
     * 状态：1 待审批 2生效 3审批不通过 4预失效 5失效
     */
    private Integer status;
    /**
     * 是否删除
     */
    private Integer isDeleted;
    private Integer formatId;
    private Sku sku;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getSsuCode() {
        return ssuCode;
    }

    public void setSsuCode(String ssuCode) {
        this.ssuCode = ssuCode;
    }

    public String getSsuName() {
        return ssuName;
    }

    public void setSsuName(String ssuName) {
        this.ssuName = ssuName;
    }

    public String getSsuFormat() {
        return ssuFormat;
    }

    public void setSsuFormat(String ssuFormat) {
        this.ssuFormat = ssuFormat;
    }

    public Integer getSsuFp() {
        return ssuFp;
    }

    public void setSsuFp(Integer ssuFp) {
        this.ssuFp = ssuFp;
    }

    public String getSsuDesc() {
        return ssuDesc;
    }

    public void setSsuDesc(String ssuDesc) {
        this.ssuDesc = ssuDesc;
    }

    public Integer getFormatType() {
        return formatType;
    }

    public void setFormatType(Integer formatType) {
        this.formatType = formatType;
    }

    public String getSsuBrand() {
        return ssuBrand;
    }

    public void setSsuBrand(String ssuBrand) {
        this.ssuBrand = ssuBrand;
    }

    public Integer getSsuUnit() {
        return ssuUnit;
    }

    public void setSsuUnit(Integer ssuUnit) {
        this.ssuUnit = ssuUnit;
    }

    public Integer getCT() {
        return cT;
    }

    public void setCT(Integer cT) {
        this.cT = cT;
    }

    public Integer getUT() {
        return uT;
    }

    public void setUT(Integer uT) {
        this.uT = uT;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Sku getSku() {
        return sku;
    }

    public void setSku(Sku sku) {
        this.sku = sku;
    }

    public Integer getFormatId() {
        return formatId;
    }

    public void setFormatId(Integer formatId) {
        this.formatId = formatId;
    }

}
