package com.qmfresh.promotion.bean.sku;

import java.io.Serializable;
import java.util.List;

public class SsuQuery implements Serializable {
    private static final long serialVersionUID = 2974233873713137077L;

    private List<Integer> ssuFormatIds;

    public List<Integer> getSsuFormatIds() {
        return ssuFormatIds;
    }

    public void setSsuFormatIds(List<Integer> ssuFormatIds) {
        this.ssuFormatIds = ssuFormatIds;
    }
}
