package com.qmfresh.promotion.bean.price;

import com.qmfresh.promotion.dto.base.PageQuery;
import java.util.List;

public class ShopSsuQuery extends PageQuery {
    private Integer class1Id;
    private Integer class2Id;
    private Integer class3Id;
    private Integer skuId;
    private String skuName;
    private String skuCode;
    private String ssuCode;
    private Integer pluCode;
    private String internationalCode;

    private Integer sTime;
    private Integer eTime;
    private Integer shopId;
    private Integer ssuId;
    private String ssuName;
    private String skuDesc;

    private List<Integer> ssuIds;

    /**
     * 1商户2个人
     */
    private Integer saleChannel;
    /**
     * 是否上线：1 上线；0 下线
     */
    private Integer isOnline;
    private Integer isCommon;
    /**
     * sku列表
     */
    private List<Integer> ids;
    private List<Integer> shopIds;
    private List<Integer> formatIds;

    public Integer getClass1Id() {
        return class1Id;
    }

    public void setClass1Id(Integer class1Id) {
        this.class1Id = class1Id;
    }

    public Integer getClass2Id() {
        return class2Id;
    }

    public void setClass2Id(Integer class2Id) {
        this.class2Id = class2Id;
    }

    public Integer getClass3Id() {
        return class3Id;
    }

    public void setClass3Id(Integer class3Id) {
        this.class3Id = class3Id;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getSsuCode() {
        return ssuCode;
    }

    public void setSsuCode(String ssuCode) {
        this.ssuCode = ssuCode;
    }

    public Integer getPluCode() {
        return pluCode;
    }

    public void setPluCode(Integer pluCode) {
        this.pluCode = pluCode;
    }

    public String getInternationalCode() {
        return internationalCode;
    }

    public void setInternationalCode(String internationalCode) {
        this.internationalCode = internationalCode;
    }

    public Integer getsTime() {
        return sTime;
    }

    public void setsTime(Integer sTime) {
        this.sTime = sTime;
    }

    public Integer geteTime() {
        return eTime;
    }

    public void seteTime(Integer eTime) {
        this.eTime = eTime;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSsuId() {
        return ssuId;
    }

    public void setSsuId(Integer ssuId) {
        this.ssuId = ssuId;
    }

    public String getSsuName() {
        return ssuName;
    }

    public void setSsuName(String ssuName) {
        this.ssuName = ssuName;
    }

    public String getSkuDesc() {
        return skuDesc;
    }

    public void setSkuDesc(String skuDesc) {
        this.skuDesc = skuDesc;
    }

    public List<Integer> getSsuIds() {
        return ssuIds;
    }

    public void setSsuIds(List<Integer> ssuIds) {
        this.ssuIds = ssuIds;
    }

    public Integer getSaleChannel() {
        return saleChannel;
    }

    public void setSaleChannel(Integer saleChannel) {
        this.saleChannel = saleChannel;
    }

    public Integer getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(Integer isOnline) {
        this.isOnline = isOnline;
    }

    public Integer getIsCommon() {
        return isCommon;
    }

    public void setIsCommon(Integer isCommon) {
        this.isCommon = isCommon;
    }

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }

    public List<Integer> getShopIds() {
        return shopIds;
    }

    public void setShopIds(List<Integer> shopIds) {
        this.shopIds = shopIds;
    }

    public List<Integer> getFormatIds() {
        return formatIds;
    }

    public void setFormatIds(List<Integer> formatIds) {
        this.formatIds = formatIds;
    }
}
