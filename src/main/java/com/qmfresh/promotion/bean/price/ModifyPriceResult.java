package com.qmfresh.promotion.bean.price;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by wyh on 2019/6/9.
 *
 * @author wyh
 */
public class ModifyPriceResult implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    /**
     * 渠道：1.B端,2.C端线上,3.C端线下
     */
    private Integer channel;

    private BigDecimal price;

    private Long priceId;

    /**
     * 商品类型：2.会员商品，4.促销商品
     */
    private Integer priceType;

    private Integer shopId;

    private Integer ssuFormatId;

    private BigDecimal modifyPrice;

    /**
     * 价格状态：0.失效，1.生效
     */
    private Integer status;

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getPriceId() {
        return priceId;
    }

    public void setPriceId(Long priceId) {
        this.priceId = priceId;
    }

    public Integer getPriceType() {
        return priceType;
    }

    public void setPriceType(Integer priceType) {
        this.priceType = priceType;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSsuFormatId() {
        return ssuFormatId;
    }

    public void setSsuFormatId(Integer ssuFormatId) {
        this.ssuFormatId = ssuFormatId;
    }

    public BigDecimal getModifyPrice() {
        return modifyPrice;
    }

    public void setModifyPrice(BigDecimal modifyPrice) {
        this.modifyPrice = modifyPrice;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
