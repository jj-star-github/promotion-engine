package com.qmfresh.promotion.bean.price;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by wyh on 2019/6/9.
 *
 * @author wyh
 */
public class ModifyVipPriceResult implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    private Long priceId;

    private BigDecimal price;

    private BigDecimal modifyPrice;

    private BigDecimal limitNum;

    private String description;

    public ModifyVipPriceResult() {
        this.limitNum = BigDecimal.ZERO;
    }

    public Long getPriceId() {
        return priceId;
    }

    public void setPriceId(Long priceId) {
        this.priceId = priceId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getModifyPrice() {
        return modifyPrice;
    }

    public void setModifyPrice(BigDecimal modifyPrice) {
        this.modifyPrice = modifyPrice;
    }

    public BigDecimal getLimitNum() {
        return limitNum;
    }

    public void setLimitNum(BigDecimal limitNum) {
        this.limitNum = limitNum;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
