package com.qmfresh.promotion.bean.price;


/**
 * <p>
 * 门店售卖价格
 * </p>
 *
 * @author sky
 * @since 2019-03-08
 */
public class ProductSsuPriceResult {

	private Long id;
	/**
	 * 当前价格
	 */
	private Long price;
	
	private Long skuId;
	
	private String skuName;
	
	private String skuFormat;
	
	private String pics;
	
	private Integer isWeight;
	
	private Integer uT;
	
	/**
	 * 是否上线
	 */
	private Integer isOnline;
	
	private Integer isLock;
	
	private Integer shopId;
	/**
	 * 1大客户2小客户
	 */
	private Integer channel;
	
	private Integer pluCode;
	private String internationalCode;
	
	private Integer class1Id;
	private Integer class2Id;
	
	private String class1Name;
	private String class2Name;
	//单位
	private String unitFormat;

	private Integer ssuFormatId;

	public String getUnitFormat() {
		return unitFormat;
	}

	public void setUnitFormat(String unitFormat) {
		this.unitFormat = unitFormat;
	}

	public Integer getIsLock() {
		return isLock;
	}

	public void setIsLock(Integer isLock) {
		this.isLock = isLock;
	}

	public Integer getClass1Id() {
		return class1Id;
	}

	public void setClass1Id(Integer class1Id) {
		this.class1Id = class1Id;
	}

	public Integer getClass2Id() {
		return class2Id;
	}

	public void setClass2Id(Integer class2Id) {
		this.class2Id = class2Id;
	}

	public String getClass1Name() {
		return class1Name;
	}

	public void setClass1Name(String class1Name) {
		this.class1Name = class1Name;
	}

	public String getClass2Name() {
		return class2Name;
	}

	public void setClass2Name(String class2Name) {
		this.class2Name = class2Name;
	}

	public String getInternationalCode() {
		return internationalCode;
	}

	public void setInternationalCode(String internationalCode) {
		this.internationalCode = internationalCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getUT() {
		return uT;
	}

	public void setUT(Integer uT) {
		this.uT = uT;
	}

	public Integer getIsOnline() {
		return isOnline;
	}

	public void setIsOnline(Integer isOnline) {
		this.isOnline = isOnline;
	}

	public Integer getShopId() {
		return shopId;
	}

	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}

	public Integer getChannel() {
		return channel;
	}

	public void setChannel(Integer channel) {
		this.channel = channel;
	}

	public Integer getPluCode() {
		return pluCode;
	}

	public void setPluCode(Integer pluCode) {
		this.pluCode = pluCode;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public String getSkuFormat() {
		return skuFormat;
	}

	public void setSkuFormat(String skuFormat) {
		this.skuFormat = skuFormat;
	}

	public String getPics() {
		return pics;
	}

	public void setPics(String pics) {
		this.pics = pics;
	}

	public Integer getIsWeight() {
		return isWeight;
	}

	public void setIsWeight(Integer isWeight) {
		this.isWeight = isWeight;
	}

	public Integer getSsuFormatId() {
		return ssuFormatId;
	}

	public void setSsuFormatId(Integer ssuFormatId) {
		this.ssuFormatId = ssuFormatId;
	}
}