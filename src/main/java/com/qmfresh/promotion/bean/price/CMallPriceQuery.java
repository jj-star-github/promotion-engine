package com.qmfresh.promotion.bean.price;

import java.util.List;

/**
 * Created by wyh on 2019/8/2.
 *
 * @author wyh
 */
public class CMallPriceQuery {

    private List<Integer> ssuFormatIds;

    private List<Integer> exSsuFormatIds;

    private List<Integer> class2Ids;

    private List<Integer> shopIds;

    private List<Integer> skuIds;

    /**
     * 1、按条件全部查询 2、查询上线，未删除，价格>0
     */
    private Integer status;

    public List<Integer> getSsuFormatIds() {
        return ssuFormatIds;
    }

    public void setSsuFormatIds(List<Integer> ssuFormatIds) {
        this.ssuFormatIds = ssuFormatIds;
    }

    public List<Integer> getExSsuFormatIds() {
        return exSsuFormatIds;
    }

    public void setExSsuFormatIds(List<Integer> exSsuFormatIds) {
        this.exSsuFormatIds = exSsuFormatIds;
    }

    public List<Integer> getClass2Ids() {
        return class2Ids;
    }

    public void setClass2Ids(List<Integer> class2Ids) {
        this.class2Ids = class2Ids;
    }

    public List<Integer> getShopIds() {
        return shopIds;
    }

    public void setShopIds(List<Integer> shopIds) {
        this.shopIds = shopIds;
    }

    public List<Integer> getSkuIds() {
        return skuIds;
    }

    public void setSkuIds(List<Integer> skuIds) {
        this.skuIds = skuIds;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
