package com.qmfresh.promotion.bean.price;

/**
 * Created by wyh on 2019/6/28.
 *
 * @author wyh
 */
public class ResetVipPriceParam {

    private Integer shopIds;

    private Integer saleChannel;

    public Integer getShopIds() {
        return shopIds;
    }

    public void setShopIds(Integer shopIds) {
        this.shopIds = shopIds;
    }

    public Integer getSaleChannel() {
        return saleChannel;
    }

    public void setSaleChannel(Integer saleChannel) {
        this.saleChannel = saleChannel;
    }
}
