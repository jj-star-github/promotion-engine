package com.qmfresh.promotion.bean.price;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by wyh on 2019/6/9.
 *
 * @author wyh
 */
public class ModifyPriceBean implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    private Integer channel;

    private Long priceId;

    private Integer shopId;

    private Integer ssuFormatId;

    private Integer class2Id;

    private BigDecimal price;

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public Long getPriceId() {
        return priceId;
    }

    public void setPriceId(Long priceId) {
        this.priceId = priceId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getSsuFormatId() {
        return ssuFormatId;
    }

    public void setSsuFormatId(Integer ssuFormatId) {
        this.ssuFormatId = ssuFormatId;
    }

    public Integer getClass2Id() {
        return class2Id;
    }

    public void setClass2Id(Integer class2Id) {
        this.class2Id = class2Id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
