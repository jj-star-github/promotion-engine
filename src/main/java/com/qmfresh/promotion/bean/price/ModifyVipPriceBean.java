package com.qmfresh.promotion.bean.price;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by wyh on 2019/6/9.
 *
 * @author wyh
 */
@Data
public class ModifyVipPriceBean implements Serializable {
    private static final long serialVersionUID = -6610138130146415533L;

    private Long priceId;

    private Integer shopId;

    private Integer ssuId;

    private BigDecimal price;
}
