package com.qmfresh.promotion.bean.price;

import java.math.BigDecimal;

/**
 * Created by wyh on 2019/8/2.
 *
 * @author wyh
 */
public class CMallPriceInfo {

    private Integer ssuFormatId;

    private BigDecimal price;

    private Integer shopId;

    public Integer getSsuFormatId() {
        return ssuFormatId;
    }

    public void setSsuFormatId(Integer ssuFormatId) {
        this.ssuFormatId = ssuFormatId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

}
