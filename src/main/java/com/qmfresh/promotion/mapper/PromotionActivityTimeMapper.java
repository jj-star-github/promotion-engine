package com.qmfresh.promotion.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.qmfresh.promotion.entity.PromotionActivityTime;


/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
@Mapper
public interface PromotionActivityTimeMapper extends BaseMapper<PromotionActivityTime> {
	//查询当前时间存在的会员活动
	List<PromotionActivityTime> getActByNowTime(@Param("nowTime") Integer nowTime,@Param("moduleId") Integer moduleId,@Param("weekDay") String weekDay);
	//查询指定时间的存在的会员活动
	List<PromotionActivityTime> getActByTime(@Param("time") Integer time,@Param("moduleId") Integer moduleId,@Param("weekDay") String weekDay);
	//查询门店的在指定时间段的活动
	List<PromotionActivityTime> getActByShopAndTime(@Param("startTime") Integer startTime,@Param("endTime") Integer endTime,@Param("shopIds") List<Integer> shopIds);
	//查询指定门店当前参与的所有活动
	List<PromotionActivityTime> getShopActTimesByShop(@Param("shopId") Integer shopId,@Param("nowTime") Integer nowTimes);

    /**
     * 根据门店id查询有效的活动
     * @param shopIds 门店ids
     * @param startTime 开始时间
     * @param endTime 截止时间
     * @return
     */
    List<PromotionActivityTime> getShopActTimesByShopIds(@Param("shopIds") List<Integer> shopIds,@Param("startTime") Integer startTime,@Param("endTime") Integer endTime);

    /**
     * 获取当前有效的活动，结束时间>=当前时间，开始时间<=当前时间
     * @param promotionModuleIds  活动类型id
     * @return
     */
    List<PromotionActivityTime> getCurrentActiveAct(@Param("promotionModuleIds") List<Integer> promotionModuleIds,@Param("startTime") Integer startTime,@Param("endTime") Integer endTime);

	/**
	 * 根据活动ID查询当前时间有效的数据
	 * @param activityIds
	 * @param now
	 * @return
	 */
	List<PromotionActivityTime> getActTimesByActIds(@Param("activityIds") List<Long> activityIds,@Param("now") Integer now);

}