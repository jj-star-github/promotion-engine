package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.ActivityClass2;

/**
 * <p>
  * 促销活动分类 Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface ActivityClass2Mapper extends BaseMapper<ActivityClass2> {

}