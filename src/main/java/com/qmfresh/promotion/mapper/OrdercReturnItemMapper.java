package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.OrdercReturnItem;

/**
 * <p>
  * 退货单详情 Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
public interface OrdercReturnItemMapper extends BaseMapper<OrdercReturnItem> {

}