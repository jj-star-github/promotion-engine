package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.PromotionSellOutWarningDetail;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author xbb
 * @since 2019-11-23
 */
public interface PromotionSellOutWarningDetailMapper extends BaseMapper<PromotionSellOutWarningDetail> {

}