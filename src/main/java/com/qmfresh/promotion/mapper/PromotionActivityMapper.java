package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.bean.promotion.PageQueryActivityParam;
import com.qmfresh.promotion.entity.PromotionActivity;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface PromotionActivityMapper extends BaseMapper<PromotionActivity> {

    /**
     * 分页查询日账单
     *
     * @param condition
     * @param start
     * @param end
     * @return
     */
    List<PromotionActivity> pageQuery(@Param("condition") PageQueryActivityParam condition, @Param("start") int start,
        @Param("end") int end);

    /**
     * 查总数
     *
     * @param condition
     * @return
     */
    int countByCondition(PageQueryActivityParam condition);

    /**
     * 分页查询日账单
     *
     * @param condition
     * @param start
     * @param end
     * @return
     */
    List<PromotionActivity> pageQuery4Cp(@Param("condition") PageQueryActivityParam condition, @Param("start") int start,
                                      @Param("end") int end);

    /**
     * 查总数
     *
     * @param condition
     * @return
     */
    int countByCondition4Cp(PageQueryActivityParam condition);

    /**
     * 查询指定门店的活动信息
     * @param shopList
     * @param moduleList
     * @return
     */
    List<PromotionActivity> queryByShop(@Param("shopList")List shopList, @Param("moduleList")List moduleList, @Param("status")Integer status);
}