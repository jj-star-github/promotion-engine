package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.PromotionModifyLog;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-25
 */
public interface PromotionModifyLogMapper extends BaseMapper<PromotionModifyLog> {

}