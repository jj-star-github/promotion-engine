package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.AreaScopeCity;

/**
 * <p>
  * 城市区域 Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface AreaScopeCityMapper extends BaseMapper<AreaScopeCity> {

}