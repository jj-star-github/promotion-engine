package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.PresentModifyLog;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-25
 */
public interface PresentModifyLogMapper extends BaseMapper<PresentModifyLog> {

}