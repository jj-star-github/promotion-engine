package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.UserScopeGroup;

/**
 * <p>
  * 用户作用域 Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface UserScopeGroupMapper extends BaseMapper<UserScopeGroup> {

}