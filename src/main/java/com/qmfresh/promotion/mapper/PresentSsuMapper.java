package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.PresentSsu;

/**
 * <p>
  * 赠品商品配置 Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface PresentSsuMapper extends BaseMapper<PresentSsu> {

}