package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.dto.PromotionSellOutWarningReq;
import com.qmfresh.promotion.entity.PromotionSellOutSkuWarning;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PromotionSellOutSkuWarningMapper extends BaseMapper<PromotionSellOutSkuWarning> {

    List<PromotionSellOutSkuWarning> pageQuery(@Param("condition") PromotionSellOutWarningReq req, @Param("start") int start, @Param("end") int end);

    int count(@Param("condition")PromotionSellOutWarningReq req);

    List<PromotionSellOutSkuWarning> match(@Param("time") String time, @Param("shopId") Integer shopId);

    List<Integer> queryIdByShopAndSku(@Param("list") List<PromotionSellOutSkuWarning> req);

    Boolean insertOrUpdateBatch(@Param("list") List<PromotionSellOutSkuWarning> list);

    List<PromotionSellOutSkuWarning> queryByCondition(@Param("condition") PromotionSellOutWarningReq param);
}
