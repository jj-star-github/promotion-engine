package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.PromotionSellOutShopWarning;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PromotionSellOutShopWarningMapper extends BaseMapper<PromotionSellOutShopWarning> {

    Boolean add(@Param("list") List<PromotionSellOutShopWarning> list);


}
