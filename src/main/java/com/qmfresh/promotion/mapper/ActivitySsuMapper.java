package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.ActivitySsu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  * 促销活动ssu Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface ActivitySsuMapper extends BaseMapper<ActivitySsu> {

    /**
     * 自定义批量插入（mybatis-plus2的insertBatch方法将异常捕获了，会导致spring事务无法回滚）
     * @param list
     * @return
     */
    Boolean batchInsertActivitySsu(@Param("list")List<ActivitySsu> list);
}