package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.PromotionSku;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author xbb
 * @since 2019-11-29
 */
public interface PromotionSkuMapper extends BaseMapper<PromotionSku> {

}