package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.ActivityAreaScope;

/**
 * <p>
  * 活动区域表 Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface ActivityAreaScopeMapper extends BaseMapper<ActivityAreaScope> {

}