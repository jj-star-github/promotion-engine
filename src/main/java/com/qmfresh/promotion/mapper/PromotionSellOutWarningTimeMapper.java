package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.PromotionSellOutWarningTime;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PromotionSellOutWarningTimeMapper extends BaseMapper<PromotionSellOutWarningTime> {

    Boolean updateByWarningId(@Param("list") List<Integer> ids);

    Boolean updateByCondition(@Param("list") List<PromotionSellOutWarningTime> times);
}