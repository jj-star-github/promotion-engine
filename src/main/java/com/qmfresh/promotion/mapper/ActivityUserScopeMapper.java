package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.ActivityUserScope;

/**
 * <p>
  * 活动用户作用域 Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface ActivityUserScopeMapper extends BaseMapper<ActivityUserScope> {

}