package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.PromotionModule;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface PromotionModuleMapper extends BaseMapper<PromotionModule> {

}