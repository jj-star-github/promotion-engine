package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.bean.present.PageQueryParam;
import com.qmfresh.promotion.entity.PresentActivity;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 赠品活动 Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-05-23
 */
public interface PresentActivityMapper extends BaseMapper<PresentActivity> {

    List<PresentActivity> queryList(@Param("condition") PageQueryParam condition);

    /**
     * 分页查询
     *
     * @param condition
     * @param start
     * @param end
     * @return
     */
    List<PresentActivity> pageQuery(@Param("condition") PageQueryParam condition, @Param("start") int start,
        @Param("end") int end);

    /**
     * 查总数
     *
     * @param condition
     * @return
     */
    int countByCondition(PageQueryParam condition);
    
    Boolean updateStatus(@Param("id") Integer id);
}