package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.OrderItemC;

/**
 * <p>
  * 订单详情 Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
public interface OrderItemCMapper extends BaseMapper<OrderItemC> {

}