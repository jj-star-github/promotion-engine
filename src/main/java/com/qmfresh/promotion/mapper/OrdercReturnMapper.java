package com.qmfresh.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qmfresh.promotion.entity.OrdercReturn;

/**
 * <p>
  * 退货单主表 Mapper 接口
 * </p>
 *
 * @author wuyanhui
 * @since 2019-06-03
 */
public interface OrdercReturnMapper extends BaseMapper<OrdercReturn> {

}