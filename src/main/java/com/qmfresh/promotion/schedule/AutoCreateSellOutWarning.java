package com.qmfresh.promotion.schedule;

import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.service.IPromotionSellOutWarningService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 每天定时触发，符合时间的预警；例如：每隔一个小时，那么分别是00:00 、1:00等时间来触发门店预警时间为1:00的预警
 * @author 聂晶
 */
@JobHandler("autoCreateSellOutWarning")
@Component
public class AutoCreateSellOutWarning extends IJobHandler{
    private static final Logger logger = LoggerFactory.getLogger(AutoCreateSellOutWarning.class);
    @Autowired
    private IPromotionSellOutWarningService promotionSellOutWarningService;
    @Override
    public ReturnT<String> execute(String s) throws Exception {
        Date date=new Date();
        logger.info("触发预警主任务开始,当前时间为："+ DateUtil.dateToStr(date,"yyyy-MM-dd HH:mm:ss"));
        //获取当前日期时间的时分例如9:00，一般而言不会有超过一分钟的误差，例如9:01处理时就匹配不上了，这样就导致触发时间不准，获取不到匹配的预警
        String time=DateUtil.dateToStr(date,"HH:mm");
        if(StringUtils.isNotEmpty(s)){
           time=s;
        }
        promotionSellOutWarningService.createWarningDetail(time);
        logger.info("触发预警主任务结束,当前时间为："+ DateUtil.dateToStr(date,"yyyy-MM-dd HH:mm:ss"));
        return ReturnT.SUCCESS;
    }
}
