package com.qmfresh.promotion.schedule;

import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionActivityTime;
import com.qmfresh.promotion.enums.ActivityStatusTypeEnums;
import com.qmfresh.promotion.manager.IPromotionActivityManager;
import com.qmfresh.promotion.manager.IPromotionActivityTimeManager;
import com.qmfresh.promotion.service.IPromotionActivityService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @ClassName SetCpActivityStatusStarted
 * @Description TODO
 * @Author xbb
 * @Date 2020/3/9 11:18
 */
@JobHandler(value = "setCpActivityStatusStarted")
@Component
public class SetCpActivityStatusStarted extends IJobHandler {
    private static final Logger logger = LoggerFactory.getLogger(SetCpActivityStatusStarted.class);

    @Resource
    private IPromotionActivityTimeManager promotionActivityTimeManager;

    @Resource
    private IPromotionActivityService promotionActivityService;

    @Resource
    private IPromotionActivityManager promotionActivityManager;

    @Override
    public ReturnT<String> execute(String s) throws Exception {
        logger.info("开始执行更新改价任务状态定时任务。。。。。。。。。。。。。。");

        List<PromotionActivity> promotionActivities = promotionActivityManager.promotionActivities(Collections.singletonList(ActivityStatusTypeEnums.APPROVED));
        if (ListUtil.isNullOrEmpty(promotionActivities)) {
            logger.info("未匹配到促销活动。。。。。。。。。。。。。。");
            return ReturnT.SUCCESS;
        }

        List<PromotionActivityTime> promotionActivityTimes = promotionActivityTimeManager
                .promotionActivityTimes(promotionActivities.stream().map(PromotionActivity::getId).distinct().collect(Collectors.toList()));


        Map<Long, List<PromotionActivityTime>> patMap = promotionActivityTimes.stream().collect(Collectors.groupingBy(PromotionActivityTime::getActivityId));
        List<Long> activityIds = new ArrayList<>(patMap.size());
        for (Map.Entry<Long, List<PromotionActivityTime>> entry : patMap.entrySet()) {
            List<PromotionActivityTime> patList = entry.getValue();
            PromotionActivityTime promotionActivityTime = patList.stream().min(Comparator.comparing(PromotionActivityTime::getEffectBeginTime)).get();
            //加1是因为，活动开始时间是0点01秒
            if (promotionActivityTime.getEffectBeginTime().compareTo(DateUtil.getStartTimeStamp() + 1) <= 0
                    && promotionActivityTime.getEffectEndTime().compareTo(DateUtil.getStartTimeStamp()) >= 0) {
                activityIds.add(promotionActivityTime.getActivityId());
            }
        }

        if (ListUtil.isNullOrEmpty(activityIds)) {
            logger.info("无促销活动需要更新。。。。。。。。。。。。。。");
            return ReturnT.SUCCESS;
        }

        promotionActivityService.start(activityIds);

        logger.info("更新促销任务状态完成。。。。。。。。。。。。。。");
        return ReturnT.SUCCESS;
    }
}
