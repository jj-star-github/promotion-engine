package com.qmfresh.promotion.schedule;

import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.entity.PromotionActivityTime;
import com.qmfresh.promotion.enums.ActivityStatusTypeEnums;
import com.qmfresh.promotion.manager.IPromotionActivityManager;
import com.qmfresh.promotion.manager.IPromotionActivityTimeManager;
import com.qmfresh.promotion.service.IPromotionActivityService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/6/17.
 *
 * @author wyh
 */
@JobHandler(value = "modifyActivityStatus")
@Component
@Slf4j
public class ModifyActivityStatus extends IJobHandler {
    private static final Logger logger = LoggerFactory.getLogger(ModifyActivityStatus.class);

    @Resource
    private IPromotionActivityTimeManager promotionActivityTimeManager;

    @Resource
    private IPromotionActivityService promotionActivityService;

    @Resource
    private IPromotionActivityManager promotionActivityManager;

    @Override
    public ReturnT<String> execute(String s) throws Exception {
        logger.info("开始执行更新促销任务状态定时任务。。。。。。。。。。。。。。");

        List<PromotionActivity> promotionActivities = promotionActivityManager.promotionActivities(
                Arrays.asList(ActivityStatusTypeEnums.STARTED,
                        ActivityStatusTypeEnums.NOT_START,
                        ActivityStatusTypeEnums.STOP));
        if (ListUtil.isNullOrEmpty(promotionActivities)) {
            logger.info("未匹配到促销活动。。。。。。。。。。。。。。");
            return ReturnT.SUCCESS;
        }

        List<PromotionActivityTime> promotionActivityTimes = promotionActivityTimeManager
                .promotionActivityTimes(promotionActivities.stream().map(PromotionActivity::getId).distinct().collect(Collectors.toList()));

        Map<Long, List<PromotionActivityTime>> patMap = promotionActivityTimes.stream().collect(Collectors.groupingBy(PromotionActivityTime::getActivityId));
        List<Long> activityIds = new ArrayList<>(patMap.size());
        for (Map.Entry<Long, List<PromotionActivityTime>> entry : patMap.entrySet()) {
            List<PromotionActivityTime> patList = entry.getValue();
            PromotionActivityTime promotionActivityTime = patList.stream().max(Comparator.comparing(PromotionActivityTime::getEffectEndTime)).get();
            if (promotionActivityTime.getEffectEndTime().compareTo(DateUtil.getStartTimeStamp()) < 0) {
                activityIds.add(promotionActivityTime.getActivityId());
            }
        }

        if (ListUtil.isNullOrEmpty(activityIds)) {
            logger.info("无促销活动需要更新。。。。。。。。。。。。。。");
            return ReturnT.SUCCESS;
        }

        promotionActivityService.finish(activityIds);

        logger.info("更新促销任务状态完成。。。。。。。。。。。。。。");
        return ReturnT.SUCCESS;
    }
}
