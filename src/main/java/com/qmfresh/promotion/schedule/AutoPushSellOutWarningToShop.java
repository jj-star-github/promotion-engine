package com.qmfresh.promotion.schedule;

import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.service.IPromotionSellOutWarningService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 每天触发完主预警后，开始将预警信息同步到门店中
 */
@JobHandler("autoPushSellOutWarningToShop")
@Component
public class AutoPushSellOutWarningToShop extends IJobHandler{
    private static final Logger logger = LoggerFactory.getLogger(AutoPushSellOutWarningToShop.class);
    @Autowired
    private IPromotionSellOutWarningService promotionSellOutWarningService;
    @Override
    public ReturnT<String> execute(String s) throws Exception {
        Date date=new Date();
        logger.info("触发预警子任务开始,当前时间为："+ DateUtil.dateToStr(date,"yyyy-MM-dd HH:mm:ss"));
        promotionSellOutWarningService.pushWarningDetailToShop();
        logger.info("触发预警子任务结束,当前时间为："+ DateUtil.dateToStr(date,"yyyy-MM-dd HH:mm:ss"));
        return ReturnT.SUCCESS;
    }
}
