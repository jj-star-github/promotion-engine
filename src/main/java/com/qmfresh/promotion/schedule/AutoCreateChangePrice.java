package com.qmfresh.promotion.schedule;

import com.qmfresh.promotion.service.IPromotionTaskServie;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @ClassName AutoCreateChangePrice
 * @Description TODO
 * @Author xbb
 * @Date 2020/4/29 14:22
 */
@JobHandler(value = "autoCreateChangePrice")
@Component
public class AutoCreateChangePrice extends IJobHandler {

    private static final Logger logger = LoggerFactory.getLogger(AutoCreateChangePrice.class);

    @Resource
    private IPromotionTaskServie promotionTaskService;

    @Override
    public ReturnT<String> execute(String s) throws Exception {
        logger.info("开始执行修改C端门店促销价定时任务。。。。。。。。。。。。。。");
        promotionTaskService.createCpPrice();
        logger.info("修改C端门店促销价任务完成。。。。。。。。。。。。。。");
        return ReturnT.SUCCESS;
    }
}
