package com.qmfresh.promotion.schedule;

import com.qmfresh.promotion.service.IPromotionTaskServie;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by wyh on 2019/6/14.
 *
 * @author wyh
 */
@JobHandler(value = "autoCreateVipPrice")
@Component
public class AutoCreateVipPrice extends IJobHandler {
    private static final Logger logger = LoggerFactory.getLogger(AutoCreateVipPrice.class);

    @Resource
    private IPromotionTaskServie promotionTaskServie;

    @Override
    public ReturnT<String> execute(String s) throws Exception {
        logger.info("开始执行修改C端门店会员价定时任务。。。。。。。。。。。。。。");
        new Thread(() -> promotionTaskServie.createVipPrice()).start();
        logger.info("修改C端门店会员价任务完成。。。。。。。。。。。。。。");
        return ReturnT.SUCCESS;
    }

    @Override
    public void destroy() {

    }
}
