package com.qmfresh.promotion.gateway;

import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.dto.QueryPromotionSellOutWarningDetailParam;
import com.qmfresh.promotion.service.IPromotionSellOutWarningDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: promotion-engine
 * @description: 出清预警
 * @author: xbb
 * @create: 2019-11-23 14:19
 **/
@RestController
@RequestMapping("/promotion")
@Slf4j
public class PromotionSellOutWarningDetailController {

    @Autowired
    private IPromotionSellOutWarningDetailService promotionSellOutWarningDetailService;

    @RequestMapping("/querySellOutWarningDetailPageList")
    public ServiceResult queryPageList(@RequestBody QueryPromotionSellOutWarningDetailParam param){
        return ServiceResultUtil.resultSuccess(promotionSellOutWarningDetailService.getPageList(param));
    }
}
