/*
 * Copyright (C) 2019-2020 Qm All rights reserved
 * Author: lxc
 * Date: 2020/2/26 0026
 * Description:SellOutWarningController.java
 */
package com.qmfresh.promotion.gateway;

import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.service.IPromotionSellOutWarningDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lxc
 * @Date 2020/2/26 0026 18:39
 */
@RestController
@RequestMapping(value = "/warningDetail")
@Slf4j
public class SellOutWarningController {

    @Autowired
    private IPromotionSellOutWarningDetailService detailService;
    
    @RequestMapping(value = "/queryWarningDetailList")
    public ServiceResult queryWarningDetailList() {
        return ServiceResultUtil.resultSuccess(detailService.queryWarningDetailList());
    }
}
