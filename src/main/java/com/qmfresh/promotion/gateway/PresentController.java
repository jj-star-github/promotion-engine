package com.qmfresh.promotion.gateway;

import com.qmfresh.promotion.bean.present.CreatePresentActivity;
import com.qmfresh.promotion.bean.present.DelPresentActivity;
import com.qmfresh.promotion.bean.present.PageQueryParam;
import com.qmfresh.promotion.bean.present.VipBindExchangeActivity;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.service.IPresentService;
import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by wyh on 2019/6/11.
 *
 * @author wyh
 */
@RestController
@RequestMapping(value = "/present")
@Slf4j
public class PresentController {

    @Resource
    private IPresentService presentService;

    @RequestMapping(value = "/queryList")
    public ServiceResult queryList(@RequestBody PageQueryParam param) {
        return ServiceResultUtil.resultSuccess(presentService.queryList(param));
    }

    @RequestMapping(value = "/pageQuery")
    public ServiceResult pageQuery(@RequestBody PageQueryParam param) {
        return ServiceResultUtil.resultSuccess(presentService.pageQuery(param));
    }

    @RequestMapping(value = "/createPresentActivity")
    public ServiceResult createPresentActivity(@RequestBody CreatePresentActivity param) {
        return ServiceResultUtil.resultSuccess(presentService.createPresentActivity(param));
    }

    @RequestMapping(value = "/delPresentActivity")
    public ServiceResult delPresentActivity(@RequestBody DelPresentActivity param) {
        return ServiceResultUtil.resultSuccess(presentService.delPresentActivity(param));
    }

    @RequestMapping(value = "/bindVipExchangeActivity")
    public ServiceResult bindVipExchangeActivity(@RequestBody VipBindExchangeActivity param) {
        Long activityId = param.getActivityId();
        List<Long> presentIds = param.getPresentIds();
        return ServiceResultUtil.resultSuccess(presentService.bindPresent(activityId, presentIds));
    }
    
}
