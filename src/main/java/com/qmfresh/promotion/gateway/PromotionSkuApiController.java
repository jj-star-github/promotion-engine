package com.qmfresh.promotion.gateway;

import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.dto.QueryPromotionSkuParam;
import com.qmfresh.promotion.service.IPromotionSkuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @program: promotion-engine
 * @description:
 * @author: xbb
 * @create: 2019-11-30 17:05
 **/
@RestController
@RequestMapping("/web/promotion/skuApi")
@Slf4j
public class PromotionSkuApiController {

    @Autowired
    private IPromotionSkuService promotionSkuService;

    @RequestMapping("/checkSkuExistOrNot")
    public ServiceResult checkSkuExistOrNot(@RequestBody QueryPromotionSkuParam param){
        return ServiceResultUtil.resultSuccess(promotionSkuService.checkSkuExistOrNot(param));
    }
}
