package com.qmfresh.promotion.gateway;

import com.qmfresh.promotion.bean.promotion.PromotionSellOutSkuWarningBean;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.dto.PromotionSellOutWarningReq;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.service.IPromotionSellOutWarningService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Controller
@RequestMapping("web/promotionSellOutWarning/")
@Slf4j
public class PromotionSellOutWarningController {

    @Autowired
    private IPromotionSellOutWarningService promotionSellOutWarningService;

    @RequestMapping("/add")
    public ServiceResult<Boolean> add(@RequestBody PromotionSellOutWarningReq req){
        return ServiceResultUtil.resultSuccess(promotionSellOutWarningService.add(req));
    }

    @RequestMapping("/search")
    public ServiceResult<ListWithPage<PromotionSellOutSkuWarningBean>> search(@RequestBody PromotionSellOutWarningReq req){
        return ServiceResultUtil.resultSuccess(promotionSellOutWarningService.search(req));
    }

    @RequestMapping("/update")
    public ServiceResult<Boolean> update(@RequestBody PromotionSellOutWarningReq req){
        return ServiceResultUtil.resultSuccess(promotionSellOutWarningService.update(req));
    }

    @RequestMapping("/setShopWarning")
    public ServiceResult<Boolean> setShopWarning(@RequestBody PromotionSellOutWarningReq req){
        return ServiceResultUtil.resultSuccess(promotionSellOutWarningService.setShopWarning(req));
    }
}
