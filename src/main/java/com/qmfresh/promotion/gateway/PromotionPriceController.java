package com.qmfresh.promotion.gateway;

import com.qmfresh.promotion.bean.price.ModifyPriceBean;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.service.IPromotionPriceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wyh on 2019/6/4.
 *
 * @author wyh
 */
@RestController
@RequestMapping(value = "/promotion/price")
@Slf4j
public class PromotionPriceController {

    @Resource
    private IPromotionPriceService promotionPriceService;

    /**
     * 修改线上会员价格
     *
     * @return
     */
    @RequestMapping(value = "/modifyPrice")
    public ServiceResult modifyPrice(@RequestBody List<ModifyPriceBean> param) {
        return ServiceResultUtil.resultSuccess(promotionPriceService.modifyPrice(param));
    }

}
