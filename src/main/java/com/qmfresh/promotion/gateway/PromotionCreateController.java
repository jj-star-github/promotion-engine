package com.qmfresh.promotion.gateway;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.gis.Shop;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.common.utils.TraceIdUtils;
import com.qmfresh.promotion.dto.*;
import com.qmfresh.promotion.enums.ChannelTypeEnums;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.promotion.platform.interfaces.common.BizCode;
import com.qmfresh.promotion.service.IOldGatewayService;
import com.qmfresh.promotion.service.IPromotionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/8/27.
 *
 * @author wyh
 */
@RestController
@RequestMapping(value = "/promotion/create")
@Slf4j
public class PromotionCreateController {
    @Resource
    private IPromotionService promotionService;

    @Resource
    private IOldGatewayService oldGatewayService;

    @Resource
    private IQmExternalApiService iQmExternalApiService;
    /**
     * 创建买赠规则--线下门店
     *
     * @return
     */
    @RequestMapping(value = "/createBzPromotion")
    public ServiceResult createBzPromotion(@RequestBody CreateBzPromotionParam param) {
        param.setChannel(ChannelTypeEnums.C_OFFLINE.getCode());
        return ServiceResultUtil.resultSuccess(promotionService.createBzPromotion(param));
    }

    /**
     * 创建买赠规则--线下门店
     *
     * @return
     */
    @RequestMapping(value = "/createDpPromotion")
    public ServiceResult createDpPromotion(@RequestBody CreateDpPromotionParam param) {
        param.setChannel(ChannelTypeEnums.C_OFFLINE.getCode());
        return ServiceResultUtil.resultSuccess(promotionService.createDpPromotion(param));
    }

    /**
     * 创建满赠规则--线下门店
     *
     * @return
     */
    @RequestMapping(value = "/createMzPromotion")
    public ServiceResult createMzPromotion(@RequestBody CreateMzPromotionParam param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("总部创建满赠规则，traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            param.setChannel(ChannelTypeEnums.C_OFFLINE.getCode());
            param.setAreaDtos(oldGatewayService.getAreaDto(param.getAreaDtos().get(0).getShopIds()));
            param.setPromotionProductDto(oldGatewayService.convertSkuToSsuFormat(param.getAreaDtos().get(0).getShopIds(), param.getChannel(), param.getPromotionProductDto()));
            return ServiceResultUtil.resultSuccess(promotionService.createMzPromotion(param));
        } catch (BusinessException be) {
            log.warn("总部创建满赠规则-业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
            return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), be.getErrorMsg());
        } catch (Exception e) {
            log.error("总部创建满赠规则-异常: traceId={}, ex={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
        }
        return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), "系统异常");
    }

    /**
     * 创建买赠规则--线下门店
     * 会员活动创建
     * @return
     */
    @RequestMapping(value = "/createVipPromotion")
    public ServiceResult createVipPromotion(@RequestBody CreateVipPromotionParam param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("创建买赠规则，traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            param.setAreaDtos(oldGatewayService.getAreaDto(param.getAreaDtos().get(0).getShopIds()));
            param.setPromotionProductDto(oldGatewayService.convertSkuToSsuFormat(param.getAreaDtos().get(0).getShopIds(), param.getChannel(), param.getPromotionProductDto()));
            param.setChannel(ChannelTypeEnums.C_OFFLINE.getCode());
            return ServiceResultUtil.resultSuccess(promotionService.createVipPromotion(param));
        } catch (BusinessException be) {
            log.warn("创建买赠规则-业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
            return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), be.getErrorMsg());
        } catch (Exception e) {
            log.error("创建买赠规则-异常: traceId={}, ex={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
        }
        return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), "系统异常");
    }

    /**
     * 创建买减规则--线下门店
     *
     * @return
     */
    @RequestMapping(value = "/createMjPromotion")
    public ServiceResult createMjPromotion(@RequestBody CreateMjPromotionParam param) {
        param.setChannel(ChannelTypeEnums.C_OFFLINE.getCode());
        return ServiceResultUtil.resultSuccess(promotionService.createMjPromotion(param));
    }

    /**
     * 创建满赠规则--线上商城
     *
     * @return
     */
    @RequestMapping(value = "/createCmallMzPromotion")
    public ServiceResult createCmallMzPromotion(@RequestBody CreateMzPromotionParam param) {
        param.setChannel(ChannelTypeEnums.C_ONLINE.getCode());
        return ServiceResultUtil.resultSuccess(promotionService.createMzPromotion(param));
    }

    /**
     * 创建买赠规则--线上商城
     *
     * @return
     */
    @RequestMapping(value = "/createCmallVipPromotion")
    public ServiceResult createCmallVipPromotion(@RequestBody CreateVipPromotionParam param) {
        param.setChannel(ChannelTypeEnums.C_ONLINE.getCode());
        return ServiceResultUtil.resultSuccess(promotionService.createVipPromotion(param));
    }

    /**
     * 创建买减规则--线上商城
     *
     * @return
     */
    @RequestMapping(value = "/createCmallMjPromotion")
    public ServiceResult createCmallMjPromotion(@RequestBody CreateMjPromotionParam param) {
        param.setChannel(ChannelTypeEnums.C_ONLINE.getCode());
        return ServiceResultUtil.resultSuccess(promotionService.createMjPromotion(param));
    }

    /**
     * 创建改价活动--线下门店
     * @param param
     * @return
     */
    @RequestMapping(value = "/createCpPromotion")
    public ServiceResult createCpPromotion(@RequestBody CreateCpPromotionParam param){
        List<PromotionAreaDto> areaDtos = param.getAreaDtos();
        //验证新版本白名单
        if (!CollectionUtils.isEmpty(areaDtos)) {
            List<Integer> shopIds = new ArrayList<>();
            areaDtos.forEach(areaDto->{
                shopIds.addAll(areaDto.getShopIds());
            });
            if (!CollectionUtils.isEmpty(shopIds)) {
                Shop shop = new Shop();
                shop.setPromotionGrayType(1);
                List<Shop> shopList = iQmExternalApiService.getShopByCondition(shop);
                if (!CollectionUtils.isEmpty(shopList)) {
                    List<Integer> whiteShopIds = new ArrayList<>();
                    shopList.forEach(shop1 -> {
                        whiteShopIds.add(shop1.getId().intValue());
                    });
                    if (!Collections.disjoint(whiteShopIds,shopIds)) {
                        log.info("总部改价-新版本用户不可使用老接口改价 shopId={} whiteShop={}", JSON.toJSONString(shopIds), JSON.toJSONString(whiteShopIds));
                        return ServiceResultUtil.resultError("所选门店中有新版本门店请重新操作："+JSON.toJSONString(whiteShopIds));
                    }
                }
            }
        }

        String traceId = TraceIdUtils.getTraceId();
        log.info("总部改价，traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            oldGatewayService.buildFullInfo(param);
            return ServiceResultUtil.resultSuccess(promotionService.createCpPromotion(param));
        } catch (BusinessException be) {
            log.warn("总部改价-业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
            return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), be.getErrorMsg());
        } catch (Exception e) {
            log.error("总部改价-异常: traceId={}, ex={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
        }
        return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), "系统异常");
    }

    /**
     *  门店创建改价活动
     * @param list
     * @return
     */
    @RequestMapping(value = "/createCpPromotionFromShop")
    public ServiceResult createCpPromotionFromShop(@RequestBody List<CreateCpFromShopParam> list){
        log.info("门店改价参数 {}",list.toString() );
        if(CollectionUtils.isEmpty(list)){
            return ServiceResultUtil.resultError("请求入参不正确");
        }
        //验证门店是否灰名单（新版本名单）
        List<Integer> shopIds = list.stream().map(CreateCpFromShopParam::getShopId).collect(Collectors.toList());
        Shop shop = new Shop();
        shop.setPromotionGrayType(1);
        List<Shop> shopList = iQmExternalApiService.getShopByCondition(shop);
        if (!CollectionUtils.isEmpty(shopList)) {
            List<Integer> whiteShopIds = new ArrayList<>();
            shopList.forEach(shop1 -> {
                whiteShopIds.add(shop1.getId().intValue());
            });
            if (!Collections.disjoint(whiteShopIds,shopIds)) {
                log.info("门店改价参数-当前门店是新版本用户不可使用老接口改价 shopId={} whiteShop={}", JSON.toJSONString(shopIds), JSON.toJSONString(whiteShopIds));
                return ServiceResultUtil.resultError("你为新版本用户，请联系区域负责更换新的安装包");
            }
        }

        String traceId = TraceIdUtils.getTraceId();
        log.info("门店改价，traceId={} param={}", traceId, JSON.toJSONString(list));
        try {
            CreateCpPromotionParam param = oldGatewayService.buildParam(list);
            oldGatewayService.buildFullInfo(param);
            return ServiceResultUtil.resultSuccess(promotionService.createCpPromotion(param));
        } catch (BusinessException be) {
            log.warn("门店改价-业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
            return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), be.getErrorMsg());
        } catch (Exception e) {
            log.error("门店改价-异常: traceId={}, ex={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
        }
        return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), "系统异常");
    }

    @RequestMapping(value = "/checkSkuInVipPromotion")
    public ServiceResult checkSkuInVipPromotion(@RequestBody List<CreateCpFromShopParam> list){
        return ServiceResultUtil.resultSuccess(promotionService.checkSkuInVipPromotion(list));
    }

}
