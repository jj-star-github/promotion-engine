package com.qmfresh.promotion.gateway;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.promotion.PageQueryActivityParam;
import com.qmfresh.promotion.bean.promotion.PromotionActivityBean;
import com.qmfresh.promotion.bean.promotion.QueryActivityParam;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.common.utils.TraceIdUtils;
import com.qmfresh.promotion.dto.IsActGoodParam;
import com.qmfresh.promotion.dto.SearchAfterVipParam;
import com.qmfresh.promotion.dto.SearchVipActParam;
import com.qmfresh.promotion.dto.VipSkuVo;
import com.qmfresh.promotion.dto.base.ListWithPage;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.promotion.platform.interfaces.common.BizCode;
import com.qmfresh.promotion.service.IPromotionActivityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.util.StringUtil;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wyh on 2019/6/15.
 *
 * @author wyh
 */
@RestController
@Api(value = "value = /promotion/activity",tags = "促销活动模块")
@RequestMapping(value = "/promotion/activity")
@Slf4j
public class PromotionActivityController {
    @Resource
    private IPromotionActivityService promotionActivityService;

    @ApiOperation(value = "根据活动id获取促销活动信息，外部调用：C端商城")
    @RequestMapping(value = "/queryPromotionActivityById")
    public ServiceResult<PromotionActivity> queryPromotionActivityById(@RequestBody Long activityId) {
        return ServiceResultUtil.resultSuccess(promotionActivityService.queryPromotionActivityById(activityId));
    }

    @RequestMapping(value = "/pageQuery")
    public ServiceResult<ListWithPage<PromotionActivityBean>> pageQuery(@RequestBody PageQueryActivityParam param) {
        return ServiceResultUtil.resultSuccess(promotionActivityService.pageQuery(param));
    }

    @RequestMapping(value = "/queryByCondition")
    public ServiceResult<List<PromotionActivity>> queryByCondition(@RequestBody QueryActivityParam param) {
        return ServiceResultUtil.resultSuccess(promotionActivityService.queryByCondition(param));
    }

    @RequestMapping(value = "/online")
    public ServiceResult<Boolean> online(@RequestBody PromotionActivity param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("活动上线，traceId = {} param= {}", traceId, JSON.toJSONString(param));
        try {
            if (null == param.getUpdateUserId()) {
                param.setUpdateUserId(param.getOperatorId());
            }
            if (StringUtil.isEmpty(param.getUpdateUserName())) {
                param.setUpdateUserName(param.getOperatorName());
            }
            return ServiceResultUtil.resultSuccess(promotionActivityService.online(param));
        } catch (BusinessException be) {
            log.warn("活动上线-业务异常，traceId={} errorMsg={}", traceId, traceId, be.getErrorMsg());
            return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), be.getErrorMsg());
        } catch (Exception e) {
            log.error("活动上线-异常: traceId={}, ex={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
        }
        return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), "系统异常");
    }

    @RequestMapping(value = "/stop")
    public ServiceResult<Boolean> stop(@RequestBody PromotionActivity param) {
        log.info("活动停止，param= {}", JSON.toJSONString(param));
        if (null == param.getUpdateUserId()) {
            param.setUpdateUserId(param.getOperatorId());
        }
        if (StringUtil.isEmpty(param.getUpdateUserName())) {
            param.setUpdateUserName(param.getOperatorName());
        }
        return ServiceResultUtil.resultSuccess(promotionActivityService.stop(param));
    }

    @RequestMapping(value = "/delete")
    public ServiceResult<Boolean> delete(@RequestBody PromotionActivity param) {
        log.info("活动删除，param= {}", JSON.toJSONString(param));
        if (null == param.getUpdateUserId()) {
            param.setUpdateUserId(param.getOperatorId());
        }
        if (StringUtil.isEmpty(param.getUpdateUserName())) {
            param.setUpdateUserName(param.getOperatorName());
        }
        return ServiceResultUtil.resultSuccess(promotionActivityService.delete(param));
    }

    @RequestMapping(value = "/queryCurrentPromotionActivity")
    public ServiceResult queryCurrentPromotionActivity(@RequestBody QueryActivityParam query) {
        if (!ListUtil.isNullOrEmpty(query.getShopIds()) || null != query.getShopId()) {
            return ServiceResultUtil.resultSuccess(promotionActivityService.queryPromotionActivityByShop(query));
        }
        return ServiceResultUtil.resultSuccess(promotionActivityService.queryCurrentPromotionActivity(query));
    }
    //查看门店当天的商品是否存在会员价格
    @RequestMapping(value = "/searchVipSku")
    public ServiceResult searchVipSku(@RequestBody SearchVipActParam param) {
    	List<VipSkuVo> searchVipSku = promotionActivityService.searchVipSku(param);
    	return ServiceResultUtil.resultSuccess(searchVipSku);
    }
    //门店商品后三天的会员商品情况
    @RequestMapping(value = "/getAfterVipdays")
    public ServiceResult getAfterVipdays(@RequestBody SearchAfterVipParam param) {
    	List<Integer> afterVipdays = promotionActivityService.getAfterVipdays(param);
    	return ServiceResultUtil.resultSuccess(afterVipdays);
    }
    //查看门店商品是否参与了活动
    @RequestMapping(value = "/isActGood")
    public ServiceResult isActGood(@RequestBody IsActGoodParam param) {
    	boolean actGood = promotionActivityService.isActGood(param);
    	return ServiceResultUtil.resultSuccess(actGood);
    }
}
