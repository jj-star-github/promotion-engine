package com.qmfresh.promotion.gateway;

import com.alibaba.fastjson.JSON;
import com.qmfresh.promotion.bean.gis.Shop;
import com.qmfresh.promotion.bean.price.ModifyVipPriceBean;
import com.qmfresh.promotion.bean.promotion.cache.CacheCommand;
import com.qmfresh.promotion.bean.sku.SsuQueryForOnline;
import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.common.utils.DateUtil;
import com.qmfresh.promotion.common.utils.ListUtil;
import com.qmfresh.promotion.common.utils.TraceIdUtils;
import com.qmfresh.promotion.dto.*;
import com.qmfresh.promotion.dto.qmpp.Class2Result;
import com.qmfresh.promotion.entity.AreaScopeShop;
import com.qmfresh.promotion.external.service.IQmExternalApiService;
import com.qmfresh.promotion.platform.domain.shared.BusinessException;
import com.qmfresh.promotion.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.promotion.platform.interfaces.MVCExceptionHandle;
import com.qmfresh.promotion.platform.interfaces.common.BizCode;
import com.qmfresh.promotion.service.IOldGatewayService;
import com.qmfresh.promotion.platform.domain.shared.LogExceptionStackTrace;
import com.qmfresh.promotion.platform.interfaces.common.BizCode;
import com.qmfresh.promotion.service.IPromotionService;
import com.qmfresh.promotion.strategy.facade.PromotionExecuteFacade;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.util.StringUtil;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by wyh on 2019/6/4.
 *
 * @author wyh
 */
@RestController
@RequestMapping(value = "/promotion")
@Slf4j
public class PromotionController {

    @Resource
    private IPromotionService promotionService;
    @Resource
    private IQmExternalApiService qmExternalApiService;
    @Resource
    private PromotionExecuteFacade promotionExecuteFacade;
    @Resource
    private IOldGatewayService oldGatewayService;

    @RequestMapping(value = "/executeStrategy")
    public ServiceResult executeStrategy(@RequestBody PromotionProtocol protocol) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("执行促销，traceId={} protocol={}", traceId, JSON.toJSONString(protocol));
        Assert.notNull(protocol.getChannel(), "渠道不能为空");
        Assert.isTrue(!ListUtil.isNullOrEmpty(protocol.getPromotionSsuList()), "商品列表不能为空");

        long beginTime = DateUtil.getTimeStamp();
        log.info("开始时间 traceId={} beginTime={}", traceId, beginTime);

        ServiceResult sr = new ServiceResult(BizCode.FAILED.getCode(), "系统异常");
        try {
            sr = ServiceResultUtil.resultSuccess(promotionExecuteFacade.executeStrategy(protocol));
        } catch (BusinessException be) {
            log.warn("执行促销-业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
        } catch (Exception e) {
            log.error("执行促销-异常: traceId={}, ex={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
        }
        long endTime = DateUtil.getTimeStamp();
        long diffTime = endTime - beginTime;
        log.info("结束时间" + endTime + "，耗时：" + diffTime + "ms");
        if (diffTime > 1000) {
            log.warn("执行促销服务时间超过1000ms");
        }
        log.info("执行促销返回：traceId={} sr={}", traceId, JSON.toJSONString(sr));
        return sr;
    }

    @RequestMapping(value = "/executeStrategyNew")
    public ServiceResult executeStrategyNew(@RequestBody PromotionProtocol protocol) {
//        return ServiceResultUtil.resultError("不走促销");
        String traceId = TraceIdUtils.getTraceId();
        log.info("执行促销，traceId={} protocol={}", traceId, JSON.toJSONString(protocol));
        Assert.notNull(protocol.getChannel(), "渠道不能为空");
        Assert.isTrue(!ListUtil.isNullOrEmpty(protocol.getPromotionSsuList()), "商品列表不能为空");
        long beginTime = DateUtil.getTimeStamp();
        ServiceResult sr = new ServiceResult(BizCode.FAILED.getCode(), "系统异常");
        try {
            sr = ServiceResultUtil.resultSuccess(promotionExecuteFacade.executeStrategy(protocol));
        } catch (BusinessException be) {
            log.warn("执行促销-业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
        } catch (Exception e) {
            log.error("执行促销-异常: traceId={}, ex={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
        }
        long endTime = DateUtil.getTimeStamp();
        long diffTime = endTime - beginTime;
        log.info("结束时间" + endTime + "，耗时：" + diffTime + "ms");
        if (diffTime > 1000) {
            log.warn("执行促销服务时间超过1000ms");
        }
        log.info("执行促销返回：traceId={} sr={}", traceId, JSON.toJSONString(sr));
        return sr;
    }

    @RequestMapping(value = "/mzPromotionCampOn")
    public ServiceResult mzPromotionCampOn(@RequestBody PromotionMzCampOnReq protocol) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("满赠促销预占，traceId = {} protocal = {}", traceId, JSON.toJSONString(protocol));
        if (StringUtil.isBlank(protocol.getOrderCode())) {
            return ServiceResultUtil.resultError("订单号");
        }
        if (ListUtil.isNullOrEmpty(protocol.getCampOnList())) {
            return ServiceResultUtil.resultError("预占列表不能为空");
        }
        try {
            return ServiceResultUtil.resultSuccess(promotionExecuteFacade.mzPromotionCampOn(protocol));
        } catch (BusinessException be) {
            log.warn("满赠促销预占-业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
            return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), be.getErrorMsg());
        } catch (Exception e) {
            log.error("满赠促销预占-异常: traceId={}, ex={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
        }
        return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), "系统异常");
    }

    /**
     * 修改会员价格
     *
     * @return
     */
    @RequestMapping(value = "/modifyVipPrice")
    public ServiceResult modifyVipPrice(@RequestBody ModifyVipPriceBean param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("修改会员价格，traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            return ServiceResultUtil.resultSuccess(promotionService.modifyVipPrice(param));
        } catch (BusinessException be) {
            log.warn("修改会员价格-业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
            return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), be.getErrorMsg());
        } catch (Exception e) {
            log.error("修改会员价格-异常: traceId={}, ex={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
        }
        return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), "系统异常");
    }

    /**
     * 获取促销详情
     *
     * @return
     */
    @RequestMapping(value = "/getPromotionDetail")
    public ServiceResult getPromotionDetail(@RequestBody Long activityId) {
        return ServiceResultUtil.resultSuccess(promotionService.getPromotionDetail(activityId));
    }

    /**
     * 接口兼容
     * @param param
     * @return
     */
    @RequestMapping(value = "/getPromotionDetailNew")
    public ServiceResult getPromotionDetailNew(@RequestBody QueryActivityParam param) {
        PromotionDto promotionDto = promotionService.getPromotionDetail(param.getActivityId());
        if (promotionDto == null) {
            return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), "未获取到活动详情，活动id:" + param.getActivityId());
        }
        if (!ListUtil.isNullOrEmpty(promotionDto.getAreaScopeShops())) {
            List<Shop> shops = qmExternalApiService.selectShopByIds(promotionDto.getAreaScopeShops().stream().map(AreaScopeShop::getShopId).collect(Collectors.toList()));
            Map<String, Shop> shopMap = ListUtil.listToMap(shops);
            promotionDto.getAreaScopeShops().forEach(
                    item -> item.setShopName(shopMap.get(item.getShopId().toString()).getShopName())
            );
        }
        if (!ListUtil.isNullOrEmpty(promotionDto.getActivityClass2s())) {
            List<Class2Result> class2s = qmExternalApiService.getClass2s();
            Map<String, Class2Result> c2Map = ListUtil.listToMap(class2s);
            promotionDto.getActivityClass2s().forEach(
                    ac2 -> {
                        if (ac2.getClassId().equals(-1)) {
                            ac2.setClassName("全部");
                        } else {
                            ac2.setClassName(c2Map.get(ac2.getClassId().toString()).getName());
                        }
                    }
            );
        }
        return ServiceResultUtil.resultSuccess(promotionDto);
    }

    /**
     * 修改买赠规则
     *
     * @return
     */
    @RequestMapping(value = "/modifyBzPromotion")
    public ServiceResult modifyBzPromotion(@RequestBody CreateBzPromotionParam param) {
        return ServiceResultUtil.resultSuccess(promotionService.modifyBzPromotion(param));
    }

    /**
     * 修改单品促销规则
     *
     * @return
     */
    @RequestMapping(value = "/modifyDpPromotion")
    public ServiceResult modifyDpPromotion(@RequestBody CreateDpPromotionParam param) {
        return ServiceResultUtil.resultSuccess(promotionService.modifyDpPromotion(param));
    }

    /**
     * 修改满赠促销规则
     *
     * @return
     */
    @RequestMapping(value = "/modifyMzPromotion")
    public ServiceResult modifyMzPromotion(@RequestBody CreateMzPromotionParam param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("修改满赠促销规则，traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            return ServiceResultUtil.resultSuccess(promotionService.modifyMzPromotion(param));
        } catch (BusinessException be) {
            log.warn("修改满赠促销规则-业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
            return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), be.getErrorMsg());
        } catch (Exception e) {
            log.error("修改满赠促销规则-异常: traceId={}, ex={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
        }
        return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), "系统异常");
    }

    /**
     * 修改满赠促销规则
     *
     * @return
     */
    @RequestMapping(value = "/modifyVipPromotion")
    public ServiceResult modifyVipPromotion(@RequestBody CreateVipPromotionParam param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("修改满赠促销规则，traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            return ServiceResultUtil.resultSuccess(promotionService.modifyVipPromotion(param));
        } catch (BusinessException be) {
            log.warn("修改满赠促销规则-业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
            return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), be.getErrorMsg());
        } catch (Exception e) {
            log.error("修改满赠促销规则-异常: traceId={}, ex={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
        }
        return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), "系统异常");
    }

    /**
     * 修改改价促销规则
     *
     * @param param
     * @return
     */
    @RequestMapping(value = "/modifyCpPromotion")
    public ServiceResult modifyCpPromotion(@RequestBody CreateCpPromotionParam param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("修改改价促销规则，traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            param.setAreaDtos(oldGatewayService.getAreaDto(param.getAreaDtos().get(0).getShopIds()));
            param.setPromotionProductDto(oldGatewayService.convertSkuToSsuFormat(param.getAreaDtos().get(0).getShopIds(),param.getChannel(),param.getPromotionProductDto()));
            return ServiceResultUtil.resultSuccess(promotionService.modifyCpPromotion(param));
        } catch (BusinessException be) {
            log.warn("修改改价促销规则-业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
            return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), be.getErrorMsg());
        } catch (Exception e) {
            log.error("修改改价促销规则-异常: traceId={}, ex={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
        }
        return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), "系统异常");
    }

    /**
     * 查询线上满赠的商品
     *
     * @return
     */
    @RequestMapping(value = "/queryPromotionGoodsOnline")
    public ServiceResult queryPromotionGoodsOnline(@RequestBody SsuQueryForOnline param) {
        return ServiceResultUtil.resultSuccess(promotionService.getSsuListByParam(param));
    }

    /**
     * 删除改价促销活动
     *
     * @param param
     * @return
     */
    @RequestMapping(value = "/deleteCpPromotion")
    public ServiceResult deleteCpPromotion(@RequestBody CreateCpPromotionParam param) {
        log.info("删除改价活动，param= {}", JSON.toJSONString(param));
        return ServiceResultUtil.resultSuccess(promotionService.deleteCpPromotion(param));
    }

    /**
     * 查询修改后的价格
     *
     * @param list
     * @return
     */
    @RequestMapping(value = "/queryChangePrice")
    public ServiceResult queryChangePrice(@RequestBody List<PriceChangeParam> list) {
        return ServiceResultUtil.resultSuccess(promotionService.queryChangePrice(list));
    }

    /**
     * 门店提交的活动状态修改
     *
     * @param param
     * @return
     */
    @RequestMapping(value = "/modifyCpPromotionStatus")
    public ServiceResult modifyCpPromotionStatus(@RequestBody ModifyPriceParam<ActivateCpPromotionParam> param) {
        String traceId = TraceIdUtils.getTraceId();
        log.info("门店提交的活动状态修改，traceId={} param={}", traceId, JSON.toJSONString(param));
        try {
            return ServiceResultUtil.resultSuccess(promotionService.modifyCpPromotionStatus(param));
        } catch (BusinessException be) {
            log.warn("门店提交的活动状态修改-业务异常，traceId={} errorMsg={}", traceId, be.getErrorMsg());
            return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), be.getErrorMsg());
        } catch (Exception e) {
            log.error("门店提交的活动状态修改-异常: traceId={}, ex={}", traceId, LogExceptionStackTrace.errorStackTrace(e));
        }
        return ServiceResultUtil.resultError(BizCode.FAILED.getCode(), "系统异常");
    }


    @RequestMapping(value = "/cache/areascopeshop", method = RequestMethod.POST)
    public ServiceResult<Boolean> cacheAreaScopeShop(@RequestBody CacheCommand cacheCommand) {
        log.info("查询缓存{}", JSON.toJSONString(cacheCommand));
        promotionExecuteFacade.initFilterAreaCache(cacheCommand.isRefreshAll(), cacheCommand.getId());
        return ServiceResultUtil.resultSuccess(true);
    }

    @RequestMapping(value = "/cache/areascopeshop/{id}", method = RequestMethod.POST)
    public ServiceResult<String> cacheAreaScopeShopGet(@PathVariable("id") Long id) {
        return ServiceResultUtil.resultSuccess(promotionExecuteFacade.get(id));
    }

}
