package com.qmfresh.promotion.gateway;

import com.qmfresh.promotion.common.business.ServiceResult;
import com.qmfresh.promotion.common.business.ServiceResultUtil;
import com.qmfresh.promotion.dto.CreatePromotionSkuParam;
import com.qmfresh.promotion.dto.QueryPromotionSkuParam;
import com.qmfresh.promotion.dto.UpdatePromotionSkuParam;
import com.qmfresh.promotion.service.IPromotionSkuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: promotion-engine
 * @description: 促销商品管理服务
 * @author: xbb
 * @create: 2019-11-29 16:42
 **/
@RestController
@RequestMapping(value = "/promotion/sku")
@Slf4j
public class PromotionSkuController {

    @Autowired
    private IPromotionSkuService promotionSkuService;

    /**
     * 分页查询
     * @param param
     * @return
     */
    @RequestMapping(value = "/pageList")
    public ServiceResult pageList(@RequestBody QueryPromotionSkuParam param){
        return ServiceResultUtil.resultSuccess(promotionSkuService.getPageList(param));
    }

    /**
     * 创建促销商品
     * @param param
     * @return
     */
    @RequestMapping(value = "/createPromotionSku")
    public ServiceResult createPromotionSku(@RequestBody CreatePromotionSkuParam param){
        return ServiceResultUtil.resultSuccess(promotionSkuService.createPromotionSku(param));
    }

    /**
     * 更新促销商品
     * @param param
     * @return
     */
    @RequestMapping(value = "/updatePromotionSku")
    public ServiceResult updatePromotionSku(@RequestBody List<UpdatePromotionSkuParam> param){
        return ServiceResultUtil.resultSuccess(promotionSkuService.updatePromotionSku(param));
    }
}
