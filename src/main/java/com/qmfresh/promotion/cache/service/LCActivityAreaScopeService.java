package com.qmfresh.promotion.cache.service;

import com.qmfresh.promotion.cache.guava.GuavaAbstractLoadingCache;
import com.qmfresh.promotion.cache.local.ILocalCache;
import com.qmfresh.promotion.entity.ActivityAreaScope;
import com.qmfresh.promotion.manager.IActivityAreaScopeManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by wyh on 2019/5/30.
 *
 * @author wyh
 */
@Service
public class LCActivityAreaScopeService extends GuavaAbstractLoadingCache<Long, ActivityAreaScope> implements ILocalCache<Long, ActivityAreaScope> {

    @Resource
    private IActivityAreaScopeManager activityAreaScopeManager;

    private LCActivityAreaScopeService() {
        setMaximumSize(100);
    }

    /**
     * 获取活动下的门店，key=${activity_id}
     *
     * @param key
     * @return
     */
    @Override
    protected ActivityAreaScope fetchData(Long key) {
        return activityAreaScopeManager.activityAreaScope(key);
    }

    @Override
    public ActivityAreaScope get(Long key) {
        try {
            return getValue(key);
        } catch (Exception e) {
            logger.error("未获取到key对应value", key, e);
            return null;
        }
    }
}
