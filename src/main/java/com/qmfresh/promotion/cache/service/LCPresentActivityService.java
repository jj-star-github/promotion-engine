package com.qmfresh.promotion.cache.service;

import com.qmfresh.promotion.cache.guava.GuavaAbstractLoadingCache;
import com.qmfresh.promotion.cache.local.ILocalCache;
import com.qmfresh.promotion.entity.PresentActivity;
import com.qmfresh.promotion.manager.IPresentActivityManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * Created by wyh on 2019/5/30.
 *
 * @author wyh
 */
@Service
public class LCPresentActivityService extends GuavaAbstractLoadingCache<Long, List<PresentActivity>> implements ILocalCache<Long, List<PresentActivity>> {

    @Resource
    private IPresentActivityManager presentActivityManager;

    private LCPresentActivityService() {
        setMaximumSize(100);
    }

    /**
     * 获取活动下的门店，key=${activity_id}
     *
     * @param key
     * @return
     */
    @Override
    protected List<PresentActivity> fetchData(Long key) {
        return presentActivityManager.presentActivities(Collections.singletonList(key));
    }

    @Override
    public List<PresentActivity> get(Long key) {
        try {
            return getValue(key);
        } catch (Exception e) {
            logger.error("未获取到key对应value", key, e);
            return null;
        }
    }
}
