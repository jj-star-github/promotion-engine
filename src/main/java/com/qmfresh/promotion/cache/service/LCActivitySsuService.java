package com.qmfresh.promotion.cache.service;

import com.qmfresh.promotion.cache.guava.GuavaAbstractLoadingCache;
import com.qmfresh.promotion.cache.local.ILocalCache;
import com.qmfresh.promotion.entity.ActivitySsu;
import com.qmfresh.promotion.manager.IActivitySsuManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * Created by wyh on 2019/5/30.
 *
 * @author wyh
 */
@Service
public class LCActivitySsuService extends GuavaAbstractLoadingCache<Long, List<ActivitySsu>> implements ILocalCache<Long, List<ActivitySsu>> {

    @Resource
    private IActivitySsuManager activitySsuManager;

    private LCActivitySsuService() {
        setMaximumSize(100);
    }

    @Override
    protected List<ActivitySsu> fetchData(Long key) {
        return activitySsuManager.activitySsu(Collections.singletonList(key));
    }

    @Override
    public List<ActivitySsu> get(Long key) {
        try {
            return getValue(key);
        } catch (Exception e) {
            logger.error("未获取到key对应value", key, e);
            return null;
        }
    }
}
