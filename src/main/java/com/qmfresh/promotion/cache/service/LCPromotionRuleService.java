package com.qmfresh.promotion.cache.service;

import com.qmfresh.promotion.cache.guava.GuavaAbstractLoadingCache;
import com.qmfresh.promotion.cache.local.ILocalCache;
import com.qmfresh.promotion.entity.PromotionRule;
import com.qmfresh.promotion.manager.IPromotionRuleManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * Created by wyh on 2019/5/30.
 *
 * @author wyh
 */
@Service
public class LCPromotionRuleService extends GuavaAbstractLoadingCache<Long, PromotionRule> implements ILocalCache<Long, PromotionRule> {
    @Resource
    private IPromotionRuleManager promotionRuleManager;

    private LCPromotionRuleService() {
        setMaximumSize(100);
    }

    @Override
    protected PromotionRule fetchData(Long key) {
        List<PromotionRule> promotionRules = promotionRuleManager.promotionRules(Collections.singletonList(key));
        return promotionRules.isEmpty() ? null : promotionRules.get(0);
    }

    @Override
    public PromotionRule get(Long key) {
        try {
            return getValue(key);
        } catch (Exception e) {
            logger.error("未获取到key对应value", key, e);
            return null;
        }
    }
}
