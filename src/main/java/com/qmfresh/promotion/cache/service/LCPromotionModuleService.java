package com.qmfresh.promotion.cache.service;

import com.qmfresh.promotion.cache.guava.GuavaAbstractLoadingCache;
import com.qmfresh.promotion.cache.local.ILocalCache;
import com.qmfresh.promotion.entity.PromotionModule;
import com.qmfresh.promotion.manager.IPromotionModuleManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by wyh on 2019/5/30.
 *
 * @author wyh
 */
@Service
public class LCPromotionModuleService extends GuavaAbstractLoadingCache<Long, PromotionModule> implements ILocalCache<Long, PromotionModule> {
    @Resource
    private IPromotionModuleManager promotionModuleManager;

    private LCPromotionModuleService() {
        setMaximumSize(20); //最大缓存条数
    }

    @Override
    protected PromotionModule fetchData(Long key) {
        return promotionModuleManager.getById(key);
    }

    @Override
    public PromotionModule get(Long key) {
        try {
            return getValue(key);
        } catch (Exception e) {
            logger.error("未获取到key对应value", key, e);
            return null;
        }
    }
}
