package com.qmfresh.promotion.cache.service;

import com.qmfresh.promotion.cache.guava.GuavaAbstractLoadingCache;
import com.qmfresh.promotion.cache.local.ILocalCache;
import com.qmfresh.promotion.entity.PresentSsu;
import com.qmfresh.promotion.manager.IPresentActivityManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * Created by wyh on 2019/5/30.
 *
 * @author wyh
 */
@Service
public class LCPresentSsuService extends GuavaAbstractLoadingCache<Long, List<PresentSsu>> implements ILocalCache<Long, List<PresentSsu>> {

    @Resource
    private IPresentActivityManager presentSsuManager;

    private LCPresentSsuService() {
        setMaximumSize(100);
    }

    @Override
    protected List<PresentSsu> fetchData(Long key) {
        return presentSsuManager.presentSsu(Collections.singletonList(key));
    }

    @Override
    public List<PresentSsu> get(Long key) {
        try {
            return getValue(key);
        } catch (Exception e) {
            logger.error("未获取到key对应value", key, e);
            return null;
        }
    }
}
