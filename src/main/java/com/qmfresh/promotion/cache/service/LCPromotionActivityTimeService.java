package com.qmfresh.promotion.cache.service;

import com.qmfresh.promotion.cache.guava.GuavaAbstractLoadingCache;
import com.qmfresh.promotion.cache.local.ILocalCache;
import com.qmfresh.promotion.entity.PromotionActivityTime;
import com.qmfresh.promotion.manager.IPromotionActivityTimeManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * Created by wyh on 2019/5/30.
 *
 * @author wyh
 */
@Service
public class LCPromotionActivityTimeService extends GuavaAbstractLoadingCache<Long, List<PromotionActivityTime>> implements ILocalCache<Long, List<PromotionActivityTime>> {
    @Resource
    private IPromotionActivityTimeManager promotionActivityTimeManager;

    private LCPromotionActivityTimeService() {
        setMaximumSize(100);
    }

    @Override
    protected List<PromotionActivityTime> fetchData(Long key) {
        return promotionActivityTimeManager.promotionActivityTimes(Collections.singletonList(key));
    }

    @Override
    public List<PromotionActivityTime> get(Long key) {
        try {
            return getValue(key);
        } catch (Exception e) {
            logger.error("未获取到key对应value", key, e);
            return null;
        }
    }
}
