package com.qmfresh.promotion.cache.service;

import com.qmfresh.promotion.cache.guava.GuavaAbstractLoadingCache;
import com.qmfresh.promotion.cache.local.ILocalCache;
import com.qmfresh.promotion.entity.PromotionActivity;
import com.qmfresh.promotion.manager.IPromotionActivityManager;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/5/30.
 *
 * @author wyh
 */
@Service
public class LCPromotionActivityService extends GuavaAbstractLoadingCache<Long, PromotionActivity> implements ILocalCache<Long, PromotionActivity> {
    @Resource
    private IPromotionActivityManager promotionActivityManager;

    private LCPromotionActivityService() {
        setMaximumSize(100);
    }

    @Override
    protected PromotionActivity fetchData(Long key) {
        return promotionActivityManager.getById(key);
    }

    @Override
    public PromotionActivity get(Long key) {
        try {
            return getValue(key);
        } catch (Exception e) {
            logger.error("未获取到key对应value", key, e);
            return null;
        }
    }
}
