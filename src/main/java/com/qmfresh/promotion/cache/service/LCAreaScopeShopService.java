package com.qmfresh.promotion.cache.service;

import com.qmfresh.promotion.cache.guava.GuavaAbstractLoadingCache;
import com.qmfresh.promotion.cache.local.ILocalCache;
import com.qmfresh.promotion.entity.AreaScopeShop;
import com.qmfresh.promotion.manager.IAreaScopeShopManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wyh on 2019/5/30.
 *
 * @author wyh
 */
@Service
public class LCAreaScopeShopService extends GuavaAbstractLoadingCache<Long, List<AreaScopeShop>> implements ILocalCache<Long, List<AreaScopeShop>> {

    @Resource
    private IAreaScopeShopManager areaScopeShopManager;

    private LCAreaScopeShopService() {
        setMaximumSize(100);
    }

    /**
     * 获取活动下的门店，key=${area_scope_id}
     *
     * @param key
     * @return
     */
    @Override
    protected List<AreaScopeShop> fetchData(Long key) {
        return areaScopeShopManager.areaScope(key);
    }

    @Override
    public List<AreaScopeShop> get(Long key) {
        try {
            return getValue(key);
        } catch (Exception e) {
            logger.error("未获取到key对应value", key, e);
            return null;
        }
    }
}
