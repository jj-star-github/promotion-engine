package com.qmfresh.promotion.cache.service;

import com.qmfresh.promotion.cache.guava.GuavaAbstractLoadingCache;
import com.qmfresh.promotion.cache.local.ILocalCache;
import com.qmfresh.promotion.entity.ActivityClass2;
import com.qmfresh.promotion.manager.IActivityClass2Manager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wyh on 2019/5/30.
 *
 * @author wyh
 */
@Service
public class LCActivityClass2Service extends GuavaAbstractLoadingCache<Long, List<ActivityClass2>> implements ILocalCache<Long, List<ActivityClass2>> {

    @Resource
    private IActivityClass2Manager activityClass2Manager;

    private LCActivityClass2Service() {
        setMaximumSize(100);
    }

    /**
     * 获取活动下的二级分类
     *
     * @param key
     * @return
     */
    @Override
    protected List<ActivityClass2> fetchData(Long key) {
        return activityClass2Manager.activityClass2(key);
    }

    @Override
    public List<ActivityClass2> get(Long key) {
        try {
            return getValue(key);
        } catch (Exception e) {
            logger.error("未获取到key对应value", key, e);
            return null;
        }
    }
}
