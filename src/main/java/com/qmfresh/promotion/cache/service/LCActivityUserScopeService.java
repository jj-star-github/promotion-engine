package com.qmfresh.promotion.cache.service;

import com.qmfresh.promotion.cache.guava.GuavaAbstractLoadingCache;
import com.qmfresh.promotion.cache.local.ILocalCache;
import com.qmfresh.promotion.entity.ActivityUserScope;
import com.qmfresh.promotion.manager.IActivityUserScopeManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * Created by wyh on 2019/5/30.
 *
 * @author wyh
 */
@Service
public class LCActivityUserScopeService extends GuavaAbstractLoadingCache<Long, ActivityUserScope> implements ILocalCache<Long, ActivityUserScope> {

    @Resource
    private IActivityUserScopeManager activityUserScopeManager;

    private LCActivityUserScopeService() {
        setMaximumSize(100);
    }

    /**
     * 获取活动下的门店，key=${activity_id}
     *
     * @param key
     * @return
     */
    @Override
    protected ActivityUserScope fetchData(Long key) {
        List<ActivityUserScope> activityUserScopeList = activityUserScopeManager.activityUserScope(Collections.singletonList(key));
        return activityUserScopeList.isEmpty() ? null : activityUserScopeList.get(0);

    }

    @Override
    public ActivityUserScope get(Long key) {
        try {
            return getValue(key);
        } catch (Exception e) {
            logger.error("未获取到key对应value", key, e);
            return null;
        }
    }
}
