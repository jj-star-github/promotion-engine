package com.qmfresh.promotion.cache.service;

import com.qmfresh.promotion.cache.guava.GuavaAbstractLoadingCache;
import com.qmfresh.promotion.cache.local.ILocalCache;
import com.qmfresh.promotion.entity.UserScopeGroup;
import com.qmfresh.promotion.manager.IUserScopeGroupManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * Created by wyh on 2019/5/30.
 *
 * @author wyh
 */
@Service
public class LCUserScopeGroupService extends GuavaAbstractLoadingCache<Long, List<UserScopeGroup>> implements ILocalCache<Long, List<UserScopeGroup>> {

    @Resource
    private IUserScopeGroupManager userScopeGroupManager;

    private LCUserScopeGroupService() {
        setMaximumSize(100);
    }

    /**
     * 获取活动下的门店，key=${activity_id}
     *
     * @param key
     * @return
     */
    @Override
    protected List<UserScopeGroup> fetchData(Long key) {
        return userScopeGroupManager.userScopeGroup(Collections.singletonList(key));
    }

    @Override
    public List<UserScopeGroup> get(Long key) {
        try {
            return getValue(key);
        } catch (Exception e) {
            logger.error("未获取到key对应value", key, e);
            return null;
        }
    }
}
