package com.qmfresh.promotion.cache.service;

import com.qmfresh.promotion.cache.guava.GuavaAbstractLoadingCache;
import com.qmfresh.promotion.cache.local.ILocalCache;
import com.qmfresh.promotion.entity.AreaScopeCity;
import com.qmfresh.promotion.manager.IAreaScopeCityManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wyh on 2019/5/30.
 *
 * @author wyh
 */
@Service
public class LCAreaScopeCityService extends GuavaAbstractLoadingCache<Long, List<AreaScopeCity>> implements ILocalCache<Long, List<AreaScopeCity>> {

    @Resource
    private IAreaScopeCityManager areaScopeCityManager;

    private LCAreaScopeCityService() {
        setMaximumSize(100);
    }

    /**
     * 获取活动下的门店，key=${area_scope_id}
     *
     * @param key
     * @return
     */
    @Override
    protected List<AreaScopeCity> fetchData(Long key) {
        return areaScopeCityManager.areaScopeCity(key);
    }

    @Override
    public List<AreaScopeCity> get(Long key) {
        try {
            return getValue(key);
        } catch (Exception e) {
            logger.error("未获取到key对应value", key, e);
            return null;
        }
    }
}
