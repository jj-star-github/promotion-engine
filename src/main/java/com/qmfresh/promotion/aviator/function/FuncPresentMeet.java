package com.qmfresh.promotion.aviator.function;

import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.Expression;
import com.googlecode.aviator.Options;
import com.qmfresh.promotion.cache.service.LCPresentSsuService;
import com.qmfresh.promotion.entity.PresentActivity;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * Created by wyh on 2019/5/31.
 *
 * @author wyh
 */
@Service
public class FuncPresentMeet {

    @Resource
    private LCPresentSsuService lcPresentSsuService;

    public Boolean isMeet(PresentActivity activity) {
        AviatorEvaluator.setOption(Options.ALWAYS_PARSE_FLOATING_POINT_NUMBER_INTO_DECIMAL, true);
        Expression compiledExp = AviatorEvaluator.compile("send + give > limit");
        Map<String, Object> env = new HashMap<>();
        env.put("send", activity.getSendNum());
        env.put("give", lcPresentSsuService.get(activity.getId()).get(0).getGiveNum());
        env.put("limit", lcPresentSsuService.get(activity.getId()).get(0).getLimitNum());
        return (Boolean) compiledExp.execute(env);
    }

}
