## 促销中心

### 原则
```
    1. code is everything,put everything together!!!
    2. solid,if forgot,we only remember 'single responsibility' 
```

第一版本改版：

1. 技术栈升级
    * springboot2.2.x
    * mybatis-plus3.x 
    * redis 
    * xxl-job1.9.x 
    * rocketmq4.6.x
    * okhttp3.x(走fegin)
    * spring cloud版本走（Hoxton.SR1）
    * log4j2
    * joda基础时间库
2. 业务架构调整
    * 走ddd模式
    * 移除本地缓存，引入redis缓存
    
3. 原则
    * 保持原有业务不动
    * 压测出瓶劲    
4. 业务活动测试
    * 促销定价相关活动
        * 平台维度
            * 单品
            * 组合品【待定】
        * 门店维度
            * 单品
            * 组合品【待定】
        * 会员优惠
        * 改价促销
    * 促销满赠/满减/折扣相关业务活动
        * 商品维度
        * 订单维度
    * 促销指标维护
    
 
 
 ## 代码规范
 
 ### 1. 业务分层
   
 ```
 以DDD分层框架为标准
 
 application: 应用服务
 domain： 领域抽象
    model: 领域模型
    service： 领域中服务的概念，谨慎使用
    valueobj; 值对象
    shared：业务异常定义，业务编码，分页
 infrastructure ： 基础组件依赖
    mq： 封装mq的使用
    job： 封装job的使用
    http： 封装http的使用
 interfaces： 三方接口
    app： 应用方
        facade： 服务和接口服务封装和转换
        controller：接口层
    admin： 管理后台
        facade：服务和接口服务封装和转换
        controller:  接口层
        
  各个层次的依赖关系：
    application    -d-> domain
    interfaces     -d->  domain/application
    infrastructure -d-> domain
        mapper     ---> 数据库相关
            entity ---> 数据库实体
    domain -u-> infrastructure (使用）
  所有的依赖层次以domain层为核心，从domain向四周做同心环。
  
  
  注意：
    1. 枚举不要统一放在一个，而要放在他的领域相关的地方
    
    
        
 参考：1. https://gitee.com/xuzhouwang/dddsample-core?_from=gitee_search
      2. 《领域驱动设计》
      3. 《实现领域驱动》
      4. 《领域驱动精粹》
      5. https://www.cnblogs.com/uoyo/p/11951840.html(值对象的解释）
      
```
 ### 2. JAVA和MySQL规范
  
  ```
  以泰山版为基准，向最新兼容；保持更新和迭代
```

 ### 3. ORM访问层
  ```
  orm层以mybatis-plus为标准，mybatis为服务。金钱，库存相关的
  更新，必须以手写sql为标准，使用mybatis
  
  实体都是以PO结尾。
  PO依赖于领域对象。
  所有的orm访问都封装在领域的repository层中,在这里repository的命名是
  manager层。所以只有这一层有PO转领域对象
  ``` 

 ### 4. 接口层
 
 ```
   1. 基本参数：服务层会经过各种网关，根据不同网关会有不同的基本参数，
   这里定义2个基本参数，针对app和针对admin的，例如AppBaseDTO，BaseDTO。
   2. 分页对象：这里定义为：PageDTO，分页参数为pageNum和pageSize
   3. 所有接口的返回值都需要包ServiceResultDTO，但是在Application中不需要包装
   4. 接口用Swagger注解，线上去掉swagger的入口
   5. 接口的命名规范：/application/module/domain/operate： application表示应用方，例如管理后台（platform）
        门店app（shop），module表示这块业务领域模块，例如改价活动（cp），domain表示具体领域对象，例如（/shop/cp)
        门店改价，（/general/cp）表示总部改价，operate表示这个领域里的操作，例如create/page/edit/info/delete
        等等。每个path并不一定只有一个名字，可以是多个。只要统一是这一层。
```

### 5. 依赖层

```
  所有外部服务的基础服务提供实体都是VO对象
```


### 6. 基础工具

```
    1. 所有时间处理都使用joda包中的DateTime进行运算
    2. 调研hutool功能的使用
```


#### 1. mybatis-plus使用经验

```
   配置篇
    1. 分页必须使用PaginationInterceptor插件，配置limit为-1，支持超过500条的分页
    2. 在springboot中，可以使用mybatis-plus.configuration.log-impl的配置
   查询篇
    1. LamdaXXXQuery注意condition的处理，condition判断为空，条件不判定为空
    2. 可以使用.select(XXX)方法来进行对select的操作，这样能够充分利用索引，减少回表次数。但是一定要封装在manager层里面
    3. 可以使用查询的groupby来进行分组，用select来进行配置查询的字段，用来支持db层面的统计
    4. select在LamdaXXXQuery和
   更新篇
    1. update必须使用id更新

```

#### 2. RocketMQ使用经验
 ```
    原则篇：
        1. 必须考虑幂等性
        2. 必须考虑乱序性


```

### 3. JAVA编程规范

```
    1. java构造方法中抛异常，会抛出什么异常？
```



### 4. 门店APP接口获取shopId
```
    1. 自己得Controller继承ShopBaseController
    2.在方法内直接使用Integer shopId = getShopId(); 来获取
    
```
     
   