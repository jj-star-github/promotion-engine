/* 9:00:39 PM qm-test-24 qm_promotion */
ALTER TABLE `t_funds_change_note` ADD INDEX `idx_createday_deleted` (`create_day`, `is_deleted`);
/* 9:30:04 PM qm-test-24 qm_promotion */
ALTER TABLE `t_funds_change_note` ADD INDEX `idx_business_code` (`business_code`);

/* 9:54:17 PM qm-test-site qm_promotion */
ALTER TABLE `t_funds_settle_info` DROP INDEX `idx_create_day`;
/* 9:55:25 PM qm-test-site qm_promotion */
ALTER TABLE `t_funds_settle_info` ADD INDEX `idx_create_day_deleted` (`create_day`, `is_deleted`);
/* 10:01:58 PM qm-test-site qm_promotion */
ALTER TABLE `t_funds_settle_info` ADD INDEX `idx_create_date_deleted_realpay_funds_use_shopuse` (`create_day`, `is_deleted`, `real_pay`, `use_amount`, `shop_funds_amount`, `shop_use_amount`);

