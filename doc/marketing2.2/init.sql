CREATE TABLE `t_cp_create_log`
(
  `id`              bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `status`          tinyint(4)          NOT NULL DEFAULT '1' COMMENT '1=待创建 2=已完成',
  `complete_status` tinyint(4)          NOT NULL DEFAULT '1' COMMENT '1=待执行 2=已过期 3=已完成 3= 已终止',
  `name`            varchar(50)         NOT NULL DEFAULT '' COMMENT '任务名称',
  `request`         text                NOT NULL COMMENT 'json',
  `remark`          varchar(2048)       NOT NULL DEFAULT ' ' COMMENT '备注（最大只有2000字）',
  `gmt_create`      datetime            NOT NULL COMMENT '创建时间',
  `gmt_modified`    datetime            NOT NULL COMMENT '更新时间',
  `operator_name`   varchar(20)         NOT NULL DEFAULT '' COMMENT '操作人名称',
  `operator_id`     int(11)             NOT NULL COMMENT '操作人id',
  `type`            tinyint(4)          NOT NULL COMMENT '1=手动创建 2=excel导入3=外部系统 4=其他',
  `activity_id`     bigint(20)          NOT NULL DEFAULT '0' COMMENT '活动id，为了支持修改',
  `is_deleted`      tinyint(1)          NOT NULL DEFAULT '0' COMMENT '1表示已删除',
  PRIMARY KEY (`id`),
  KEY `idx_operator_status_gmtcreate` (`operator_id`, `status`, `gmt_create`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 100000
  DEFAULT CHARSET = utf8mb4;