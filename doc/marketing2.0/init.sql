CREATE TABLE `t_activity_change_price`
(
  `id`                       bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '改价活动id',
  `activity_id`              bigint(20)          NOT NULL COMMENT '促销活动id',
  `type`                     int(11)             NOT NULL COMMENT '类型：10=改价 11=门店改价 12=总部改价 13=会员改价 来自t_activity',
  `name`                     varchar(50)         NOT NULL DEFAULT '' COMMENT '活动名称(来自于t_activity）',
  `status`                   tinyint(4)          NOT NULL DEFAULT '1' COMMENT '1=待进行 2=改价中 3=进行中 4= 已取消 5= 关闭中 6=结束中 7=已关闭 8=已结束 9=已过期',
  `audit_status`             tinyint(4)          NOT NULL COMMENT '1=待审核 2=审核通过 3=审核失败',
  `audit_person_id`          bigint(20)          NOT NULL DEFAULT '0' COMMENT '审核人id',
  `audit_person_name`        varchar(20)         NOT NULL DEFAULT ' ' COMMENT '审核人名称',
  `audit_remark`             varchar(120)        NOT NULL DEFAULT ' ' COMMENT '审核备注',
  `plan_activity_start_time` datetime            NOT NULL COMMENT '活动开始时间',
  `plan_activity_end_time`   datetime            NOT NULL COMMENT '活动结束时间',
  `remark`                   varchar(50)         NOT NULL DEFAULT '' COMMENT '结束备注',
  `gmt_create`               datetime            NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified`             datetime            NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_deleted`               tinyint(1)          NOT NULL COMMENT '1=已删除',
  `version`                  int(11)             NOT NULL DEFAULT '0' COMMENT '版本信息',
  `create_person_id`         bigint(20)          NOT NULL DEFAULT '0' COMMENT '创建人id：0=系统',
  `plan_marketing_amount`    decimal(15, 4)      NOT NULL DEFAULT '0.0000' COMMENT ' 计划营销费用',
  `shop_id`                  int(11)             NOT NULL COMMENT '门店id',
  `shop_name`                varchar(50)                  DEFAULT NULL COMMENT '门店名称',
  `shop_city_id`             int(11)                      DEFAULT NULL,
  `subject_id`               bigint(20)          NOT NULL COMMENT '账户主体id：shopId/大区/平台',
  `real_activity_start_time` datetime            NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '实际活动开始时间',
  `real_activity_end_time`   datetime            NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '实际活动结束时间',
  `create_person_name`       varchar(20)         NOT NULL DEFAULT ' ' COMMENT '创建人名称',
  `last_operator_id`         bigint(20)          NOT NULL COMMENT '最近操作人id',
  `last_operator_name`       varchar(20)         NOT NULL DEFAULT '' COMMENT '最近操作人名称',
  `sku_id`                   int(11)             NOT NULL COMMENT '商品id',
  `sku_name`                 varchar(50)         NOT NULL DEFAULT '' COMMENT '商品名称',
  `sku_format`               varchar(50)         NOT NULL DEFAULT '' COMMENT '商品单位',
  `rule_price`               decimal(15, 6)      NOT NULL COMMENT '规则价格',
  `current_price`            decimal(15, 6)      NOT NULL COMMENT '当前最终售价',
  `sku_image`                varchar(100)                 DEFAULT NULL COMMENT 'sku图片地址',
  `cp_type`                  int(11)             NOT NULL COMMENT '改价类型：1: 差值 2:打折 3:目标值',
  `syn_status`               int(11)                      DEFAULT 0,
  `marketing_diff_price`     decimal(15, 6)      NOT NULL COMMENT '营销差异值',
  `shop_type`                int(11)                      DEFAULT NULL COMMENT '门店类型',
  `original_price`           decimal(15, 6)               DEFAULT NULL COMMENT '原价',
  `class1_name`              varchar(50)                  DEFAULT NULL COMMENT '一级类目',
  `class3_name`              varchar(50)                  DEFAULT NULL COMMENT '三级类目',
  `class2_name`              varchar(50)                  DEFAULT NULL COMMENT '二级类目名称',
  `shop_area_id`             int(11)                      DEFAULT NULL COMMENT '门店区域id',
  `unit_format`              varchar(50)                  DEFAULT NULL COMMENT '商品单位',
  PRIMARY KEY (`id`),
  KEY `idx_activity_status` (`activity_id`, `status`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1000406169931620354
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE `t_activity`
(
  `id`           bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `gmt_create`   datetime            NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime            NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted`   tinyint(1)          NOT NULL DEFAULT '0' COMMENT '1=已删除',
  `version`      int(11)             NOT NULL DEFAULT '0' COMMENT '版本信息',
  `type`         int(11)             NOT NULL COMMENT '类型：10=改价 11=门店改价 12=总部改价 13=会员改价',
  `name`         varchar(50)         NOT NULL DEFAULT '' COMMENT '活动名称',
  `trigger_rule` varchar(1024)       NOT NULL DEFAULT ' ' COMMENT '触发规则',
  `limit_rule`   varchar(1024)       NOT NULL DEFAULT ' ' COMMENT '限制规则',
  `settle_rule`  varchar(1024)       NOT NULL DEFAULT ' ' COMMENT '结算规则',
  `status`       int(11)                      DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 80
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE `t_mq_log`
(
  `id`           bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `gmt_create`   datetime            NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime            NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_deleted`   tinyint(4)          NOT NULL DEFAULT '0' COMMENT '1=已删除',
  `topic`        varchar(50)         NOT NULL DEFAULT '' COMMENT 'topic字段',
  `msg_id`       varchar(64)         NOT NULL DEFAULT '' COMMENT '消息id',
  `key`          varchar(64)         NOT NULL DEFAULT '' COMMENT 'key',
  `payload`      text                NOT NULL COMMENT '消息体',
  `status`       tinyint(4)          NOT NULL COMMENT '1=发送成功 2=发送失败 3=重试失败',
  `retried_time` datetime            NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '已重试时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1000430658337832962
  DEFAULT CHARSET = utf8mb4;


CREATE TABLE `t_funds_account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '账户ID',
  `shop_name` varchar(50) DEFAULT NULL COMMENT '账户名称',
  `shop_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '账户类型：1.总部，2.大区，3.片区，4.门店',
  `total_amount` decimal(15,6) NOT NULL DEFAULT '0.000000' COMMENT '总费用',
  `lock_amount` decimal(15,6) NOT NULL DEFAULT '0.000000' COMMENT '锁住费用',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '账户状态，1.正常，2.注销，3.挂起',
  `gmt_create` datetime NOT NULL,
  `gmt_modified` datetime NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `idx_shop_id_type` (`shop_id`,`shop_type`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='费用账户';

CREATE TABLE `t_funds_apply` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) NOT NULL COMMENT '账户ID',
  `apply_user_id` bigint(20) NOT NULL COMMENT '申请人用户ID',
  `apply_user_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '申请人用户名',
  `apply_amount` decimal(15,6) NOT NULL COMMENT '申请额度',
  `apply_remark` varchar(50) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '申请描述',
  `apply_type` int(1) NOT NULL COMMENT '申请类型，1.用户，2.系统',
  `examine_user_id` bigint(20) DEFAULT NULL COMMENT '审核人用户ID',
  `examine_user_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '审核人用户名',
  `examine_remark` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '审核描述',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '审核状态:0.审核中，4.通过，5.驳回',
  `gmt_create` int(11) NOT NULL COMMENT '创建时间',
  `gmt_modified` int(11) NOT NULL COMMENT '更改时间',
  `is_deleted` int(11) NOT NULL COMMENT '删除标记',
  `apply_progress` varchar(255) CHARACTER SET utf8mb4 NOT NULL COMMENT '申请进程',
  `examine_type` int(1) DEFAULT '2' COMMENT '审核类型，1.系统自动审核 2.独裁审批 3.流程审批',
  `current_amount` decimal(15,6) NOT NULL COMMENT '当前金额',
  `examine_time` int(11) DEFAULT NULL COMMENT '审核时间',
  `account_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '账户名',
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '门店ID',
  `shop_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '账户类型：1.总部，2.大区，3.片区，4.门店',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_account_id` (`account_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='费用申请';

CREATE TABLE `t_funds_change_note` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '账户ID',
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '门店ID',
  `shop_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '账户类型：1.总部，2.大区，3.片区，4.门店',
  `target_shop` int(11) NOT NULL DEFAULT '0' COMMENT '产生费用的门店信息',
  `change_before` decimal(15,6) NOT NULL DEFAULT '0.000000' COMMENT '变更之前',
  `change_amount` decimal(15,6) NOT NULL DEFAULT '0.000000' COMMENT '变更费用',
  `change_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '交易类型：1.加 -1减',
  `change_after` decimal(15,6) NOT NULL DEFAULT '0.000000' COMMENT '变更之后',
  `business_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '业务类型：1.费用申请，2.单品费用收入，3.营销费用支出，4.订单抹零，5.订单改价，6.周清收入，7.周清支出',
  `is_sku_clean` int(11) NOT NULL DEFAULT '0' COMMENT '是否参与周清1：参与0：不参与',
  `activity_id` bigint(20) NOT NULL DEFAULT '-1' COMMENT '活动ID',
  `business_id` varchar(56) NOT NULL DEFAULT '' COMMENT '业务ID',
  `business_code` varchar(56) NOT NULL DEFAULT '' COMMENT '业务编码',
  `business_real_amount` decimal(15,6) NOT NULL DEFAULT '0.000000' COMMENT '销售额',
  `business_marketing_amount` decimal(15,6) NOT NULL DEFAULT '0.000000' COMMENT '促销费用',
  `business_gmv` decimal(15,6) NOT NULL DEFAULT '0.000000' COMMENT 'GMV',
  `create_day` varchar(50) NOT NULL DEFAULT '' COMMENT '日期',
  `remark` varchar(250) NOT NULL DEFAULT '' COMMENT '描述',
  `gmt_create` datetime NOT NULL,
  `gmt_modified` datetime NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_account_id` (`account_id`),
  KEY `idx_create_day` (`create_day`),
  KEY `idx_gmt_create` (`gmt_create`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='费用流水';

CREATE TABLE `t_funds_lock_change` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `business_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '活动ID',
  `account_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '账户ID',
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '门店ID',
  `shop_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '账户类型：1.总部，2.大区，3.片区，4.门店',
  `change_amount` decimal(15,6) NOT NULL DEFAULT '0.000000' COMMENT '变更费用',
  `change_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '交易类型：1.加 -1减',
  `create_day` varchar(50) NOT NULL DEFAULT '' COMMENT '创建时间',
  `gmt_create` datetime NOT NULL,
  `gmt_modified` datetime NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_create_day` (`create_day`),
  KEY `idx_gmt_create` (`gmt_create`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='锁定费用流水';

CREATE TABLE `t_funds_settle_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '账户ID',
  `order_code` varchar(52) NOT NULL DEFAULT '' COMMENT '订单号',
  `shop_id` int(11) NOT NULL DEFAULT '0' COMMENT '门店ID',
  `shop_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '账户类型：1.总部，2.大区，3.片区，4.门店',
  `real_pay` decimal(15,6) NOT NULL DEFAULT '0.000000' COMMENT '订单金额',
  `marketing_pay` decimal(15,6) NOT NULL DEFAULT '0.000000' COMMENT '营销销售额',
  `funds_amount` decimal(15,6) NOT NULL DEFAULT '0.000000' COMMENT '订单总营销费用',
  `shop_funds_amount` decimal(15,6) NOT NULL DEFAULT '0.000000' COMMENT '门店营销费用',
  `use_amount` decimal(15,6) NOT NULL DEFAULT '0.000000' COMMENT '总使用营销费用',
  `shop_use_amount` decimal(15,6) NOT NULL DEFAULT '0.000000' COMMENT '门店使用营销费用',
  `create_day` varchar(50) NOT NULL DEFAULT '' COMMENT '日期',
  `gmt_create` datetime NOT NULL,
  `gmt_modified` datetime NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_shop_id_type` (`shop_id`,`shop_type`),
  KEY `idx_account_id` (`account_id`),
  KEY `idx_create_day` (`create_day`),
  KEY `idx_gmt_create` (`gmt_create`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='费用订单信息';


CREATE TABLE `t_shop_promotion_rule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键自增',
  `shop_grade` int(11) NOT NULL COMMENT '门店等级(123)',
  `shop_type` int(11) NOT NULL COMMENT '门店类别(1.A,2.B,3.C)',
  `loss_percent` decimal(12,6) NOT NULL DEFAULT '0.000000' COMMENT '损耗率',
  `promotion_percent` decimal(12,6) NOT NULL DEFAULT '0.000000' COMMENT '营销费率',
  `base_percent` decimal(12,6) NOT NULL DEFAULT '0.000000' COMMENT '总部占比',
  `area_percent` decimal(12,6) NOT NULL DEFAULT '0.000000' COMMENT '大区占比',
  `shop_percent` decimal(12,6) NOT NULL DEFAULT '0.000000' COMMENT '门店占比',
  `operator_id` int(11) NOT NULL DEFAULT '0' COMMENT '操作人id',
  `operator_name` varchar(50) NOT NULL DEFAULT '0' COMMENT '操作人名称',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除(0:未删除1:已删除)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='门店营销费用规则表';

CREATE TABLE `t_shop_week_clear` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键自增',
  `shop_id` bigint(20) NOT NULL COMMENT '门店id',
  `sku_id` bigint(20) NOT NULL COMMENT '4skuid',
  `origin_price` decimal(15,6) NOT NULL COMMENT '商品每日原价',
  `real_amount` decimal(15,6) NOT NULL COMMENT '商品当日实际到货数量',
  `price_amount` decimal(15,6) NOT NULL COMMENT '商品实际到货值',
  `is_skuClear` int(11) NOT NULL COMMENT 'sku是否参与周清(0否，1是)',
  `create_day` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '日期',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  `is_deleted` int(11) NOT NULL DEFAULT '0' COMMENT '删除',
  PRIMARY KEY (`id`),
  KEY `idx_shop_id` (`shop_id`,`create_day`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='周清货值表';



